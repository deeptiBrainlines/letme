﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
namespace BE
{
   public class QuestionMasterBE
    {
        private string  QuestionId_PK;

        public string  propQuestionId_PK
        {
            get { return QuestionId_PK; }
            set { QuestionId_PK = value; }
        }
        private string  Question;

        public string  propQuestion
        {
            get { return Question; }
            set { Question = value; }
        }

        private string  ClusterIds;

        public string  propClusterIds
        {
            get { return ClusterIds; }
            set { ClusterIds = value; }
        }
        private int NoOfAnswers;

        public int propNoOfAnswers
        {
            get { return NoOfAnswers; }
            set { NoOfAnswers = value; }
        }
        private string  IsReverseScore;

        public string   propIsReverseScore
        {
            get { return IsReverseScore; }
            set { IsReverseScore = value; }
        }
        private int Weightage;

        public int propWeightage
        {
            get { return Weightage; }
            set { Weightage = value; }
        }
        private string  FlagNeutralAnswer;

        public string  propFlagNeutralAnswer
        {
            get { return FlagNeutralAnswer; }
            set { FlagNeutralAnswer = value; }
        }         

        private string  CreatedBy;

        public string  propCreatedBy
        {
            get { return CreatedBy; }
            set { CreatedBy = value; }
        }

        private DateTime CreatedDate;

        public DateTime propCreatedDate
        {
            get { return CreatedDate; }
            set { CreatedDate = value; }
        }
        private string  UpdatedBy;

        public string  propUpdatedBy
        {
            get { return UpdatedBy; }
            set { UpdatedBy = value; }
        }
        private DateTime UpdatedDate;

        public DateTime propUpdatedDate
        {
            get { return UpdatedDate; }
            set { UpdatedDate = value; }
        }
        private string  DeletedBy;

        public string  propDeletedBy
        {
            get { return DeletedBy; }
            set { DeletedBy = value; }
        }
        private DateTime DeletedDate;

        public DateTime propDeletedDate
        {
            get { return DeletedDate; }
            set { DeletedDate = value; }
        }
        private string  IsActive;

        public string  propIsActive
        {
            get { return IsActive; }
            set { IsActive = value; }
        }

        private DataTable AnswerTable;

        public DataTable propAnswerTable
        {
            get { return AnswerTable; }
            set { AnswerTable = value; }
        }

        public string propAssessmentId_FK { get; set; }
        public const string SpQuestionMasterGet = "SpQuestionMasterGet";
        public const string SpQuestionMasterInsert = "SpQuestionMasterInsert";
        public const string SpQuestionMasterUpdate = "SpQuestionMasterUpdate";
        public const string SpQuestionMasterGetByQuestionId = "SpQuestionMasterGetByQuestionId";
        public const string SpQuestionMasterGetTotalnoAnswersForQuestion = "SpQuestionMasterGetTotalnoAnswersForQuestion";
        public const string SpQuestionMasterGetByAssessmentId = "SpQuestionMasterGetByAssessmentId";
    }
}
