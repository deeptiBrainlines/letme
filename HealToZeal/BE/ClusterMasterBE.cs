﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;

namespace BE
{
   public class ClusterMasterBE
    {
        private string  ClusterId_PK;

        public string  propClusterId_PK
        {
            get { return ClusterId_PK; }
            set { ClusterId_PK = value; }
        }

        private string ClusterName;

        public string propClusterName
        {
            get { return ClusterName; }
            set { ClusterName = value; }
        }
        private string  ClusterDescription;

        public string  propClusterDescription
        {
            get { return ClusterDescription; }
            set { ClusterDescription = value; }
        }
        private string  CreatedBy;

        public string  propCreatedBy
        {
            get { return CreatedBy; }
            set { CreatedBy = value; }
        }
        private DateTime CreatedDate;

        public DateTime propCreatedDate
        {
            get { return CreatedDate; }
            set { CreatedDate = value; }
        }
           private string  UpdatedBy;

       public string  propUpdatedBy
       {
           get { return UpdatedBy; }
           set { UpdatedBy = value; }
       }
       private DateTime UpdatedDate;

       public DateTime propUpdatedDate
       {
           get { return UpdatedDate; }
           set { UpdatedDate = value; }
       }
       private string  DeletedBy;

       public string  propDeletedBy
       {
           get { return DeletedBy; }
           set { DeletedBy = value; }
       }
       private DateTime DeletedDate;

       public DateTime propDeletedDate
       {
           get { return DeletedDate; }
           set { DeletedDate = value; }
       }
       private string  IsActive;

       public string  propIsActive
       {
           get { return IsActive; }
           set { IsActive = value; }
       }

       public const string SpClusterMasterGet="SpClusterMasterGet";
     
        //Cluster type
       public string propClusterTypeId_PK { get; set; }
       public string  propClusterTypeName { get; set; }
       public DataTable   propClusterIds { get; set; }

       public const string SpClusterTypeMasterInsert = "SpClusterTypeMasterInsert";
       public const string SpClusterTypeMasterGet = "SpClusterTypeMasterGet";
       public const string SpClusterTypeMasterGetClustersById = "SpClusterTypeMasterGetClustersById";
       public const string SpClusterTypeMasterUpdate = "SpClusterTypeMasterUpdate";
       public const string SpClusterTypeMasterCheckDuplicate = "SpClusterTypeMasterCheckDuplicate";
   }
}
