﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BE
{
    public class UsersFeedbackBE
    {
       public string propFeedBackId_PK { get; set; } 
       public string  propEmployeeId_FK { get; set; } 
       public string propFeedbackQuestionId_FK { get; set; } 
       public string propFeedbackAnswerId_FK { get; set; } 
       public string  propCreatedBy { get; set; } 
       public DateTime propCreatedDate { get; set; } 
       public string propUpdatedBy { get; set; }
       public DateTime  propUpdatedDate { get; set; }
       public string propDeletedBy { get; set; }
       public DateTime  propDeletedDate { get; set; }
       public string propIsActive { get; set; }
       public string propFeedback { get; set; }

       public const string SpUsersFeedbackInsert = "SpUsersFeedbackInsert";
       public const string SpFeedbackQuestionMasterGet = "SpFeedbackQuestionMasterGet";
       public const string SpFeedbackAnswerMasterGetById = "SpFeedbackAnswerMasterGetById";
       public const string SpGetUserFeedback = "SpGetUserFeedback";
    }
}
