﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BE
{
   public  class EmployeeAssessmentDetailsBE
    {
        public string propEmployeeAssessmentId_PK { get; set; }
        public string propEmployeeId_FK { get; set; }
        public int propEmployeeCompanyID { get; set; }
        public string propAssessmentId_FK { get; set; }
        public string propQuestionId_FK { get; set; }
        public string propAnswerId_FK { get; set; }
        public string propCreatedBy { get; set; }

        public string propClusterScore { get; set; }
        public string propClusterID { get; set; }

        public string batchid { get; set; }

        public DateTime propCreatedDate { get; set; }
        public string propUpdatedBy { get; set; }
        public DateTime propUpdatedDate { get; set; }
        public string propDeletedBy { get; set; }
        public DateTime propDeletedDate { get; set; }
        public string propInstanceCompanyRelationId_FK { get; set; }

        public const string SpEmployeeAssessmentDetailsInsert = "SpEmployeeAssessmentDetailsInsert";
        public const string SpEmployeeAssessmentDetailsGet = "SpEmployeeAssessmentDetailsGet";
        public const string SpEmployeeAssessmentDetailsGetAll = "SpEmployeeAssessmentDetailsGetAll";
        public const string SpEmployeeAssessmentDetailsGetCount = "SpEmployeeAssessmentDetailsGetCount";

        public const string ImportEmployeeDeatils = "ImportEmployeeDeatils";
        public const string ImportEmployeeDeatilsClusterwise = "ImportEmployeeDeatilsClusterwise";
        public string propAssessmentInstanceId_FK { get; set; }
        public string propClustertotalcount { get; set; }
        public decimal propPercentage { get; set; }
        public string propCategory { get; set; }

        public string propClusterScoreO { get; set; }
        public string propClusterIDO { get; set; }
        public string propClustertotalcountO{ get; set; }

        public string propClusterScoreS { get; set; }
        public string propClusterIDS { get; set; }
        public string propClustertotalcountS { get; set; }

        public string propClusterScoreM { get; set; }
        public string propClusterIDM { get; set; }
        public string propClustertotalcountM { get; set; }

        public int pie_id { get; set; }
        public string percentagered { get; set; }
        public string percentageAmber1 { get; set; }
        public string percentageAmber2 { get; set; }
      
        public string percentagegreen { get; set; }
        public string batBatchId { get; set; }
        public string percentagecompid { get; set; }
        public string Color { get; set; }
        public string empid { get; set; }
    }
}
