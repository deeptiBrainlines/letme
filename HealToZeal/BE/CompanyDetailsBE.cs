﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BE
{
   public class CompanyDetailsBE
   {
       private string   CompanyId_PK;

       public string  propCompanyId_PK
       {
           get { return CompanyId_PK; }
           set { CompanyId_PK = value; }
       }

       private string CompanyName;

       public string propCompanyName
       {
           get { return CompanyName; }
           set { CompanyName = value; }
       }

       private string  DomainId_FK;

       public string  propDomainId_FK
       {
           get { return DomainId_FK; }
           set { DomainId_FK = value; }
       }

       private int NoOfEmployees;

       public int propNoOfEmployees
       {
           get { return NoOfEmployees; }
           set { NoOfEmployees = value; }
       }

       private string UserName;

       public string propUserName
       {
           get { return UserName; }
           set { UserName = value; }
       }
       private string Password;

       public string propPassword
       {
           get { return Password; }
           set { Password = value; }
       }

       private string CompanyAddress;

       public string propCompanyAddress
       {
           get { return CompanyAddress; }
           set { CompanyAddress = value; }
       }
       private string City;

       public string propCity
       {
           get { return City; }
           set { City = value; }
       }
       private string Country;

       public string propCountry
       {
           get { return Country; }
           set { Country = value; }
       }

       private string ZipCode;

       public string propZipCode
       {
           get { return ZipCode; }
           set { ZipCode = value; }
       }
       private string CompanyContactNo;

       public string propCompanyContactNo
       {
           get { return CompanyContactNo; }
           set { CompanyContactNo = value; }
       }

       private string ContactPersonName;

       public string propContactPersonName
       {
           get { return ContactPersonName; }
           set { ContactPersonName = value; }
       }

       private string EmailId;

       public string propEmailId
       {
           get { return EmailId; }
           set { EmailId = value; }
       }
       private string ContactPersonContactNo;

       public string propContactPersonContactNo
       {
           get { return ContactPersonContactNo; }
           set { ContactPersonContactNo = value; }
       }
       private string MobileNo;

       public string propMobileNo
       {
           get { return MobileNo; }
           set { MobileNo = value; }
       }

       private string  CreatedBy;

       public string  propCreatedBy
       {
           get { return CreatedBy; }
           set { CreatedBy = value; }
       }

       private DateTime CreatedDate;

       public DateTime propCreatedDate
       {
           get { return CreatedDate; }
           set { CreatedDate = value; }
       }
       private string  UpdatedBy;

       public string  propUpdatedBy
       {
           get { return UpdatedBy; }
           set { UpdatedBy = value; }
       }
       private DateTime UpdatedDate;

       public DateTime propUpdatedDate
       {
           get { return UpdatedDate; }
           set { UpdatedDate = value; }
       }
       private string  DeletedBy;

       public string  propDeletedBy
       {
           get { return DeletedBy; }
           set { DeletedBy = value; }
       }
       private DateTime DeletedDate;

       public DateTime propDeletedDate
       {
           get { return DeletedDate; }
           set { DeletedDate = value; }
       }
       private string  IsActive;

       public string  propIsActive
       {
           get { return IsActive; }
           set { IsActive = value; }
       }
       #region Company Details

       public const string SpGetAllCompanyDetails = "SpGetAllCompanyDetails";
       public const string SpCompanyDetailsGet = "SpCompanyDetailsGet";
       public const string SpCompanyDetailsInsert = "SpCompanyDetailsInsert";
       public const string SpCompanyDetailsUpdate = "SpCompanyDetailsUpdate";
       public const string SpCompanyDetailsGetByCompanyId = "SpCompanyDetailsGetByCompanyId";
       public const string SpCompanyDetailsLogin = "SpCompanyDetailsLogin";
       public const string SPCompanyReportStatistics = "SPCompanyReportStatistics11";
        public const string SPCompanyReportStatisticsnew = "SPCompanyReportStatisticsnew"; //Added by meenakshi 17.12.18
        public const string SpClusterPercentage = "SpClusterPercentage11";
        public const string SpClusterPercentage1 = "SpClusterPercentage"; //Added by meenakshi 18.12.18
        public const string SpAllScoreByEmployeeIdAndAssessmentIdForCompany = "SpAllScoreByEmployeeIdAndAssessmentIdForCompany";
       public const string SpEmployeeResultGetForCompany = "SpEmployeeResultGetForCompany";

       //Code by Poonam
       public const string SpGetALlCadre = "SpGetALlCadre";
       public const string SpGetAllDepartments = "SpGetAllDepartments";
        public const string Sp_GetAnswerData = "Sp_GetAnswerData";
        public const string Sp_GetQuestionData = "Sp_GetQuestionData";
        public const string SpGetcountofEmployee = "SpGetcountofEmployee";
        public const string sp_GetStudentResult = "sp_GetStudentResult";
        public const string usp_GetClusterScore = "usp_GetClusterScore";
        //17Dec15 swaranjali
        public const string SpCompanyDetailsGetByEmployeeId = "SpCompanyDetailsGetByEmployeeId";
        public const string Sp_get_Allclusterscore = "Sp_get_Allclusterscore";//Added by meenakshi
        public const string Sp_get_clustername = "Sp_get_clustername";//Added by meenakshi
        public const string Sp_clusterpercentage3_1_19 = "Sp_clusterpercentage3_1_19";//Added by meenakshi
        public const string SpGetPercentageClusterwise = "SpGetPercentageClusterwise";//Added by meenakshi
        #endregion Company Details

    }
}
