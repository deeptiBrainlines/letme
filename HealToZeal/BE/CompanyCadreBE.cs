﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BE
{
    public class CompanyCadreBE
    {
        public string propCadreId_PK { get; set; }
        public string propCompanyId_FK { get; set; }
        public string propDeptId_FK { get; set; }       

        public string propCadreName { get; set; }        
        public string propCreatedBy { get; set; }
        public DateTime propCreatedDate { get; set; }
        public string propUdpatedBy { get; set; }
        public DateTime propUpdatedDate { get; set; }
        public string propDeletedBy { get; set; }
        public DateTime propDeletedDate { get; set; }
        public string propIsActive { get; set; }

        public const string SpCadreInsert = "SpCompanyCadreInsert";        
        public const string SpCompanyCadreGetByCompanyId = "SpCompanyCadreGetByCompanyId";
        public const string SpGetAllCadrevalue = "SpGetAllCadrevalue";
        public const string SpCadreGetByCadreId = "SpCadreGetByCadreId";
        public const string SpCadreUpdate = "SpCompanyCadreUpdate";
        public const string spDeleteCadre = "spDeleteCadre";
        public const string SpCadreDelete = "SpCadreDelete";
        public const string SpCadreGetByName = "SpCadreGetByName";
       
    }
}
