﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;

namespace BE
{
   public class AssessmentBE
    {
        private string AssessmentId_PK;

        public string propAssessmentId_PK
        {
            get { return AssessmentId_PK; }
            set { AssessmentId_PK = value; }
        }

        private string  AssessmentName;

        public string  propAssessmentName
        {
            get { return AssessmentName; }
            set { AssessmentName = value; }
        }

        private int NoOfQuestions;

        public int propNoOfQuestions
        {
            get { return NoOfQuestions; }
            set { NoOfQuestions = value; }
        }

        private string PersonaId_FK;

        public string propPersonaId_FK
        {
            get { return PersonaId_FK; }
            set { PersonaId_FK = value; }
        }

        private string  IsComplete;

        public string  propIsComplete
        {
            get { return IsComplete; }
            set { IsComplete = value; }
        }

        private string CreatedBy;

        public string propCreatedBy
        {
            get { return CreatedBy; }
            set { CreatedBy = value; }
        }

        private DateTime CreatedDate;

        public DateTime propCreatedDate
        {
            get { return CreatedDate; }
            set { CreatedDate = value; }
        }
        private string UpdatedBy;

        public string propUpdatedBy
        {
            get { return UpdatedBy; }
            set { UpdatedBy = value; }
        }
        private DateTime UpdatedDate;

        public DateTime propUpdatedDate
        {
            get { return UpdatedDate; }
            set { UpdatedDate = value; }
        }
        private string DeletedBy;

        public string propDeletedBy
        {
            get { return DeletedBy; }
            set { DeletedBy = value; }
        }
        private DateTime DeletedDate;

        public DateTime propDeletedDate
        {
            get { return DeletedDate; }
            set { DeletedDate = value; }
        }
        private string IsActive;

        public string propIsActive
        {
            get { return IsActive; }
            set { IsActive = value; }
        }

        private string IsStd;
        public string propIsStd
        {
            get { return IsStd; }
            set { IsStd = value; }
        }

        private string Description;
        public string propDescription
        {
            get { return Description; }
            set { Description = value; }
        }

        public const string SpAssessmentGet = "SpAssessmentGet";
        public const string SpAssessmentUpdate = "SpAssessmentUpdate";
        public const string SpAssessmentInsert = "SpAssessmentInsert";
        public const string SpAssessmentGetByAssessmentId = "SpAssessmentGetByAssessmentId";
     //   public const string SpAssessmentQuestionsRelationGet = "SpAssessmentQuestionsRelationGet";
      
       // 14 july 15
       // Assessment instance master

        public string  propAssessmentInstanceId_PK { get; set; }
        public string propAssessmentInstanceName { get; set; }

        //Assessment Instance Assessment Relation
        public string  propInstanceRelationId_PK { get; set; }
        public string propAssessmentInstanceId_FK { get; set; }
       
        public DataTable propAssessmentIds { get; set; }
        public string propDisplayResult { get; set; }

        //Assessment Instance Company Relation
       //Added by Poonam on 29th july
        public DataTable propAssessmentInstanceIds { get; set; }
       //
        public string  propInstanceCompanyRelationId_PK{ get; set; }
    //  public string  propAssessmentInstanceId_FK { get; set; }
        public DataTable  propCompanyIds { get; set; }
        public DateTime  propValidFrom { get; set; }
        public DateTime propValidTo { get; set; }
        public string propStatus { get; set; }
        public string  propCompanyId_FK { get; set; }
        public string propBatchID_FK { get; set; }


        public const string SpAssessmentInstanceAssessmentInsert = "SpAssessmentInstanceAssessmentInsert";
        public const string SpAssessmentInstanceCompanyRelationInsert = "SpAssessmentInstanceCompanyRelationInsert";
       //sp Added by Poonam
        public const string SpAssessmentInstanceCompanyRelationInsertRecord = "SpAssessmentInstanceCompanyRelationInsertRecord";
        public const string SpCompanyRelationAssessmentInstanceUpdate = "SpCompanyRelationAssessmentInstanceUpdate";
        public const string spTocheckInstanceExistsOrNot = "spTocheckInstanceExistsOrNot";
        public const string SpGetBatchCompanyWise = "SpGetBatchCompanyWise";



        public const string SpAssessmentInstanceMasterGet = "SpAssessmentInstanceMasterGet";
        public const string SpAssessmentInstanceAssessmentRelationGetByInstanceId = "SpAssessmentInstanceAssessmentRelationGetByInstanceId";
        public const string SpAssessmentInstanceCompanyRelationGet = "SpAssessmentInstanceCompanyRelationGet";
        public const string SpAssessmentInstanceMasterCheckDuplicate = "SpAssessmentInstanceMasterCheckDuplicate";
        public const string SpAssessmentInstanceGetByInstanceId = "SpAssessmentInstanceGetByInstanceId";
        public const string SpAssessmentInstanceAssessmentUpdate = "SpAssessmentInstanceAssessmentUpdate";
        public const string SpAssessmentInstanceCompanyRelationUpdate = "SpAssessmentInstanceCompanyRelationUpdate";
        public const string SpAssessmentInstanceCompanyByIdUpdate = "SpAssessmentInstanceCompanyByIdUpdate";

        public const string SpGetAssessmentsByEmployeeId = "SpGetAssessmentsByEmployeeId";
       //For company
        public const string SpEmployeeAssementsGetForCompany = "SpEmployeeAssementsGetForCompany";

        public const string SpAssessmentInstanceMasterGetByCompanyId = "SpAssessmentInstanceMasterGetByCompanyId";

     //   public const string spAssesmentWiseBatch = "spAssesmentWiseBatch";

        public const string SpCompanyRelationAssessmentInstanceInsert = "SpCompanyRelationAssessmentInstanceInsert1";
    }
}
