﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BE
{
   public  class RemedySuggestionsBE
    {
       //24 july 2015
        public string RemedySuggestionId_PK { get; set; }
        public string propSuggestion { get; set; }
        public string propAssessmentId_FK { get; set; }
        public string propClusterId_FK { get; set; }
        public string propColor { get; set; }
        public int propMinScore { get; set; }
        public int propMaxScore { get; set; }
        public int propMinScorePercentage { get; set; }
        public int propMaxScorePercentage { get; set; }
        public string  propCreatedBy { get; set; }
        public DateTime propCreatedDate { get; set; }
        public string propUpdatedBy { get; set; }
        public DateTime propUpdatedDate { get; set; }
        public string propDeletedBy { get; set; }
        public DateTime propDeletedDate { get; set; }
        public string propIsActive { get; set; }

        public const string SpRemedySuggestionsGet = "SpRemedySuggestionsGet";
        public const string SpRemedySuggestionsInsert = "SpRemedySuggestionsInsert";
        public const string SpRemedySuggestionsUpdate = "SpRemedySuggestionsUpdate";
        public const string SpRemedySuggestionsGetById = "SpRemedySuggestionsGetById";
        public const string SpRemedySuggestionsGetByAssessmentAndClusterId = "SpRemedySuggestionsGetByAssessmentAndClusterId";
    }
}
