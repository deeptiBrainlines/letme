﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BE
{
   public class AssessmentQuestionRelationBE
    {
        private string  AssessmentQuestionId_PK;

        public string  propAssessmentQuestionId_PK
        {
            get { return AssessmentQuestionId_PK; }
            set { AssessmentQuestionId_PK = value; }
        }

        private string  AssessmentId_FK;

        public string  propAssessmentId_FK
        {
            get { return AssessmentId_FK; }
            set { AssessmentId_FK = value; }
        }

        private string  QuestionId_FK;

        public string  propQuestionId_FK
        {
            get { return QuestionId_FK; }
            set { QuestionId_FK = value; }
        }


        private string CreatedBy;

        public string propCreatedBy
        {
            get { return CreatedBy; }
            set { CreatedBy = value; }
        }

        private DateTime CreatedDate;

        public DateTime propCreatedDate
        {
            get { return CreatedDate; }
            set { CreatedDate = value; }
        }
        private string UpdatedBy;

        public string propUpdatedBy
        {
            get { return UpdatedBy; }
            set { UpdatedBy = value; }
        }
        private DateTime UpdatedDate;

        public DateTime propUpdatedDate
        {
            get { return UpdatedDate; }
            set { UpdatedDate = value; }
        }
        private string DeletedBy;

        public string propDeletedBy
        {
            get { return DeletedBy; }
            set { DeletedBy = value; }
        }
        private DateTime DeletedDate;

        public DateTime propDeletedDate
        {
            get { return DeletedDate; }
            set { DeletedDate = value; }
        }
        private string IsActive;

        public string propIsActive
        {
            get { return IsActive; }
            set { IsActive = value; }
        }

       public const string SpAssessmentQuestionsRelationGet = "SpAssessmentQuestionsRelationGet";
       public const string SpEmployeeAssessmentDetailsGetAll = "SpEmployeeAssessmentDetailsGetAll";
    }
}
