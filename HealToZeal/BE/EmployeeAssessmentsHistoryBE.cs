﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BE
{
  public class EmployeeAssessmentsHistoryBE
    {
        public string  propEmployeeAssessmentHistoryId_PK { get; set; }
        public string propEmployeeId_FK { get; set; }
        public string  propAssessmentId_FK { get; set; }
        public string  propAssessmentSubmitted { get; set; }
        public int propScore { get; set; }
        public string  propTimeTakenforAssessment { get; set; }
        public string propCreatedBy { get; set; }
        public DateTime propCreatedDate { get; set; }
        public string  propUpdatedBy { get; set; }
        public DateTime propUpdatedDate { get; set; }
        public string propDeletedBy { get; set; }
        public DateTime  propDeletedDate { get; set; }
        public string propIsActive { get; set; }
        public string  propAssessmentInstanceId_FK { get; set; }
        public string  propInstanceCompanyRelationId_FK { get; set; }
        public const string SpEmployeeAssessmentsHistoryInserttest = "SpEmployeeAssessmentsHistoryInserttest";
        public const string SpEmployeeAssessmentsHistoryInsert = "SpEmployeeAssessmentsHistoryInsert";
        public const string SpEmployeeAssessmentsHistoryInsertnew = "SpEmployeeAssessmentsHistoryInsertnew";

        public const string SpEmployeeAssessmentsHistoryAssessmentSubmited = "SpEmployeeAssessmentsHistoryAssessmentSubmited";
        public const string SpAssessmentHistoryAllGet = "SpAssessmentHistoryAllGet";
        public const string SpEmployeeAssessmentHistoryGetEmployees = "SpEmployeeAssessmentHistoryGetEmployees";
        public const string SpEmployeeAssementsGet = "SpEmployeeAssementsGet1";
        public int propEmployeeCompanyID { get; set; }

        public string batchid { get; set; }
        
    }
}
