﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BE
{
   public  class EmployeeBE
    {
        public string propEmployeeId_PK { get; set; }
        public string propCompanyId_FK { get; set; }
        public int  propEmployeeCompanyId { get; set; }
        public string propEmployeeCompanyIdNew { get; set; }
        public string  propUserName { get; set; }
        public string propPassword { get; set; }
        public string  propFirstName { get; set; }
        public string  propLastName { get; set; }
        public string  propEmployeeAddress { get; set; }
        public int propCity { get; set; }
        public int propCountry { get; set; }
        public string propZipCode { get; set; }
        public string  propEmailId { get; set; }
        public string  propContactNo { get; set; }
        public string  propMobileNo { get; set; }
        public string  propAge { get; set; }
        public string  propCadreId_FK { get; set; }
        public string propDeptId_PK { get; set; }
        public string  propExperienceInYears { get; set; }
        public string propGender { get; set; }
        public string  propOccupation { get; set; }
        public string propQualification { get; set; }
        public string propPosition { get; set; }
        public string propCreatedBy { get; set; }
        public DateTime propCreatedDate { get; set; }
        public string  propUdpatedBy { get; set; }
        public DateTime  propUpdatedDate { get; set; }
        public string  propDeletedBy { get; set; }
        public DateTime propDeletedDate { get; set; }
        public string propIsActive { get; set; }
        public string propMangerID { get; set; }
        public string PropBatchName{get; set;}
        public string PropBatchID { get; set; }
        public string PropTandD { get; set; }
        public string Verificationcode { get; set; }
        public string VerificationFlag { get; set; }
        public string PaymentFlag { get; set; }


        public const string SpEmployeeInsert = "SpEmployeeInsert";
        public const string SpEmployeeImport = "SpEmployeeImport";
        public const string SpBatchInsert = "SpBatchInsert";
        public const string SpGetBatchName = "SpGetBatchName";
        public const string SpEmployeesGetByCompanyId = "SpEmployeesGetByCompanyId";

        public const string SpGetEmployeeBatchWise = "SpGetEmployeeBatchWise";
        public const string SpCompanyCadreGetByCompanyId = "SpCompanyCadreGetByCompanyId";
        public const string spAssesmentWiseBatch = "spAssesmentWiseBatch";
        public const string Sp_ToCheckBatch = "Sp_ToCheckBatch";
       //code by Poonam on 25 july 2016
        public const string SpCompanyDepartmentGetByCompanyId = "SpCompanyDepartmentGetByCompanyId";
       //

        public const string SpInsertManger = "SpInsertManger";
        public const string SpEmployeesLogin = "SpEmployeesLogin";
        public const string SpEmployeesLoginGmail = "SpEmployeesLoginGmail";
        public const string SpEmployeeDetails = "SpEmployeeDetails";
        public const string SpEmployeesGetByEmployeeId = "SpEmployeesGetByEmployeeId";
        public const string SpEmployeesUpdate = "SpEmployeesUpdate";
        public const string SpEmployeesFirstUpdate = "SpEmployeesFirstUpdate";
        public const string SpEmployeesUpdateByCompanyId = "SpEmployeesUpdateByCompanyId";
        public const string SpEmployeeAssessmentClusterwiseReport= "SpEmployeeAssessmentClusterwiseReport";
        public const string SpAssessmentFinalResult = "SpAssessmentFinalResult";
        public const string SpResultTextMasterGet = "SpResultTextMasterGet";
        public const string SpSuggestionMasterGetRandom = "SpSuggestionMasterGetRandom";
        public const string SpEmployeesDelete = "SpEmployeesDelete";
        public const string SpEmployeeAssessmentSubmittedGetByEmployeeId = "SpEmployeeAssessmentSubmittedGetByEmployeeId";
        public const string SpEmployeeGetByEmployeeCompanyId = "SpEmployeeGetByEmployeeCompanyId";
        public const string SpEmployeeResultGet = "SpEmployeeResultGet";
        public const string SpEmployeesGetByCadreId = "SpEmployeesGetByCadreId";
        public const string SpEmployeeGetByName = "SpEmployeeGetByName";
        public const string SpDatewiseScoreByEmployeeCompanyId = "SpDatewiseScoreByEmployeeCompanyId";
        public const string SpGetStdVsChetanaclusterScore = "SpGetStdVsChetanaclusterScore";
        public const string SpStdVsChetanaScoreByCompanyID = "SpStdVsChetanaScoreByCompanyID";
        public const string SpEmployeeAllResultGet = "SpEmployeeAllResultGet";
        public const string SpEmployeeResultGetForEmployee = "SpEmployeeResultGetForEmployee";
        public const string SpParentEmployeeRelationshipGetParent = "SpParentEmployeeRelationshipGetParent";
        public const string SpParentEmployeeRelationshipGetByParentId = "SpParentEmployeeRelationshipGetByParentId";
        public const string SpEmployeeGetByNameByEmployeeParentId = "SpEmployeeGetByNameByEmployeeParentId";
        public const string SpEmployeesGetEmployeesNotSubmited = "SpEmployeesGetEmployeesNotSubmited";

        public const string SP_Getbyemployeebatchwise = "SP_Getbyemployeebatchwise";

        public const string SpEmployeeRegistration = "SpEmployeeRegistration";
        public const string SpEmployeesAcceptTermUpdate = "SpEmployeesAcceptTermAndonditionUpdate";
        public string propTestFlag { get; set; }
    }
}
