﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BE
{
  public class AnswerMasterBE
    {
        private string  AnswerId_PK;

        public string  propAnswerId_PK
        {
            get { return AnswerId_PK; }
            set { AnswerId_PK = value; }
        }
        private string  QuestionId_FK;

        public string  propQuestionId_FK
        {
            get { return QuestionId_FK; }
            set { QuestionId_FK = value; }
        }

        private string Answer;

        public string propAnswer
        {
            get { return Answer; }
            set { Answer = value; }
        }
        private string  AnswerDescription;

        public string  propAnswerDescription
        {
            get { return AnswerDescription; }
            set { AnswerDescription = value; }
        }
        private int ScoreForAnswer;

        public int propScoreForAnswer
        {
            get { return ScoreForAnswer; }
            set { ScoreForAnswer = value; }
        }
        

        private string  CreatedBy;

        public string  propCreatedBy
        {
            get { return CreatedBy; }
            set { CreatedBy = value; }
        }

        private DateTime CreatedDate;

        public DateTime propCreatedDate
        {
            get { return CreatedDate; }
            set { CreatedDate = value; }
        }
        private string  UpdatedBy;

        public string  propUpdatedBy
        {
            get { return UpdatedBy; }
            set { UpdatedBy = value; }
        }
        private DateTime UpdatedDate;

        public DateTime propUpdatedDate
        {
            get { return UpdatedDate; }
            set { UpdatedDate = value; }
        }
        private string  DeletedBy;

        public string  propDeletedBy
        {
            get { return DeletedBy; }
            set { DeletedBy = value; }
        }
        private DateTime DeletedDate;

        public DateTime propDeletedDate
        {
            get { return DeletedDate; }
            set { DeletedDate = value; }
        }
        private string  IsActive;

        public string  propIsActive
        {
            get { return IsActive; }
            set { IsActive = value; }
        }

        public const string SpAnswerMasterGet = "SpAnswerMasterGet";
        public const string SpAnswerMasterInsert = "SpAnswerMasterInsert";
        public const string SpAnswerMasterUpdate = "SpAnswerMasterUpdate";
        public const string SpAnswerMasterGetByQuestionId = "SpAnswerMasterGetByQuestionId";
        public const string SpAnswerMasterGetByAnswerId = "SpAnswerMasterGetByAnswerId";
        public const string SpAnswerMasterGetWithDescriptionByQuestionId = "SpAnswerMasterGetWithDescriptionByQuestionId";



        public const string spGetQuestionID = "spGetQuestionID";
        public const string spgetclusterID = "spgetclusterID";
        public const string SpgetAnswerID_FK = "SpgetAnswerID_FK";
        public const string SpgetAnswerID_FK1 = "SpgetAnswerID_FK1";
        public const string SpGetAssessmentID = "SpGetAssessmentID";

    }
}
