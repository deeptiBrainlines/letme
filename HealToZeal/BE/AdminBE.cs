﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BE
{
  public  class AdminBE
    {
      public string  propUserId_PK { get; set; }
     public string propName { get; set; }
      public string  propEmailId { get; set; }
      public string  propPassword { get; set; }
      public string  propReadOnlyOrReadWrite { get; set; }
	public string  propCreatedBy { get; set; }
      public DateTime  propCreatedDate { get; set; }
      public string propUpdatedBy { get; set; }
      public DateTime propUpdatedDate { get; set; }
	public string propDeletedBy { get; set; }
	public DateTime  propDeletedDate { get; set; }
      public string  propIsActive { get; set; }

      public const string SpAdminLogin = "SpAdminLogin";
    }
}
