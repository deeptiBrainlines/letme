﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BE
{
   public class CompanyDepartmentBE
    {
        //Code by Poonam  on 14 july 2016

        public string propDeptId_PK { get; set; }
        public string propCompanyId_FK { get; set; }
        public string propDepartmentName { get; set; }
        public string propCreatedBy { get; set; }
        public DateTime propCreatedDate { get; set; }
        public string propUdpatedBy { get; set; }
        public DateTime propUpdatedDate { get; set; }
        public string propDeletedBy { get; set; }
        public DateTime propDeletedDate { get; set; }
        public string propIsActive { get; set; }

        public const string SpCompanyDeaprtmentInsert = "SpCompanyDepartmentInsert";
        public const string spTocheckDepartmentExistsOrNot = "spTocheckDepartmentExistsOrNot";
        public const string SpCompanyDepartmentGetByCompanyId = "SpCompanyDepartmentGetByCompanyId";
        public const string GetcompnyName = "SpGetcompnyName";
        public const string SpCompanyDepartmentGetByCompanyIdandDeptID = "SpCompanyDepartmentGetByCompanyIdandDeptID";
        public const string GetDeptID = "GetDeptID";
        public const string SpDepartmentUpdate = "SpCompanyDepartmentUpdate";
        public const string spDeleteDepartMent = "spDeleteDepartMent";
        public const string spAllDeatilsOfDepartment = "spAllDeatilsOfDepartment";
        public const string SpGetAllDepartment = "SpGetAllDepartment";
    }
}
