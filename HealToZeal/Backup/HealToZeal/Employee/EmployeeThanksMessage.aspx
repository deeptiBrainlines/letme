﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="EmployeeThanksMessage.aspx.cs" Inherits="HealToZeal.Employee.EmployeeThanksMessage" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div class="divshortProfClass" style="width:90%; font-family:Georgia; color:#333300; line-height:1.5; text-align:justify ;margin-left:20px" >
         <p>
        Thank you very much for participation in this experiment!
         </p>      
          <p>
          We will do our due diligence with the data we have collected and will definitely inform you about success of our experiment and how this experiment helped us further.
          </p>     
           <p>We appreciate your continued support and best wishes to Chetana TSPL!!</p>
           <p>
          Heal To Zeal Team
           </p>
            <p>
            Initiative by Chetana TSPL
            </p>
            <p>            
           www.chetanasystems.com
            </p>
                   

  
    </div>
    </form>
</body>
</html>
