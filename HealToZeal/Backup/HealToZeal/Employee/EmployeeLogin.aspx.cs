﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DAL;
using System.Data;
namespace HealToZeal.Employee
{
    public partial class EmployeeLogin : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnLogin_Click(object sender, EventArgs e)
        {
            try
            {
                EmployeeDAL objEmployeeDAL = new EmployeeDAL();

                DataTable DtEmployeeDetails = objEmployeeDAL.EmployeesLogin(txtUserName.Text, Encryption.Encrypt(txtPassword.Text));
                if (DtEmployeeDetails != null && DtEmployeeDetails.Rows.Count > 0)
                {
                    Session["UserName"] = txtUserName.Text;
                    //Session["DtCompanyDetails"] = DtCompanyDetails;
                    Session["EmployeeId"] = DtEmployeeDetails.Rows[0]["EmployeeId_PK"].ToString();
                    if (Request.QueryString["first"] != null)
                    {
                        Response.Redirect("EmployeeHome.aspx?first=1");
                    }
                    else
                    {
                        Response.Redirect("EmployeeHome.aspx", false);
                    }
                }
                else
                {
                    lblMessage.Text = "Invalid username/password...";
                }
            }
            catch(Exception ex)
            {
                lblMessage.Text = ex.Message;
            }
          
        }

        protected void ImgBtnLogin_Click(object sender, ImageClickEventArgs e)
        {
            try
            {
                EmployeeDAL objEmployeeDAL = new EmployeeDAL();

                DataTable DtEmployeeDetails = objEmployeeDAL.EmployeesLogin(txtUserName.Text, Encryption.Encrypt(txtPassword.Text));
                if (DtEmployeeDetails != null && DtEmployeeDetails.Rows.Count > 0)
                {
                    Session["UserName"] = txtUserName.Text;
                    //Session["DtCompanyDetails"] = DtCompanyDetails;
                    Session["EmployeeId"] = DtEmployeeDetails.Rows[0]["EmployeeId_PK"].ToString();
                    if (Request.QueryString["first"] != null)
                    {
                        Response.Redirect("EmployeeHome.aspx?first=1");
                    }
                    else
                    {
                       Response.Redirect("EmployeeHome.aspx", false);
                    }
                }
                else
                {
                    lblMessage.Text = "Invalid username/password...";
                }
            }
            catch (Exception ex)
            {
                lblMessage.Text = ex.Message;
            }
          
        }
    }
}