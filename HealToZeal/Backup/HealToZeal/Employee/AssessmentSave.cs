﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HealToZeal.Employee
{
    public class AssessmentSave
    {
        private string Question;

        public string QuestionId_FK
        {
            get { return Question; }
            set { Question = value; }
        }
        private string Answer;

        public string AnswerId_FK
        {
            get { return Answer; }
            set { Answer = value; }
        }
        
    }
}