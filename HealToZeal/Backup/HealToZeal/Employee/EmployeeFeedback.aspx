﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Employee/Employee.Master" AutoEventWireup="true" CodeBehind="EmployeeFeedback.aspx.cs" Inherits="HealToZeal.Employee.EmployeeFeedback" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="EmployeeColumn" runat="server">
    <div class="divClass">
    <asp:Label ID="lblmessage" runat="server" ForeColor="Red" Text=""></asp:Label>
<table>
<tr>
<td colspan="2">
        <asp:DataList ID="DlistQuestions" runat="server" 
            onitemdatabound="DlistQuestions_ItemDataBound"  AlternatingItemStyle-BackColor="#CDFECD">
        <ItemTemplate>
       <div id="maincontainer">
        <table>
        <tr>
        <td style="padding-bottom:10px;padding-top:10px">
            <asp:Label ID="lblSrNo" runat="server" Text='<%#Eval("SrNo") %>'></asp:Label>
             <asp:Label ID="lblQuestionId" runat="server" Text='<%#Eval("FeedbackQuestionId_PK") %>' Visible="false"></asp:Label>
            <asp:Label ID="lblQuestion" runat="server" Text='<%#Eval("FeedbackQuestion") %>'></asp:Label>
           
        </td>
        </tr>
        <tr>
        <td>
            <asp:RadioButtonList ID="rblistAnswers" runat="server" RepeatDirection="Horizontal" >
            </asp:RadioButtonList>
             <asp:TextBox ID="txtFeedback" runat="server" TextMode="MultiLine" Width="100%" CssClass="txtmultiClass"></asp:TextBox>
        </td>
        </tr>
        </table>
        </div>
        </ItemTemplate>
        </asp:DataList>
</td>
</tr>
<%--<tr>
<td align="right">
    <asp:Label ID="lblFeedback" runat="server" Text="Any query"></asp:Label>
 </td>
<td>
    <asp:TextBox ID="txtFeedback" runat="server" TextMode="MultiLine" CssClass="txtmultiClass"></asp:TextBox>
</td>
</tr>--%>
<tr>
<td style="height:10%">

    <asp:Button ID="btnSubmit" runat="server" Text="Submit" CssClass="btnLoginClass" BackColor="#CDFECD"
        onclick="btnSubmit_Click" />
    &nbsp;
    
</td>
<td>

    <asp:Button ID="btnCancel" runat="server" Text="Cancel" CssClass="btnLoginClass" BackColor="#CDFECD"
        onclick="btnCancel_Click" />

</td>
</tr>
</table>
</div>
</asp:Content>
