﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="EmployeeMessage.aspx.cs" Inherits="HealToZeal.Employee.EmployeeMessage" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div class="divshortProfClass" style="width:90%; font-family:Georgia; color:#333300; line-height:1.5; text-align:justify ;margin-left:20px" >
    
     <h4 style="color:Red">Welcome to Heal To Zeal Content Validation Initiative! 
         </h4>
         <p>
         We appreciate your willingness to participate in this exercise and expect your thorough and frank feedback.
         </p>      
          <p>
          On the first time login, you are being asked to fill in some information about yourself. You will not be asked for identity anytime during this exercise.<u> The purpose of this exercise is to validate some of our assumptions and thought process and not to diagnose or comment on any of the individual answers or scores. Any personal information will not be shared with anyone, anytime.</u>
          You can trust us for the same and be rest assured.
          </p>     
           <p>Once you fill in this small form, you will be presented with a series of questions and answer options and you will be required to answer all of them. You might find some repetition of the questions but it is deliberate and needed to get the right results out of this important exercise.
           Estimated time needed is about 45 minutes.
           </p>
           <p>
           If you like, you can logout at any time and come back to answer remaining the questions. But your assessment will not be marked “submitted”, until you complete the same. So please make sure you complete the assessment and click the “Submit” button.
           </p>
            <p>
            There would be a small feedback form at the end and that’s it!
            </p>
            <p>
            We will definitely keep in touch and will let you know how this experiment goes, how it helps us in determining our further strategy!
            </p>
            <p>
           <b style="color:Red"> Thank you very much!</b>
            </p>

  
    </div>
    </form>
</body>
</html>
