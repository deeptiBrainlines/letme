﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Assessment.aspx.cs" MasterPageFile="~/Employee/Employee.Master" Inherits="HealToZeal.Employee.Assessment" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<%--<asp:Content ID="Content2" ContentPlaceHolderID="EmployeeColumn" runat="server">
    <link href="../Styles/Employee.css" rel="stylesheet" type="text/css" />
  <div class="divClass" style="height:500px;">
      <asp:Label ID="lblmessage" runat="server" ForeColor="Red"></asp:Label>
      <h4 style="text-align:center">   <asp:Label ID="lblAssessment" runat="server" Text="Assessment"></asp:Label></h4>
   
    <table style="height:80%">
    <tr style="height:90%">
    <td colspan="4">
    <div style="height:100%; overflow:scroll;">
         <asp:DataList ID="DlistQuestions" runat="server" 
              onitemdatabound="DlistQuestions_ItemDataBound">
              <AlternatingItemStyle BackColor="LightGray" CssClass="AlternateRowClass"  />
             <SeparatorStyle BorderColor="Black" BorderStyle="Solid"  Width="100%" />
        <ItemTemplate>
       
       <div id="maincontainer" >
        <table>
        <tr>
        <td style="padding-bottom:10px;padding-top:10px">
            <asp:Label ID="lblSrNo" runat="server" Text='<%#Eval("SrNo") %>'></asp:Label>
             <asp:Label ID="lblQuestionId" runat="server" Text='<%#Eval("QuestionId_FK") %>' Visible="false"></asp:Label>
            <asp:Label ID="lblQuestion" runat="server" Text='<%#Eval("Question") %>'></asp:Label>
        </td>
        </tr>
        <tr>
        <td  >
            <asp:RadioButtonList ID="rblistAnswers" runat="server" RepeatDirection="Vertical">
            </asp:RadioButtonList>
        </td>
        </tr>
        </table>
        </div>
        </ItemTemplate>
        </asp:DataList>
    </div>     
    </td>
    </tr>
    <tr style="height:10%">
    <td>
       <asp:Button ID="btnPrevious" runat="server" Text="Previous"  CssClass="btnLoginClass"
            onclick="btnPrevious_Click" />
    </td>
    <td>
        <asp:Button ID="btnNext" runat="server" Text="Next" CssClass="btnLoginClass" onclick="btnNext_Click" />
    </td>
    <td>
        &nbsp;</td>
    </tr>
    </table>
  
    </div>
</asp:Content>--%>
<asp:Content ID="Content2" ContentPlaceHolderID="EmployeeColumn" runat="server">
    <link href="../Styles/Employee.css" rel="stylesheet" type="text/css" />
    <div class="divClass">
        <asp:Label ID="lblmessage" runat="server" ForeColor="Red"></asp:Label>
        
        <table style="width:100%;">
        <tr>
        <td>
          <asp:Label ID="lblTitle" runat="server" Font-Size="X-Large"
                ForeColor="#333300" Text="Questions"></asp:Label>
        </td>
        <td>
        </td>
        </tr>
            <tr>
                <td colspan="4"  style="width:100%">
                    <asp:DataList ID="DlistQuestions" runat="server" OnItemDataBound="DlistQuestions_ItemDataBound"  >
                        <AlternatingItemStyle BackColor="#CDFECD" ></AlternatingItemStyle>
                        <ItemTemplate>
                            
                                <table  style=" width:100%;" cellspacing="5" 
                                    cellpadding="5">
                                    <tr>
                                    <td>
                                    <div id="maincontainer" class="datalistClass">
                                     <table style=" width:100%;" >
                                     <tr >
                                        <td style="padding-bottom: 10px; padding-top: 10px; width:100%" >
                                            <asp:Label ID="lblSrNo" runat="server" Text='<%#Eval("SrNo") %>'></asp:Label>
                                            <asp:Label ID="lblQuestionId" runat="server" Text='<%#Eval("QuestionId_FK") %>' Visible="false"></asp:Label>
                                            <asp:Label ID="lblQuestion" runat="server" Text='<%#Eval("Question") %>'></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width:100%">
                                            <asp:RadioButtonList ID="rblistAnswers" runat="server" RepeatDirection="Vertical">
                                            </asp:RadioButtonList>
                                        </td>
                                    </tr>
                                    </table>
                                       </div>
                                        </td>
                                    </tr>
                                   
                                    
                                </table>
                               
                         
                                
                                <%--<br />--%>
                        </ItemTemplate>
                    </asp:DataList>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:ImageButton ID="btnPrevious" runat="server" 
                        ImageUrl="~/images/arrow_left.gif"  onclick="btnPrevious_Click" />
                </td>
                <td>
                    &nbsp;<asp:ImageButton ID="btnNext" runat="server" ImageUrl="~/images/arrow_right.gif" onclick="btnNext_Click" />
                </td>
            </tr>
        </table>
    </div>
</asp:Content>
