﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using BE;
using DAL;

namespace HealToZeal.Employee
{
    public partial class Assessment : System.Web.UI.Page
    {
        AssessmentQuestionRelationDAL objAssessmentQuestionRelationDAL = new AssessmentQuestionRelationDAL();
        EmployeeAssessmentDetailsDAL objEmployeeAssessmentDetailsDAL = new EmployeeAssessmentDetailsDAL();
        EmployeeAssessmentDetailsBE objEmployeeAssessmentDetailsBE = new EmployeeAssessmentDetailsBE();

        string AssessmentId = "";
        string QuestionId = "";
        string EmployeeId = "";
        AnswerMasterDAL objAnswerMasterDAL = new AnswerMasterDAL();
        int position = 0;
        int Default = 10;
       
        DataTable DtSaveAnswer = new DataTable();

        protected void Page_Load(object sender, EventArgs e)
        {
            if(!Page.IsPostBack)
            {
                Session["Pagename"] = null;
                if (Session["EmployeeId"] != null)
                {
                    //assessment id total question count
                    AssessmentId = "AAD38F93-0DC7-4EA1-8A61-6B95816AED6B"; //assessment session
                    EmployeeId = Session["EmployeeId"].ToString();
                    //check whether assessment is submitted or not
                    EmployeeAssessmentsHistoryDAL objEmployeeAssessmentsHistoryDAL = new EmployeeAssessmentsHistoryDAL();
                 DataTable DtAssessmentSubmitted=objEmployeeAssessmentsHistoryDAL.EmployeeAssessmentsHistoryAssessmentSubmited(EmployeeId, AssessmentId);
                 if (DtAssessmentSubmitted != null && DtAssessmentSubmitted.Rows.Count > 0)
                 {
                     Response.Redirect("EmployeeHome.aspx?Assessment=Y");
                 }
                 else
                 {
                     DataTable DtTotalQuestionCount = objEmployeeAssessmentDetailsDAL.EmployeeAssessmentDetailsGetCount(AssessmentId);
                     if (DtTotalQuestionCount != null && DtTotalQuestionCount.Rows.Count > 0)
                     {
                         Session["TotalQuestionCount"] = DtTotalQuestionCount.Rows[0][0].ToString();
                         if (Session["Position"] == null)
                         {
                             btnPrevious.Enabled = false;
                         }
                     }
                     BindTestQuestions();
                     //27 jan 2015
                     BindSavedAnswers();
                     //
                 }                   
                }
                else
                {
                    Response.Redirect("EmployeeLogin.aspx");
                }
            }
        }

        private void BindTestQuestions()
        {
            try
            {
              //  AssessmentId = Session["AssessmentId"].ToString();
                //AssessmentId = "AAD38F93-0DC7-4EA1-8A61-6B95816AED6B";
                if (Session["Position"] != null)
                {
                    position = Convert.ToInt32(Session["Position"].ToString());
                }
               
                DataTable dtAssessmentQuestions = objAssessmentQuestionRelationDAL.AssessmentQuestionsRelationGet(AssessmentId,position);
                DlistQuestions.DataSource = dtAssessmentQuestions;
                DlistQuestions.DataBind();

                //BindAnswers();

                //if (Session["Position"] != null)
                //    Session["Position"] = Convert.ToInt32(Session["Position"].ToString()) + DlistQuestions.Items.Count ;
                //else
                //    Session["Position"] = DlistQuestions.Items.Count;
            }
            catch 
            {
            }
        }

        private void BindAnswers()
        {
            try
            {
                foreach (DataListItem questions in DlistQuestions.Items)
                {
                    QuestionId = ((Label)questions.FindControl("lblQuestionId")).Text;
                    DataTable DtAnswers = objAnswerMasterDAL.AnswerMasterGetWithDescriptionByQuestionId(QuestionId);
                    ((RadioButtonList)questions.FindControl("rblistAnswers")).DataSource = DtAnswers;
                    ((RadioButtonList)questions.FindControl("rblistAnswers")).DataTextField = "Answer";
                    ((RadioButtonList)questions.FindControl("rblistAnswers")).DataValueField = "AnswerId_PK";
                    ((RadioButtonList)questions.FindControl("rblistAnswers")).DataBind();
                }
            }
            catch
            {
                
            }
        }

        protected void btnNext_Click(object sender, EventArgs e)
        {
            try
            {                
                //save answers 
             int status=SaveAnswersSession();
             if (status > 0)
             {
                 if (Session["Position"] != null)
                 {
                     Session["Position"] = Convert.ToInt32(Session["Position"].ToString()) + DlistQuestions.Items.Count;
                     btnPrevious.Enabled = true;
                 }
                 else
                     Session["Position"] = DlistQuestions.Items.Count;
             
                 BindTestQuestions();
                 BindSavedAnswers();

                 int Count = Convert.ToInt32(Session["Position"].ToString()) + DlistQuestions.Items.Count;

                 if (Convert.ToInt32(Session["TotalQuestionCount"].ToString()) == Convert.ToInt32(Session["Position"].ToString()))
                 {
                     Session["TotalQuestionCount"] = null;
                     Session["Position"] = null;
                     //send mail 
                     Response.Redirect("EmployeeHome.aspx?Assessment=submited");
                 }                 
                 else if (Convert.ToInt32(Session["TotalQuestionCount"].ToString()) == Count)
                 {
                   //  btnNext.Text = "Submit";
                 }
                 if (Session["Position"] != null)
                 {
                     btnPrevious.Enabled = true;
                 }
             }              
            }
            catch 
            {
             
            }
        }

        private int SaveAnswersSession()
        {
            try
            {
                int status =0;
                objEmployeeAssessmentDetailsBE.propAssessmentId_FK = "AAD38F93-0DC7-4EA1-8A61-6B95816AED6B"; //AssessmentId 
                EmployeeId = Session["EmployeeId"].ToString();
                objEmployeeAssessmentDetailsBE.propEmployeeId_FK = EmployeeId;
            //    objEmployeeAssessmentDetailsBE.propEmployeeId_FK = "C885389E-8F6D-4B6D-AAC3-57EE93336C25"; // logged in user
                objEmployeeAssessmentDetailsBE.propCreatedBy = EmployeeId;
              //  objEmployeeAssessmentDetailsBE.propCreatedBy = "C885389E-8F6D-4B6D-AAC3-57EE93336C25";
                objEmployeeAssessmentDetailsBE.propCreatedDate = DateTime.Now.Date;

                foreach(DataListItem answerItem in DlistQuestions.Items)
                {
                    QuestionId = ((Label)answerItem.FindControl("lblQuestionId")).Text;
                    //  string AnswerId = ((RadioButtonList)answerItem.FindControl("rblistAnswers")).SelectedItem.Value;
                    string AnswerId = "";
                    RadioButtonList rblistAnswers = (RadioButtonList)answerItem.FindControl("rblistAnswers");
                    foreach (ListItem rdo in rblistAnswers.Items)
                    {
                        if (rdo.Selected)
                        {
                            AnswerId = rdo.Value;
                        }
                    }
                    objEmployeeAssessmentDetailsBE.propAnswerId_FK = AnswerId;
                    objEmployeeAssessmentDetailsBE.propQuestionId_FK = QuestionId;

                  status = objEmployeeAssessmentDetailsDAL.EmployeeAssessmentDetailsInsert(objEmployeeAssessmentDetailsBE);
                    {
                        if (status <= 0)
                        {
                            lblmessage.Text = "Please select answer";
                            break;
                        }
                        else
                        {
                            lblmessage.Text = "";
                        }
                    }
                }
                return status;
            }
            catch 
            {
                return 0;
            }
        }

        protected void btnPrevious_Click(object sender, EventArgs e)
        {
            try
            {
             int Count = Convert.ToInt32(Session["Position"].ToString()) + DlistQuestions.Items.Count;

             if (Convert.ToInt32(Session["TotalQuestionCount"].ToString()) == Count)
                 SaveAnswersSession();

             if (Session["Position"] != null)
             {
                 Session["Position"] = Convert.ToInt32(Session["Position"].ToString()) - Default;
                 if (Convert.ToInt32(Session["Position"].ToString()) == 0)
                 {
                     btnPrevious.Enabled = false;
                 }
                 else 
                 {
                     btnPrevious.Enabled = true ;
                 }
             }
                
                //else
                //    Session["Position"] = DlistQuestions.Items.Count;
                
                BindTestQuestions();
                BindSavedAnswers();
            }
            catch 
            {
            }
        }

        private void BindSavedAnswers()
        {
            try
            {
                EmployeeAssessmentDetailsDAL objEmployeeAssessmentDetailsDAL = new EmployeeAssessmentDetailsDAL();
                DataTable DtEmployeeAnswers = new DataTable();

                AssessmentId = "AAD38F93-0DC7-4EA1-8A61-6B95816AED6B"; //AssessmentId 
                EmployeeId = Session["EmployeeId"].ToString();
                // EmployeeId = "C885389E-8F6D-4B6D-AAC3-57EE93336C25"; // logged in user
                //27 jan 2015
                if (Session["Position"] != null)
                {
                    position = Convert.ToInt32(Session["Position"].ToString());
                }
                DtEmployeeAnswers = objEmployeeAssessmentDetailsDAL.EmployeeAssessmentDetailsGet(AssessmentId, position, EmployeeId);
        
                foreach (DataListItem answerItem in DlistQuestions.Items)
                {
                    QuestionId = ((Label)answerItem.FindControl("lblQuestionId")).Text;
                    //  string AnswerId = ((RadioButtonList)answerItem.FindControl("rblistAnswers")).SelectedItem.Value;
                    DataRow[] DrAnswer= DtEmployeeAnswers.Select("QuestionId_FK='"+QuestionId+"'");
                    if (DrAnswer.Length > 0)
                    {
                        RadioButtonList rblistAnswers = (RadioButtonList)answerItem.FindControl("rblistAnswers");
                        foreach (ListItem rdo in rblistAnswers.Items)
                        {
                           if( DrAnswer[0][1].ToString()==rdo.Value.ToString())
                            {
                                
                                rdo.Selected = true;
                            }
                        }    
                    }                                
                }
            }
            catch 
            {

            }
        }

        protected void DlistQuestions_ItemDataBound(object sender, DataListItemEventArgs e)
        {
            try
            {
               RadioButtonList rblistAnswers=(RadioButtonList)e.Item.FindControl("rblistAnswers");
               Label lblQuestionId = (Label)e.Item.FindControl("lblQuestionId");

               QuestionId = lblQuestionId.Text;
               DataTable DtAnswers = objAnswerMasterDAL.AnswerMasterGetWithDescriptionByQuestionId(QuestionId);
               //DataTable DtAnswers = objAnswerMasterDAL.AnswerMasterGetByQuestionId(QuestionId);
               rblistAnswers.DataSource = DtAnswers;
               rblistAnswers.DataTextField = "Answer";
               rblistAnswers.DataValueField = "AnswerId_PK";
               rblistAnswers.DataBind();
            }
            catch 
            {

            }
        }

   

    }
}