﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DAL;
using BE;
using System.Data;
namespace HealToZeal.Employee
{
    public partial class EmployeeProfile : System.Web.UI.Page
    {
        string employeeId = "";

        EmployeeDAL objEmployeeDAL = new EmployeeDAL();
        CountryDAL objCountryDAL = new CountryDAL();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                if (Session["EmployeeId"] != null)
                {
                    BindCountry();
                    BindEmployee();
                }
                else
                {
                    Response.Redirect("EmployeeLogin.aspx");
                }
            }
        }

        private void BindCountry()
        {
            try
            {
                DataTable DtCountry = objCountryDAL.CountryMasterGet();
                if (DtCountry != null && DtCountry.Rows.Count > 0)
                {
                    ddlCountry.DataSource = DtCountry;
                    ddlCountry.DataTextField = "CountryName";
                    ddlCountry.DataValueField = "CountryId_PK";
                    ddlCountry.DataBind();

                    ddlCountry.Items.Insert(0, new ListItem("--Select country--", "--Select country--"));
                    ddlCountry.SelectedIndex = 0;
                }
            }
            catch 
            {
                
            }
        }     

        private void BindEmployee()
        {
            try
            {
                employeeId = Session["EmployeeId"].ToString();
                DataTable dtEmployee= objEmployeeDAL.EmployeesGetByEmployeeId(employeeId);
                    if(dtEmployee!=null && dtEmployee.Rows.Count>0)
                    {
                        txtUserName.Text= dtEmployee.Rows[0]["UserName"].ToString();
                        txtFirstName.Text = dtEmployee.Rows[0]["FirstName"].ToString();
                        txtLastName.Text = dtEmployee.Rows[0]["LastName"].ToString();
                        txtEmailId.Text = dtEmployee.Rows[0]["EmailId"].ToString();
                        if (dtEmployee.Rows[0]["ExperienceInYears"] != null)
                        {
                            ddlExperience.SelectedValue = dtEmployee.Rows[0]["ExperienceInYears"].ToString();
                        }
                        if (dtEmployee.Rows[0]["ZipCode"] != null)
                        {
                            txtZipcode.Text = dtEmployee.Rows[0]["ZipCode"].ToString();
                        }
                        if (dtEmployee.Rows[0]["EmployeeAddress"]!=null)                       
                        txtAddress.Text = dtEmployee.Rows[0]["EmployeeAddress"].ToString();
                        if (dtEmployee.Rows[0]["MobileNo"]!=null)
                        txtMobileNo.Text = dtEmployee.Rows[0]["MobileNo"].ToString();
                        if (dtEmployee.Rows[0]["ContactNo"]!=null )
                        txtContactNo.Text = dtEmployee.Rows[0]["ContactNo"].ToString();
                        if (dtEmployee.Rows[0]["Country"] != null)
                        {
                            {
                                if (dtEmployee.Rows[0]["Country"].ToString()!="")
                                {
                                    ddlCountry.SelectedValue = dtEmployee.Rows[0]["Country"].ToString();
                                    BindCity(Convert.ToInt32(ddlCountry.SelectedValue)); 
                                }
                            }                            
                        }
                        if (dtEmployee.Rows[0]["City"] != null)
                        {
                            if (dtEmployee.Rows[0]["City"].ToString() != "")
                                ddlCity.SelectedValue = dtEmployee.Rows[0]["City"].ToString();
                        }
                        txtOccupation.Text = dtEmployee.Rows[0]["Occupation"].ToString();
                        ddlEducation.SelectedValue = dtEmployee.Rows[0]["Qualification"].ToString();
                        ddlPosition.SelectedValue = dtEmployee.Rows[0]["Position"].ToString();
                        if (dtEmployee.Rows[0]["Age"].ToString() != "")
                        {
                            txtBirthDate.Text =Convert.ToDateTime(dtEmployee.Rows[0]["Age"].ToString()).ToShortDateString();
                        }                       
                        if (dtEmployee.Rows[0]["Gender"].ToString() != "")
                        {
                            rblGender.SelectedValue = dtEmployee.Rows[0]["Gender"].ToString();                        
                        }
                    }
            }
            catch 
            {

            }
        }

        private void BindCity(int CountryId)
        {
            try
            {
                DataTable DtCity = objCountryDAL.CityMasterGetByCountryId(CountryId);
                ddlCity.DataSource = DtCity;
                ddlCity.DataValueField = "CityId_PK";
                ddlCity.DataTextField = "CityName";
                ddlCity.DataBind();

                ddlCity.Items.Insert(0, new ListItem("--Select city--", "--Select city--"));
                ddlCity.SelectedIndex = 0;
            }
            catch 
            {

            }
        }

        protected void ddlCountry_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlCountry.SelectedIndex != 0)
            {
                int CountryId = Convert.ToInt32(ddlCountry.SelectedItem.Value);
                BindCity(CountryId);
            
            }
        }

        protected void btnEditProfile_Click(object sender, EventArgs e)
        {
            try
            {
                if (Session["EmployeeId"] != null)
                {
                    employeeId=Session["EmployeeId"].ToString();

                    EmployeeBE objEmployeeBE = new EmployeeBE();

                    objEmployeeBE.propEmployeeId_PK = employeeId;
                    objEmployeeBE.propFirstName = txtFirstName.Text;
                    objEmployeeBE.propLastName = txtLastName.Text;
                    objEmployeeBE.propMobileNo = txtMobileNo.Text;
                    objEmployeeBE.propExperienceInYears = ddlExperience.SelectedItem.Value;
                    objEmployeeBE.propEmailId = txtEmailId.Text;
                    objEmployeeBE.propContactNo = txtContactNo.Text;
                    objEmployeeBE.propCountry =Convert.ToInt32( ddlCountry.SelectedItem.Value);
                    objEmployeeBE.propCity = Convert.ToInt32(ddlCity.SelectedItem.Value);
                    objEmployeeBE.propUserName=txtUserName.Text;
                    objEmployeeBE.propEmployeeAddress=txtAddress.Text;
                    objEmployeeBE.propZipCode=txtZipcode.Text;
                    objEmployeeBE.propUdpatedBy=employeeId;
                    objEmployeeBE.propUpdatedDate=DateTime.Now.Date;
                    objEmployeeBE.propAge =Convert.ToDateTime(txtBirthDate.Text);
                    objEmployeeBE.propGender = rblGender.SelectedItem.Value;
                    objEmployeeBE.propOccupation = txtOccupation.Text;
                    objEmployeeBE.propPosition = ddlPosition.SelectedItem.Value;
                    objEmployeeBE.propQualification = ddlEducation.SelectedItem.Value;

                    int status= objEmployeeDAL.EmployeesUpdate(objEmployeeBE);
                    string message = "";

                    if(status>0)
                    {
                      ClearControls();
                      Response.Redirect("EmployeeHome.aspx?Profile=1");
                      message = "Your profile updated successfully...";
                      
                    }
                    else
                    {
                        message = "Can not update profile...";
                    }
                }
            }
            catch 
            {
                
            }
        }

        private void ClearControls()
        {
            try
            {
                txtFirstName.Text = "";
                txtLastName.Text = "";
                txtAddress.Text = "";
                txtContactNo.Text = "";
                txtEmailId.Text = "";
                txtMobileNo.Text = "";
                txtUserName.Text = "";
                txtZipcode.Text = "";
                txtBirthDate.Text = "";
                txtOccupation.Text = "";
                ddlCity.SelectedIndex = 0;
                ddlCountry.SelectedIndex = 0;
                ddlEducation.SelectedIndex = 0;
                ddlExperience.SelectedIndex=0;
                ddlPosition.SelectedIndex = 0;
            }
            catch
            {

            }
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            try
            {
                ClearControls();
                Response.Redirect("Home.aspx",false );
            }
            catch 
            {
                
            }
        }
    }
}