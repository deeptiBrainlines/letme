﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Employee/Employee.Master" AutoEventWireup="true" CodeBehind="EmployeeHome.aspx.cs" Inherits="HealToZeal.Employee.EmployeeHome" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server" >
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="EmployeeColumn" runat="server">
    
    
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js"></script>
   
    
    <!-- Custom Stylesheet -->
    <link href="../Styles/style.css" rel="stylesheet" type="text/css">
      
    
    <!-- Slider Stylesheet -->
    <link href="../Styles/slider.css" rel="stylesheet" type="text/css">
    <script src="../Scripts/jquery.colorbox-min.js" type="text/javascript"></script>   
    <script src="../Scripts/jquery.colorbox.js" type="text/javascript"></script>
   <%-- <link href="../Styles/ColorBox.css" rel="stylesheet" type="text/css" />--%>
    <script type="text/javascript">
//        $(document).bind('cbox_closed', function () {
//        });
        function DisplayColorBox(url) {

            $.fn.colorbox({ href: url, iframe: true, scrolling: true, width: '900px', position: 'Fixed', height: '660px', overlayClose: false });
            //, onClosed: function () { parent.location.reload(false); }
            return false;
            //$(cntrl).colorbox({      
            //        iframe: true,
            //        opacity:1,
            //        href: '~/Pathway.aspx',
            //        width: '1100px',
            //        height: '600px',
            //        overlayClose: false   ,
            //        border: '10px',
            //         margin: '200px',
            //         inline:'true',
            //         transition:'elastic',
            //});
        }

        function DisplayColorBox1(url) {

            $.fn.colorbox({ href: url, iframe: true, scrolling: true, width: '600px', position: 'Fixed', height: '450px', overlayClose: false });
            //, onClosed: function () { parent.location.reload(false); }
            return false;
            //$(cntrl).colorbox({      
            //        iframe: true,
            //        opacity:1,
            //        href: '~/Pathway.aspx',
            //        width: '1100px',
            //        height: '600px',
            //        overlayClose: false   ,
            //        border: '10px',
            //         margin: '200px',
            //         inline:'true',
            //         transition:'elastic',
            //});
        }
        </script>
     <asp:Label ID="lblMessage" runat="server" ForeColor="#333300" Font-Size="Large"></asp:Label>
        <div class="divClass" style="width:80%; margin-right:0;margin-top:0">       
       
        <asp:Panel ID="pnlEmployeeBasicDetails" runat="server" Visible="False">
        <table>
        <tr>
        <td></td>
        <td colspan="2">
            <asp:Label ID="lblTitle" runat="server" Font-Size="X-Large"
                ForeColor="#333300" Text="Profile"></asp:Label>
            </td>
      
        </tr>
        <tr>
              <td style="text-align: right">
                    <asp:Label ID="lblBirthDate" runat="server" Text="Birth date"></asp:Label>
                </td>
                <td>
                    <asp:TextBox ID="txtBirthDate" runat="server" CssClass="txtLoginClass" 
                        ValidationGroup="0"></asp:TextBox>
                  
                    <cc1:CalendarExtender ID="txtBirthDate_CalendarExtender" runat="server" 
                        Enabled="True" TargetControlID="txtBirthDate">
                    </cc1:CalendarExtender>
                  
                </td>
                <td>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" 
                        ControlToValidate="txtBirthDate" ErrorMessage="Select birthdate." 
                        ForeColor="Red" ValidationGroup="0"></asp:RequiredFieldValidator>
              </td>
              
        </tr>
        <tr>
             <td style="text-align: right">
                    <asp:Label ID="lblGender" runat="server" Text="Gender"></asp:Label>
                </td>
                <td>
                    
                    <asp:RadioButtonList ID="rblGender" runat="server" RepeatDirection="Horizontal" 
                        ValidationGroup="0">
                        <asp:ListItem Value="M">Male</asp:ListItem>
                        <asp:ListItem Value="F">Female</asp:ListItem>
                    </asp:RadioButtonList>
                    
                </td>
                <td>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" 
                        ControlToValidate="rblGender" ErrorMessage="Select gender." ForeColor="Red" 
                        ValidationGroup="0"></asp:RequiredFieldValidator>
             </td>
        </tr>
        <tr>
            <td style="text-align: right">
                    
                    <asp:Label ID="lblEducation" runat="server" Text="Highest Qualification"></asp:Label>
                </td>
                <td>
                    
                    <asp:DropDownList ID="ddlEducation" runat="server"  CssClass="txtLoginClass" 
                        ValidationGroup="0">
                        <asp:ListItem>--Select--</asp:ListItem>
                        <asp:ListItem>PostGraduateDiploma</asp:ListItem>
                        <asp:ListItem>Graduate</asp:ListItem>
                        <asp:ListItem>Doctorate</asp:ListItem>
                    </asp:DropDownList>
                    
                </td>
                <td>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" 
                        ControlToValidate="ddlEducation" ErrorMessage="Select highest qualification." 
                        ForeColor="Red" InitialValue="--Select--" ValidationGroup="0"></asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr>
          <td style="text-align: right">
                    
                    <asp:Label ID="lblOccupation" runat="server" Text="Occupation"></asp:Label>
                </td>
                <td>
                    
                    <asp:TextBox ID="txtOccupation" runat="server"  CssClass="txtLoginClass" 
                        ValidationGroup="0"></asp:TextBox>
                    
                </td>
                <td>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" 
                        ControlToValidate="txtOccupation" ErrorMessage="Enter occupation." 
                        ForeColor="Red" ValidationGroup="0"></asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr>
             <td style="text-align: right">
                    <asp:Label ID="lblExperience" runat="server" Text="Experience"></asp:Label>
                </td>
                <td>
                    <asp:DropDownList ID="ddlExperience" runat="server"  CssClass="txtLoginClass" 
                        ValidationGroup="0">
                        <asp:ListItem>--Select--</asp:ListItem>
                        <asp:ListItem>0-3 years</asp:ListItem>
                        <asp:ListItem>3-7years</asp:ListItem>
                        <asp:ListItem>10-15years</asp:ListItem>
                        <asp:ListItem>15 and above</asp:ListItem>
                    </asp:DropDownList>
                </td>
                <td>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" 
                        ControlToValidate="ddlExperience" ErrorMessage="Select experience." 
                        ForeColor="Red" InitialValue="--Select--" ValidationGroup="0"></asp:RequiredFieldValidator>
             </td>
        </tr>
        <tr>
             <td style="text-align: right">
                    <asp:Label ID="lblPosition" runat="server" Text="Position"></asp:Label>
                </td>
                <td>
                    <asp:DropDownList ID="ddlPosition" runat="server"  CssClass="txtLoginClass" 
                        ValidationGroup="0">
                        <asp:ListItem>--Select--</asp:ListItem>
                        <asp:ListItem>Junior level</asp:ListItem>
                        <asp:ListItem>middle management</asp:ListItem>
                        <asp:ListItem>Senior management</asp:ListItem>
                    </asp:DropDownList>
                </td>
                <td>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" 
                        ControlToValidate="ddlPosition" ErrorMessage="Select position." ForeColor="Red" 
                        InitialValue="--Select--" ValidationGroup="0"></asp:RequiredFieldValidator>
             </td>
        </tr>
        <tr>
          <td></td>
         <td>
             <asp:Button ID="btnSubmit" runat="server" Text="Submit" CssClass="btnLoginClass" BackColor="#CDFECD"
                 onclick="btnSubmit_Click" ValidationGroup="0" />
            </td>
        <td></td>
        </tr>
            <tr>
                <td>
                    &nbsp;</td>
                <td>
                    &nbsp;</td>
                <td>
                    &nbsp;</td>
            </tr>
        </table>
        </asp:Panel>
    </div>
  
   
       <div class="web_page_body" id="slidediv" runat="server">
            <div id="da-slider" class="da-slider hidden-xs">
                <div class="da-slide">
                    <h2>Mental Health</h2>                   
                    <!--<a href="#" class="da-link">Read more</a>-->
                    <div class="da-img"><img src="../images/psycologyimg.jpg" alt="image01" /></div>
                </div>
                <div class="da-slide">
                    <h2>Plan</h2>                   
                    <!--<a href="#" class="da-link">Read more</a>-->
                    <div class="da-img"><img src="../images/psycologyimg1.jpg" alt="image01" /></div>
                </div>
                <div class="da-slide">                  
                    <!--<a href="#" class="da-link">Read more</a>-->
                    <div class="da-img"><img src="../images/psycologyimg2.jpg" alt="image01" /></div>
                </div>
                <div class="da-slide">                  
                    <!--<a href="#" class="da-link">Read more</a>-->
                    <div class="da-img"><img src="../images/psycologyimg4.jpg" alt="image01" /></div>
                </div>
                <nav class="da-arrows">
                    <span class="da-arrows-prev"></span>
                    <span class="da-arrows-next"></span>
                </nav>
            </div>
            
    	
    </div>
        
   
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
   
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    
    
    <!-- Slider Start -->
    <script type="text/javascript" src="../Scripts/modernizr.custom.28468.js"></script>
	<script type="text/javascript" src="../Scripts/jquery.cslider.js"></script>
	<script type="text/javascript">
	    $(function () {

	        $('#da-slider').cslider({
	            autoplay: true,
	            bgincrement: 450
	        });

	    });
    </script>
    <!-- Slider end -->
</asp:Content>
