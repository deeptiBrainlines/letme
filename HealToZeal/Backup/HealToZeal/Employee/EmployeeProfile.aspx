﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Employee/Employee.Master" AutoEventWireup="true" CodeBehind="EmployeeProfile.aspx.cs" Inherits="HealToZeal.Employee.EmployeeProfile" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

    <style type="text/css">
        .style1
        {
            height: 22px;
        }
        .style2
        {
            height: 21px;
        }
    </style>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="EmployeeColumn" runat="server">
    <link href="../Styles/Employee.css" rel="stylesheet" type="text/css" />
     <div class="divProfileClass">
           <table >
             <tr>
                <td>
                    &nbsp;</td>
                <td>
                </td>
              
                <td></td>
                <td></td>
               <td></td>
            </tr>
              <tr>
                <td colspan="5" align="center" valign="top">
                    
                  <asp:Label ID="lblTitle" runat="server" Text="Update profile" 
                        CssClass="HealToZeal" Width="265px" ForeColor="#333300"></asp:Label>
                  </td>
            </tr>
              <tr>
                <td colspan="5" align="center" valign="top">
                    
                    &nbsp;</td>
            </tr>
            <tr>
                <td  align="right">
                    <asp:Label ID="lblFirstName" runat="server" Text="First name"></asp:Label>
                </td>
                <td>
                    <asp:TextBox ID="txtFirstName" runat="server" CssClass="txtLoginClass" 
                        ValidationGroup="0"></asp:TextBox>
                </td>
               
                <td style="text-align: right">
                    <asp:Label ID="lblUserId" runat="server" Text="User Id"></asp:Label>
                </td>
                <td>
                    <asp:TextBox ID="txtUserName" runat="server"  CssClass="txtLoginClass" 
                        ValidationGroup="0"></asp:TextBox>
                </td>
                                <td>
                                    &nbsp;</td>
            </tr>  
            <tr>
                <td>
                    &nbsp;</td>
                <td>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" 
                                        ControlToValidate="txtFirstName" ErrorMessage="Enter first name." 
                                        ForeColor="Red" ValidationGroup="0"></asp:RequiredFieldValidator>
                </td>
             
                <td>
                    &nbsp;</td>
                <td>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" 
                        ControlToValidate="txtUserName" ErrorMessage="Enter User Id." ForeColor="Red" 
                        ValidationGroup="0"></asp:RequiredFieldValidator>
                </td>
                <td>
                                    &nbsp;</td>
            </tr>
            <tr>
                <td  align="right">
                    <asp:Label ID="lblLastName" runat="server" Text="Last name"></asp:Label>
                </td>
                <td>
                    <asp:TextBox ID="txtLastName" runat="server"  CssClass="txtLoginClass" 
                        ValidationGroup="0"></asp:TextBox>
                </td>                
              
                <td style="text-align: right">
                    <asp:Label ID="lblEmailId" runat="server" Text="Email Id"></asp:Label>
                </td>
                <td>
                    <asp:TextBox ID="txtEmailId" runat="server"  CssClass="txtLoginClass" 
                        ValidationGroup="0"></asp:TextBox>
                </td>
                <td>&nbsp;</td>
            </tr>
            <tr>
            <td></td>
            <td>
                             <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" 
                                 ErrorMessage="Enter last name." ForeColor="Red" ValidationGroup="0" 
                                 ControlToValidate="txtLastName"></asp:RequiredFieldValidator>
                </td>
            
            <td></td>
            <td>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" 
                                        ControlToValidate="txtEmailId" ErrorMessage="Enter Email id." ForeColor="Red" 
                                        ValidationGroup="0"></asp:RequiredFieldValidator>
                                    <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" 
                                        ControlToValidate="txtEmailId" ErrorMessage="Enter proper email id." 
                                        ForeColor="Red" 
                                        ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" 
                                        ValidationGroup="0"></asp:RegularExpressionValidator>
                </td>
            <td></td>
            </tr>
            <tr>
                <td align="right">
                    <asp:Label ID="lblCountry" runat="server" Text="Country"></asp:Label>
                </td>
                <td>
                   
                    <asp:DropDownList ID="ddlCountry" runat="server" 
                        onselectedindexchanged="ddlCountry_SelectedIndexChanged" 
                        AutoPostBack="True" CssClass="txtLoginClass" ValidationGroup="0">
                    </asp:DropDownList>
                   
                </td>                
                
                <td style="text-align: right">
                    <asp:Label ID="lblBirthDate" runat="server" Text="Birth date"></asp:Label>
                </td>
                <td>
                    <asp:TextBox ID="txtBirthDate" runat="server"  CssClass="txtLoginClass"></asp:TextBox>
                    <cc1:CalendarExtender ID="txtBirthDate_CalendarExtender" runat="server" 
                        Enabled="True" TargetControlID="txtBirthDate">
                    </cc1:CalendarExtender>
                </td>
                <td></td>
            </tr>
              <tr>
            <td></td>
            <td>
                         <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" 
                             ControlToValidate="ddlCountry" ErrorMessage="Select country." ForeColor="Red" 
                             InitialValue="--Select country--" ValidationGroup="0"></asp:RequiredFieldValidator>
                     </td>
            <td></td>
            <td></td>
            <td></td>
           
            </tr>
            <tr>
                <td  align="right">
                    <asp:Label ID="lblCity" runat="server" Text="City"></asp:Label>
                </td>
                <td>
                   
                    <asp:DropDownList ID="ddlCity" runat="server" CssClass="txtLoginClass">
                    </asp:DropDownList>
                   
                </td>                
               
                <td style="text-align: right">
                    <asp:Label ID="lblGender" runat="server" Text="Gender"></asp:Label>
                </td>
                <td>
                    
                    <asp:RadioButtonList ID="rblGender" runat="server" RepeatDirection="Horizontal">
                        <asp:ListItem Value="M">Male</asp:ListItem>
                        <asp:ListItem Value="F">Female</asp:ListItem>
                    </asp:RadioButtonList>
                    
                </td>
                <td></td>
            </tr>
              <tr>
            <td></td>
            <td>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" 
                            ControlToValidate="ddlCity" ErrorMessage="Select city." ForeColor="Red" 
                            InitialValue="--Select city--" ValidationGroup="0"></asp:RequiredFieldValidator>
                    </td>
            
            <td></td>
            <td></td>
            <td></td>
            </tr>
            <tr>
                <td  align="right">
                    <asp:Label ID="lblAddress" runat="server" Text="Address"></asp:Label>
                </td>
                <td>
                   
                    <asp:TextBox ID="txtAddress" runat="server"  CssClass="txtLoginClass"></asp:TextBox>
                </td>                
               
                <td  align="right">
                    
                    <asp:Label ID="lblEducation" runat="server" Text="Highest Qualification"  ></asp:Label>
                </td>
                <td>
                    
                    <asp:DropDownList ID="ddlEducation" runat="server" CssClass="txtLoginClass">
                        <asp:ListItem>--Select--</asp:ListItem>
                        <asp:ListItem>PostGraduate</asp:ListItem>
                        <asp:ListItem>Diploma</asp:ListItem>
                        <asp:ListItem>Graduate</asp:ListItem>
                        <asp:ListItem>Doctorate</asp:ListItem>
                    </asp:DropDownList>
                    
                </td>
                <td>&nbsp;</td>
            </tr>
              <tr>
            <td></td>
            <td></td>
            
            <td></td>
            <td></td>
            <td></td>
            </tr>
            <tr>
                <td  align="right">
                    <asp:Label ID="lblZipcode" runat="server" Text="Zip code"></asp:Label>
                </td>
                <td>
                   
                    <asp:TextBox ID="txtZipcode" runat="server" CssClass="txtLoginClass"></asp:TextBox>
                </td>
              
                <td style="text-align: right">
                    
                    <asp:Label ID="lblOccupation" runat="server" Text="Occupation"></asp:Label>
                </td>
                <td>
                    
                    <asp:TextBox ID="txtOccupation" runat="server" CssClass="txtLoginClass"></asp:TextBox>
                    
                </td>
                <td>&nbsp;</td>
            </tr>
              <tr>
            <td></td>
            <td></td>
           
            <td></td>
            <td></td>
            <td></td>
            </tr>
            <tr>
                <td  align="right">
                    <asp:Label ID="lblContactNo" runat="server" Text="Contact no"></asp:Label>
                </td>
                <td>
                    
                    <asp:TextBox ID="txtContactNo" runat="server" CssClass="txtLoginClass" 
                        ValidationGroup="0"></asp:TextBox>
                    
                </td>
                
                <td style="text-align: right">
                    <asp:Label ID="lblExperience" runat="server" Text="Experience"></asp:Label>
                </td>
                <td>
                    <asp:DropDownList ID="ddlExperience" runat="server" CssClass="txtLoginClass">
                        <asp:ListItem>--Select--</asp:ListItem>
                        <asp:ListItem>0-3 years</asp:ListItem>
                        <asp:ListItem>3-7 years</asp:ListItem>
                        <asp:ListItem>10-15 years</asp:ListItem>
                        <asp:ListItem>15 and above</asp:ListItem>
                    </asp:DropDownList>
                </td>
                <td></td>
            </tr>
              <tr>
            <td></td>
            <td>&nbsp;</td>
            <td></td>
            <td></td>
            <td></td>
            </tr>
            <tr>
                <td  align="right">
                    <asp:Label ID="lblMobileNo" runat="server" Text="Mobile no"></asp:Label>
                </td>
                <td>
                    
                    <asp:TextBox ID="txtMobileNo" runat="server" CssClass="txtLoginClass" 
                        ValidationGroup="0"></asp:TextBox>
                    
                </td>
               
                <td style="text-align: right">
                    <asp:Label ID="lblPosition" runat="server" Text="Position"></asp:Label>
                </td>
                <td>
                    <asp:DropDownList ID="ddlPosition" runat="server" CssClass="txtLoginClass">
                        <asp:ListItem>--Select--</asp:ListItem>
                        <asp:ListItem>Junior level</asp:ListItem>
                        <asp:ListItem>middle management</asp:ListItem>
                        <asp:ListItem>Senior management</asp:ListItem>
                    </asp:DropDownList>
                </td>
                <td></td>
            </tr>
              <tr>
            <td class="style1"></td>
            <td class="style1">
                <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" 
                    ControlToValidate="txtMobileNo" ErrorMessage="Enter Mobile no properly." 
                    ForeColor="Red" ValidationExpression="[0-9]{10}" ValidationGroup="0"></asp:RegularExpressionValidator>
                  </td>
           
            <td class="style1"></td>
            <td class="style1"></td>
            <td class="style1"></td>
            </tr>
            <tr>
                <td>
                    </td>
                <td >
                    </td>
                <td colspan="2">
                  <asp:Button ID="btnEditProfile" runat="server" CssClass="btnLoginClass"  
                        ForeColor="Black" Text="Submit" 
                        onclick="btnEditProfile_Click" BackColor="#CDFECD" Font-Bold="True" 
                        Font-Names="Calibri" Font-Size="Large" ValidationGroup="0" />
                    <asp:Button ID="btnCancel" runat="server" CssClass="btnLoginClass"  
                        ForeColor="Black" Text="Cancel" BackColor="#CDFECD" Font-Bold="True" 
                        Font-Names="Calibri" Font-Size="Large" onclick="btnCancel_Click" />
                </td>
                <td >
                    &nbsp;</td>
              
            </tr>
              <tr>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
          
            </tr>
        </table>
    
    </div>
</asp:Content>
