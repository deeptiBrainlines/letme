﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="EmployeeLogin.aspx.cs" Inherits="HealToZeal.Employee.EmployeeLogin" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <link href="../Styles/Employee.css" rel="stylesheet" type="text/css" />
    <title></title>
   
    <style type="text/css">
        .style1
        {
            height: 22px;
        }
    </style>
   
</head>
<body>

  <%--  <form id="form1" runat="server">
     <div class="EmployeeLogin">
    <table>
    <tr>
    <td class="style1"></td>
    <td class="style1">
    <h4>Employee login</h4>
    </td>
    <td class="style1"></td>
    </tr>
     <tr>
    <td>
        <asp:Label ID="lblUserName" runat="server" Text="Username" ></asp:Label>
    </td>
    <td>
        <asp:TextBox ID="txtUserName" runat="server" CssClass="txtLoginClass" 
            ValidationGroup="0" ></asp:TextBox>
    </td>
    <td></td>
    </tr>
    <tr>
    <td></td>
    <td>
        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" 
            ControlToValidate="txtUserName" ErrorMessage="Enter username." ForeColor="Red" 
            ValidationGroup="0"></asp:RequiredFieldValidator>
        </td>
    </tr>
      <tr>
    <td>
        <asp:Label ID="lblPassword" runat="server" Text="Password"></asp:Label>
    </td>
    <td>
        <asp:TextBox ID="txtPassword" runat="server" TextMode="Password" 
            CssClass="txtLoginClass" ValidationGroup="0"></asp:TextBox>
    </td>
    <td></td>
    </tr>
      <tr>
    <td></td>
    <td>
        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" 
            ControlToValidate="txtPassword" ErrorMessage="Enter password." ForeColor="Red" 
            ValidationGroup="0"></asp:RequiredFieldValidator>
          </td>
    </tr>
     <tr>
    <td></td>
    <td>
        <asp:Button ID="btnLogin" runat="server" Text="Login" CssClass="btnLoginClass"
                    onclick="btnLogin_Click" ValidationGroup="0" /></td>
    <td></td>
    </tr>
    <tr>
    <td></td>
    <td>
        <asp:Label ID="lblMessage" runat="server" ForeColor="Red"></asp:Label>
        </td>
    <td></td>
    </tr>
    </table>
    </div>
    </form>--%>

    <form id="form1" runat="server">
    <div  class="EmployeeLogin" style="background-color: #CDE7FD;">
    <table width="100%" >
    <tr>
    <td style="width:25%" ></td>
    <td style="width:75%">
    <h3 style="font-size:x-large; font-family:Calibri; color:#333300;">Employee login</h3>
    </td>
    </tr>
     <tr>
    <td>
        <asp:Label ID="lblUserName" runat="server" Text="Username" ></asp:Label>
    </td>
    <td>
        <asp:TextBox ID="txtUserName" runat="server"  CssClass="txtLoginClass" ></asp:TextBox>
    </td>
    </tr>
       <tr>
    <td></td>
    <td>
        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" 
            ControlToValidate="txtUserName" ErrorMessage="Enter username." ForeColor="Red" 
            ValidationGroup="0"></asp:RequiredFieldValidator>
        </td>
    </tr>
      <tr>
    <td>
        <asp:Label ID="lblPassword" runat="server" Text="Password"></asp:Label>
    </td>
    <td>
        <asp:TextBox ID="txtPassword" runat="server" TextMode="Password" CssClass="txtLoginClass"></asp:TextBox>
    </td>
    </tr>
      <tr>
    <td></td>
    <td>
        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" 
            ControlToValidate="txtPassword" ErrorMessage="Enter password." ForeColor="Red" 
            ValidationGroup="0"></asp:RequiredFieldValidator>
          </td>
    </tr>
     <tr>
    <td></td>
    <td>
    <asp:ImageButton ID="ImgBtnLogin" runat="server" ImageUrl="~/images/loginimg.jpg"
            Height="40px" onclick="ImgBtnLogin_Click" Width="134px" />
    </td>
    </tr>
    <tr>
    <td></td>
    <td>
        <asp:Label ID="lblMessage" runat="server" Font-Italic="True" ForeColor="Red" 
           ></asp:Label>
        </td>
    </tr>
    </table>
    </div>
    </form>
</body>
</html>
