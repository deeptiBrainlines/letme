﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using DAL;
using BE;
using System.Net.Mail;
using System.Net.Mime;

namespace HealToZeal.Employee
{
    public partial class EmployeeHome : System.Web.UI.Page
    {
        string EmployeeId = "";
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!Page.IsPostBack)
                {
                    Session["Pagename"] = "EmployeeHome.aspx";

                    if (Session["EmployeeId"] == null)
                    {
                        Response.Redirect("EmployeeLogin.aspx",false);
                    }
                    else
                    {

                        if (Request.QueryString["first"] != null)
                        {
                            EmployeeDAL objEmployeeDAL = new EmployeeDAL();
                            EmployeeId = Session["EmployeeId"].ToString();
                            DataTable dtEmployee = objEmployeeDAL.EmployeesGetByEmployeeId(EmployeeId);

                            if (dtEmployee != null && dtEmployee.Rows.Count > 0)
                            {
                                if (dtEmployee.Rows[0]["Age"].ToString() != "")
                                {
                                    pnlEmployeeBasicDetails.Visible = false;
                                    slidediv.Visible = true;
                                    Response.Redirect("EmployeeHome.aspx",false);
                                    ScriptManager.RegisterStartupScript(this, this.GetType(), "starScript", "DisplayColorBox('EmployeeMessage.aspx');", true);  
                                }
                                else
                                {
                                    pnlEmployeeBasicDetails.Visible = true;
                                    slidediv.Visible = false;
                                    ((System.Web.UI.HtmlControls.HtmlAnchor)Page.Master.FindControl("A1")).HRef = "EmployeeHome.aspx?first=1";
                                    //  ((System.Web.UI.HtmlControls.HtmlAnchor)Page.Master.FindControl("A2")).HRef = "EmployeeHome.aspx?first=1";
                                    ((System.Web.UI.HtmlControls.HtmlAnchor)Page.Master.FindControl("LnkStart")).HRef = "EmployeeHome.aspx?first=1";
                                    ((System.Web.UI.HtmlControls.HtmlAnchor)Page.Master.FindControl("LnkFeedback")).HRef = "EmployeeHome.aspx?first=1";
                                    // ClientScript.RegisterStartupScript(this.GetType(), "openColorBox", "DisplayColorBox();", true);
                                    ScriptManager.RegisterStartupScript(this, this.GetType(), "starScript", "DisplayColorBox('EmployeeMessage.aspx');", true);  
                                 //   lblMessage.Text = "On the first time login, you are being asked to fill in some information about yourself. You will not be asked for identity anytime during this exercise. This exercise is to validate some of our assumptions and thought process during the product development and not to diagnose or comment on any of the individual answers or scores.  Any personal information will not be shared with anyone, anytime.You can trust us for the same and be rest assured."+"\n"+"Once you fill in this small form, you will be presented with a series of questions and answer options and you will be required to answer all of them. You might find some repetition of the questions but it is deliberate and needed to get the right results out of this important exercise."+"\n"+" If you like, you can logout at any time and come back to answer remaining the questions. But your assessment will not be marked “submitted”, until you complete the same. So please make sure you complete the assessment and click the “Submit” button."+"\n"+" There would be a small feedback form at the end and that’s it!"+"\n"+"We will definitely keep in touch and will let you know how this experiment goes, how it helps us in determining our further strategy!"+"\n";
                                }
                            }               //                     


                        }
                        else if (Request.QueryString["Assessment"] != null)
                        {
                            string Assessment = Request.QueryString["Assessment"].ToString();
                            if (Assessment == "Y")
                            {
                                lblMessage.Text = "You already have submitted assessment...";
                            }
                            else
                            {
                                EmployeeId = Session["EmployeeId"].ToString();

                                EmployeeAssessmentsHistoryBE objEmployeeAssessmentsHistoryBE = new EmployeeAssessmentsHistoryBE();
                                EmployeeAssessmentsHistoryDAL objEmployeeAssessmentsHistoryDAL = new EmployeeAssessmentsHistoryDAL();

                                objEmployeeAssessmentsHistoryBE.propEmployeeId_FK = EmployeeId;
                                //objEmployeeAssessmentsHistoryBE.propAssessmentId_FK=
                                objEmployeeAssessmentsHistoryBE.propAssessmentSubmitted = "Y";
                                objEmployeeAssessmentsHistoryBE.propCreatedBy = EmployeeId;
                                objEmployeeAssessmentsHistoryBE.propCreatedDate = DateTime.Now.Date;

                                int status = objEmployeeAssessmentsHistoryDAL.EmployeeAssessmentsHistoryInsert(objEmployeeAssessmentsHistoryBE);
                                //lblMessage.Text = "Assessment submitted successfully...";                             
                                
                                if (status > 0)
                                {
                                    System.Net.Mail.MailMessage insMail = new System.Net.Mail.MailMessage();

                                    System.Net.Mail.MailAddress insFrom = new MailAddress("info@chetanasystems.com");
                                    insMail.From = insFrom;

                                    insMail.To.Add("priyamvada@chetanasystems.com");

                                    insMail.Subject = "Assessment submitted by user";
                                    insMail.IsBodyHtml = true;
                                    string strBody = "<html><body><font color=\"black\">Hello Mam <br/><br/> <b>" + Session["UserName"].ToString() + "</b> with userid " + EmployeeId + " has submitted the assessment successfully. <br/><br/> Thank You.<BR/><br/> Best Regards, <br/><br/> HealToZealTeam.</font></body></html>";
                                    insMail.Body = strBody;

                                    System.Net.Mail.SmtpClient ns = new System.Net.Mail.SmtpClient("relay-hosting.secureserver.net"); //"localhost";//"relay-hosting.secureserver.net";
                                    ns.Send(insMail);

                                   // lblMessage.Text = "Assessment submitted successfully...";
                                    // lblMessage.Text = "Thank you very much for participation in this experiment!" + "\n" + "We will do our due diligence with the data we have collected and will definitely inform you about success of our experiment and how this experiment helped us further." + "\n" + "Once we validate our own assumptions, we will be happy to share with you the individual scores with you, upon request!" + "\n" + "We appreciate your continued support and best wishes to Chetana TSPL!!";
                                    Response.Redirect("EmployeeFeedback.aspx",false);
                                }
                                else
                                {

                                }
                            }
                        }
                        else if (Request.QueryString["Profile"] != null)
                        {
                            lblMessage.Text = "Profile updated successfully...";
                            //  Profile
                        }
                        else if (Request.QueryString["FeedBack"] != null)
                        {
                           // lblMessage.Text = "Your Feedback has been submitted successfully...";
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "starScript", "DisplayColorBox1('EmployeeThanksMessage.aspx');", true);  
                           // lblMessage.Text = "Thank you very much for participation in this experiment!" + "\n" + "We will do our due diligence with the data we have collected and will definitely inform you about success of our experiment and how this experiment helped us further." + "\n" + "Once we validate our own assumptions, we will be happy to share with you the individual scores with you, upon request!" + "\n" + "We appreciate your continued support and best wishes to Chetana TSPL!!";

                        }
                        else
                        {
                           ScriptManager.RegisterStartupScript(this, this.GetType(), "starScript", "DisplayColorBox('EmployeeMessage.aspx');", true);  
                          //  ClientScript.RegisterClientScriptBlock(this.GetType(), "openColorBox", "DisplayColorBox();", true);
                            pnlEmployeeBasicDetails.Visible = false;

                           EmployeeDAL objEmployeeDAL = new EmployeeDAL();
                            EmployeeId = Session["EmployeeId"].ToString();
                            DataTable dtEmployee = objEmployeeDAL.EmployeesGetByEmployeeId(EmployeeId);

                            if (dtEmployee != null && dtEmployee.Rows.Count > 0)
                            {
                                if (dtEmployee.Rows[0]["Age"].ToString() != "")
                                {
                                    pnlEmployeeBasicDetails.Visible = false;
                                    slidediv.Visible = true;
                              //      Response.Redirect("EmployeeHome.aspx", false);
                                    ScriptManager.RegisterStartupScript(this, this.GetType(), "starScript", "DisplayColorBox('EmployeeMessage.aspx');", true);
                                }
                                else
                                {
                                    pnlEmployeeBasicDetails.Visible = true;
                                    slidediv.Visible = false;
                                    ((System.Web.UI.HtmlControls.HtmlAnchor)Page.Master.FindControl("A1")).HRef = "EmployeeHome.aspx?first=1";
                                    //  ((System.Web.UI.HtmlControls.HtmlAnchor)Page.Master.FindControl("A2")).HRef = "EmployeeHome.aspx?first=1";
                                    ((System.Web.UI.HtmlControls.HtmlAnchor)Page.Master.FindControl("LnkStart")).HRef = "EmployeeHome.aspx?first=1";
                                    ((System.Web.UI.HtmlControls.HtmlAnchor)Page.Master.FindControl("LnkFeedback")).HRef = "EmployeeHome.aspx?first=1";
                                    // ClientScript.RegisterStartupScript(this.GetType(), "openColorBox", "DisplayColorBox();", true);
                                    ScriptManager.RegisterStartupScript(this, this.GetType(), "starScript", "DisplayColorBox('EmployeeMessage.aspx');", true);
                                    //   lblMessage.Text = "On the first time login, you are being asked to fill in some information about yourself. You will not be asked for identity anytime during this exercise. This exercise is to validate some of our assumptions and thought process during the product development and not to diagnose or comment on any of the individual answers or scores.  Any personal information will not be shared with anyone, anytime.You can trust us for the same and be rest assured."+"\n"+"Once you fill in this small form, you will be presented with a series of questions and answer options and you will be required to answer all of them. You might find some repetition of the questions but it is deliberate and needed to get the right results out of this important exercise."+"\n"+" If you like, you can logout at any time and come back to answer remaining the questions. But your assessment will not be marked “submitted”, until you complete the same. So please make sure you complete the assessment and click the “Submit” button."+"\n"+" There would be a small feedback form at the end and that’s it!"+"\n"+"We will definitely keep in touch and will let you know how this experiment goes, how it helps us in determining our further strategy!"+"\n";
                                }
                            }
                           // ScriptManager.RegisterClientScriptBlock(this.Page, GetType(), "openColorBox", "DisplayColorBox();", true);
                          
                        }
                    }
                }
            }
            catch (Exception)
            {
                
                //throw;
            }
          
        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            try
            {

                if (Page.IsValid)
                { 
                EmployeeId=Session["EmployeeId"].ToString();
                EmployeeDAL objEmployeeDAL = new EmployeeDAL();
                EmployeeBE objEmployeeBE = new EmployeeBE();

                objEmployeeBE.propAge =Convert.ToDateTime(txtBirthDate.Text);
                objEmployeeBE.propExperienceInYears = ddlExperience.SelectedItem.Text;
                objEmployeeBE.propPosition = ddlPosition.SelectedItem.Text;
                objEmployeeBE.propQualification = ddlEducation.SelectedItem.Text;
                objEmployeeBE.propOccupation = txtOccupation.Text;
                objEmployeeBE.propGender = rblGender.SelectedItem.Value;

                objEmployeeBE.propEmployeeId_PK = EmployeeId;
                objEmployeeBE.propUdpatedBy = EmployeeId;
                objEmployeeBE.propUpdatedDate = DateTime.Now.Date;

            int status= objEmployeeDAL.EmployeesFirstUpdate(objEmployeeBE);
            string message = "";
            if (status > 0)
            {
                message = "Updated successfully...";
                Response.Redirect("Assessment.aspx",false);
            }
            else
            {
                message = "Can not update...";
            }
                }
            }
            catch
            {

            }

        }

        protected void Button1_Click(object sender, EventArgs e)
        {



        }
    }
}