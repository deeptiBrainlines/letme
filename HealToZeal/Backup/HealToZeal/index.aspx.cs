﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using DAL;
using BE;
namespace HealToZeal
{
    public partial class index : System.Web.UI.Page
    {
        private string EmployeeId;

        protected void Page_Load(object sender, EventArgs e)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "starScript", "DisplayColorBox('../EmployeeMessage.aspx');", true);  
        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            try
            {

                if (Page.IsValid)
                {
                    EmployeeId = Session["EmployeeId"].ToString();
                    EmployeeDAL objEmployeeDAL = new EmployeeDAL();
                    EmployeeBE objEmployeeBE = new EmployeeBE();

                    objEmployeeBE.propAge = Convert.ToDateTime(txtBirthDate.Text);
                    objEmployeeBE.propExperienceInYears = ddlExperience.SelectedItem.Text;
                    objEmployeeBE.propPosition = ddlPosition.SelectedItem.Text;
                    objEmployeeBE.propQualification = ddlEducation.SelectedItem.Text;
                    objEmployeeBE.propOccupation = txtOccupation.Text;
                    objEmployeeBE.propGender = rblGender.SelectedItem.Value;

                    objEmployeeBE.propEmployeeId_PK = EmployeeId;
                    objEmployeeBE.propUdpatedBy = EmployeeId;
                    objEmployeeBE.propUpdatedDate = DateTime.Now.Date;

                    int status = objEmployeeDAL.EmployeesFirstUpdate(objEmployeeBE);
                    string message = "";
                    if (status > 0)
                    {
                        message = "Updated successfully...";
                        Response.Redirect("Assessment.aspx", false);
                    }
                    else
                    {
                        message = "Can not update...";
                    }
                }
            }
            catch
            {

            }
        }
    }
}