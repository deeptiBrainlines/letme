﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DAL;
using BE;
using System.Data;
namespace HealToZeal.Company
{
    public partial class CompanyProfile : System.Web.UI.Page
    {
   
        public  string CompanyId = "";

        CompanyDetailsDAL objCompanyDetailsDAL = new CompanyDetailsDAL();
        CountryDAL objCountryDAL = new CountryDAL();

        string adminId = "";

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            { 
                // display
                if (Session["AdminId"] != null)
                {
                    BindCompanyDetailsGrid();
                    BindCountry();
                    bindCompanyDomain();
                }             
            }
        }

        private void bindCompanyDomain()
        {
            try
            {
                DataTable DtDoamin= objCountryDAL.CompanyDomainGet();
                ddlDomain.DataSource = DtDoamin;
                ddlDomain.DataValueField = "DomainId_PK";
                ddlDomain.DataTextField = "DomainName";
                ddlDomain.DataBind();
                ddlDomain.Items.Insert(0, new ListItem("--Select Domain--", "--Select Domain--"));
                ddlDomain.SelectedIndex = 0;
            }
            catch 
            {
                
            }
        }

        private void BindCountry()
        {
            try
            {
                DataTable DtCountry=objCountryDAL.CountryMasterGet();

                ddlCountry.DataSource = DtCountry;
                ddlCountry.DataValueField = "CountryId_PK";
                ddlCountry.DataTextField  = "CountryName";
                ddlCountry.DataBind();

                ddlCountry.Items.Insert(0, new ListItem("--Select country--", "--Select country--"));
                ddlCountry.SelectedIndex = 0;

            }
            catch 
            {

            }
        }

        private void BindCompanyDetailsGrid()
        {
            try
            {
                DataTable DtCompanyDetails = objCompanyDetailsDAL.CompanyDetailsGet();
             if (DtCompanyDetails != null && DtCompanyDetails.Rows.Count > 0)
             {
                 GvViewCompanyProfile.DataSource = DtCompanyDetails;
                 GvViewCompanyProfile.DataBind();
             }     
            }
            catch
            {
            }
        }

        protected void BtnAddCompanyProfile_Click(object sender, EventArgs e)
        {
            try
            {
                InsertUpdate();
            }
            catch 
            {
                
            }
        }

        private void InsertUpdate()
        {
            try
            {
                if(Page.IsValid)
                {
                    if (Session["AdminId"] != null)
                    { 
                    
                CompanyDetailsBE objCompanyDetailsBE = new CompanyDetailsBE();
                objCompanyDetailsBE.propCompanyName = txtCompanyName.Text;
                objCompanyDetailsBE.propUserName = txtCompanyUserName.Text;              
                objCompanyDetailsBE.propDomainId_FK = ddlDomain.SelectedValue;
                objCompanyDetailsBE.propNoOfEmployees = Convert.ToInt32(txtNoOfEmployees.Text);
                objCompanyDetailsBE.propCompanyAddress = txtAddress.Text;
                objCompanyDetailsBE.propCompanyContactNo = txtCompanyContactNo.Text;
                objCompanyDetailsBE.propCountry = ddlCountry.SelectedValue;
                objCompanyDetailsBE.propCity = ddlCity.SelectedValue;
                objCompanyDetailsBE.propContactPersonName = txtContactPerson.Text;
                objCompanyDetailsBE.propMobileNo = txtMobileNo.Text;
                objCompanyDetailsBE.propEmailId = txtEmailId.Text;
                objCompanyDetailsBE.propContactPersonContactNo = txtContactNo.Text;
                objCompanyDetailsBE.propZipCode = txtZipcode.Text;
                objCompanyDetailsBE.propCreatedBy = adminId;// "18DD4921-9D48-43C6-805E-BE5CA95AC286";// Admin user
                objCompanyDetailsBE.propCreatedDate=DateTime.Now.Date;
                objCompanyDetailsBE.propUpdatedBy = adminId;// "18DD4921-9D48-43C6-805E-BE5CA95AC286";
                objCompanyDetailsBE.propUpdatedDate = DateTime.Now.Date;
                int status = 0;
                string  message = "";
                if (Session["CompanyId"]!= null )
                {
                    objCompanyDetailsBE.propCompanyId_PK = Session["CompanyId"].ToString();
                    
                    status = objCompanyDetailsDAL.CompanyDetailsUpdate(objCompanyDetailsBE);
                    if (status > 0)
                    {
                        message = "Company profile updated successfully...";
                        ClearControls();                       
                    }
                    else
                    {
                        message = "Can not update company profile...";
                    }
                    
                }
                else
                {
                    string password = Encryption.CreatePassword();
                    objCompanyDetailsBE.propPassword = Encryption.Encrypt(password);
                    status = objCompanyDetailsDAL.CompanyDetailsInsert(objCompanyDetailsBE);
                    if (status > 0)
                    {
                        message = "Company profile added successfully...";
                        ClearControls();
                    }
                    else
                    {
                        message = "Can not add company profile...";                                            
                    }
                }
                BindCompanyDetailsGrid();
                }

                }
            }
            catch 
            {
               
            }
        }

        protected void GvViewCompanyProfile_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                if (e.CommandName == "EditCompany")
                {
                  //CompanyId = e.CommandArgument.ToString();
                  Session["CompanyId"] = e.CommandArgument.ToString();
                  BindCompanyProfile();

                }
            }
            catch 
            {
               
            }
        }

        private void BindCompanyProfile()
        {
            try
            {
                  CompanyId = Session["CompanyId"].ToString();
                 DataTable DtCompanyDetails = objCompanyDetailsDAL.CompanyDetailsGetByCompanyId(CompanyId);
                 if (DtCompanyDetails != null && DtCompanyDetails.Rows.Count > 0)
                 {
                     txtCompanyName.Text = DtCompanyDetails.Rows[0]["CompanyName"].ToString();
                     ddlDomain.SelectedValue = DtCompanyDetails.Rows[0]["DomainId_FK"].ToString();
                     txtNoOfEmployees.Text = DtCompanyDetails.Rows[0]["NoOfEmployees"].ToString();
                     txtCompanyUserName.Text = DtCompanyDetails.Rows[0]["UserName"].ToString();
                     //txtPassword.Text = DtCompanyDetails.Rows[0]["UserName"].ToString();
                     txtAddress.Text = DtCompanyDetails.Rows[0]["CompanyAddress"].ToString();
                     ddlCity.SelectedValue = DtCompanyDetails.Rows[0]["City"].ToString();
                     ddlCountry.SelectedValue = DtCompanyDetails.Rows[0]["Country"].ToString();
                     txtZipcode.Text = DtCompanyDetails.Rows[0]["ZipCode"].ToString();
                     txtCompanyContactNo.Text = DtCompanyDetails.Rows[0]["CompanyContactNo"].ToString();
                     txtContactPerson.Text = DtCompanyDetails.Rows[0]["ContactPersonName"].ToString();
                     txtEmailId.Text = DtCompanyDetails.Rows[0]["EmailId"].ToString();
                     txtContactNo.Text = DtCompanyDetails.Rows[0]["ContactPersonContactNo"].ToString();
                     txtMobileNo.Text = DtCompanyDetails.Rows[0]["MobileNo"].ToString();
                     CompanyId = DtCompanyDetails.Rows[0]["CompanyId_PK"].ToString();
                     TabContainerCompanyProfile.ActiveTabIndex = 1;
                 }
            }
            catch 
            {
                
            }
        }

        protected void btnReset_Click(object sender, EventArgs e)
        {
            try
            {
                ClearControls();
            }
            catch 
            {
                
            }
        }

        private void ClearControls()
        {
            try
            {
                txtCompanyName.Text = "";
                txtAddress.Text = "";
                txtCompanyContactNo.Text = "";
                txtCompanyUserName.Text = "";
                txtContactNo.Text = "";
                txtContactPerson.Text = "";
                txtEmailId.Text = "";
                txtMobileNo.Text = "";
                txtNoOfEmployees.Text = "";
              
                txtZipcode.Text = "";
                ddlCity.SelectedIndex = -1;
                ddlCountry.SelectedIndex = -1;
                ddlDomain.SelectedIndex = -1;
                CompanyId = "";
                Session["CompanyId"] = null;
            }
            catch 
            {
                
            }
        }

        protected void ddlCountry_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (ddlCountry.SelectedIndex != 0)
                { 
                    int CountryId=Convert.ToInt32(ddlCountry.SelectedItem.Value);
                    DataTable  DtCity= objCountryDAL.CityMasterGetByCountryId(CountryId);
                    ddlCity.DataSource = DtCity;
                    ddlCity.DataValueField = "CityId_PK";
                    ddlCity.DataTextField = "CityName";
                    ddlCity.DataBind();

                    ddlCity.Items.Insert(0, new ListItem("--Select city--", "--Select city--"));
                    ddlCity.SelectedIndex = 0;
                }
            }
            catch
            {

            }
        }
    }
}