﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="CompanyProfile.aspx.cs" Inherits="HealToZeal.Company.CompanyProfile"  %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <asp:ScriptManager ID="ScriptManager1" runat="server">
        </asp:ScriptManager>
        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <cc1:TabContainer ID="TabContainerCompanyProfile" runat="server" 
            ActiveTabIndex="0">
          <cc1:TabPanel ID="TabPnlView" runat="server" >
          <HeaderTemplate>View Companies</HeaderTemplate>
          <ContentTemplate>
              <asp:GridView ID="GvViewCompanyProfile" AutoGenerateColumns="False" 
                  runat="server" onrowcommand="GvViewCompanyProfile_RowCommand">
              <Columns>
              <asp:TemplateField>
              <HeaderTemplate>
              <table  width="100%">
              <tr>
              <td  width="15%">Company name </td>
              <td  width="15%">Company domain </td>
              <td  width="20%">No of Employees</td>
              <td  width="20%">Contact no.</td>
              <td  width="20%">Contact person</td>
              <td></td>
              <td></td>
              </tr>
              </table>
              </HeaderTemplate>
              <ItemTemplate>
              <table  width="100%">
              <tr>
              <td  width="15%">
                  <asp:Label ID="lblCompanyName" runat="server" Text='<%#Eval("CompanyName") %>'></asp:Label>
              </td>
              <td  width="15%"> 
                  <asp:Label ID="lblCompanyDomain" runat="server" Text='<%#Eval("DomainName") %>'></asp:Label>
              </td>
              <td width="20%">
                <asp:Label ID="lblNoOfEmployee" runat="server" Text='<%#Eval("NoOfEmployees") %>'></asp:Label>
              </td>
              <td  width="20%">
              <asp:Label ID="lblCompanyContactNo" runat="server" Text='<%#Eval("CompanyContactNo") %>'></asp:Label>
              </td>
               <td width="20%">
              <asp:Label ID="lblContactPersonName" runat="server" Text='<%#Eval("ContactPersonName") %>'></asp:Label>
              </td>
              <td  width="10%">
                  <asp:Button ID="BtnEdit" runat="server" Text="Edit" CommandName="EditCompany" CommandArgument='<%#Eval("CompanyId_PK") %>' />
              </td>
                 <td>
                  <asp:Button ID="BtnDelete" runat="server" Text="Delete" CommandName="DeleteCompany" CommandArgument='<%#Eval("CompanyId_PK") %>' Visible="false" />
              </td>
              </tr>
              </table>
              </ItemTemplate>
              </asp:TemplateField>
              </Columns>
              </asp:GridView>
          </ContentTemplate>
          </cc1:TabPanel> 
          <cc1:TabPanel ID="TabPnlAddUpdate" runat="server" >
            <HeaderTemplate>Add/Update Companies</HeaderTemplate>
                <ContentTemplate>                    
                       <table >
                    <tr>
                        <td>
                            &nbsp;</td>
                        <td>
                            <asp:Label ID="lblTitle" runat="server" Text="Company profile"></asp:Label>
                        </td>
                        <td>
                            &nbsp;</td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Label ID="lblCompanyName" runat="server" Text="Company name"></asp:Label>
                        </td>
                        <td>
                            <asp:TextBox ID="txtCompanyName" runat="server" ></asp:TextBox>
                        </td>
                        <td>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" 
                                ControlToValidate="txtCompanyName" ErrorMessage="Enter company name..." 
                                ForeColor="Red" ValidationGroup="0"></asp:RequiredFieldValidator>
                        </td>
                    </tr>
                         <tr>
                        <td>
                            <asp:Label ID="lblUserName" runat="server" Text="Company user name"></asp:Label>
                        </td>
                        <td>
                            <asp:TextBox ID="txtCompanyUserName" runat="server" ></asp:TextBox>
                        </td>
                        <td>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" 
                                ControlToValidate="txtCompanyUserName" ErrorMessage="Enter user name..." 
                                ForeColor="Red" ValidationGroup="0"></asp:RequiredFieldValidator>
                             </td>
                    </tr>
                    
                    <tr>
                        <td>
                            <asp:Label ID="lblCompanydomain" runat="server" Text="Domain"></asp:Label>
                        </td>
                        <td>
                            <asp:DropDownList ID="ddlDomain" runat="server">
                            </asp:DropDownList>
                        </td>
                        <td>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" 
                                ControlToValidate="ddlDomain" ErrorMessage="Select domain..." ForeColor="Red" 
                                InitialValue="--Select Domain--" ValidationGroup="0"></asp:RequiredFieldValidator>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Label ID="lblNoOfEmployees" runat="server" Text="Number of Employees"></asp:Label>
                        </td>
                        <td>
                            <asp:TextBox ID="txtNoOfEmployees" runat="server"></asp:TextBox>
                        </td>
                        <td>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" 
                                ControlToValidate="txtNoOfEmployees" ErrorMessage="Enter no of employees..." 
                                ForeColor="Red" ValidationGroup="0"></asp:RequiredFieldValidator>
                        </td>
                    </tr>
                        <tr>
                        <td>
                            <asp:Label ID="lblCompanyContactNo" runat="server" Text="Company Contact no"></asp:Label>
                        </td>
                        <td>
                            <asp:TextBox ID="txtCompanyContactNo" runat="server"></asp:TextBox>
                        </td>
                        <td>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" 
                                ControlToValidate="txtContactNo" ErrorMessage="Enter contact number..." 
                                ForeColor="Red" ValidationGroup="0"></asp:RequiredFieldValidator>
                            <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" 
                                ControlToValidate="txtCompanyContactNo" ErrorMessage="Enter proper contact no..." 
                                ForeColor="Red" ValidationExpression="^[0-9]{10,15}$"></asp:RegularExpressionValidator>
                            </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Label ID="lblCompanyAddress" runat="server" Text="Address"></asp:Label>
                        </td>
                        <td>
                            <asp:TextBox ID="txtAddress" runat="server"></asp:TextBox>
                        </td>
                        <td>
                            &nbsp;</td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Label ID="lblZipcode" runat="server" Text="Zip code"></asp:Label>
                        </td>
                        <td>
                            <asp:TextBox ID="txtZipcode" runat="server"></asp:TextBox>
                        </td>
                        <td>
                            &nbsp;</td>
                    </tr>
                     <tr>
                        <td>
                            <asp:Label ID="lblCountry" runat="server" Text="Country"></asp:Label>
                        </td>
                        <td>
                  
                            <asp:DropDownList ID="ddlCountry" runat="server" AutoPostBack="True" 
                                onselectedindexchanged="ddlCountry_SelectedIndexChanged">
                            <asp:ListItem Text="India" Value="India"></asp:ListItem>                        
                            </asp:DropDownList>
                  
                        </td>
                        <td>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" 
                                ControlToValidate="ddlCountry" ErrorMessage="Select country..." ForeColor="Red"></asp:RequiredFieldValidator>
                         </td>
                    </tr>                    
                     <tr>
                        <td>
                            <asp:Label ID="lblCity" runat="server" Text="City"></asp:Label>
                        </td>
                        <td>
                  
                            <asp:DropDownList ID="ddlCity" runat="server">

                            <asp:ListItem></asp:ListItem>
                            </asp:DropDownList>
                  
                        </td>
                        <td></td>
                    </tr>
               
                    <tr>
                        <td>
                            <asp:Label ID="lblContactPerson" runat="server" Text="Contact person"></asp:Label>
                        </td>
                        <td>
                            <asp:TextBox ID="txtContactPerson" runat="server"></asp:TextBox>
                        </td>
                        <td></td>
                    </tr>
                      <tr>
                        <td>
                            <asp:Label ID="lblEmailId" runat="server" Text="Email Id"></asp:Label>
                        </td>
                        <td>
                            <asp:TextBox ID="txtEmailId" runat="server"></asp:TextBox>
                        </td>
                        <td>
                            <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" 
                                ControlToValidate="txtEmailId" 
                                ErrorMessage="Enter email id in correct format..." ForeColor="Red" 
                                ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" 
                                ValidationGroup="0"></asp:RegularExpressionValidator>
                          </td>
                    </tr>
                     <tr>
                        <td>
                            <asp:Label ID="lblContactNo" runat="server" Text="Contact no"></asp:Label>
                        </td>
                        <td>
                            <asp:TextBox ID="txtContactNo" runat="server"></asp:TextBox>
                        </td>
                        <td>
                            <asp:RegularExpressionValidator ID="RegularExpressionValidator3" runat="server" 
                                ControlToValidate="txtContactNo" ErrorMessage="Enter contact no..." 
                                ForeColor="Red" ValidationExpression="^[0-9]{10,15}$" ValidationGroup="0"></asp:RegularExpressionValidator>
                         </td>
                    </tr>
                      <tr>
                        <td>
                            <asp:Label ID="lblMobileNumber" runat="server" Text="Mobile no"></asp:Label>
                        </td>
                        <td>
                            <asp:TextBox ID="txtMobileNo" runat="server"></asp:TextBox>
                        </td>
                        <td>
                            <asp:RegularExpressionValidator ID="RegularExpressionValidator4" runat="server" 
                                ControlToValidate="txtMobileNo" ErrorMessage="Enter mobile no..." 
                                ForeColor="Red" ValidationExpression="^[0-9]{10,15}$" ValidationGroup="0"></asp:RegularExpressionValidator>
                          </td>
                    </tr>
                    <tr>
                        <td>
                            &nbsp;</td>
                        <td>
                            <asp:Button ID="BtnAddCompanyProfile" runat="server" Text="Submit" 
                                onclick="BtnAddCompanyProfile_Click" />
                            <asp:Button ID="btnReset" runat="server" Text="Reset" 
                                onclick="btnReset_Click" />
                        </td>
                        <td>
                            &nbsp;</td>
                    </tr>
                </table>   
                </ContentTemplate>   
        </cc1:TabPanel>
        </cc1:TabContainer>

        </ContentTemplate>
        </asp:UpdatePanel>
    
    </div>
    </form>
</body>
</html>
