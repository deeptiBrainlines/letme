﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BE;
using DAL;
using System.Data;
namespace HealToZeal.Admin
{
    public partial class AnswerMaster : System.Web.UI.Page
    {
        QuestionMasterDAL objQuestionMasterDAL = new QuestionMasterDAL();
        QuestionMasterBE objQuestionMasterBE = new QuestionMasterBE();
        AnswerMasterBE objAnswerMasterBE = new AnswerMasterBE();
        AnswerMasterDAL objAnswerMasterDAL = new AnswerMasterDAL();

        string QuestionId = "";
        string AnswerId = "";
        string adminId = "";

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Session["AdminId"] != null)
                {
                    if (Session["QuestionId"] != null)
                    {
                        QuestionId = Session["QuestionId"].ToString();
                        DataTable DtAnswers = objAnswerMasterDAL.AnswerMasterGetByQuestionId(QuestionId);
                        GvViewAnswersOfQuestion.DataSource = DtAnswers;
                        GvViewAnswersOfQuestion.DataBind();
                        TabContainerAnswerMaster.ActiveTabIndex = 0;
                    }
                    else
                    {
                        TabPnlViewAnswers.Enabled = false;
                        TabContainerAnswerMaster.ActiveTabIndex = 1;
                    }
                    BindQuestionDetailsGrid();
                    BindDdlQuestion();
                }
                else
                {
                    Response.Redirect("adminLogin.aspx", false);
                }
            }
        }

        private void BindQuestionDetailsGrid()
        {
            try
            {
                DataTable DtQuestion = objQuestionMasterDAL.QuestionMasterGet();
                if (DtQuestion != null && DtQuestion.Rows.Count > 0)
                {
                    GvViewQuestions.DataSource = DtQuestion;
                    GvViewQuestions.DataBind();

                    foreach (GridViewRow GvRow in GvViewQuestions.Rows)
                    {
                        Label L = (Label)GvRow.FindControl("lblQuestionId");
                       
                        DataTable DtAnswers = objAnswerMasterDAL.AnswerMasterGetByQuestionId(L.Text);

                        GridView GvViewAnswers = (GridView)GvRow.FindControl("GvViewAnswers");
                        GvViewAnswers.DataSource = DtAnswers;
                        GvViewAnswers.DataBind();
                    }
                }
            }
            catch
            {
            }
        }

        protected void GvViewAnswers_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                if (e.CommandName == "EditAnswer")
                {
                    AnswerId = e.CommandArgument.ToString();
                    Session["AnswerId"] = AnswerId;
                    BindAnswer();                   
                }
            }
            catch
            {

            }
        }

        private void BindAnswer()
        {
            try
            {
                AnswerId = Session["AnswerId"].ToString();
                DataTable DtAnswer = objAnswerMasterDAL.AnswerMasterGetByAnswerId(AnswerId);
                if (DtAnswer != null && DtAnswer.Rows.Count > 0)
                {
                    ddlQuestions.SelectedValue = DtAnswer.Rows[0]["QuestionId_FK"].ToString();
                    txtAnswer.Text = DtAnswer.Rows[0]["Answer"].ToString();
                    txtAnswerDescription.Text = DtAnswer.Rows[0]["AnswerDescription"].ToString();
                    txtScoreForAnswer.Text = DtAnswer.Rows[0]["ScoreForAnswer"].ToString();

                    TabContainerAnswerMaster.ActiveTabIndex = 2;
                }
            }
            catch 
            {

            }
        }

        private void BindDdlQuestion()
        {
            try
            {
                DataTable DtQuestion = objQuestionMasterDAL.QuestionMasterGet();
                if (DtQuestion != null && DtQuestion.Rows.Count > 0)
                {
                    ddlQuestions.DataSource = DtQuestion;
                    ddlQuestions.DataTextField ="Question";
                    ddlQuestions.DataValueField = "QuestionId_PK";
                    ddlQuestions.DataBind();

                    ddlQuestions.Items.Insert(0, new ListItem("--Select Question--", "--Select Question--"));
                    ddlQuestions.SelectedIndex = 0;
                }
            }
            catch
            {
               
            }
        }

        protected void BtnAddUpdate_Click(object sender, EventArgs e)
        {
            try
            {
                if (Session["AdminId"] != null)
                {
                    adminId = Session["AdminId"].ToString();
                objAnswerMasterBE.propAnswer = txtAnswer.Text;
                objAnswerMasterBE.propAnswerDescription = txtAnswerDescription.Text;
                objAnswerMasterBE.propQuestionId_FK = ddlQuestions.SelectedItem.Value;
                objAnswerMasterBE.propScoreForAnswer = Convert.ToInt32(txtScoreForAnswer.Text);
                objAnswerMasterBE.propCreatedBy = adminId;// "18DD4921-9D48-43C6-805E-BE5CA95AC286"; //user id
                objAnswerMasterBE.propCreatedDate = DateTime.Now.Date;
                objAnswerMasterBE.propUpdatedBy = adminId; // "18DD4921-9D48-43C6-805E-BE5CA95AC286";//user id
                objAnswerMasterBE.propUpdatedDate = DateTime.Now.Date;
                int status = 0;
                string message = "";

                if (Session["AnswerId"] != null)
                {
                    AnswerId = Session["AnswerId"].ToString();
                    objAnswerMasterBE.propAnswerId_PK = AnswerId;
                    status = objAnswerMasterDAL.AnswerMasterUpdate(objAnswerMasterBE);
                    if (status > 0)
                    {
                        message = "Answer updated successully...";
                        ClearAnswers();
                    }
                    else
                    {
                        message = "Can not update answer...";
                    }
                }
                else
                {
                    status = objAnswerMasterDAL.AnswerMasterInsert(objAnswerMasterBE);
                    if (status > 0)
                    {
                        message = "Answer added successully...";
                        ClearAnswers();
                    }
                    else
                    {
                        message = "Can not add answer...";
                    }
                }
                }
            }
            catch 
            {

            }
        }

        private void ClearAnswers()
        {
            try
            {
                Session["Answer"] = null;
                txtAnswer.Text = "";
                txtAnswerDescription.Text = "";
                txtScoreForAnswer.Text = "";
                ddlQuestions.SelectedIndex = 0;
                Session["AnswerId"] = null;
            }
            catch
            {
            }
        }

        protected void GvViewAnswersOfQuestion_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                if (e.CommandName == "EditAnswer")
                {
                    AnswerId = e.CommandArgument.ToString();
                    Session["AnswerId"] = AnswerId;
                    BindAnswer();    
                }
            }
            catch 
            {
                
            }
        }

        protected void ddlQuestions_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (ddlQuestions.SelectedIndex != 0)
                {
                    DataTable DtAnswerCount = objQuestionMasterDAL.QuestionMasterGetTotalnoAnswersForQuestion(ddlQuestions.SelectedValue.ToString());
                    if (DtAnswerCount != null && DtAnswerCount.Rows.Count > 0)
                    {
                        int answerCount = Convert.ToInt32(DtAnswerCount.Rows[0]["AnswerCount"].ToString());
                        if (answerCount == 0)
                        {
                            ddlQuestions.SelectedIndex = 0;
                            //message cant add more answers
                        }
                    }
                }           
            }
            catch 
            {
              
            }
        }

        protected void BtnReset_Click(object sender, EventArgs e)
        {
            try
            {
                ClearAnswers();
            }
            catch
            {
            }
        }
    }
}