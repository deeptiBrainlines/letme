﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using BE;
using DAL;
namespace HealToZeal.Admin
{
    public partial class AdminLogin : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnLogin_Click(object sender, EventArgs e)
        {
            try
            {
                AdminDAL  objAdminDAL = new AdminDAL ();

                DataTable DtAdmin = objAdminDAL.SpAdminLogin(txtUsername.Text , Encryption.Encrypt(txtPassword.Text));
                if (DtAdmin != null && DtAdmin.Rows.Count > 0)
                {
                    Session["AdminName"] = txtUsername .Text;
                    //Session["DtCompanyDetails"] = DtCompanyDetails;
                    Session["AdminId"] = DtAdmin.Rows[0]["UserId_PK"].ToString();
                    Response.Redirect("AdminHome.aspx");
                }
                else
                {
                    lblMessage .Text = "Invalid Username/Password...";
                }
            }
            catch
            {

            }
        }
    }
}