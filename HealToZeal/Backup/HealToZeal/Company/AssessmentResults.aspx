﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Company/Company.Master" AutoEventWireup="true" CodeBehind="AssessmentResults.aspx.cs" Inherits="HealToZeal.Company.AssessmentResults" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="LeftColumn" runat="server">

 <div class="divClass">
     <asp:DropDownList ID="ddlEmployees" runat="server" 
         onselectedindexchanged="ddlEmployees_SelectedIndexChanged">

     </asp:DropDownList>
    <asp:DataList ID="dtlst" runat="server">
   
       <ItemTemplate>       
            <asp:Label ID="lblEmployeeId" runat="server" Text='<%#Eval("EmployeeId_FK") %>'></asp:Label>            
            <asp:Label ID="lblEmployeeName" runat="server" Text='<%#Eval("EmployeeName") %>'></asp:Label>
             <asp:Label ID="lblAssessmentId" runat="server" Text='<%#Eval("AssessmentId_FK") %>'></asp:Label>
             <asp:Label ID="lblClusterId" runat="server" Text='<%#Eval("ClusterId") %>'></asp:Label>
             <asp:Label ID="lblClusterName" runat="server" Text='<%#Eval("ClusterName") %>'></asp:Label>
               <asp:Label ID="lblTotalScore" runat="server" Text='<%#Eval("TotalScore") %>'></asp:Label>
                 <asp:Label ID="Label1" runat="server" Text='<%#Eval("Score") %>'></asp:Label>
             
        </ItemTemplate>

    </asp:DataList>
</div>
</asp:Content>
