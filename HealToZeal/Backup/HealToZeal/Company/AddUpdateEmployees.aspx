﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="AddUpdateEmployees.aspx.cs" MasterPageFile="~/Company/Company.Master" Inherits="HealToZeal.Company.AddEmployees" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>


<asp:Content ID="Content2" ContentPlaceHolderID="LeftColumn" runat="Server">

   <div>
     
       <cc1:TabContainer ID="TabContainerEmployees" runat="server" ActiveTabIndex="1"> 
       <cc1:TabPanel ID="TabPnlViewEmployee" runat="server" HeaderText="View Employees">
       <ContentTemplate>
       <div>
           <asp:GridView ID="GvViewEmployee" runat="server" AutoGenerateColumns="False">
           <Columns>
           <asp:TemplateField>
           <ItemTemplate>
           <table>
           <tr>
           <td>
            <asp:Label ID="lblSrNo" runat="server" Text='<%#Eval("SrNo") %>'></asp:Label>         
           </td>
           <td>
               <asp:Label ID="lblEmployeeId" runat="server" Text='<%#Eval("EmployeeId_PK") %>' Visible="false"></asp:Label>
           </td>
           <td>
            <asp:Label ID="lblCompanyId_FK" runat="server" Text='<%#Eval("CompanyId_FK") %>' Visible="false" ></asp:Label>
           </td>
             <td>
            <asp:Label ID="lblEmployeeCompanyId" runat="server" Text='<%#Eval("EmployeeCompanyId") %>'></asp:Label>
           </td>
            <td>
            <asp:Label ID="lblEmployeeName" runat="server" Text='<%#Eval("EmployeeName") %>'></asp:Label>
           </td>
           <td>
            <asp:Label ID="lblEmployeeAddress" runat="server" Text='<%#Eval("EmployeeAddress") %>'></asp:Label>
           </td>
            <td>
            <asp:Label ID="lblEmailId" runat="server" Text='<%#Eval("EmailId") %>'></asp:Label>
           </td>
            <td>
            <asp:Label ID="lblMobileNo" runat="server" Text='<%#Eval("MobileNo") %>'></asp:Label>
           </td>
           <td>
               <asp:Button ID="btnEdit" runat="server" Text="Edit" CommandArgument='<%#Eval("EmployeeId_PK") %>' CommandName="EditEmployee" Visible="false"/>
           </td>
           </tr>
           </table>
           </ItemTemplate>
           </asp:TemplateField>
           </Columns>
           </asp:GridView>
       </div>
       </ContentTemplate>
       </cc1:TabPanel>
      <cc1:TabPanel ID="TabPnlAddUpdateEmployee" runat="server" HeaderText="Add/Update Employees">
       <ContentTemplate>
       <div>
            <table class="style1">
            <tr>
                <td>
                    &nbsp;</td>
                <td>
                    <asp:Label ID="lblTitle" runat="server" Text="Add Employee Profile"></asp:Label>
                </td>
                <td>
                    &nbsp;</td>
                    <td></td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="lblCompanydomain" runat="server" Text="Cadre"></asp:Label>
                </td>
                <td>
                    <asp:DropDownList ID="ddlCadre" runat="server">
                    </asp:DropDownList>
                </td>
                <td>
                    &nbsp;</td>
                <td>
                    &nbsp;</td>
            </tr>
              <tr>
                <td>
                    <asp:Label ID="lblCompanyAddress" runat="server" Text="Employee Id"></asp:Label>
                </td>
                <td>
                    <asp:TextBox ID="txtEmployeeCompanyId" runat="server"></asp:TextBox>
                </td>
                  <td>
                      &nbsp;</td>
                <td>
                    &nbsp;</td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="lblFirstName" runat="server" Text="First name"></asp:Label>
                </td>
                <td>
                    <asp:TextBox ID="txtFirstName" runat="server"></asp:TextBox>
                </td>
                <td>
                    &nbsp;</td>
                <td>
                    &nbsp;</td>
            </tr>
         
            <tr>
                <td>
                    <asp:Label ID="lblLastName" runat="server" Text="Last name"></asp:Label>
                </td>
                <td>
                    <asp:TextBox ID="txtLastName" runat="server"></asp:TextBox>
                </td>
              <td>
                    &nbsp;</td>
                <td>
                    &nbsp;</td>
            </tr>
            <tr>
            <td>
                <asp:Label ID="lblEmployeeEmailId" runat="server" Text="Employee email id"></asp:Label>
            </td>
              <td>
                 <asp:TextBox ID="txtEmailId" runat="server"></asp:TextBox>
            </td>
               
            </tr>
            <tr>
                <td>
                    &nbsp;</td>
                <td>
                    <asp:Button ID="btnAddEmployee" runat="server" Text="Add employee" 
                        onclick="btnAddEmployee_Click" />
                </td>
                <td>
                    &nbsp;</td>
                    <td>
                    &nbsp;</td>
            </tr>
            <tr>
                <td>
                    &nbsp;</td>
                <td>
                    &nbsp;</td>
                <td>
                    &nbsp;</td>
                    <td>
                    &nbsp;</td>
            </tr>
        </table>
    
       </div>
       </ContentTemplate>
       </cc1:TabPanel>
       </cc1:TabContainer>
   
    </div>
</asp:Content> 


