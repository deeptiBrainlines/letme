﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DAL;
using System.Data;
namespace HealToZeal.Company
{
    public partial class CompanyLogin : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnLogin_Click(object sender, EventArgs e)
        {
            try
            {
                CompanyDetailsDAL objCompanyDetailsDAL = new CompanyDetailsDAL();

                DataTable DtCompanyDetails= objCompanyDetailsDAL.CompanyDetailsLogin( txtUserName.Text,Encryption.Encrypt(txtPassword.Text));
                if (DtCompanyDetails != null && DtCompanyDetails.Rows.Count > 0)
                {
                    Session["CompanyName"] = txtUserName.Text;
                    //Session["DtCompanyDetails"] = DtCompanyDetails;
                    Session["CompanyId"] = DtCompanyDetails.Rows[0]["CompanyId_PK"].ToString();
                    Response.Redirect("CompanyHome.aspx");
                }
                else
                {
                    lblmessage.Text = "Invalid Username/Password...";
                }
            }
            catch
            {
                
            }
        }

        protected void ImgBtnLogin_Click(object sender, ImageClickEventArgs e)
        {
            try
            {
                CompanyDetailsDAL objCompanyDetailsDAL = new CompanyDetailsDAL();

                DataTable DtCompanyDetails = objCompanyDetailsDAL.CompanyDetailsLogin(txtUserName.Text, Encryption.Encrypt(txtPassword.Text));
                if (DtCompanyDetails != null && DtCompanyDetails.Rows.Count > 0)
                {
                    Session["CompanyName"] = txtUserName.Text;
                    //Session["DtCompanyDetails"] = DtCompanyDetails;
                    Session["CompanyId"] = DtCompanyDetails.Rows[0]["CompanyId_PK"].ToString();
                    Response.Redirect("CompanyHome.aspx");
                }
                else
                {
                    lblmessage.Text = "Invalid Username/Password...";
                }
            }
            catch
            {

            }
        }
    }
}