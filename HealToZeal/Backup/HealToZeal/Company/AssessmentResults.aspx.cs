﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BE;
using DAL;
using System.Data;

namespace HealToZeal.Company
{
    public partial class AssessmentResults : System.Web.UI.Page
    {
        string companyId = "";
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                if (Session["CompanyId"] != null)
                {
                    BindEmployees();
                }
            }
        }

        private void BindEmployees()
        {
            try
            {
                companyId = Session["CompanyId"].ToString();
                EmployeeAssessmentsHistoryDAL objEmployeeAssessmentsHistoryDAL = new EmployeeAssessmentsHistoryDAL();
                DataTable dtEmployees= objEmployeeAssessmentsHistoryDAL.EmployeeAssessmentHistoryGetEmployees(companyId);
                if (dtEmployees != null && dtEmployees.Rows.Count > 0)
                {
                    ddlEmployees.DataSource = dtEmployees;
                    ddlEmployees.DataTextField = "EmpName";
                    ddlEmployees.DataValueField = "EmployeeId_FK";
                    ddlEmployees.DataBind();
                    ddlEmployees.Items.Insert(0, new ListItem("--Select Employee--", "--Select Employee--"));
                    ddlEmployees.SelectedIndex = 0;
                }

            }
            catch 
            {
               
            }
        }

        protected void ddlEmployees_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlEmployees.SelectedIndex != 0)
            { 
                
            }
        }
    }
}