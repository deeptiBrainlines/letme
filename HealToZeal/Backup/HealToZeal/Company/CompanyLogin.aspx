﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="CompanyLogin.aspx.cs" Inherits="HealToZeal.Company.CompanyLogin" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link href="../Styles/Company.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <form id="form1" runat="server">
    <div class="CompanyLogin" style="background-color: #CDE7FD;" >
    <table>
    <tr>
    <td></td>
    <td>
      <h3 style="font-size:x-large; font-family:Calibri; color:#333300;">Company login</h3>
    </td>
    <td></td>
    </tr>
     <tr>
    <td>
        <asp:Label ID="lblUserName" runat="server" Text="Username"></asp:Label>
    </td>
    <td>
        <asp:TextBox ID="txtUserName" runat="server" ValidationGroup="0"  CssClass="txtLoginClass"></asp:TextBox>
    </td>
    <td></td>
    </tr>
    <tr>
    <td></td>
    <td>
        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" 
            ControlToValidate="txtUserName" ErrorMessage="Enter username." ForeColor="Red" 
            ValidationGroup="0"></asp:RequiredFieldValidator>
        </td>
    <td></td>
    </tr>
      <tr>
    <td>
        <asp:Label ID="lblPassword" runat="server" Text="Password"></asp:Label>
    </td>
    <td>
        <asp:TextBox ID="txtPassword" runat="server" TextMode="Password"   CssClass="txtLoginClass"
            ValidationGroup="0"></asp:TextBox>
    </td>
    <td></td>
    </tr>
      <tr>
    <td></td>
    <td>
        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" 
            ControlToValidate="txtPassword" ErrorMessage="Enter password." ForeColor="Red" 
            ValidationGroup="0"></asp:RequiredFieldValidator>
          </td>
    <td></td>
    </tr>
     <tr>
    <td></td>
    <td>
    <%--    <asp:Button ID="btnLogin" runat="server" Text="Login" CssClass="btnLoginClass" 
            onclick="btnLogin_Click" ValidationGroup="0" />--%>
               <asp:ImageButton ID="ImgBtnLogin" runat="server" ImageUrl="~/images/loginimg.jpg"
            Height="40px"  Width="134px" onclick="ImgBtnLogin_Click" />
            </td>
    <td></td>
    </tr>
     <tr>
    <td>&nbsp;</td>
    <td>
        <asp:Label ID="lblmessage" runat="server" ForeColor="Red" Font-Italic="True" ></asp:Label>
         </td>
    <td>&nbsp;</td>
    </tr>
    </table>
    </div>
    </form>
</body>
</html>
