﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Company/Company.Master" AutoEventWireup="true" CodeBehind="AddEmployee.aspx.cs" Inherits="HealToZeal.Company.AddEmployee" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .style6
        {
            width: 125px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="LeftColumn" runat="server">
    <div class="divClass" style="width:100%">

    <cc1:TabContainer ID="TabContainerEmployees" runat="server" ActiveTabIndex="0"  
            Width="100%"  BackColor="#f0f0f0"  >
    <cc1:TabPanel ID="TabPnlViewEmployee" runat="server" HeaderText="View Employees" BackColor="#f0f0f0">
         <ContentTemplate>
         <div class="divClass" >
         <asp:GridView ID="GvViewEmployee" runat="server" AutoGenerateColumns="False"  Width="100%"
                 CellPadding="6" CellSpacing="6">
             <AlternatingRowStyle BackColor="#CDFECD" />
             <HeaderStyle BackColor="SkyBlue" />
         <Columns>
            <%--<asp:TemplateField HeaderText="SrNo">
             <HeaderStyle BorderStyle="None"  />
         <ItemStyle BorderStyle="None" />
        <ItemTemplate>
         <table> 
         <tr>
         <td style="width:50px">--%>
         <%--<asp:Label ID="lblSrNo" runat="server" Text='<%#Eval("SrNo") %>'></asp:Label>--%>
         <%--  </td>
       <td><asp:Label ID="lblEmployeeId" runat="server" Text='<%#Eval("EmployeeId_PK") %>' Visible="false"></asp:Label></td>
         <td><asp:Label ID="lblCompanyId_FK" runat="server" Text='<%#Eval("CompanyId_FK") %>' Visible="false" ></asp:Label></td>
         <td style="width:150px"><asp:Label ID="lblEmployeeCompanyId" runat="server" Text='<%#Eval("Company") %>'></asp:Label></td>
         <td style="width:150px"><asp:Label ID="lblEmployeeName" runat="server" Text='<%#Eval("EmployeeName") %>'></asp:Label></td>
         <td style="width:200px"><asp:Label ID="lblEmployeeAddress" runat="server" Text='<%#Eval("EmployeeAddress") %>'></asp:Label></td>
         <td  style="width:200px"><asp:Label ID="lblEmailId" runat="server" Text='<%#Eval("EmailId") %>'></asp:Label></td>
         <td style="width:100px"><asp:Label ID="lblMobileNo" runat="server" Text='<%#Eval("MobileNo") %>'></asp:Label></td>
         <td ><asp:Button ID="btnEdit" runat="server" Text="Edit" CommandArgument='<%#Eval("EmployeeId_PK") %>' Visible="false" CommandName="EditEmployee" /></td>
         </tr>
         </table>
         </ItemTemplate>
         </asp:TemplateField> --%>         
               
           <asp:TemplateField HeaderText="CompanyId">
               <HeaderStyle BorderStyle="None" />
             <ItemStyle BorderStyle="None" />
          <ItemTemplate>
             <asp:Label ID="lblEmployeeCompanyId" runat="server" Text='<%#Eval("Company") %>'>
             </asp:Label>
          </ItemTemplate>
          </asp:TemplateField >
           <asp:TemplateField HeaderText="Name">
               <HeaderStyle BorderStyle="None" />
             <ItemStyle BorderStyle="None" />
          <ItemTemplate>
            <asp:Label ID="lblEmployeeName" runat="server" Text='<%#Eval("EmployeeName") %>'>
             </asp:Label>
          </ItemTemplate>
          </asp:TemplateField>
           <asp:TemplateField HeaderText="Address">
               <HeaderStyle BorderStyle="None" />
             <ItemStyle BorderStyle="None" />
          <ItemTemplate>
            <asp:Label ID="lblEmployeeAddress" runat="server" Text='<%#Eval("EmployeeAddress") %>'>
             </asp:Label>
          </ItemTemplate>
          </asp:TemplateField>
            <asp:TemplateField HeaderText="Email id">
                <HeaderStyle BorderStyle="None" />
              <ItemStyle BorderStyle="None" />
          <ItemTemplate>
           <asp:Label ID="lblEmailId" runat="server" Text='<%#Eval("EmailId") %>'>
             </asp:Label>
          </ItemTemplate>
          </asp:TemplateField>
            <asp:TemplateField HeaderText="Mobile no.">
                <HeaderStyle BorderStyle="None" />
              <ItemStyle BorderStyle="None" />
          <ItemTemplate>
         <asp:Label ID="lblMobileNo" runat="server" Text='<%#Eval("MobileNo") %>'>
             </asp:Label>
          </ItemTemplate>
          </asp:TemplateField>
          <asp:TemplateField >
               <HeaderStyle BorderStyle="None" />
             <ItemStyle BorderStyle="None" />
          <ItemTemplate>
            <asp:Label ID="lblEmployeeId" runat="server" Text='<%#Eval("EmployeeId_PK") %>' Visible="false"></asp:Label>
          </ItemTemplate>
          </asp:TemplateField> 
           <asp:TemplateField>
               <HeaderStyle BorderStyle="None" />
             <ItemStyle BorderStyle="None" />
          <ItemTemplate>
            <asp:Label ID="lblCompanyId_FK" runat="server" Text='<%#Eval("CompanyId_FK") %>' Visible="false" ></asp:Label>
          </ItemTemplate>
          </asp:TemplateField>
             <asp:TemplateField>
                 <HeaderStyle BorderStyle="None" />
               <ItemStyle BorderStyle="None" />
          <ItemTemplate>
       <asp:Button ID="btnEdit" runat="server" Text="Edit" CommandArgument='<%#Eval("EmployeeId_PK") %>' Visible="false" CommandName="EditEmployee" />
          </ItemTemplate>
          </asp:TemplateField>
         </Columns>
         </asp:GridView>
         </div>
         </ContentTemplate>
    </cc1:TabPanel>

    <cc1:TabPanel ID="TabPnlAddUpdateEmployee" runat="server" HeaderText="Add/Update Employees" Width="100%" BackColor="#f0f0f0">
          <ContentTemplate>
          <div  class="divClass">
          <table width="100%" >
          <tr><td colspan="2" align="center" >
              <asp:Label ID="lblTitle" runat="server" Font-Size="X-Large" ForeColor="#333300" 
                  Text=" Employee Profile"></asp:Label>
              </td>
              <td></td>
              <td></td>
          </tr>
          <tr>
          <td align="right" class="style6">
          <asp:Label ID="lblCompanydomain" runat="server" Text="Cadre"></asp:Label>
          </td>
          <td><asp:DropDownList ID="ddlCadre" runat="server" ValidationGroup="0" CssClass="txtLoginClass"></asp:DropDownList></td>
          <td >&nbsp;</td>
          <td>&nbsp;</td>
          </tr>
          <tr>
          <td></td>
                  <td >&nbsp;</td>
          <td>&nbsp;</td>
          <td></td>
          </tr>
          <tr>
          <td align="right" class="style6">
          <asp:Label ID="lblCompanyAddress" runat="server" Text="Employee Id"></asp:Label>
          </td>
          <td >
          <asp:TextBox ID="txtEmployeeCompanyId" runat="server" ValidationGroup="0" CssClass="txtLoginClass"></asp:TextBox>
          </td>
          <td >
              &nbsp;</td>
          <td>&nbsp;</td>
          </tr>
          <tr>
          <td class="style6"></td>
          <td>
              <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" 
                  ControlToValidate="txtEmployeeCompanyId" ErrorMessage="Enter Employee Id." 
                  ForeColor="Red" ValidationGroup="0"></asp:RequiredFieldValidator>
              </td>
          <td></td>
          <td></td>
          </tr>
          <tr>
          <td align="right" class="style6"><asp:Label ID="lblFirstName" runat="server" Text="First name"></asp:Label></td>
          <td ><asp:TextBox ID="txtFirstName" runat="server" ValidationGroup="0" CssClass="txtLoginClass"></asp:TextBox></td>
          <td>
              &nbsp;</td>
          <td>&nbsp;</td>
          </tr>
            <tr>
          <td class="style6"></td>
           <td>
               <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" 
                   ControlToValidate="txtFirstName" ErrorMessage="Enter first name." 
                   ForeColor="Red" ValidationGroup="0"></asp:RequiredFieldValidator>
                </td>
            <td></td>
             <td></td>
          </tr> 
          <tr>
          <td align="right" class="style6"><asp:Label ID="lblLastName" runat="server" Text="Last name"></asp:Label></td>
          <td ><asp:TextBox ID="txtLastName" runat="server" ValidationGroup="0" CssClass="txtLoginClass"></asp:TextBox></td>
          <td >
              &nbsp;</td><td>&nbsp;</td>
          </tr>
           <tr>
          <td class="style6"></td>
           <td>
               <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" 
                   ControlToValidate="txtLastName" ErrorMessage="Enter last name" ForeColor="Red" 
                   ValidationGroup="0"></asp:RequiredFieldValidator>
               </td>
            <td></td>
             <td></td>
          </tr> 
          <tr>
          <td align="right" class="style6"><asp:Label ID="lblEmployeeEmailId" runat="server" Text="Email id"></asp:Label></td>
          <td ><asp:TextBox ID="txtEmailId" runat="server" ValidationGroup="0" CssClass="txtLoginClass"></asp:TextBox></td>
          <td >
              <br />
              </td>
          </tr>
           <tr>
          <td class="style6"></td>
           <td>
               <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" 
                   ControlToValidate="txtEmailId" ErrorMessage="Enter valid email id." 
                   ForeColor="Red" 
                   ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" 
                   ValidationGroup="0"></asp:RegularExpressionValidator>
               <br />
               <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" 
                   ControlToValidate="txtEmailId" ErrorMessage="Enter email id" ForeColor="Red" 
                   ValidationGroup="0"></asp:RequiredFieldValidator>
               </td>
            <td></td>
             <td></td>
          </tr> 
          <tr>
          <td class="style6">&nbsp;</td>
          <td >
          <asp:Button ID="btnAddEmployee" runat="server" Text="Add employee"  CssClass="btnLoginClass"
                        onclick="btnAddEmployee_Click" ValidationGroup="0"  BackColor="#CDFECD" Font-Bold="True" 
                        Font-Names="Calibri" Font-Size="Large" /></td>
                        <td >&nbsp;</td>
                        <td>&nbsp;</td>
                        </tr>
                        <tr>
                        <td class="style6">&nbsp;</td>
                        <td ><asp:Label ID="lblMessage" runat="server" ForeColor="Red"></asp:Label></td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        </tr>
                        </table>
                        </div>
                        </ContentTemplate>
    </cc1:TabPanel>
    </cc1:TabContainer>
</div>
</asp:Content>
