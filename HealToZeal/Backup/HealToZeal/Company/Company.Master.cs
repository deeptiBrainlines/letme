﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace HealToZeal.Company
{
    public partial class Company : System.Web.UI.MasterPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["CompanyName"] != null)
            {
             //   Menu1.FindItem("User").Text = Session["CompanyName"].ToString();
                lblusername.Text = "Welcome " + Session["CompanyName"].ToString();
            }
        }

        protected void lbLogout_Click(object sender, EventArgs e)
        {
            try
            {
                Session.Clear();
                Session.Abandon();
                Response.Redirect("CompanyLogin.aspx");
            }
            catch 
            {
            }
          
        }
    }
}