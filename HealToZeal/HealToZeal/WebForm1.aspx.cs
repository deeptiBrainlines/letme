﻿using System.Web;
using System.Data;
using System.IO;
using iTextSharp.text.pdf;
using System.Text;
using iTextSharp.text;
using System;
using System.Web.UI.DataVisualization.Charting;
using iTextSharp.text.html.simpleparser;

namespace HealToZeal
{
    public partial class WebForm1 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //FillData();

        }

        protected void btnexport_Click(object sender, EventArgs e)
        {


        }


        protected void ExportToImage(object sender, EventArgs e)
        {

            

            string base64 = Request.Form[hfImageData.UniqueID].Split(',')[1];
            byte[] bytes = Convert.FromBase64String(base64);
            Response.Clear();
            Response.ContentType = "image/jpeg";
            Response.AddHeader("Content-Disposition", "attachment; filename=HTML.jpg");
            Response.Buffer = true;
            Response.Cache.SetCacheability(HttpCacheability.NoCache);
            Response.BinaryWrite(bytes);
            Response.End();
        }

        //public void exporttopdf()
        //{
        //    DataTable dtresult = new DataTable();

        //    if (dtresult.Columns.Count == 0)
        //    {
        //        // dtresult.Columns.Add("EmployeeID_PK");
        //        dtresult.Columns.Add("Sr No");
        //        dtresult.Columns.Add("Self Assurance");
        //        dtresult.Columns.Add("Emotional Balance");
        //        dtresult.Columns.Add("Ability to Withstand Pressure");

        //        dtresult.Columns.Add("Lack of Motivation");
        //        dtresult.Columns.Add("Coping Issues");
        //        dtresult.Columns.Add("Self Esteem Issues");
        //    }
        //    dtresult.Rows.Add(199, 70, 60, 40, 5, 6, 7);


        //    StringBuilder sb = new StringBuilder();
        //    sb.Append("<html><body><h3>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>My Mindfulness Report</b></h3></body></html>");

        //    //sb.Append("<html><body><h6>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;nbsp;&nbsp;" + DateTime.Now.ToString("dd/mm/yyyy")+"</h6></body></html>");


        //    string s ="<html><body><br/><font size=2>Roll No "+dtresult.Rows[0][0]+"</font></body></html>";
        //    sb.Append(s);

        //    sb.Append("<html><body>Graph</html>");

        //    sb.Append("<html><body><br/><br/><br/></html>");

        //    sb.Append("<html><body><font size=2><br/> If the bullet is showing green, then you are doing well, if it is yellow you need to take appropriate action.</body></html>");

        //    //string img = ("C:\\09-12-2017\\HealToZeal\\HealToZeal\\images\\IMG_20150406_130816.jpg");
        //    //sb.Append(img);

        //    if (Convert.ToInt32(dtresult.Rows[0][1]) < 35)
        //    {
        //        string s1 = "<html><body><br/><font size=2>At present, you seem to be coping well with the situation. But it is still recommeded to be observant/vigilant about coping with changes or challenges that you may come along the way in the future.</font></body></html>";
        //        sb.Append(s1);

        //    }

        //    if (Convert.ToInt32(dtresult.Rows[0][2]) < 40)
        //    {

        //        string s2 = "<html><body><br/><font size=2>Your responses tell us that you are doing a good job maintaining your emotional balance. Things in your life seem to be generally stable, and you seem to be able to handle them well. You seem to be able to enjoy happy moments in your life, appropriately coping with unpleasant situations. We also see a good level of energy and enthusiasm in you, to counter discouragements if any.</ font></body></html>";
        //        sb.Append(s2);

        //    }
        //    if (Convert.ToInt32(dtresult.Rows[0][3]) < 40)
        //    {

        //        string s3 = "<html><body><br/><font size=2>You seem to be in good control of situations/work. This will definitely give you a good sense of satisfaction. This must certainly be helping you to be content.</font></body></html>";
        //        sb.Append(s3);
        //    }

        //    if (Convert.ToInt32(dtresult.Rows[0][1]) >= 35 && Convert.ToInt32(dtresult.Rows[0][1]) <= 69)
        //    {
        //        string s1 = "<html><body><br/><font size=2>Your responses tell us that you may be worrying a bit more than can be of any help. You may be worrying a lot even on trivial matters. Your confidence is likely to be plunging, hampering your decision making process. It is possible, that even daily routine may be taking an emotional toll on you. It will be a good idea if you choose to use the support structure available.</font></body></html>";
        //        sb.Append(s1);

        //    }
        //    if (Convert.ToInt32(dtresult.Rows[0][1]) >= 70)
        //    {
        //        string s6 = "<html><body><br/><font size=2>From your responses, it seems that worries and fears take a grip on you at times. Though they may not affect you big-time and you may still succeed putting up an image as a confident, balanced person; it is likely that this pressure is making you feel low. Your hidden strengths are not being used to their full potential. It is advisable to use the support structure available.</font></body></html>";
        //        sb.Append(s6);

        //    }
        //    //Emotional Balance

        //    if ((Convert.ToInt32(dtresult.Rows[0][2]) >= 40) && Convert.ToInt32(dtresult.Rows[0][2]) <= 69)
        //    {
        //        string s7 = "<html><body><br/><font size=2>Your responses tell us that your mood is dependent on the situations around you, which you may not be control. Also, it appears that you may have inhibitions in seeking help.s…you are the best judge to say what is the case. But either of these can make it tough to handle academic and personal challenges. We also see a possibility that your sleep is not adequately refreshing you. Do you also think these days you have to struggle to keep up your confidence and to get rid of the guilt? Your responses tell us that you probably are. A little  help can ease up your struggle. Do consider using the support network.</font></body></html>";
        //        sb.Append(s7);


        //    }
        //    if ((Convert.ToInt32(dtresult.Rows[0][2]) >= 70))
        //    {
        //        string s8 = "<html><body><br/><font size=2>Does it happen that you are cheerful at one time and unnecessarily low at some other times? It seems that you still have kept your work unaffected, making it due justice. But do you think your effectiveness fluctuates? If yes, please keep an eye on your self, observe the frequency and the triggers. That would help you stabilize a bit and a feeling of inner </font></body></html>";
        //        sb.Append(s8);

        //    }

        //    //Pressure


        //    if ((Convert.ToInt32(dtresult.Rows[0][3]) >= 40) && Convert.ToInt32(dtresult.Rows[0][3]) <= 69)
        //    {
        //        string s1 = "<html><body><br/><font size=2>Your responses suggest that you are currently functioning in an environment which you perceive to be highly stressful. It is likely that your responsibilities and tasks are burdening you…. may be you feel it’s getting more and more difficult to handle things… Now, that can really be stressful.  May be you would like to meet someone from available support structure? Sometimes it is really required and will be useful.</font></body></html>";
        //        sb.Append(s1);

        //    }
        //    if (Convert.ToInt32(dtresult.Rows[0][3]) > 70)
        //    {
        //        string s1 = "<html><body><br/><font size=2>Your responses suggest that you have started feeling stressful and low. At times, your responsibilities are likely to burden you, and may overwhelm you. So far it seems you may be managing despite this stress. However, we are sure that you would like to feel less stressed and enjoy the well deserved student life. We therefore suggest you consider the need to do something about your stress.</font></body></html>";
        //        sb.Append(s1);


        //    }

        //    if (Convert.ToInt32(dtresult.Rows[0][4]) < 5)// || Convert.ToInt32(lblselfesteem.Text)>5 || Convert.ToInt32(lblCoping.Text)>0)
        //    {
        //        string s1 = "<html><body><br/><font size=2>Utilizing the opportunity keeping with academic commitments that keeps you relaxed.</ font></body></html>";
        //        sb.Append(s1);

        //    }
        //    else
        //    {
        //        string s1 = "<html><body><br/><font size=2>You have not been able to keep up with academic commitments.This  may be stopping you from expoloring avaliable opportunities around the campus</ font></body></html>";
        //        sb.Append(s1);


        //    }

        //    if (Convert.ToInt32(dtresult.Rows[0][5]) < 5)
        //    {
        //        string s1 = "<html><body><br/><font size=2>All your reationships give you comfort(seem to be giving you) which becomes your strength</ font></body></html>";
        //        sb.Append(s1);

        //    }
        //    else
        //    {
        //        string s1 = "<html><body><br/><font size=2>You seem to facing chanllanges handling your relationships they may become detrimental your performance.</ font></body></html>";
        //        sb.Append(s1);


        //    }

        //    if (Convert.ToInt32(dtresult.Rows[0][6]) < 5)
        //    {
        //        string s1 = "<html><body><br/><font size=2>You seem to be feeling confident & content about yourself so you are performing well</font></body></html>";
        //        sb.Append(s1);

        //    }
        //    else
        //    {
        //        string s1 = "<html><body><br/><font size=2>Compared to others.Hence you may be feeling low and pessimisitice anxious.</ font></body></html>";
        //        sb.Append(s1);

        //    }



        //    sb.Append("<html><body><font size=2><br/><b>Contact for Appointment</b></font></body></html>");

        //    sb.Append("<html><body><font size=2><br/>Call <b>91-9850864727</b> or please write an email- <b>healtozeal@chetanaTSPL.com</b> with your contact number. We will get back to you soon.</font></body></html>");


        //    sb.Append("<html><body><font size=2><br/><br/><b>This report has been prepared in good faith on the basis of information available and individual responses received at the date of assessment without any independent verification. Chetana TSPL does not guarantee or warrant the accuracy, reliability, completeness or currency of the information in this report nor its usefulness in achieving any purpose. Readers are responsible for assessing the relevance and accuracy of the content of this report. Chetana TSPL will not be liable for any loss, damage, cost or expense incurred or arising by reason of any person using or relying on information in this report.This is not a diagnosis of individual's mindfulness but just a effort towards Self Discovery. You may want to use it as a direction to go deeper in the mindefulness aspect with professional help if you wish to.</b></font></body></html>");



        //    StringReader sr = new StringReader(sb.ToString());

        //    Document pdfDoc = new Document(PageSize.A4, 10f, 10f, 10f, 0f);
        //    HTMLWorker htmlparser = new HTMLWorker(pdfDoc);
        //    using (MemoryStream memoryStream = new MemoryStream())
        //    {
        //        PdfWriter writer = PdfWriter.GetInstance(pdfDoc, memoryStream);
        //        pdfDoc.Open();

        //        htmlparser.Parse(sr);
        //        pdfDoc.Close();

        //        byte[] bytes = memoryStream.ToArray();
        //        memoryStream.Close();


        //        // Clears all content output from the buffer stream                
        //        Response.Clear();
        //        // Gets or sets the HTTP MIME type of the output stream.
        //        Response.ContentType = "application/pdf";
        //        // Adds an HTTP header to the output stream
        //        int i = 0;
        //        string filename="Report" + i;
        //        Response.AddHeader("Content-Disposition", "attachment; filename="+filename+".pdf");

        //        //Gets or sets a value indicating whether to buffer output and send it after
        //        // the complete response is finished processing.
        //        Response.Buffer = true;
        //        // Sets the Cache-Control header to one of the values of System.Web.HttpCacheability.
        //        Response.Cache.SetCacheability(HttpCacheability.NoCache);
        //        // Writes a string of binary characters to the HTTP output stream. it write the generated bytes .
        //        Response.BinaryWrite(bytes);
        //        // Sends all currently buffered output to the client, stops execution of the
        //        // page, and raises the System.Web.HttpApplication.EndRequest event.

        //        Response.End();
        //        // HttpContext.Current.ApplicationInstance.CompleteRequest();
        //        //  Closes the socket connection to a client. it is a necessary step as you must close the response after doing work.its best approach.
        //        // Response.Close();



        //    }
        //}

        //private void FillData()
        //{
        //    double plotY;
        //    double plotY2 = 200.0;
        //    if (Chart1.Series["Series1"].Points.Count > 0)
        //    {
        //        plotY = Chart1.Series["Series1"].Points[Chart1.Series["Series1"].Points.Count - 1].YValues[0];

        //    }
        //    Random random = new Random();
        //    for (int i = 0; i < 10; i++)
        //    {
        //        plotY = 10;
        //        Chart1.Series["Series1"].Points.AddY(plotY);


        //    }
        //}



    }
}