﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="MoodGraphTest.aspx.cs" Inherits="HealToZeal.MoodGraphTest" %>

<%@ Register assembly="Microsoft.ReportViewer.WebForms, Version=11.0.0.0, Culture=neutral, PublicKeyToken=89845dcd8080cc91" namespace="Microsoft.Reporting.WebForms" tagprefix="rsweb" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>

        <rsweb:ReportViewer ID="ReportViewer1" runat="server" Font-Names="Verdana" Font-Size="8pt" WaitMessageFont-Names="Verdana" WaitMessageFont-Size="14pt">
            <LocalReport ReportPath="Reports\InstancePercentage.rdlc">
                <DataSources>
                    <rsweb:ReportDataSource DataSourceId="ObjectDataSource1" Name="DataSet1" />
                </DataSources>
            </LocalReport>
        </rsweb:ReportViewer>
        <asp:ObjectDataSource ID="ObjectDataSource1" runat="server" OldValuesParameterFormatString="original_{0}" SelectMethod="GetData" TypeName="HealToZeal.AssessmentPercentageTableAdapters.SpClusterPercentageTableAdapter">
            <SelectParameters>
                <asp:Parameter DbType="Guid" DefaultValue="d095361c-464e-4751-9a6b-8fbf348e88b8" Name="CompanyId" />
                <asp:Parameter DbType="Guid" DefaultValue="6334946B-09A7-43D2-AF44-1B689FE267E1" Name="AssessmentInstanceId" />
            </SelectParameters>
        </asp:ObjectDataSource>

    </div>
    </form>
</body>
</html>
