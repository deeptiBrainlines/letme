﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="WebForm1.aspx.cs" Inherits="HealToZeal.WebForm1" %>

<!DOCTYPE html>
<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=11.0.0.0, Culture=neutral, PublicKeyToken=89845dcd8080cc91" Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>

<%@ Register Assembly="System.Web.DataVisualization, Version=3.5.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" Namespace="System.Web.UI.DataVisualization.Charting" TagPrefix="asp" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
    <script>
//    window.onload = function () {

//var chart = new CanvasJS.Chart("chartContainer", {
//	animationEnabled: true,
//	theme: "light2",
//	title:{
//		text: "My Wellness  Graph"
//	},
//	axisY:{
//		includeZero: true
//	},
//	data: [{        
//		type: "line",       
//		dataPoints: [
//			{ y: 67, indexLabel: "Self Assurance", markerColor: "yellow" },
//			{ y: 67, indexLabel: "Emotional Balance", markerColor: "yellow" },
//			{ y: 82, indexLabel: "Pressure", markerColor: "red" },

//            { y: 73, indexLabel: "Goal clarity", markerColor: "yellow" },
//            { y: 77, indexLabel: "Lack of Motivation", markerColor: "yellow" },
//			{ y: 77, indexLabel: "Insight", markerColor: "yellow" },
			
//            { y: 66, indexLabel: "Coping Issues", markerColor: "yellow" },
//			{ y: 68, indexLabel: "Self Esteem Issues", markerColor: "yellow" },
//            { y: 70, indexLabel: "Pracrastination", markerColor: "yellow" },
//			{ y: 65, indexLabel: "Acceptance of facts", markerColor: "yellow" },
            

//		]
//	}]
//});
//chart.render();

        //    }
        window.onload = function () {
            var chart = new CanvasJS.Chart("chartContainer",
            {
                title: {
                    text: "My Wellness Graph"
                },
                axisY: {
                    title: "Cluster",
                    
                },
                axisY: {
                    title: "Score",
                    maximum: 100
                },
                data: [
                {
                    type: "line",
                    showInLegend: true,
                    legendText: "Date :Sept 2017",
                    color: "blue",
                    dataPoints: [
                 // 13411
            //{ y: 67, label: "Self Assurance", markerColor: "green" },
			//{ y: 71, label: "Emotional Balance", markerColor: "green" },
			//{ y: 64, label: "Ability to withstand Pressure", markerColor: "green" },
            //{ y: 68, label: "Motivation", markerColor: "green" },
            //{ y: 60, label: "Goal clarity", markerColor: "green" },
            
			//{ y: 60, label: "Insight", markerColor: "green" },

            //{ y: 80, label: "Coping", markerColor: "green" },
			//{ y: 80, label: "Self Esteem", markerColor: "green" },
            //{ y: 78, label: "Pracrastination", markerColor: "green" },
			//{ y: 80, label: "Acceptance of facts", markerColor: "green" },

            ////13362
            // { y: 54, label: "Self Assurance", markerColor: "yellow" },
			//{ y: 58, label: "Emotional Balance", markerColor: "yellow" },
			//{ y: 59, label: "Ability to withstand Pressure", markerColor: "yellow" },
            //{ y: 58, label: "Motivation", markerColor: "yellow" },
            //{ y: 58, label: "Goal clarity", markerColor: "yellow" },

			//{ y: 57, label: "Insight", markerColor: "yellow" },

            //{ y: 63, label: "Coping", markerColor: "green" },
			//{ y: 58, label: "Self Esteem", markerColor: "yellow" },
            //{ y: 57, label: "Pracrastination", markerColor: "yellow" },
			//{ y: 61, label: "Acceptance of facts", markerColor: "green" },

             //12933
            // { y: 46, label: "Self Assurance", markerColor: "yellow" },
			//{ y: 46, label: "Emotional Balance", markerColor: "yellow" },
			//{ y: 42, label: "Ability to withstand Pressure", markerColor: "yellow" },
            //{ y: 35, label: "Motivation", markerColor: "yellow" },
            //{ y: 37, label: "Goal clarity", markerColor: "yellow" },

			//{ y: 38, label: "Insight", markerColor: "yellow" },

            //{ y: 55, label: "Coping", markerColor: "yellow" },
			//{ y: 45, label: "Self Esteem", markerColor: "yellow" },
            //{ y: 45, label: "Pracrastination", markerColor: "yellow" },
			//{ y: 42, label: "Acceptance of facts", markerColor: "yellow" },

            //13160
            // { y: 33, label: "Self Assurance", markerColor: "yellow" },
			//{ y: 33, label: "Emotional Balance", markerColor: "yellow" },
			//{ y: 18, label: "Ability to withstand Pressure", markerColor: "red" },
            //{ y: 21, label: "Motivation", markerColor: "yellow" },
            //{ y: 20, label: "Goal clarity", markerColor: "yellow" },

			//{ y: 38, label: "Insight", markerColor: "yellow" },

            //{ y: 35, label: "Coping", markerColor: "yellow" },
			//{ y: 30, label: "Self Esteem", markerColor: "yellow" },
            //{ y: 32, label: "Pracrastination", markerColor: "yellow" },
			//{ y: 32, label: "Acceptance of facts", markerColor: "yellow" },


            //12897
            // { y: 35, label: "Self Assurance", markerColor: "yellow" },
			//{ y: 25, label: "Emotional Balance", markerColor: "red" },
			//{ y: 32, label: "Ability to withstand Pressure", markerColor: "yellow" },
            //{ y: 39, label: "Motivation", markerColor: "yellow" },
            //{ y: 40, label: "Goal clarity", markerColor: "yellow" },

			//{ y: 39, label: "Insight", markerColor: "yellow" },

            //{ y: 35, label: "Coping", markerColor: "yellow" },
			//{ y: 38, label: "Self Esteem", markerColor: "yellow" },
            //{ y: 38, label: "Pracrastination", markerColor: "yellow" },
			//{ y: 37, label: "Acceptance of facts", markerColor: "yellow" },

            ////12944
            //  { y: 35, label: "Self Assurance", markerColor: "yellow" },
			//{ y: 50, label: "Emotional Balance", markerColor: "yellow" },
			//{ y: 27, label: "Ability to withstand Pressure", markerColor: "red" },
            //{ y: 50, label: "Motivation", markerColor: "yellow" },
            //{ y: 49, label: "Goal clarity", markerColor: "yellow" },

			//{ y: 48, label: "Insight", markerColor: "yellow" },

            //{ y: 40, label: "Coping", markerColor: "yellow" },
			//{ y: 40, label: "Self Esteem", markerColor: "yellow" },
            //{ y: 39, label: "Pracrastination", markerColor: "yellow" },
			//{ y: 40, label: "Acceptance of facts", markerColor: "yellow" },


            //  //12777
            //  { y: 63, label: "Self Assurance", markerColor: "green" },
			//{ y: 58, label: "Emotional Balance", markerColor: "yellow" },
			//{ y: 73, label: "Ability to withstand Pressure", markerColor: "green" },
            //{ y: 60, label: "Motivation", markerColor: "green" },
            //{ y: 59, label: "Goal clarity", markerColor: "green" },

			//{ y: 60, label: "Insight", markerColor: "green" },

            //{ y: 67, label: "Coping", markerColor: "green" },
			//{ y: 67, label: "Self Esteem", markerColor: "green" },
            //{ y: 68, label: "Pracrastination", markerColor: "green" },
			//{ y: 69, label: "Acceptance of facts", markerColor: "green" },


              //12688
            { y: 4, label: "Self Assurance", markerColor: "red" },
			{ y: 21, label: "Emotional Balance", markerColor: "red" },
			{ y: 0, label: "Ability to withstand Pressure", markerColor: "red" },
            { y: 25, label: "Motivation", markerColor: "yellow" },
            { y: 23, label: "Goal clarity", markerColor: "yellow" },

			{ y: 24, label: "Insight", markerColor: "yellow" },

            { y: 24, label: "Coping", markerColor: "yellow" },
			{ y: 18, label: "Self Esteem", markerColor: "yellow" },
            { y: 16, label: "Pracrastination", markerColor: "yellow" },
			{ y: 19, label: "Acceptance of facts", markerColor: "yellow" },





                    ]
                },


                ]
            });

            chart.render();
        }
   </script>
   

<script src="js/html2canvas.js"></script>
<script src="js/jquery-1.11.0.min.js"></script>

<script type="text/javascript">
    function ConvertToImage(btnExport) {
        html2canvas($("#dvTable")[0]).then(function (canvas) {
            var base64 = canvas.toDataURL();
            $("[id*=hfImageData]").val(base64);
            __doPostBack(btnExport.name, "");
        });
        return false;
    }
</script>

<body>
    <form id="form1" runat="server">

        
        <div id="dvTable" style="border:none;height: 400px; max-width: 1020px; margin: 0px auto;" >
      
<br />
<div id="chartContainer" style="height: 370px; max-width: 1020px; margin: 0px auto;"></div>
            <script src="js/canvasjs.min.js"></script>
            </div>
<br />
 <asp:HiddenField ID="hfImageData" runat="server" />
<asp:Button ID="btnExport" Text="Export to Image" runat="server" UseSubmitBehavior="false"
    OnClick="ExportToImage" OnClientClick="return ConvertToImage(this)" />
 
    </form>
</body>
</html>
