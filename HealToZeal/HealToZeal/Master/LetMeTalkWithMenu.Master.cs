﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace HealToZeal.Master
{
    public partial class LetMeTalkWithMenu : System.Web.UI.MasterPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            char[] delimiterChars = { ' ' };
            if (Session["UserFullName"] !=null && Convert.ToString(Session["UserFullName"]) != "")
            {
                string userName = Convert.ToString(Session["UserFullName"]);
                string[] strings = userName.Split(delimiterChars);
               
                lblUserName.Text = Convert.ToString(strings[0]);
            }
            if (Session["Heading"] != null && Convert.ToString(Session["Heading"]) != "")
            {
                lblHeading.Text = Convert.ToString(Session["Heading"]);
                lblHeading1.Text = Convert.ToString(Session["Heading"]);
            }
            else
            {
                lblHeading.Text = "";
                lblHeading1.Text = "";
            }
        }
    }
}