﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Facebook.aspx.cs" Inherits="HealToZeal.Facebook" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <script>
        window.fbAsyncInit = function () {
            FB.init({
                appId: '682470882264872',
                cookie: true,
                xfbml: true,
                version: 'v4.0'
            });

            FB.AppEvents.logPageView();

        };

        function init() {
            FB.api(
              '/l214.animaux',
              { "fields": "fan_count" },
              function (response) {
                  alert(response.fan_count);
              }
            );
        }

        (function (d, s, id) {
            var js, fjs = d.getElementsByTagName(s)[0];
            if (d.getElementById(id)) { return; }
            js = d.createElement(s); js.id = id;
            js.src = "https://connect.facebook.net/en_US/sdk.js";
            fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));

        FB.getLoginStatus(function (response) {
            statusChangeCallback(response);
        });

        function checkLoginState() {
            FB.getLoginStatus(function (response) {
                statusChangeCallback(response);
            });
        }

    </script>

</head>
<body>
    <form id="form1" runat="server">
    <div>
       <fb:login-button
                scope="public_profile,email" onlogin="checkLoginState();">
            </fb:login-button>
    </div>
    </form>
</body>
</html>
