﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="AddCompany.aspx.cs" MasterPageFile="~/Admin/Admin.Master" Inherits="HealToZeal.Admin.AddCompany" %>


<asp:Content ContentPlaceHolderID="LeftColumn" runat="server">
<link href="../Styles/Admin.css" rel="stylesheet" type="text/css" />
    <div>
    
        <table class="style1">
            <tr>
                <td>
                    &nbsp;</td>
                <td>
                    <asp:Label ID="lblTitle" runat="server" Text="Add company"></asp:Label>
                </td>
                <td>
                    &nbsp;</td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="lblCompanyName" runat="server" Text="Company name"></asp:Label>
                </td>
                <td>
                    <asp:TextBox ID="txtCompanyName" runat="server"></asp:TextBox>
                </td>
                <td>
                    &nbsp;</td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="lblCompanydomain" runat="server" Text="Domain"></asp:Label>
                </td>
                <td>
                    <asp:DropDownList ID="ddlDomain" runat="server">
                    </asp:DropDownList>
                </td>
                <td>
                    &nbsp;</td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="lblNoOfEmployees" runat="server" Text="Number of Employees"></asp:Label>
                </td>
                <td>
                    <asp:TextBox ID="txtNoOfEmployees" runat="server"></asp:TextBox>
                </td>
                <td>
                    &nbsp;</td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="lblCompanyAddress" runat="server" Text="Address"></asp:Label>
                </td>
                <td>
                    <asp:TextBox ID="txtAddress" runat="server"></asp:TextBox>
                </td>
                <td>
                    &nbsp;</td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="lblContactNo" runat="server" Text="Contact number"></asp:Label>
                </td>
                <td>
                    <asp:TextBox ID="txtContactNo" runat="server"></asp:TextBox>
                </td>
                <td>
                    &nbsp;</td>
            </tr>
            <tr>
             <td>
                    <asp:Label ID="Label1" runat="server" Text="Contact number"></asp:Label>
                </td>
                <td>
                    <asp:TextBox ID="TextBox1" runat="server"></asp:TextBox>
                </td>
                <td>
                    &nbsp;</td>
            </tr>
            <tr>
                <td>
                    &nbsp;</td>
                <td>
                    <asp:Button ID="btnAddcompany" runat="server" Text="Add company" />
                </td>
                <td>
                    &nbsp;</td>
            </tr>

            <tr>
                <td>
                    &nbsp;</td>
                <td>
                    &nbsp;</td>
                <td>
                    &nbsp;</td>
            </tr>
        </table>
    
    </div>
</asp:Content>

