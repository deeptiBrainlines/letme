﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="AdminLogin.aspx.cs" Inherits="HealToZeal.Admin.AdminLogin" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title></title>
</head>
<body bgcolor="#EAF2F8">
    <form id="form1" runat="server">
     <div style="align-content:center">    
        <h3 class="hs_theme_color" >Login</h3>
        <table >
            <%--<tr>
                <td >
                    &nbsp;</td>
                <td>
                    <asp:Label ID="lblTitle" runat="server"  Text="Login"></asp:Label>
                </td>
                <td>
                    &nbsp;</td>
            </tr--%>
            <tr>
                <td style="height: 50px">
                    <asp:Label ID="LblUsername" runat="server" Text="UserName"></asp:Label>
                </td>
                <td>
                    <asp:TextBox ID="txtUsername"  Width="200px" Height="35px" runat="server" placeholder="User name(required)" ValidationGroup="0"></asp:TextBox>
                </td>
                <td>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" 
                        ControlToValidate="txtUsername" ErrorMessage="Enter Username." ForeColor="Red" 
                        ValidationGroup="0"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td style="height: 50px">
                    <asp:Label ID="lblPassword" runat="server" Text="Password"></asp:Label>
                </td>
                <td >
                    <asp:TextBox ID="txtPassword" runat="server" Width="200px" Height="35px" runat="server" placeholder="Password(required)" TextMode="Password" 
                        ValidationGroup="0"></asp:TextBox>
                </td>
                <td>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" 
                        ControlToValidate="txtPassword" ErrorMessage="Enter password." ForeColor="Red" 
                        ValidationGroup="0"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td >
                    &nbsp;</td>
                <td class="style2">
                    <asp:Button ID="btnLogin" height="30px" Width="100px" runat="server" Text="Login" 
                        onclick="btnLogin_Click" ValidationGroup="0" />
                </td>
                <td>
                    &nbsp;</td>
            </tr>
            <tr>
                <td>
                    &nbsp;</td>
                <td>
                    <asp:Label ID="lblMessage" runat="server" ForeColor="Red"></asp:Label>
                </td>
                <td>
                    &nbsp;</td>
            </tr>
        </table>
    <footer id="hs_footer">
        <div class="container">
            <div class="hs_footer_content">
                <div class="row">
                    <div class="col-lg-8 col-md-8 col-sm-8">
                        <div class="row">
                            <div class="hs_footer_about_us">
                                <div class="col-lg-3 col-md-4 col-sm-12 col-md-12 col-sm-12">
                                    <a>
                                        <img src="../images/logo1.png" />
                                    </a>
                                    <a style="font-family: Calibri; color:black; font-size: large; font-weight: bold; text-transform: uppercase">Let Me Talk2 Me
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </footer>
    </div>
    </form>
</body>
</html>

