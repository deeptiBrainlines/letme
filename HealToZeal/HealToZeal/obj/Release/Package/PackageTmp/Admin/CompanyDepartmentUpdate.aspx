﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/Admin.Master" AutoEventWireup="true" CodeBehind="CompanyDepartmentUpdate.aspx.cs" Inherits="HealToZeal.Admin.CompanyDepartmentUpdate" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="LeftColumn" runat="server">
    <table>
        <tr>
            <td></td>
            <td>
                <asp:Label ID="lblTitle" runat="server" Text="Company Department" ></asp:Label>
            </td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="lblCompany" runat="server" Text="Company"></asp:Label>
            </td>
            <td>
                 <asp:TextBox ID="TxtCompany" runat="server" Enabled="false"></asp:TextBox>
            </td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="lblCompanyDepartment" runat="server" Text="Department"></asp:Label>
            </td>
            <td>
                <asp:TextBox ID="txtCompanyDepartment" runat="server"></asp:TextBox>
            </td>
            <td>
            
            </td>
            <td></td>
        </tr>
        <tr>
            <td></td>
            <td>
        
                <asp:Button ID="btnSubmit" runat="server" Text="Update"   />
        
            </td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td></td>
            <td>
                <asp:Label ID="lblMessage" runat="server" Text=""></asp:Label>
            </td>
            <td></td>
            <td></td>
        </tr>
    </table>
</asp:Content>
