﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/Admin.Master" AutoEventWireup="true" CodeBehind="CompanyLoginInfo.aspx.cs" Inherits="HealToZeal.Admin.CompanyLoginInfo" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="LeftColumn" runat="server">
    <asp:ScriptManager ID="Scr" runat="server"></asp:ScriptManager>
    <asp:UpdatePanel ID="UpPnl" runat="server">
        <ContentTemplate>
            <table style="width: 100%">
                <tr>
                    <td>
                        <asp:GridView ID="GdvCompanyLoginInfo" runat="server" Width="100%" BorderStyle="None"
                            AutoGenerateColumns="False" AllowSorting="True">
                            <Columns>
                                <asp:TemplateField>
                                    <HeaderTemplate>
                                        <table style="width: 100%">
                                            <tr>
                                                <td style="width:30%" align="left">
                                                    <asp:Label ID="lblEmployeeName" runat="server" Text="CompanyName"></asp:Label>
                                                </td>
                                                <td style="width:30%" align="left">
                                                    <asp:Label ID="lblQualification" runat="server" Text="UserName">
                                                    </asp:Label>
                                                </td>
                                                <td style="width:40%" align="left">
                                                    <asp:Label ID="Label4" runat="server" Text="Password">
                                                    </asp:Label>
                                                </td>
                                            </tr>
                                        </table>
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <table style="width: 100%">
                                            <tr>
                                                <td style="width:30%">
                                                    <asp:Label ID="Label1" runat="server" Text='<%#Eval("CompanyName") %>'></asp:Label>
                                                </td>
                                                <td style="width:30%">

                                                    <asp:Label ID="Label2" runat="server" Text='<%#Eval("UserName") %>'>
                                                    </asp:Label>
                                                </td>
                                                <td style="width:40%">
                                                    <asp:Label Width="40px" Enabled="false" ID="Label3" Text='<%#Eval("DisplayPassword") %>' runat="server" Height="20px"></asp:Label>
                                                </td>
                                            </tr>
                                        </table>
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                        </asp:GridView>
                    </td>
                </tr>
            </table>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
