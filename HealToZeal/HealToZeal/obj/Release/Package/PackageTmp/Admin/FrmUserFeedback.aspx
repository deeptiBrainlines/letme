﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/Admin.Master" AutoEventWireup="true" CodeBehind="FrmUserFeedback.aspx.cs" Inherits="HealToZeal.Admin.FrmUserFeedback" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="LeftColumn" runat="server">

    <asp:ScriptManager ID="ScrMngr" runat="server"></asp:ScriptManager>
    <asp:UpdatePanel ID="UpPnl" runat="server">
        <ContentTemplate>
            <table style="width: 100%">
                <tr>
                    <td class="auto-style1"></td>
                    <td align="center">
                        <asp:Label ID="Label1" runat="server" Text="Report"></asp:Label>
                    </td>
                    <td></td>
                </tr>
                <tr>
                    <td class="auto-style1">
                        <asp:Label ID="Label2" runat="server" Text="Select company"></asp:Label>
                    </td>
                    <td>
                        <asp:DropDownList ID="DrpCompany" AutoPostBack="true" runat="server" Width="90%" OnSelectedIndexChanged="DrpCompany_SelectedIndexChanged">
                        </asp:DropDownList>
                    </td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td class="auto-style1">
                        <asp:Label ID="Label3" runat="server" Text="Select Employee"></asp:Label>
                    </td>
                    <td>
                        <asp:DropDownList ID="DrpEmployees" AutoPostBack="true" runat="server" Width="90%" OnSelectedIndexChanged="DrpEmployees_SelectedIndexChanged">
                        </asp:DropDownList>
                    </td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td class="auto-style1">
                        <asp:Label ID="Label10" runat="server" Text="Select EmployeeCompany ID"></asp:Label>
                    </td>
                    <td valign="top">
                        <asp:DropDownList ID="DrpEmployeesCompanyID" runat="server" AutoPostBack="true" OnSelectedIndexChanged="DrpEmployeesCompanyID_SelectedIndexChanged" Width="90%">
                        </asp:DropDownList>
                    </td>
                    <td>&nbsp;</td>
                </tr>
                <tr id="TrEmp" runat="server" visible="false">
                    <td class="auto-style1">&nbsp;</td>
                    <td>
                        <%--<asp:GridView ID="GridView1" runat="server" Width="100%" BorderStyle="None"
                            AutoGenerateColumns="False" AllowSorting="True">
                            <Columns>
                                <asp:TemplateField>
                                    <HeaderTemplate>
                                        <table style="width: 100%">
                                            <tr>
                                                <td>
                                                    <asp:Label ID="lblEmployeeName" runat="server" Text="AssessmentName"></asp:Label>
                                                </td>
                                                <td>
                                                    <asp:Label ID="lblQualification" runat="server" Text="ClusterName">
                                                    </asp:Label>
                                                </td>
                                                <td>
                                                    <asp:Label Width="40px" Enabled="false" ID="LblColor" Text="MaxScore" runat="server" Height="20px"></asp:Label>
                                                </td>
                                                <td>
                                                    <asp:Label ID="Label4" runat="server" Text="NoofQuestions">
                                                    </asp:Label>
                                                </td>
                                                <td>
                                                    <asp:Label ID="Label5" runat="server" Text="Score">
                                                    </asp:Label>
                                                </td>
                                                <td>
                                                    <asp:Label ID="Label8" runat="server" Text="%">
                                                    </asp:Label>
                                                </td>
                                            </tr>
                                        </table>
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <table style="width: 100%">
                                            <tr>
                                                <td>
                                                    <asp:Label ID="lblEmployeeName" runat="server" Text='<%#Eval("AssessmentName") %>'></asp:Label>
                                                </td>
                                                <td>

                                                    <asp:Label ID="lblQualification" runat="server" Text='<%#Eval("ClusterName") %>'>
                                                    </asp:Label>
                                                </td>
                                                <td>
                                                    <asp:Label Width="40px" Enabled="false" ID="LblColor" Text='<%#Eval("MaxScore") %>' runat="server" Height="20px"></asp:Label>
                                                </td>
                                                <td>
                                                    <asp:Label ID="Label4" runat="server" Text='<%#Eval("NoofQuestions") %>'>
                                                    </asp:Label>
                                                </td>
                                                <td>
                                                    <asp:Label ID="Label5" runat="server" Text='<%#Eval("Score") %>'>
                                                    </asp:Label>
                                                </td>
                                                <td>
                                                    <asp:Label ID="Label9" runat="server" Text='<%#Eval("Percentage") %>'>
                                                    </asp:Label>
                                                </td>
                                            </tr>
                                        </table>

                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                        </asp:GridView>--%>
                    </td>
                    <td>&nbsp;</td>
                </tr>
                <tr id="TrAll" runat="server" visible="false">
                    <td colspan="3">
                        <asp:GridView ID="GdvAll" runat="server" Width="100%" BorderStyle="None"
                            AutoGenerateColumns="False" AllowSorting="True">
                            <Columns>
                                <asp:TemplateField>
                                    <HeaderTemplate>
                                        <table style="width: 100%">
                                            <tr>
                                                <td align="left">
                                                    <asp:Label ID="Label6" runat="server" Text="Company">
                                                    </asp:Label>
                                                </td>
                                                <td colspan="4"></td>
                                            </tr>
                                        </table>
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <table style="width: 100%">
                                            <tr>
                                                <td>
                                                    <asp:Label ID="LblCompName" runat="server" Text='<%#Eval("CompanyName") %>'></asp:Label>
                                                    <asp:Label ID="LblEmplId" Visible="false" runat="server" Text='<%#Eval("CompanyId_PK")%>'></asp:Label>
                                                </td>
                                                <td colspan="4">
                                                    <table style="width: 100%">
                                                        <tr>
                                                            <td>
                                                                <asp:GridView ID="GdvAllInner" runat="server" Width="100%" BorderStyle="None"
                                                                    AutoGenerateColumns="False" AllowSorting="True">
                                                                    <Columns>
                                                                        <asp:TemplateField>
                                                                            <HeaderTemplate>
                                                                                <table style="width: 100%">
                                                                                    <tr>
                                                                                        <td align="left">
                                                                                            <asp:Label ID="Label6" runat="server" Text="Employee">
                                                                                            </asp:Label>
                                                                                        </td>
                                                                                        <td colspan="4"></td>
                                                                                    </tr>
                                                                                </table>
                                                                            </HeaderTemplate>
                                                                            <ItemTemplate>
                                                                                <table style="width: 100%">
                                                                                    <tr>
                                                                                        <td style="width:150px" align="left" >
                                                                                            <asp:Label  ID="lblEmployeeName" runat="server" Text='<%#Eval("Name") %>'></asp:Label> 
                                                                                           ( <asp:Label  ID="Label4" runat="server" Text='<%#Eval("EmployeeCompanyId") %>'></asp:Label> )
                                                                                            <asp:Label  ID="LblEmpId" Visible="false" runat="server" Text='<%#Eval("EmployeeId_PK") %>'></asp:Label>
                                                                                        </td>
                                                                                        <td colspan="4">
                                                                                            <asp:GridView ID="GvInnerInner" runat="server" Width="100%" BorderStyle="None"
                                                                                                AutoGenerateColumns="False" AllowSorting="True">
                                                                                                <Columns>
                                                                                                    <asp:TemplateField>
                                                                                                        <HeaderTemplate>
                                                                                                            <table style="width: 100%">
                                                                                                                <tr>
                                                                                                                    <td>
                                                                                                                        <asp:Label Width="200px" ID="lblEmployeeName" runat="server" Text="Question"></asp:Label>
                                                                                                                    </td>
                                                                                                                    <td>
                                                                                                                        <asp:Label ID="lblQualification" runat="server" Text="Answer">
                                                                                                                        </asp:Label>
                                                                                                                    </td>
                                                                                                                    <td>
                                                                                                                        <asp:Label ID="Label11" runat="server" Text="Feedback">
                                                                                                                        </asp:Label>
                                                                                                                    </td>

                                                                                                                </tr>
                                                                                                            </table>
                                                                                                        </HeaderTemplate>
                                                                                                        <ItemTemplate>
                                                                                                            <table style="width: 100%">
                                                                                                                <tr>
                                                                                                                    <td>
                                                                                                                        <asp:Label ID="Label7" runat="server" Text='<%#Eval("FeedbackQuestion") %>'>
                                                                                                                        </asp:Label>
                                                                                                                    </td>
                                                                                                                    <td>
                                                                                                                        <asp:Label ID="lblQualification" runat="server" Text='<%#Eval("FeedbackAnswer") %>'>
                                                                                                                        </asp:Label>
                                                                                                                    </td>
                                                                                                                    <td>
                                                                                                                        <asp:Label ID="Label12" runat="server" Text='<%#Eval("Feedback") %>'>
                                                                                                                        </asp:Label>
                                                                                                                    </td>
                                                                                                                </tr>
                                                                                                            </table>
                                                                                                        </ItemTemplate>
                                                                                                    </asp:TemplateField>
                                                                                                </Columns>
                                                                                            </asp:GridView>
                                                                                        </td>
                                                                                        <%--<td>
                                                                                            <asp:Label ID="lblQualification" runat="server" Text='<%#Eval("ClusterName") %>'>
                                                                                            </asp:Label>
                                                                                        </td>
                                                                                        <td>
                                                                                            <asp:Label Width="40px" Enabled="false" ID="LblColor" Text='<%#Eval("MaxScore") %>' runat="server" Height="20px"></asp:Label>
                                                                                        </td>
                                                                                        <td>
                                                                                            <asp:Label ID="Label4" runat="server" Text='<%#Eval("NoofQuestions") %>'>
                                                                                            </asp:Label>
                                                                                        </td>
                                                                                        <td>
                                                                                            <asp:Label ID="Label5" runat="server" Text='<%#Eval("Score") %>'>
                                                                                            </asp:Label>
                                                                                        </td>
                                                                                        <td>
                                                                                            <asp:Label ID="Label7" runat="server" Text='<%#Eval("Percentage") %>'>
                                                                                            </asp:Label>
                                                                                        </td>--%>
                                                                                    </tr>
                                                                                </table>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                    </Columns>
                                                                </asp:GridView>
                                                            </td>
                                                        </tr>
                                                    </table>


                                                </td>

                                            </tr>
                                        </table>

                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                        </asp:GridView>
                    </td>

                </tr>
            </table>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
