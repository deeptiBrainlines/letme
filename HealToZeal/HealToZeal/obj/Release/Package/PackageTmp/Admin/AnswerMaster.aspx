﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Admin/Admin.Master" CodeBehind="AnswerMaster.aspx.cs" Inherits="HealToZeal.Admin.AnswerMaster" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>


<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="LeftColumn" runat="server">
     <script src="../Scripts/jquery-1.9.1.js" type="text/javascript"></script>
    <%--<script src="http://code.jquery.com/jquery-1.9.1.min.js"></script>--%>
    <script type="text/javascript">
        //        function divexpandcollapse(divname) {
        //            var img = "img" + divname;
        //            if ($("#" + img).attr("src") == "images/plus.gif") {
        //                $("#" + img)
        //             .closest("tr")
        //             .after("<tr><td></td><td colspan = '100%'>" + $("#" + divname)
        //             .html() + "</td></tr>");
        //                $("#" + img).attr("src", "images/minus.gif");
        //            } else {
        //                $("#" + img).closest("tr").next().remove();
        //                $("#" + img).attr("src", "images/plus.gif");
        //            }
        //        }
        function collapseExpand(obj) {
            var gvObject = document.getElementById(obj);
            var imageID = document.getElementById('image' + obj);

            if (gvObject.style.display == "none") {
                gvObject.style.display = "inline";
                imageID.src = "../images/minus.gif";
            }
            else {
                gvObject.style.display = "none";
                imageID.src = "../images/plus.gif";
            }
        }

</script>
    <div>
       <asp:ScriptManager ID="ScriptManager1" runat="server">
        </asp:ScriptManager>

        <cc1:TabContainer ID="TabContainerAnswerMaster" runat="server" 
            ActiveTabIndex="0">
            <cc1:TabPanel ID="TabPnlViewAnswers" runat="server">
            <HeaderTemplate>Answers</HeaderTemplate>
            <ContentTemplate>
                <asp:GridView ID="GvViewAnswersOfQuestion" runat="server" 
                    AutoGenerateColumns="False" onrowcommand="GvViewAnswersOfQuestion_RowCommand">
                <Columns>
                <asp:TemplateField>
                <HeaderTemplate></HeaderTemplate>
                <ItemTemplate>
                <table>
                 <tr>
                      <td>
                          <asp:Label ID="lblAnswer" runat="server" Text='<%#Eval("Answer") %>'></asp:Label>
                      </td>
                      <td>
                       <asp:Label ID="lblScoreForAnswer" runat="server" Text='<%#Eval("ScoreForAnswer") %>'></asp:Label>
                      </td>
                      <td>
                          <asp:Button ID="btnEdit" runat="server" Text="Edit" CommandName="EditAnswer" CommandArgument='<%#Eval("AnswerId_PK") %>' />
                      </td>
                      <td>
                          <asp:Button ID="btnDelete" runat="server" Text="Delete" Visible="false"/>
                      </td>                     
                      </tr>
                </table>
                </ItemTemplate>
                </asp:TemplateField>
                </Columns>
                </asp:GridView>
            </ContentTemplate>
            </cc1:TabPanel>
          <cc1:TabPanel ID="TabPnlView" runat="server" >
          <HeaderTemplate>View Answers</HeaderTemplate>
          <ContentTemplate>
              <asp:GridView ID="GvViewQuestions" AutoGenerateColumns="False" 
                  runat="server">
              <Columns>
              <asp:TemplateField>
              <ItemTemplate>               
              <table>
              <tr>
              <td>         
               <a href="javascript:collapseExpand('div<%# Eval("QuestionId_PK") %>');">                
               <img id="imagediv<%#Eval("QuestionId_PK") %>" alt="" border="0" src="../images/plus.gif" />                 
               </a>
              </td>
              <td>
                 <asp:Label ID="lblQuestionId" runat="server" Text='<%#Eval("QuestionId_PK") %>' Visible="false"></asp:Label>
                  <asp:Label ID="lblQuestion" runat="server" Text='<%#Eval("Question") %>'></asp:Label>
              </td>
              </tr>
              <tr>
              <td>               
                   <div id='div<%# Eval("QuestionId_PK") %>' style="display:none;position:relative;left:25px;">
                      <asp:GridView ID="GvViewAnswers" AutoGenerateColumns="False" runat="server"  onrowcommand="GvViewAnswers_RowCommand" >
                      <Columns>
                      <asp:TemplateField>
                      <ItemTemplate>
                      <table>
                      <tr>
                      <td>
                          <asp:Label ID="lblAnswer" runat="server" Text='<%#Eval("Answer") %>' ></asp:Label>
                      </td>
                      <td>
                       <asp:Label ID="lblScoreForAnswer" runat="server" Text='<%#Eval("ScoreForAnswer") %>'></asp:Label>
                      </td>
                      <td>
                          <asp:Button ID="btnEdit" runat="server" Text="Edit" CommandName="EditAnswer" CommandArgument='<%#Eval("AnswerId_PK") %>' />
                      </td>
                      <td>
                          <asp:Button ID="btnDelete" runat="server" Text="Delete" Visible="false"/>
                      </td>                     
                      </tr>
                      </table>
                      </ItemTemplate>
                      </asp:TemplateField>
                      </Columns>
                      </asp:GridView>
                </div> 
              </td>
              </tr>
              </table>
              </ItemTemplate>
              </asp:TemplateField>
              </Columns>
              </asp:GridView>
          </ContentTemplate>
          </cc1:TabPanel> 
        <cc1:TabPanel ID="TabPnlAddUpdate" runat="server" >
            <HeaderTemplate>Add/Update Answers</HeaderTemplate>
                <ContentTemplate>
                    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                    <ContentTemplate>
                             <div>
                <table>
                <tr>
                <td>
                        <table>
                           <tr>
                        <td>
                            &nbsp;</td>
                        <td>
                            <asp:Label ID="lblTitle" runat="server" Text="Answer details"></asp:Label>
                        </td>
                        <td>
                            &nbsp;</td>
                         <tr>
                        <td>
                            <asp:Label ID="lblQuestions" runat="server" Text="Questions"></asp:Label>
                        </td>
                        <td>
                            <asp:DropDownList ID="ddlQuestions" runat="server" AutoPostBack="True" 
                                onselectedindexchanged="ddlQuestions_SelectedIndexChanged" 
                                ValidationGroup="1">
                            </asp:DropDownList>
                        </td>
                        <td>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" 
                                ControlToValidate="ddlQuestions" ErrorMessage="Select a question..." 
                                ForeColor="Red" InitialValue="--Select Question--" ValidationGroup="1"></asp:RequiredFieldValidator>
                             </td>
                    </tr>
                    </tr>
                           <tr>
                        <td>
                            <asp:Label ID="lblAnswer" runat="server" Text="Answer"></asp:Label>
                        </td>
                        <td>
                            <asp:TextBox ID="txtAnswer" TextMode="MultiLine" runat="server" 
                                ValidationGroup="1" ></asp:TextBox>
                        </td>
                        <td>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" 
                                ControlToValidate="txtAnswer" ErrorMessage="Enter the answer..." 
                                ForeColor="Red" ValidationGroup="1"></asp:RequiredFieldValidator>
                               </td>
                    </tr>
                           <tr>
                        <td>
                            <asp:Label ID="lblAnswerDescription" runat="server" Text="Answer description"></asp:Label>
                        </td>
                        <td>
                            <asp:TextBox ID="txtAnswerDescription" runat="server" ValidationGroup="1" ></asp:TextBox>
                        </td>
                        <td>
                            &nbsp;</td>
                    </tr>
                           <tr>
                        <td>
                            <asp:Label ID="lblScoreForAnswer" runat="server" Text="Score"></asp:Label>
                        </td>
                        <td>
                            <asp:TextBox ID="txtScoreForAnswer" runat="server" ValidationGroup="1" ></asp:TextBox>
                        </td>
                        <td>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" 
                                ControlToValidate="txtScoreForAnswer" ErrorMessage="Enter the score " 
                                ForeColor="Red" ValidationGroup="1"></asp:RequiredFieldValidator>
                               </td>
                    </tr>
                         
                           <tr>
                        <td>
                            &nbsp;</td>
                        <td>
                            &nbsp;</td>
                        <td>
                            &nbsp;</td>
                    </tr>              
                    <tr>
                    <td></td><td>
                        <asp:Button ID="BtnAddUpdate" runat="server" Text="Submit" 
                            onclick="BtnAddUpdate_Click" ValidationGroup="1" />
                         <asp:Button ID="BtnReset" runat="server" Text="Reset" 
                            onclick="BtnReset_Click" />
                        </td>

                    </tr>
                      </table> 
                </td>
                <td>
                <table>
                <tr>
                <td>
                    &nbsp;</td>
                </tr>
                </table>
                </td>
                </tr>
                </table>
                </div>               
                    </ContentTemplate>
                    </asp:UpdatePanel>
       
                </ContentTemplate>   
        </cc1:TabPanel>
        </cc1:TabContainer>
    </div>
</asp:Content>

  
