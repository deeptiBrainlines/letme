﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ViewAssignedInstances.aspx.cs" MasterPageFile="~/Admin/Admin.Master" Inherits="HealToZeal.Admin.ViewAssignedInstances" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="LeftColumn" runat="server">
    <div>
           <table>
            <tr>
                <td colspan="4">
                    <asp:Label ID="lblTitle" runat="server" Text="Assessment Instances"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:DataList ID="DlistInstances" runat="server" OnItemCommand="DlistInstances_ItemCommand"
                         OnItemDataBound="DlistInstances_ItemDataBound" Width="100%" GridLines="Both">
                        <HeaderTemplate>
                            <table style="width:100%">
                                <tr>
                                    <td style="width:70%">Assessment instance</td>
                                     <td></td>
                                     <td></td>
                                </tr>
                            </table>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <table>
                                <tr>
                                    <td style="width:80%">
                                          <asp:Label ID="lblInstances" runat="server" Text='<%#Eval("AssessmentInstanceName") %>'> </asp:Label>
                                    </td>
                                    <td>
                                          <asp:Label ID="lblInstanceId" runat="server" Text='<%#Eval("AssessmentInstanceId_PK") %>' Visible="false"> </asp:Label>
                                    </td>
                                    <td style="width:20%">
                                        <asp:Button ID="btnUpdate" runat="server" Text="Update" CommandName="UpdateInstance" CommandArgument='<%#Eval("AssessmentInstanceId_PK") %>'/>
                                    </td>
                                </tr>
                            </table>
                            <asp:DataList ID="DlistCompanies" runat="server" 
                                OnItemCommand="DlistCompanies_ItemCommand" GridLines="Both">
                                <HeaderTemplate>
                                    <table style="width:100%">
                                        <tr>
                                            <td style="width:30%">Company </td>
                                             <td></td>
                                             <td></td>
                                             <td style="width:20%">Valid from </td>
                                             <td style="width:40%">Valid to</td>
                                        </tr>
                                    </table>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <table style="width:100%">
                                        <tr>
                                            <td style="width:50%">
                                                  <asp:Label ID="lblCompanyName" runat="server" Text='<%#Eval("CompanyName") %>'> </asp:Label>
                                            </td>
                                            <td>
                                                 <asp:Label ID="lblCompanyId" runat="server" Text='<%#Eval("CompanyId_FK") %>' Visible="false"> </asp:Label>
                                            </td>
                                            <td>
                                                  <asp:Label ID="lblInstanceId" runat="server" Text='<%#Eval("AssessmentInstanceId_FK") %>' Visible="false"> </asp:Label>                                                
                                            </td>
                                                <td style="width:20%">
                                                 <asp:Label ID="lblValidFrom" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "ValidFrom", "{0:dd/MM/yyyy}") %>'> </asp:Label>
                                            </td>                                            
                                                <td style="width:20%">
                                                 <asp:Label ID="lblValidTo" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "ValidTo", "{0:dd/MM/yyyy}") %>'> </asp:Label>
                                            </td>
                                            <td style="width:10%">
                                                   <asp:Button ID="btnUpdateCompany" runat="server" Text="Update" CommandName="UpdateCompany" CommandArgument='<%#Eval("CompanyId_FK") %>'/>
                                            </td>
                                        </tr>
                                    </table>
                                </ItemTemplate>
                            </asp:DataList>
                        </ItemTemplate>
                    </asp:DataList>
                </td>
            </tr>
        </table>
    </div>
    </asp:Content>