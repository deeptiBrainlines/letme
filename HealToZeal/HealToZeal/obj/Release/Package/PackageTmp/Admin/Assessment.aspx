﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Assessment.aspx.cs" MasterPageFile="~/Admin/Admin.Master" Inherits="HealToZeal.Admin.Assessment" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="LeftColumn" runat="server" >
    <div class="new1">
        <table>
            <tr>
                <td>
                    
                </td>
                <td><asp:Label ID="lblTitle" runat="server" Text="Assessment"></asp:Label>
                </td>
                <td></td>
            </tr>
             <tr>
                <td>
                    <asp:Label ID="lblAssessmentName" runat="server" Text="Assessment name" > </asp:Label>
                </td>
                <td>
                    <asp:TextBox ID="txtAssessmentName" runat="server" autofocus></asp:TextBox>
                </td>
                 <td>
                     <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="Please enter name." ControlToValidate="txtAssessmentName" Display="Dynamic" ForeColor="Red"></asp:RequiredFieldValidator>
                 </td>
            </tr>
             <tr>
                <td>
                    <asp:Label ID="lblNoOfQuestions" runat="server" Text="Number of Questions"></asp:Label>
                </td>
                <td>
                    <asp:TextBox ID="txtNoOfQuestions" runat="server" ></asp:TextBox>
                </td>
                 <td></td>
            </tr>     
          
            <tr>
                <td>
                    <asp:Label ID="lblIsComplete" runat="server" Text="Is complete"></asp:Label>
                </td>
                <td>
                    <asp:CheckBox ID="chkIsComplete" runat="server" />
                </td>
                <td></td>
            </tr> 
              <tr>
                <td></td>
                <td colspan="2">
                    <asp:Button ID="btnSubmit" runat="server" Text="Submit" OnClick="btnSubmit_Click" />
                </td>             
            </tr>    
            <tr>
                <td></td>
                <td colspan="2">
                    <asp:Label ID="lblMessage" runat="server" ForeColor="Red"></asp:Label>
                </td>
            </tr> 
        </table>
    </div>
  
</asp:Content>
