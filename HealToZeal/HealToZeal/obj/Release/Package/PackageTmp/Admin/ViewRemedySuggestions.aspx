﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/Admin.Master" AutoEventWireup="true" CodeBehind="ViewRemedySuggestions.aspx.cs" Inherits="HealToZeal.Admin.ViewRemedySuggestions" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="LeftColumn" runat="server">
    <table>
        <tr>
            <td colspan="2">
                <asp:Label ID="lblTitle" runat="server" Text="Remedy suggestions"></asp:Label>
            </td>           
        </tr>
        <tr>
            <td>
                <asp:DropDownList ID="ddlAssessment" runat="server">

                </asp:DropDownList>
            </td>
            <td>
                <asp:DropDownList ID="ddlCluster" runat="server">

                </asp:DropDownList>
            </td>
            <td>
                <asp:Button ID="btnShow" runat="server" Text="Show" OnClick="btnShow_Click" />
            </td>
        </tr>
           <tr>
            <td colspan="2">
                <asp:DataList ID="dlistRemedySuggestions" OnItemCommand="dlistRemedySuggestions_ItemCommand" GridLines="Both" runat="server">
                    <HeaderTemplate>
                        <table style="width:90%">
                            <tr>
                                <td style="width:50%">Suggestion</td>
                                <td></td>
                                <td style="width:15%">Minimum score</td>
                                <td  style="width:15%">Maximum score</td>
                            </tr>
                        </table>
                    </HeaderTemplate>
                    <ItemTemplate>
                      <table>
                          <tr>
                              <td style="width:60%">
                                      <asp:Label ID="lblSuggestion" runat="server" Text='<%#Eval("Suggestion") %>'></asp:Label>
                              </td>
                              <td>
                                      <asp:Label ID="lblRemedySuggestionId" runat="server" Visible="false" Text='<%#Eval("RemedySuggestionId_PK") %>'></asp:Label>
                              </td>
                            <%--  <td>
                                      <asp:Label ID="lblAssessment" runat="server" Text='<%#Eval("AssessmentName") %>'></asp:Label>
                              </td>
                                  <td>
                                      <asp:Label ID="lblClusterName" runat="server" Text='<%#Eval("ClusterName") %>'></asp:Label>
                              </td>--%>
                                   <td style="width:10%">
                                      <asp:Label ID="lblMinScore" runat="server" Text='<%#Eval("MinScore") %>'></asp:Label>
                              </td>
                               <td style="width:10%">
                                      <asp:Label ID="lblMaxScore" runat="server" Text='<%#Eval("MaxScore") %>'></asp:Label>
                              </td>
                                   <td style="width:20%">
                                       <asp:Button ID="btnUpdate" runat="server" Text="Update" CommandArgument='<%#Eval("RemedySuggestionId_PK") %>' CommandName="UpdateSuggestion" />
                              </td>
                          </tr>
                      </table>
                    </ItemTemplate>
                </asp:DataList>
            </td>           
        </tr>
        
    </table>
</asp:Content>
