﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Admin/Admin.Master" CodeBehind="ViewAssessmentInstances.aspx.cs" Inherits="HealToZeal.Admin.ViewAssessmentInstances" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="LeftColumn" runat="server">
    <div>
        <table style="width:60%">
            <tr>
                <td colspan="4">
                    <asp:Label ID="lblTitle" runat="server" Text="Assessment Instances"></asp:Label>
                </td>
            </tr>
            <tr>
                <td colspan="4">
                    <asp:DataList ID="DlistInstances" runat="server" OnItemCommand="DlistInstances_ItemCommand" OnItemDataBound="DlistInstances_ItemDataBound" Width="100%" GridLines="Both">
                        
                        <HeaderTemplate>
                            <table style="width:100%">
                                <tr>
                                    <td style="width:80%">Assessment Instance</td>
                                </tr>
                            </table>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <table>
                                <tr>
                                    <td style="width:80%">
                                          <asp:Label ID="lblInstances" runat="server" Text='<%#Eval("AssessmentInstanceName") %>'> </asp:Label>
                                    </td>
                                    <td>
                                          <asp:Label ID="lblInstanceId" runat="server" Text='<%#Eval("AssessmentInstanceId_PK") %>' Visible="false"> </asp:Label>
                                    </td>
                                    <td style="width:20%">
                                        <asp:Button ID="btnUpdate" runat="server" Text="Update" CommandName="UpdateInstance" CommandArgument='<%#Eval("AssessmentInstanceId_PK") %>'/>
                                    </td>
                                </tr>
                            </table>
                            <asp:DataList ID="DlistAssessments" runat="server" GridLines="Both">
                                <HeaderTemplate>
                                    <table style="width:100%">
                                        <tr>
                                            <td style="width:80%">Assessments</td>
                                        </tr>
                                    </table>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <table style="width:100%">
                                        <tr>
                                            <td style="width:90%">
                                                  <asp:Label ID="lblAssessmentName" runat="server" Text='<%#Eval("AssessmentName") %>'> </asp:Label>
                                            </td>
                                            <td>
                                                 <asp:Label ID="lblAssessmentId" runat="server" Text='<%#Eval("AssessmentId_FK") %>' Visible="false"> </asp:Label>
                                            </td>
                                        </tr>
                                    </table>
                                </ItemTemplate>
                            </asp:DataList>
                        </ItemTemplate>
                    </asp:DataList>
                </td>
            </tr>
        </table>
    </div>
</asp:Content>