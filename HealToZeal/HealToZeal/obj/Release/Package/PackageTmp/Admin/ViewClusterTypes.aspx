﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ViewClusterTypes.aspx.cs" MasterPageFile="~/Admin/Admin.Master" Inherits="HealToZeal.Admin.ViewClusterTypes" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="LeftColumn" runat="server">
    <div>
        <table>
            <tr>
                <td colspan="4">
                    <asp:Label ID="lblTitle" runat="server" Text="Cluster types"></asp:Label>
                </td>               
            </tr>           
            <tr>
                <td colspan="4">
                    <asp:DataList ID="DlistClusterTypes" runat="server" OnItemCommand="DlistClusterTypes_ItemCommand" 
                        OnItemDataBound="DlistClusterTypes_ItemDataBound" Width="100%" GridLines="Both">
                        <HeaderTemplate>
                            <table>
                                <tr>
                                    <td>Cluster type </td>
                                     <td></td> 
                                    <td></td>
                                </tr>
                            </table>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <table>
                                <tr>
                                    <td>
                                          <asp:Label ID="lblClusterTypeName" runat="server" Text='<%#Eval("ClusterTypeName") %>'> </asp:Label>
                                    </td>
                                    <td>
                                          <asp:Label ID="lblClusterTypeId" runat="server" Text='<%#Eval("ClusterTypeId_PK") %>' Visible="false"> </asp:Label>
                                    </td>
                                    <td>
                                        <asp:Button ID="btnUpdate" runat="server" Text="Update" CommandName="UpdateClustertype" CommandArgument='<%#Eval("ClusterTypeId_PK") %>'/>
                                    </td>
                                </tr>
                            </table>
                            <asp:DataList ID="DlistClusters" runat="server" GridLines="Both">
                                <HeaderTemplate>
                                    <table>
                                        <tr>
                                            <td>Cluster</td>
                                            <td></td>
                                            <td></td>
                                        </tr>
                                    </table>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <table>
                                        <tr>
                                            <td>
                                                  <asp:Label ID="lblClusterName" runat="server" Text='<%#Eval("ClusterName") %>'> </asp:Label>
                                            </td>
                                            <td>
                                                 <asp:Label ID="lblClusterId" runat="server" Text='<%#Eval("ClusterId_PK") %>' Visible="false"> </asp:Label>
                                            </td>
                                            <td>
                                                  <asp:Label ID="lblClusterTypeId" runat="server" Text='<%#Eval("ClusterTypeId_FK") %>' Visible="false"> </asp:Label>                                                
                                            </td>   
                                        </tr>
                                    </table>
                                </ItemTemplate>
                            </asp:DataList>
                        </ItemTemplate>
                    </asp:DataList>
                </td>
               
            </tr>
        
        </table>
    </div>
</asp:Content>
