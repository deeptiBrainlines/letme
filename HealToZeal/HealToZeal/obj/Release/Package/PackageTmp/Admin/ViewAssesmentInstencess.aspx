﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/Admin.Master" AutoEventWireup="true" CodeBehind="ViewAssesmentInstencess.aspx.cs" Inherits="HealToZeal.Admin.ViewAssesmentInstencess" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="cc1" %>


<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="LeftColumn" runat="server">
    <script type="text/javascript">

        function myFunction() {
            document.getElementById("ddlCompany").classList.toggle("show");
        }

        function filterFunction() {
            var input, filter, ul, li, a, i;
            input = document.getElementById("myInput");
            filter = input.value.toUpperCase();
            div = document.getElementById("myDropdown");
            a = div.getElementsByTagName("a");
            for (i = 0; i < a.length; i++) {
                if (a[i].innerHTML.toUpperCase().indexOf(filter) > -1) {
                    a[i].style.display = "";
                } else {
                    a[i].style.display = "none";
                }
            }
        }
</script>
    <div>
        <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
    </div>
    <table>
    <tr>
        <td colspan="4"><b> Company Assessment Instances</b></td>    

    </tr>
    <tr>
        <td>
                <asp:Label ID="lblCompany" runat="server" Text="Company"></asp:Label>
            </td>
            <td>
                <div>
                    <asp:DropDownList ID="ddlCompany"  runat="server" OnSelectedIndexChanged="ddlCompany_SelectedIndexChanged"  AutoPostBack="true" Width="205px" Height="30px"></asp:DropDownList>
                </div>
                
            </td>
    </tr>
        <br />
        <br />
     <tr>
        <td></td>
        <td>
             <asp:DataList ID="DlistCompany"  runat="server"   OnItemCommand="DlistCompany_ItemCommand"
                 Width="400px" GridLines="Both"> 
                        <ItemTemplate>
                            
                                    <table id="Tb1" style="width:100%;background-color:#ebf5fb" >
                                       
                                        <tr>
                                            
                                            <td style="width:80%" >
                                                  <asp:TextBox ID="lblAssesmentname"  BackColor="#ebf5fb" runat="server" Text='<%#Eval("AssessmentInstanceName") %>' BorderColor="Transparent"> </asp:TextBox>
                                            </td>
                                            <td style="width:80%" >
                                                  <asp:TextBox ID="lblAssesmentID"  BackColor="#ebf5fb"  style="display: none"  runat="server" Text='<%#Eval("AssessmentInstanceId_FK") %>' BorderColor="Transparent"> </asp:TextBox>
                                            </td>
                                            <%-- <td style="width:80%" >
                                                  <asp:Label ID="lbBatchName"  BackColor="#ebf5fb"    runat="server" Text='<%#Eval("BatchName") %>' BorderColor="Transparent"> </asp:Label>
                                            </td>--%>
                                            <td>
                                                 <asp:TextBox ID="txtValidFrom" runat="server"    Text= '<%# ""+ DataBinder.Eval(Container.DataItem, "ValidFrom", "{0:dd/MM/yyyy}") %>' > </asp:TextBox>
                                            <cc1:CalendarExtender ID="txtValidFrom_CalendarExtender" runat="server"   Format="dd/MM/yyyy"    TargetControlID="txtValidFrom">
                                        </cc1:CalendarExtender>
                                            </td>
                                            <td>
                                                  <asp:TextBox ID="txtValidTo"  runat="server" Text= '<%# ""+ DataBinder.Eval(Container.DataItem, "ValidTo", "{0:dd/MM/yyyy}") %>' > </asp:TextBox>    
                                                 <cc1:CalendarExtender ID="txtValidTo_CalendarExtender"  runat="server"  Format="dd/MM/yyyy"   TargetControlID="txtValidTo">
                                    </cc1:CalendarExtender>                                            
                                            </td>                                            
                                           <td style="width:20%">
                                                   <asp:LinkButton ID="btnUpdateDepartment" runat="server" Text="Update"  CommandName="UpdateDepartment" CommandArgument='<%#Eval("AssessmentInstanceId_FK") %>'/>
                                            </td>
                                         
                                        </tr>
                                    </table>
                             
                        </ItemTemplate>
                    </asp:DataList>
        </td>
        <td></td>
        <td></td>
    </tr>
     <tr>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
    </tr>
</table>
</asp:Content>
