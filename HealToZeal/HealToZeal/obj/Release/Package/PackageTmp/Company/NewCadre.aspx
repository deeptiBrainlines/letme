﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Company/Company.Master" AutoEventWireup="true" CodeBehind="NewCadre.aspx.cs" Inherits="HealToZeal.Company.NewCadre" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .style6 
        {
            width: 125px;
        }

        .style7 
        {
            height: 26px;
        }

        .hs_pager1 
        {
            display: inline-block;
            list-style: none;
            padding-left: 0px;
        }

            .hs_pager1 a 
            {
                padding: 5px 12px;
                color: #7f9aa0;
                border: 1px solid #7f9aa0;
                text-align: center;
                font-weight: bold;
                font-size: 16px;
                -webkit-transition: all 0.5s;
                -moz-transition: all 0.5s;
                -ms-transition: all 0.5s;
                -o-transition: all 0.5s;
                margin-bottom: 60px;
            }

                .hs_pager1 a:hover, .hs_pager1 a:active 
                {
                    color: #00ac7a;
                    border: 1px solid #00ac7a;
                    -webkit-transition: all 0.5s;
                    -moz-transition: all 0.5s;
                    -ms-transition: all 0.5s;
                    -o-transition: all 0.5s;
                }

        .pagination 
        {
            line-height: 26px;
        }

            .pagination span 
            {
                padding: 5px;
                border: solid 1px #477B0E;
                text-decoration: none;
                white-space: nowrap;
                background: #547B2A;
            }

            .pagination a, .pagination a:visited 
            {
                text-decoration: none;
                padding: 6px;
                white-space: nowrap;
            }

                .pagination a:hover, .pagination a:active 
                {
                    padding: 5px;
                    border: solid 1px #9ECDE7;
                    text-decoration: none;
                    white-space: nowrap;
                    background: #486694;
                }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="LeftColumn" runat="server">
    <style type="text/css">
        .autocomplete_completionListElement 
        {
            visibility: hidden;
            margin: 0px !important;
            /* background-color: inherit;*/ /* code commented by chetana                                    system 30 july */
            background-color: White; /*/code by chetana sytem 30 july/*/
            color: windowtext;
            border: buttonshadow;
            border-width: 1px;
            border-style: solid;
            overflow: auto;
            /*height: 300px;*/ /* code commented by chetana system 30 july */
            height: auto; /*/code by chetana sytem 30 july/*/
            max-height: 350px;
            text-align: left;
            list-style-type: none;
            font-family: Tahoma;
            font-size: 8pt;
            cursor: pointer;
            position: relative;
            z-index: 500;
            padding: 0px;
        }
        .autocomplete_highlightedListItem {
            background-color: #00AC7A;
            color: black;
            padding: 1px;
        }
        .autocomplete_listItem {
            background-color: window;
            color: windowtext;
            padding: 1px;
        }
        .titlebar {
            font-family: Tahoma;
            font-size: small;
            font-weight: normal;
            color: #D8E9FF;
            background-color: #0D4181;
        }
    </style>

    <script type="text/javascript">
        <%--function UpdateItemSelected() {
            
            var index = $find('<%= txtEmployeeId_AutoCompleteExtender.ClientID %>')._selectIndex;
           
            var value = $find('<%= txtEmployeeId_AutoCompleteExtender.ClientID %>').get_completionList().childNodes[index]._value;

            $get('<%= HdnAutoId.ClientID %>').value = index;
            $get('<%= HdnAutoValue.ClientID %>').value = value;
            $find('<%= txtEmployeeId_AutoCompleteExtender.ClientID %>').get_element().value = value

            var UName = $get('<%= txtEmployeeId.ClientID %>');
            $get('<%= HdnSearch.ClientID %>').value = UName.value;

            $get('<%= btnSearch.ClientID %>').click();
        }--%>

        <%--function UpdateItemSelectedforName() {
            var index = $find('<%= AutoCompleteExtender1.ClientID %>')._selectIndex;
            var value = $find('<%= AutoCompleteExtender1.ClientID %>').get_completionList().childNodes[index]._value;

            $get('<%= HdnAutoId.ClientID %>').value = index;
            $get('<%= HdnAutoValue.ClientID %>').value = value;
            $find('<%= AutoCompleteExtender1.ClientID %>').get_element().value = value

            var UName = $get('<%= txtName.ClientID %>');
            $get('<%= HdnSearch.ClientID %>').value = UName.value;

            $get('<%= btnSearchName.ClientID %>').click();
        }--%>
    </script>

    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <h4 class="hs_heading" id="hs_appointment_form_link">Search </h4>
            <div class="container">
                <div class="row">
                    <asp:Panel ID="PnlAddNewCadre" runat="server" Visible="false">
                        <table>
                            <tr>
                                <td>
                                    <asp:TextBox ID="TxtNewCadreName" runat="server" CssClass="form-control" placeholder="Cadre Name"></asp:TextBox>
                                    <div class="pull-left">
                                        <div class="input-group">
                                            <span class="input-group-btn">
                                                <asp:Button ID="BtnSubmit" runat="server" Text="Submit" Visible="false" CssClass="btn btn-primary btn-xs" OnClick="BtnSubmit_Click" />
                                                <asp:Button ID="BtnUpdate" runat="server" Text="Update" Visible="false" CssClass="btn btn-primary btn-xs" OnClick="BtnUpdate_Click" />
                                            </span>
                                        </div>
                                    </div>
                                </td>

                            </tr>
                        </table>
                    </asp:Panel>
                    <table style="width: 100%">
                        <%--  Search starts here--%>
                        <tr>
                            <td style="width: 100%; text-align: left">
                                <div class="col-lg-8 col-md-8 col-sm-7">
                                    <div class="hs_comment_form">
                                        <div class="row">
                                            <div class="row">
                                                <div class="col-lg-12">
                                                    <div class="pull-right">
                                                        <div class="input-group">
                                                            <span class="input-group-btn">
                                                                <asp:Button ID="BtnRefreshAll" runat="server" Text="Reset" CssClass="btn btn-primary btn-xs" OnClick="BtnRefreshAll_Click" />
                                                            </span>
                                                        </div>
                                                    </div>
                                                    <div class="pull-left">
                                                        <div class="input-group">
                                                            <span class="input-group-btn">
                                                                <asp:Button ID="BtnAddNew" runat="server" Text="Add New Cadre" CssClass="btn btn-primary btn-xs" OnClick="BtnAddNew_Click" />
                                                            </span>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-lg-6 col-md-6 col-sm-12">
                                                    <div class="input-group">
                                                        <span class="input-group-btn">
                                                            <div class="btn btn-success"><i class="fa fa-hand-o-right"></i></div>
                                                        </span>
                                                        <asp:HiddenField ID="HdnAutoId" runat="server" />
                                                        <asp:HiddenField ID="HdnAutoValue" runat="server" />
                                                        <asp:Button ID="btnSearch" runat="server" OnClick="btnSearch_Click"
                                                            Style="display: none" Text="" />
                                                        <asp:TextBox ID="txtEmployeeId" runat="server" CssClass="form-control" placeholder="Cadre Name"></asp:TextBox>
                                                        <cc1:AutoCompleteExtender ID="txtEmployeeId_AutoCompleteExtender"
                                                            runat="server" Enabled="True" OnClientItemSelected="UpdateItemSelected"
                                                            ServiceMethod="ShowListSearch"
                                                            TargetControlID="txtEmployeeId" CompletionInterval="250"
                                                            CompletionListCssClass="autocomplete_completionListElement"
                                                            CompletionListHighlightedItemCssClass="autocomplete_highlightedListItem"
                                                            CompletionListItemCssClass="autocomplete_listItem" CompletionSetCount="15"
                                                            EnableCaching="true" FirstRowSelected="true" MinimumPrefixLength="1"
                                                            UseContextKey="true">
                                                        </cc1:AutoCompleteExtender>
                                                        <asp:HiddenField ID="HdnSearch" runat="server" />
                                                    </div>
                                                    <!-- /input-group -->
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                    <div class="hs_margin_30"></div>
                                    <div class="clearfix"></div>
                                    <div class="hs_margin_40"></div>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 100%; text-align: left">
                                <div class="col-lg-8 col-md-8 col-sm-7">

                                    <div class="row">
                                        <div id="divalert" class="alert alert-success" runat="server">
                                            <strong>
                                                <asp:Label ID="lblMessage" runat="server"></asp:Label>
                                            </strong>
                                        </div>
                                    </div>

                                    <asp:GridView ID="GvCadre" runat="server" Width="100%" BorderStyle="None"
                                        AutoGenerateColumns="False" AllowPaging="True"
                                        OnPageIndexChanging="GvCadre_PageIndexChanging" PageSize="10"
                                        AllowSorting="True" OnRowCommand="GvCadre_RowCommand">
                                        <PagerStyle CssClass="hs_pager1" />
                                        <Columns>
                                            <asp:TemplateField>
                                                <ItemTemplate>
                                                    <div class="hs_comment">
                                                        <div class="row">
                                                            <div class="col-lg-9">
                                                                <div class="hs_comment_date">
                                                                    <ul>
                                                                        <li>
                                                                            <a>
                                                                                <asp:Label ID="lblQualification" runat="server" Text='<%#Eval("Cadrename") %>'>
                                                                                </asp:Label>
                                                                            </a>
                                                                        </li>
                                                                    </ul>
                                                                </div>
                                                            </div>
                                                            <div class="col-lg-3">
                                                                <asp:Button ID="btnRemove" runat="server" Text="Remove" CssClass="btn btn-success" CommandArgument='<%#Eval("CadreId_PK") %>' CommandName="DeleteCadre" OnClientClick="return confirm('Are you sure, you want to delete this Cadre?')" />
                                                                <asp:Button ID="BtnEdit" runat="server" Text="View" CommandArgument='<%#Eval("CadreId_PK") %>' CommandName="ViewCadre" CssClass="btn btn-success" />
                                                            </div>
                                                            <hr />
                                                            <div class="col-lg-2 col-md-2 col-sm-2">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                        </Columns>
                                    </asp:GridView>
                                    <div class="clearfix"></div>
                                    <div class="hs_margin_30"></div>
                                    <div class="clearfix"></div>
                                    <div class="hs_margin_40"></div>
                                </div>
                            </td>
                        </tr>
                    </table>
                    <div class="col-lg-12 col-md-12 col-sm-12">
                    </div>
                </div>
            </div>

        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
