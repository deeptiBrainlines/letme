﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Company/Company.Master" AutoEventWireup="true" CodeBehind="GetResult.aspx.cs" Inherits="HealToZeal.Company.GetResult" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="LeftColumn" runat="server">
    <div class="container">
        <h5>
           <asp:LinkButton ID="lbBack" runat="server" PostBackUrl="~/Employee/EmployeeAssessments.aspx" CssClass="btn btn-link btn-lg">Back</asp:LinkButton></h5>
   
        <h4 class="hs_heading" id="hs_appointment_form_link">My mindfulness report
        </h4>
                
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12">
                <div class="hs_single_profile">
                    <div class="hs_single_profile_detail">
                        <asp:Repeater ID="RptrEmployee" runat="server">
                            <ItemTemplate>
                                <h3>
                                    <%--<asp:Label ID="lblName" runat="server" Text='<%# Eval("EmployeeName") %>'></asp:Label>--%>
                                    <asp:Label ID="lblName" runat="server" Text="Poonam"></asp:Label>

                                </h3>
                                <div class="row">
                                    <div class="col-lg-3 col-md-6 col-sm-6">
                                        <i class="fa fa-calendar"></i>Date:
                                            <a href="">
                                                <asp:Label ID="lblDate" runat="server" Text='<%# DateTime.Now.ToString("dd/MM/yyyy") %>'></asp:Label>
                                            </a>
                                    </div>
                                
                                  
                                </div>
                            </ItemTemplate>
                        </asp:Repeater>
                    </div>
                </div>
            </div>
        </div>
        
        
    </div>
</asp:Content>
