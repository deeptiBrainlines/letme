﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Company/Company.Master" AutoEventWireup="true" CodeBehind="ShowResult.aspx.cs" Inherits="HealToZeal.Company.ShowResult" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="LeftColumn" runat="server">
    <asp:Label ID="lblcolor" runat="server" Text="Label" Visible="false"></asp:Label>
    <asp:Label ID="Label1" runat="server" Text="Batch Name:"></asp:Label>
    <asp:Label ID="Lblbatch" runat="server" Text="" Visible="true"></asp:Label>
    <br />
    <asp:Label ID="Label2" runat="server" Text="Company Name:"></asp:Label>
    <asp:Label ID="lblcompanyid" runat="server" Text="" Visible="true"></asp:Label>
    <br />
    <asp:Label ID="Lblnotdis" runat="server" Text="Undisclosed Employees:"></asp:Label>
    <asp:Label ID="lblundisclosed" runat="server" Text=""></asp:Label>
    <asp:GridView ID="gridresult" runat="server" AutoGenerateColumns="False" OnPageIndexChanging="gridresult_PageIndexChanging" AllowPaging="True" BackColor="White" BorderColor="#999999" BorderStyle="Solid" BorderWidth="1px" CellPadding="3" ForeColor="Black" GridLines="Vertical">
        <AlternatingRowStyle BackColor="#CCCCCC" />
        <Columns>
            <%--<asp:TemplateField ItemStyle-HorizontalAlign="Center" HeaderText="Company Id" HeaderStyle-Height="20px" HeaderStyle-Width="60px">
                <ItemTemplate>
                   <asp:Label ID="lblcompanyId" runat="server" Text='<%#Eval("CompanyId_FK") %>'  Visible="true"> </asp:Label>
                </ItemTemplate>
            </asp:TemplateField>--%>

             <asp:TemplateField ItemStyle-HorizontalAlign="Center" HeaderText="Employee Id" HeaderStyle-Height="20px" HeaderStyle-Width="60px" Visible="false">
                <ItemTemplate>
                   <asp:Label ID="lblEmpId" runat="server" Text='<%#Eval("EmployeeId_PK") %>'  Visible="false"> </asp:Label>
                </ItemTemplate>

<HeaderStyle Height="20px" Width="60px"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
            </asp:TemplateField>

             <asp:TemplateField ItemStyle-HorizontalAlign="Center" HeaderText="Employee CompanyId" HeaderStyle-Height="20px" HeaderStyle-Width="60px">
                <ItemTemplate>
                   <asp:Label ID="lblEmpcompanyId" runat="server" Text='<%#Eval("EmployeeCompanyId") %>'  Visible="true"> </asp:Label>
                </ItemTemplate>

<HeaderStyle Height="20px" Width="60px"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
            </asp:TemplateField>

             <asp:TemplateField ItemStyle-HorizontalAlign="Center" HeaderText="FirstName" HeaderStyle-Height="20px" HeaderStyle-Width="60px">
                <ItemTemplate>
                   <asp:Label ID="lblFirstName" runat="server" Text='<%#Eval("FirstName") %>'  Visible="true"> </asp:Label>
                </ItemTemplate>

<HeaderStyle Height="20px" Width="60px"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
            </asp:TemplateField>

             <asp:TemplateField ItemStyle-HorizontalAlign="Center" HeaderText="LastName" HeaderStyle-Height="20px" HeaderStyle-Width="60px">
                <ItemTemplate>
                   <asp:Label ID="lblLastName" runat="server" Text='<%#Eval("LastName") %>'  Visible="true"> </asp:Label>
                </ItemTemplate>

<HeaderStyle Height="20px" Width="60px"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
            </asp:TemplateField>

             <asp:TemplateField ItemStyle-HorizontalAlign="Center" HeaderText="Color" HeaderStyle-Height="20px" HeaderStyle-Width="60px">
                <ItemTemplate>
                   <asp:Label ID="lblColor" runat="server" Text='<%#Eval("Color") %>'  Visible="true"> </asp:Label>
                </ItemTemplate>

<HeaderStyle Height="20px" Width="60px"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
            </asp:TemplateField>

              <asp:TemplateField ItemStyle-HorizontalAlign="Center" HeaderText="ReadyToShareDetails" HeaderStyle-Height="20px" HeaderStyle-Width="60px">
                <ItemTemplate>
                   <asp:Label ID="lblreadytoshare" runat="server" Text='<%#Eval("ReadyToShareDetails") %>'  Visible="true"> </asp:Label>
                </ItemTemplate>

<HeaderStyle Height="20px" Width="60px"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
            </asp:TemplateField>
             <%--<asp:TemplateField ItemStyle-HorizontalAlign="Center" HeaderText="Batch" HeaderStyle-Height="20px" HeaderStyle-Width="60px">
                <ItemTemplate>
                   <asp:Label ID="lblBatchId" runat="server" Text='<%#Eval("BatchId") %>'  Visible="true"> </asp:Label>
                </ItemTemplate>
            </asp:TemplateField>--%>

        </Columns>


        <FooterStyle BackColor="#CCCCCC" />
        <HeaderStyle BackColor="Black" Font-Bold="True" ForeColor="White" />
        <PagerStyle BackColor="#999999" ForeColor="Black" HorizontalAlign="Center" />
        <SelectedRowStyle BackColor="#000099" Font-Bold="True" ForeColor="White" />
        <SortedAscendingCellStyle BackColor="#F1F1F1" />
        <SortedAscendingHeaderStyle BackColor="#808080" />
        <SortedDescendingCellStyle BackColor="#CAC9C9" />
        <SortedDescendingHeaderStyle BackColor="#383838" />


    </asp:GridView>
    <asp:Button ID="ExporttoExcel" runat="server" Text="Export to Excel" OnClick="ExporttoExcel_Click" />

</asp:Content>
