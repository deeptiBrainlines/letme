﻿
<%@ Page Title="" Language="C#" MasterPageFile="~/Company/Company.Master" AutoEventWireup="true" CodeBehind="EmployeeResultNew.aspx.cs" Inherits="HealToZeal.Company.EmployeeResultNew" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">


</asp:Content>
 
<asp:Content ID="Content2" ContentPlaceHolderID="LeftColumn" runat="server">
   
     <html xmlns="http://www.w3.org/1999/xhtml">
<head >
<title></title>
</head>
<body>
    <div>
        <label>Select Batch:</label>
    <div>
        <asp:DropDownList ID="ddlbatch" runat="server" OnSelectedIndexChanged="ddlbatch_SelectedIndexChanged" AutoPostBack="true">
            <asp:ListItem>---select---</asp:ListItem>
        </asp:DropDownList>
    </div>
    </div>

<div>     
    <asp:Label ID="lblAssessmentId1" runat="server"  Visible="true"> </asp:Label>
    <br />
    <br />

    <asp:Button  ID="btnexport" runat="server" Text="ExportToExcel" OnClick="btnexport_Click"/>
    <asp:GridView ID="GvAssessments" AutoGenerateColumns="False" runat="server" GridLines="Both" AllowPaging="false"  OnPageIndexChanging="GvAssessments_PageIndexChanging" OnRowCommand="GvAssessments_RowCommand"  PageSize="50" Width="800px" OnRowDataBound="GvAssessments_RowDataBound" Visible="false">
       <HeaderStyle HorizontalAlign="Center" />
         <Columns>
             
            <asp:TemplateField  ItemStyle-HorizontalAlign="Center" HeaderText="Roll No" HeaderStyle-Height="20px" HeaderStyle-Width="60px">
                <HeaderStyle  HorizontalAlign="Center" VerticalAlign="Middle" />
                <ItemTemplate>
                  <asp:Label ID="lblAssessmentId" runat="server" Text='<%#Eval("Sr No") %>'  Visible="true"> </asp:Label>
                   </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField ItemStyle-HorizontalAlign="Center" HeaderText="Self Assurance" HeaderStyle-Height="20px" HeaderStyle-Width="60px" >
                 <HeaderStyle  HorizontalAlign="Center"/>
                  <ItemTemplate>
                    <asp:Label ID="lblAssessmentInstanceId" runat="server" Text='<%#Eval("Self Assurance") %>'  Visible="true"> </asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField  ItemStyle-HorizontalAlign="Center" HeaderText="Emotional Balance" HeaderStyle-Height="20px" HeaderStyle-Width="60px">
                  <HeaderStyle  HorizontalAlign="Center"/>
                 <ItemTemplate>
                    <asp:Label ID="lblInstanceCompanyRelationId"  runat="server" Text='<%#Eval("Emotional Balance") %>' Visible="true"> </asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField  ItemStyle-HorizontalAlign="Center" HeaderText="Ability to Withstand Pressure" HeaderStyle-Height="20px" HeaderStyle-Width="60px" >
                  <HeaderStyle  HorizontalAlign="Center"/>
                 <ItemTemplate >
                    <asp:Label ID="lblEmployeeId" runat="server"  Text='<%#Eval("Ability to Withstand Pressure") %>' Visible="true"> </asp:Label>
                </ItemTemplate>
            </asp:TemplateField>

        <asp:TemplateField ItemStyle-HorizontalAlign="Center" HeaderText="Lack of Motivation" HeaderStyle-Height="20px" HeaderStyle-Width="60px">
               <HeaderStyle  HorizontalAlign="Center"/>   
             <ItemTemplate>
                    <asp:Label ID="lblAssessmentInstanceId1" runat="server" Text='<%#Eval("Lack of Motivation") %>'  Visible="true"> </asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField  ItemStyle-HorizontalAlign="Center" HeaderText="Coping Issues" HeaderStyle-Height="20px" HeaderStyle-Width="60px">
                  <HeaderStyle  HorizontalAlign="Center"/>
                 <ItemTemplate>
                    <asp:Label ID="lblInstanceCompanyRelationId1"  runat="server" Text='<%#Eval("Coping Issues") %>' Visible="true"> </asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField  ItemStyle-HorizontalAlign="Center" HeaderText="Self Esteem Issues" HeaderStyle-Height="20px" HeaderStyle-Width="60px">
                 <HeaderStyle  HorizontalAlign="Center"/>
                <ItemTemplate >
                    <asp:Label ID="lblEmployeeId1" runat="server"  Text='<%#Eval("Self Esteem Issues") %>' Visible="true"> </asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
             <asp:TemplateField  ItemStyle-HorizontalAlign="Center" HeaderText="Resultant Status" HeaderStyle-Height="20px" HeaderStyle-Width="60px">
                 <HeaderStyle  HorizontalAlign="Center"/>
                <ItemTemplate >
                   <%-- <asp:HyperLink ID="lblEmployeeId1" runat="server"  text="view report"  CommandArgument="'<%#Eval("Sr No") %>'"  CommandName="ReportID" Visible="true"> </asp:HyperLink>--%>
                </ItemTemplate>
            </asp:TemplateField>
              <asp:TemplateField  ItemStyle-HorizontalAlign="Center" HeaderText="Actual Status" HeaderStyle-Height="20px" HeaderStyle-Width="60px">
                 <HeaderStyle  HorizontalAlign="Center"/>
                <ItemTemplate >
                   <%-- <asp:HyperLink ID="lblEmployeeId1" runat="server"  text="view report"  CommandArgument="'<%#Eval("Sr No") %>'"  CommandName="ReportID" Visible="true"> </asp:HyperLink>--%>
                </ItemTemplate>
            </asp:TemplateField>
        </Columns>
    </asp:GridView>
    <asp:Label ID="Label1" runat="server" Text="Label"></asp:Label>
    <asp:Label ID="Label2" runat="server" Text="Label"></asp:Label>
     <asp:Label ID="Label3" runat="server" Text="Label"></asp:Label>
    <asp:Label ID="Label4" runat="server" Text="Label"></asp:Label>
     <asp:Label ID="Label5" runat="server" Text="Label"></asp:Label>
    </div>
       
    </body>
</html>   
</asp:Content>
     