﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="CompanyLogin.aspx.cs" Inherits="HealToZeal.Company.CompanyLogin" %>

        
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="keywords" content="Health Care, health" />
    <meta name="description" content="Health Care HTML Template" />
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <!-- favicon links -->
    <link rel="shortcut icon" type="image/ico" href="../favicon.ico" />
    <link rel="icon" type="image/ico" href="../favicon.ico" />
    <!--Google web fonts-->
    <link href='http://fonts.googleapis.com/css?family=Raleway' rel='stylesheet' type='text/css' />
    <!-- main css -->
    <link rel="stylesheet" href="../css/main.css" media="screen" />
    <%--<link rel="stylesheet" id="theme-color" type="text/css" href="#"/>--%>
    <!--page title-->
    <title>LetMeTalk2Me</title>

</head>
<!--
**********************************************************************************************************
    Copyright (c) 2014 Himanshu Softtech.
********************************************************************************************************** -->
<body>
   
    <div id="preloader">
        <div id="status">
            <img src="../images/loader.gif" id="preloader_image" width="36" height="36" alt="loading image" />
        </div>
    </div>
  
    <div id="style-switcher" class="hs_color_set">
        <div>
            <h3>color options</h3>
            <ul class="colors">
                <li>
                    <p class='colorchange' id='color'>
                    </p>
                </li>
                <li>
                    <p class='colorchange' id='color2'>
                    </p>
                </li>
                <li>
                    <p class='colorchange' id='color3'>
                    </p>
                </li>
                <li>
                    <p class='colorchange' id='color4'>
                    </p>
                </li>
                <li>
                    <p class='colorchange' id='color5'>
                    </p>
                </li>
                <li>
                    <p class='colorchange' id='style'>
                    </p>
                </li>
            </ul>
            <h3>Theme Option</h3>
            <select class="rock_themeoption" onchange="location = this.options[this.selectedIndex].value;">
                <option>Select Option</option>
                <option value="one-page.html">Single Page</option>
                <option value="index.html">Multi Pages</option>
            </select>         
        </div>
        <div class="bottom">
            <a class="settings"><i class="fa fa-gear"></i></a>
        </div>
    </div>    
    <header id="hs_header">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 clearfix ">
                    <div class="col-lg-2 col-md-2 col-sm-12">
                        <div id="hs_logo">
                            <a href="http://www.letmetalk2.me/"><img alt="letmetalktome" src="../images/logo1.png"/></a>
                           <a style="font-family:  Calibri;color:white; font-size:large; font-weight:bold;  text-transform:uppercase"> LetMeTalk2Me
                             </a>
                             <%-- <a>
                                <img src="../images/logo1.png" />
                            </a>
                            <a style="font-family: Calibri; color: white; font-size: large; font-weight: bold; text-transform: uppercase">LetMeTalk2.me
                            </a>--%>
                        </div>
                    </div>
                    <div class="col-lg-8 col-md-8 col-sm-12">
                        <button type="button" class="hs_nav_toggle navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">Menu<i class="fa fa-bars"></i></button>
                        <nav>
                            <ul class="hs_menu collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                                <li><a class="active" href="CompanyHome.aspx">Home</a>
                                </li>
                               <%-- <li><a href="#">About us</a>
                                    <ul>
                                        <li><a href="AboutUs.aspx">About us </a></li>
                                        <li><a href="OurTeam.aspx">Team</a></li>
                                    </ul>
                                </li>
                                <li><a href="ContactUs.aspx">Contact</a></li>--%>
                               <%-- <li><a href="RssFeeds.aspx">RSS Feeds</a></li>--%>
                            </ul>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
    </header> 
    <div id="carousel-example-generic" class="carousel slide carousel-fade" data-ride="carousel"> 
    </div>

     <div class="container-fluid">
    <div class="container">
        <div class="row">
            <div class="col-lg-6-new col-md-5 col-sm-12">
                <br />
                <div style="height: 100%; width: 100%">
                    <form id="form1" runat="server">
                        <div id="myTabContent" class="tab-content">
                            <h4 class="hs_theme_color">Log in</h4>
                            <table>
                                <tr>
                                    <td>
                                        <div class="form-group">
                                            <input type="text" id="UserName" runat="server" name="UserName"
                                                class="form-control" placeholder="User name(required)" required />
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="height: 50px">
                                        <div class="form-group">
                                            <input type="password" id="Password" runat="server" name="Password"
                                                class="form-control" placeholder="Password(required)" required />
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="height: 50px">
                                        <div class="form-group">
                                            <asp:Button ID="btnLogin" runat="server" Text="Login" CssClass="btn btn-default"
                                                OnClick="btnLogin_Click" ValidationGroup="0" />
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="height: 50px">
                                        <div class="form-group">
                                            <label id="message" runat="server"></label>
                                        </div>
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <p id="appointment_err">
                        </p>
                    </form>
                </div>
            </div>
        </div>   
        <div class="hs_margin_40">
        </div>    
        <div class="clearfix">
        </div>   
        <div class="hs_margin_60">
        </div>
    </div>
         </div>

    <footer id="hs_footer">
        <div class="container">
            <div class="hs_footer_content">
                <div class="row">
                    <div class="col-lg-8 col-md-8 col-sm-8">
                        <div class="row">
                            <div class="hs_footer_about_us">
                                <div class="col-lg-3 col-md-4 col-sm-12 col-md-12 col-sm-12">
                                   
                                        <%--<img src="../images/logo1.png" />--%>
                                       <%-- <p><a href="http://www.letmetalk2.me/"><img src="../images/logo1.png"></a></p>--%>

                                    
                                    <a style="font-family: Calibri; color: white; font-size: large; font-weight: bold; text-transform: uppercase">LetMeTalk2Me
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </footer>
    <div class="hs_copyright">
           Copyright © 2018 Clairvoyance Mindware Private Limited
    </div>
  
    <script type="text/javascript" src="../js/jquery-1.11.0.min.js"></script>
    <script type="text/javascript" src="../js/bootstrap.js"></script>
    <script type="text/javascript" src="../js/owl.carousel.js"></script>
    <script type="text/javascript" src="../js/jquery.bxslider.js"></script>
    <script type="text/javascript" src="../js/jquery.mixitup.min.js"></script>
    <script type="text/javascript" src="../js/smoothscroll.js"></script>
    <script type="text/javascript" src="../js/single-0.1.0.js"></script>
    <script type="text/javascript" src="../js/custom.js"></script>
   
</body>
</html>
