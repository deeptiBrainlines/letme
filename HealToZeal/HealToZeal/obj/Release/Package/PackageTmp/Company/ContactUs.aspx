﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Company/Company.Master" AutoEventWireup="true" CodeBehind="ContactUs.aspx.cs" Inherits="HealToZeal.Company.ContactUs" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="LeftColumn" runat="server">
    <div class="container">
        <div class="row">
            <div class="col-lg-4 col-md-5 col-sm-5">
                   <h4 class="hs_heading">Contact</h4>
      <div class="hs_contact">
        <ul>
          <li> <i class="fa fa-map-marker"></i>
            <p> 12, Parisar Apartments, Dr Ketkar Road , Pune 411004, India</p>
          </li>
          <li> <i class="fa fa-phone"></i>
            <p>+91-20-25453533</p>
            <p>+91-9011055047</p>
          </li>
          <li> <i class="fa fa-envelope"></i>
            <p><a href="Mailto:info@healtozeal.com">priyamvada.bavadekar@letmetalk2.me</a></p>
          
          </li>
        </ul>
      </div>
     <%-- <div class="hs_contact_social">
        <div class="hs_profile_social">
          <ul>
            <li><a href=""><i class="fa fa-facebook"></i></a></li>
            <li><a href=""><i class="fa fa-twitter"></i></a></li>
            <li><a href=""><i class="fa fa-linkedin"></i></a></li>
            <li><a href=""><i class="fa fa-skype"></i></a></li>
            <li><a href=""><i class="fa fa-youtube"></i></a></li>
          </ul>
        </div>
      </div>--%>
            </div>
        </div>
    </div>
</asp:Content>
