﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="AllClustersByEmployeeId.aspx.cs" Inherits="HealToZeal.Company.AllClustersByEmployeeId" %>

<%@ Register assembly="Microsoft.ReportViewer.WebForms, Version=11.0.0.0, Culture=neutral, PublicKeyToken=89845dcd8080cc91" namespace="Microsoft.Reporting.WebForms" tagprefix="rsweb" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>

    &nbsp;<asp:Label ID="Label1" runat="server" Text="Employee Id"></asp:Label>
&nbsp;<asp:TextBox ID="txtEmployeeId" runat="server"></asp:TextBox>
        <asp:Button ID="btnSubmit" runat="server" Text="Submit" OnClick="btnSubmit_Click" />
        <br />
        <rsweb:ReportViewer ID="ReportViewer1" runat="server" Font-Names="Verdana" Font-Size="8pt" WaitMessageFont-Names="Verdana" WaitMessageFont-Size="14pt" Width="1168px" Height="540px" ShowToolBar="False">
            <LocalReport ReportPath="Reports\EmployeeIdClusters.rdlc">
                <DataSources>
                    <rsweb:ReportDataSource DataSourceId="ObjectDataSource1" Name="DataSet1" />
                </DataSources>
            </LocalReport>
        </rsweb:ReportViewer>

        <asp:ObjectDataSource ID="ObjectDataSource1" runat="server" OldValuesParameterFormatString="original_{0}" SelectMethod="GetData" TypeName="HealToZeal.AllClustersTableAdapters.SpEmployeeAssessmentClusterwiseReportTableAdapter">
            <SelectParameters>
                <asp:ControlParameter ControlID="txtEmployeeId" DefaultValue="1" Name="EmployeeCompanyId" PropertyName="Text" Type="Int32" />
            </SelectParameters>
        </asp:ObjectDataSource>

    </div>
    </form>
</body>
</html>
