﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="UserResult.aspx.cs" Inherits="HealToZeal.Company.UserResult" %>

<%@ Register assembly="System.Web.DataVisualization, Version=4.0.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" namespace="System.Web.UI.DataVisualization.Charting" tagprefix="asp" %>

    
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body style="width: 1098px">
    <form id="form1" runat="server">

         <div>
    
        <br />
        <asp:Label ID="Label1" runat="server" Text="Employee Id"></asp:Label>
&nbsp;<asp:TextBox ID="txtEmployeeId" runat="server"></asp:TextBox>
        <asp:Button ID="btnSubmit" runat="server" Text="Submit" OnClick="btnSubmit_Click" />      
             <br />
             <table>
                 <tr>
                     <td>
                         <asp:Repeater ID="RptrEmployee" runat="server">                             
                             <ItemTemplate>                               
                                 <table style="background-color:#f0f0f0 ;width:900px">
                                     <tr>
                                         <td>
                                             <asp:Label ID="lblName" runat="server" Text='<%# "Name: "+Eval("EmployeeName") %>'></asp:Label>
                                         </td>
                                         <td>
                                             <asp:Label ID="lblDate" runat="server" Text='<%# "Date: "+  DateTime.Now.ToString("dd/MM/yyyy") %>'></asp:Label>
                                         </td>
                                     </tr>
                                     <tr>
                                         <td>
                                             <asp:Label ID="lblGender" runat="server" Text='<%# "Gender: "+Eval("Gender") %>'></asp:Label>
                                         </td>
                                         <td>
                                             <asp:Label ID="lblBirthDate" runat="server" Text='<%# "BirthDate: "+ DataBinder.Eval(Container.DataItem, "Age", "{0:dd/MM/yyyy}") %>'></asp:Label>
                                         </td>
                                     </tr>
                                     <tr>
                                         <td>
                                             <asp:Label ID="lblSubmittedDate" runat="server" Text='<%# "Assessment Submitted Date: "+ DataBinder.Eval(Container.DataItem, "CreatedDate", "{0:dd/MM/yyyy}") %>'></asp:Label>
                                         </td>
                                         <td></td>

                                     </tr>
                                 </table>

                             </ItemTemplate>
                         </asp:Repeater>
                     </td>
                 </tr>
                 <tr style="align-content:center">
                   
                     <td >  
                         <br />
                         <br />
                       <asp:GridView ID="GvResult" runat="server" AutoGenerateColumns="False" GridLines="None" BackColor="#d2e9ff" >
                             <Columns>
                                 <asp:TemplateField  HeaderText="Result"  HeaderStyle-Font-Bold="true" HeaderStyle-Font-Size="Medium" HeaderStyle-ForeColor="Maroon">
                                     <ItemTemplate >
                                         <table>
                                             <tr>
                                                 <%--   ='<%# string.IsNullOrEmpty(Eval("Result").ToString()) ? "false" : "true"%>'>--%>
                                                 <td style="width:40px; vertical-align:top" >
                                                         <asp:Label ID="lblColor" runat="server" Text="" Height="20px" Width="20px" BackColor='<%# System.Drawing.Color.FromName((string) Eval("Color")) %>'> </asp:Label>
                                                 </td>
                                                 <td style="width:800px">                                              
                                                     <%--          <asp:Label ID="Label3" runat="server" Text='<%#(String.IsNullOrEmpty(Eval("Address3").ToString()) ? " " : "," + Eval("Address3"))%> '></asp:Label>--%>
                                                     <%--  <asp:Label ID="lblResultText" runat="server" Text='<%#Eval("Result") %>'  ForeColor='<%# System.Drawing.Color.FromName((string) Eval("Color")) %>'></asp:Label>--%>
                                                     <asp:Label ID="lblResultText" runat="server" Text='<%#Eval("Result")%> '> </asp:Label>

                                                 </td>
                                                
                                             </tr>
                                         </table>
                                     </ItemTemplate>
                                 </asp:TemplateField>
                             </Columns>
                         </asp:GridView>
                     </td>
                 </tr>
                 <tr>
                     <td style="align-content:center">
                   
                         <br />
                         <asp:GridView ID="GvRemedyObservation" runat="server" AutoGenerateColumns="False" GridLines="None" BackColor="#d2e9ff" >
                             <Columns>
                               
                                 <asp:TemplateField HeaderText="Observations"  HeaderStyle-Font-Bold="true" HeaderStyle-Font-Size="Medium" HeaderStyle-ForeColor="Maroon">
                                     <ItemTemplate>
                                         <table>
                                             <tr>
                                                 <td style="width:40px; vertical-align:top" >
                                                         <asp:Label ID="lblColor" runat="server" Text="" Height="20px" Width="20px" BackColor='<%# System.Drawing.Color.FromName((string) Eval("Color")) %>'> </asp:Label>
                                                 </td>
                                                 <td  style="width:800px; text-align:left" >
                                                         <asp:Label ID="lblSuggestion" runat="server" Text='<%#Eval("Suggestion") %>' ></asp:Label>
                                                 </td>
                                             </tr>
                                         </table>                                     
                                     </ItemTemplate>
                                 </asp:TemplateField>
                             </Columns>
                         </asp:GridView>
                     </td>
                 </tr>
                 <tr>
                     <td>
                     
                         <br />
                         <asp:GridView ID="GvSuggestion" runat="server" AutoGenerateColumns="False" GridLines="None"   BackColor="#d2e9ff">
                             <Columns>                             
                                 <asp:TemplateField HeaderText="Suggestion"  HeaderStyle-Font-Bold="true" HeaderStyle-Font-Size="Medium" HeaderStyle-ForeColor="Maroon">
                                     <ItemTemplate>
                                         <table>
                                             <tr>
                                                 <td style="width:40px;"></td>
                                                 <td   style="width:800px;">
                                                       <asp:Label ID="lblSuggestionText" runat="server" Text='<%# Eval("Title")+"- "+Eval("SuggestionText") %>'></asp:Label>
                                                 </td>
                                             </tr>
                                         </table>                                       
                                     </ItemTemplate>
                                 </asp:TemplateField>
                             </Columns>
                         </asp:GridView>
                     </td>
                 </tr>
                 <tr>
                     <td>
                      
                         <br />
                         <asp:GridView ID="GvStandard" runat="server" AutoGenerateColumns="False" GridLines="None"  BackColor="#d2e9ff" Width="840px" >
                             <Columns>

                                 <asp:TemplateField HeaderText="Assessment">
                                     <ItemTemplate>
                                         <asp:Label ID="lblAssessmentName" runat="server" Text='<%#Eval("AssessmentName") %>'></asp:Label>
                                     </ItemTemplate>
                                 </asp:TemplateField>
                                 <asp:TemplateField HeaderText="Cluster">
                                     <ItemTemplate>
                                         <asp:Label ID="lblClusterName" runat="server" Text='<%#Eval("ClusterName") %>'></asp:Label>
                                     </ItemTemplate>
                                 </asp:TemplateField>
                                 <asp:TemplateField HeaderText="Score">
                                     <ItemTemplate>
                                         <asp:Label ID="lblScore" runat="server" Text='<%#Eval("ClusterScore") %>'></asp:Label>
                                     </ItemTemplate>
                                 </asp:TemplateField>
                                 <asp:TemplateField HeaderText="NoOfQuestions">
                                     <ItemTemplate>
                                         <asp:Label ID="lblTotalQuestions" runat="server" Text='<%#Eval("NoofQuestions") %>'></asp:Label>
                                     </ItemTemplate>
                                 </asp:TemplateField>
                                 <asp:TemplateField HeaderText="NoOfMaxScoredAnswers">
                                     <ItemTemplate>
                                         <asp:Label ID="Label2" runat="server" Text='<%#Eval("AnswerCount") %>'></asp:Label>
                                     </ItemTemplate>
                                 </asp:TemplateField>
                             </Columns>
                         </asp:GridView>
                     </td>
                 </tr>
             </table>    
             <br />  
        <br />
        <asp:GridView ID="GvEmployeeResult" runat="server" style="margin-right: 0px" AutoGenerateColumns="False" GridLines="None">
            <Columns>
                <asp:TemplateField  HeaderText="Assessment">
                    <ItemTemplate>
                      <%--  <table>
                            <tr>
                                 <td>--%>
                                       <asp:Label ID="Label9" runat="server" Text='<%#Eval("AssessmentName") %>'></asp:Label>
                               <%-- </td>
                                
                                   <td>
                                       <asp:Label ID="lblClusterName" runat="server" Text='<%#Eval("ClusterName") %>' ></asp:Label>
                                </td>
                                <td>
                                       <asp:Label ID="lblScore" runat="server" Text='<%#Eval("Score") %>'></asp:Label>
                                </td>
                                 <td>
                                       <asp:Label ID="lblTotalQuestions" runat="server" Text='<%#Eval("TotalQuestions") %>' ></asp:Label>
                                </td>
                                  <td>
                                       <asp:Label ID="lblansscore" runat="server" Text='<%#Eval("ansscore") %>'></asp:Label>
                                </td>
                                  <td>
                                       <asp:Label ID="lbloutofScore" runat="server" Text='<%#Eval("outofScore") %>'></asp:Label>
                                </td>
                                  <td>
                                       <asp:Label ID="lblpercentage" runat="server" Text='<%#Eval("percentage") %>' ></asp:Label>
                                </td>--%>
                            <%--</tr>

                        </table>--%>

                    </ItemTemplate>
                </asp:TemplateField>
               <asp:TemplateField  HeaderText="Cluster">
                   <ItemTemplate>
                         <asp:Label ID="Label10" runat="server" Text='<%#Eval("ClusterName") %>' ></asp:Label>
                   </ItemTemplate>
               </asp:TemplateField>
                  <asp:TemplateField  HeaderText="Score">
                   <ItemTemplate>
                             <asp:Label ID="Label11" runat="server" Text='<%#Eval("Score") %>'></asp:Label>
                   </ItemTemplate>
               </asp:TemplateField>
                   <asp:TemplateField  HeaderText="TotalQuestions">
                   <ItemTemplate>
                       <asp:Label ID="Label12" runat="server" Text='<%#Eval("TotalQuestions") %>' ></asp:Label>
                   </ItemTemplate>
               </asp:TemplateField>
                   <asp:TemplateField  HeaderText="ansscore">
                   <ItemTemplate>
                               <asp:Label ID="lblansscore" runat="server" Text='<%#Eval("ansscore") %>'></asp:Label>
                   </ItemTemplate>
               </asp:TemplateField>
           <asp:TemplateField  HeaderText="outofScore">
                   <ItemTemplate>
                              <asp:Label ID="lbloutofScore" runat="server" Text='<%#Eval("outofScore") %>'></asp:Label>
                   </ItemTemplate>
               </asp:TemplateField>
                     <asp:TemplateField  HeaderText="percentage">
                   <ItemTemplate>
                <asp:Label ID="Label13" runat="server" Text='<%#Eval("percentage") %>' ></asp:Label>
                   </ItemTemplate>
               </asp:TemplateField>
            </Columns>
        </asp:GridView>
    
    </div>
    </form>
</body>

</html>