﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Company/Company.Master" AutoEventWireup="true" CodeBehind="ImportEmployeesdata .aspx.cs" Inherits="HealToZeal.Company.ImportEmployeesdata" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="LeftColumn" runat="server">
    <div>
        <label>Select Batch:</label>
    <div>
        <asp:DropDownList ID="ddlbatch" runat="server"></asp:DropDownList>
    </div>
    </div>
    <br />
    <br />
     <asp:FileUpload ID="exampleInputFile"  runat="server"/> 
                              <p class="help-block text-center"></p>
         <asp:Button ID="btnimport" Text="Import" runat="server"  OnClientClick="return preventMultipleSubmissions();" OnClick="btnimport_Click1" />
                       <br />
    <script type="text/javascript">
 
  var isSubmitted = false;
 
  function preventMultipleSubmissions() {
 
  if (!isSubmitted) {
 
     $('#<%=btnimport.ClientID %>').val('Submitting.. Plz Wait..');
 
    isSubmitted = true;
 
    return true;
 
  }
 
  else {
 
    return false;
 
  }
 
}
 
    </script>
</asp:Content>
