﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Company/Company.Master" AutoEventWireup="true"
    CodeBehind="AddEmployee.aspx.cs" Inherits="HealToZeal.Company.AddEmployee" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style>
        .hs_pager1
        {
            display: inline-block;
            list-style: none;
            padding-left: 0px;
        }
        .hs_pager1 a
        {
            padding: 5px 12px;
            color: #7f9aa0;
            border: 1px solid #7f9aa0;
            text-align: center;
            font-weight: bold;
            font-size: 16px;
            -webkit-transition: all 0.5s;
            -moz-transition: all 0.5s;
            -ms-transition: all 0.5s;
            -o-transition: all 0.5s;
            margin-bottom: 60px;
        }
        .hs_pager1 a:hover, .hs_pager1 a:active
        {
            color: #00ac7a;
            border: 1px solid #00ac7a;
            -webkit-transition: all 0.5s;
            -moz-transition: all 0.5s;
            -ms-transition: all 0.5s;
            -o-transition: all 0.5s;
        }
        .pagination
        {
            line-height: 26px;
        }
        
        .pagination span
        {
            padding: 5px;
            border: solid 1px #477B0E;
            text-decoration: none;
            white-space: nowrap;
            background: #547B2A;
        }
        
        .pagination a, .pagination a:visited
        {
            text-decoration: none;
            padding: 6px;
            white-space: nowrap;
        }
        .pagination a:hover, .pagination a:active
        {
            padding: 5px;
            border: solid 1px #9ECDE7;
            text-decoration: none;
            white-space: nowrap;
            background: #486694;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="LeftColumn" runat="server">
    <div class="hs_tab">
        <ul id="myTab" class="nav nav-tabs">
            <li class="active"><a href="#ViewEmp" data-toggle="tab">View employee</a></li>
            <li><a href="#AddEditEmp" data-toggle="tab">Add/Update Employee</a></li>
        </ul>
        <div id="myTabContent" class="tab-content">
            <div class="tab-pane fade" id="ViewEmp">
                <div class="row">
                    <div class="hs_sidebar">
                        <h4 class="hs_heading">Employee list</h4>
                        <div class="health_care_advice">
                            <asp:GridView ID="GvViewEmployee" runat="server" AutoGenerateColumns="False" Width="100%"
                                CellPadding="6" CellSpacing="6" AllowPaging="true" PageSize="3">
                                <PagerStyle CssClass="hs_pager1" />
                                <Columns>
                                    <asp:TemplateField>
                                        <ItemTemplate>
                                            <table style="width: 100%">
                                                <tr>
                                                    <td>
                                                        <div class="hs_post">
                                                            <asp:Label ID="lblEmployeeCompanyId" runat="server" Text='<%#Eval("Company") %>'>
                                                            </asp:Label>
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <div class="hs_post">
                                                            <h4>
                                                                <asp:Label ID="lblEmployeeName" runat="server" Text='<%#Eval("EmployeeName") %>'>
                                                                </asp:Label>
                                                            </h4>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <div class="hs_post">
                                                            <asp:Label ID="lblEmailId" runat="server" Text='<%#Eval("EmailId") %>'>
                                                            </asp:Label>
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <div class="hs_post">
                                                            <asp:Label ID="lblMobileNo" runat="server" Text='<%#Eval("MobileNo") %>'>
                                                            </asp:Label>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td colspan="2">
                                                        <div class="hs_post">
                                                            <asp:Label ID="lblEmployeeAddress" runat="server" Text='<%#Eval("EmployeeAddress") %>'>
                                                            </asp:Label>
                                                        </div>
                                                        <%-- hidden fields--%>
                                                        <div class="hs_post" style="display: none">
                                                            <asp:Label ID="lblEmployeeId" runat="server" Text='<%#Eval("EmployeeId_PK") %>' Visible="false"></asp:Label>
                                                        </div>
                                                        <div class="hs_post" style="display: none">
                                                            <asp:Label ID="lblCompanyId_FK" runat="server" Text='<%#Eval("CompanyId_FK") %>'
                                                                Visible="false"></asp:Label>
                                                        </div>
                                                        <div class="hs_post" style="display: none">
                                                            <asp:Button ID="btnEdit" runat="server" Text="Edit" CommandArgument='<%#Eval("EmployeeId_PK") %>'
                                                                Visible="false" CommandName="EditEmployee" />
                                                        </div>
                                                    </td>
                                                </tr>
                                            </table>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <%--  <asp:TemplateField HeaderText="CompanyId">                                 
                                    <ItemStyle BorderStyle="None" />
                                    <ItemTemplate>
                                    <div class="hs_post">
                                    <asp:Label ID="lblEmployeeCompanyId" runat="server" Text='<%#Eval("Company") %>'>
                                        </asp:Label>
                                    </div>                                        
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Name">
                                    
                                    <ItemStyle BorderStyle="None" />
                                    <ItemTemplate>
                                     <div class="hs_post">
                              <asp:Label ID="lblEmployeeName" runat="server" Text='<%#Eval("EmployeeName") %>'>
                                        </asp:Label>
                                      </div>                                      
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Address">
                                    
                                    <ItemStyle BorderStyle="None" />
                                    <ItemTemplate>
                                     <div class="hs_post">
                              <asp:Label ID="lblEmployeeAddress" runat="server" Text='<%#Eval("EmployeeAddress") %>'>
                                        </asp:Label>
                              </div>
                                      
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Email id">
                                   
                                    <ItemStyle BorderStyle="None" />
                                    <ItemTemplate>
                                    <div class="hs_post">
                                        <asp:Label ID="lblEmailId" runat="server" Text='<%#Eval("EmailId") %>'>
                                        </asp:Label>
                                        </div>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Mobile no.">
                                   
                                    <ItemStyle BorderStyle="None" />
                                    <ItemTemplate>
                                    <div class="hs_post">
                                        <asp:Label ID="lblMobileNo" runat="server" Text='<%#Eval("MobileNo") %>'>
                                        </asp:Label>
                                       </div>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField>
                                    
                                    <ItemStyle BorderStyle="None" />
                                    <ItemTemplate>
                                    <div class="hs_post">
                                        <asp:Label ID="lblEmployeeId" runat="server" Text='<%#Eval("EmployeeId_PK") %>' Visible="false"></asp:Label>
                                        </div>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField>
                                   
                                    <ItemStyle BorderStyle="None" />
                                    <ItemTemplate>
                                    <div class="hs_post">
                                        <asp:Label ID="lblCompanyId_FK" runat="server" Text='<%#Eval("CompanyId_FK") %>'
                                            Visible="false"></asp:Label>
                                            </div>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField>
                                    
                                    <ItemStyle BorderStyle="None" />
                                    <ItemTemplate>
                                    <div class="hs_post">
                                        <asp:Button ID="btnEdit" runat="server" Text="Edit" CommandArgument='<%#Eval("EmployeeId_PK") %>'
                                            Visible="false" CommandName="EditEmployee" />
                                            </div>
                                    </ItemTemplate>
                                </asp:TemplateField>--%>
                                </Columns>
                            </asp:GridView>
                        </div>
                    </div>
                </div>
            </div>
            <div class="tab-pane fade in active" id="AddEditEmp">
                <div class="row">
                    <div class="col-lg-6-new col-md-6 col-sm-6">
                        <h4 class="hs_theme_color">
                            Register new employee.</h4>
                        <div class="hs_margin_30">
                        </div>
                        <table>
                            <tr>
                                <td style="height: 50px">
                                    <div class="form-group">
                                        <asp:DropDownList ID="ddlCadre" runat="server" CssClass="form-control">
                                        </asp:DropDownList>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td style="height: 50px">
                                    <div class="form-group">
                                        <input type="text" id="EmployeeCompanyId" runat="server" name="EmployeeCompanyId"
                                            class="form-control" placeholder="Employee company id (required)" required />
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td style="height: 50px">
                                    <div class="form-group">
                                        <input type="text" id="FirstName" runat="server" name="fname" class="form-control"
                                            placeholder="First Name (required)" required />
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td style="height: 50px">
                                    <div class="form-group">
                                        <input id="LastName" name="lname" runat="server" class="form-control" placeholder="Last Name (required)"
                                            required />
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td style="height: 50px">
                                    <div class="form-group">
                                        <input type="text" id="EmailId" runat="server" name="EmailID" class="form-control"
                                            placeholder="Email (required)" required />
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td style="height: 50px">
                                    <div class="form-group">
                                        <asp:Button ID="btnAddEmployee" runat="server" Text="Add employee" CssClass="btn btn-default"
                                            OnClick="btnAddEmployee_Click" />
                                    </div>
                                </td>
                            </tr>
                        </table>
                    </div>
                    <div class="hs_margin_30">
                    </div>
                    <%--<a href="../blog-single-post-rightsidebar.html" class="btn btn-default">Read more</a>--%>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
