﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Company/Company.Master" AutoEventWireup="true" CodeBehind="Charts1.aspx.cs" Inherits="HealToZeal.Company.Charts1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="LeftColumn" runat="server">
    <asp:Label ID="Label1" runat="server" Text="Summary of pie chart"></asp:Label>
    <div class="col-md-4">
<asp:Label>Select Batch</asp:Label>
        <asp:DropDownList ID="ddlbatch" runat="server" OnSelectedIndexChanged="ddlbatch_SelectedIndexChanged" AutoPostBack="true">

            <asp:ListItem>---select---</asp:ListItem>
        </asp:DropDownList>
   
         </div>

     <asp:chart id="Chart1" runat="server" Height="300px" Width="400px" OnClick="Chart1_Click">
         
  <titles>
      
    <asp:Title ShadowOffset="3" Name="Title1" />
  </titles>
  <legends>
    <asp:Legend Alignment="Center" Docking="Bottom"
                IsTextAutoFit="False" Name="Default"
                LegendStyle="Row" />
  </legends>
  <series>
    <asp:Series Name="Default" />
  </series>
  <chartareas>
    <asp:ChartArea Name="ChartArea1"
                     BorderWidth="0" />
  </chartareas>
</asp:chart>

</asp:Content>
