﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Company/Company.Master" AutoEventWireup="true" CodeBehind="AssessmentResults.aspx.cs" Inherits="HealToZeal.Company.AssessmentResults" %>

<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=11.0.0.0, Culture=neutral, PublicKeyToken=89845dcd8080cc91" Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="LeftColumn" runat="server">
    <%-- <asp:UpdatePanel ID="UpPnl" runat="server">
        <ContentTemplate>--%>
            <div class="container">
                <h4 class="hs_heading" id="hs_appointment_form_link">Mindfulness overview
                </h4>

                <div class="row">
                     <div class="col-lg-4 col-md-4 col-sm-4" style="font-size: medium" align="left">
                        <asp:DropDownList ID="ddlAssessmentInstance" runat="server" CssClass="form-control" AutoPostBack="true" OnSelectedIndexChanged="ddlAssessmentInstance_SelectedIndexChanged" Visible="true"></asp:DropDownList>
                        <%-- <asp:Label></asp:Label> --%>
                         <br />
                          </div>
                       <div class="col-lg-4 col-md-4 col-sm-4" style="font-size: medium" align="center">
                      
                         <asp:RadioButtonList ID="rbbatch"  runat="server"   CellPadding="3" CellSpacing="2"  style="text-decoration:underline ;color:green " AutoPostBack="true" OnSelectedIndexChanged="rbbatch_SelectedIndexChanged" RepeatDirection="Horizontal" CssClass="form-control1" ValidationGroup="0" Visible="false">
                                   
                                    <asp:ListItem   Value="B">&nbsp; Batch wise</asp:ListItem>
                                    <asp:ListItem Id="ff"  Value="B" > Bat</asp:ListItem>
                                    <asp:ListItem  Value="D">&nbsp; Department wise</asp:ListItem>
                                    
                                </asp:RadioButtonList>
                           
                       <div class="col-lg-4 col-md-4 col-sm-4" style="font-size: medium" align="center">
                      
                    </div>
                </div>
                    </div>
              
                 <div class="row">
                        <%--style="color: #333;  outline: none;  padding-left: 3px;  padding-right: 3px; text-decoration: underline"--%>
               
                          <div class="col-lg-4 col-md-4 col-sm-4" style="font-size: medium" align="left">
                           <asp:Label Height="10px">Select Batch</asp:Label>
                          <asp:DropDownList ID="ddBatch" runat="server" CssClass="form-control" OnSelectedIndexChanged="ddBatch_SelectedIndexChanged" AutoPostBack="true"></asp:DropDownList>
                                 
                           
                            </div>
                        </div> 
                           
                    <div class="row">
                    <div class="col-lg-4 col-md-4 col-sm-4" style="font-size: medium" align="right">
                        <asp:Label></asp:Label>
                        <asp:DropDownList ID="dddepartment" runat="server" CssClass="form-control" OnSelectedIndexChanged="dddepartment_SelectedIndexChanged" AutoPostBack="true" Visible="false"></asp:DropDownList>
                         
                    </div>


                </div>
                <div class="row">

                    <asp:Repeater ID="RptrEmployee" runat="server">
                        <ItemTemplate>
                            <div class="col-lg-10 col-md-10 col-sm-10" style="font-size: medium">
                                <div class="hs_single_profile">
                                    <%-- <div class="hs_single_profile_detail">--%>
                                    <div class="row">
                                        <div class="col-lg-4">
                                            Total Employees: 
                                     <a>
                                         <asp:Label ID="lblEmployeeCompanyId" runat="server" Text='<%#Eval("Total Employees") %>'> </asp:Label>
                                     </a>
                                        </div>

                                        <div class="col-lg-4">
                                            Test attempted: <a>
                                                <asp:Label ID="lblCadre" runat="server" Text='<%#Eval("Test attempted") %>'> </asp:Label>
                                            </a>
                                        </div>
                                    </div>
                                    <hr />
                                    <div class="row">
                                        <div class="col-lg-3">
                                            <div class="col-lg-12">
                                                <a>
                                                    <asp:Label ID="LblStress1" runat="server" Text='<%#Eval("Stress1") %>'> </asp:Label>
                                                </a>
                                            </div>
                                            <div class="col-lg-12 ">
                                                <a>
                                                    <asp:Label ID="lblEmailId" runat="server" Text='<%# Eval("Stress2") %>'> </asp:Label>
                                                </a>
                                            </div>
                                            <div class="col-lg-12 ">
                                                <a>
                                                    <asp:Label ID="Label1" runat="server" Text='<%# Eval("Stress3") %>'> </asp:Label>
                                                </a>

                                            </div>
                                        </div>
                                        <div class="col-lg-3">
                                            <div class="col-lg-12 ">
                                                <a>
                                                    <asp:Label ID="Label2" runat="server" Text='<%# Eval("Mood1") %>'> </asp:Label>
                                                </a>
                                            </div>
                                            <div class="col-lg-12 ">
                                                <a>
                                                    <asp:Label ID="Label3" runat="server" Text='<%# Eval("Mood2") %>'> </asp:Label>
                                                </a>
                                            </div>

                                            <div class="col-lg-12 ">
                                                <a>
                                                    <asp:Label ID="Label4" runat="server" Text='<%# Eval("Mood3") %>'> </asp:Label>
                                                </a>
                                            </div>
                                        </div>
                                        <div class="col-lg-3">
                                            <div class="col-lg-12 ">
                                                <a>
                                                    <asp:Label ID="Label5" runat="server" Text='<%# Eval("Anxiety1") %>'> </asp:Label>
                                                </a>
                                            </div>
                                            <div class="col-lg-12">
                                                <a>
                                                    <asp:Label ID="Label6" runat="server" Text='<%# Eval("Anxiety2") %>'> </asp:Label>
                                                </a>
                                            </div>
                                            <div class="col-lg-12 ">
                                                <a>
                                                    <asp:Label ID="Label7" runat="server" Text='<%# Eval("Anxiety3") %>'> </asp:Label>
                                                </a>
                                            </div>
                                        </div>
                                        <div class="col-lg-3">
                                            <div class="col-lg-12">
                                                <a>
                                                    <asp:Label ID="Label8" runat="server" Text='<%# Eval("Other1") %>'> </asp:Label>
                                                </a>
                                            </div>
                                            <div class="col-lg-12">
                                                <a>
                                                    <asp:Label ID="Label9" runat="server" Text='<%# Eval("Other2") %>'> </asp:Label>
                                                </a>
                                            </div>
                                        </div>

                                          <label id="message" runat="server"></label>

                                    </div>
                                    <%--</div>--%>
                                </div>
                        </ItemTemplate>
                    </asp:Repeater>
                </div>
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12" style="font-size: medium">
                        <rsweb:ReportViewer ID="ReportViewer1" runat="server" Font-Names="Verdana" Font-Size="8pt" Height="252px" WaitMessageFont-Names="Verdana" WaitMessageFont-Size="14pt" Width="950px" ShowBackButton="False" ShowCredentialPrompts="False" ShowDocumentMapButton="False" ShowExportControls="False" ShowFindControls="False" ShowPageNavigationControls="False" ShowParameterPrompts="False" ShowPrintButton="False" ShowPromptAreaButton="False" ShowRefreshButton="False" ShowToolBar="False" ShowWaitControlCancelLink="False" ShowZoomControl="False" AsyncRendering="False">
                            <LocalReport ReportPath="Reports\AssessmentPercentage.rdlc">
                                <DataSources>
                                    <rsweb:ReportDataSource DataSourceId="ObjectDataSource1" Name="DataSet1" />
                                </DataSources>
                            </LocalReport>
                        </rsweb:ReportViewer>
                        <asp:ObjectDataSource ID="ObjectDataSource1" runat="server" OldValuesParameterFormatString="original_{0}" SelectMethod="GetData" TypeName="HealToZeal.AssessmentPercentageTableAdapters.SpClusterPercentageTableAdapter">
                            <SelectParameters>
                                <asp:SessionParameter DbType="Guid" Name="CompanyId" SessionField="CompanyId" />
                                <asp:ControlParameter ControlID="ddlAssessmentInstance" DbType="Guid" Name="AssessmentInstanceId" PropertyName="SelectedValue" />
                                <asp:ControlParameter ControlID="dddepartment" DbType="Guid"  Name="DeptID_FK" PropertyName="SelectedValue" />
                            </SelectParameters>
                        </asp:ObjectDataSource>
                    </div>
                </div>
                <div class="col-lg-12 col-md-12 col-sm-12" style="font-size: medium">
                    <div class="hs_contact">
                        <ul>
                            <li> 
                                <p>
                                    Yellow Zone :Recommended :Life style changes and undergo a followup test after 3 months.
                                <br />
                                    Red Zone : Seek professional help                                    
                                </p>
                            </li>
                            <li>
                                <p>
                                    <h4 class="hs_heading" id="hs_appointment_form_link"><strong>Action points for HR :</strong> </h4>
                                   This is a reflection of your employees' psychological well being. It would be advisable to keep running this excersize every quarter. You may go for professional help as an organization as well as individual employees.
                                    <asp:Button ID="BtnSetUpMeeting" Font-Bold="true" CssClass="btn btn-link" runat="server" Text="Contact for Appointment" OnClick="BtnSetUpMeeting_Click" />
                                   <%-- <p>
                                    </p>
                                    <p>
                                    </p>--%>
                                </p>
                                
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="row" id="DivMailBody" runat="server" visible="false">
                    <div class="col-lg-8 col-md-7 col-sm-7">
                        <div class="row" id="DivTitle" runat="server" visible="false">
                            <div id="div7" class="alert alert-info" runat="server">
                                <strong>
                                    <asp:Label ID="Label14" runat="server" Text='Call 91-9850864727 or please write an email with your contact number. We will get back to you soon.'></asp:Label>
                                </strong>
                            </div>
                        </div>

                        <div class="hs_comment_form">
                            <div class="row">
                                <div class="col-lg-6 col-md-6 col-sm-12">
                                    <div>
                                        <asp:Label Visible="false" ID="Label15" runat="server" Text='From'></asp:Label>
                                        <input visible="false" disabled="disabled" id="uemail" runat="server" type="text" class="form-control" placeholder="From" />
                                    </div>
                                    <!-- /input-group -->
                                </div>
                                <!-- /.col-lg-6 -->
                                <div class="col-lg-6 col-md-6 col-sm-12">
                                    <div>
                                        <asp:Label ID="Label16" Visible="false" runat="server" Text='To'></asp:Label>
                                        <input disabled="disabled" runat="server" visible="false" id="Text4" type="text" class="form-control" placeholder="healtozeal@chetanaTSPL.com" />
                                    </div>
                                    <!-- /input-group -->
                                </div>
                                <!-- /.col-lg-6 -->
                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <textarea id="message" runat="server" class="form-control" rows="8" cols="20"></textarea>
                                      

                                    </div>
                                    <!-- /input-group -->
                                </div>
                                <p id="err"></p>
                                <div class="form-group">
                                    <div class="col-lg-8 col-md-8 col-sm-12">
                                        <div class="checkbox">
                                            <label>
                                                <input type="checkbox" id="hs_checkbox" class="css-checkbox lrg" checked="checked" />

                                            </label>
                                        </div>
                                    </div>
                                    <div class="col-lg-4 col-md-4 col-sm-12">
                                        <asp:Button ID="Button3" runat="server" CssClass="btn btn-success pull-right" Text="Submit" OnClick="BtnSendEmail_Click" />
                                        <%--<button id="em_sub" class="btn btn-success pull-right" type="submit">Send</button>--%>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
       <%-- </ContentTemplate>
    </asp:UpdatePanel>--%>
</asp:Content>
