﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="DatewiseScore.aspx.cs" Inherits="HealToZeal.Company.DatewiseScore" %>

<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=11.0.0.0, Culture=neutral, PublicKeyToken=89845dcd8080cc91" Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
        <br />
        <asp:Label ID="lblEmployeeId" runat="server" Text="Employee Id"></asp:Label>
&nbsp;<asp:TextBox ID="txtEmployeeId" runat="server"></asp:TextBox>
        <asp:Button ID="btnSubmit" runat="server" OnClick="btnSubmit_Click" Text="Submit" />
        <rsweb:ReportViewer ID="ReportViewer1" runat="server" Font-Names="Verdana" Font-Size="8pt" Height="483px" WaitMessageFont-Names="Verdana" WaitMessageFont-Size="14pt" Width="746px">
            <LocalReport ReportPath="Reports\DatewiseScore.rdlc">
                <DataSources>
                    <rsweb:ReportDataSource DataSourceId="ObjectDataSource1" Name="DataSet1" />
                </DataSources>
            </LocalReport>
        </rsweb:ReportViewer>
        <asp:ObjectDataSource ID="ObjectDataSource1" runat="server" OldValuesParameterFormatString="original_{0}" SelectMethod="GetData" TypeName="HealToZeal.DatewiseScoreTableAdapters.SpDatewiseScoreByEmployeeCompanyIdTableAdapter">
            <SelectParameters>
                <asp:ControlParameter ControlID="txtEmployeeId" DefaultValue="1009" Name="EmployeeCompanyId" PropertyName="Text" Type="Int32" />
            </SelectParameters>
        </asp:ObjectDataSource>
    </div>
    </form>
</body>
</html>
