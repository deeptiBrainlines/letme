﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace HealToZeal.Employee
{
    
    public class GooglePlusAccessToken
    {
        public string access_token { get; set; }
        public string token_type { get; set; }
        public int expires_in { get; set; }
        public string id_token { get; set; }
        public string refresh_token { get; set; }
    }
    public class GoogleUserOutputData
    {
        public string id { get; set; }
        public string name { get; set; }
        public string given_name { get; set; }
        public string family_name { get; set; }
        public string email { get; set; }
        public string picture { get; set; }
        public string gender { get; set; }
        public string locale{  get; set;  }
        
    }
    //[DataContract]
    //public class GooglePlusUserProfile
    //{
    //    [DataMember(Name = "id")]
    //    public string id { get; set; }
    //    [DataMember(Name = "email")]
    //    public string email { get; set; }
    //    [DataMember(Name = "name")]
    //    public string name { get; set; }
    //    [DataMember(Name = "given_name")]
    //    public string given_name { get; set; }
    //    [DataMember(Name = "family_name")]
    //    public string family_name { get; set; }
    //    [DataMember(Name = "link")]
    //    public string link { get; set; }
    //    [DataMember(Name = "picture")]
    //    public string picture { get; set; }
    //    [DataMember(Name = "gender")]
    //    public string gender { get; set; }
    //    [DataMember(Name = "birthday")]
    //    public string birthday { get; set; }

    //}

}