﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DAL;
using BE;

namespace HealToZeal.Admin
{
    public partial class ViewAssignedInstances : System.Web.UI.Page
    {
        AssessmentDAL objAssessmentDAL = new AssessmentDAL();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                BindInstances();
            }
        }
        private void BindInstances()
        {
            try
            {
                DlistInstances.DataSource = objAssessmentDAL.AssessmentInstanceMasterGet();
                DlistInstances.DataBind();
            }
            catch (Exception)
            {

            }
        }    

        protected void DlistInstances_ItemCommand(object source, DataListCommandEventArgs e)
        {
            try
            {
                if (e.CommandName == "UpdateInstance")
                {
                //    Response.Redirect("AssignAssessmentInstance.aspx?InstanceId=" + e.CommandArgument.ToString());
                    Response.Redirect("AssignAssessmentInstance.aspx?InstanceId=" + e.CommandArgument.ToString());
                }
               
            }
            catch 
            {
               
            }
        }

        protected void DlistInstances_ItemDataBound(object sender, DataListItemEventArgs e)
        {
            try
            {
                DataList DlistCompanies = (DataList)e.Item.FindControl("DlistCompanies");
                Label lblInstanceId = (Label)e.Item.FindControl("lblInstanceId");

                DlistCompanies.DataSource = objAssessmentDAL.AssessmentInstanceCompanyRelationGet(lblInstanceId.Text);
                DlistCompanies.DataBind();

            }
            catch 
            {
                
            }
        }

        protected void DlistCompanies_ItemCommand(object source, DataListCommandEventArgs e)
        {
            try
            {
                if (e.CommandName == "UpdateCompany")
                {
                    Label lblInstanceId = (Label)e.Item.FindControl("lblInstanceId");
                  Response.Redirect("AssignAssessmentInstance.aspx?CompanyId=" + e.CommandArgument.ToString() + "&InstanceId=" + lblInstanceId.Text);
               
                }
            }
            catch 
            {
                
            }
        }
    }
}