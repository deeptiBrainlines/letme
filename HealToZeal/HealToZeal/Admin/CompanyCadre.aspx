﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/Admin.Master" AutoEventWireup="true" CodeBehind="CompanyCadre.aspx.cs" Inherits="HealToZeal.Admin.CompanyCadre" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .auto-style1 {
            height: 26px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="LeftColumn" runat="server">
    <table bgcolor="#EAF2F8">
        <tr>
            <td></td>
            <td>
                <asp:Label ID="lblTitle" runat="server" Text="Company Cadre"></asp:Label>
            </td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="lblCompany" runat="server" Text="Company"></asp:Label>
            </td>
            <td>
                <asp:DropDownList ID="ddlCompany" runat="server" Width="205px" Height="30px"></asp:DropDownList>
            </td>
            <td></td>
            <td></td>
        </tr>
        <%--<tr>
            <td>
                <asp:Label ID="lblCompanyDepartment" runat="server" Text="Department"></asp:Label>
            </td>
            <td>
                <asp:DropDownList ID="ddCompanyDepartments"  runat="server" Width="205px" Height="30px" ></asp:DropDownList>
            </td>
            <td>
            
            </td>
            <td></td>
        </tr>--%>
        <tr>
            <td>
                <asp:Label ID="lblCompanyCadre" runat="server" Text="Cadre"></asp:Label>
            </td>
            <td>
                <asp:TextBox ID="txtCompanyCadre" runat="server" BorderColor="Black" Width="200px" Height="30px"></asp:TextBox>
            </td>
            <td>
            
            </td>
            <td></td>
        </tr>
        <tr>
            <td></td>
            <td>
        
                <asp:Button ID="btnSubmit" runat="server" Text="Submit" OnClick="btnSubmit_Click" />
        
            </td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td></td>
            <td>
                <asp:Label ID="lblMessage" runat="server" Text=""></asp:Label>
            </td>
            <td></td>
            <td></td>
        </tr>
    </table>
</asp:Content>
