﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DAL;
using BE;
using System.Data;
using System.Web.Mail;


namespace HealToZeal.Admin
{
    public partial class AssignAssesmentInstances : System.Web.UI.Page
    {
        CompanyDetailsDAL objCompanyDetailsDAL = new CompanyDetailsDAL();
        AssessmentDAL objAssessmentDAL = new AssessmentDAL();
        AssessmentBE objAssessmentBE = new AssessmentBE();
        string validTo = string.Empty;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                if (Session["AdminId"] != null)
                {
                    BindInstances();
                    BindCompanies();
                    ddlBatch.Visible = false;
                    lbBatch.Visible = false;
                }
            }

        }

        private void BindInstances()
        {
            try
            {

                //Code by Poonam on 28th july
                ddlInstances.DataSource = objAssessmentDAL.AssessmentInstanceMasterGet();
                ddlInstances.DataTextField = "AssessmentInstanceName";
                ddlInstances.DataValueField = "AssessmentInstanceId_PK";
                ddlInstances.DataBind();

            }
            catch (Exception)
            {

            }
        }

        private void BindCompanies()
        {
            try
            {

                //Code by Poonam on 28th july
                ddlCompany.DataSource = objCompanyDetailsDAL.CompanyDetailsGet();
                ddlCompany.DataTextField = "CompanyName";
                ddlCompany.DataValueField = "CompanyId_PK";
                ddlCompany.DataBind();


            }
            catch (Exception)
            {

            }
        }

        protected void ddlCompany_SelectedIndexChanged(object sender, EventArgs e)
        {
            lbBatch.Visible = true;
            ddlBatch.Visible = true;
            DataTable dt = objAssessmentDAL.GetBatchCompanyWise(ddlCompany.SelectedValue);

            ddlBatch.DataSource = dt;
            ddlBatch.DataTextField = "BatchName";
            ddlBatch.DataValueField = "BatchID_PK";
            ddlBatch.DataBind();
        }

        protected void ddlInstances_SelectedIndexChanged(object sender, EventArgs e)
        {
            DataTable TocheckInstanceExistsOrNot = objAssessmentDAL.TocheckInstanceExistsOrNot(ddlCompany.SelectedValue, ddlInstances.SelectedValue);
            if (TocheckInstanceExistsOrNot.Rows.Count > 0)
            {
                lbmessage.Text = "Assessment instance AllReady present...";
            }

        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            try
            {
                int Status = 0;

                objAssessmentBE.propCompanyId_FK = ddlCompany.SelectedItem.Value;
                objAssessmentBE.propCreatedBy = Session["AdminId"].ToString();
                objAssessmentBE.propCreatedDate = DateTime.Now;

                DataTable dtInstance = new DataTable();
                dtInstance.Columns.Add(new System.Data.DataColumn("AssessmentInstanceId_FK", typeof(string)));

                foreach (ListItem chkCompany in ddlInstances.Items)
                {
                    if (chkCompany.Selected)
                    {
                        dtInstance.Rows.Add(chkCompany.Value);
                    }
                }

                string Validfromdate = txtValidFrom.Text;
                string validTodate = txtValidTo.Text;
                DateTime date = DateTime.ParseExact(Validfromdate, "dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture);
                DateTime date1 = DateTime.ParseExact(validTodate, "dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture);
                //string ValidFrom = date.ToString("MM/dd/yyyy");
                //string validTo = date1.ToString("MM/dd/yyyy");
                string ValidFrom = date.ToString("yyyy/MM/dd");
                 validTo = date1.ToString("yyyy/MM/dd");


                objAssessmentBE.propValidFrom = Convert.ToDateTime(ValidFrom);
                objAssessmentBE.propValidTo = Convert.ToDateTime(validTo);
                objAssessmentBE.propAssessmentInstanceId_FK = dtInstance.Rows[0]["AssessmentInstanceId_FK"].ToString();
                objAssessmentBE.propBatchID_FK = ddlBatch.SelectedValue;

                if (dtInstance != null && dtInstance.Rows.Count > 0)
                {

                    Status = objAssessmentDAL.CompanyRelationAssessmentInstanceInsert(objAssessmentBE);
                    if (Status > 0)
                    {
                        lbmessage.Text = "Assessment instance assigned successfully...";
                        //Send mail to employees
                        SendMailToEployees();
                        ClearFields();
                    }
                }
                else
                {
                    lbmessage.Text = "Select at least one company...";
                }

            }
            catch(Exception ex)
            {
               Response.Write(ex.Message);
            }

        }

        private void ClearFields()
        {
            try
            {
                ddlCompany.ClearSelection();
                ddlInstances.ClearSelection();
                txtValidFrom.Text = "";
                txtValidTo.Text = "";

            }
            catch
            {

            }
        }

        private void SendMailToEployees()
        {
            EmployeeDAL objEmployeeDAL = new EmployeeDAL();
            DataTable dtEmployees;
            MailMessage mail;
            foreach (ListItem chkCompany in ddlCompany.Items)
            {
                if (chkCompany.Selected)
                {
                    dtEmployees = objEmployeeDAL.GetEmployeeBatchWise(chkCompany.Value, ddlBatch.SelectedItem.Value);
                    if (dtEmployees != null)
                    {
                        if (dtEmployees.Rows.Count > 0)
                        {
                            for (int cnt = 0; cnt < dtEmployees.Rows.Count; cnt++)
                            {
                                string password = Encryption.Decrypt(dtEmployees.Rows[cnt]["Password"].ToString());
                                mail = new MailMessage();
                                mail.To = dtEmployees.Rows[0]["EmailId"].ToString();
                                mail.From = "HealToZeal@chetanatspl.com";
                                mail.Subject = "New Instance assigned.";

                                mail.Body = "<html><body><font color=\"black\">Dear " + dtEmployees.Rows[0]["FirstName"] + " " + dtEmployees.Rows[0]["LastName"] + ",<br/><br/>Welcome to <b> Let Me Talk2 Me: A Health and Personality Insights and Actions Tool.</b><br></br> Thanks for attempting previous assessment.  <br></br>You have assigned next assessement for gaining some more useful insights into your own personality.<br/><br/>It is recommended that ypu use this tool periodically to monitor yourself and be aware of your health and personality parameters. <br/><br/>Please use the link below to login  <br/><br/> <a href='http://chetanatspl.com/healtozealcurrent/Employee/EmployeeLogin.aspx' >http://chetanatspl.com/healtozealcurrent/Employee/EmployeeLogin.aspx</a><BR/><br/>Your login details are as follows:<br/>UserId:<b>" + dtEmployees.Rows[0]["Username"] + "</b><br/>Password:<b>" + password + "</b><br/><br/>Please ensure that you complete this assessment on or before " + validTo + "  <BR/> Thank You.<br/><br/></font></body></html>";
                                mail.BodyFormat = MailFormat.Html;

                                mail.Fields.Add("http://schemas.microsoft.com/cdo/configuration/smtpauthenticate", "1");    //basic authentication
                                mail.Fields.Add("http://schemas.microsoft.com/cdo/configuration/sendusername", "HealToZeal@chetanatspl.com"); //set your username here
                                mail.Fields.Add("http://schemas.microsoft.com/cdo/configuration/sendpassword", "HealToZeal");    //set your password here

                                SmtpMail.SmtpServer = "holly.arvixe.com";  //your real server goes here                        
                                SmtpMail.Send(mail);
                            }
                        }
                    }
                }
            }

        }


    }
}