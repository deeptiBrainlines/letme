﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="AssignAssessmentInstance.aspx.cs" MasterPageFile="~/Admin/Admin.Master" Inherits="HealToZeal.Admin.AssignAssessmentInstance" %>

<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="LeftColumn" runat="server">
       <div>
           <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
       <table>
        <tr>
            <td colspan="4">
                <asp:Label ID="lblTitle" runat="server" Text="Assessment Instance"></asp:Label>
            </td>        
        </tr>         
      <tr>
          <td></td>
          <td style="vertical-align:text-top">
              <asp:RadioButtonList ID="rblInstances" runat="server" AutoPostBack="true" OnSelectedIndexChanged="rblInstances_SelectedIndexChanged"></asp:RadioButtonList>
          
          </td>
          <td></td>
          <td>
              
               <asp:CheckBoxList ID="chklistCompanies" runat="server"></asp:CheckBoxList>   
              <asp:GridView ID="GvCompanies" AutoGenerateColumns="false" runat="server">
                  <Columns>
                      <asp:TemplateField>
                          <HeaderTemplate>
                              <table>
                                  <tr>
                                      <td></td>
                                       <td>Company</td>
                                       <td></td>
                                       <td>Valid from</td>
                                       <td>Valid to</td>
                                  </tr>
                              </table>
                          </HeaderTemplate>
                          <ItemTemplate>
                              <table>
                                  <tr>
                                      <td>
                                          <asp:CheckBox ID="chkCompany" runat="server" />
                                      </td>
                                      <td>
                                          <asp:Label ID="lblCompanyName" runat="server" Text='<%#Eval("CompanyName") %>'></asp:Label>
                                      </td>
                                      <td>
                                            <asp:Label ID="lblCompanyId" runat="server" Text='<%#Eval("CompanyId_PK") %>' Visible="false"></asp:Label>
                                      </td>
                                      <td>
                                          <asp:TextBox ID="txtValidFrom" runat="server"></asp:TextBox>
                                          <cc1:CalendarExtender ID="txtValidFrom_CalendarExtender" Format="MM'/'dd'/'yyyy HH':'mm':'ss" runat="server" BehaviorID="txtValidFrom_CalendarExtender" TargetControlID="txtValidFrom">
                                          </cc1:CalendarExtender>
                                      </td>
                                      <td>
                                          <asp:TextBox ID="txtValidTo" runat="server" ></asp:TextBox>
                                          <cc1:CalendarExtender ID="txtValidTo_CalendarExtender" runat="server" Format="MM'/'dd'/'yyyy HH':'mm':'ss"  BehaviorID="txtValidTo_CalendarExtender" TargetControlID="txtValidTo">
                                          </cc1:CalendarExtender>
                                      </td>                                    
                                  </tr>
                              </table>
                          </ItemTemplate>                             
                      </asp:TemplateField>
                  </Columns>
              </asp:GridView>
          </td>
      </tr>
          <tr>
              <td colspan="4">
                  <asp:Panel ID="pnlValidity" runat="server">
                      <table>
                          <tr>
                              <td>
                                  <asp:Label ID="ValidFrom" runat="server" Text="Valid from"></asp:Label>
                                  <asp:TextBox ID="txtValidFrom" runat="server"></asp:TextBox>
                                  <cc1:CalendarExtender ID="txtValidFrom_CalendarExtender" runat="server" BehaviorID="txtValidFrom_CalendarExtender" TargetControlID="txtValidFrom">
                                  </cc1:CalendarExtender>
                              </td>
                              <td>
                                  <asp:Label ID="ValidTo" runat="server" Text="Valid to"></asp:Label>
                                  <asp:TextBox ID="txtValidTo" runat="server"></asp:TextBox>
                                  <cc1:CalendarExtender ID="txtValidTo_CalendarExtender" runat="server" BehaviorID="txtValidTo_CalendarExtender" TargetControlID="txtValidTo">
                                  </cc1:CalendarExtender>
                              </td>
                          </tr>
                      </table>
                  </asp:Panel>                 
              </td>         
        </tr>
          <tr>
            <td></td>
            <td colspan="3">
                <asp:Button ID="btnSubmit" runat="server" Text="Assign" OnClick="btnSubmit_Click" />
              </td>
        </tr>
          <tr>
            <td>&nbsp;</td>
            <td colspan="3">
                <asp:Label ID="lblMessage" runat="server" Text=""></asp:Label>
              </td>
        </tr>
    </table>
    </div>

   </asp:Content>
 