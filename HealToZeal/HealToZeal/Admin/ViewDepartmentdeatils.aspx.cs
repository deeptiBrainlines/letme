﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using DAL;
using BE;

namespace HealToZeal.Admin
{
    //Code by Poonam on 14 july 2016
    public partial class ViewDepartmentdeatils : System.Web.UI.Page
    {
      public   CompanyDepartmentDAL objCompanyDeptDAL = new CompanyDepartmentDAL();
      public   CompanyDetailsDAL objCompanyDetailsDAL = new CompanyDetailsDAL();
      public   CompanyDepartmentBE objCompanyDeptBE = new CompanyDepartmentBE();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                BindCompanies();
             //   DlistCompany.DataSource = objCompanyDetailsDAL.CompanyDetailsGet();
             //   DlistCompany.DataBind();
                if (Session["AdminId"] != null)
                {
                   // BindCompanies();
                    if (Request.QueryString["Id"] != null)
                    {
                        DataTable Dtdept = objCompanyDeptDAL.CompanyDeaprtmentGetByCompanyId(Request.QueryString["Id"].ToString());
                        if (Dtdept != null && Dtdept.Rows.Count > 0)
                        {
                          //  ddlCompany.SelectedValue = Dtdept.Rows[0][""].ToString();
                        }
                    }
                }
            }
           
        }

        private void BindCompanies()
        {
            try
            {
                CompanyDetailsDAL objCompanyDetailsDAL = new CompanyDetailsDAL();
                ddlCompany.DataSource = objCompanyDetailsDAL.CompanyDetailsGet();
                ddlCompany.DataTextField = "CompanyName";
                ddlCompany.DataValueField = "CompanyId_PK";
                ddlCompany.DataBind();
                ddlCompany.Items.Insert(0, new ListItem("--Select Company--", "--Select Company--"));
                ddlCompany.SelectedIndex = 0;
            }
            catch
            {

            }
        }
      

        protected void DlistCompany_ItemDataBound(object sender, DataListItemEventArgs e)
        {
           //string str;
            try
            {
                DataList DlistDepartment = (DataList)e.Item.FindControl("DlistDepartment");
                Label lblCompanyId = (Label)e.Item.FindControl("lblCompanyId");
                Label lblDeptId = (Label)e.Item.FindControl("lblDeptId");


                DlistDepartment.DataSource = objCompanyDeptDAL.CompanyDeaprtmentGetByCompanyId(ddlCompany.SelectedValue.ToString());
                DlistDepartment.DataBind();
              

            }
            catch(Exception)
            {

            }
        }

       

        protected void ddlCompany_SelectedIndexChanged(object sender, EventArgs e)
        {
            CompanyDepartmentDAL objCompanyDeptDAL = new CompanyDepartmentDAL();
            DlistCompany.DataSource = objCompanyDeptDAL.GetAllDepartment(ddlCompany.SelectedValue.ToString());
            
            DlistCompany.DataBind();
        }

        protected void DlistCompany_ItemCommand(object source, DataListCommandEventArgs e)
        {
            DataList DlistDepartment = (DataList)e.Item.FindControl("DlistDepartment");
            Label lblCompanyId = (Label)e.Item.FindControl("lblCompanyId");
            Label lblDeptId = (Label)e.Item.FindControl("lblDeptId");
            TextBox lblDepartmentname = (TextBox)e.Item.FindControl("lblDepartmentname");
            if (e.CommandName == "UpdateDepartment")
            {

                DataTable dt = objCompanyDeptDAL.CompanyDepartmentGetByCompanyIdandDeptID(lblCompanyId.Text, lblDeptId.Text);
                int status = 0;
                objCompanyDeptBE.propCompanyId_FK = lblCompanyId.Text;
                objCompanyDeptBE.propDepartmentName = lblDepartmentname.Text;
                objCompanyDeptBE.propCreatedBy = Session["AdminId"].ToString();
                objCompanyDeptBE.propCreatedDate = DateTime.Now.Date;

                if (lblDeptId.Text != null)
                {
                    objCompanyDeptBE.propDeptId_PK = lblDeptId.Text.ToString();

                    status = objCompanyDeptDAL.DepartmentUpdate(objCompanyDeptBE);
                    if (status > 0)
                    {

                        ScriptManager.RegisterClientScriptBlock(this.Page, this.GetType(), "startScript", "<script>alert('Deaprtment updated successfully'); </script>", false);

                    }
                }
                

            }
            else
            {
                CompanyDepartmentDAL objCompanyDeptDAL = new CompanyDepartmentDAL();
                DataTable dt1 = objCompanyDeptDAL.DeleteDepartMent(lblDeptId.Text);
                ScriptManager.RegisterClientScriptBlock(this.Page, this.GetType(), "startScript", "<script>alert('Deaprtment Deleted '); </script>", false);
                DlistCompany.DataSource = objCompanyDeptDAL.GetAllDepartment(ddlCompany.SelectedValue.ToString());

                DlistCompany.DataBind();
            }

        }

    }
}