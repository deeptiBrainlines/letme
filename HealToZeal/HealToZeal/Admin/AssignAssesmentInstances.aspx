﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/Admin.Master" AutoEventWireup="true" CodeBehind="AssignAssesmentInstances.aspx.cs" Inherits="HealToZeal.Admin.AssignAssesmentInstances" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="LeftColumn" runat="server">
    <div> <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager></div>
    <table bgcolor="#EAF2F8">
        <tr>
            <td>
                <asp:Label ID="lblTitle" runat="server" Font-Bold="true" Text="Assign Assesment Instances" ></asp:Label>
            </td>
             <td></td>
            <td></td>
        </tr>
         
        <tr>
            <td>
                <asp:Label ID="lblCompany" runat="server" Text="Company" ></asp:Label>
               
                
            </td>
            &nbsp;
            <td>
                <asp:DropDownList ID="ddlCompany" AutoPostBack="true" OnSelectedIndexChanged="ddlCompany_SelectedIndexChanged" runat="server" Width="205px" Height="30px" ></asp:DropDownList>
                              
            </td>
            <td></td>
            <td></td>
        </tr>
         <tr>
            <td>
                <asp:Label ID="lblInstances" runat="server" Text="Assesment Instance"></asp:Label>
                
            </td>
            <td>
                <asp:DropDownList ID="ddlInstances" runat="server"  OnSelectedIndexChanged="ddlInstances_SelectedIndexChanged"  Width="205px" Height="30px"></asp:DropDownList>
               
            </td>
            <td>
            
            </td>
            <td></td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="lbBatch" runat="server" Text="Batch"></asp:Label>
               
            </td>
            &nbsp;
            <td>
                <asp:DropDownList ID="ddlBatch"   runat="server" Width="205px" Height="30px" ></asp:DropDownList>
                
            </td>
            <td></td>
            <td></td>
        </tr>
        <tr>
        <td>
          <asp:Label ID="ValidFrom" runat="server" Text="Valid from"></asp:Label>


        </td>
        <td>
          <asp:TextBox ID="txtValidFrom" runat="server"  ></asp:TextBox>
                                  <cc1:CalendarExtender ID="txtValidFrom_CalendarExtender" runat="server"   Format="dd/MM/yyyy"   TargetControlID="txtValidFrom">
                                  </cc1:CalendarExtender>
        </td>
       
        </tr>
         <tr>
            <td>
               <asp:Label ID="ValidTo" runat="server" Text="Valid to"></asp:Label>
            <td>
                <asp:TextBox ID="txtValidTo" runat="server" ></asp:TextBox>
                                  <cc1:CalendarExtender ID="txtValidTo_CalendarExtender"  runat="server"  Format="dd/MM/yyyy"  TargetControlID="txtValidTo">
                                  </cc1:CalendarExtender>
            </td>
        </tr>
        <tr>
            <td></td>
            <td>
        
                <asp:Button ID="btnSubmit" runat="server" Text="Submit" OnClick="btnSubmit_Click"  Width="80px"  />
        
            </td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td></td>
            <td>
                <asp:Label ID="lbmessage" runat="server" Text=""></asp:Label>
<%--                <asp:Label ID="lbmessage"  runat="server" Text=""></asp:Label>--%>
            </td>
            <td></td>
            <td></td>


                 </tr>
        <tr>

        </table>
</asp:Content>
