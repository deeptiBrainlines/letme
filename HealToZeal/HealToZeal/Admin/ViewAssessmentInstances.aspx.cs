﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DAL;
using BE;
namespace HealToZeal.Admin
{
    public partial class ViewAssessmentInstances : System.Web.UI.Page
    {
        AssessmentDAL objAssessmentDAL = new AssessmentDAL();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                BindInstances();                   
            }
        }
        private void BindInstances()
        {
            try
            {
             DlistInstances.DataSource=objAssessmentDAL.AssessmentInstanceMasterGet();
             DlistInstances.DataBind();
            }
            catch (Exception)
            {

            }
        }

        protected void DlistInstances_ItemCommand(object source, DataListCommandEventArgs e)
        {
            try
            {
                if (e.CommandName == "UpdateInstance")
                {
                    Response.Redirect("AssessmentInstance.aspx?Id=" + e.CommandArgument.ToString());
                   // Response.Redirect("WebForm2.aspx?Id=" + e.CommandArgument.ToString());
                }
            }
            catch
            {
               
            }
        }

        protected void DlistInstances_ItemDataBound(object sender, DataListItemEventArgs e)
        {
            try
            {
                DataList DlistAssessments = (DataList)e.Item.FindControl("DlistAssessments");
                Label lblInstanceId = (Label)e.Item.FindControl("lblInstanceId");

                DlistAssessments.DataSource = objAssessmentDAL.AssessmentInstanceAssessmentRelationGetByInstanceId(lblInstanceId.Text);
                DlistAssessments.DataBind();

            }
            catch 
            {
                
            }
        }

    }
}