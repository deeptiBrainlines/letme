﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/Admin.Master" AutoEventWireup="true" CodeBehind="ViewAllCompanyDetails.aspx.cs" Inherits="HealToZeal.Admin.ViewAllCompanyDetails" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="LeftColumn" runat="server">
    <table>
    <tr>
        <td colspan="4"> <b>Company Details</b></td>    
    </tr>
     <tr>
        <td></td>
        <td>
            
             <asp:GridView CellPadding="2" CellSpacing="2" ID="GridDisplay"  runat="server"   AutoGenerateColumns="false" Width="100%" ShowFooter="True" OnDataBound="GridDisplay_DataBound" >
                                <Columns>
                                
                                    <asp:TemplateField HeaderText="Company" ItemStyle-Width="200px" >
                                        <ItemTemplate>
                                            <asp:Label ID="lbCompnyName"   runat="server" Text='<%#Eval("CompanyName") %>' > </asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle CssClass="text_css" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Deapartment" ItemStyle-Width="250px">
                                        <ItemTemplate>
                                            <asp:Label ID="lblDepartmentname" runat="server" Text='<%#Eval("Departmentname") %>' Width="150px"> </asp:Label>
                                            
                                        </ItemTemplate>
                                        <ItemStyle CssClass="text_css" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Cadre" ItemStyle-Width="250px">
                                        <ItemTemplate>
                                            <asp:Label ID="lbCadreName"  runat="server"  Text='<%#Eval("Cadrename") %>' Width="150px" > </asp:Label>  
                                             
                                        </ItemTemplate>
                                        <ItemStyle CssClass="text_css" />
                                    </asp:TemplateField>
                                    
                                </Columns>
                                <HeaderStyle BackColor="LightBlue" Font-Bold="True" HorizontalAlign="Center" VerticalAlign="Middle" />
                            </asp:GridView>
                                           
        </td>
        <td></td>
        <td></td>
    </tr>
     <tr>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
    </tr>
</table>
</asp:Content>
