﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using DAL;

namespace HealToZeal.Admin
{
    public partial class CompanyLoginInfo : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if(!IsPostBack)
            {
                CompanyDetailsDAL ObjDAL = new CompanyDetailsDAL();
                DataTable DtComp = ObjDAL.CompanyDetailsGet();
               
                if(DtComp!=null)
                {
                    if(DtComp.Rows.Count >0)
                    {
                        DtComp.Columns.Add("DisplayPassword", typeof(string));

                        for(int a=0;a<DtComp.Rows.Count;a++)
                        {
                            string OriginalPassword = DtComp.Rows[a]["Password"].ToString();
                            string DisplayPassword = Encryption.Decrypt(OriginalPassword);
                            DtComp.Rows[a]["DisplayPassword"] = DisplayPassword;
                        }

                        GdvCompanyLoginInfo.DataSource = DtComp;
                        GdvCompanyLoginInfo.DataBind();
                    }
                }
            }
        }
    }
}