﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ClusterType.aspx.cs" MasterPageFile="~/Admin/Admin.Master" Inherits="HealToZeal.Admin.ClusterType" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="LeftColumn" runat="server">
    <div>
        <table>
            <tr>
                <td colspan="4">
                    <asp:Label ID="lblTitle" runat="server" Text="Add cluster type"></asp:Label>
                </td>             
            </tr>
            <tr>
                <td>
                    <asp:Label ID="lblClusterTypeName" runat="server" Text="Cluster type name"></asp:Label>
                </td>
                <td>
                    <asp:TextBox ID="txtClusterTypeName" runat="server"></asp:TextBox>
                </td>
                <td>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="Enter the cluster type name." ControlToValidate="txtClusterTypeName" Display="Dynamic" ForeColor="Red"></asp:RequiredFieldValidator>
                </td>
                <td>

                </td>
            </tr>
            <tr>
                <td></td>
                <td>
                    <asp:CheckBoxList ID="chklistClusters" runat="server">
                    </asp:CheckBoxList>
                </td>
                <td></td>
                <td></td>
            </tr>
               <tr>
                <td></td>
                <td>
                    <asp:Button ID="btnSubmit" runat="server" Text="Submit" OnClick="btnSubmit_Click" />
                   </td>
                <td></td>
                <td></td>
            </tr>
             <tr>
                <td></td>
                <td>
                    <asp:Label ID="lblMessage" runat="server" ForeColor="Red"></asp:Label>
                   </td>
                <td></td>
                <td></td>
            </tr>
        </table>
    </div>
</asp:Content>