﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DAL;
using BE;
using System.Data;

namespace HealToZeal.Admin
{
    public partial class Assessment : System.Web.UI.Page
    {
        AssessmentDAL objAssessmentDAL = new AssessmentDAL();
      
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                //Page.SetFocus(txtAssessmentName);

               // ((TextBox)Master.FindControl("txtAssessmentName")).Focus();

                if (Session["AdminId"] == null)
                {
                    Response.Redirect("AdminLogin.aspx");
                }
                else
                {
                    if (Request.QueryString["AssessmentId"] != null)
                    {
                        UpdateAssessment();
                    }
                }
            }
        }

        private void UpdateAssessment()
        {
            try
            {
               DataTable DtAssessment= objAssessmentDAL.AssessmentGetByAssessmentId(Request.QueryString["AssessmentId"].ToString());
               if (DtAssessment != null && DtAssessment.Rows.Count > 0)
               {
                   txtAssessmentName.Text = DtAssessment.Rows[0]["AssessmentName"].ToString();
                   txtNoOfQuestions.Text = DtAssessment.Rows[0]["NoOfQuestions"].ToString();
                   if (DtAssessment.Rows[0]["IsComplete"]!=null)
                   {
                       if (DtAssessment.Rows[0]["IsComplete"].ToString() == "Y")
                       {
                           chkIsComplete.Checked = true;
                       }
                       else
                       {
                           chkIsComplete.Checked = false;
                       }
                   }
               }
            }
            catch
            {

            }
        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            try
            {
                int status = 0;
                AssessmentBE objAssessmentBE = new AssessmentBE();
                objAssessmentBE.propAssessmentName = txtAssessmentName.Text;
                objAssessmentBE.propNoOfQuestions = Convert.ToInt32(txtNoOfQuestions.Text);
                objAssessmentBE.propCreatedBy = Session["AdminId"].ToString();
                objAssessmentBE.propCreatedDate = DateTime.Now.Date;
                if (chkIsComplete.Checked)
                {
                    objAssessmentBE.propIsComplete = "Y";
                }
                else
                {
                    objAssessmentBE.propIsComplete = "N";
                }
                if (Request.QueryString["AssessmentId"] != null)
                {
                    objAssessmentBE.propAssessmentId_PK = Request.QueryString["AssessmentId"].ToString();
                    status = objAssessmentDAL.AssessmentUpdate(objAssessmentBE);
                    if (status > 0)
                    {
                        lblMessage.Text = "Assessment updated successfully...";
                    }
                }
                else
                {
                    status = objAssessmentDAL.AssessmentInsert(objAssessmentBE);
                    if (status > 0)
                    {
                        lblMessage.Text = "Assessment inserted successfully...";
                    }
                }
            
            }
            catch
            {
                
            }
        }
    }
}