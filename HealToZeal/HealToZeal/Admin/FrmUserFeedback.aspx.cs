﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using DAL;

namespace HealToZeal.Admin
{
    public partial class FrmUserFeedback : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                CompanyDetailsDAL ObjDAL = new CompanyDetailsDAL();
                DataTable Dtcomapny = ObjDAL.CompanyDetailsGet();

                if (Dtcomapny != null)
                {
                    if (Dtcomapny.Rows.Count > 0)
                    {
                        DrpCompany.DataSource = Dtcomapny;
                        DrpCompany.DataValueField = "CompanyId_PK";
                        DrpCompany.DataTextField = "CompanyName";
                        DrpCompany.DataBind();
                        DrpCompany.Items.Insert(0, new ListItem("--Select company--", "--Select company--"));

                        BindAllresult();
                    }
                }
            }
        }

        public void BindAllresult()
        {
            try
            {
                UsersFeedbackDAL ObjDAL = new UsersFeedbackDAL();
                DataTable DtResultAll = ObjDAL.GetUserFeedback();

                if (DtResultAll != null)
                {
                    if (DtResultAll.Rows.Count > 0)
                    {
                        TrAll.Visible = true;
                        TrEmp.Visible = false;

                        DataView view = new DataView(DtResultAll);
                        DataTable distinctValuesCmp = view.ToTable(true, "CompanyId_PK", "CompanyName");

                        GdvAll.DataSource = distinctValuesCmp;
                        GdvAll.DataBind();

                        //DataTable DtTble = DtResultAll.DefaultView.ToTable();
                        //DataView view1 = new DataView(DtResultAll);
                        //DataTable distinctValues = view1.ToTable(true, "EmployeeId_PK");

                        for (int i = 0; i < GdvAll.Rows.Count; i++)
                        {
                            Label LblCompID = (Label)GdvAll.Rows[i].FindControl("LblEmplId");
                            if (LblCompID != null)
                            {
                                if (LblCompID.Text != "")
                                {
                                    GridView Gdvinner = (GridView)GdvAll.Rows[i].FindControl("GdvAllInner");
                                    DataTable DtEmpData = DtResultAll.Select("CompanyId_PK='" + LblCompID.Text.ToString() + "'").CopyToDataTable();

                                    DataView view1 = new DataView(DtEmpData);
                                    DataTable distinctValuesEmp = view1.ToTable(true, "EmployeeId_PK", "Name","EmployeeCompanyId");

                                    if (distinctValuesEmp != null)
                                    {
                                        if (distinctValuesEmp.Rows.Count > 0)
                                        {
                                            Gdvinner.DataSource = distinctValuesEmp;
                                            Gdvinner.DataBind();
                                        }
                                    }

                                    for (int p = 0; p < Gdvinner.Rows.Count; p++)
                                    {
                                        Label LblEmpID = (Label)Gdvinner.Rows[p].FindControl("LblEmpId");
                                        if (LblEmpID != null)
                                        {
                                            if (LblEmpID.Text != "")
                                            {
                                                GridView Gdvinnerinner = (GridView)Gdvinner.Rows[p].FindControl("GvInnerInner");
                                                DataTable DtUserFeedBack = DtResultAll.Select("EmployeeId_PK='" + LblEmpID.Text.ToString() + "'").CopyToDataTable();
                                                if (DtUserFeedBack != null)
                                                {
                                                    if (DtUserFeedBack.Rows.Count > 0)
                                                    {
                                                        Gdvinnerinner.DataSource = DtUserFeedBack;
                                                        Gdvinnerinner.DataBind();
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                    else
                    {
                        TrAll.Visible = false;
                        TrEmp.Visible = false;
                    }
                }
                else
                {
                    TrAll.Visible = false;
                    TrEmp.Visible = false;
                }
            }
            catch (Exception )
            {

            }
        }
        protected void DrpCompany_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (DrpCompany.SelectedItem.Text != "--Select company--")
                {
                   // EmployeeDAL ObjEmpDAL = new EmployeeDAL();
                   // DataTable DtEmployees = ObjEmpDAL.EmployeesGetByCompanyId(DrpCompany.SelectedValue.ToString(), 2);
                    UsersFeedbackDAL ObjDAL = new UsersFeedbackDAL();
                    DataTable Dt = ObjDAL.GetUserFeedback();
                    DataTable DtResultAll = Dt.Select("CompanyId_PK='" + DrpCompany.SelectedValue.ToString() + "'").CopyToDataTable();

                    if (DtResultAll != null)
                    {
                        if (DtResultAll.Rows.Count > 0)
                        {
                            DataView view = new DataView(DtResultAll);
                            DataTable distinctValuesEmp = view.ToTable(true, "EmployeeId_PK", "EmployeeCompanyId","Name");

                            DrpEmployeesCompanyID.DataSource = distinctValuesEmp;
                            DrpEmployeesCompanyID.DataValueField = "EmployeeId_PK";
                            DrpEmployeesCompanyID.DataTextField = "EmployeeCompanyId";
                            DrpEmployeesCompanyID.DataBind();
                            DrpEmployeesCompanyID.Items.Insert(0, new ListItem("--Select employeecompanyId--", "--Select employeecompanyId--"));

                            DrpEmployees.DataSource = distinctValuesEmp;
                            DrpEmployees.DataValueField = "EmployeeId_PK";
                            DrpEmployees.DataTextField = "Name";
                            DrpEmployees.DataBind();
                            DrpEmployees.Items.Insert(0, new ListItem("--Select employee--", "--Select employee--"));
                        }
                    }

                    // Bind Gridview Data
                   
                   // DataTable DtResultAll = Dt.Select("CompanyId_PK='" + DrpCompany.SelectedValue.ToString() + "'").CopyToDataTable(); //view.ToTable(true, "CompanyId_PK", "CompanyName");

                    if (DtResultAll != null)
                    {
                        if (DtResultAll.Rows.Count > 0)
                        {
                            TrAll.Visible = true;
                            TrEmp.Visible = false;

                            DataView view = new DataView(DtResultAll);
                            DataTable distinctValuesCmp =view.ToTable(true, "CompanyId_PK", "CompanyName");

                            GdvAll.DataSource = distinctValuesCmp;
                            GdvAll.DataBind();                           

                            for (int i = 0; i < GdvAll.Rows.Count; i++)
                            {
                                Label LblCompID = (Label)GdvAll.Rows[i].FindControl("LblEmplId");
                                if (LblCompID != null)
                                {
                                    if (LblCompID.Text != "")
                                    {
                                        GridView Gdvinner = (GridView)GdvAll.Rows[i].FindControl("GdvAllInner");
                                        DataTable DtEmpData = DtResultAll.Select("CompanyId_PK='" + LblCompID.Text.ToString() + "'").CopyToDataTable();

                                        DataView view1 = new DataView(DtEmpData);
                                        DataTable distinctValuesEmp = view1.ToTable(true, "EmployeeId_PK", "Name", "EmployeeCompanyId");

                                        if (distinctValuesEmp != null)
                                        {
                                            if (distinctValuesEmp.Rows.Count > 0)
                                            {
                                                Gdvinner.DataSource = distinctValuesEmp;
                                                Gdvinner.DataBind();
                                            }
                                        }

                                        for (int p = 0; p < Gdvinner.Rows.Count; p++)
                                        {
                                            Label LblEmpID = (Label)Gdvinner.Rows[p].FindControl("LblEmpId");
                                            if (LblEmpID != null)
                                            {
                                                if (LblEmpID.Text != "")
                                                {
                                                    GridView Gdvinnerinner = (GridView)Gdvinner.Rows[p].FindControl("GvInnerInner");
                                                    DataTable DtUserFeedBack = DtResultAll.Select("EmployeeId_PK='" + LblEmpID.Text.ToString() + "'").CopyToDataTable();
                                                    if (DtUserFeedBack != null)
                                                    {
                                                        if (DtUserFeedBack.Rows.Count > 0)
                                                        {
                                                            Gdvinnerinner.DataSource = DtUserFeedBack;
                                                            Gdvinnerinner.DataBind();
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        else
                        {
                            TrAll.Visible = false;
                            TrEmp.Visible = false;
                        }
                    }
                    else
                    {
                        TrAll.Visible = false;
                        TrEmp.Visible = false;
                    }
                }
            }
            catch (Exception )
            {

            }
        }
        protected void DrpEmployees_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (DrpEmployees.SelectedItem.Text != "--Select employee--")
                {
                    DrpEmployeesCompanyID.SelectedIndex = 0;
                    // Bind Gridview Data
                    UsersFeedbackDAL ObjDAL = new UsersFeedbackDAL();
                    DataTable Dt = ObjDAL.GetUserFeedback();
                    DataTable DtResultAll = Dt.Select("CompanyId_PK='" + DrpCompany.SelectedValue.ToString() + "' And EmployeeId_PK='" + DrpEmployees.SelectedValue.ToString() + "'").CopyToDataTable(); //view.ToTable(true, "CompanyId_PK", "CompanyName");

                    if (DtResultAll != null)
                    {
                        if (DtResultAll.Rows.Count > 0)
                        {
                            TrAll.Visible = true;
                            TrEmp.Visible = false;

                            DataView view = new DataView(DtResultAll);
                            DataTable distinctValuesCmp =view.ToTable(true, "CompanyId_PK", "CompanyName");

                            GdvAll.DataSource = distinctValuesCmp;
                            GdvAll.DataBind();                           

                            for (int i = 0; i < GdvAll.Rows.Count; i++)
                            {
                                Label LblCompID = (Label)GdvAll.Rows[i].FindControl("LblEmplId");
                                if (LblCompID != null)
                                {
                                    if (LblCompID.Text != "")
                                    {
                                        GridView Gdvinner = (GridView)GdvAll.Rows[i].FindControl("GdvAllInner");
                                        DataTable DtEmpData = DtResultAll.Select("CompanyId_PK='" + LblCompID.Text.ToString() + "'").CopyToDataTable();

                                        DataView view1 = new DataView(DtEmpData);
                                        DataTable distinctValuesEmp = view1.ToTable(true, "EmployeeId_PK", "Name", "EmployeeCompanyId");

                                        if (distinctValuesEmp != null)
                                        {
                                            if (distinctValuesEmp.Rows.Count > 0)
                                            {
                                                Gdvinner.DataSource = distinctValuesEmp;
                                                Gdvinner.DataBind();
                                            }
                                        }

                                        for (int p = 0; p < Gdvinner.Rows.Count; p++)
                                        {
                                            Label LblEmpID = (Label)Gdvinner.Rows[p].FindControl("LblEmpId");
                                            if (LblEmpID != null)
                                            {
                                                if (LblEmpID.Text != "")
                                                {
                                                    GridView Gdvinnerinner = (GridView)Gdvinner.Rows[p].FindControl("GvInnerInner");
                                                    DataTable DtUserFeedBack = DtResultAll.Select("EmployeeId_PK='" + LblEmpID.Text.ToString() + "'").CopyToDataTable();
                                                    if (DtUserFeedBack != null)
                                                    {
                                                        if (DtUserFeedBack.Rows.Count > 0)
                                                        {
                                                            Gdvinnerinner.DataSource = DtUserFeedBack;
                                                            Gdvinnerinner.DataBind();
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        else
                        {
                            TrAll.Visible = false;
                            TrEmp.Visible = false;
                        }
                    }
                    else
                    {
                        TrAll.Visible = false;
                        TrEmp.Visible = false;
                    }
                }
            }
            catch (Exception )
            {

            }
        }
        protected void DrpEmployeesCompanyID_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (DrpEmployeesCompanyID.SelectedItem.Text != "--Select employeecompanyId--")
                {
                    DrpEmployees.SelectedIndex = 0;
                    UsersFeedbackDAL ObjDAL = new UsersFeedbackDAL();
                    DataTable Dt = ObjDAL.GetUserFeedback();
                    DataTable DtResultAll = Dt.Select("CompanyId_PK='" + DrpCompany.SelectedValue.ToString() + "' And EmployeeId_PK='" + DrpEmployeesCompanyID.SelectedValue.ToString() + "'").CopyToDataTable(); //view.ToTable(true, "CompanyId_PK", "CompanyName");

                    if (DtResultAll != null)
                    {
                        if (DtResultAll.Rows.Count > 0)
                        {
                            TrAll.Visible = true;
                            TrEmp.Visible = false;

                            DataView view = new DataView(DtResultAll);
                            DataTable distinctValuesCmp = view.ToTable(true, "CompanyId_PK", "CompanyName");

                            GdvAll.DataSource = distinctValuesCmp;
                            GdvAll.DataBind();

                            for (int i = 0; i < GdvAll.Rows.Count; i++)
                            {
                                Label LblCompID = (Label)GdvAll.Rows[i].FindControl("LblEmplId");
                                if (LblCompID != null)
                                {
                                    if (LblCompID.Text != "")
                                    {
                                        GridView Gdvinner = (GridView)GdvAll.Rows[i].FindControl("GdvAllInner");
                                        DataTable DtEmpData = DtResultAll.Select("CompanyId_PK='" + LblCompID.Text.ToString() + "'").CopyToDataTable();

                                        DataView view1 = new DataView(DtEmpData);
                                        DataTable distinctValuesEmp = view1.ToTable(true, "EmployeeId_PK", "Name", "EmployeeCompanyId");

                                        if (distinctValuesEmp != null)
                                        {
                                            if (distinctValuesEmp.Rows.Count > 0)
                                            {
                                                Gdvinner.DataSource = distinctValuesEmp;
                                                Gdvinner.DataBind();
                                            }
                                        }

                                        for (int p = 0; p < Gdvinner.Rows.Count; p++)
                                        {
                                            Label LblEmpID = (Label)Gdvinner.Rows[p].FindControl("LblEmpId");
                                            if (LblEmpID != null)
                                            {
                                                if (LblEmpID.Text != "")
                                                {
                                                    GridView Gdvinnerinner = (GridView)Gdvinner.Rows[p].FindControl("GvInnerInner");
                                                    DataTable DtUserFeedBack = DtResultAll.Select("EmployeeId_PK='" + LblEmpID.Text.ToString() + "'").CopyToDataTable();
                                                    if (DtUserFeedBack != null)
                                                    {
                                                        if (DtUserFeedBack.Rows.Count > 0)
                                                        {
                                                            Gdvinnerinner.DataSource = DtUserFeedBack;
                                                            Gdvinnerinner.DataBind();
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        else
                        {
                            TrAll.Visible = false;
                            TrEmp.Visible = false;
                        }
                    }
                    else
                    {
                        TrAll.Visible = false;
                        TrEmp.Visible = false;
                    }
                }
            }
            catch (Exception )
            {

            }
        }
    }
}