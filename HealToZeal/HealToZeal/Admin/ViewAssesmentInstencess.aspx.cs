﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DAL;
using BE;
using System.Data;

namespace HealToZeal.Admin
{
    public partial class ViewAssesmentInstencess : System.Web.UI.Page
    {
        CompanyDetailsDAL objCompanyDetailsDAL = new CompanyDetailsDAL();
        AssessmentDAL objAssessmentDAL = new AssessmentDAL();
        AssessmentBE objAssessmentBE = new AssessmentBE();
        protected void Page_Load(object sender, EventArgs e)
        {

            if (!Page.IsPostBack)
            {
                if (Session["AdminId"] != null)
                {
                  
                    BindCompanies();

                }
            }

        }
        private void BindCompanies()
        {
            try
            {


                //Code by Poonam on 28th july
                ddlCompany.DataSource = objCompanyDetailsDAL.CompanyDetailsGet();
                ddlCompany.DataTextField = "CompanyName";
                ddlCompany.DataValueField = "CompanyId_PK";
                ddlCompany.DataBind();


            }
            catch (Exception)
            {

            }
        }
        protected void ddlCompany_SelectedIndexChanged(object sender, EventArgs e)
        {
            AssessmentDAL objAssessmentDAL = new AssessmentDAL();
            DataTable DtInstances = objAssessmentDAL.AssessmentInstanceCompanyRelationGet(ddlCompany.SelectedValue);
            DlistCompany.DataSource = DtInstances;
            DlistCompany.DataBind();

        }

        protected void DlistCompany_ItemCommand(object source, DataListCommandEventArgs e)
        {
           //  DataTable DtInstances = objAssessmentDAL.AssessmentInstanceCompanyRelationGet(ddlCompany.SelectedValue);
            AssessmentDAL objAssessmentDAL = new AssessmentDAL();
             
             DataList  DlistCompany = (DataList)e.Item.FindControl("DlistCompany");
            


            TextBox txtValidFrom = (TextBox)e.Item.FindControl("txtValidFrom");
              TextBox txtValidTo = (TextBox)e.Item.FindControl("txtValidTo");
              TextBox lblAssesmentID = (TextBox)e.Item.FindControl("lblAssesmentID");

              string Validfromdate = txtValidFrom.Text;
              string validTodate = txtValidTo.Text;
              DateTime date = DateTime.ParseExact(Validfromdate, "dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture);
              DateTime date1 = DateTime.ParseExact(validTodate, "dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture);
              string ValidFrom = date.ToString("MM/dd/yyyy");
              string validTo = date1.ToString("MM/dd/yyyy");
            if (e.CommandName == "UpdateDepartment")
            {
               // objAssessmentBE.propValidFrom = Convert.ToDateTime(txtValidFrom.Text);
             //objAssessmentBE.propValidTo = Convert.ToDateTime(txtValidTo.Text);
             //objAssessmentBE.propAssessmentInstanceId_FK = lblAssesmentID.Text;




                DataTable Dt = objAssessmentDAL.CompanyRelationAssessmentInstanceUpdate(ddlCompany.SelectedValue, lblAssesmentID.Text, Convert.ToDateTime(ValidFrom), Convert.ToDateTime(validTo));

                if (Dt.Rows.Count > 0)
                        {
                           // lblMessage.Text = "Assessment instance assigned successfully...";
                          
                        }
                    
                    else
                    {
                       // lblMessage.Text = "Select at least one company...";

                       DataTable DtInstances = objAssessmentDAL.AssessmentInstanceCompanyRelationGet(ddlCompany.SelectedValue);



                       // DlistCompany.DataSource = DtInstances;


                       //  DlistCompany.DataBind();
                    }
                }
          
            }
        
    }
}