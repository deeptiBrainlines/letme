﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DAL;
using BE;
using System.Data;
namespace HealToZeal.Admin
{
    public partial class ClusterType : System.Web.UI.Page
    {
        ClusterMasterDAL objClusterMasterDAL = new ClusterMasterDAL();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                if (Session["AdminId"] != null)
                {
                    BindClusters();
                    if (Request.QueryString["Id"] != null)
                    {
                        UpdateData();
                    }
                }
            }
        }

        private void UpdateData()
        {
            try
            {
               DataTable DtClusters=  objClusterMasterDAL.ClusterTypeMasterGetClustersById(Request.QueryString["Id"].ToString());
               txtClusterTypeName.Text = DtClusters.Rows[0]["ClusterTypeName"].ToString();
               foreach (ListItem listClusters in chklistClusters.Items)
               {
                   if (DtClusters.Select("ClusterId_PK='" + listClusters.Value + "'").Length > 0)
                   {
                       listClusters.Selected = true;
                   }
                   else
                   {
                       listClusters.Selected = false;
                   }
               }
            }
            catch 
            {

            }
        }

        private void BindClusters()
        {
            try
            {
                chklistClusters.DataSource= objClusterMasterDAL.ClusterMasterGet();
                chklistClusters.DataValueField = "ClusterId_PK";
                chklistClusters.DataTextField = "ClusterName";
                chklistClusters.DataBind();
            }
            catch
            {
                
            }
        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            try
            {
                ClusterMasterBE objClusterMasterBE = new ClusterMasterBE();
                  int status=0;
                objClusterMasterBE.propClusterTypeName = txtClusterTypeName.Text;
                objClusterMasterBE.propCreatedBy = Session["AdminId"].ToString();
                objClusterMasterBE.propCreatedDate = DateTime.Now.Date;

                DataTable dtClusters = new DataTable();
                dtClusters.Columns.Add(new System.Data.DataColumn("ClusterId_PK", typeof(string)));
                foreach (ListItem chkClusters in chklistClusters.Items)
                {
                    if (chkClusters.Selected)
                    {
                        dtClusters.Rows.Add(chkClusters.Value);
                    }
                }
                objClusterMasterBE.propClusterIds = dtClusters;
                if (Request.QueryString["Id"] != null)
                {
                    objClusterMasterBE.propClusterTypeId_PK = Request.QueryString["Id"].ToString();
                    status = objClusterMasterDAL.ClusterTypeMasterUpdate(objClusterMasterBE);
                    if (status > 0)
                    {
                        lblMessage.Text = "Cluster type updated successfully...";
                        ClearFields();
                    }
                }
                else
                {
                  DataTable dtDuplicate=  objClusterMasterDAL.ClusterTypeMasterCheckDuplicate(txtClusterTypeName.Text);
                  if (dtDuplicate != null && dtDuplicate.Rows.Count > 0)
                  {
                      lblMessage.Text = "Cluster type name already exists...";
                  }
                  else
                  {
                      status = objClusterMasterDAL.ClusterTypeMasterInsert(objClusterMasterBE);
                      if (status > 0)
                      {
                          lblMessage.Text = "Cluster type inserted successfully...";
                          ClearFields();
                      }
                  }                   
                }          
            }
            catch
            {

            }
        }

        private void ClearFields()
        {
            try
            {
                txtClusterTypeName.Text = "";
                chklistClusters.ClearSelection();
            }
            catch 
            {
                
            }
        }


    }
}