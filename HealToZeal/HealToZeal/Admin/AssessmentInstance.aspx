﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/Admin.Master" AutoEventWireup="true" CodeBehind="AssessmentInstance.aspx.cs" Inherits="HealToZeal.Admin.AssessmentInstance" %>

<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="LeftColumn" runat="server">
       <div>
           <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
       <table>
           <tr>
               <td colspan="4">
                   <asp:Label ID="lblTitle" runat="server" Text="Assessment Instance"></asp:Label>
               </td>
           </tr> 
              <tr>
            <td>
                <asp:Label ID="lblName" runat="server" Text="Instance"></asp:Label>
              </td>
            <td>
                <asp:TextBox ID="txtInstance" runat="server"></asp:TextBox>
            </td>
            <td>
                &nbsp;</td>
            <td>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtInstance" Display="Dynamic" ErrorMessage="RequiredFieldValidator" ForeColor="Red"></asp:RequiredFieldValidator>
              </td>
        </tr>
        <tr>
            <td>
                  <asp:Label ID="lblAssessment" runat="server" Text="Select assessment/s"></asp:Label>
            </td>
        </tr>       
      <tr>         
           <td colspan="4">           
              <asp:GridView ID="GvAssessments" AutoGenerateColumns="false" runat="server">
                  <Columns>
                      <asp:TemplateField>
                          <HeaderTemplate>
                            <table style="width:100%">
                                <tr>
                                    <td style="width:10%"></td>
                                       <td style="width:50%">Assessment</td>
                                       <td></td>
                                       <td style="width:20%">Display result</td>
                                    <td style="width:20%">Display for company</td>
                                </tr>
                            </table>
                          </HeaderTemplate>
                          <ItemTemplate>
                              <table>
                                  <tr>
                                      <td  style="width:10%">
                                          <asp:CheckBox ID="chkAssessments" runat="server" />
                                      </td>
                                      <td style="width:50%">
                                          <asp:Label ID="lblAssessmentName" runat="server" Text='<%#Eval("AssessmentName") %>'></asp:Label>
                                      </td>
                                      <td>
                                          <asp:Label ID="lblAssessmentId" runat="server" Text='<%#Eval("AssessmentId_PK") %>' Visible="false"></asp:Label>
                                      </td>
                                      <td  style="width:20%">
                                          <asp:CheckBox ID="chkDisplayResult" runat="server" />
                                      </td>
                                      <td  style="width:20%">
                                           <asp:CheckBox ID="chkDisplayResultForCompany" runat="server" />
                                      </td>
                                  </tr>
                              </table>
                          </ItemTemplate>                             
                      </asp:TemplateField>
                  </Columns>
              </asp:GridView>
          </td>
      </tr>
     <tr>
            <td></td>
            <td>
                <asp:Button ID="btnSubmit" runat="server" Text="Assign" OnClick="btnSubmit_Click" />
              </td>
            <td>
                &nbsp;</td>
            <td></td>
        </tr>
          <tr>
            <td colspan="3">
                <asp:Label ID="lblmessage" runat="server" Text=""></asp:Label>
              </td>
            <td></td>
        </tr>
    </table>
    </div>

</asp:Content>
