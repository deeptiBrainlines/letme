﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Admin/Admin.Master" CodeBehind="QuestionMaster.aspx.cs" Inherits="HealToZeal.Admin.QuestionMaster" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .auto-style1 {
            width: 4px;
        }
    </style>
        </asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="LeftColumn" runat="server">
    <div>
        <asp:ScriptManager ID="ScriptManager1" runat="server">
        </asp:ScriptManager>

        <cc1:TabContainer ID="TabContainerQuestionMaster" runat="server"
            ActiveTabIndex="0">
            <cc1:TabPanel ID="TabPnlView" runat="server">
                <HeaderTemplate>View Questions</HeaderTemplate>
                <ContentTemplate>
                    <table>
                        <tr>
                            <td>
                                <asp:DropDownList ID="ddlAssessment" AutoPostBack="True" runat="server" OnSelectedIndexChanged="ddlAssessment_SelectedIndexChanged"></asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:GridView ID="GvViewQuestions" AutoGenerateColumns="False"
                                    runat="server" OnRowCommand="GvViewQuestions_RowCommand">
                                    <Columns>
                                        <asp:TemplateField>
                                            <HeaderTemplate>
                                                <table width="90%">
                                                    <tr>
                                                        <td width="80%">Question </td>
                                                        <%-- <td  width="40%">No of answers </td>--%>
                                                        <td></td>
                                                        <td></td>
                                                    </tr>
                                                </table>
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <table width="90%">
                                                    <tr>
                                                        <td width="80%">
                                                            <asp:Label ID="lblQuestion" runat="server" Text='<%#Eval("Question") %>'></asp:Label></td>
                                                        <%-- <td  width="30%"> 
                  <asp:Label ID="lblNoOfAnswers" runat="server" Text='<%#Eval("NoOfAnswers") %>'></asp:Label>
              </td>--%><td>
                  <asp:Button ID="BtnEdit" runat="server" Text="Edit" CommandName="EditQuestion" CommandArgument='<%#Eval("QuestionId_PK") %>' /></td>
                                                        <td>
                                                            <asp:Button ID="BtnDelete" runat="server" Text="Delete" CommandName="DeleteQuestion" CommandArgument='<%#Eval("QuestionId_PK") %>' Visible="false" /></td>
                                                        <td>
                                                            <asp:Button ID="btnEditAnswers" runat="server" Text="Edit Answers" CommandName="EditAnswer" CommandArgument='<%#Eval("QuestionId_PK") %>' /></td>
                                                    </tr>
                                                </table>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                </asp:GridView>
                            </td>
                        </tr>
                    </table>

                </ContentTemplate>
            </cc1:TabPanel>
            <cc1:TabPanel ID="TabPnlAddUpdate" runat="server">
                <HeaderTemplate>Add/Update Questions</HeaderTemplate>
                <ContentTemplate>
                    <div>
                        <table>
                            <tr>
                                <td valign="top">
                                    <table>
                                        <tr>
                                            <td>&nbsp;</td>
                                            <td>
                                                <asp:Label ID="lblTitle" runat="server" Text="Questions Details"></asp:Label></td>
                                            <td class="auto-style1">&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:Label ID="lblAssessment" runat="server" Text="Assessment"></asp:Label>
                                            </td>
                                            <td>
                                                <asp:DropDownList ID="ddlAssessments" runat="server"></asp:DropDownList>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:Label ID="lblQuestion" runat="server" Text="Question"></asp:Label></td>
                                            <td>
                                                <asp:TextBox ID="txtQuestion" TextMode="MultiLine" runat="server" ValidationGroup="0"></asp:TextBox></td>
                                            <td class="auto-style1">&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:Label ID="lblNoOfAnswers" runat="server" Text="No of answers"></asp:Label></td>
                                            <td style="margin-left: 40px">
                                                <asp:TextBox ID="txtNoOfAnswers" runat="server" OnTextChanged="txtNoOfAnswers_TextChanged" AutoPostBack="True" TabIndex="1" ValidationGroup="0"></asp:TextBox></td>
                                            <td class="auto-style1">&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td colspan="3">
                                                <asp:Panel ID="pnlAnswer" runat="server" Visible="False">
                                                    <table width="100%">
                                                        <tr>
                                                            <td>Answers</td>
                                                            <td>
                                                                <asp:Label ID="lblAnswerNo" runat="server"></asp:Label></td>
                                                            <td></td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <asp:Label ID="lblAnswer" runat="server" Text="Answer"></asp:Label></td>
                                                            <td>
                                                                <asp:TextBox ID="txtAnswer" runat="server" TabIndex="2" ValidationGroup="1"></asp:TextBox></td>
                                                            <td></td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <asp:Label ID="lblAnswerDescription" runat="server" Text="Description"></asp:Label></td>
                                                            <td>
                                                                <asp:TextBox ID="txtAnswerDescription" runat="server" TabIndex="3"
                                                                    ValidationGroup="1"></asp:TextBox></td>
                                                            <td></td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <asp:Label ID="lblScoreForAnswer" runat="server" Text="Score"></asp:Label></td>
                                                            <td>
                                                                <asp:TextBox ID="txtAnswerScore" runat="server" TabIndex="4"
                                                                    ValidationGroup="1"></asp:TextBox></td>
                                                            <td></td>
                                                        </tr>
                                                        <tr>
                                                            <td>&#160;</td>
                                                            <td>
                                                                <asp:Button ID="btnAddAnswer" runat="server" Text="Add Answer"
                                                                    OnClick="btnAddAnswer_Click" TabIndex="5" ValidationGroup="1" /></td>
                                                            <td></td>
                                                        </tr>
                                                    </table>
                                                </asp:Panel>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:Label ID="lblWeightage" runat="server" Text="Weightage"></asp:Label></td>
                                            <td>
                                                <asp:TextBox ID="txtWeightage" runat="server" TabIndex="6" ValidationGroup="0"></asp:TextBox></td>
                                            <td class="auto-style1">&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:Label ID="lblIsReverseScore" runat="server" Text="IsReverseScore"></asp:Label></td>
                                            <td>
                                                <asp:CheckBox ID="chkIsReverseScore" runat="server" TabIndex="7"
                                                    ValidationGroup="0" /></td>
                                            <td class="auto-style1">&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:Label ID="lblFlagNeutralAnswer" runat="server" Text="Neutral Answer"></asp:Label></td>
                                            <td>
                                                <asp:CheckBox ID="chkFlagNeutralAnswer" runat="server" TabIndex="8"
                                                    ValidationGroup="0" /></td>
                                            <td class="auto-style1">&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td></td>
                                            <td>
                                                <asp:Button ID="BtnAddUpdate" runat="server" Text="Submit"
                                                    OnClick="BtnAddUpdate_Click" TabIndex="10" ValidationGroup="0" /><asp:Button ID="BtnReset" runat="server" Text="Reset" TabIndex="11"
                                                        ValidationGroup="0" OnClick="BtnReset_Click" /></td>
                                        </tr>
                                    </table>
                                </td>
                                <td>
                                    <table>
                                        <tr>
                                            <td>

                                                <asp:CheckBoxList ID="chkClusterIds" runat="server" TabIndex="9"
                                                    ValidationGroup="0" Height="100px" RepeatColumns="2">
                                                </asp:CheckBoxList>

                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:GridView ID="GvAnswers" runat="server" AutoGenerateColumns="False" OnRowDeleting="GvAnswers_RowDeleting">
                                                    <Columns>
                                                        <asp:TemplateField>
                                                            <HeaderTemplate>
                                                                <table width="50%">
                                                                    <tr>
                                                                        <td>Answer</td>
                                                                        <td>Description</td>
                                                                        <td>Score</td>
                                                                        <td></td>
                                                                    </tr>
                                                                </table>
                                                            </HeaderTemplate>
                                                            <ItemTemplate>
                                                                <table>
                                                                    <tr>
                                                                        <td>
                                                                            <asp:Label ID="lblAnswer" runat="server" Text='<%#Eval("Answer") %>'></asp:Label></td>
                                                                        <td>
                                                                            <asp:Label ID="lblAnswerDescription" runat="server" Text='<%#Eval("AnswerDescription") %>'></asp:Label></td>
                                                                        <td>
                                                                            <asp:Label ID="lblScoreForAnswer" runat="server" Text='<%#Eval("ScoreForAnswer") %>'></asp:Label></td>
                                                                        <td>
                                                                            <asp:Button ID="btnDelete" runat="server" Text="Delete" CommandArgument='<%#Eval("SrNo") %>' CommandName="Delete" /></td>
                                                                    </tr>
                                                                </table>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                    </Columns>
                                                </asp:GridView>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </div>
                </ContentTemplate>
            </cc1:TabPanel>
        </cc1:TabContainer>
    </div>
            </asp:Content>
 
 
