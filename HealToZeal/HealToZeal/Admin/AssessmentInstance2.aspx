﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="AssessmentInstance.aspx.cs" MasterPageFile="~/Admin/Admin.Master" Inherits="HealToZeal.Admin.AssessmentInstance" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="LeftColumn" runat="server">
    <div>
    <table>
        <tr>
            <td colspan="4">
                <asp:Label ID="lblTitle" runat="server" Text="Assessment Instance"></asp:Label>

            </td>
        
        </tr>
          <tr>
            <td>
                <asp:Label ID="lblName" runat="server" Text="Instance"></asp:Label>
              </td>
            <td>
                <asp:TextBox ID="txtInstance" runat="server"></asp:TextBox>
            </td>
            <td>
                &nbsp;</td>
            <td>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtInstance" Display="Dynamic" ErrorMessage="RequiredFieldValidator" ForeColor="Red"></asp:RequiredFieldValidator>
              </td>
        </tr>
        <tr>
            <td>
                  <asp:Label ID="lblAssessment" runat="server" Text="Select assessment/s"></asp:Label>
            </td>
        </tr>
          <tr>
             
            <td colspan="3">
       <%--         <asp:CheckBoxList ID="chklistAssessments" RepeatDirection="Vertical" runat="server"></asp:CheckBoxList>--%>
                <asp:GridView ID="GvAssessments" runat="server" AutoGenerateColumns="false">
                    <Columns>
                     <asp:TemplateField>
                         <HeaderTemplate>
                             <table>
                                 <tr>
                                     <td></td>
                                     <td>Assessment</td>
                                     <td></td>
                                     <td>Display Result</td>
                                 </tr>
                             </table>
                         </HeaderTemplate>
                         <ItemTemplate>
                             <table>
                                 <tr>
                                     <td>
                                         <asp:CheckBox ID="chkAssessments" runat="server" />
                                     </td>
                                     <td>
                                         <asp:Label ID="lblAssessmentName" runat="server" Text='<%#Eval("AssessmentName") %>'> </asp:Label>
                                     </td>
                                     <td>
                                         <asp:Label ID="lblAssessmentId" runat="server" Text='<%#Eval("AssessmentId_PK") %>' Visible="false"> </asp:Label>
                                     </td>
                                     <td>
                                          <asp:CheckBox ID="chkDisplayResult" runat="server"/>
                                     </td>
                                 </tr>
                             </table>
                         </ItemTemplate>
                     </asp:TemplateField>
                    </Columns>
                </asp:GridView>
            </td>          
        </tr>
          <tr>
            <td></td>
            <td>
                <asp:Button ID="btnSubmit" runat="server" Text="Assign" OnClick="btnSubmit_Click" />
              </td>
            <td>
                &nbsp;</td>
            <td></td>
        </tr>
          <tr>
            <td colspan="3">
                <asp:Label ID="lblmessage" runat="server" Text=""></asp:Label>
              </td>
            <td></td>
        </tr>
    </table>
    </div>
</asp:Content>
