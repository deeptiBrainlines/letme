﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DAL;
using BE;
using System.Data;

namespace HealToZeal.Admin
{
    public partial class ViewRemedySuggestions : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Session["AdminId"] != null)
                {
                    BindAssessments();
                    BindClusters();
                }
            }
        }

        private void BindClusters()
        {
            try
            {
                ClusterMasterDAL objClusterMasterDAL = new ClusterMasterDAL();
                ddlCluster.DataSource= objClusterMasterDAL.ClusterMasterGet();
                ddlCluster.DataTextField = "ClusterName";
                ddlCluster.DataValueField = "ClusterId_PK";
                ddlCluster.DataBind();
                ddlCluster.Items.Insert(0, new ListItem("--Select Cluster--", "--Select Cluster--"));
                ddlCluster.SelectedIndex = 0;
            }
            catch
            {
              
            }
        }

        private void BindAssessments()
        {
            try
            {
                AssessmentDAL objAssessmentDAL = new AssessmentDAL();
                ddlAssessment.DataSource= objAssessmentDAL.AssessmentGet();
                ddlAssessment.DataTextField = "AssessmentName";
                ddlAssessment.DataValueField = "AssessmentId_PK";
                ddlAssessment.DataBind();
            }
            catch 
            {
            }
        }

        private void BindRemedySuggestions()
        {
            try
            {
                RemedySuggestionsDAL objRemedySuggestionsDAL = new RemedySuggestionsDAL();
                DataTable DtRemedysuggestions= objRemedySuggestionsDAL.RemedySuggestionsGetByAssessmentAndClusterId(ddlAssessment.SelectedValue,ddlCluster.SelectedValue);
                dlistRemedySuggestions.DataSource = DtRemedysuggestions;
                dlistRemedySuggestions.DataBind();
            }
            catch 
            {
            }
        }

        protected void dlistRemedySuggestions_ItemCommand(object source, DataListCommandEventArgs e)
        {
            try
            {
                if (e.CommandName == "UpdateSuggestion")
                {
                    Response.Redirect("RemedySuggestions.aspx?Id="+e.CommandArgument.ToString());
                }
            }
            catch
            {
                
            }
        }

        protected void btnShow_Click(object sender, EventArgs e)
        {
            try
            {
                if (ddlAssessment.SelectedIndex != 0 && ddlCluster.SelectedIndex != 0)
                {
                    BindRemedySuggestions();
                }               
            }
            catch
            {
                
            }
        }
    }
}