﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DAL;
using System.Collections;

namespace HealToZeal.Admin
{
    public partial class ViewAllCompanyDetails : System.Web.UI.Page
    {
        public CompanyDetailsDAL objCompanyDetailsDAL = new CompanyDetailsDAL();

        protected void Page_Load(object sender, EventArgs e)
        {
            GridDisplay.DataSource = objCompanyDetailsDAL.GetAllCompanyDetails();
            GridDisplay.DataBind();


        }

        protected void GridDisplay_DataBound(object sender, EventArgs e)
        {
          
		string text = "";
        string text1 = "";
		int count = 0;
        int count1 = 0;
		Hashtable ht = new Hashtable();
        Hashtable ht1 = new Hashtable();
		

		// loop through all rows to get row counts
        for (int i = 0; i < GridDisplay.Rows.Count;i++ )
        {
            Label lbCompnyName = (Label)GridDisplay.Rows[i].FindControl("lbCompnyName");
            Label lblDepartmentname = (Label)GridDisplay.Rows[i].FindControl("lblDepartmentname");

            if (lbCompnyName.Text == text )
                {
                    count++;
                }
                else
                {
                    if (count > 0)
                    {
                        ht.Add(text, count);
                    }
                    text = lbCompnyName.Text;

                    count = 1;
                }
            if (lblDepartmentname.Text == text1)
            {
                count1++;
            }
            else
            {
                if (count1 > 0)
                {
                    ht1.Add(text1, count1);
                }
                text1 = lblDepartmentname.Text;

                count1 = 1;
            }
            
        }


        if (count > 1)
        {
            ht.Add(text, count);
        }
        if (count1 > 1)
        {
            ht1.Remove(text1);
           ht1.Add(text1, count1);
        }
         

		// loop through all rows again to set rowspan
		text = "";
        text1 = "";
         for (int i = 0; i < GridDisplay.Rows.Count;i++ )
        {
            Label lbCompnyName = (Label)GridDisplay.Rows[i].FindControl("lbCompnyName");
            Label lblDepartmentname = (Label)GridDisplay.Rows[i].FindControl("lblDepartmentname");
            LinkButton LnkUpdate = (LinkButton)GridDisplay.Rows[i].FindControl("LnkUpdate");
                if (lbCompnyName.Text == text)
				{
                    lbCompnyName.Text = "";
                    
				}
				else
				{
                    text = lbCompnyName.Text;
                   // GridDisplay.cells[0].RowSpan = Convert.ToInt32(ht[text]);
				}
                if (lblDepartmentname.Text == text1)
                {
                    lblDepartmentname.Text = "";
                    
                    

                }
                else
                {
                    text1 = lblDepartmentname.Text;
                    
                    // GridDisplay.cells[0].RowSpan = Convert.ToInt32(ht[text]);
                }
              
			
		
	}
          
        }

        protected void GridDisplay_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            //if (e.Row.RowType == DataControlRowType.DataRow)
            //{
            //    if (e.Row.RowIndex % 4 == 0)
            //    {
            //        e.Row.Cells[0].Attributes.Add("rowspan", "4");
            //    }
            //    else
            //    {
            //        e.Row.Cells[0].Visible = false;
            //    }
            //}
        }

        

        
    }
}