﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DAL;
using System.Data;
using BE;
namespace HealToZeal.Admin
{
    public partial class RemedySuggestions : System.Web.UI.Page
    {
        RemedySuggestionsDAL objRemedySuggestionsDAL = new RemedySuggestionsDAL();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                if (Session["AdminId"] != null)
                {
                    BindAssessment();
                    BindClusters();
                    if (Request.QueryString["Id"] != null)
                    {
                      DataTable DtRemedySuggestion= objRemedySuggestionsDAL.RemedySuggestionsGetById(Request.QueryString["Id"].ToString());
                      if (DtRemedySuggestion != null && DtRemedySuggestion.Rows.Count > 0)
                      {
                          ddlAssessment.SelectedValue = DtRemedySuggestion.Rows[0]["AssessmentId_FK"].ToString();
                          ddlCluster.SelectedValue = DtRemedySuggestion.Rows[0]["ClusterId_FK"].ToString();
                          ddlColour.SelectedValue = DtRemedySuggestion.Rows[0]["Color"].ToString();
                          txtMaxMarks.Text = DtRemedySuggestion.Rows[0]["MaxScore"].ToString();
                          txtMinMarks.Text = DtRemedySuggestion.Rows[0]["MinScore"].ToString();
                          txtSuggestions.Text = DtRemedySuggestion.Rows[0]["Suggestion"].ToString();
                      }
                    }
                }            
            }
        }

        private void BindClusters()
        {
            try
            {
                ClusterMasterDAL objClusterMasterDAL = new ClusterMasterDAL();
                DataTable DtClusters= objClusterMasterDAL.ClusterMasterGet();
                ddlCluster.DataSource = DtClusters;
                ddlCluster.DataValueField = "ClusterId_PK";
                ddlCluster.DataTextField = "ClusterName";
                ddlCluster.DataBind();

                ddlCluster.Items.Insert(0, new ListItem("--Select Cluster--", "--Select Cluster--"));
                ddlCluster.SelectedIndex = 0;
            }
            catch
            {
               
            }
        }

        private void BindAssessment()
        {
            try
            {
                AssessmentDAL objAssessmentDAL = new AssessmentDAL();
                DataTable DtAssessment = objAssessmentDAL.AssessmentGet();
                ddlAssessment.DataSource = DtAssessment;
                ddlAssessment.DataValueField = "AssessmentId_PK";
                ddlAssessment.DataTextField = "AssessmentName";
                ddlAssessment.DataBind();

                ddlAssessment.Items.Insert(0, new ListItem("--Select Assessment--", "--Select Assessment--"));
                ddlAssessment.SelectedIndex = 0;
            }
            catch
            {
                
            }
        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            try
            {
                int status = 0;
           
                RemedySuggestionsBE objRemedySuggestionsBE = new RemedySuggestionsBE();

                objRemedySuggestionsBE.propAssessmentId_FK = ddlAssessment.SelectedValue;
                objRemedySuggestionsBE.propClusterId_FK = ddlCluster.SelectedValue;
                objRemedySuggestionsBE.propColor = ddlColour.SelectedValue;
                objRemedySuggestionsBE.propMinScore =Convert.ToInt32( txtMinMarks.Text);
                objRemedySuggestionsBE.propMaxScore =Convert.ToInt32( txtMaxMarks.Text);
                objRemedySuggestionsBE.propSuggestion = txtSuggestions.Text;
                objRemedySuggestionsBE.propCreatedBy = Session["AdminId"].ToString();
                objRemedySuggestionsBE.propCreatedDate = DateTime.Now.Date;

                if (Request.QueryString["Id"] != null)
                {
                    objRemedySuggestionsBE.RemedySuggestionId_PK = Request.QueryString["Id"].ToString();
                    status = objRemedySuggestionsDAL.RemedySuggestionsUpdate(objRemedySuggestionsBE);
                    if (status > 0)
                    {
                        lblMessage.Text = "Remedy suggestion updated successfully...";
                    }
                }
                else
                {
                    status = objRemedySuggestionsDAL.RemedySuggestionsInsert(objRemedySuggestionsBE);
                    if (status > 0)
                    {
                        lblMessage.Text = "Remedy suggestion inserted successfully...";
                    }
                }
            }
            catch
            {

            }
        }
    }
}