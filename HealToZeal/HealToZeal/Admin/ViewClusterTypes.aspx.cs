﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DAL;
using System.Data;
namespace HealToZeal.Admin
{
    public partial class ViewClusterTypes : System.Web.UI.Page
    {
        ClusterMasterDAL objClusterMasterDAL = new ClusterMasterDAL();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
              DlistClusterTypes.DataSource= objClusterMasterDAL.ClusterTypeMasterGet();
              DlistClusterTypes.DataBind();
            }
        }

        protected void DlistClusterTypes_ItemCommand(object source, DataListCommandEventArgs e)
        {
            try
            {
                if (e.CommandName == "UpdateClustertype")
                {
                    
                    Response.Redirect("ClusterType.aspx?Id="+ e.CommandArgument.ToString());
                }
            }
            catch 
            {
              
            }
        }

        protected void DlistClusterTypes_ItemDataBound(object sender, DataListItemEventArgs e)
        {
            try
            {
                DataList DlistClusters = (DataList)e.Item.FindControl("DlistClusters");
                Label lblClusterTypeId = (Label)e.Item.FindControl("lblClusterTypeId");

                DlistClusters.DataSource = objClusterMasterDAL.ClusterTypeMasterGetClustersById(lblClusterTypeId.Text);
                DlistClusters.DataBind();
            }
            catch 
            {
                
            }        
        }
    }
}