﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ViewAssessment.aspx.cs" MasterPageFile="~/Admin/Admin.Master" Inherits="HealToZeal.Admin.ViewAssessment" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="LeftColumn" runat="server">
    <div>
        <table>
            <tr>
                <td colspan="3">
                    <asp:Label ID="lblTitle" runat="server" Text="Assessments"></asp:Label>
                </td>            
            </tr>
            <tr>
                <td></td>
                <td>
                      <asp:GridView ID="GvViewAssessment" AutoGenerateColumns="False"
                      runat="server" OnRowCommand="GvViewAssessment_RowCommand">
                      <Columns>
                          <asp:TemplateField>
                              <HeaderTemplate>
                                  <table style="width:100%">
                                      <tr>
                                          <td style="width:60%">Assessment</td>
                                            <td></td>
                                            <td style="width:20%">No of questions</td>
                                            <td style="width:20%">Is complete</td>
                                            <td></td>
                                      </tr>
                                  </table>
                              </HeaderTemplate>
                              <ItemTemplate>
                                       <table style="width:100%">
                                <tr>
                                    <td style="width:60%">
                                        <asp:Label ID="lblAssessmentName" runat="server" Text='<%#Eval("AssessmentName") %>'> </asp:Label>
                                    </td>
                                    <td>
                                        <asp:Label ID="lblAssessmentId_PK" runat="server" Text='<%#Eval("AssessmentId_PK") %>' Visible="false"> </asp:Label>
                                    </td>
                                    <td style="width:20%">
                                        <asp:Label ID="lblNoOfQuestions" runat="server" Text='<%#Eval("NoOfQuestions") %>'> </asp:Label>
                                    </td>
                                    <td style="width:20%">
                                        <asp:Label ID="lblIsComplete" runat="server" Text='<%#Eval("IsComplete") %>'> </asp:Label>
                                    </td>                                
                                    <td style="width:20%">
                                        <asp:Button ID="btnUpdateAssessment" runat="server" Text="Update" CommandName="UpdateAssessment" CommandArgument='<%#Eval("AssessmentId_PK") %>' />
                                    </td>
                                </tr>
                            </table>
                              </ItemTemplate>
                              </asp:TemplateField>
                          </Columns>
                          </asp:GridView>
                 

                </td>
                <td></td>
            </tr>
         
        </table>
    </div>
</asp:Content>