﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using DAL;
using BE;

namespace HealToZeal.Admin
{
   
    public partial class WebForm2 : System.Web.UI.Page
    {
        public static string deptId;
        CompanyDepartmentBE objCompanyDeptBE = new CompanyDepartmentBE();
        CompanyDepartmentDAL objCompanyDeptDAL = new CompanyDepartmentDAL();
        CompanyDetailsDAL objCompanyDetailsDAL = new CompanyDetailsDAL();

        protected void Page_Load(object sender, EventArgs e)
        {

            //DataTable dt = objCompanyDeptDAL.GetcompnyName(objCompanyDeptBE.propCompanyId_FK);
            DataTable DtCompanyDetails = objCompanyDetailsDAL.CompanyDetailsGet();
            ddlCompany.DataSource = objCompanyDetailsDAL.CompanyDetailsGet();
            ddlCompany.DataBind();
          
            if (!Page.IsPostBack)
            {
                if (Session["AdminId"] != null)
                {
                    BindCompanies();
                    if (Request.QueryString["Id"] != null)
                    {
                        DataTable Dtdept = objCompanyDeptDAL.CompanyDeaprtmentGetByCompanyId(Request.QueryString["Id"].ToString());
                        if (Dtdept != null && Dtdept.Rows.Count > 0)
                        {
                            ddlCompany.SelectedValue = Dtdept.Rows[0][""].ToString();
                        }
                    }
                }
            }
        }
        private void BindCompanies()
        {
            try
            {
                CompanyDetailsDAL objCompanyDetailsDAL = new CompanyDetailsDAL();
                ddlCompany.DataSource = objCompanyDetailsDAL.CompanyDetailsGet();
                ddlCompany.DataTextField = "CompanyName";
                ddlCompany.DataValueField = "CompanyId_PK";
                ddlCompany.DataBind();
                ddlCompany.Items.Insert(0, new ListItem("--Select Company--", "--Select Company--"));
                ddlCompany.SelectedIndex = 0;
            }
            catch
            {

            }
        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            try
            {
                int status = 0;
                objCompanyDeptBE.propCompanyId_FK = ddlCompany.SelectedValue;
                objCompanyDeptBE.propDepartmentName = txtCompanyDepartment.Text;
                objCompanyDeptBE.propCreatedBy = Session["AdminId"].ToString();
                objCompanyDeptBE.propCreatedDate = DateTime.Now.Date;

                if (Request.QueryString["Id"] != null)
                {
                    objCompanyDeptBE.propDeptId_PK = Request.QueryString["Id"].ToString();

                    status = objCompanyDeptDAL.DepartmentUpdate(objCompanyDeptBE);
                    if (status > 0)
                    {
                       
                        ScriptManager.RegisterClientScriptBlock(this.Page, this.GetType(), "startScript", "<script>alert('Deaprtment updated successfully'); </script>", false);
                         ClearFields();
                    }
                }
                else
                {
                    DataTable dtcheck = objCompanyDeptDAL.TocheckDepartmentExistsOrNot(txtCompanyDepartment.Text, objCompanyDeptBE.propCompanyId_FK);
                   
                         if(dtcheck.Rows.Count>0)
                         {
                             status = objCompanyDeptDAL.CompanyDepartInsert(objCompanyDeptBE);
                               if (status > 0)
                               {
                               // ScriptManager.RegisterClientScriptBlock(this.Page, this.GetType(), "startScript", "<script>alert('Deaprtment added successfully'); </script>", false); ;
                                  ClearFields();
                               }
                               Response.Redirect("CompanyCadre.aspx");


                          }
                         else
                         {
                             ScriptManager.RegisterClientScriptBlock(this.Page, this.GetType(), "startScript", "<script>alert('Deaprtment alredy added successfully'); </script>", false); ;
                         }
                }

            }
            catch
            {

            }
        }

        private void ClearFields()
        {
            try
            {
                txtCompanyDepartment.Text = "";
                ddlCompany.SelectedIndex = 0;
            }
            catch
            {

            }
        }
    }
}