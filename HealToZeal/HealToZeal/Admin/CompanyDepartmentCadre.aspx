﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/Admin.Master" AutoEventWireup="true" CodeBehind="CompanyDepartmentCadre.aspx.cs" Inherits="HealToZeal.Admin.CompanyDepartmentCadre" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="LeftColumn" runat="server">
    <table>
        <tr>
            <td></td>
            <td>
                <asp:Label ID="lblTitle" runat="server" Font-Bold="true" Text="Company Department" ></asp:Label>
            </td>
            <td></td>
            <td></td>
        </tr>
        
        <tr>
      </tr>
        <tr></tr>
        <tr>
            <td>
                <asp:Label ID="lblCompany" runat="server" Text="Company"></asp:Label>
            </td>
            &nbsp;
            <td>
                <asp:DropDownList ID="ddlCompany"  runat="server" Width="205px" Height="30px" ></asp:DropDownList>
                
            </td>
            <td></td>
            <td></td>
        </tr>
        <tr>

        </tr>
        <tr></tr>
        <tr>
            <td>
                <asp:Label ID="lblCompanyDepartment" runat="server" Text="Department"></asp:Label>
            </td>
            <td>
                <asp:DropDownList ID="ddCompanyDepartments"  runat="server" Width="205px" Height="30px" ></asp:DropDownList>
            </td>
            <td>
            
            </td>
            <td></td>
        </tr>
        <tr>

        </tr>
        <tr></tr>
        <tr>
            <td>
                <asp:Label ID="lblCadre" runat="server" Text="Cadre
                    "></asp:Label>
            </td>
            <td>
                <asp:DropDownList ID="ddCadre"  runat="server" Width="205px" Height="30px" ></asp:DropDownList>
            </td>
            <td>
            
            </td>
            <td></td>
        </tr>
        <tr>
            <td></td>
            <td>
        
                <asp:Button ID="btnSubmit" runat="server" Text="Submit" Width="80px"  />
        
            </td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td></td>
            <td>
                <asp:Label ID="lblMessage" runat="server" Text=""></asp:Label>
            </td>
            <td></td>
            <td></td>
        </tr>
    </table>
</asp:Content>
