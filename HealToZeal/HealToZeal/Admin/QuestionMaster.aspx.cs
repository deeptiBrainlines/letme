﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DAL;
using BE;
using System.Data;
using System.Text;
namespace HealToZeal.Admin
{
    public partial class QuestionMaster : System.Web.UI.Page
    {
        QuestionMasterDAL objQuestionMasterDAL = new QuestionMasterDAL();
        QuestionMasterBE objQuestionMasterBE = new QuestionMasterBE();
        AnswerMasterBE objAnswerMasterBE = new AnswerMasterBE();
        AnswerMasterDAL objAnswerMasterDAL = new AnswerMasterDAL();

       static  List<AnswerMasterBE> objListAnswer = new List<AnswerMasterBE>();
       static DataTable DtAnswer = new DataTable();

       string adminId = "";
        string QuestionId = "";
        static int NoOfAnswers = 0;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Session["AdminId"] != null)
                {
                    BindAssessments();
                    BindCheckBoxList();
                    BindQuestionDetailsGrid();
                    if (DtAnswer.Columns.Count <= 0)
                    {
                        DtAnswer.Columns.Add(new System.Data.DataColumn("SrNo", typeof(int)));
                        DtAnswer.Columns.Add(new System.Data.DataColumn("Answer", typeof(string)));
                        DtAnswer.Columns.Add(new System.Data.DataColumn("AnswerDescription", typeof(string)));
                        DtAnswer.Columns.Add(new System.Data.DataColumn("ScoreForAnswer", typeof(int)));
                    }
                }
                else
                {
                    Response.Redirect("AdminHome.aspx", false);
                }              
            }              
        }

        private void BindAssessments()
        {
            try
            {
                AssessmentDAL objAssessmentDAL = new AssessmentDAL();
                ddlAssessments.DataSource = objAssessmentDAL.AssessmentGet();
                ddlAssessments.DataTextField = "AssessmentName";
                ddlAssessments.DataValueField = "AssessmentId_PK";
                ddlAssessments.DataBind();
            
                ddlAssessment.DataSource = objAssessmentDAL.AssessmentGet();
                ddlAssessment.DataTextField = "AssessmentName";
                ddlAssessment.DataValueField = "AssessmentId_PK";
                ddlAssessment.DataBind();

                ddlAssessment.Items.Insert(0, new ListItem("--Select assessment--", "--Select assessment--"));
                ddlAssessment.SelectedIndex = 0;
            }
            catch 
            {

            }
        }

        private void BindQuestionDetailsGrid()
        {
            try
            {
                DataTable DtQuestion = objQuestionMasterDAL.QuestionMasterGet();
                if (DtQuestion != null && DtQuestion.Rows.Count > 0)
                {
                    GvViewQuestions.DataSource = DtQuestion;
                    GvViewQuestions.DataBind();
                }
            }
            catch
            {

            }
        }

        private void BindCheckBoxList()
        {
            try
            {
               ClusterMasterDAL objClusterMasterDAL = new ClusterMasterDAL();
               DataTable DtClusters=  objClusterMasterDAL.ClusterMasterGet();
               if (DtClusters != null && DtClusters.Rows.Count > 0)
               {
                   chkClusterIds.DataSource = DtClusters;
                   chkClusterIds.DataTextField = "ClusterName";
                   chkClusterIds.DataValueField = "ClusterId_PK";
                   chkClusterIds.DataBind();
               }
            }
            catch 
            {
                
            }
        }

        protected void BtnAddUpdate_Click(object sender, EventArgs e)
        {
            try
            {
                InsertUpdateQuestions();
            }
            catch 
            {
                
            }
        }

        private void InsertUpdateQuestions()
        {
            try
            {
                if( Session["AdminId"]!=null)
                {
                    adminId = Session["AdminId"].ToString();
                
                objQuestionMasterBE.propQuestion = txtQuestion.Text;
                objQuestionMasterBE.propWeightage =Convert.ToInt32(txtWeightage.Text);
                objQuestionMasterBE.propNoOfAnswers =Convert.ToInt32(txtNoOfAnswers.Text);
                if(chkIsReverseScore.Checked)
                {
                    objQuestionMasterBE.propIsReverseScore = "Y";                
                }
                else
                {
                    objQuestionMasterBE.propIsReverseScore = "N";
                }
                if (chkFlagNeutralAnswer.Checked)
                {
                    objQuestionMasterBE.propFlagNeutralAnswer = "Y";
                }
                else                  
                {
                    objQuestionMasterBE.propFlagNeutralAnswer = "N";
                }

                StringBuilder clusterIds = new StringBuilder();
                foreach(ListItem checkedClusters in chkClusterIds.Items)
                {                  
                    if (checkedClusters.Selected )
                    {
                        if (clusterIds.ToString() != "")
                        {
                            clusterIds.Append(",");
                        }
                        clusterIds.Append(checkedClusters.Value);
                    }
                }
                objQuestionMasterBE.propClusterIds = clusterIds.ToString();
                objQuestionMasterBE.propCreatedBy = adminId; //"18DD4921-9D48-43C6-805E-BE5CA95AC286";//user id
                objQuestionMasterBE.propCreatedDate = DateTime.Now.Date;
                objQuestionMasterBE.propUpdatedBy = adminId; // "18DD4921-9D48-43C6-805E-BE5CA95AC286";//user id
                objQuestionMasterBE.propUpdatedDate = DateTime.Now.Date;
                int status = 0;
                    string message = string.Empty;
                if (Session["QuestionId"] != null)
                {
                    QuestionId = Session["QuestionId"].ToString();
                  objQuestionMasterBE.propQuestionId_PK = QuestionId;
                  status=objQuestionMasterDAL.QuestionMasterUpdate(objQuestionMasterBE);
                  if (status > 0)
                  {
                      message = "Question updated successully...";
                      TabContainerQuestionMaster.ActiveTabIndex = 0;
                      clearQuestion();
                  }
                  else
                  {
                      message = "Can not update question...";
                  }
                }
                else 
                {
                    DtAnswer.Columns.Remove("SrNo");
                    DtAnswer.AcceptChanges();
                    objQuestionMasterBE.propAnswerTable = DtAnswer;

                    objQuestionMasterBE.propAssessmentId_FK = ddlAssessments.SelectedValue;

                    status = objQuestionMasterDAL.QuestionMasterInsert(objQuestionMasterBE);
                    if (status >0)
                    {
                        //InsertAnswers(QuestionId);
                        message = "Question added successully...";
                        clearQuestion();
                    }
                    else
                    {
                        message = "Can not add question...";
                    }
                }
                BindQuestionDetailsGrid();
                }
            }
            catch(Exception e) 
            {
                throw e;
                
            }
        }

        //private void InsertAnswers(string QuestionId)
        //{
        //    try
        //    {
        //        int answerStatus = 0;
        //        if (objListAnswer != null)
        //        {
        //            foreach(AnswerMasterBE answer in objListAnswer)
        //            {
        //                objAnswerMasterBE.propQuestionId_FK = QuestionId;
        //                objAnswerMasterBE.propAnswer = answer.propAnswer;
        //                objAnswerMasterBE.propAnswerDescription = answer.propAnswerDescription;
        //                objAnswerMasterBE.propScoreForAnswer = answer.propScoreForAnswer;
        //                objAnswerMasterBE.propCreatedBy = answer.propCreatedBy;

        //                answerStatus = objAnswerMasterDAL.AnswerMasterInsert(objAnswerMasterBE);
        //                if (answerStatus <= 0)
        //                    break;                     
        //            }
        //            clearQuestion();
        //        }
        //    }
        //    catch 
        //    {
                
        //    }
        //}

        private void clearQuestion()
        {
            try
            {
                txtQuestion.Text = "";
                txtNoOfAnswers.Text = "";
                txtWeightage.Text = "";
                chkFlagNeutralAnswer.Checked = false;
                chkIsReverseScore.Checked = false;
                chkClusterIds.ClearSelection();
                clearAnswer();
                objListAnswer = null;
                DtAnswer.Rows.Clear();
                Session["QuestionId"] = null;
                GvAnswers.DataSource = null;
                GvAnswers.DataBind();
                if (!DtAnswer.Columns.Contains("SrNo"))
                {
                    DataColumn SrNo=DtAnswer.Columns.Add("SrNo", typeof(int));
                    SrNo.SetOrdinal(0);
                    DtAnswer.AcceptChanges();
                }
            }
            catch 
            {

            }
        }

        protected void GvViewQuestions_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                QuestionId = e.CommandArgument.ToString();
                    //add session
                    Session["QuestionId"] = QuestionId;
                if (e.CommandName == "EditQuestion")
                {                    
                    BindQuestion();     
               
                }
                else if (e.CommandName == "EditAnswer")
                {
                    Response.Redirect("AnswerMaster.aspx");
                }
            }
            catch
            {

            }
        }

        private void BindQuestion()
        {
            try
            {
                QuestionId = Session["QuestionId"].ToString();
                DataTable DtQuestionDetails = objQuestionMasterDAL.QuestionMasterGetByQuestionId(QuestionId);

                    if (DtQuestionDetails != null && DtQuestionDetails.Rows.Count > 0)
                    {
                        txtQuestion.Text = DtQuestionDetails.Rows[0]["Question"].ToString();
                        txtNoOfAnswers.Text = DtQuestionDetails.Rows[0]["NoOfAnswers"].ToString();
                        txtWeightage.Text = DtQuestionDetails.Rows[0]["Weightage"].ToString();
                        if (DtQuestionDetails.Rows[0]["IsReverseScore"].ToString() == "Y")
                            chkIsReverseScore.Checked = true;
                        if (DtQuestionDetails.Rows[0]["FlagNeutralAnswer"].ToString() == "Y")
                            chkFlagNeutralAnswer.Checked = true;
                        string ClusterIds = DtQuestionDetails.Rows[0]["ClusterIds"].ToString();

                        string[] ArrClusterIds = ClusterIds.Split(',');
                        if (ArrClusterIds.Length > 0)
                        {
                            foreach (ListItem chkclusters in chkClusterIds.Items)
                            {
                                if (ArrClusterIds.Contains(chkclusters.Value))
                                    chkclusters.Selected = true;
                                else
                                    chkclusters.Selected = false;
                            }
                        }
                        TabContainerQuestionMaster.ActiveTabIndex = 1;
                    }
            }
            catch
            {
                
            }
        }

        protected void txtNoOfAnswers_TextChanged(object sender, EventArgs e)
        {
            try
            {
                if (txtNoOfAnswers.Text != "" && txtNoOfAnswers.Text != "0")
                {
                    NoOfAnswers = Convert.ToInt32(txtNoOfAnswers.Text);
                    lblAnswerNo.Text = "Answer No: " + (DtAnswer.Rows.Count + 1);                   
                    pnlAnswer.Visible = true;
                }
                else
                {
                    pnlAnswer.Visible = false;
                }
            }
            catch 
            {

            }
        }

        protected void btnAddAnswer_Click(object sender, EventArgs e)
        {
            try
            {
              //  objAnswerMasterBE.propAnswer = txtAnswer.Text;
              //  objAnswerMasterBE.propAnswerDescription = txtAnswerDescription.Text;
              //  objAnswerMasterBE.propScoreForAnswer =Convert.ToInt32(txtAnswerScore.Text);
              ////  objAnswerMasterBE.propCreatedBy = "18DD4921-9D48-43C6-805E-BE5CA95AC286"; // user Id

              //  objListAnswer.Add(objAnswerMasterBE);

              //  Session["NoOfAnswers"] = objListAnswer.Count;
              //  clearAnswer();
              //  lblAnswerNo.Text ="Answer No: "+ (objListAnswer.Count + 1);
              //  if (objListAnswer.Count >= NoOfAnswers)
              //  {
              //      pnlAnswer.Visible = false;
              //  }

                  int srNo=0;
                if(DtAnswer.Rows.Count>0)
                 srNo=Convert.ToInt32( DtAnswer.Rows[DtAnswer.Rows.Count-1]["SrNo"].ToString());            
                DtAnswer.Rows.Add((srNo+1), txtAnswer.Text, txtAnswerDescription.Text, Convert.ToInt32(txtAnswerScore.Text));
                Session["NoOfAnswers"] = DtAnswer.Rows.Count;
                clearAnswer();
                lblAnswerNo.Text = "Answer No: " + (DtAnswer.Rows.Count + 1);
                if (DtAnswer.Rows.Count >= NoOfAnswers)
                {
                    pnlAnswer.Visible = false;
                }

                GvAnswers.DataSource = DtAnswer;
                GvAnswers.DataBind();
            }
            catch 
            {
                
            }
        }

        private void clearAnswer()
        {
            try
            {
                txtAnswer.Text = "";
                txtAnswerDescription.Text = "";
                txtAnswerScore.Text = "";
            }
            catch
            {
                
            }
        }

        protected void BtnReset_Click(object sender, EventArgs e)
        {
            try
            {
                clearQuestion();
            }
            catch
            {
            }
        }      

        protected void GvAnswers_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            try
            {
                int answerNo = Convert.ToInt32(e.RowIndex);
                DtAnswer.Rows.RemoveAt(answerNo);
                DtAnswer.AcceptChanges();
                GvAnswers.DataSource = DtAnswer;
                GvAnswers.DataBind();

                pnlAnswer.Visible = true;
                lblAnswerNo.Text = "Answer No: " + (DtAnswer.Rows.Count + 1);           

            }
            catch 
            {
            }
        }

        protected void ddlAssessment_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
               DataTable DtQuestions= objQuestionMasterDAL.QuestionMasterGetByAssessmentId(ddlAssessment.SelectedValue);
               GvViewQuestions.DataSource = DtQuestions;
               GvViewQuestions.DataBind();
            }
            catch
            {
                
            }
        }    
    }
}