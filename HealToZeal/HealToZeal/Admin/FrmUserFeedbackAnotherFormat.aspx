﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/Admin.Master" AutoEventWireup="true" CodeBehind="FrmUserFeedbackAnotherFormat.aspx.cs" Inherits="HealToZeal.Admin.FrmUserFeedbackAnotherFormat" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="LeftColumn" runat="server">
    <asp:ScriptManager ID="ScrMngr" runat="server"></asp:ScriptManager>
    <asp:UpdatePanel ID="UpPnl" runat="server">
        <ContentTemplate>
        <table style="width: 100%">
            <tr>
                <td class="auto-style1"></td>
                <td align="center">
                    <asp:Label ID="Label1" runat="server" Text="Report"></asp:Label>
                </td>
                <td></td>
            </tr>
            <tr>
                <td class="auto-style1">
                    <asp:Label ID="Label2" runat="server" Text="Select company"></asp:Label>
                </td>
                <td>
                    <asp:DropDownList ID="DrpCompany" AutoPostBack="true" runat="server" Width="90%" OnSelectedIndexChanged="DrpCompany_SelectedIndexChanged">
                    </asp:DropDownList>
                </td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td colspan="3">
                    <asp:GridView ID="GvFeedback" runat="server" Width="100%" BorderStyle="None"
                        AutoGenerateColumns="False" AllowSorting="True">
                        <Columns>
                            <asp:TemplateField>
                                <HeaderTemplate>
                                    <table style="width: 100%">
                                        <tr>
                                            <td style="width:60%" align="left">
                                                <asp:Label Width="200px" ID="lblEmployeeName" runat="server" Text="Question"></asp:Label>
                                            </td>
                                            <td style="width:10%" align="left" >
                                                <asp:Label ID="lblQualification" runat="server" Text="Answer">
                                                </asp:Label>
                                            </td>
                                            <td style="width:20%">
                                                <asp:Label ID="Label11" runat="server" Text="Feedback">
                                                </asp:Label>
                                            </td>
                                            <td style="width:10%">
                                                <asp:Label ID="Label3" runat="server" Text="Employee">
                                                </asp:Label>
                                            </td>
                                        </tr>
                                    </table>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <table style="width: 100%">
                                        <tr>
                                            <td style="width:60%">
                                                <asp:Label Width="100%" ID="Label7" runat="server" Text='<%#Eval("FeedbackQuestion") %>'>
                                                </asp:Label>
                                            </td>
                                            <td style="width:10%" align="left" >
                                                <asp:Label Width="100%"  ID="lblQualification" runat="server" Text='<%#Eval("FeedbackAnswer") %>'>
                                                </asp:Label>
                                            </td>
                                            <td style="width:20%">
                                                <asp:Label ID="Label12" runat="server" Text='<%#Eval("Feedback") %>'>
                                                </asp:Label>
                                            </td>
                                             <td style="width:10%">
                                                <asp:Label ID="Label4" runat="server" Text='<%#Eval("Name") %>'>
                                                </asp:Label>
                                            </td>
                                        </tr>
                                    </table>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                    </asp:GridView>
                </td>
            </tr>
        </table>
       </ContentTemplate>
        </asp:UpdatePanel>

</asp:Content>
