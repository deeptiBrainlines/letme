﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using DAL;
using BE;

namespace HealToZeal.Admin
{
    public partial class CompanyCadre : System.Web.UI.Page
    {
        CompanyCadreBE objCompanyCadreBE = new CompanyCadreBE();
        CompanyDetailsDAL objCompanyDetailsDAL = new CompanyDetailsDAL();
        CompanyCadreDAL objCompanyCadreDAL = new CompanyCadreDAL();
       
         
        protected void Page_Load(object sender, EventArgs e)
        {
             DataTable dt = objCompanyDetailsDAL.GetAllCompanyDetails();

            if (!Page.IsPostBack)
            {
                if (Session["AdminId"] != null)
                {
                    BindCompanies();
                   
                    if (Request.QueryString["Id"] != null)
                    {
                        DataTable DtCadre = objCompanyCadreDAL.CompanyCadreGetByCadreId(Request.QueryString["Id"].ToString());
                        if (DtCadre != null && DtCadre.Rows.Count > 0)
                        {
                            ddlCompany.SelectedValue = DtCadre.Rows[0][""].ToString();
                        }
                    }
                    
                }
                
            }
        }

        private void BindCompanies()
        {
            try
            {
                CompanyDetailsDAL objCompanyDetailsDAL = new CompanyDetailsDAL();
                ddlCompany.DataSource=  objCompanyDetailsDAL.CompanyDetailsGet();
                ddlCompany.DataTextField = "CompanyName";
                ddlCompany.DataValueField = "CompanyId_PK";
                ddlCompany.DataBind();
                ddlCompany.Items.Insert(0, new ListItem("--Select Company--", "--Select Company--"));
                ddlCompany.SelectedIndex = 0;
            }
            catch 
            {
                
            }
        }

       
        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            try
            {      
                 int status=0;
              objCompanyCadreBE.propCompanyId_FK = ddlCompany.SelectedValue;
              objCompanyCadreBE.propCadreName = txtCompanyCadre.Text;
              objCompanyCadreBE.propCreatedBy = Session["AdminId"].ToString();
              objCompanyCadreBE.propCreatedDate = DateTime.Now.Date;

              if (Request.QueryString["Id"] != null)
              {
                  objCompanyCadreBE.propCadreId_PK = Request.QueryString["Id"].ToString();

                  status = objCompanyCadreDAL.CadreUpdate(objCompanyCadreBE);
                  if (status > 0)
                  {
                      lblMessage.Text = "Cadre updated successfully...";
                      ClearFields();
                  }
              }
              else
              {
                  status = objCompanyCadreDAL.CompanyCadreInsert(objCompanyCadreBE);
                  if (status > 0)
                  {
                      lblMessage.Text = "Cadre added successfully...";
                      ClearFields();
                  }
              }
             
            }
            catch 
            {
                
            }
        }

        private void ClearFields()
        {
            try
            {
                txtCompanyCadre.Text = "";
                ddlCompany.SelectedIndex = 0;
            }
            catch
            {
                
            }
        }
    }
}