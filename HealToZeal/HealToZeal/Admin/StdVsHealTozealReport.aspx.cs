﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DAL;
using BE;
using System.Data;

namespace HealToZeal.Admin
{
    public partial class StdVsHealTozealReport : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                BindCompanyNames();
            }
        }

        public void BindCompanyNames()
        {
            try
            {
                CompanyDetailsDAL ObjDAL = new CompanyDetailsDAL();
                DataTable Dtcomapny = ObjDAL.CompanyDetailsGet();

                if (Dtcomapny != null)
                {
                    if (Dtcomapny.Rows.Count > 0)
                    {
                        DrpCompany.DataSource = Dtcomapny;
                        DrpCompany.DataValueField = "CompanyId_PK";
                        DrpCompany.DataTextField = "CompanyName";
                        DrpCompany.DataBind();
                        DrpCompany.Items.Insert(0, new ListItem("--Select company--", "--Select company--"));
                    }
                }
            }
            catch (Exception )
            {

            }
        }

        protected void DrpCompany_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if(DrpCompany.SelectedItem.Text !="--Select company--")
                {
                    EmployeeDAL ObjDAL = new EmployeeDAL();
                    DataTable DtEmployees = ObjDAL.EmployeesGetByCompanyId(DrpCompany.SelectedValue.ToString(), 2);
                    
                    if(DtEmployees !=null)
                    {
                        if(DtEmployees.Rows.Count>0)
                        {
                            DrpEmployeesCompanyID.DataSource = DtEmployees;
                            DrpEmployeesCompanyID.DataValueField = "EmployeeId_PK";
                            DrpEmployeesCompanyID.DataTextField = "EmployeeCompanyId";
                            DrpEmployeesCompanyID.DataBind();
                            DrpEmployeesCompanyID.Items.Insert(0, new ListItem("--Select employeecompanyId--", "--Select employeecompanyId--"));

                            DrpEmployees.DataSource = DtEmployees;
                            DrpEmployees.DataValueField = "EmployeeId_PK";
                            DrpEmployees.DataTextField = "EmployeeName";
                            DrpEmployees.DataBind();
                            DrpEmployees.Items.Insert(0, new ListItem("--Select employee--", "--Select employee--"));
                        }
                    }

                    DataTable DtResultAll = ObjDAL.GetStdVsChetanaScoreByCompanyID(DrpCompany.SelectedValue.ToString());
                   
                    if(DtResultAll!=null)
                    {
                        if(DtResultAll.Rows.Count>0)
                        {
                            TrAll.Visible = true;
                            TrEmp.Visible = false;

                            DataView view = new DataView(DtResultAll);
                            DataTable distinctValuesEmp = view.ToTable(true, "EmployeeId_FK", "Name","EmployeeCompanyId");

                            GdvAll.DataSource = distinctValuesEmp;
                            GdvAll.DataBind(); 

                            //DataTable DtTble = DtResultAll.DefaultView.ToTable();
                            DataView view1 = new DataView(DtResultAll);
                            DataTable distinctValues = view1.ToTable(true, "EmployeeId_FK");

                            for (int i = 0; i < distinctValues.Rows.Count; i++)
                            {
                               // Label LblEmpName = (Label)GdvAll.Rows[i].FindControl("LblEmpId");
                               // if(LblEmpName !=null)
                                {
                                   // if(LblEmpName.Text !="")
                                    {
                                        GridView Gdvinner = (GridView)GdvAll.Rows[i].FindControl("GdvAllInner");
                                        DataTable DtEmpData = DtResultAll.Select("EmployeeId_FK='" + distinctValues.Rows[i]["EmployeeId_FK"].ToString() + "'").CopyToDataTable();
                                        if(DtEmpData!=null)
                                        {
                                            if(DtEmpData.Rows.Count>0)
                                            {
                                                Gdvinner.DataSource = DtEmpData;
                                                Gdvinner.DataBind();
                                            }
                                        }

                                    }
                                }
                            }
                        }
                        else
                        {
                            TrAll.Visible = false;
                            TrEmp.Visible = false;
                        }
                    }
                    else
                    {
                        TrAll.Visible = false;
                        TrEmp.Visible = false;
                    }
                }
            }
            catch (Exception )
            {

            }
        }

        protected void DrpEmployees_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (DrpEmployees.SelectedItem.Text != "--Select employee--")
                {
                    DrpEmployeesCompanyID.SelectedIndex = 0;
                   
                    EmployeeDAL ObjDAL = new EmployeeDAL();
                    DataTable DtEmployeeResult = ObjDAL.GetStdVsChetanaclusterScore(DrpEmployees.SelectedValue.ToString(), DrpCompany.SelectedValue.ToString());
                    
                    if (DtEmployeeResult != null)
                    {
                        if (DtEmployeeResult.Rows.Count > 0)
                        {
                            LblTitle.Text = "Result for " + DrpEmployees.SelectedItem.Text + " (" + DtEmployeeResult.Rows[0]["EmployeeCompanyId"].ToString() + ")";
                            TrEmp.Visible = true;
                            TrAll.Visible = false;
                            GridView1.DataSource = DtEmployeeResult;
                            GridView1.DataBind();
                        }
                        else
                        {
                            TrEmp.Visible = false;
                            TrAll.Visible = false;
                        }
                    }
                    else
                    {
                        TrEmp.Visible = false;
                        TrAll.Visible = false;
                    }
                }
            }
            catch (Exception )
            {

            }
        }

        protected void DrpEmployeesCompanyID_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (DrpEmployeesCompanyID.SelectedItem.Text != "--Select employeecompanyId--")
                {
                    DrpEmployees.SelectedIndex = 0;
                    EmployeeDAL ObjDAL = new EmployeeDAL();
                    DataTable DtEmployeeResult = ObjDAL.GetStdVsChetanaclusterScore(DrpEmployeesCompanyID.SelectedValue.ToString(), DrpCompany.SelectedValue.ToString());
                    if (DtEmployeeResult != null)
                    {
                        if (DtEmployeeResult.Rows.Count > 0)
                        {
                            LblTitle.Text = "Result for " + DtEmployeeResult.Rows[0]["Name"].ToString() + " (" + DtEmployeeResult.Rows[0]["EmployeeCompanyId"].ToString() + ")";
                            TrEmp.Visible = true;
                            TrAll.Visible = false;
                            GridView1.DataSource = DtEmployeeResult;
                            GridView1.DataBind();
                        }
                        else
                        {
                            TrEmp.Visible = false;
                            TrAll.Visible = false;
                        }
                    }
                    else
                    {
                        TrEmp.Visible = false;
                        TrAll.Visible = false;
                    }
                }
            }
            catch (Exception )
            {

            }
        }
    }
}