﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using DAL;
using BE;

namespace HealToZeal.Admin
{
    public partial class CompanyDepartmentCadre : System.Web.UI.Page
    {
        CompanyDepartmentBE objCompanyDeptBE = new CompanyDepartmentBE();
        CompanyDepartmentDAL objCompanyDeptDAL = new CompanyDepartmentDAL();
        CompanyDetailsDAL objCompanyDetailsDAL = new CompanyDetailsDAL();

        protected void Page_Load(object sender, EventArgs e)
        {
            DataTable DtCompanyDetails = objCompanyDetailsDAL.CompanyDetailsGet();
            ddlCompany.DataSource = objCompanyDetailsDAL.CompanyDetailsGet();
            ddlCompany.DataBind();

            if (!Page.IsPostBack)
            {
                if (Session["AdminId"] != null)
                {
                    BindCompanies();
                    BindDepartments();
                    BindCadre();
                    if (Request.QueryString["Id"] != null)
                    {
                        DataTable Dtdept = objCompanyDeptDAL.CompanyDeaprtmentGetByCompanyId(Request.QueryString["Id"].ToString());
                        if (Dtdept != null && Dtdept.Rows.Count > 0)
                        {
                            ddlCompany.SelectedValue = Dtdept.Rows[0][""].ToString();
                        }
                    }
                }
            }
        }

        private void BindCompanies()
        {
            try
            {
                CompanyDetailsDAL objCompanyDetailsDAL = new CompanyDetailsDAL();
                ddlCompany.DataSource = objCompanyDetailsDAL.CompanyDetailsGet();
                ddlCompany.DataTextField = "CompanyName";
                ddlCompany.DataValueField = "CompanyId_PK";
                ddlCompany.DataBind();
                ddlCompany.Items.Insert(0, new ListItem("--Select Company--", "--Select Company--"));
                ddlCompany.SelectedIndex = 0;
            }
            catch
            {

            }
        }

        private void BindDepartments()
        {
            try
            {
                CompanyDetailsDAL objCompanyDetailsDAL = new CompanyDetailsDAL();
                ddCompanyDepartments.DataSource = objCompanyDetailsDAL.GetAllDepartments();
                ddCompanyDepartments.DataTextField = "Departmentname";
                ddCompanyDepartments.DataValueField = "DeptId_PK";
                ddCompanyDepartments.DataBind();
                ddCompanyDepartments.Items.Insert(0, new ListItem("--Select Department--", "--Select Department--"));
                ddCompanyDepartments.SelectedIndex = 0;
            }
            catch
            {

            }
        }

        private void BindCadre()
        {
            try
            {
                CompanyDetailsDAL objCompanyDetailsDAL = new CompanyDetailsDAL();
                ddCadre.DataSource = objCompanyDetailsDAL.GetALlCadre();
                ddCadre.DataTextField = "Cadrename";
                ddCadre.DataValueField = "CadreId_PK";
                ddCadre.DataBind();
                ddCadre.Items.Insert(0, new ListItem("--Select Cadre--", "--Select Cadre--"));
                ddCadre.SelectedIndex = 0;
            }
            catch
            {

            }
        }
    }
}