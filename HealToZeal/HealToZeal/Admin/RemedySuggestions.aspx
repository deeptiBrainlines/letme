﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/Admin.Master" AutoEventWireup="true" CodeBehind="RemedySuggestions.aspx.cs" Inherits="HealToZeal.Admin.RemedySuggestions" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .auto-style1 {
            height: 26px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="LeftColumn" runat="server">
    <table>
        <tr>
            <td colspan="3">
                <asp:Label ID="lblTitle" runat="server" Text="Remedy suggestions"></asp:Label>
            </td>        
        </tr>
         <tr>
            <td>
                <asp:Label ID="lblAssessment" runat="server" Text="Assessment"></asp:Label>
            </td>
            <td>
                <asp:DropDownList ID="ddlAssessment" runat="server" ValidationGroup="0"></asp:DropDownList>
            </td>
            <td>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="ddlAssessment" Display="Dynamic" ErrorMessage="Select assessment" ForeColor="Red" InitialValue="--Select Assessment--" ValidationGroup="0">*</asp:RequiredFieldValidator>
             </td>
        </tr>
         <tr>
            <td>
                   <asp:Label ID="lblCluster" runat="server" Text="Cluster"></asp:Label>
            </td>
            <td>
                <asp:DropDownList ID="ddlCluster" runat="server" ValidationGroup="0"></asp:DropDownList>
            </td>
            <td>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ControlToValidate="ddlCluster" Display="Dynamic" ErrorMessage="Select cluster" ForeColor="Red" ValidationGroup="0">*</asp:RequiredFieldValidator>
             </td>
        </tr>
         <tr>
            <td>
                <asp:Label ID="lblColour" runat="server" Text="Colour"></asp:Label>
             </td>
            <td>
                <asp:DropDownList ID="ddlColour" runat="server" ValidationGroup="0">
                    <asp:ListItem Text="Green" Value="#88e46d"></asp:ListItem>
                    <asp:ListItem Text="Yellow" Value="#f4fb73"></asp:ListItem>
                    <asp:ListItem Text="Red" Value="#e43c37"></asp:ListItem>
                    <asp:ListItem Text="Amber1" Value="#FFD700"></asp:ListItem> <%--yellow shades--%>
                    <asp:ListItem Text="Amber2" Value="#FFA500"></asp:ListItem> <%--orange--%>
                </asp:DropDownList>
            </td>
            <td></td>
        </tr>
           <tr>
            <td>
                  <asp:Label ID="lblSuggestions" runat="server" Text="Suggestion"></asp:Label>
            </td>
            <td>
                <asp:TextBox ID="txtSuggestions" runat="server" TextMode="MultiLine" ValidationGroup="0"></asp:TextBox>
            </td>
            <td>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtSuggestions" Display="Dynamic" ErrorMessage="Please enter suggestion" ForeColor="Red" ValidationGroup="0">*</asp:RequiredFieldValidator>
               </td>
        </tr>
           <tr>
            <td class="auto-style1">
                 <asp:Label ID="lblMinMarks" runat="server" Text="Minimum marks"></asp:Label>
            </td>
            <td class="auto-style1">
                      <asp:TextBox ID="txtMinMarks" runat="server" ValidationGroup="0"></asp:TextBox>
            </td>
            <td class="auto-style1">
                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtMinMarks" Display="Dynamic" ErrorMessage="RequiredFieldValidator" ForeColor="Red" ValidationGroup="0">*</asp:RequiredFieldValidator>
               </td>
        </tr>
             <tr>
            <td>
                 <asp:Label ID="lblMaxMarks" runat="server" Text="Maximum marks"></asp:Label>
            </td>
            <td>
                 <asp:TextBox ID="txtMaxMarks" runat="server" ValidationGroup="0"></asp:TextBox>
            </td>
            <td>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="txtMaxMarks" Display="Dynamic" ErrorMessage="Enter maximum marks" ForeColor="Red" ValidationGroup="0">*</asp:RequiredFieldValidator>
                 </td>
        </tr>
           <tr>
            <td></td>
            <td>
                <asp:Button ID="btnSubmit" runat="server" Text="Submit" OnClick="btnSubmit_Click" ValidationGroup="0" />
            </td>
            <td></td>
        </tr>
        <tr>
            <td colspan="3">
                       <asp:Label ID="lblMessage" runat="server" Text=""></asp:Label>
            </td>           
        </tr>
    </table>
</asp:Content>
