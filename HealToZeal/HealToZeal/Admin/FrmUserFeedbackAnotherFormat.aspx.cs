﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using DAL;

namespace HealToZeal.Admin
{
    public partial class FrmUserFeedbackAnotherFormat : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                CompanyDetailsDAL ObjDAL = new CompanyDetailsDAL();
                DataTable Dtcomapny = ObjDAL.CompanyDetailsGet();

                if (Dtcomapny != null)
                {
                    if (Dtcomapny.Rows.Count > 0)
                    {
                        DrpCompany.DataSource = Dtcomapny;
                        DrpCompany.DataValueField = "CompanyId_PK";
                        DrpCompany.DataTextField = "CompanyName";
                        DrpCompany.DataBind();
                        DrpCompany.Items.Insert(0, new ListItem("--Select company--", "--Select company--"));
                    }
                }
            }
        }

        protected void DrpCompany_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (DrpCompany.SelectedItem.Text != "--Select company--")
                {
                    // EmployeeDAL ObjEmpDAL = new EmployeeDAL();
                    // DataTable DtEmployees = ObjEmpDAL.EmployeesGetByCompanyId(DrpCompany.SelectedValue.ToString(), 2);
                    UsersFeedbackDAL ObjDAL = new UsersFeedbackDAL();
                    DataTable Dt = ObjDAL.GetUserFeedback();
                    DataTable DtResultAll = Dt.Select("CompanyId_PK='" + DrpCompany.SelectedValue.ToString() + "'").CopyToDataTable();
                    // Bind Gridview Data

                    // DataTable DtResultAll = Dt.Select("CompanyId_PK='" + DrpCompany.SelectedValue.ToString() + "'").CopyToDataTable(); //view.ToTable(true, "CompanyId_PK", "CompanyName");

                    if (DtResultAll != null)
                    {
                        if (DtResultAll.Rows.Count > 0)
                        {
                           // DataView view = new DataView(DtResultAll);
                           // DataTable distinctValuesCmp = view.ToTable(true, "CompanyId_PK", "CompanyName");

                            GvFeedback.DataSource = DtResultAll;
                            GvFeedback.DataBind();

                        }
                    }
                }
            }
            catch (Exception )
            {

            }
        }
    }
}