﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DAL;
using BE;
using System.Data;

namespace HealToZeal.Admin
{
    public partial class ViewAssessment : System.Web.UI.Page
    {
        AssessmentDAL objAssessmentDAL = new AssessmentDAL();
       
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                BindAssessments();
            }
        }

        private void BindAssessments()
        {
            try
            {
                GvViewAssessment.DataSource = objAssessmentDAL.AssessmentGet();
                GvViewAssessment.DataBind();
            }
            catch
            {

            }
        }

        protected void GvViewAssessment_RowCommand(object source, GridViewCommandEventArgs e)
        {
            try
            {
                if (e.CommandName == "UpdateAssessment")
                {
                    Response.Redirect("Assessment.aspx?AssessmentId="+e.CommandArgument.ToString());
                }
            }
            catch
            {
               
            }          
        }
    }
}