﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DAL;
using BE;
using System.Data;
using System.Web.Mail;
namespace HealToZeal.Admin
{

    public partial class AssignAssessmentInstance : System.Web.UI.Page
    {
        CompanyDetailsDAL objCompanyDetailsDAL = new CompanyDetailsDAL();
        AssessmentDAL objAssessmentDAL = new AssessmentDAL();
        AssessmentBE objAssessmentBE = new AssessmentBE();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                if (Session["AdminId"] != null)
                {
                    BindInstances();
                    BindCompanies();

                    UpdateData();
                }
            }
        }

        private void UpdateData()
        {
            try
            {
                DataTable DtInstanceCompanies = new DataTable();
                if (Request.QueryString["InstanceId"] != null)
                {
                    GvCompanies.Visible = true;

                    pnlValidity.Visible = false;
                    rblInstances.SelectedValue = Request.QueryString["InstanceId"].ToString();
                    rblInstances.Enabled = false;

                    chklistCompanies.Visible = false;
                    DtInstanceCompanies = objAssessmentDAL.AssessmentInstanceCompanyRelationGet(Request.QueryString["InstanceId"].ToString());

                    if (DtInstanceCompanies != null && DtInstanceCompanies.Rows.Count > 0)
                    {
                        foreach (GridViewRow gvrow in GvCompanies.Rows)
                        {
                            Label lblCompanyId = (Label)gvrow.FindControl("lblCompanyId");
                            CheckBox chkcompany = (CheckBox)gvrow.FindControl("chkCompany");

                            if (DtInstanceCompanies.Select("CompanyId_FK='" + lblCompanyId.Text + "'").Length > 0)
                            {
                                chkcompany.Checked = true;
                                if (Request.QueryString["CompanyId"] != null)
                                {
                                    if (lblCompanyId.Text == Request.QueryString["CompanyId"].ToString())
                                    {
                                        chkcompany.Checked = true;
                                    }
                                    else
                                    {
                                        chkcompany.Checked = false;
                                    }
                                    chkcompany.Enabled = false;
                                }
                            }
                            if (Request.QueryString["CompanyId"] != null)
                            {
                                chkcompany.Enabled = false;
                            }
                        }
                        //check company
                        if (Request.QueryString["CompanyId"] != null)
                        {
                            pnlValidity.Visible = true;

                            DataTable dtc = DtInstanceCompanies.Select("CompanyId_FK='" + Request.QueryString["CompanyId"].ToString() + "'").CopyToDataTable();
                            txtValidFrom.Text = dtc.Rows[0]["ValidFrom"].ToString();
                            txtValidTo.Text = dtc.Rows[0]["ValidTo"].ToString();
                            if ((Convert.ToDateTime(txtValidFrom.Text) > DateTime.Now.Date) && (Convert.ToDateTime(txtValidTo.Text) > DateTime.Now.Date))
                            {
                                txtValidFrom.Enabled = true;
                                txtValidTo.Enabled = true;
                            }
                            else if ((Convert.ToDateTime(txtValidFrom.Text) <= DateTime.Now.Date) && (Convert.ToDateTime(txtValidTo.Text) > DateTime.Now.Date))
                            {
                                txtValidFrom.Enabled = false;
                                txtValidTo.Enabled = true;
                            }
                            else
                            {
                                txtValidFrom.Enabled = false;
                                txtValidTo.Enabled = false;
                            }
                        }
                    }
                }
            }
            catch
            {

            }
        }

        private void BindInstances()
        {
            try
            {
                rblInstances.DataSource = objAssessmentDAL.AssessmentInstanceMasterGet();
                rblInstances.DataTextField = "AssessmentInstanceName";
                rblInstances.DataValueField = "AssessmentInstanceId_PK";
                rblInstances.DataBind();


            }
            catch (Exception)
            {

            }
        }

        private void BindCompanies()
        {
            try
            {
                chklistCompanies.DataSource = objCompanyDetailsDAL.CompanyDetailsGet();
                chklistCompanies.DataTextField = "CompanyName";
                chklistCompanies.DataValueField = "CompanyId_PK";
                chklistCompanies.DataBind();

                GvCompanies.DataSource = objCompanyDetailsDAL.CompanyDetailsGet();
                GvCompanies.DataBind();
                GvCompanies.Visible = false;
            }
            catch (Exception)
            {

            }
        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            try
            {
                int Status = 0;

                objAssessmentBE.propAssessmentInstanceId_FK = rblInstances.SelectedItem.Value;
                objAssessmentBE.propCreatedBy = Session["AdminId"].ToString();
                objAssessmentBE.propCreatedDate = DateTime.Now.Date;


                DataTable dtCompanies = new DataTable();
                dtCompanies.Columns.Add(new System.Data.DataColumn("CompanyId_FK", typeof(string)));

                if (Request.QueryString["CompanyId"] != null)
                {
                    objAssessmentBE.propValidFrom = Convert.ToDateTime(txtValidFrom.Text);
                    objAssessmentBE.propValidTo = Convert.ToDateTime(txtValidTo.Text);
                    objAssessmentBE.propCompanyId_FK = Request.QueryString["CompanyId"].ToString();
                    Status = objAssessmentDAL.AssessmentInstanceCompanyByIdUpdate(objAssessmentBE);
                    if (Status > 0)
                    {
                        lblMessage.Text = "Assessment instance company updated successfully...";
                        ClearFields();
                    }
                }
                else if (Request.QueryString["InstanceId"] != null)
                {
                    foreach (GridViewRow gvrow in GvCompanies.Rows)
                    {
                        Label lblCompanyId = (Label)gvrow.FindControl("lblCompanyId");
                        CheckBox chkCompany = (CheckBox)gvrow.FindControl("chkCompany");
                        if (chkCompany.Checked)
                        {
                            dtCompanies.Rows.Add(lblCompanyId.Text);
                        }
                    }
                    if (dtCompanies != null && dtCompanies.Rows.Count > 0)
                    {
                        objAssessmentBE.propCompanyIds = dtCompanies;
                        Status = objAssessmentDAL.AssessmentInstanceCompanyRelationUpdate(objAssessmentBE);
                        if (Status > 0)
                        {
                            lblMessage.Text = "Assessment instance updated successfully...";
                            ClearFields();
                        }
                    }
                    else
                    {
                        lblMessage.Text = "Select at least one company...";
                    }
                }
                else
                {
                    foreach (ListItem chkCompany in chklistCompanies.Items)
                    {
                        if (chkCompany.Selected)
                        {
                            dtCompanies.Rows.Add(chkCompany.Value);
                        }
                    }
                    objAssessmentBE.propValidFrom = Convert.ToDateTime(txtValidFrom.Text);
                    objAssessmentBE.propValidTo = Convert.ToDateTime(txtValidTo.Text);
                    objAssessmentBE.propCompanyIds = dtCompanies;
                    if (dtCompanies != null && dtCompanies.Rows.Count > 0)
                    {
                        Status = objAssessmentDAL.SpAssessmentInstanceCompanyRelationInsert(objAssessmentBE);
                        if (Status > 0)
                        {
                            lblMessage.Text = "Assessment instance assigned successfully...";
                            //Send mail to employees
                            SendMailToEployees();
                            ClearFields();
                        }
                    }
                    else
                    {
                        lblMessage.Text = "Select at least one company...";
                    }
                }
            }
            catch
            {

            }
        }

        private void SendMailToEployees()
        {
            EmployeeDAL objEmployeeDAL = new EmployeeDAL();
            DataTable dtEmployees;
               System.Web.Mail.MailMessage mail;
            foreach (ListItem chkCompany in chklistCompanies.Items)
            {
                if (chkCompany.Selected)
                {
                    dtEmployees = objEmployeeDAL.EmployeesGetByCompanyId(chkCompany.Value, 0);
                    if (dtEmployees != null)
                    {
                        if (dtEmployees.Rows.Count > 0)
                        {
                            for (int cnt = 0; cnt < dtEmployees.Rows.Count; cnt++)
                            {
                                mail = new System.Web.Mail.MailMessage();
                                mail.To = dtEmployees.Rows[cnt]["EmailId"].ToString();
                                mail.From = "Let Me Talk2 MeToZeal@chetanatspl.com";
                                mail.Subject = "New Instance assigned.";

                                mail.Body = "<html><body><font color=\"black\">Dear " + dtEmployees.Rows[cnt]["EmployeeName"].ToString() + ",<br/><br/>Welcome to <b> Let Me Talk2 Me to Zeal: A Let Me Talk2 Meth and Personality Insights and Actions Tool.</b> Your organization will be able to gain some useful insights into your team's mental Let Me Talk2 Meth well being and learn about remedial actions in case they are needed. <b>'Let Me Talk2 Me to Zeal'</b> will help your employees in self-discovery. It will lead on a path of self development while avoiding pitfalls of modern complex life and careers.<br/><br/>It is recommended that one uses this tool periodically to monitor self and be aware of one's Let Me Talk2 Meth and personality parameters. <br/><br/>Please use the link below to login  <br/><br/> <a href='http://chetanatspl.com/Let Me Talk2 MeToZeal1/Employee/EmployeeLogin.aspx' >http://chetanatspl.com/Let Me Talk2 MeToZeal1/Employee/EmployeeLogin.aspx</a><BR/> <BR/> Thank You.<br/><br/></font></body></html>";
                                mail.BodyFormat = MailFormat.Html;
                                mail.Fields.Add("http://schemas.microsoft.com/cdo/configuration/smtpauthenticate", "1");    //basic authentication
                                mail.Fields.Add("http://schemas.microsoft.com/cdo/configuration/sendusername", "Let Me Talk2 MeToZeal@chetanatspl.com"); //set your username here
                                mail.Fields.Add("http://schemas.microsoft.com/cdo/configuration/sendpassword", "Let Me Talk2 MeToZeal");    //set your password here

                                System.Web.Mail.SmtpMail.SmtpServer = "holly.arvixe.com";  //your real server goes here                        
                               // System.Web.Mail.SmtpMail.Send(mail);                                
                            }
                        }
                    }
                }
            }
        
        }

        private void ClearFields()
        {
            try
            {
                chklistCompanies.ClearSelection();
                rblInstances.ClearSelection();
                txtValidFrom.Text = "";
                txtValidTo.Text = "";
            }
            catch
            {

            }
        }

        protected void rblInstances_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                //show already assigned companies
                AssessmentDAL objAssessmentDAL = new AssessmentDAL();
                DataTable DtCompanies = objAssessmentDAL.AssessmentInstanceCompanyRelationGet(rblInstances.SelectedValue);
                if (DtCompanies != null && DtCompanies.Rows.Count > 0)
                {
                    foreach (ListItem chkcompany in chklistCompanies.Items)
                    {

                        if (DtCompanies.Select("CompanyId_FK='" + chkcompany.Value + "'").Length > 0)
                        {
                            chkcompany.Selected = true;
                        }
                        else
                        {
                            chkcompany.Selected = false;
                        }
                    }
                }
                else
                {
                    foreach (ListItem chkcompany in chklistCompanies.Items)
                    {
                        chkcompany.Selected = false;
                    }
                }
            }
            catch
            {

            }
        }
    }
}