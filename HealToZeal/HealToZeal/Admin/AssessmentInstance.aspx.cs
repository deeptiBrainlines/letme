﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DAL;
using BE;
using System.Data;

namespace HealToZeal.Admin
{
    public partial class AssessmentInstance : System.Web.UI.Page
    {
       AssessmentDAL objAssessmentDAL = new AssessmentDAL();       

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                if (Session["AdminId"] != null)
                {
                    BindAssessments();   
                    UpdateData();
                }
            }
        }

        private void UpdateData()
        {
            try
            {
                DataTable DtInstanceCompanies = new DataTable();
                if (Request.QueryString["Id"] != null)
                {
                  
                    DataTable dtAssessmentInstance = objAssessmentDAL.AssessmentInstanceGetByInstanceId(Request.QueryString["Id"].ToString());
                    if (dtAssessmentInstance != null && dtAssessmentInstance.Rows.Count > 0)
                    {
                        txtInstance.Text = dtAssessmentInstance.Rows[0]["AssessmentInstanceName"].ToString();
                    }
                    if (dtAssessmentInstance != null && dtAssessmentInstance.Rows.Count > 0)
                    {
                        foreach (GridViewRow gvrow in GvAssessments.Rows)
                        {
                            Label lblAssessmentId = (Label)gvrow.FindControl("lblAssessmentId");
                            CheckBox chkAssessments = (CheckBox)gvrow.FindControl("chkAssessments");
                            CheckBox chkDisplayResult = (CheckBox)gvrow.FindControl("chkDisplayResult");
                            CheckBox chkDisplayResultForCompany = (CheckBox)gvrow.FindControl("chkDisplayResultForCompany");
                            if ((dtAssessmentInstance.Select("AssessmentId_FK='" + lblAssessmentId.Text + "'").Length > 0))
                            {
                                chkAssessments.Checked = true;
                            }
                            else
                            {
                                chkAssessments.Checked = false;
                            }
                            if ((dtAssessmentInstance.Select("AssessmentId_FK='" + lblAssessmentId.Text + "' and DisplayResult='Y'").Length > 0))
                            {
                                chkDisplayResult.Checked = true;
                            }
                            else
                            {
                                chkDisplayResult.Checked = false;
                            }
                            if ((dtAssessmentInstance.Select("AssessmentId_FK='" + lblAssessmentId.Text + "' and DisplayResultForCompany='Y'").Length > 0))
                            {
                                chkDisplayResultForCompany.Checked = true;
                            }
                            else
                            {
                                chkDisplayResultForCompany.Checked = false;
                            }
                          
                        }
                       
                    }
                }
            }
            catch
            {

            }
        }

        private void BindAssessments()
        {
            try
            {
                DataTable DtAssessments = new DataTable();
                DtAssessments = (objAssessmentDAL.AssessmentGet()).Select("IsComplete='Y'").CopyToDataTable();
               

                GvAssessments.DataSource = DtAssessments;
                GvAssessments.DataBind();
            }
            catch
            {

            }
        }


     
        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            try
            {
                DataTable dtDuplicate = objAssessmentDAL.AssessmentInstanceMasterCheckDuplicate(txtInstance.Text);

                lblmessage.Text = "";
                AssessmentBE objAssessmentBE = new AssessmentBE();
                objAssessmentBE.propAssessmentInstanceName = txtInstance.Text;
                DataTable dtAssessments = new DataTable();
                dtAssessments.Columns.Add(new System.Data.DataColumn("AssessmentId_FK", typeof(string)));
                dtAssessments.Columns.Add(new System.Data.DataColumn("DisplayResult", typeof(string)));
                dtAssessments.Columns.Add(new System.Data.DataColumn("DisplayResultForCompany", typeof(string)));
                //foreach (ListItem chkCompany in chklistAssessments.Items)
                //{
                //    if (chkCompany.Selected)
                //    {
                //        dtAssessments.Rows.Add(chkCompany.Value);
                //    }
                //}
                foreach (GridViewRow gvrow in GvAssessments.Rows)
                {
                    Label lblAssessmentId = (Label)gvrow.FindControl("lblAssessmentId");
                    CheckBox chkAssessments = (CheckBox)gvrow.FindControl("chkAssessments");
                    CheckBox chkDisplayResult = (CheckBox)gvrow.FindControl("chkDisplayResult");
                    CheckBox chkDisplayResultForCompany = (CheckBox)gvrow.FindControl("chkDisplayResultForCompany");
                    if (chkAssessments.Checked && chkDisplayResult.Checked && chkDisplayResultForCompany.Checked)
                    {
                        dtAssessments.Rows.Add(lblAssessmentId.Text, "Y","Y");
                    }
                    else if (chkAssessments.Checked && chkDisplayResultForCompany.Checked)
                    {
                        dtAssessments.Rows.Add(lblAssessmentId.Text, "N","Y");
                    }
                    else if (chkAssessments.Checked && chkDisplayResult.Checked)
                    {
                        dtAssessments.Rows.Add(lblAssessmentId.Text, "Y", "N");
                    }
                    else if (chkAssessments.Checked)
                    {
                        dtAssessments.Rows.Add(lblAssessmentId.Text, "N", "N");
                    }

                }
                objAssessmentBE.propAssessmentIds = dtAssessments;
                objAssessmentBE.propCreatedBy = Session["AdminId"].ToString();
                objAssessmentBE.propCreatedDate = DateTime.Now.Date;
                int status = 0;
                if (Request.QueryString["Id"] != null)
                {
                    objAssessmentBE.propAssessmentInstanceId_FK = Request.QueryString["Id"].ToString();
                    status = objAssessmentDAL.AssessmentInstanceAssessmentUpdate(objAssessmentBE);
                    if (status > 0)
                    {
                        lblmessage.Text = "Assessment instance updated successfully...";
                        ClearFields();
                    }
                }
                else
                {
                    if (dtDuplicate != null && dtDuplicate.Rows.Count > 0)
                    {
                        lblmessage.Text = "Instance name already exists...";
                    }
                    else
                    {
                        status = objAssessmentDAL.AssessmentInstanceAssessmentInsert(objAssessmentBE);
                        if (status > 0)
                        {
                            lblmessage.Text = "Assessment instance added successfully...";
                            ClearFields();
                        }
                    }
                }
            }
            catch
            {

            }
        }

        private void ClearFields()
        {
            try
            {
                txtInstance.Text = "";
            }
            catch
            {

            }
        }

        
    }
}