﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using DAL;

namespace HealToZeal.Admin
{
    public partial class ViewCompanyCadre : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            { 
                  // CompanyDetailsDAL objCompanyDetailsDAL = new CompanyDetailsDAL();
                  
                   BindCompanies();

            }
        }

        private void BindCompanies()
        {
            try
            {
                CompanyDetailsDAL objCompanyDetailsDAL = new CompanyDetailsDAL();
                ddlCompany.DataSource = objCompanyDetailsDAL.CompanyDetailsGet();
                ddlCompany.DataTextField = "CompanyName";
                ddlCompany.DataValueField = "CompanyId_PK";
                ddlCompany.DataBind();
                ddlCompany.Items.Insert(0, new ListItem("--Select Company--", "--Select Company--"));
                ddlCompany.SelectedIndex = 0;
            }
            catch
            {

            }
        }

        protected void DlistCompany_ItemDataBound(object sender, DataListItemEventArgs e)
        {
            try
            {
                CompanyCadreDAL objCompanyCadreDAL = new CompanyCadreDAL();
                DataList DlistCadre = (DataList)e.Item.FindControl("DlistCadre");
                Label lblCompanyId = (Label)e.Item.FindControl("lblCompanyId");

                DlistCadre.DataSource = objCompanyCadreDAL.GetAllCadrevalue(ddlCompany.SelectedValue.ToString());
                DlistCadre.DataBind();

            }
            catch
            {

            }
            
        }


        protected void ddlCompany_SelectedIndexChanged1(object sender, EventArgs e)
        {
            CompanyCadreDAL objCompanyCadreDAL = new CompanyCadreDAL();
            DlistCompany.DataSource = objCompanyCadreDAL.GetAllCadrevalue(ddlCompany.SelectedValue.ToString());

            DlistCompany.DataBind();
        }

        protected void DlistCompany_ItemCommand(object source, DataListCommandEventArgs e)
        {
            Label lblCadreId = (Label)e.Item.FindControl("lblCadreId");
            if (e.CommandName == "UpdateCadre")
            {
                Response.Redirect("CompanyCadre.aspx?Id=" + e.CommandArgument.ToString(), true);
            }
            else
            {
                CompanyCadreDAL objCompanyCadreDAL = new CompanyCadreDAL();
                DataTable dt = objCompanyCadreDAL.DeleteCadre(lblCadreId.Text);
               
                DlistCompany.DataSource = objCompanyCadreDAL.GetAllCadrevalue(ddlCompany.SelectedValue.ToString());

                DlistCompany.DataBind();
            }
        }

  
    }
}