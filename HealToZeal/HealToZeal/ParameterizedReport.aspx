﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ParameterizedReport.aspx.cs" Inherits="HealToZeal.ParameterizedReport" %>

<%@ Register assembly="Microsoft.ReportViewer.WebForms, Version=11.0.0.0, Culture=neutral, PublicKeyToken=89845dcd8080cc91" namespace="Microsoft.Reporting.WebForms" tagprefix="rsweb" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
    <table>
        <tr>
            <td>
                <asp:Label ID="LblTitle" runat="server" Text="Enter ID" ></asp:Label>
            </td>
            <td>
                <asp:TextBox ID="TxtID" runat="server" Width="179px" ></asp:TextBox>
            </td>
            <td>
                <asp:Button ID="BtnSubmit" runat="server" Text="Submit" OnClick="BtnSubmit_Click" />
            </td>
        </tr>
        <tr>
            <td colspan="3">
                <rsweb:ReportViewer ID="ReportViewer1" runat="server" Font-Names="Verdana" Font-Size="8pt" WaitMessageFont-Names="Verdana" WaitMessageFont-Size="14pt" Width="601px">
                    <LocalReport ReportPath="ParameterizedRDLC.rdlc">
                        <DataSources>
                            <rsweb:ReportDataSource DataSourceId="ObjectDataSource2" Name="DataSet1" />
                        </DataSources>
                    </LocalReport>
                </rsweb:ReportViewer>
                <asp:ObjectDataSource ID="ObjectDataSource2" runat="server" OldValuesParameterFormatString="original_{0}" SelectMethod="GetData" TypeName="HealToZeal.DsParameterizedTableAdapters.EmployeesTableAdapter">
                    <SelectParameters>
                        <asp:ControlParameter ControlID="TxtID" DefaultValue="2" Name="Name" PropertyName="Text" Type="Int32" />
                    </SelectParameters>
                </asp:ObjectDataSource>
            </td>
        </tr>
    </table>
    </div>
        <asp:ScriptManager ID="ScriptManager1" runat="server">
        </asp:ScriptManager>
        <br />
        <asp:Chart ID="Chart1" runat="server" DataSourceID="ObjectDataSource1">
            <Series>
                <asp:Series ChartType="Pie" Name="Series1" ToolTip="#VAL" XValueMember="Percentage" YValueMembers="Percentage">
                </asp:Series>
            </Series>
            <ChartAreas>
                <asp:ChartArea Name="ChartArea1">
                </asp:ChartArea>
            </ChartAreas>
        </asp:Chart>
        <asp:ObjectDataSource ID="ObjectDataSource1" runat="server" OldValuesParameterFormatString="original_{0}" SelectMethod="GetData" TypeName="HealToZeal.AllClustersTableAdapters.SpClusterPercentageTableAdapter"></asp:ObjectDataSource>
    </form>
</body>
</html>
