﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ReportFormula.aspx.cs" Inherits="HealToZeal.Company.ReportFormula" %>

<%@ Register assembly="Microsoft.ReportViewer.WebForms, Version=11.0.0.0, Culture=neutral, PublicKeyToken=89845dcd8080cc91" namespace="Microsoft.Reporting.WebForms" tagprefix="rsweb" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
    <%--    <asp:DropDownList ID="DrpLstCluster" runat="server" AutoPostBack="true" OnSelectedIndexChanged="DrpLstCluster_SelectedIndexChanged" >
            <asp:ListItem>Anxiety</asp:ListItem>
<asp:ListItem >Stress</asp:ListItem>
        </asp:DropDownList>--%>
        <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
        <rsweb:ReportViewer ID="ReportViewer1" runat="server" Font-Names="Verdana" Font-Size="8pt" WaitMessageFont-Names="Verdana" WaitMessageFont-Size="14pt" Width="943px">
            <LocalReport ReportPath="Reports\Cluster1.rdlc">
                <DataSources>
                    <rsweb:ReportDataSource DataSourceId="ObjectDataSource2" Name="DataSet1" />
                </DataSources>
            </LocalReport>

        </rsweb:ReportViewer>
    
        <asp:ObjectDataSource ID="ObjectDataSource2" runat="server" OldValuesParameterFormatString="original_{0}" SelectMethod="GetData" TypeName="HealToZeal.DataSet3TableAdapters.SpAnxietyAssessmentResultTableAdapter"></asp:ObjectDataSource>
        <br />
        <asp:ObjectDataSource ID="ObjectDataSource1" runat="server" OldValuesParameterFormatString="original_{0}" SelectMethod="GetData" TypeName="HealToZeal.DataSet3TableAdapters.SpAnxietyAssessmentResultTableAdapter"></asp:ObjectDataSource>
    
    </div>
    </form>
</body>
</html>
