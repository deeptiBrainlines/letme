﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using BE;
using DAL;
using System.Net.Mail;
using System.Net.Mime;

namespace HealToZeal.Company
{
    public partial class AddEmployee1 : System.Web.UI.Page
    {

        string CompanyId = "";
        EmployeeDAL objEmployeeDAL = new EmployeeDAL();
        string EmployeeId = "";
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                if (Session["CompanyId"] != null)
                {
                     BindCompanyEmployees(0);
                    BindCadre();
                }
            }
        }


        private void BindCadre()
        {
            try
            {

                CompanyId = Session["CompanyId"].ToString();
                DataTable DtCadre = objEmployeeDAL.CompanyCadreGetByCompanyId(CompanyId);

                ddlCadre.DataSource = DtCadre;
                ddlCadre.DataTextField = "Cadrename";
                ddlCadre.DataValueField = "CadreId_PK";
                ddlCadre.DataBind();

                ddlCadre.Items.Insert(0, new ListItem("--SelectCadre--", "--Select Cadre--"));
                ddlCadre.SelectedIndex = 0;
            }
            catch
            {

            }
        }

        private void BindCompanyEmployees(int Flag)
        {
            try
            {
                CompanyId = Session["CompanyId"].ToString();
                DataTable DtEmployees = objEmployeeDAL.EmployeesGetByCompanyId(CompanyId, Flag);
                if (DtEmployees != null && DtEmployees.Rows.Count > 0)
                {
                    GvViewEmployee.DataSource = DtEmployees;
                    GvViewEmployee.DataBind();
                }
            }
            catch
            {

            }
        }

        protected void btnAddEmployee_Click(object sender, EventArgs e)
        {
            try
            {
                InsertUpdateEmployee();
            }
            catch
            {

            }
        }

        private void InsertUpdateEmployee()
        {
            try
            {
                //insert
                string password = Encryption.CreatePassword();

                EmployeeBE objEmployeeBE = new EmployeeBE();
                string userName = Encryption.CreateUsername();//txtEmployeeCompanyId.Text + "" + txtFirstName.Text;
                CompanyId = Session["CompanyId"].ToString();
                objEmployeeBE.propCompanyId_FK = CompanyId;
                objEmployeeBE.propEmployeeCompanyId = Convert.ToInt32(EmployeeCompanyId.Value);
                objEmployeeBE.propUserName = userName;
                objEmployeeBE.propPassword = Encryption.Encrypt(password);
                objEmployeeBE.propFirstName = FirstName.Value;
                objEmployeeBE.propLastName = LastName.Value;
                objEmployeeBE.propEmailId = Encryption.Encrypt(EmailId.Value);
                objEmployeeBE.propCadreId_FK = ddlCadre.SelectedItem.Value;
                objEmployeeBE.propCreatedBy = CompanyId;
                objEmployeeBE.propCreatedDate = DateTime.Now.Date;
                string message = string.Empty;
                int Status = 0;
                if (Session["EmployeeId"] != null)
                {
                    EmployeeId = Session["EmployeeId"].ToString();

                    Status = objEmployeeDAL.EmployeesUpdateByCompanyId(objEmployeeBE);
                }
                else
                {
                    Status = objEmployeeDAL.EmployeesInsert(objEmployeeBE);
                    if (Status > 0)
                    {

                        // email

                        MailMessage insMail = new MailMessage();

                        System.Net.Mail.MailAddress insFrom = new MailAddress("info@chetanasystems.com", "Chetana TSPL");
                        insMail.From = insFrom;

                        insMail.To.Add(EmailId.Value);

                        insMail.Subject = "You have been invited to participate in the product validation exercise .";

                        insMail.IsBodyHtml = true;
                        //change url
                        //string strBody = "<html><body><font color=\"black\">Hello " + txtFirstName.Text + " " + txtLastName.Text + ",<BR>Your UserId is " + userName + " and password is " + password + " . <BR>Login to this link <BR> <a href='http://chetanasystems.com/ChetanaHealToZeal/Employee/EmployeeLogin.aspx?first=1' >http://chetanasystems.com/ChetanaHealToZeal/Employee/EmployeeLogin.aspx</a><BR> <BR>Regards<BR>Let Me Talk2 Me.</font></body></html>";
                        string strBody = "<html><body><font color=\"black\">Dear " + FirstName.Value + " " + LastName.Value + ",<br/><br/>Thank you for your willingness to participate. Your login details are as follows: <br/><br/> UserId:  <b>" + userName + "</b> <br/><br/>  Password: <b> " + password + "</b> <br/>If you would like to take a break during assessment, please make sure you log out from the system or close the browser window. Please do not leave the browser idle. You can always log back in whenever you like.<br/>Please use the link below to login <br/><br/> <a href='http://chetanasystems.com/ChetanaHealToZeal/Employee/EmployeeLogin.aspx?first=1' >http://chetanasystems.com/ChetanaHealToZeal/Employee/EmployeeLogin.aspx</a><BR/> <BR/>Thank You.<BR/><br/> Best Regards, <br/><br/> HealToZealTeam.</font></body></html>";

                        insMail.Body = strBody;


                        System.Net.Mail.SmtpClient ns = new SmtpClient("relay-hosting.secureserver.net"); //"localhost";//"relay-hosting.secureserver.net";
                        // ns.Send(insMail);
                        ClearEmployeeDetails();
                        message = "Employee added successfully...";
                    }
                    else
                    {
                        message = "Employee can not be added...";
                    }
                }
                // lblMessage.Text = message;
            }
            catch
            {

            }
        }

        private void ClearEmployeeDetails()
        {
            EmailId.Value = "";
            EmployeeCompanyId.Value = "";
            FirstName.Value = "";
            LastName.Value = "";
            ddlCadre.SelectedIndex = 0;
        }

    

        //string CompanyId = "";
        //EmployeeDAL objEmployeeDAL = new EmployeeDAL();
        //string EmployeeId = "";

        //protected void Page_Load(object sender, EventArgs e)
        //{
        //    if (!Page.IsPostBack)
        //    {
        //        Session["CompanyId"] = "54c81b6c-d4f9-4ab3-84b6-ba866a5ef90a";
               
        //        if (Session["CompanyId"] != null)
        //        {
        //            BindCompanyEmployees();
        //            BindCadre();
        //        }
        //    }
        //}

        //private void BindCadre()
        //{
        //    try
        //    {

        //        CompanyId = Session["CompanyId"].ToString();
        //        DataTable DtCadre = objEmployeeDAL.CompanyCadreGetByCompanyId(CompanyId);

        //        ddlCadre.DataSource = DtCadre;
        //        ddlCadre.DataTextField = "Cadrename";
        //        ddlCadre.DataValueField = "CadreId_PK";
        //        ddlCadre.DataBind();

        //        ddlCadre.Items.Insert(0, new ListItem("--Select Cadre--", "--Select Cadre--"));
        //        ddlCadre.SelectedIndex = 0;
        //    }
        //    catch
        //    {

        //    }
        //}

        //private void BindCompanyEmployees()
        //{
        //    try
        //    {
        //        CompanyId = Session["CompanyId"].ToString();
        //        //DataTable DtEmployees = objEmployeeDAL.EmployeesGetByCompanyId(CompanyId);
        //        //if (DtEmployees != null && DtEmployees.Rows.Count > 0)
        //        //{
        //        //    GvViewEmployee.DataSource = DtEmployees;
        //        //    GvViewEmployee.DataBind();
        //        //}
        //    }
        //    catch
        //    {

        //    }
        //}

        //protected void btnAddEmployee_Click(object sender, EventArgs e)
        //{
        //    try
        //    {
        //        InsertUpdateEmployee();
        //    }
        //    catch
        //    {

        //    }
        //}

        //private void InsertUpdateEmployee()
        //{
        //    try
        //    {
        //        //insert
        //        string password = Encryption.CreatePassword();
        //        EmployeeBE objEmployeeBE = new EmployeeBE();
        //        string userName = EmployeeCompanyId.Value + "" + FirstName.Value;
        //        CompanyId =Session["CompanyId"].ToString();
        //        objEmployeeBE.propCompanyId_FK = CompanyId;
        //        objEmployeeBE.propEmployeeCompanyId = Convert.ToInt32(EmployeeCompanyId.Value);
        //        objEmployeeBE.propUserName = userName;
        //        objEmployeeBE.propPassword = Encryption.Encrypt(password);
        //        objEmployeeBE.propFirstName = FirstName.Value;
        //        objEmployeeBE.propLastName = LastName.Value;
        //        objEmployeeBE.propEmailId = EmailId.Value;
        //        objEmployeeBE.propCadreId_FK = ddlCadre.SelectedItem.Value;
        //        objEmployeeBE.propCreatedBy = CompanyId;
        //        objEmployeeBE.propCreatedDate = DateTime.Now.Date;

        //        string message = "";
        //        int Status = 0;

        //        if (Session["EmployeeId"] != null)
        //        {
        //            EmployeeId = Session["EmployeeId"].ToString();
        //            Status= objEmployeeDAL.EmployeesUpdateByCompanyId(objEmployeeBE);
        //        }
        //        else
        //        {
        //            Status = objEmployeeDAL.EmployeesInsert(objEmployeeBE);
        //            if (Status > 0)
        //            {
        //                message = "Employee added successfully...";
        //                // email

        //                System.Net.Mail.MailMessage insMail = new System.Net.Mail.MailMessage();

        //                System.Net.Mail.MailAddress insFrom = new MailAddress("info@chetanasystems.com");
        //                insMail.From = insFrom;

        //                insMail.To.Add(EmailId.Value);

        //                insMail.Subject = "You have been invited to participate in the product validation exercise .";

        //                insMail.IsBodyHtml = true;
        //                //change urlegardsre
        //                string strBody = "<html><body><font color=\"black\">Dear " + FirstName.Value + " " + LastName.Value + ",<br/><br/>Thank you for your willingness to participate. Your login details are as follows: <br/><br/> UserId:  <b>" + userName + "</b> <br/><br/>  Password: <b> " + password + "</b> . <br/><br/>Please use the link below to login and register <br/><br/> <a href='http://chetanasystems.com/ChetanaHealToZeal/Employee/EmployeeLogin.aspx?first=1' >http://chetanasystems.com/ChetanaHealToZeal/Employee/EmployeeLogin.aspx</a><BR/> <BR/>Thank You.<BR/><br/> Best Regards, <br/><br/> HealToZealTeam.</font></body></html>";
        //                insMail.Body = strBody;

        //                System.Net.Mail.SmtpClient ns = new SmtpClient("relay-hosting.secureserver.net"); //"localhost";//"relay-hosting.secureserver.net";
        //                ns.Send(insMail);
        //                ClearEmployeeDetails();
        //            }
        //            else
        //            {
        //                message = "Employee can not be added...";
        //            }
        //        }
             
        //       //lblMessage.Text = message;
        //    }
        //    catch
        //    {

        //    }
        //}

        //private void ClearEmployeeDetails()
        //{
        //    EmailId.Value = "";
        //    EmployeeCompanyId.Value = "";
        //    FirstName.Value = "";
        //    LastName.Value = "";
        //    ddlCadre.SelectedIndex = 0;
        //}

        //protected void GvViewEmployee_RowCommand(object sender, GridViewCommandEventArgs e)
        //{
        //    try
        //    {
        //        EmployeeId  = e.CommandArgument.ToString();
        //        //add session
        //        Session["EmployeeId"] = EmployeeId ;
        //        if (e.CommandName == "EditEmployee")
        //        {
        //            BindQuestion();
        //        }
              
        //    }
        //    catch 
        //    {
                
        //    }
        //}
    }
}