﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using DAL;
using System.Drawing;
namespace HealToZeal.Company
{
    public partial class UserResult : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if(!Page.IsPostBack)
            {
            
            }
        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        { 
            EmployeeDAL objEmployeeDAL = new EmployeeDAL();

            DataTable DtEmployeeDetails = objEmployeeDAL.EmployeeAssessmentSubmittedGetByEmployeeId(Session["EmployeeId_FK"].ToString());
            RptrEmployee.DataSource= DtEmployeeDetails;
            RptrEmployee.DataBind();

          //DataTable Dt=  objEmployeeDAL.EmployeeAssessmentClusterwiseReport(Convert.ToInt32(txtEmployeeId.Text));

          //GvStandard.DataSource = Dt;
          //GvStandard.DataBind();

          //GvEmployeeResult.DataSource = Dt;
          //GvEmployeeResult.DataBind();

          //DataTable DtResult = new DataTable();//Dt;
          //DtResult = Dt.Select(" Result<>'' or Suggestion<>''","Result Desc").CopyToDataTable();
          //GvResult.DataSource = DtResult;
          //GvResult.DataBind();

          //GvResult.DataSource = Dt;
          //GvResult.DataBind();

          //DataTable DtRemedyObservation = new DataTable();//Dt;
          //DtRemedyObservation = Dt.Select(" Suggestion<>''").CopyToDataTable();
          //GvRemedyObservation.DataSource = DtRemedyObservation;
          //GvRemedyObservation.DataBind();



        //  DataTable DtStndard = new DataTable();//Dt;
        ////  DtStndard = Dt.Select("( ClusterName='Stress' and AssessmentName='Standard stress Assessment') OR( AssessmentName='Standard Mood Assessment' AND  ClusterName='Mood')").CopyToDataTable();
        //  DtStndard = Dt.Select("ClusterName<>'Anxiety' AND ClusterName<>'Mood' AND ClusterName<>'Stress'").CopyToDataTable();
        //  DataTable DtSubStndard = new DataTable();
        //  DtSubStndard = Dt.Select("( ClusterName='Stress' and AssessmentName='Standard stress Assessment') OR( AssessmentName='Standard Mood Assessment' AND  ClusterName='Mood')OR( AssessmentName='Standard Anxiety Assessment' AND  ClusterName='Anxiety')").CopyToDataTable();
        //   DtSubStndard.Merge(DtStndard);

        //  GvStandard.DataSource = DtSubStndard;
        //  GvStandard.DataBind();

        //  DataTable DtRemedyObservation = objEmployeeDAL.AssessmentFinalResult(Convert.ToInt32(txtEmployeeId.Text));
        //  DataTable DtRemedy = new DataTable();//Dt;
        //  //  DtStndard = Dt.Select("( ClusterName='Stress' and AssessmentName='Standard stress Assessment') OR( AssessmentName='Standard Mood Assessment' AND  ClusterName='Mood')").CopyToDataTable();
        //  DtRemedy = DtRemedyObservation.Select("ClusterName<>'Anxiety' AND ClusterName<>'Mood' AND ClusterName<>'Stress'").CopyToDataTable();
        //  DataTable DtRemedyStandard = new DataTable();
        //  DtRemedyStandard = DtRemedyObservation.Select("( ClusterName='Stress' and AssessmentName='Standard stress Assessment') OR( AssessmentName='Standard Mood Assessment' AND  ClusterName='Mood')OR( AssessmentName='Standard Anxiety Assessment' AND  ClusterName='Anxiety')").CopyToDataTable();
        //  object ObjAvg = DtRemedyStandard.Compute("Avg(Percentage)", "");
        //  DtRemedyStandard.Merge(DtRemedy);
        //  GvRemedyObservation.DataSource = DtRemedyStandard;
        //  GvRemedyObservation.DataBind();
        //  DataTable Flag = objEmployeeDAL.ResultTextMasterGet(Convert.ToInt32(ObjAvg.ToString()));

        //  GvResult.DataSource = Flag;
        //  GvResult.DataBind();

           DataTable Dt = objEmployeeDAL.EmployeeResultGet(Session["EmployeeId_FK"].ToString(),"");
          GvResult.DataSource = Dt;
          GvResult.DataBind();

          DataTable DtSuggestions = objEmployeeDAL.SuggestionMasterGetRandom();
          GvSuggestion.DataSource = DtSuggestions;
          GvSuggestion.DataBind();

          
          
        }
    }
}