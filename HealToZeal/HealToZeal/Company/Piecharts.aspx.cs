﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BE;
using DAL;
using System.Data;
using System.Web.Mail;
using System.Data.SqlClient;

namespace HealToZeal.Company
{
    public partial class Piecharts : System.Web.UI.Page
    {
        public string companyId = "";
        EmployeeDAL objEmployeeDAL = new EmployeeDAL();
        CompanyDetailsDAL ObjCompanyDAL = new CompanyDetailsDAL();
        DataTable DtCompDetails = new DataTable();
        DataTable DtClusterPercentage = new DataTable();
        protected void Page_Load(object sender, EventArgs e)
        {
            rbbatch.Items[1].Attributes.CssStyle.Add("visibility", "hidden");
           
            if (!Page.IsPostBack)
            {
                if (Session["CompanyId"] != null)
                {
                    dddepartment.Visible = false;
                    ddBatch.Visible = true;
                    Bindbatch();

                    BindAssessmentInstances();
                    if (rbbatch.SelectedValue != "M" && rbbatch.SelectedValue != "B")
                    {
                        ddlAssessmentInstance.SelectedValue = Guid.NewGuid().ToString();
                        dddepartment.SelectedValue = Guid.NewGuid().ToString();
                        ddBatch.SelectedValue = Guid.NewGuid().ToString();
                        BindCompanyDetails();
                        ReportViewer1.Visible = false;
                    }


                }
            }
        }

        private void BindAssessmentInstances()
        {
            try
            {
                AssessmentDAL objAssessmentDAL = new AssessmentDAL();
                DataTable AssessmentInstance = objAssessmentDAL.AssessmentInstanceMasterGetByCompanyId(Session["CompanyId"].ToString());
                if (AssessmentInstance != null)
                {
                    if (AssessmentInstance.Rows.Count > 0)
                    {
                        ddlAssessmentInstance.DataSource = AssessmentInstance;
                        ddlAssessmentInstance.DataTextField = "AssessmentInstanceName";
                        ddlAssessmentInstance.DataValueField = "AssessmentInstanceId_PK";
                        ddlAssessmentInstance.DataBind();
                        ddlAssessmentInstance.Items.Insert(0, new ListItem("--select Instances--"));
                    }
                }
               // BindCompanyDetails();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        //private void Bindbatch()
        //{
        //    try
        //    {
        //        SqlConnection con = new SqlConnection(@"Data Source=192.227.69.198;Initial Catalog=HealToZealNew;Integrated Security=True;Connection Timeout=36000");

        //        EmployeeBE objEmployeeBE = new EmployeeBE();

        //        string com = "Select * from BatchDetails";
        //        SqlDataAdapter adpt = new SqlDataAdapter(com, con);
        //        DataTable dt = new DataTable();
        //        adpt.Fill(dt);
        //        ddBatch.DataSource = dt;
        //        ddBatch.DataBind();
        //        ddBatch.DataTextField = "BatchName";
        //        ddBatch.DataValueField = "BatchID_PK";
        //        ddBatch.DataBind();
        //        //ddlbatch.Items.Add(new System.Web.UI.WebControls.ListItem("---Select---", "0"));
        //        ddBatch.Items.Insert(0, new System.Web.UI.WebControls.ListItem("--Select--", "0"));
        //        //ddlbatch.Items.Insert(0,new ListItem("--Select--", "0"));
        //        con.Close();

        //        //  string CompanyId = Session["CompanyId"].ToString();
        //       //  // DataTable Dtdbatch = objEmployeeDAL.AssesmentWiseBatch(CompanyId, ddlAssessmentInstance.SelectedValue);
        //        //  DataTable Dtdbatch = objEmployeeDAL.AssesmentWiseBatch();
        //        //  Dtdbatch.Rows.Add(DBNull.Value, "All");
        //        //  ddBatch.DataSource = Dtdbatch;
        //        //  ddBatch.DataTextField = "BatchName";
        //        //  ddBatch.DataValueField = "BatchID_FK";
        //        //  ddBatch.DataBind();
        //        //  ddBatch.Items.Insert(0, new System.Web.UI.WebControls.ListItem("--Select--", "0"));
        //        //  //ddBatch.Items.Insert(0, new ListItem("Select Batch", "Select Batch"));
        //        //  ddBatch.SelectedIndex = 0;
        //        ////  BindCompanyDetails();

        //    }
        //    catch
        //    {

        //    }
        //}

        private void Bindbatch()
        {
            try
            {
                string CompanyId = "";
                CompanyId = Session["CompanyId"].ToString();
                DataTable Dtdbatch = objEmployeeDAL.GetBatchName(CompanyId);

                ddBatch.DataSource = Dtdbatch;
                ddBatch.DataTextField = "BatchName";
                ddBatch.DataValueField = "BatchID_PK";
                ddBatch.DataBind();

                ddBatch.Items.Insert(0, new ListItem("--SelectBatch--", "--Select Batch--"));
                ddBatch.SelectedIndex = 0;
            }
            catch
            {

            }
        }
        private void BindDepartment()
        {
            try
            {

                companyId = Session["CompanyId"].ToString();
                DataTable DtDept = objEmployeeDAL.CompanyDepartmentGetByCompanyId(companyId);
                DtDept.Rows.Add(DBNull.Value, "All", companyId);
                dddepartment.DataSource = DtDept;

                dddepartment.DataTextField = "Departmentname";
                dddepartment.DataValueField = "DeptId_PK";
                dddepartment.DataBind();

               // dddepartment.Items.Insert(0, new ListItem("Department", "Department"));
              // BindCompanyDetails();
               // dddepartment.SelectedIndex = 0;
            }
            catch(Exception e)
            {
                throw e;
            }
        }
        protected void btnCounsel_Click(object sender, EventArgs e)
        {
            try
            {
                DivMailBody.Visible = false;
                DivTitle.Visible = false;

                EmployeeDAL objEmployeeDAL = new EmployeeDAL();
                DataTable DtEmployeeDetails = objEmployeeDAL.EmployeeAssessmentSubmittedGetByEmployeeId(Session["EmployeeId_FK"].ToString());
                uemail.Value = DtEmployeeDetails.Rows[0]["EmailId"].ToString();

            }
            catch (Exception )
            {

            }
        }

        protected void BtnSendEmail_Click(object sender, EventArgs e)
        {
            try
            {
                System.Web.Mail.MailMessage mail = new System.Web.Mail.MailMessage();

                if (Session["CompanyEmailId"] != null)
                {
                    mail.From = Session["CompanyEmailId"].ToString();//uemail.Value;
                    mail.To = "prajakta.mulay@chetanasystems.com";
                    mail.Subject = "User want to contact for appointment.";
                    if (message.Value != "")
        
                    
                    {
                        mail.Body = message.Value;
                    }
                    mail.BodyFormat = MailFormat.Html;
                    mail.Fields.Add("http://schemas.microsoft.com/cdo/configuration/smtpauthenticate", "1");    //basic authentication
                    mail.Fields.Add("http://schemas.microsoft.com/cdo/configuration/sendusername", "HealToZeal@chetanatspl.com"); //set your username here
                    mail.Fields.Add("http://schemas.microsoft.com/cdo/configuration/sendpassword", "HealToZeal");    //set your password here
                    System.Web.Mail.SmtpMail.SmtpServer = "holly.arvixe.com";  //your real server goes here                        
                    System.Web.Mail.SmtpMail.Send(mail);

                    DivMailBody.Visible = false;
                    DivTitle.Visible = false;
                    ScriptManager.RegisterClientScriptBlock(this.Page, GetType(), "openColorBox", "<script> alert('We have received your meeting request. We will get back to you very soon'); </script>;", true);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private void BindCompanyDetails()
        {
            try
            {
                ReportViewer1.Visible = true;
                

                if (DtCompDetails != null)
                {
                    if (DtCompDetails.Rows.Count > 0)
                    {
                        if (DtCompDetails.Columns.Contains("Stress1"))
                        {

                        }
                        else
                        {
                            DtCompDetails.Columns.Add("Stress1", typeof(string));
                            DtCompDetails.Columns.Add("Stress2", typeof(string));
                            DtCompDetails.Columns.Add("Stress3", typeof(string));

                            DtCompDetails.Columns.Add("Mood1", typeof(string));
                            DtCompDetails.Columns.Add("Mood2", typeof(string));
                            DtCompDetails.Columns.Add("Mood3", typeof(string));

                            DtCompDetails.Columns.Add("Anxiety1", typeof(string));
                            DtCompDetails.Columns.Add("Anxiety2", typeof(string));
                            DtCompDetails.Columns.Add("Anxiety3", typeof(string));

                            DtCompDetails.Columns.Add("Other1", typeof(string));
                            DtCompDetails.Columns.Add("Other2", typeof(string));
                        }

                     

                       
                        if (DtClusterPercentage != null)
                        {
                            if (DtClusterPercentage.Rows.Count > 0)
                            {
                                string Stress = string.Empty;
                                //Stress
                                DataRow[] Drow = DtClusterPercentage.Select("Color='#e43c37' And ClusterName='Stress'");

                               // DataTable dt = DtClusterPercentage.Select("Color='#e43c37' And ClusterName='Stress'").CopyToDataTable();
                                if (Drow.Length > 0)
                                {
                                    //if(dt.Rows.Count>0)
                                    //{
                                    //    DtCompDetails.Rows[0]["Stress1"] = dt.Rows[0][0] + " % employees supposed to be looking to be stressed out";
                                    //}
                                    DtCompDetails.Rows[0]["Stress1"] = Drow[0].ItemArray[0].ToString() + " % employees supposed to be looking to be stressed out";
                                   
                                }
                                else if(Drow.Length > 1)
                                {
                                    DtCompDetails.Rows[0]["Stress1"] = Drow[0].ItemArray[0].ToString() + " % employees supposed to be looking to be stressed out";
                                }
                                //else
                                //{
                                //    DtCompDetails.Rows[0]["Stress1"] = "0% of the employees are suffering from stress";
                                //}

                                DataRow[] Drow2 = DtClusterPercentage.Select("Color='#f4fb73' And ClusterName='Stress'");//yellow

                                if (Drow2.Length > 0 && Drow.Length > 0)
                                {
                                    DtCompDetails.Rows[0]["Stress1"] = DtCompDetails.Rows[0]["Stress1"] + " and " + Drow2[0].ItemArray[0].ToString() + " % supposed to be fighting to cope up with the stress.";
                                }
                                else if (Drow2.Length > 0 && Drow.Length == 0)
                                {
                                    DtCompDetails.Rows[0]["Stress1"] = Drow2[0].ItemArray[0].ToString() + " % supposed to be fighting to cope up with the stress.";
                                }
                                //else
                                //{
                                //    DtCompDetails.Rows[0]["Stress2"] = "0% of the employees are supposed to be suffering from stress"; ;
                                //}

                                DataRow[] Drow1 = DtClusterPercentage.Select("Color='#88e46d' And ClusterName='Stress'");//green

                                if (Drow1.Length > 0)
                                {
                                    DtCompDetails.Rows[0]["Stress1"] = DtCompDetails.Rows[0]["Stress1"] + " " + Drow1[0].ItemArray[0].ToString() + " % employees are able to handle stress well.";
                                }
                                //else
                                //{
                                //    DtCompDetails.Rows[0]["Stress3"] = "0% of the employees are having balanced stress"; ;
                                //}


                                //Mood
                                DataRow[] DrowMood = DtClusterPercentage.Select("Color='#e43c37' And ClusterName='Mood'");

                                if (DrowMood.Length > 0)
                                {
                                    DtCompDetails.Rows[0]["Mood1"] = DrowMood[0].ItemArray[0].ToString() + " % of the total employees may be suffering from mood swings.";
                                }
                                //else
                                //{
                                //    DtCompDetails.Rows[0]["Mood1"] = "0% of the employees are suffering from Mood";
                                //}

                                DataRow[] DrowMood1 = DtClusterPercentage.Select("Color='#f4fb73' And ClusterName='Mood'");//yellow
                                if (DrowMood1.Length > 0)
                                {
                                    DtCompDetails.Rows[0]["Mood1"] = DtCompDetails.Rows[0]["Mood1"] + " " + DrowMood1[0].ItemArray[0].ToString() + " % supposed to be experiencing unstable or frequent changes in moods.";
                                }
                                //else
                                //{
                                //    DtCompDetails.Rows[0]["Mood2"] = "0% of the employees are supposed to be from Mood"; ;
                                //}

                                DataRow[] DrowMood2 = DtClusterPercentage.Select("Color='#88e46d' And ClusterName='Mood'");//green
                                if (DrowMood2.Length > 0)
                                {
                                    DtCompDetails.Rows[0]["Mood1"] = DtCompDetails.Rows[0]["Mood1"] + " " + DrowMood2[0].ItemArray[0].ToString() + " % employees are balanced in mood.";
                                }
                                //else
                                //{
                                //    DtCompDetails.Rows[0]["Mood3"] = "0% of the employees are haveing balanced mood"; 
                                //}

                                //Anxiety 
                                DataRow[] DrowAnxiety = DtClusterPercentage.Select("Color='#e43c37' And ClusterName='Anxiety'");

                              //  DataTable dt1 = DtClusterPercentage.Select("Color='#e43c37' And ClusterName='Anxiety'").CopyToDataTable();
                                if (DrowAnxiety.Length > 0)
                                {
                                    if (DrowAnxiety.Length > 1)
                                    {
                                        DtCompDetails.Rows[0]["Anxiety1"] = (Convert.ToDouble(DrowAnxiety[0].ItemArray[0].ToString()) + Convert.ToDouble(DrowAnxiety[1].ItemArray[0].ToString())) + " % employees are unable to handle the anxiety related issues.";
                                    }
                                    else
                                    {
                                        DtCompDetails.Rows[0]["Anxiety1"] = DrowAnxiety[0].ItemArray[0].ToString() + " % employees are unable to handle the anxiety related issues.";
                                    }
                                   // DtCompDetails.Rows[0]["Anxiety1"] = DrowAnxiety[0].ItemArray[0].ToString() + " % employees are unable to handle the anxiety related issues.";
                                }
                                //else
                                //{
                                //    DtCompDetails.Rows[0]["Anxiety1"] = "0% of the employees are suffering from Anxiety";
                                //}

                                DataRow[] DrowAnxiety1 = DtClusterPercentage.Select("Color='#f4fb73' And ClusterName='Anxiety'");//yellow
                                if (DrowAnxiety1.Length > 0)
                                {
                                    DtCompDetails.Rows[0]["Anxiety1"] = DtCompDetails.Rows[0]["Anxiety1"] + " " + DrowAnxiety1[0].ItemArray[0].ToString() + " % supposed to be suffering from anxiety.";
                                }
                                //else
                                //{
                                //    DtCompDetails.Rows[0]["Anxiety2"] = "0% of the employees are supposed to suffering from Anxiety"; ;
                                //}

                                DataRow[] DrowAnxiety2 = DtClusterPercentage.Select("Color='#88e46d' And ClusterName='Anxiety'");//green
                                if (DrowAnxiety2.Length > 0)
                                {
                                    DtCompDetails.Rows[0]["Anxiety1"] = DtCompDetails.Rows[0]["Anxiety1"] + " " + DrowAnxiety2[0].ItemArray[0].ToString() + " % employees can control their anxiety well.";
                                }
                                //else
                                //{
                                //    DtCompDetails.Rows[0]["Anxiety3"] = "0% of the employees are having balanced Anxiety"; ;
                                //}

                                //Other
                                DataRow[] DrowOther = DtClusterPercentage.Select("Color='#f4fb73' And ClusterName='Other'");

                                if (DrowOther.Length > 0)
                                {
                                    DtCompDetails.Rows[0]["Other1"] = DrowOther[0].ItemArray[0].ToString() + " % employees are supposed to be having some or other compulsive habits.";
                                }

                                DataRow[] DrowOther1 = DtClusterPercentage.Select("Color='#88e46d' And ClusterName='Other'");//green
                                if (DrowOther1.Length > 0)
                                {
                                    DtCompDetails.Rows[0]["Other2"] = DtCompDetails.Rows[0]["Other2"] + " " + DrowOther1[0].ItemArray[0].ToString() + " % employees seem to have good control of their habits.";
                                }

                                RptrEmployee.DataSource = DtCompDetails;
                                RptrEmployee.DataBind();
                                ReportViewer1.Visible = true;

                                ReportViewer1.LocalReport.DataSources.Clear();
                                ReportViewer1.LocalReport.DataSources.Add(new Microsoft.Reporting.WebForms.ReportDataSource("DataSet1", DtClusterPercentage));
                            }
                        }
                    }
                }
                else
                {

                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        protected void BtnSetUpMeeting_Click(object sender, EventArgs e)
        {
            try
            {
                DivTitle.Focus();
                DivMailBody.Visible = true;
                DivTitle.Visible = true;

               
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        protected void ddlAssessmentInstance_SelectedIndexChanged(object sender, EventArgs e)
        {

            
            DtCompDetails.Clear();
            DtClusterPercentage.Clear();
            string batchid = ddBatch.SelectedValue;
            string instanceid = ddlAssessmentInstance.SelectedValue;
            //Bindbatch();
            try
            {
                CompanyDetailsDAL ObjCompanyDAL = new CompanyDetailsDAL();

                if (ddlAssessmentInstance.SelectedItem.Text == "All")
                {
                    DtCompDetails = ObjCompanyDAL.CompanyReportStatisticsGet1(Session["CompanyId"].ToString(), instanceid);
                    DtClusterPercentage = ObjCompanyDAL.ClusterPercentageGetByCompanyId1(Session["CompanyId"].ToString(),instanceid);

                   // DtCompDetails = ObjCompanyDAL.CompanyReportStatisticsGet(Session["CompanyId"].ToString(), ddlAssessmentInstance.SelectedValue.ToString(), Guid.NewGuid().ToString(), Guid.NewGuid().ToString());
                   // DtClusterPercentage = ObjCompanyDAL.ClusterPercentageGetByCompanyId(Session["CompanyId"].ToString(), ddlAssessmentInstance.SelectedValue.ToString(), Guid.NewGuid().ToString(), Guid.NewGuid().ToString());
                }
                else
                {
                   
                     // DtCompDetails = ObjCompanyDAL.CompanyReportStatisticsGet(Session["CompanyId"].ToString(), ddlAssessmentInstance.SelectedValue.ToString(), dddepartment.SelectedValue.ToString(), Guid.NewGuid().ToString());
                        DtCompDetails = ObjCompanyDAL.CompanyReportStatisticsGet1(Session["CompanyId"].ToString(), instanceid);
                        DtClusterPercentage = ObjCompanyDAL.ClusterPercentageGetByCompanyId1(Session["CompanyId"].ToString(), instanceid);
                        //   DtClusterPercentage = ObjCompanyDAL.ClusterPercentageGetByCompanyId(Session["CompanyId"].ToString(), ddlAssessmentInstance.SelectedValue.ToString(), dddepartment.SelectedValue.ToString(), Guid.NewGuid().ToString());

                  
                    
                }
                
              
                ReportViewer1.Visible = true;

                ReportViewer1.LocalReport.DataSources.Clear();

                ReportViewer1.LocalReport.DataSources.Add(new Microsoft.Reporting.WebForms.ReportDataSource("DataSet1", DtClusterPercentage));
                BindCompanyDetails();

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        protected void dddepartment_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                CompanyDetailsDAL ObjCompanyDAL = new CompanyDetailsDAL();
             

                ReportViewer1.LocalReport.DataSources.Clear();

                if (dddepartment.SelectedItem.Text == "All")
                {
                 //   DtCompDetails = ObjCompanyDAL.CompanyReportStatisticsGet(Session["CompanyId"].ToString(), ddlAssessmentInstance.SelectedValue.ToString(), Guid.NewGuid().ToString(), Guid.NewGuid().ToString());
                  //  DtClusterPercentage = ObjCompanyDAL.ClusterPercentageGetByCompanyId(Session["CompanyId"].ToString(), ddlAssessmentInstance.SelectedValue.ToString(), Guid.NewGuid().ToString(), Guid.NewGuid().ToString());
                }
                else
                {
                 //   DtCompDetails = ObjCompanyDAL.CompanyReportStatisticsGet(Session["CompanyId"].ToString(), ddlAssessmentInstance.SelectedValue.ToString(), dddepartment.SelectedValue.ToString(), Guid.NewGuid().ToString());
                 //   DtClusterPercentage = ObjCompanyDAL.ClusterPercentageGetByCompanyId(Session["CompanyId"].ToString(), ddlAssessmentInstance.SelectedValue.ToString(), dddepartment.SelectedValue.ToString(), Guid.NewGuid().ToString());
                }
                ReportViewer1.Visible = true;

                ReportViewer1.LocalReport.DataSources.Clear();
                ReportViewer1.LocalReport.DataSources.Add(new Microsoft.Reporting.WebForms.ReportDataSource("DataSet1", DtClusterPercentage));
                BindCompanyDetails();





            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        protected void ddBatch_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                CompanyDetailsDAL ObjCompanyDAL = new CompanyDetailsDAL();

                string batchid = ddBatch.SelectedValue;
                string instanceid = ddlAssessmentInstance.SelectedValue;

                //Guid guid;
                //if (Guid.TryParse(ddlAssessmentInstance.SelectedItem.Value, out guid))
                //{
                //    Parameters.Add("@ParentGroupId", SqlDbType.UniqueIdentifier).Value = guid;
                //}

                if (ddBatch.SelectedItem.Text == "All")
                {
                    DtCompDetails = ObjCompanyDAL.CompanyReportStatisticsGet2(Session["CompanyId"].ToString(), batchid);
                    // DtCompDetails = ObjCompanyDAL.CompanyReportStatisticsGet(Session["CompanyId"].ToString(), ddlAssessmentInstance.SelectedValue.ToString(), Guid.NewGuid().ToString(), Guid.NewGuid().ToString());
                     //DtClusterPercentage = ObjCompanyDAL.ClusterPercentageGetByCompanyId(Session["CompanyId"].ToString(), ddlAssessmentInstance.SelectedValue.ToString(), Guid.NewGuid().ToString(), Guid.NewGuid().ToString());
                    DtClusterPercentage = ObjCompanyDAL.ClusterPercentageGetByCompanyId2(Session["CompanyId"].ToString(),batchid);
                }
                else
                {
                    DtCompDetails = ObjCompanyDAL.CompanyReportStatisticsGet2(Session["CompanyId"].ToString(), batchid);
                  //  DtCompDetails = ObjCompanyDAL.CompanyReportStatisticsGet(Session["CompanyId"].ToString(), ddlAssessmentInstance.SelectedValue.ToString(), Guid.NewGuid().ToString(), ddBatch.SelectedValue.ToString());
                    DtClusterPercentage = ObjCompanyDAL.ClusterPercentageGetByCompanyId2(Session["CompanyId"].ToString(), batchid);

                   // DtClusterPercentage = ObjCompanyDAL.ClusterPercentageGetByCompanyId(Session["CompanyId"].ToString(), ddlAssessmentInstance.SelectedValue.ToString(), Guid.NewGuid().ToString(), ddBatch.SelectedValue.ToString());
                }
                ReportViewer1.Visible = true;

                ReportViewer1.LocalReport.DataSources.Clear();
                ReportViewer1.LocalReport.DataSources.Add(new Microsoft.Reporting.WebForms.ReportDataSource("DataSet1", DtClusterPercentage));
                BindCompanyDetails();


            }
            catch (Exception ex)
            {
                ex.InnerException.ToString();
            }
        }

        protected void rbbatch_SelectedIndexChanged(object sender, EventArgs e)
        {


            if (rbbatch.SelectedValue == "B")
            {
                dddepartment.Visible = false;
                ddBatch.Visible = true;
                //ddlAssessmentInstance.Visible = true;
                BindAssessmentInstances();
               // BindDepartment();
              //  Bindbatch();
              //  DtCompDetails = ObjCompanyDAL.CompanyReportStatisticsGet(Session["CompanyId"].ToString(), ddlAssessmentInstance.SelectedValue.ToString(), Guid.NewGuid().ToString(), ddBatch.SelectedValue.ToString());
              //  DtClusterPercentage = ObjCompanyDAL.ClusterPercentageGetByCompanyId(Session["CompanyId"].ToString(), ddlAssessmentInstance.SelectedValue.ToString(), Guid.NewGuid().ToString(), ddBatch.SelectedValue.ToString());
                ReportViewer1.Visible = true;

               

            }
            if (rbbatch.SelectedValue == "D")
            {
                dddepartment.Visible = true;
                ddlAssessmentInstance.Visible = true;
                ddBatch.Visible = false;
                BindAssessmentInstances();
               // BindDepartment();
              //  DtCompDetails = ObjCompanyDAL.CompanyReportStatisticsGet(Session["CompanyId"].ToString(), ddlAssessmentInstance.SelectedValue.ToString(), dddepartment.SelectedValue.ToString(), Guid.NewGuid().ToString());
             //   DtClusterPercentage = ObjCompanyDAL.ClusterPercentageGetByCompanyId(Session["CompanyId"].ToString(), ddlAssessmentInstance.SelectedValue.ToString(), dddepartment.SelectedValue.ToString(), Guid.NewGuid().ToString());
            }
        
            BindCompanyDetails();

        }


        public void createsqqlcommand()
        {
            SqlCommand cmd = new SqlCommand();
            cmd.CommandTimeout = 60;
            cmd.CommandType = CommandType.Text;
        }
    }
}