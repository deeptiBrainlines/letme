﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Company/Company.Master" AutoEventWireup="true" CodeBehind="AboutUs.aspx.cs" Inherits="HealToZeal.Company.AboutUs" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="LeftColumn" runat="server">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12" style="font-size: medium">
                <h4></h4>
                <div class="row hs_how_we_are">
                    <div class="col-lg-11 col-md-11 col-sm-11">
                        <%--<img src="../images/IMG_20150406_130816.jpg" />--%>

                        <div id="divalert" class="alert alert-dismissable" runat="server">

                            <p>
                                <h4><strong><u>LetMeTalk2Me - For individuals</u></strong> </h4>
                                Creating harmony within assures harmony around. In these fast, impatient, and competitive times of today, emotional fitness is of utmost importance. How else do we hope to stand up to the statistics which say that every one person in four is suffering from stress, anxiety or depression?
                            </p>
                            <br />
                            <p>
                                Most of the people reading this text can find themselves shifting between five compartments in their lives:Individual/personal,Marital,Family,Social,Professional
                            </p>                             
                            <br />
                            <p>
                                Emotional fitness in one compartment will reflect in all others. Likewise lack of emotional fitness and stability in one of those will essentially spill over to rest of them. This will results in inefficiency, stress, and a vicious cycle of further compromised fitness. Anger outbursts, crying spells, general lethargy, lack of enjoyment…just a few manifestations of compromised emotional fitness. We are sure that when your efficiency and productivity goes for a toss due to these, you don’t particularly enjoy the situation. After all no one likes poor quality of life.
                            </p>
                            <br />
                            <p>
                                So what can you do about it?
                            </p>
                          
                            <p>
                                Step 1: Awareness of the situation. Just KNOWING that emotional fitness is so much important.
                            </p>
                           
                            <p>
                                Step 2: Acknowledgment. You could be going very strong in some areas and not so strong in others. ACKNOWLEDGE your areas of strengths and those of weakness.
                            </p>
                            
                            <p>
                                Step 3: Working on those weaknesses, so as to cope effectively, and lead a good quality life.
                            </p>
                            <br />

                            <p>
                                Alright. Then where can you start?
                            </p>
                            <br />
                            <p>
                                Just as you undergo a regular physical testing for sugar, cholesterol, blood pressure etc, you can make a practice of assessing your mental and emotional health. Look again…we are talking of NOT ILLNESS, but HEALTH. Ask yourself certain questions which can give you a look inside your mind from different perspectives. Your answers to these questions will tell you about your efficiency and effectiveness. These will make you aware of how well you handle different situations and roles.
                            </p>
                            
                            <p>
                                As we value your contribution to our Organization, we care for your overall well-being. We will provide any further assistance if you feel the need to work on the weaknesses.
                            </p>
                            <br />
                            <p>
                                <h4><strong><u>LetMeTalk2Me - For organizations</u></strong> </h4>
                                An organization's success depends, to a large extent, on the workforce it employs. While it is important to hire with the right skill sets, it is equally important to help them evaluate their psychological health to improve performance and create a harmonious work environment. 
                                <br />
                            </p>
                            <p> 
                                LetMeTalk2Me is a web and mobile based portal that facilitates self assessment using simple tests so that individuals recognize their traits and distress at their convenience. Developed by subject matter experts, this psychometric assessment enables organizations to monitor psychological health of the employees regularly, report observations and offer solutions to address the findings. 
                                Timely measure of any such psychological issue/difficulty/challenge and providing the appropriate solution helps prevent further complications and also boost the morale of the employees, thus improving their performance as workplace.
                            </p>

                            <hr style="color: black;" />
                            <br />

                            <%-- <p>
                                <h4><strong><u>Team</u></strong> </h4>
                                <b>Priyamvada Bavadekar </b>- CEO
                                <br />
                                 <p>
                                    ¤  Over 15 years of valuable experience (including 9 years of entrepreneurship) in IT Consultancy Services, Project Management and Client Relationship Management
                                </p>
                                <p>
                                    ¤  An enterprising leader with skills in mentoring and motivating individuals towards maximising productivity as well as in forming cohesive team environments
                                </p>
                                <br />
                                <b>Hemant Karandikar </b>– Advisor and Mentor
                                <br />
                                <p>
                                    ¤  Coaching to CEOs & management teams through a combination of onsite and web based support through Learning Leadership set up for work based workouts and coaching
                                </p>
                                <br />
                                <b>Vandana Kulkarni </b>– Counselor 20 years of experience
                                 <br />
                                 <p>¤  Associated with IT companies as a Counselor </p>

                                 <p>¤  Workshops and seminars facilitator </p>   

                                <br />
                                <b>Kavitagauri Joshi</b>– Clinical Psychologist
                                <br />
                                <p>
                                    ¤  Independent  counselor and assessment expert for various age groups
                                </p>
                               
                                <p>
                                    ¤  Masters in Clinical Psychology; working with a Mental Health Institute Workshops and seminars facilitator for a plethora of mental health related issues
                                </p>
                            </p>--%>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
