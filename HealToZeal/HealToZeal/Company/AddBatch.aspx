﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Company/Company.Master" AutoEventWireup="true" CodeBehind="AddBatch.aspx.cs" Inherits="HealToZeal.Company.AddBatch" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style>
        .hs_pager1
        {
            display: inline-block;
            list-style: none;
            padding-left: 0px;
        }
        .hs_pager1 a
        {
            padding: 5px 12px;
            color: #7f9aa0;
            border: 1px solid #7f9aa0;
            text-align: center;
            font-weight: bold;
            font-size: 16px;
            -webkit-transition: all 0.5s;
            -moz-transition: all 0.5s;
            -ms-transition: all 0.5s;
            -o-transition: all 0.5s;
            margin-bottom: 60px;
        }
        .hs_pager1 a:hover, .hs_pager1 a:active
        {
            color: #00ac7a;
            border: 1px solid #00ac7a;
            -webkit-transition: all 0.5s;
            -moz-transition: all 0.5s;
            -ms-transition: all 0.5s;
            -o-transition: all 0.5s;
        }
        .pagination
        {
            line-height: 26px;
        }
        
        .pagination span
        {
            padding: 5px;
            border: solid 1px #477B0E;
            text-decoration: none;
            white-space: nowrap;
            background: #547B2A;
        }
        
        .pagination a, .pagination a:visited
        {
            text-decoration: none;
            padding: 6px;
            white-space: nowrap;
        }
        .pagination a:hover, .pagination a:active
        {
            padding: 5px;
            border: solid 1px #9ECDE7;
            text-decoration: none;
            white-space: nowrap;
            background: #486694;
        }
    </style>
    <style type="text/css">
        .style6
        {
            width: 125px;
        }
        .style7
        {
            height: 26px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="LeftColumn" runat="server">
     <h4 class="hs_heading" id="hs_appointment_form_link">Add Batch</h4>
     <div class="container">
                <div class="row">
                    <table style="width: 100%">
                        <tr>
                            <td style="width: 100%; text-align:left">
                                   <div class="col-lg-8 col-md-8 col-sm-7">
                                    <div class="hs_comment_form">
                                                               
                                          <div class="row">   
                                                 <div class="col-lg-6 col-md-6 col-sm-12">
                                                        <div class="input-group">
                                                            <span class="input-group-btn">                                                            
                                                            </span>
                                                            <input type="text" id="Batchname" runat="server" name="Batch Name" class="form-control" placeholder="Batch Name (required)" required />
                                                            </div>
                                                     </div>
                                        </div>
                                          <div class="row">   
                                                 <div class="col-lg-6 col-md-6 col-sm-12">
                                                        <div class="input-group">
                                                            <span class="input-group-btn">                                                            
                                                            </span>
                                                             <asp:Button ID="btnAddbatch" OnClick="btnAddbatch_Click" runat="server" Text="Add" CssClass="btn btn-success pull-right" />
                                                            </div>
                                                     </div>
                                        </div>
                                        <div class="row">
                                              <div id="divalert" class="alert alert-success" runat="server">
                           <strong> <asp:Label ID="lblMessage" runat="server" ></asp:Label></strong>
                        </div>
                                        </div>

                                       <%-- <div class="row">
                                            <asp:Label ID="Label1" runat="server" Text="Select Assessment"></asp:Label><br />
                                            <asp:DropDownList ID="ddlassessments" runat="server" ></asp:DropDownList>
                                        </div>--%>
                         </div>
                         </div>
                            </td>
                        </tr>
                    </table>
                  
                    </div>
         </div>
  

</asp:Content>
