﻿using System;
using System.Web.UI;
using DocumentFormat.OpenXml.Spreadsheet;
using System.Data;
using System.IO;
using DAL;
using BE;
using System.Web.Mail;

namespace HealToZeal.Company
{
    public partial class ImportEmployees1 : System.Web.UI.Page
    {
        public string CustomerInfoPath = "";
        // string CompanyId = string.Empty;
        EmployeeDAL objEmployeeDAL = new EmployeeDAL();
        string EmployeeId = "";

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                if (Session["CompanyId"] != null)
                {

                }
            }
            else
            {
                Response.Redirect("Companylogin.aspx");
            }
        }

        private void ValidateData(string FileLocation)
        {
            try
            {
                Boolean hasError = false;
                // string Message = "";
                //Int16 errorOnRows = 0;

                ImportExportToExcel importExportToExcel = new ImportExportToExcel();
                DataTable DtExcelDataTable = importExportToExcel.ExcelToDatatable(FileLocation);
                Session["DtExcelDataTable"] = DtExcelDataTable;

                if (!hasError)
                {

                    ImportDatafromExcel(FileLocation);

                    ImportExportToExcel ObjImport = new ImportExportToExcel();


                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        private void ImportDatafromExcel(string FileLocation)
        {
            ImportExportToExcel importExportToExcel = new ImportExportToExcel();
            DataTable dtData = importExportToExcel.ExcelToDatatable(FileLocation);
            ImportMemberData(dtData);

        }

        private void ImportMemberData(DataTable dtData)
        {
            int rownumber;
            try
            {
                //insert
                EmployeeBE[] objEmployeeBE = new EmployeeBE[dtData.Rows.Count];
                for (rownumber = 0; rownumber < dtData.Rows.Count; rownumber++)
                {

                    objEmployeeBE[rownumber] = new EmployeeBE();
                    string password = Encryption.CreatePassword();

                    string userName = Encryption.CreateUsername();
                    string CompanyId = Session["CompanyId"].ToString();
                    objEmployeeBE[rownumber].propCompanyId_FK = CompanyId;
                    objEmployeeBE[rownumber].propEmployeeCompanyId = Convert.ToInt32(dtData.Rows[rownumber]["ComapnyID"]);
                    objEmployeeBE[rownumber].propUserName = userName;
                    objEmployeeBE[rownumber].propPassword = Encryption.Encrypt(password);
                    objEmployeeBE[rownumber].propFirstName = dtData.Rows[rownumber]["FirstName"].ToString();
                    objEmployeeBE[rownumber].propLastName = dtData.Rows[rownumber]["Lastname"].ToString(); ;

                    objEmployeeBE[rownumber].propEmailId = dtData.Rows[rownumber]["EmailID"].ToString();
                    objEmployeeBE[rownumber].propCadreId_FK = dtData.Rows[rownumber]["FunctionalDesg"].ToString();

                    //code by Poonam
                    objEmployeeBE[rownumber].propDeptId_PK = dtData.Rows[rownumber]["Department"].ToString();

                    objEmployeeBE[rownumber].PropBatchID = dtData.Rows[rownumber]["Batch"].ToString();
                    objEmployeeBE[rownumber].propCreatedBy = CompanyId;
                    objEmployeeBE[rownumber].propCreatedDate = DateTime.Now.Date;
                    // string message = "";
                    int Status = 0;

                    if (Session["EmployeeId"] != null)
                    {
                        EmployeeId = Session["EmployeeId"].ToString();
                        Status = objEmployeeDAL.EmployeesUpdateByCompanyIdImport(objEmployeeBE[rownumber]);
                    }
                    else
                    {
                        Status = objEmployeeDAL.EmployeesImport(objEmployeeBE[rownumber]);

                        // InsertManager();
                        if (Status > 0)
                        {
                            MailMessage mail = new MailMessage();
                            // System.Web.Mail.MailMessage mail = new System.Web.Mail.MailMessage();
                            mail.To = dtData.Rows[rownumber]["EmailID"].ToString();
                            mail.Bcc = "priyamvada@chetanasystems.com";
                            mail.From = "HealToZeal@chetanatspl.com";
                            mail.Subject = "Welcome to the Health and Personality Insights and Actions Tool.";
                            string getDt = DateTime.Now.Date.AddDays(7).ToString("MM/dd/yyyy");

                            mail.Body = "<html><body><font color=\"black\">Dear " + dtData.Rows[rownumber]["FirstName"].ToString() + " " + dtData.Rows[rownumber]["Lastname"].ToString() + ",<br/><br/>Welcome to <b> Let Me Talk2 Me: A Health and Personality Insights and Actions Tool.</b> You will be able to gain some useful insights into your own personality and learn about remedial actions in case they are needed. <b>'Let Me Talk2 Me'</b> will help you in self-discovery. It will lead on a path of self development while avoiding pitfalls of modern complex life and careers. <br/><br/>It is recommended that you use this tool periodically to monitor yourself and be aware of your health and personality parameters. <br/><br/> Please use the link below to login <br/><br/><a href='http://chetanatspl.com/Healtozealcurrent/Employee/EmployeeLogin.aspx?first=1'> http://chetanatspl.com/Healtozealcurrent/Employee/EmployeeLogin.aspx </a><br/>Your login details are as follows:<br/>UserId:<b>" + userName + "</b><br/>Password:<b>" + password + "</b><br/><br/>If you would like to take a break during assessment, please make sure you log out from the system or close the browser window for security purposes. Please do not leave the browser idle. You can always log back in whenever you like.<br/><br/>Please ensure that you complete this assessment on or before " + getDt + ". To get a maximum benefit, please repeat this exercise every 3 months and study the differentials.<br/> You will be notified for the same when the next assessment is due.<br/><br/> Thank You.<br/><br/></font></body></html>";
                            mail.BodyFormat = MailFormat.Html;
                            mail.Fields.Add("http://schemas.microsoft.com/cdo/configuration/smtpauthenticate", "1");    //basic authentication
                            mail.Fields.Add("http://schemas.microsoft.com/cdo/configuration/sendusername", "HealToZeal@chetanatspl.com"); //set your username here
                            mail.Fields.Add("http://schemas.microsoft.com/cdo/configuration/sendpassword", "HealToZeal");    //set your password here

                            System.Web.Mail.SmtpMail.SmtpServer = "holly.arvixe.com";  //your real server goes here                        
                            System.Web.Mail.SmtpMail.Send(mail);



                            lbmessage.Text = "Employee added successfully...";
                        }
                        else
                        {
                            //  divalert.Attributes.Add("class", "alert alert-danger");
                            //message = "Employee can not be added...";
                            lbmessage.Text = "Employee can not be added...";
                        }
                    }
                }
                // lblMessage.Text = message;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        protected void btnimport_Click(object sender, EventArgs e)
        {
            try
            {


                if (exampleInputFile.HasFile)
                {
                    string ext = System.IO.Path.GetExtension(exampleInputFile.PostedFile.FileName).ToLower();
                    if (ext == ".xls" || ext == ".xlsx")
                    {


                        if (!Directory.Exists(Server.MapPath("Data" + CustomerInfoPath)))
                        {
                            Directory.CreateDirectory(Server.MapPath("Data" + CustomerInfoPath));
                        }

                        String datetime = DateTime.Now.Ticks.ToString();
                        String fileExt = Path.GetExtension(exampleInputFile.FileName);


                        exampleInputFile.SaveAs(Server.MapPath("Data" + CustomerInfoPath) + "\\" + datetime + fileExt);
                        string path = Server.MapPath("Data" + CustomerInfoPath) + "\\" + datetime + fileExt;

                        if (path != "")
                        {
                            ValidateData(path);
                            exampleInputFile.Enabled = false;
                            exampleInputFile.Enabled = false;
                        }
                    }
                    else
                    {
                        lbmessage.Text = "Select Only .xslx and .xls format for Import.";
                    }
                }
                else
                {
                    lbmessage.Text = "Please Select File first.";
                }
            }

            catch (Exception ex)
            {
                throw ex;
            }
        }

    }
}