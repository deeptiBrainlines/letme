﻿using DAL;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace HealToZeal.Company
{
    public partial class EmployeeHistory : System.Web.UI.Page
    {
        DataTable dt = new DataTable();
        DataTable dt2 = new DataTable();
        protected void Page_Load(object sender, EventArgs e)
        {
            //if (Session["CompanyId"] != null)
            //{
            //    if (Request.QueryString["Id"] != null)
            //    {
            //        lblName.Text = Request.QueryString["EmployeeId"].ToString() + "'s ";
            //    }
            //}

            String empid = Request.QueryString["Id"].ToString();
            //if (Session["EmployeeId"] != null)
            //{
            //    if (Request.QueryString["EmployeeId"] != null)
            //    {
            //    }
            //}
            addColumns();
            EmployeeAssessmentsHistoryDAL objEmployeeDAL = new EmployeeAssessmentsHistoryDAL();
            DataTable DtClusterPercentage = new DataTable();

            DtClusterPercentage = objEmployeeDAL.EmployeeHistory(empid);

            for (int i = 0; i < DtClusterPercentage.Rows.Count; i++)
            {

                dt.Rows.Add(DtClusterPercentage.Rows[i]["EmployeeCompanyId"],
                    DtClusterPercentage.Rows[i]["AssessmentName"],
               DtClusterPercentage.Rows[i]["AssessmentId_FK"],
               DtClusterPercentage.Rows[i]["num"],
               DtClusterPercentage.Rows[i]["ClusterName"],
               DtClusterPercentage.Rows[i]["ClusterScore"],
               DtClusterPercentage.Rows[i]["CreatedDate"], DtClusterPercentage.Rows[i]["EmployeeName"],
               DtClusterPercentage.Rows[i]["ClusterPercentage"],
               DtClusterPercentage.Rows[i]["Color"],
               DtClusterPercentage.Rows[i]["IsPositive"],
               DtClusterPercentage.Rows[i]["AssessmentInstanceId_FK"],
               DtClusterPercentage.Rows[i]["AssessmentInstanceName"]);


                ReportViewer1.Visible = true;

                ReportViewer1.LocalReport.DataSources.Clear();

                ReportViewer1.LocalReport.DataSources.Add(new Microsoft.Reporting.WebForms.ReportDataSource("DataSet1", dt));

            

            }
        }

        public void addColumns()
        {
            dt.Columns.Add("EmployeeCompanyId", typeof(string));
            dt.Columns.Add("AssessmentName", typeof(string));
            dt.Columns.Add("AssessmentId_FK", typeof(string));
            dt.Columns.Add("num", typeof(int));
            dt.Columns.Add("ClusterName", typeof(string));
            dt.Columns.Add("ClusterScore", typeof(string));
            dt.Columns.Add("CreatedDate", typeof(DateTime));
            dt.Columns.Add("EmployeeName", typeof(string));
            dt.Columns.Add("ClusterPercentage", typeof(double));
            dt.Columns.Add("Color", typeof(string));
            dt.Columns.Add("IsPositive", typeof(char));
            dt.Columns.Add("AssessmentInstanceId_FK", typeof(string));
            dt.Columns.Add("AssessmentInstanceName", typeof(string));


         
        }
    }
}