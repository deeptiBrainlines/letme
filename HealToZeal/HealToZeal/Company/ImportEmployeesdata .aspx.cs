﻿using DAL;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BE;
using System.Data.SqlClient;

namespace HealToZeal.Company
{
    public partial class ImportEmployeesdata : System.Web.UI.Page
    {
        EmployeeDAL objEmployeeDAL = new EmployeeDAL();
        CompanyDetailsDAL ObjCompanyDetailsDAL = new CompanyDetailsDAL();
        static DataTable DtAnswer = new DataTable();
        public string ItemFilePath = "";
       // string adminId = "";
       // string QuestionId = "";
       // static int NoOfAnswers = 0;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                //SqlConnection con = new SqlConnection(@"Data Source=192.227.69.198;Initial Catalog=HealToZealNew;Integrated Security=True;Connection Timeout=36000");
                //EmployeeBE objEmployeeBE = new EmployeeBE();

                //string com = "Select * from BatchDetails";
                //SqlDataAdapter adpt = new SqlDataAdapter(com, con);
                //DataTable dt = new DataTable();
                //adpt.Fill(dt);
                //ddlbatch.DataSource = dt;
                //ddlbatch.DataBind();
                //ddlbatch.DataTextField = "BatchName";
                //ddlbatch.DataValueField = "BatchID_PK";
                //ddlbatch.DataBind();
                //ddlbatch.Items.Insert(0, new ListItem("--Select--", "0"));
                //con.Close();
                Bindbatch();
            }
        }
        private void Bindbatch()
        {
            try
            {
                string CompanyId = "";
                CompanyId = Session["CompanyId"].ToString();
                DataTable Dtdbatch = objEmployeeDAL.GetBatchName(CompanyId);

                ddlbatch.DataSource = Dtdbatch;
                ddlbatch.DataTextField = "BatchName";
                ddlbatch.DataValueField = "BatchID_PK";
                ddlbatch.DataBind();

                ddlbatch.Items.Insert(0, new System.Web.UI.WebControls.ListItem("--SelectBatch--", "--Select Batch--"));
                ddlbatch.SelectedIndex = 0;
            }
            catch
            {

            }
        }
        private void ImportDatafromExcel(string FileLocation)
        {
            ImportExportToExcel importExportToExcel = new ImportExportToExcel();
            DataTable dtData = importExportToExcel.ExcelToDatatable(FileLocation);
            ImportMemberData(dtData);

        }


        // public string PropQuestion { get; set; }
        private void ImportMemberData(DataTable dtData)
        {
            try
            {
                int rownumber = default(int);
               

                EmployeeAssessmentDetailsBE objQuestionMasterBE1 = new EmployeeAssessmentDetailsBE();
                EmployeeAssessmentDetailsDAL ObjEmployeeAssessmentDetailsDAL = new EmployeeAssessmentDetailsDAL();
                AnswerMasterDAL objAnswerMasterDAL = new AnswerMasterDAL();
                //for (rownumber = 1; rownumber < dtData.Rows.Count; rownumber++)
                //{
                //    for (int i = 1; i < dtData.Columns.Count; i++)
                //    {

                //        DataTable dt = objAnswerMasterDAL.GetQuestionID(dtData.Rows[i]["Question"].ToString());
                //        if (dt.Rows.Count > 0)
                //        {

                //            DataTable dtanswer;
                //            objQuestionMasterBE1.propQuestionId_FK = dt.Rows[0]["QuestionID_PK"].ToString();
                //            //if (Convert.ToInt32(dtData.Rows[rownumber][i]).ToString() != "")
                //            //{
                //            string answer = (dtData.Rows[rownumber][i].ToString());

                //            dtanswer = objAnswerMasterDAL.GetAnswerID1(dt.Rows[0]["QuestionID_PK"].ToString(), answer);
                //            if (dtanswer.Rows.Count == 0)
                //            {
                //                dtanswer = objAnswerMasterDAL.GetAnswerID1(dt.Rows[0]["QuestionID_PK"].ToString(), "Sometimes");
                //            }

                //            if (dtanswer.Rows.Count > 0)
                //            {
                //                objQuestionMasterBE1.propAnswerId_FK = dtanswer.Rows[0]["AnswerID_PK"].ToString();
                //                DataTable dtAssessment = objAnswerMasterDAL.GetAssessmentID((dtData.Rows[rownumber]["Assessment"].ToString()));

                //                objQuestionMasterBE1.propAssessmentId_FK = dtAssessment.Rows[0]["AssessmentId_PK"].ToString();
                //                objQuestionMasterBE1.propEmployeeId_FK = dtData.Rows[0][i].ToString();
                //                objQuestionMasterBE1.propCreatedBy = dtData.Rows[0][i].ToString();
                //                objQuestionMasterBE1.propInstanceCompanyRelationId_FK = "600A89C7-B144-4ED6-B1E8-8C8FFF6417DE";
                //                //objQuestionMasterBE1.propInstanceCompanyRelationId_FK = "BD293E6B-12D2-4F88-A360-6D274E6FC1D4";
                //                int status = ObjEmployeeAssessmentDetailsDAL.EmployeeAssessmentDetailsImport(objQuestionMasterBE1);
                //            }
                //        }
                //        // }

                //    }
                //}


                for (rownumber = 1; rownumber < dtData.Rows.Count; rownumber++)
                {
                    for (int i = 1; i < dtData.Columns.Count; i++)
                    {
                        string batchid = ddlbatch.SelectedValue;
                        DataTable dt = objAnswerMasterDAL.GetQuestionID(dtData.Rows[0][i].ToString(), batchid);
                        if (dt.Rows.Count > 0)
                        {

                            DataTable dtanswer;
                             objQuestionMasterBE1.propQuestionId_FK = dt.Rows[0]["QuestionID_PK"].ToString();
                            //if (Convert.ToInt32(dtData.Rows[rownumber][i]).ToString() != "")
                            //{
                            string answer = (dtData.Rows[rownumber][i].ToString());

                            dtanswer = objAnswerMasterDAL.GetAnswerID1(dt.Rows[0]["QuestionID_PK"].ToString(), answer, batchid);
                            if (dtanswer.Rows.Count == 0)
                            {
                                if (dt.Rows.Count > 0)
                                {
                                    dtanswer = objAnswerMasterDAL.GetAnswerID1(dt.Rows[0]["QuestionID_PK"].ToString(), "Sometimes", batchid);
                                    // dtanswer.Rows[0]["AnswerID_PK"] = 1;
                                }
                            }

                            if (dtanswer.Rows.Count > 0)
                            {
                                objQuestionMasterBE1.propAnswerId_FK = dtanswer.Rows[0]["AnswerID_PK"].ToString();
                               //  DataTable dtAssessment = objAnswerMasterDAL.GetAssessmentID((dtData.Rows[rownumber][dtData.Rows.Count].ToString()));

                                // objQuestionMasterBE1.propAssessmentId_FK = dtAssessment.Rows[0]["AssessmentId_PK"].ToString();
                           // objQuestionMasterBE1.propAssessmentId_FK = "9465B829-997E-42F1-85D2-128FF3E5B6C6";
                              
                           // objQuestionMasterBE1.propAssessmentId_FK = "BD293E6B-12D2-4F88-A360-6D274E6FC1D4";
                               objQuestionMasterBE1.propEmployeeCompanyID= Convert.ToInt32(dtData.Rows[rownumber][0]); //commented by meenakshi 17.11.18
                                objQuestionMasterBE1.batchid = ddlbatch.SelectedValue; //added by meenu 1.12.18
                                objQuestionMasterBE1.propCreatedBy = dtData.Rows[rownumber][0].ToString();
                                //  objQuestionMasterBE1.propInstanceCompanyRelationId_FK = "F4B11269-D4FF-4C28-9295-11C92CEB6F4D";
                                //  objQuestionMasterBE1.propInstanceCompanyRelationId_FK= "9465B829-997E-42F1-85D2-128FF3E5B6C6";
                                //  objQuestionMasterBE1.propInstanceCompanyRelationId_FK = "C65CC290-922B-479E-8D86-E1FE92A778FD";
                             //   objQuestionMasterBE1.propInstanceCompanyRelationId_FK = "F4743A54-2D60-4E71-973A-492F07ECF98E";
                                int status = ObjEmployeeAssessmentDetailsDAL.EmployeeAssessmentDetailsImport(objQuestionMasterBE1);

                               // Added by meenakshi 14.12.18 not using instead of this directly calculating through assessment details

                               //EmployeeAssessmentsHistoryBE objEmployeeAssessmentsHistoryBE = new EmployeeAssessmentsHistoryBE();
                               // EmployeeAssessmentsHistoryDAL objEmployeeAssessmentsHistoryDAL = new EmployeeAssessmentsHistoryDAL();

                               // objEmployeeAssessmentsHistoryBE.propAssessmentId_FK = "BD293E6B-12D2-4F88-A360-6D274E6FC1D4"; //"9465B829-997E-42F1-85D2-128FF3E5B6C6"; //"BD293E6B-12D2-4F88-A360-6D274E6FC1D4";
                               // objEmployeeAssessmentsHistoryBE.propEmployeeCompanyID = Convert.ToInt32(dtData.Rows[rownumber][0]);
                               // objEmployeeAssessmentsHistoryBE.batchid = ddlbatch.SelectedValue;
                               // objEmployeeAssessmentsHistoryBE.propAssessmentSubmitted = "Y";
                               // objEmployeeAssessmentsHistoryBE.propCreatedBy = dtData.Rows[rownumber][0].ToString();
                               // //objEmployeeAssessmentsHistoryBE.propCreatedDate = DateTime.Now.Date;
                               // objEmployeeAssessmentsHistoryBE.propAssessmentInstanceId_FK = "F4743A54-2D60-4E71-973A-492F07ECF98E";//"3005FED7-B5D3-4784-A3F6-52860304EE1E"; //"3005FED7-B5D3-4784-A3F6-52860304EE1E"; //Session["AssessmentInstanceId"].ToString();
                               // objEmployeeAssessmentsHistoryBE.propInstanceCompanyRelationId_FK = "F4743A54-2D60-4E71-973A-492F07ECF98E";// Session["AssessmentInstanceCompanyId"].ToString();

                               // int status1 = objEmployeeAssessmentsHistoryDAL.EmployeeAssessmentsHistoryInsert(objEmployeeAssessmentsHistoryBE);




                            }

                        }
                        // }

                    }
                }
                Response.Write("<script>alert('Data Imported successfully')</script>");
            }
            catch (Exception ex)
            {
                ex.InnerException.ToString(); //throw (ex);
            }
        }


        private void ValidateData(string FileLocation)
        {
            try
            {
                Boolean hasError = false;

                ImportExportToExcel importExportToExcel = new ImportExportToExcel();
                DataTable DtExcelDataTable = importExportToExcel.ExcelToDatatable(FileLocation);
                Session["DtExcelDataTable"] = DtExcelDataTable;

                if (!hasError)
                {
                    ImportDatafromExcel(FileLocation);
                    ImportExportToExcel ObjImport = new ImportExportToExcel();

                }
            }
            catch (Exception ex)
            {
                ex.InnerException.ToString();
            }
        }



        protected void btnimport_Click1(object sender, EventArgs e)
        {
            try
            {

                if (exampleInputFile.HasFile)
                {
                    string ext = System.IO.Path.GetExtension(exampleInputFile.PostedFile.FileName).ToLower();
                    if (ext == ".xls" || ext == ".xlsx")
                    {


                        if (!System.IO.Directory.Exists(Server.MapPath("Data" + ItemFilePath)))
                        {
                            System.IO.Directory.CreateDirectory(Server.MapPath("Data" + ItemFilePath));
                        }

                        String datetime = DateTime.Now.Ticks.ToString();
                        String fileExt = System.IO.Path.GetExtension(exampleInputFile.FileName);


                        exampleInputFile.SaveAs(Server.MapPath("Data" + ItemFilePath) + "\\" + datetime + fileExt);
                        string path = Server.MapPath("Data" + ItemFilePath) + "\\" + datetime + fileExt;

                        if (path != "")
                        {
                            ValidateData(path);
                            //exampleInputFile.Enabled = true;
                            //btnimport.Enabled = true;

                        }
                    }
                    else
                    {
                    }
                }
                else
                {

                }
            }
            catch (Exception )
            {

            }
        }

        protected void btnImoprtExcelData_Click(object sender, EventArgs e)
        {
            try
            {

                if (exampleInputFile.HasFile)
                {
                    string ext = System.IO.Path.GetExtension(exampleInputFile.PostedFile.FileName).ToLower();
                    if (ext == ".xls" || ext == ".xlsx")
                    {


                        if (!System.IO.Directory.Exists(Server.MapPath("Data" + ItemFilePath)))
                        {
                            System.IO.Directory.CreateDirectory(Server.MapPath("Data" + ItemFilePath));
                        }

                        String datetime = DateTime.Now.Ticks.ToString();
                        String fileExt = System.IO.Path.GetExtension(exampleInputFile.FileName);


                        exampleInputFile.SaveAs(Server.MapPath("Data" + ItemFilePath) + "\\" + datetime + fileExt);
                        string path = Server.MapPath("Data" + ItemFilePath) + "\\" + datetime + fileExt;

                        if (path != "")
                        {
                            ValidateData(path);
                            

                        }
                    }
                    else
                    {
                    }
                }
                else
                {

                }
            }
            catch (Exception)
            {

            }
            finally
            {
                exampleInputFile.Enabled = true;
                btnimport.Enabled = true;
            }
        }
    }
}

