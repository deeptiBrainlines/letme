﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using DAL;
using BE;
using System.Web.UI.DataVisualization.Charting;

namespace HealToZeal.Company
{
    public partial class Charts2 : System.Web.UI.Page
    {
        string result, result1;
        public static DataTable dtcount = new DataTable();
        EmployeeDAL objEmployeeDAL = new EmployeeDAL();
        EmployeeAssessmentDetailsBE objQuestionMasterBE1 = new EmployeeAssessmentDetailsBE();
        EmployeeAssessmentDetailsDAL ObjEmployeeAssessmentDetailsDAL = new EmployeeAssessmentDetailsDAL();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                Bindbatch();
                if (dtcount.Columns.Count == 0)
                {
                    dtcount.Columns.Add("EmpId");
                    dtcount.Columns.Add("Results");
                   
                }

            }

        }

      

        private void Bindbatch()
        {
            try
            {
                string CompanyId = "";
                CompanyId = Session["CompanyId"].ToString();
                DataTable Dtdbatch = objEmployeeDAL.GetBatchName(CompanyId);

                ddlbatch.DataSource = Dtdbatch;
                ddlbatch.DataTextField = "BatchName";
                ddlbatch.DataValueField = "BatchID_PK";
                ddlbatch.DataBind();
                ddlbatch.Items.Insert(0, new ListItem("--SelectBatch--", "--Select Batch--"));
           //     ddlbatch.Items.Insert(0, new System.Web.UI.WebControls.ListItem("--SelectBatch--", "--Select Batch--"));
                ddlbatch.SelectedIndex = 0;
            }
            catch
            {

            }
        }


        protected void ddlbatch_SelectedIndexChanged(object sender, EventArgs e)
        {
            
            string CompanyId = "";
            CompanyId = Session["CompanyId"].ToString();
            string batchid = ddlbatch.SelectedValue;
            Session["batch"] = batchid;
            DataTable dt = objEmployeeDAL.get_color(CompanyId, batchid);
            dt.Columns.Add("Resultant");
            // gridshow.DataSource = dt;
            // gridshow.DataBind();
            if (dt.Rows.Count > 0)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    //int Str=0, Moods=0,anxietys=0;
                  
                    int red = 0, green = 0, yellow = 0;
                    string compid = dt.Rows[i]["EmployeeCompanyId"].ToString();
                    string empid = dt.Rows[i]["EmployeeId_PK"].ToString();
                        string Stress = dt.Rows[i]["Stress"].ToString();
                    if (Stress == "#f4fb73")
                    {
                        red++;
                    }

                    if (Stress == "#e43c37")
                    {
                        yellow++;
                    }

                    if (Stress == "#88e46d")
                    {
                        green++;
                    }

                    string Mood = dt.Rows[i]["Mood"].ToString();
                    if (Mood == "#f4fb73")
                    {
                        red++;
                    }

                    if (Mood == "#e43c37")
                    {
                        yellow++;
                    }

                    if (Mood == "#88e46d")
                    {
                        green++;
                    }

                    string Anxiety = dt.Rows[i]["Anxiety"].ToString();
                    if (Anxiety == "#f4fb73")
                    {
                        red++;
                    }

                    if (Anxiety == "#e43c37")
                    {
                        yellow++;
                    }

                    if (Anxiety == "#88e46d")
                    {
                        green++;
                    }


                    result = yellow.ToString()+ "yellow" + "-"+ red.ToString()+"red" + "-" + green.ToString()+"green";
                    if (result == "0yellow-3red-0green")
                    {
                        result1 = "red";
                      

                    }
                    if (result == "0yellow-0red-3green")
                    {
                        result1 = "green";
                       
                    }

                    if (result == "3yellow-0red-0green")
                    {
                        result1 = "Amber2";
                        
                    }

                    if (result == "1yellow-0red-2green")
                    {
                        result1 = "green";
                        
                    }


                    if (result == "1yellow-2red-0green")
                    {
                        result1 = "red";
                       
                    }


                    if (result == "0yellow-2red-1green")
                    {
                        result1 = "red";
                        
                    }


                    if (result == "2yellow-0red-1green")
                    {
                        result1 = "Amber1";
                       
                    }

                    if (result == "2yellow-1red-0green")
                    {
                        result1 = "Amber2";
                       
                    }

                    if (result == "0yellow-1red-2green")
                    {
                        result1 = "green";
                       
                    }
                    if (result == "1yellow-1red-1green")
                    {
                        result1 = "Amber2";
                       
                    }
                    dtcount.Rows.Add(compid,result1);
                    if (dtcount.Rows.Count > 0)
                    {
                        objQuestionMasterBE1.percentagecompid = Convert.ToString(compid);
                        objQuestionMasterBE1.Color = Convert.ToString(result1);

                        objQuestionMasterBE1.batBatchId = ddlbatch.SelectedValue;
                        objQuestionMasterBE1.empid = empid;
                    }
                    int status = ObjEmployeeAssessmentDetailsDAL.countforpieInsert(objQuestionMasterBE1);
                }

            }

            DataTable DtEmployees = objEmployeeDAL.Percentageget(batchid);
            if (DtEmployees.Rows.Count > 0)
            {
                for (int j = 0; j < DtEmployees.Rows.Count; j++)
                {

                    string[] XPointMember = new string[DtEmployees.Rows.Count];
                    int[] YPointMember = new int[DtEmployees.Rows.Count];

                    for (int count = 0; count < DtEmployees.Rows.Count; count++)
                    {
                        //storing Values for X axis  
                        XPointMember[count] = DtEmployees.Rows[count]["Color"].ToString();
                        //storing values for Y Axis  
                        YPointMember[count] = Convert.ToInt32(DtEmployees.Rows[count]["percentages"]);

                    }
                    Chart1.Series[0].Points.DataBindXY(XPointMember, YPointMember);


                    foreach (Series charts in Chart1.Series)
                    {
                        foreach (DataPoint point in charts.Points)
                        {
                            switch (point.AxisLabel)
                            {
                                case "green": point.Color = System.Drawing.Color.LightGreen; break;
                                case "Amber1": point.Color = System.Drawing.Color.Yellow; break;
                                case "Amber2": point.Color = System.Drawing.Color.Orange; break;
                                case "red": point.Color = System.Drawing.Color.Red; break;
                                
                            }
                            point.Label = string.Format("{0:0} - {1}", point.YValues[0], point.AxisLabel);

                        }
                    }
                    //Setting width of line  
                    Chart1.Series[0].BorderWidth = 10;
                    //setting Chart type   
                    Chart1.Series[0].ChartType = SeriesChartType.Pie;

                    //Chart1.Series["Default"].Label = "#PERCENT";
                    //Chart1.Series["Default"].Points[0].Color = System.Drawing.Color.Yellow;
                    //Chart1.Series["Default"].Points[1].Color = System.Drawing.Color.Green;
                    //Chart1.Series["Default"].Points[2].Color = System.Drawing.Color.Red;


                    Chart1.Series["Default"].ChartType = SeriesChartType.Pie;
                    Chart1.Series["Default"]["PieLabelStyle"] = "enable";
                    Chart1.ChartAreas["ChartArea1"].Area3DStyle.Enable3D = false;
                    Chart1.Legends[0].Enabled = true;

                    //addded for onclick event
                    //Chart1.Series[0].Points.DataBindXY(xValues, yValues);
                    Chart1.Series[0].ChartType = SeriesChartType.Pie;
                    Chart1.ChartAreas["ChartArea1"].Area3DStyle.Enable3D = false;
                    Chart1.Legends[0].Enabled = true;

                    Chart1.Series[0].LegendUrl = "Charts2.aspx";
                    Chart1.Series[0].LabelUrl = "Charts2.aspx";
                    Chart1.Series[0].Url = "Charts2.aspx";

                    Chart1.Series[0].LegendPostBackValue = "#VALY-#VALX";
                    Chart1.Series[0].LabelPostBackValue = "#VALY-#VALX";
                    Chart1.Series[0].PostBackValue = "#VALY-#VALX";

                    Chart1.Series[0].LegendMapAreaAttributes = "target=\"_blank\"";
                    Chart1.Series[0].LabelMapAreaAttributes = "target=\"_blank\"";
                    Chart1.Series[0].MapAreaAttributes = "target=\"_blank\"";

                }


            }
            }

        protected void Chart2_Click(object sender, ImageMapEventArgs e)
        {

        }

        protected void Chart1_Click(object sender, ImageMapEventArgs e)
        {
            HttpContext.Current.Session["VAL"] = e.PostBackValue;
            Response.Redirect("ShowResult.aspx");
            
        }
    }
}