﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Company/Company.Master" AutoEventWireup="true" CodeBehind="Charts2.aspx.cs" Inherits="HealToZeal.Company.Charts2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="LeftColumn" runat="server">
    <div class="col-md-4">
    <asp:Label>Select Batch</asp:Label>
        <asp:DropDownList ID="ddlbatch" runat="server" OnSelectedIndexChanged="ddlbatch_SelectedIndexChanged" AutoPostBack="true">

            <asp:ListItem>---select---</asp:ListItem>
        </asp:DropDownList>
   
         </div>

     <asp:chart id="Chart1" runat="server" Height="300px" Width="400px" OnClick="Chart1_Click">
         
  <titles>
      
    <asp:Title ShadowOffset="3" Name="Title1" />
  </titles>
  <legends>
    <asp:Legend Alignment="Center" Docking="Bottom"
                IsTextAutoFit="False" Name="Default"
                LegendStyle="Row" />
  </legends>
  <series>
    <asp:Series Name="Default" />
  </series>
  <chartareas>
    <asp:ChartArea Name="ChartArea1"
                     BorderWidth="0" />
  </chartareas>
         
</asp:chart>
    <asp:GridView ID="gridshow" runat="server"></asp:GridView>

    <asp:Chart ID="Chart2" runat="server" OnClick="Chart2_Click">
        <Series>
            <asp:Series Name="Series1"></asp:Series>
        </Series>
        <ChartAreas>
            <asp:ChartArea Name="ChartArea1"></asp:ChartArea>
        </ChartAreas>
    </asp:Chart>

</asp:Content>
