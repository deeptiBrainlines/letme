﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using DAL;
using System.Web.Mail;
using System.Net.Mail;

namespace HealToZeal.Company
{
    public partial class EmployeeSummaryResult : System.Web.UI.Page
    {
        DataTable dt = new DataTable();
        protected void Page_Load(object sender, EventArgs e)
        {
           
            if (!Page.IsPostBack)
            {
                if (Session["CompanyId"] != null)
                {
                   
                    divalert.Visible = false;
                    divalert1.Visible = false;
                 //   BindAssessments();

                    BindEmployeeSummary();
                  
                }
               
            }
        }

        //private void BindAssessments()
        //{
        //    try
        //    {
        //        AssessmentDAL objAssessmentDAL=new AssessmentDAL();
        //        DataTable DtEmployeeAssessments = objAssessmentDAL.GetAssessmentsByEmployeeId(Request.QueryString["Id"].ToString());
        //        if (DtEmployeeAssessments != null && DtEmployeeAssessments.Rows.Count>0)
        //        {
        //            ddlAssessment.DataSource = DtEmployeeAssessments;
        //            ddlAssessment.DataTextField = "AssessmentName";
        //            ddlAssessment.DataValueField = "AssessmentId_FK";
        //            ddlAssessment.DataBind();
        //            //ddlAssessment.Items.Insert(0, new ListItem("--Select Assessment--", "--Select Assessment--"));
        //            //ddlAssessment.SelectedIndex = 0;
        //        }
        //    }
        //    catch 
        //    {
                
        //    }
        //}

        protected void ddlAssessment_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                ReportViewer1.LocalReport.Refresh();
                BindEmployeeSummary();
            }
            catch
            {
                
            }
        }

        private void BindEmployeeSummary()
        {
            try
            {
                EmployeeDAL objEmployeeDAL = new EmployeeDAL();
                CompanyDetailsDAL objCompanyDetailsDAL = new CompanyDetailsDAL();
           //     string EmployeeId = Request.QueryString["Id"].ToString();
                
                DataTable DtEmployee = objEmployeeDAL.EmployeeGetByEmployeeCompanyId(Session["CompanyId"].ToString(), Session["EmployeeId"].ToString());
                // DataTable DtEmployeeDetails = objEmployeeDAL.EmployeeAssessmentSubmittedGetByEmployeeId(EmployeeId));
                uemail1.Value = DtEmployee.Rows[0]["EmailId"].ToString();
                uemail2.Value = DtEmployee.Rows[0]["EmailId"].ToString();

                RptrEmployee.DataSource = DtEmployee;
                RptrEmployee.DataBind();

                //DataTable DtEmployeeDetails = objEmployeeDAL.EmployeeAssessmentSubmittedGetByEmployeeId(Convert.ToInt32(Session["EmployeeCompanyId"].ToString()));
                //RptrEmployee.DataSource = DtEmployeeDetails;
                //RptrEmployee.DataBind();

                //DataTable Dt = objEmployeeDAL.EmployeeAssessmentClusterwiseReport(EmployeeId);

               // DataTable Dt = objEmployeeDAL.EmployeeResultGet(EmployeeId,ddlAssessment.SelectedValue);
                
                //23rd Oct 15
                DataTable Dt = objCompanyDetailsDAL.EmployeeResultGetForCompany(Session["EmployeeId"].ToString(), Session["AssessmentInstanceId"].ToString(), Session["InstanceCompanyRelationId"].ToString(), Session["AssessmentId"].ToString());
               // DataTable Dt = objEmployeeDAL.EmployeeResultGetForEmployee(Session["EmployeeId"].ToString(), Session["AssessmentInstanceId"].ToString(), Session["InstanceCompanyRelationId"].ToString());
                if (Dt != null && Dt.Rows.Count > 0)
                {
                    divalert1.Visible = false;
                    PnlReport.Visible = true;
                    //count 

                    DataTable DtCount = new DataTable();

                    int CountYellow = (int)Dt.Compute("Count(Color)", "Color='#f4fb73'");
                    int CountRed = (int)Dt.Compute("Count(Color)", "Color='#e43c37'");
                    int CountGreen = (int)Dt.Compute("Count(Color)", "Color='#88e46d'");
                    //#88e46d
                    // show button
                    if (CountRed > 0)
                    {
                        //lblMessage.Text = "Improve your lifestyle. We suggest a repeat test in 3 months. We strongly recommend an immediate meeting with a professional counselor.";
                        // LblMessageUp.Text = "Improve your lifestyle.We suggest a repeat test in 3 months.We strongly recommend an immediate meeting with a professional counselor.";
                        lblMessage.Text = "Recommended Professional Help.";
                        LblMessageUp.Text = "Recommended Professional Help.";
                        divalert.Visible = true;
                        divalert2.Visible = true;
                        Ttle.Visible = false;
                        Ttle1.Visible = true;
                        Ttle2.Visible = true;
                        GvSuggestion.Visible = true;
                      //commented on 7 Jan 16  btnCounsel.Visible = true;
                        if (CountGreen > 0)
                        {
                            Ttle.Visible = true;
                        }
                    }
                    //else if (CountRed>1)
                    //{
                    //    lblMessage.Text = " Improve your lifestyle. We suggest a repeat test in 3 months. 'If you feel you need to talk to someone, set up a meeting with a professional counselor'";                
                    //}
                    else if ((CountYellow > 0) && (CountYellow == 1))
                    {
                        Ttle.Visible = false;
                        Ttle1.Visible = true;
                        Ttle2.Visible = true;
                        //p1.InnerText = "Improve your lifestyle.";
                        //p2.InnerText = " We suggest a repeat test in 3 months.";
                        //p3.InnerText = "If you feel you need to talk to someone, set up a meeting with a professional counselor";
                        lblMessage.Text = "Recommended Professional Help.";
                        LblMessageUp.Text = "Recommended Professional Help.";
                        //   lblMessage.Text = " Improve your lifestyle. We suggest a repeat test in 3 months. If you feel you need to talk to someone,";
                        // LblMessageUp.Text = "Improve your lifestyle. We suggest a repeat test in 3 months. If you feel you need to talk to someone,";
                        divalert.Visible = true;
                        divalert2.Visible = true;
                        GvSuggestion.Visible = true;
                       //commented on 7 jan 16 btnCounsel.Visible = true;
                        if (CountGreen > 0)
                        {
                            Ttle.Visible = true;
                        }
                    }
                    else if (CountYellow > 1)
                    {
                        //lblMessage.Text = "Improve your lifestyle. We suggest a repeat test in 3 months. We recommend meeting a professional counselor.";
                        //LblMessageUp.Text = "Improve your lifestyle. We suggest a repeat test in 3 months. We recommend meeting a professional counselor.";
                        lblMessage.Text = "Recommended Professional Help.";
                        LblMessageUp.Text = "Recommended Professional Help.";
                        divalert.Visible = true;
                        divalert2.Visible = true;
                        Ttle.Visible = false;
                        Ttle1.Visible = true;
                        Ttle2.Visible = true;
                        GvSuggestion.Visible = true;
                      //commented on 7 jan 16  btnCounsel.Visible = true;
                        if (CountGreen > 0)
                        {
                            Ttle.Visible = true;
                        }
                    }
                    else if (CountGreen>0 )
                    {
                        Ttle.Visible = true;
                        Ttle1.Visible = false;
                        Ttle2.Visible = false;
                        divalert.Visible = false;
                        divalert2.Visible = false;
                        GvSuggestion.Visible = false;
                       //commented on 7 jan 16 btnCounsel.Visible = false;
                    }
                    else
                    {
                        divalert.Visible = false;
                        divalert2.Visible = false;
                        Ttle1.Visible = false;
                        Ttle2.Visible = false;
                        GvSuggestion.Visible = false;
                      //commented on 7 jan 16  btnCounsel.Visible = false;
                    }


                   // //display positive assets negative factors

                   // DataTable DtPositive = new DataTable();//Dt;
                   // DataRow[] DrowPositive = Dt.Select(" IsPositive='Y'", "");

                   // if (DrowPositive.Length > 0)
                   // {
                   //     DtPositive = Dt.Select(" IsPositive='Y'", "").CopyToDataTable();
                   //     if (DtPositive != null)
                   //     {
                   //         if (DtPositive.Rows.Count > 0)
                   //         {
                   //        //     Ttle.Visible = true;
                   //             GvPositive.DataSource = DtPositive;
                   //             GvPositive.DataBind();
                   //         }
                   //         else
                   //         {
                   //             // Ttle.Visible = false;
                   //         }
                   //     }
                   //     else
                   //     {
                   //         // Ttle.Visible = false;
                   //     }
                   // }
                   // else
                   // {
                   //     GvPositive.DataSource = null;
                   //     GvPositive.DataBind();
                   ////     Ttle.Visible = false;
                   // }

                   // DataRow[] DrowNegative = Dt.Select(" IsPositive='N'", "");

                   // if (DrowNegative.Length > 0)
                   // {
                   //     DtPositive = Dt.Select(" IsPositive='N'", "").CopyToDataTable();

                   //     if (DtPositive != null)
                   //     {
                   //         if (DtPositive.Rows.Count > 0)
                   //         {
                   //         //     Ttle1.Visible = true;
                   //             GvResult.DataSource = DtPositive;
                   //             GvResult.DataBind();
                   //         }
                   //         else
                   //         {
                   //             //Ttle1.Visible = false;
                   //         }
                   //     }
                   //     else
                   //     {
                   //         //Ttle1.Visible = false;
                   //     }
                   // }
                   // else
                   // {
                   //     GvResult.DataSource = null;
                   //     GvResult.DataBind();
                   //     //Ttle1.Visible = false;
                   // }

                    DataTable DtPositive = new DataTable();//Dt;
                    DataRow[] DrowPositive = Dt.Select( "Color='#88e46d'", "");
                    if (DrowPositive.Length > 0)
                    {
                        DtPositive = Dt.Select("Color='#88e46d'", "").CopyToDataTable();
                        if (DtPositive != null && DtPositive.Rows.Count > 0)
                        {
                            GvPositive.DataSource = DtPositive;
                            GvPositive.DataBind();
                        }
                        else
                        {
                            GvPositive.DataSource = null;
                            GvPositive.DataBind();
                        }
                    }
                    else
                    {
                        GvPositive.DataSource = null;
                        GvPositive.DataBind();
                    }
                    DataRow[] DrowNegative = Dt.Select("Color='#e43c37' Or Color='#f4fb73'", "");
                    if (DrowNegative.Length > 0)
                    {
                        DtPositive = Dt.Select("(Color='#e43c37' Or Color='#f4fb73') and (Ispositive='N')","").CopyToDataTable();
                        if (DtPositive != null && DtPositive.Rows.Count > 0)
                        {
                            GvResult.DataSource = DtPositive;
                            GvResult.DataBind();
                        }
                        else
                        {
                            GvResult.DataSource = null;
                            GvResult.DataBind();
                        }
                    }
                    else
                    {
                        GvResult.DataSource = null;
                        GvResult.DataBind();
                    }

                    DataTable DtSuggestions = objEmployeeDAL.SuggestionMasterGetRandom();
                    DataView view = new DataView(DtSuggestions);
                    DataTable distinctValues = view.ToTable(true, "Title");

                    if (distinctValues != null)
                    {
                        if (distinctValues.Rows.Count > 0)
                        {
                            // Ttle2.Visible = true;
                            GvSuggestion.DataSource = distinctValues; //DtSuggestions;
                            GvSuggestion.DataBind();
                        }
                        else
                        {
                            //Ttle2.Visible = false;
                        }
                    }
                    else
                    {
                        // Ttle2.Visible = false;
                    }

                    for (int a = 0; a < GvSuggestion.Rows.Count; a++)
                    {
                        Label lblSuggestionTitle = GvSuggestion.Rows[a].FindControl("lblSuggestionTitle") as Label;
                        if (lblSuggestionTitle != null)
                        {
                            DataTable DtSuggestionText = DtSuggestions.Select("Title='" + lblSuggestionTitle.Text + "'").CopyToDataTable();
                            GridView Gdv = (GridView)GvSuggestion.Rows[a].FindControl("GdvSuggestionText");
                            Gdv.DataSource = DtSuggestionText;
                            Gdv.DataBind();
                        }
                    }
                }
                else
                {
                    PnlReport.Visible = false;
                    divalert1.Visible = true;
                    lblMessage1.Text = "No report found for this user...";
                }

                addColumns();
                DataTable DtClusterPercentage = new DataTable();
                EmployeeAssessmentsHistoryDAL objEmployeeDAL1 = new EmployeeAssessmentsHistoryDAL();
                DtClusterPercentage = objEmployeeDAL1.EmployeeHistory(Session["EmployeeId"].ToString());

                for (int i = 0; i < DtClusterPercentage.Rows.Count; i++)
                {

                    dt.Rows.Add(DtClusterPercentage.Rows[i]["EmployeeCompanyId"],
                        DtClusterPercentage.Rows[i]["AssessmentName"],
                   DtClusterPercentage.Rows[i]["AssessmentId_FK"],
                   DtClusterPercentage.Rows[i]["num"],
                   DtClusterPercentage.Rows[i]["ClusterName"],
                   DtClusterPercentage.Rows[i]["ClusterScore"],
                   DtClusterPercentage.Rows[i]["CreatedDate"], DtClusterPercentage.Rows[i]["EmployeeName"],
                   DtClusterPercentage.Rows[i]["ClusterPercentage"],
                   DtClusterPercentage.Rows[i]["Color"],
                   DtClusterPercentage.Rows[i]["IsPositive"]);

                    ReportViewer1.Visible = true;

                    ReportViewer1.LocalReport.DataSources.Clear();

                    ReportViewer1.LocalReport.DataSources.Add(new Microsoft.Reporting.WebForms.ReportDataSource("DataSet1", dt));

                }

    


    }


            catch(Exception e)
            {
                e.InnerException.ToString();
            }
        }
        public void addColumns()
        {
            dt.Columns.Add("EmployeeCompanyId", typeof(string));
            dt.Columns.Add("AssessmentName", typeof(string));
            dt.Columns.Add("AssessmentId_FK", typeof(string));
            dt.Columns.Add("num", typeof(int));
            dt.Columns.Add("ClusterName", typeof(string));
            dt.Columns.Add("ClusterScore", typeof(string));
            dt.Columns.Add("CreatedDate", typeof(DateTime));
            dt.Columns.Add("EmployeeName", typeof(string));
            dt.Columns.Add("ClusterPercentage", typeof(double));
            dt.Columns.Add("Color", typeof(string));
            dt.Columns.Add("IsPositive", typeof(char));


        }
        protected void btnCounsel_Click(object sender, EventArgs e)
        {
            try
            {
                DivSetUpMeeting.Visible = true;
                DivEmail.Visible = true;
                DivMailBody.Visible = false;
                DivTitle.Visible = false;
                EmployeeDAL objEmployeeDAL = new EmployeeDAL();
                DataTable DtEmployeeDetails = objEmployeeDAL.EmployeeAssessmentSubmittedGetByEmployeeId(Session["EmployeeId_FK"].ToString());
                uemail.Value = DtEmployeeDetails.Rows[0]["EmailId"].ToString();
            }
            catch (Exception )
            {

            }
        }

        protected void BtnSetUpMeeting_Click(object sender, EventArgs e)
        {
            try
            {
                DivTitle.Focus();
                DivMailBody.Visible = true;
                DivTitle.Visible = true;

                DivEmail.Visible = false;
                DivSetUpMeeting.Visible = false;

                EmployeeDAL objEmployeeDAL = new EmployeeDAL();
                DataTable DtEmployeeDetails = objEmployeeDAL.EmployeeAssessmentSubmittedGetByEmployeeId(Session["EmployeeId_FK"].ToString());
                uemail.Value = DtEmployeeDetails.Rows[0]["EmailId"].ToString();
            }
            catch (Exception )
            {

            }
        }

        protected void BtnSendEmail_Click(object sender, EventArgs e)
        {
            try
            {
                System.Net.Mail.MailMessage mail = new System.Net.Mail.MailMessage();
                // System.Web.Mail.MailMessage mail = new System.Web.Mail.MailMessage();
                if (uemail1.Value != "")
                {
                    mail.To.Add(uemail1.Value); 
                }
                else if (uemail2.Value != "")
                {
                    mail.To.Add(uemail2.Value); 
                }
                
                mail.Bcc.Add("priyamvada.bavadekar@letmetalk2.me");
                // mail.From = "contact@letmetalk2.me";
               //mail.From = new MailAddress("contact@letmetalk2.me", "Let me talk to me", System.Text.Encoding.UTF8);
                mail.Subject = "User want to contact for an appointment.";
                string getDt = DateTime.Now.Date.AddDays(7).ToString("MM/dd/yyyy");
                if (message.Value != "")
                {
                    mail.Body = message.Value;
                }
                else if (message1.Value != "")
                {
                    mail.Body = message1.Value;
                }
               
                mail.IsBodyHtml = true;

                mail.Priority = System.Net.Mail.MailPriority.High;
                SmtpClient client = new SmtpClient();
                //client.Credentials = new System.Net.NetworkCredential("contact@letmetalk2.me", "contact123");
                //client.Port = 587;
                //client.Host = "hazel.arvixe.com";
                client.EnableSsl = false;

                client.Send(mail);

                //System.Web.Mail.MailMessage mail = new System.Web.Mail.MailMessage();
                //mail.From = "HealToZeal@chetanatspl.com";//uemail.Value;
                //if (uemail1.Value != "")
                //{
                //    mail.To = uemail1.Value; //"prajakta.mulay@chetanasystems.com";
                //}
                //else if (uemail2.Value != "")
                //{
                //    mail.To = uemail2.Value; //"prajakta.mulay@chetanasystems.com";
                //}

                //mail.Subject = "User want to contact for an appointment.";
                //if (message.Value != "")
                //{
                //    mail.Body = message.Value;
                //}
                //else if (message1.Value != "")
                //{
                //    mail.Body = message1.Value;
                //}

                //mail.BodyFormat = MailFormat.Html;
                //mail.Fields.Add("http://schemas.microsoft.com/cdo/configuration/smtpauthenticate", "1");    //basic authentication
                //mail.Fields.Add("http://schemas.microsoft.com/cdo/configuration/sendusername", "HealToZeal@chetanatspl.com"); //set your username here
                //mail.Fields.Add("http://schemas.microsoft.com/cdo/configuration/sendpassword", "HealToZeal");    //set your password here
                //System.Web.Mail.SmtpMail.SmtpServer = "holly.arvixe.com";  //your real server goes here                        
                //System.Web.Mail.SmtpMail.Send(mail);
                //DivEmail.Visible = false;
                //DivSetUpMeeting.Visible = false;
                //DivMailBody.Visible = false;
                //DivTitle.Visible = false;
                // ScriptManager.RegisterClientScriptBlock(this.Page, GetType(), "openColorBox", "<script> alert('We have received your meeting request. We will get back to you very soon'); </script>;", true);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
            
    }
}