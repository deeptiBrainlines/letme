﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ReportChart.aspx.cs" Inherits="HealToZeal.Company.ReportChart" %>

<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=11.0.0.0, Culture=neutral, PublicKeyToken=89845dcd8080cc91" Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
             <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
    <div Width="767px">
        <rsweb:ReportViewer ID="rptChart" runat="server" Font-Names="Verdana" Font-Size="8pt" WaitMessageFont-Names="Verdana" WaitMessageFont-Size="14pt" Width="767px">
            <localreport reportpath="Reports\Report3.rdlc">
                <datasources>
                    <rsweb:ReportDataSource DataSourceId="ObjectDataSource1" Name="DataSet1" />
                </datasources>
            </localreport>
        </rsweb:ReportViewer>
        <asp:ObjectDataSource ID="ObjectDataSource1" runat="server" OldValuesParameterFormatString="original_{0}" SelectMethod="GetData" TypeName="HealToZeal.DataSet1TableAdapters.SpEmployeeAssessmentResultTableAdapter"></asp:ObjectDataSource>
    </div>
    </form>
</body>
</html>
