﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using DAL;
using HealToZeal.Company;
using System.IO;

namespace HealToZeal.Company
{
    public partial class ShowResult : System.Web.UI.Page
    {
        EmployeeDAL objEmployeeDAL = new EmployeeDAL();
        protected void Page_Load(object sender, EventArgs e)
        {
            string batchid = Session["batch"].ToString();
           
           
            string session = HttpContext.Current.Session["VAL"].ToString();
            String[] Values = Session["VAL"].ToString().Split(new Char[] { '-' }, StringSplitOptions.RemoveEmptyEntries);
            lblcolor.Text = Values[1];
            string color = lblcolor.Text;
            DataTable Dtgetresult = objEmployeeDAL.ResultGetBybatchwise(color,batchid);
            lblcompanyid.Text = Dtgetresult.Rows[0]["CompanyName"].ToString();
            Lblbatch.Text = Dtgetresult.Rows[0]["BatchName"].ToString();
            gridresult.DataSource = Dtgetresult;
            gridresult.DataBind();

            DataTable dtnotready= objEmployeeDAL.NotReady_toShare_Details(color, batchid);
            lblundisclosed.Text = dtnotready.Rows[0]["undisclosed_details"].ToString();
        }


        protected void gridresult_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            
            gridresult.PageIndex = e.NewPageIndex;
            gridresult.DataBind();
        }

        protected void ExporttoExcel_Click(object sender, EventArgs e)
        {

            Response.Clear();
            Response.Buffer = true;
            string FileName = Lblbatch.Text + DateTime.Now + ".xls";
            Response.AddHeader("content-disposition", "attachment;filename="+FileName+"");
            Response.Charset = "";
            Response.ContentType = "application/vnd.ms-excel";
            using (StringWriter sw = new StringWriter())
            {
                HtmlTextWriter hw = new HtmlTextWriter(sw);

                //To Export all pages
                gridresult.AllowPaging = false;
             //   this.BindGrid();

                gridresult.HeaderRow.BackColor = System.Drawing.Color.White;
                foreach (TableCell cell in gridresult.HeaderRow.Cells)
                {
                    cell.BackColor = gridresult.HeaderStyle.BackColor;
                }
                foreach (GridViewRow row in gridresult.Rows)
                {
                    row.BackColor = System.Drawing.Color.White;
                    foreach (TableCell cell in row.Cells)
                    {
                        if (row.RowIndex % 2 == 0)
                        {
                            cell.BackColor = gridresult.AlternatingRowStyle.BackColor;
                        }
                        else
                        {
                            cell.BackColor = gridresult.RowStyle.BackColor;
                        }
                        cell.CssClass = "textmode";
                    }
                }

                gridresult.RenderControl(hw);

                //style to format numbers to string
                string style = @"<style> .textmode { } </style>";
                Response.Write(style);
                Response.Output.Write(sw.ToString());
                Response.Flush();
                Response.End();
            }

        }
        //    DataTable Dtgetresult=objEmployeeDAL.ResultGetBybatchwise(batchid,color);

        public override void VerifyRenderingInServerForm(Control control)
        {
            /* Verifies that the control is rendered */
        }
    }
}