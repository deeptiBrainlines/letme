﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DAL;
using BE;
using System.Data;
using System.IO;
using iTextSharp.text.pdf;
using iTextSharp.text.html.simpleparser;
using System.Configuration;
using System.Text;
using iTextSharp.text;
using System.Data.SqlClient;
//using System.Windows.Forms;

namespace HealToZeal.Company
{
    public partial class EmployeeResultNew : System.Web.UI.Page
    {
        EmployeeDAL objEmployeeDAL = new EmployeeDAL();
        CompanyDetailsDAL ObjCompanyDetailsDAL = new CompanyDetailsDAL();
        EmployeeAssessmentDetailsBE objQuestionMasterBE1 = new EmployeeAssessmentDetailsBE();
        EmployeeAssessmentDetailsDAL ObjEmployeeAssessmentDetailsDAL = new EmployeeAssessmentDetailsDAL();
        public static DataTable dtresult = new DataTable();
        public static DataTable dtclusters = new DataTable();
        public static DataTable dtclustersS = new DataTable();
        public static DataTable dtclustersM = new DataTable();
        public static DataTable dtclustersO = new DataTable();
        int reds = 0, green = 0, Amber2 = 0, Amber1 = 0;
        int count = 0;
        protected void Page_Load(object sender, EventArgs e)
        {

            if (!IsPostBack)
            {

                Bindbatch();
               
            }

      
        

    }

        private void Bindbatch()
        {
            try
            {
                string CompanyId = "";
                CompanyId = Session["CompanyId"].ToString();
                DataTable Dtdbatch = objEmployeeDAL.GetBatchName(CompanyId);

                ddlbatch.DataSource = Dtdbatch;
                ddlbatch.DataTextField = "BatchName";
                ddlbatch.DataValueField = "BatchID_PK";
                ddlbatch.DataBind();

                ddlbatch.Items.Insert(0, new System.Web.UI.WebControls.ListItem("--SelectBatch--", "--Select Batch--"));
                ddlbatch.SelectedIndex = 0;
            }
            catch
            {

            }
        }

        protected void ddlbatch_SelectedIndexChanged(object sender, EventArgs e)
        {
            //if (!IsPostBack) { 

            GvAssessments.Visible = true;
            bindgrid();
           // percentages();
            
       // }

        }

        private void bindgrid()
        {
            string batchid = ddlbatch.SelectedValue;
            dtresult = new DataTable();
            EmployeeDAL objEmployeeDAL = new EmployeeDAL();
            string CompanyId = Session["CompanyId"].ToString();
            DataTable DtEmployees = objEmployeeDAL.EmployeesGetByCompanyIdbatchwise(batchid);
            Session["DtEmployees"] = null;
            Session["DtEmployees"] = DtEmployees;
            DataTable dtEmployess = new DataTable();
            dtEmployess = Session["DtEmployees"] as DataTable;
            //dtEmployess = DtEmployees;
            DataTable dtclusterresult = new DataTable();

            decimal d = 0;
            decimal per = 0;
            decimal d1 = 0;
            decimal d4 = 0;
            decimal d5 = 0;
            decimal d6 = 0;
           // decimal d7 = 0;
           //decimal d8 = 0;
           // decimal d9 = 0;
           // decimal d10 = 0;
           // decimal d71 = 0;
           // decimal d81 = 0;
           // decimal d91= 0;
           // decimal d101 = 0;
            if (dtresult.Columns.Count == 0)
            {
               // dtresult.Columns.Add("EmployeeID_PK");
                dtresult.Columns.Add("Sr No");
                dtresult.Columns.Add("Self Assurance");
                dtresult.Columns.Add("Emotional Balance");
                dtresult.Columns.Add("Ability to Withstand Pressure");

                dtresult.Columns.Add("Lack of Motivation");
                dtresult.Columns.Add("Coping Issues");
                dtresult.Columns.Add("Self Esteem Issues");
            }


            if (dtclusters.Columns.Count == 0)
            {
                dtclusters.Columns.Add("EmployeeID_PK");
                dtclusters.Columns.Add("ClusterId");
                dtclusters.Columns.Add("Score");
                dtclusters.Columns.Add("totalcount");
                //dtclusters.Columns.Add("Mood");
                //dtclusters.Columns.Add("Others");

            }


          
            //for (int j = 0; j < 9; j++)
            //{
            for (int i = 0; i < dtEmployess.Rows.Count; i++)
            {
                //if (i < 27 || i >27 )
                //{
                DataTable dt = ObjCompanyDetailsDAL.GetEmployeeResultNewClusterwise(dtEmployess.Rows[i]["EmployeeID_PK"].ToString(), "Anxiety");
                //Comment line below as per Priyamada mam discussion
                //DataTable dt = ObjCompanyDetailsDAL.GetEmployeeResultNew(dtEmployess.Rows[i]["EmployeeID_PK"].ToString(), "W");
                if (dt != null && dt.Rows.Count > 0)
                {
                    if (string.IsNullOrEmpty(dt.Rows[0]["Score"].ToString()))
                    {

                    }
                    else
                    {
                        d = Math.Round(Convert.ToDecimal(((Convert.ToDouble(dt.Rows[0]["Score"]) * 100) / (Convert.ToDouble(dt.Rows[0]["totalcount"])))), 0);

                        // d = Math.Round(Convert.ToDecimal(((Convert.ToDouble(dt.Rows[0]["Score"]) * 100) / 4)), 0);
                        //   d = Math.Round(Convert.ToDecimal(((Convert.ToDouble(dt.Rows[0]["Score"]) * 100) / 46)), 0);
                    }
                }

                DataTable dt1 = ObjCompanyDetailsDAL.GetEmployeeResultNewClusterwise(dtEmployess.Rows[i]["EmployeeID_PK"].ToString(), "Mood");

                //DataTable dt1 = ObjCompanyDetailsDAL.GetEmployeeResultNew(dtEmployess.Rows[i]["EmployeeID_PK"].ToString(), "M");
                if (dt1 != null && dt1.Rows.Count > 0)
                {
                    if (string.IsNullOrEmpty(dt1.Rows[0]["Score"].ToString()))
                    {

                    }
                    else
                    {
                        per = Math.Round(((Convert.ToDecimal(dt1.Rows[0]["Score"]) * 100) / (Convert.ToDecimal(dt1.Rows[0]["totalcount"]))), 0);
                        // per = Math.Round(((Convert.ToDecimal(dt1.Rows[0]["Score"]) * 100) / 2), 0);
                        //per = Math.Round(((Convert.ToDecimal(dt1.Rows[0]["Score"]) * 100) / 26), 0); //24
                    }
                }

                DataTable dt2 = ObjCompanyDetailsDAL.GetEmployeeResultNewClusterwise(dtEmployess.Rows[i]["EmployeeID_PK"].ToString(), "Stress");
                //DataTable dt2 = ObjCompanyDetailsDAL.GetEmployeeResultNew(dtEmployess.Rows[i]["EmployeeID_PK"].ToString(), "S");
                if (dt2 != null && dt2.Rows.Count > 0)
                {
                    if (string.IsNullOrEmpty(dt2.Rows[0]["Score"].ToString()))
                    {

                    }
                    else

                    {

                        d1 = Math.Round(Convert.ToDecimal(((Convert.ToDouble(dt2.Rows[0]["Score"]) * 100) / (Convert.ToDouble(dt2.Rows[0]["totalcount"])))), 0);
                        //   d1 = Math.Round(Convert.ToDecimal(((Convert.ToDouble(dt2.Rows[0]["Score"]) * 100) / 4)), 0);
                        //d1 = Math.Round(Convert.ToDecimal(((Convert.ToDouble(dt2.Rows[0]["Score"]) * 100) / 26)), 0);//22
                    }

                }
                

                DataTable dtMotivation = ObjCompanyDetailsDAL.GetEmployeeResultNewClusterwise(dtEmployess.Rows[i]["EmployeeID_PK"].ToString(), "motivation");
                if (dtMotivation != null && dtMotivation.Rows.Count > 0)
                {
                    if (string.IsNullOrEmpty(dtMotivation.Rows[0]["Score"].ToString()))
                    {

                    }
                    else
                    {
                        d4 = Math.Round(((Convert.ToDecimal(dtMotivation.Rows[0]["Score"]) * 10) / Convert.ToDecimal(dtMotivation.Rows[0]["totalcount"])), 0); //26
                                                                                                                                                               //  d4 = Math.Round(((Convert.ToDecimal(dtMotivation.Rows[0]["Score"]) * 10) / 30), 0); //26
                    }
                }

                DataTable dtCoping = ObjCompanyDetailsDAL.GetEmployeeResultNewClusterwise((dtEmployess.Rows[i]["EmployeeID_PK"].ToString()), "Coping Ability");
                if (dtCoping != null && dtCoping.Rows.Count > 0)
                {
                    if (string.IsNullOrEmpty(dtCoping.Rows[0]["Score"].ToString()))
                    {

                    }
                    else
                    {
                        d5 = Math.Round(((Convert.ToDecimal(dtCoping.Rows[0]["Score"]) * 10) / (Convert.ToDecimal(dtCoping.Rows[0]["totalcount"]))), 0); //56 70
                                                                                                                                                         // d5 = Math.Round(((Convert.ToDecimal(dtCoping.Rows[0]["Score"]) * 10) / 70), 0); //56 70
                    }
                }

                DataTable dtselfesteem = ObjCompanyDetailsDAL.GetEmployeeResultNewClusterwise(dtEmployess.Rows[i]["EmployeeID_PK"].ToString(), "Self Esteem");
                if (dtselfesteem != null && dtselfesteem.Rows.Count > 0)
                {
                    if (string.IsNullOrEmpty(dtselfesteem.Rows[0]["Score"].ToString()))
                    {

                    }
                    else

                    {
                        d6 = Math.Round((Convert.ToDecimal(Convert.ToDouble(dtselfesteem.Rows[0]["Score"]) * 10) / (Convert.ToDecimal(dtselfesteem.Rows[0]["totalcount"]))), 0); //28
                                                                                                                                                                                 // d6 = Math.Round((Convert.ToDecimal(Convert.ToDouble(dtselfesteem.Rows[0]["Score"]) * 10) / 28), 0); //28
                    }
                }
               
                dtresult.Rows.Add(dtEmployess.Rows[i]["EmployeeCompanyId"].ToString(), d, per, d1, d4, d5, d6);

               
              //  by meenakshi inserting in to EmployeeClusterScoreDetails
                DataTable getclusters = ObjCompanyDetailsDAL.Getclsuterscore_byemployee(dtEmployess.Rows[i]["EmployeeID_PK"].ToString(),dtEmployess.Rows[i]["CompanyId_FK"].ToString());
                if (getclusters.Rows.Count > 0)
                {
                    for (int j = 0; j < getclusters.Rows.Count; j++)
                    {

                        objQuestionMasterBE1.propClusterID = getclusters.Rows[j]["ClusterId"].ToString();

                        objQuestionMasterBE1.propClusterScore = getclusters.Rows[j]["Score"].ToString();
                        decimal score = Convert.ToInt32(getclusters.Rows[j]["Score"].ToString());
                      decimal totalcount = Convert.ToInt32((getclusters.Rows[j]["totalcount"].ToString()));

                      //  Math.Round(Convert.ToDecimal(((Convert.ToDouble(dt.Rows[0]["Score"]) * 100) / (Convert.ToDouble(dt.Rows[0]["totalcount"])))), 0)

                        //DataTable dtAssessment = objAnswerMasterDAL.GetAssessmentID((dtData.Rows[rownumber]["Assessment"].ToString()));
                        //  objQuestionMasterBE1.propClustertotalcount = getclusters.Rows[j]["totalcount"].ToString();
                         objQuestionMasterBE1.propPercentage = Math.Round((score * 100) / totalcount);
                        // objQuestionMasterBE1.propCategory= getclusters.Rows[j]["Category"].ToString();
                        // objQuestionMasterBE1.propAssessmentId_FK = "BD293E6B-12D2-4F88-A360-6D274E6FC1D4";
                        objQuestionMasterBE1.propAssessmentId_FK = getclusters.Rows[j]["AssessmentId_FK"].ToString(); ; //getclusters.Rows[0]["AssessmentId_FK"].ToString();
                        objQuestionMasterBE1.propEmployeeId_FK = getclusters.Rows[j]["EmployeeId_FK"].ToString();
                        objQuestionMasterBE1.propCreatedBy = getclusters.Rows[j]["EmployeeId_FK"].ToString();
                        objQuestionMasterBE1.batchid = ddlbatch.SelectedValue;
                        // objQuestionMasterBE1.propAssessmentInstanceId_FK = "EAE3AC8D-B39A-41EB-BDB0-6FA4617D2161";
                        //  objQuestionMasterBE1.propAssessmentInstanceId_FK = "3005FED7-B5D3-4784-A3F6-52860304EE1E";
                        //   objQuestionMasterBE1.propInstanceCompanyRelationId_FK = "F4B11269-D4FF-4C28-9295-11C92CEB6F4D";
                        //   objQuestionMasterBE1.propInstanceCompanyRelationId_FK = "F4743A54-2D60-4E71-973A-492F07ECF98E";
                        int status = ObjEmployeeAssessmentDetailsDAL.EmployeeAssessmentDetailsImportClusterWise(objQuestionMasterBE1);
                    }
                }
                ddlbatch.DataBind();

            }

            GvAssessments.DataSource = dtresult;
            GvAssessments.DataBind();

        }




        protected void GvAssessments_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            // GvAssessments.DataSource = dtresult;
            // GvAssessments.DataBind();
            int red = 0, yellow = 0, Green=0; 
            Label lblSelfAssurance = (Label)e.Row.FindControl("lblAssessmentInstanceId");
            Label lblEmotinalBalance = (Label)e.Row.FindControl("lblInstanceCompanyRelationId");
            Label lblPressure = (Label)e.Row.FindControl("lblEmployeeId");

            Label lbmotivation = (Label)e.Row.FindControl("lblAssessmentInstanceId1");
            Label lblCoping = (Label)e.Row.FindControl("lblInstanceCompanyRelationId1");
            Label lblselfesteem = (Label)e.Row.FindControl("lblEmployeeId1");
             Label empCompId = (Label)e.Row.FindControl("lblAssessmentId");
            DataTable dt = new DataTable();
            if (dt.Columns.Count == 0)
            {
                // dtresult.Columns.Add("EmployeeID_PK");
                dt.Columns.Add("EMpid");
                dt.Columns.Add("Color");

            }
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                string compid = empCompId.Text;
                //TableCell cell0 = e.Row.Cells[0];
                TableCell cell = e.Row.Cells[1];
                TableCell cell1 = e.Row.Cells[2];
                TableCell cell2 = e.Row.Cells[3];
                TableCell cell3 = e.Row.Cells[4];
                TableCell cell4 = e.Row.Cells[5];
                TableCell cell5 = e.Row.Cells[6];
                TableCell cell7 = e.Row.Cells[7];
                TableCell cell8 = e.Row.Cells[8];
                if (e.Row.RowIndex >= 0)
                {
                   
                   
                    if (Convert.ToInt32(lblSelfAssurance.Text) < 35)
                    {
                        cell.BackColor = System.Drawing.Color.LightGreen;
                       // count = count + 1;
                       // lblAssessmentId1.Text = count.ToString();
                       
                    }

                    if (Convert.ToInt32(lblEmotinalBalance.Text) < 35)
                    {
                        cell1.BackColor = System.Drawing.Color.LightGreen;

                    }
                    if (Convert.ToInt32(lblPressure.Text) <= 40)
                    {
                        cell2.BackColor = System.Drawing.Color.LightGreen;

                    }

                    if ((Convert.ToInt32(lblSelfAssurance.Text) >= 35) && Convert.ToInt32(lblSelfAssurance.Text) < 65)
                    {
                        cell.BackColor = System.Drawing.Color.Yellow;

                    }
                    if (Convert.ToInt32(lblSelfAssurance.Text) >= 65)
                    {
                        cell.BackColor = System.Drawing.Color.Red;

                    }
                    //Emotional Balance

                    if ((Convert.ToInt32(lblEmotinalBalance.Text) >= 35) && Convert.ToInt32(lblEmotinalBalance.Text) < 65)
                    {
                        cell1.BackColor = System.Drawing.Color.Yellow;

                    }
                    if (Convert.ToInt32(lblEmotinalBalance.Text) >= 65)
                    {
                        cell1.BackColor = System.Drawing.Color.Red;

                    }
                    //Pressure
                    if ((Convert.ToInt32(lblPressure.Text) >= 41) && Convert.ToInt32(lblPressure.Text) <= 69)
                    {
                        cell2.BackColor = System.Drawing.Color.Yellow;

                    }
                    if (Convert.ToInt32(lblPressure.Text) >= 70)
                    {
                        cell2.BackColor = System.Drawing.Color.Red;

                    }

                    if (Convert.ToInt32(lbmotivation.Text) < 5)// || Convert.ToInt32(lblselfesteem.Text)>5 || Convert.ToInt32(lblCoping.Text)>0)
                    {
                        cell3.BackColor = System.Drawing.Color.LightGreen;

                    }
                    else
                    {
                        cell3.BackColor = System.Drawing.Color.Yellow;

                    }

                    if (Convert.ToInt32(lblselfesteem.Text) < 5)
                    {
                        cell5.BackColor = System.Drawing.Color.LightGreen;
                    }
                    else
                    {
                        cell5.BackColor = System.Drawing.Color.Yellow;

                    }

                    if (Convert.ToInt32(lblCoping.Text) < 5)
                    {
                        cell4.BackColor = System.Drawing.Color.LightGreen;
                    }
                    else
                    {
                        cell4.BackColor = System.Drawing.Color.Yellow;
                    }


                }
               
                int i1 = 0;
               //get count of colors
                
                foreach (TableCell cell12 in e.Row.Cells)
                {
                     if (i1 <= 3) { 

                    if (cell12.BackColor == System.Drawing.Color.Red)
                        red++;
                    if (cell12.BackColor == System.Drawing.Color.Yellow)
                        yellow++;
                    if (cell12.BackColor == System.Drawing.Color.LightGreen)
                        Green++;
                        i1++;
                    }
                }
           

                cell7.Text= yellow.ToString()+"yellow"+ "-"+red.ToString()+"red"+"-"+ Green.ToString()+"green";
                
              //calculating for combination
                if (cell7.Text=="0yellow-3red-0green")
                {
                    cell8.Text ="red";
                    cell8.BackColor = System.Drawing.Color.Red;
                    reds++;

                } 
                if(cell7.Text== "0yellow-0red-3green")
                {
                    cell8.Text = "green";
                    cell8.BackColor = System.Drawing.Color.LightGreen;
                    green++;
                }

                if (cell7.Text == "3yellow-0red-0green")
                {
                    cell8.Text = "Amber2";
                    cell8.BackColor = System.Drawing.Color.YellowGreen;
                    Amber2++;
                }

                if (cell7.Text == "1yellow-0red-2green")
                {
                    cell8.Text = "green";
                    cell8.BackColor = System.Drawing.Color.LightGreen;
                    green++;
                }


                if (cell7.Text == "1yellow-2red-0green")
                {
                    cell8.Text = "red";
                    cell8.BackColor = System.Drawing.Color.Red;
                    reds++;
                }


                if (cell7.Text == "0yellow-2red-1green")
                {
                    cell8.Text = "red";
                    cell8.BackColor = System.Drawing.Color.Red;
                    reds++;
                }


                if (cell7.Text == "2yellow-0red-1green")
                {
                    cell8.Text = "Amber1";
                    cell8.BackColor = System.Drawing.Color.Yellow;
                    Amber1++;
                }

                if (cell7.Text == "2yellow-1red-0green")
                {
                    cell8.Text = "Amber2";
                    cell8.BackColor = System.Drawing.Color.YellowGreen;
                    Amber2++;
                }

                if (cell7.Text == "0yellow-1red-2green")
                {
                    cell8.Text = "green";
                    cell8.BackColor = System.Drawing.Color.LightGreen;
                    green++;
                }
                if (cell7.Text == "1yellow-1red-1green")
                {
                    cell8.Text = "Amber2";
                    cell8.BackColor = System.Drawing.Color.YellowGreen;
                    Amber2++;
                }
              
                dt.Rows.Add(compid,cell8.Text);

                if (dt.Rows.Count > 0)
                {
                    objQuestionMasterBE1.percentagecompid = Convert.ToString(compid);
                    objQuestionMasterBE1.Color = Convert.ToString(cell8.Text);
                  
                    objQuestionMasterBE1.batBatchId = ddlbatch.SelectedValue;
                }
                int status = ObjEmployeeAssessmentDetailsDAL.countforpieInsert(objQuestionMasterBE1);
            }
          

            //Label1.Text = reds.ToString();
            //Label2.Text = Amber2.ToString();
            //Label3.Text = Amber1.ToString();
            //Label4.Text = green.ToString();

            //Session["red"] = Label1.Text.ToString();
            //Session["Amber2"] = Label2.Text.ToString();
            //Session["Amber1"] = Label3.Text.ToString();
            //Session["green"] = Label4.Text.ToString();
            // CountRowColor();
            //percentages();
        }

        //public void percentages()
        //{
        //    //int tot =Convert.ToInt32( Session["red"].ToString());
        //    int red1 = Convert.ToInt32(Session["red"].ToString());
        //    int amber2 = Convert.ToInt32(Session["Amber2"].ToString());
        //    int amber1 = Convert.ToInt32(Session["Amber1"].ToString());
        //    int green = Convert.ToInt32(Session["green"].ToString());
        //    int total = red1 + amber2 + amber1 + green;

        //    //var per = (red1 * 100) / total;

        //    int percentagered = (red1 * 100) / total;
        //    int percentageAmber2 = (amber2 * 100) / total;
        //    int percentageAmber1 = (amber1 * 100) / total;
        //    int percentagegreen = (green * 100) / total;

        //    //Session["perred1"] = percentagered;
        //    //Session["peramber2"] = percentageAmber2;
        //    //Session["peramber1"] = percentageAmber1;
        //    //Session["pergreen"] = percentagegreen;
        //    //   Response.Redirect("Charts1.aspx");

        //    DataTable dtpie = new DataTable();
        //    if (dtpie.Columns.Count == 0)
        //    {
        //        // dtresult.Columns.Add("EmployeeID_PK");
        //        dtpie.Columns.Add("percentagered");
        //        dtpie.Columns.Add("percentageAmber1");
        //        dtpie.Columns.Add("percentageAmber2");
        //        dtpie.Columns.Add("percentagegreen");
        //        dtpie.Columns.Add("Batchid");

        //      //  dtpie.Columns.Add("Emp id");
        //        //dtpie.Columns.Add("Color");
        //        //dtpie.Columns.Add("Percentage");
        //        //dtpie.Columns.Add("Batchid");

        //    }

        //    //checking for existing records
        //    DataTable getexists = ObjEmployeeAssessmentDetailsDAL.ifexistsrecords(ddlbatch.SelectedValue);

        //    //if (getexists.Rows.Count<= 0)
        //    //{
        //        dtpie.Rows.Add(percentagered, percentageAmber1, percentageAmber2, percentagegreen, ddlbatch.SelectedValue);
        //        if (dtpie.Rows.Count > 0)
        //        {
        //            objQuestionMasterBE1.percentagered = Convert.ToString(percentagered);
        //            objQuestionMasterBE1.percentageAmber1 = Convert.ToString(percentageAmber1);
        //            objQuestionMasterBE1.percentageAmber2 = Convert.ToString(percentageAmber2);
        //            objQuestionMasterBE1.percentagegreen = Convert.ToString(percentagegreen);

        //            objQuestionMasterBE1.batBatchId = ddlbatch.SelectedValue;
        //        }
        //        int status = ObjEmployeeAssessmentDetailsDAL.PiechartInsert(objQuestionMasterBE1);
        //   // }
        //    //else
        //    //{
        //    //    Response.Write("Records exists");
        //    //}
           
        //}
        //public enum Color { Red, Green, Blue, Black, Orange }  
        //        switch(cases)
        //        {
        //            case Color.Red:
        //                Console.BackgroundColor = ConsoleColor.Red;
        //                Console.WriteLine("Red");
        //        }

        public void CountRowColor()
        {
            int red = 0, yellow = 0;



            foreach (GridViewRow row in GvAssessments.Rows)
            {
                
                //row.BackColor = Color.White;
                foreach (TableCell cell in row.Cells)
                {

                        if (cell.BackColor == System.Drawing.Color.Red)
                            red++;
                        if (cell.BackColor == System.Drawing.Color.Yellow)
                            yellow++;
                   
                   
                }
            }
            

            this.Label1.Text = yellow.ToString();
            this.Label2.Text = red.ToString();
        }


        protected void GvAssessments_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
           GvAssessments.DataSource = dtresult;
          GvAssessments.PageIndex = e.NewPageIndex;
            GvAssessments.DataBind();
        }



        public void exporttopdf()
        {

            StringBuilder sb = new StringBuilder();
            sb.Append("<table  border='1'>");

            sb.Append("<thead>");
            sb.Append("<tr>");
            sb.Append("<th colspan='4'>Question</th>");
            sb.Append("<th colspan='3'>Answer</th>");
            sb.Append("<th>Count</th>");

            sb.Append("</tr>");
            sb.Append("</thead>");

            sb.Append("<tbody>");
            if (GvAssessments.Rows.Count > 0)
            {
                for (int count = 0; count < 10; count++)
                {
                    sb.Append("<tr>");

                }
                sb.Append("</tbody>");
                sb.Append("</table>");
            }
         
            StringReader sr = new StringReader(sb.ToString());

            Document pdfDoc = new Document(PageSize.A4, 10f, 10f, 10f, 0f);
            HTMLWorker htmlparser = new HTMLWorker(pdfDoc);
            using (MemoryStream memoryStream = new MemoryStream())
            {
                PdfWriter writer = PdfWriter.GetInstance(pdfDoc, memoryStream);
                pdfDoc.Open();

                htmlparser.Parse(sr);
                pdfDoc.Close();

                byte[] bytes = memoryStream.ToArray();
                memoryStream.Close();


                // Clears all content output from the buffer stream                
                Response.Clear();
                // Gets or sets the HTTP MIME type of the output stream.
                Response.ContentType = "application/pdf";
                // Adds an HTTP header to the output stream
                Response.AddHeader("Content-Disposition", "attachment; filename=Invoice.pdf");

                //Gets or sets a value indicating whether to buffer output and send it after
                // the complete response is finished processing.
                Response.Buffer = true;
                // Sets the Cache-Control header to one of the values of System.Web.HttpCacheability.
                Response.Cache.SetCacheability(HttpCacheability.NoCache);
                // Writes a string of binary characters to the HTTP output stream. it write the generated bytes .
                Response.BinaryWrite(bytes);
                // Sends all currently buffered output to the client, stops execution of the
                // page, and raises the System.Web.HttpApplication.EndRequest event.

                Response.End();
                // HttpContext.Current.ApplicationInstance.CompleteRequest();
                //  Closes the socket connection to a client. it is a necessary step as you must close the response after doing work.its best approach.
                // Response.Close();
            }
        }
        protected void btnexport_Click(object sender, EventArgs e)
        {

            //Response.Clear();
            //Response.Buffer = true;
            //Response.AddHeader("content-disposition", "attachment;filename=GridViewExport.xls");
            //Response.Charset = "";
            //Response.ContentType = "application/vnd.ms-excel";
            //using (StringWriter sw = new StringWriter())
            //{
            //    HtmlTextWriter hw = new HtmlTextWriter(sw);

            //    //To Export all pages
            //    GvAssessments.AllowPaging = false;
            //    this.bindgrid();

            //   // GvAssessments.HeaderRow.BackColor = Color.White;
            //    foreach (TableCell cell in GvAssessments.HeaderRow.Cells)
            //    {
            //        cell.BackColor = GvAssessments.HeaderStyle.BackColor;
            //    }
            //    foreach (GridViewRow row in GvAssessments.Rows)
            //    {
            //        //row.BackColor = Color.White;
            //        foreach (TableCell cell in row.Cells)
            //        {
            //            if (row.RowIndex % 2 == 0)
            //            {
            //                cell.BackColor = GvAssessments.AlternatingRowStyle.BackColor;
            //            }
            //            else
            //            {
            //                cell.BackColor = GvAssessments.RowStyle.BackColor;
            //            }
            //            cell.CssClass = "textmode";
            //        }
            //    }

            //    GvAssessments.RenderControl(hw);

            //    //style to format numbers to string
            //    string style = @"<style> .textmode { } </style>";
            //    Response.Write(style);
            //    Response.Output.Write(sw.ToString());
            //    Response.Flush();
            //    Response.End();
            //}

            //exporttopdf();
            string filename = "Report.xls";
            System.IO.StringWriter tw = new System.IO.StringWriter();
            System.Web.UI.HtmlTextWriter hw = new System.Web.UI.HtmlTextWriter(tw);
            DataGrid dgGrid = new DataGrid();
            dgGrid.DataSource = dtresult;
            dgGrid.DataBind();
            dgGrid.AllowSorting = true;
            //Get the HTML for the control.
            dgGrid.RenderControl(hw);
            //Write the HTML back to the browser.
            //Response.ContentType = application/vnd.ms-excel;
            Response.ContentType = "application/vnd.ms-excel";
            Response.AppendHeader("Content-Disposition", "attachment; filename=" + filename + "");
            this.EnableViewState = false;
            Response.Write(tw.ToString());
            Response.End();

            //Response.Clear();
            //Response.Buffer = true;
            //Response.ClearContent();
            //Response.ClearHeaders();
            //Response.Charset = "";
            //string FileName = "Vithal" + DateTime.Now + ".xls";
            //StringWriter strwritter = new StringWriter();
            //HtmlTextWriter htmltextwrtter = new HtmlTextWriter(strwritter);
            //Response.Cache.SetCacheability(HttpCacheability.NoCache);
            //Response.ContentType = "application/vnd.ms-excel";
            //Response.AddHeader("Content-Disposition", "attachment;filename=" + FileName);
            //GvAssessments.GridLines = GridLines.Both;
            //GvAssessments.HeaderStyle.Font.Bold = true;
            //GvAssessments.RenderControl(htmltextwrtter);
            //Response.Write(strwritter.ToString());
            //Response.End();
        }

        public override void VerifyRenderingInServerForm(Control control)
        {
            /* Verifies that the control is rendered */
        }

        protected void lblEmployeeId1_Click(object sender, EventArgs e)
        {
            
        }

        protected void GvAssessments_RowCommand(object sender, GridViewCommandEventArgs e)
        {


            if(e.CommandName=="ReportID")
            {
              if(e.CommandArgument!="")
                {

                }

                       
             }
        }

       
    }
}