﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BE;
using DAL;
using System.Data;
using System.Data.SqlClient;

namespace HealToZeal.Company
{
    public partial class AddBatch : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Session["CompanyId"] != null)
                {
                   

                }
            }
        }


        protected void addBatch()
        {
           
            EmployeeBE objEmployeeBE = new EmployeeBE();
            EmployeeDAL objEmployeeDAL=new EmployeeDAL();
            string  CompanyId = Session["CompanyId"].ToString();
            objEmployeeBE.PropBatchName =Batchname.Value ;
            objEmployeeBE.propCompanyId_FK = CompanyId ;
            objEmployeeBE.propCreatedBy = CompanyId;
            objEmployeeBE.propCreatedDate = DateTime.Now.Date;
         //   objEmployeeBE.PropBatchID =;
            

            DataTable dt = objEmployeeDAL.ToCheckBatch(CompanyId, Batchname.Value);
          
            if (dt.Rows[0][0].ToString() == (1).ToString())
            {
                lblMessage.Text = "Batch  already added...";
            }
            else
            {
                int status = objEmployeeDAL.BatchInsert(objEmployeeBE);
                lblMessage.Text = "Batch Added successfully..";
            }
        }

        protected void btnAddbatch_Click(object sender, EventArgs e)
        {
            addBatch();
        }

        
    }
}