﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using BE;
using DAL;
using System.Net.Mime;
using System.Web.Mail;
using System.Net;
using System.Data.SqlClient;
using System.Configuration;

namespace HealToZeal.Company
{
    public partial class ImportEmployee : System.Web.UI.Page
    {
        string CompanyId = "";
        EmployeeDAL objEmployeeDAL = new EmployeeDAL();
        string EmployeeId = "";
        static DataTable DtAnswer = new DataTable();
        public string ItemFilePath = "";
        string adminId = "";
        string QuestionId = "";
        static int NoOfAnswers = 0;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                //    SqlConnection con = new SqlConnection(@"Data Source=192.227.69.198;Initial Catalog=HealToZealNew;Integrated Security=True;Connection Timeout=36000");
                //EmployeeBE objEmployeeBE = new EmployeeBE();

                //string com = "Select * from BatchDetails";
                //SqlDataAdapter adpt = new SqlDataAdapter(com, con);
                //DataTable dt = new DataTable();
                //adpt.Fill(dt);
                //ddlbatch.DataSource = dt;
                //ddlbatch.DataBind();
                //ddlbatch.DataTextField = "BatchName";
                //ddlbatch.DataValueField = "BatchID_PK";
                //ddlbatch.DataBind();
                //    ddlbatch.Items.Insert(0, new ListItem("--Select--", "0"));
                //    con.Close();
                Bindbatch();
            }
        }
        private void Bindbatch()
        {
            try
            {
                string CompanyId = "";
                CompanyId = Session["CompanyId"].ToString();
                DataTable Dtdbatch = objEmployeeDAL.GetBatchName(CompanyId);

                ddlbatch.DataSource = Dtdbatch;
                ddlbatch.DataTextField = "BatchName";
                ddlbatch.DataValueField = "BatchID_PK";
                ddlbatch.DataBind();

                ddlbatch.Items.Insert(0, new System.Web.UI.WebControls.ListItem("--SelectBatch--", "--Select Batch--"));
                ddlbatch.SelectedIndex = 0;
            }
            catch
            {

            }
        }
        //private void InsertUpdateEmployee()
        //{
        //    try
        //    {
        //        //insert
        //        string password = Encryption.CreatePassword();

        //        EmployeeBE objEmployeeBE = new EmployeeBE();
        //        string userName = Encryption.CreateUsername();//txtEmployeeCompanyId.Text + "" + txtFirstName.Text;
        //        CompanyId = Session["CompanyId"].ToString();
        //        objEmployeeBE.propCompanyId_FK = CompanyId;
        //        objEmployeeBE.propEmployeeCompanyId = Convert.ToInt32(EmployeeCompanyId.Value);
        //        objEmployeeBE.propUserName = userName;
        //        objEmployeeBE.propPassword = Encryption.Encrypt(password);
        //        objEmployeeBE.propFirstName = FirstName.Value;
        //        objEmployeeBE.propLastName = LastName.Value;
        //        //objEmployeeBE.propEmailId = Encryption.Encrypt(EmailId.Value);
        //        objEmployeeBE.propEmailId = EmailId.Value;
        //        objEmployeeBE.propCadreId_FK = ddlCadre.SelectedItem.Value;

        //        //code by Poonam
        //        objEmployeeBE.PropBatchID = ddlBatch.SelectedValue;
        //        objEmployeeBE.propDeptId_PK = ddDepartment.SelectedItem.Value;
        //        objEmployeeBE.propCreatedBy = CompanyId;
        //        objEmployeeBE.propCreatedDate = DateTime.Now.Date;

        //        // string message = "";
        //        int Status = 0;
        //       // divalert.Visible = true;

        //        if (Session["EmployeeId"] != null)
        //        {
        //            EmployeeId = Session["EmployeeId"].ToString();
        //            Status = objEmployeeDAL.EmployeesUpdateByCompanyId(objEmployeeBE);
        //        }
        //        else
        //        {
        //            Status = objEmployeeDAL.EmployeesInsert(objEmployeeBE);

        //           // InsertManager();
        //            if (Status > 0)
        //            {
        //                MailMessage mail = new MailMessage();
        //                // System.Web.Mail.MailMessage mail = new System.Web.Mail.MailMessage();
        //                //mail.To = EmailId.Value;

        //                //mail.From = "HealToZeal@chetanatspl.com";
        //                //mail.Subject = "Welcome to the Health and Personality Insights and Actions Tool.";
        //                //string getDt = DateTime.Now.Date.AddDays(7).ToString("MM/dd/yyyy");

        //                //mail.Body = "<html><body><font color=\"black\">Dear " + FirstName.Value + " " + LastName.Value + ",<br/><br/>Welcome to <b> Let Me Talk2 Me: A Health and Personality Insights and Actions Tool.</b> You will be able to gain some useful insights into your own personality and learn about remedial actions in case they are needed. <b>'Let Me Talk2 Me'</b> will help you in self-discovery. It will lead on a path of self development while avoiding pitfalls of modern complex life and careers. <br/><br/>It is recommended that you use this tool periodically to monitor yourself and be aware of your health and personality parameters. <br/><br/> Please use the link below to login <br/><br/><a href='http://chetanatspl.com/HealToZeal1/Employee/EmployeeLogin.aspx?first=1'> http://chetanatspl.com/HealToZeal1/Employee/EmployeeLogin.aspx </a><br/>Your login details are as follows:<br/>UserId:<b>" + userName + "</b><br/>Password:<b>" + password + "</b><br/><br/>If you would like to take a break during assessment, please make sure you log out from the system or close the browser window for security purposes. Please do not leave the browser idle. You can always log back in whenever you like.<br/><br/>Please ensure that you complete this assessment on or before " + getDt + ". To get a maximum benefit, please repeat this exercise every 3 months and study the differentials.<br/> You will be notified for the same when the next assessment is due.<br/><br/> Thank You.<br/><br/></font></body></html>";
        //                //mail.BodyFormat = MailFormat.Html;
        //                //mail.Fields.Add("http://schemas.microsoft.com/cdo/configuration/smtpauthenticate", "1");    //basic authentication
        //                //mail.Fields.Add("http://schemas.microsoft.com/cdo/configuration/sendusername", "HealToZeal@chetanatspl.com"); //set your username here
        //                //mail.Fields.Add("http://schemas.microsoft.com/cdo/configuration/sendpassword", "HealToZeal");    //set your password here

        //                //System.Web.Mail.SmtpMail.SmtpServer = "holly.arvixe.com";  //your real server goes here                        
        //                //System.Web.Mail.SmtpMail.Send(mail);

        //                //  ClearEmployeeDetails();
        //                //message = "Employee added successfully...";
        //              //  divalert.Attributes.Add("class", "alert alert-success");
        //               // lblMessage.Text = "Employee added successfully...";
        //            }
        //            else
        //            {
        //               // divalert.Attributes.Add("class", "alert alert-danger");
        //                //message = "Employee can not be added...";
        //              //  lblMessage.Text = "Employee can not be added...";
        //            }
        //        }
        //        // lblMessage.Text = message;
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //}



        private void ImportDatafromExcel(string FileLocation)
        {
            ImportExportToExcel importExportToExcel = new ImportExportToExcel();
            DataTable dtData = importExportToExcel.ExcelToDatatable(FileLocation);
            ImportMemberData(dtData);

        }


        // public string PropQuestion { get; set; }
        private void ImportMemberData(DataTable dtData)
        {
            string adminEmail = ConfigurationManager.AppSettings.Get("DefaultUser");
            try
            {
                //insert
                for (int i = 0; i < dtData.Rows.Count; i++)
                {
                    string password = Encryption.CreatePassword();

                    EmployeeBE objEmployeeBE = new EmployeeBE();
                    string userName = Encryption.CreateUsername();//txtEmployeeCompanyId.Text + "" + txtFirstName.Text;
                    CompanyId = Session["CompanyId"].ToString();
                    objEmployeeBE.propCompanyId_FK = CompanyId;
                    objEmployeeBE.propEmployeeCompanyId = Convert.ToInt32(dtData.Rows[i]["EmployeeCompanyId"]);
                    objEmployeeBE.propUserName = userName;
                    objEmployeeBE.propPassword = Encryption.Encrypt(password);
                    objEmployeeBE.propFirstName = dtData.Rows[i]["FirstName"].ToString();
                    objEmployeeBE.propLastName = dtData.Rows[i]["LastName"].ToString();
                    //objEmployeeBE.propEmailId = Encryption.Encrypt(EmailId.Value);
                    //objEmployeeBE.propEmailId = "meenakshi@brainlines.in";
                    objEmployeeBE.propEmailId = adminEmail;
                    //objEmployeeBE.propCadreId_FK = ddlCadre.SelectedItem.Value;

                    //Added by meenakshi 1.12.18
                    objEmployeeBE.PropBatchID =ddlbatch.SelectedValue;
                   // objEmployeeBE.propDeptId_PK = ddDepartment.SelectedItem.Value;
                    objEmployeeBE.propCreatedBy = CompanyId;
                    objEmployeeBE.propCreatedDate = DateTime.Now.Date;

                    // string message = "";
                    int Status = 0;
                    // divalert.Visible = true;

                   // if (Session["EmployeeId"] != null)
                   // {
                       // EmployeeId = Session["EmployeeId"].ToString();
                    //    Status = objEmployeeDAL.EmployeesUpdateByCompanyId(objEmployeeBE);
                   // }
                   // else
                    {
                        Status = objEmployeeDAL.EmployeesImport(objEmployeeBE);

                        // InsertManager();
                        if (Status > 0)
                        {
                            MailMessage mail = new MailMessage();
                            // System.Web.Mail.MailMessage mail = new System.Web.Mail.MailMessage();
                            //mail.To = EmailId.Value;

                            //mail.From = "HealToZeal@chetanatspl.com";
                            //mail.Subject = "Welcome to the Health and Personality Insights and Actions Tool.";
                            //string getDt = DateTime.Now.Date.AddDays(7).ToString("MM/dd/yyyy");

                            //mail.Body = "<html><body><font color=\"black\">Dear " + FirstName.Value + " " + LastName.Value + ",<br/><br/>Welcome to <b> Let Me Talk2 Me: A Health and Personality Insights and Actions Tool.</b> You will be able to gain some useful insights into your own personality and learn about remedial actions in case they are needed. <b>'Let Me Talk2 Me'</b> will help you in self-discovery. It will lead on a path of self development while avoiding pitfalls of modern complex life and careers. <br/><br/>It is recommended that you use this tool periodically to monitor yourself and be aware of your health and personality parameters. <br/><br/> Please use the link below to login <br/><br/><a href='http://chetanatspl.com/HealToZeal1/Employee/EmployeeLogin.aspx?first=1'> http://chetanatspl.com/HealToZeal1/Employee/EmployeeLogin.aspx </a><br/>Your login details are as follows:<br/>UserId:<b>" + userName + "</b><br/>Password:<b>" + password + "</b><br/><br/>If you would like to take a break during assessment, please make sure you log out from the system or close the browser window for security purposes. Please do not leave the browser idle. You can always log back in whenever you like.<br/><br/>Please ensure that you complete this assessment on or before " + getDt + ". To get a maximum benefit, please repeat this exercise every 3 months and study the differentials.<br/> You will be notified for the same when the next assessment is due.<br/><br/> Thank You.<br/><br/></font></body></html>";
                            //mail.BodyFormat = MailFormat.Html;
                            //mail.Fields.Add("http://schemas.microsoft.com/cdo/configuration/smtpauthenticate", "1");    //basic authentication
                            //mail.Fields.Add("http://schemas.microsoft.com/cdo/configuration/sendusername", "HealToZeal@chetanatspl.com"); //set your username here
                            //mail.Fields.Add("http://schemas.microsoft.com/cdo/configuration/sendpassword", "HealToZeal");    //set your password here

                            //System.Web.Mail.SmtpMail.SmtpServer = "holly.arvixe.com";  //your real server goes here                        
                            //System.Web.Mail.SmtpMail.Send(mail);

                            //  ClearEmployeeDetails();
                            //message = "Employee added successfully...";
                            //  divalert.Attributes.Add("class", "alert alert-success");
                             //lblMessage.Text = "Employee added successfully...";
                        }
                        else
                        {
                            // divalert.Attributes.Add("class", "alert alert-danger");
                            //message = "Employee can not be added...";
                            //  lblMessage.Text = "Employee can not be added...";
                        }
                    }
                    // lblMessage.Text = message;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            Response.Write("<script>alert('Employess Imported successfully')</script>");

        }


        private void ValidateData(string FileLocation)
        {
            try
            {
                Boolean hasError = false;

                ImportExportToExcel importExportToExcel = new ImportExportToExcel();
                DataTable DtExcelDataTable = importExportToExcel.ExcelToDatatable(FileLocation);
                Session["DtExcelDataTable"] = DtExcelDataTable;

                if (!hasError)
                {
                    ImportDatafromExcel(FileLocation);
                    ImportExportToExcel ObjImport = new ImportExportToExcel();

                }
            }
            catch (Exception )
            {

            }
        }



        protected void btnimport_Click1(object sender, EventArgs e)
        {
            try
            {

                if (exampleInputFile.HasFile)
                {
                    string ext = System.IO.Path.GetExtension(exampleInputFile.PostedFile.FileName).ToLower();
                    if (ext == ".xls" || ext == ".xlsx")
                    {


                        if (!System.IO.Directory.Exists(Server.MapPath("Data" + ItemFilePath)))
                        {
                            System.IO.Directory.CreateDirectory(Server.MapPath("Data" + ItemFilePath));
                        }

                        String datetime = DateTime.Now.Ticks.ToString();
                        String fileExt = System.IO.Path.GetExtension(exampleInputFile.FileName);


                        exampleInputFile.SaveAs(Server.MapPath("Data" + ItemFilePath) + "\\" + datetime + fileExt);
                        string path = Server.MapPath("Data" + ItemFilePath) + "\\" + datetime + fileExt;

                        if (path != "")
                        {
                            ValidateData(path);
                            exampleInputFile.Enabled = true;
                            btnimport.Enabled = true;

                        }
                    }
                    else
                    {
                    }
                }
                else
                {

                }
            }
            catch (Exception )
            {

            }
        }
    }

  
}