﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Company/Company.Master" AutoEventWireup="true" CodeBehind="UsersClusterwiseResult.aspx.cs" Inherits="HealToZeal.Company.UsersClusterwiseResult" %>
<%@ Register assembly="System.Web.DataVisualization, Version=4.0.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" namespace="System.Web.UI.DataVisualization.Charting" tagprefix="asp" %>

<%@ Register assembly="Microsoft.ReportViewer.WebForms, Version=11.0.0.0, Culture=neutral, PublicKeyToken=89845dcd8080cc91" namespace="Microsoft.Reporting.WebForms" tagprefix="rsweb" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="LeftColumn" runat="server">
    <div class="container"  style="width:100%">
        <div class="row">
               <div class="col-lg-6 col-md-7 col-sm-12">
        <br />
        <asp:Label ID="Label1" runat="server" Text="Employee Id"></asp:Label>
        &nbsp;<asp:TextBox ID="txtEmployeeId" runat="server"></asp:TextBox>
        <asp:Button ID="btnSubmit" runat="server" Text="Submit" OnClick="btnSubmit_Click" />
        <div class="hs_tab">
            <ul id="myTab" class="nav nav-tabs">
                <li class="active"><a href="#services1" data-toggle="tab">Standard Result</a></li>
                <li><a href="#services2" data-toggle="tab">Remedy Observation</a></li>
                <li><a href="#services3" data-toggle="tab">Result</a></li>
                <li><a href="#services4" data-toggle="tab">Suggestion</a></li>
            </ul>
            <div id="myTabContent" class="tab-content">
                <div class="tab-pane fade in active" id="services1">
                    <div class="row">
                        <div class="col-lg-12 col-md-12 col-sm-12">
                            <div class="col-lg-6 col-md-6 col-sm-6">
                                &nbsp;
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-6">
                                <%--   <h4 class="hs_theme_color">Standard result</h4>--%>
                                <div class="hs_margin_30"></div>
                                <asp:GridView ID="GvStandard" runat="server" AutoGenerateColumns="False" GridLines="Both" BorderWidth="2px" BorderColor="Black">
                                    <Columns>

                                        <asp:TemplateField HeaderText="Assessment">

                                            <ItemTemplate>
                                                <asp:Label ID="lblAssessmentName" runat="server" Text='<%#Eval("AssessmentName") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Cluster">
                                            <ItemTemplate>
                                                <asp:Label ID="lblClusterName" runat="server" Text='<%#Eval("ClusterName") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Score">
                                            <ItemTemplate>
                                                <asp:Label ID="lblScore" runat="server" Text='<%#Eval("ClusterScore") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="NoOfQuestions">
                                            <ItemTemplate>
                                                <asp:Label ID="lblTotalQuestions" runat="server" Text='<%#Eval("NoofQuestions") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="NoOfMaxScoredAnswers">
                                            <ItemTemplate>
                                                <asp:Label ID="Label2" runat="server" Text='<%#Eval("AnswerCount") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                </asp:GridView>
                                <div class="hs_margin_30"></div>
                                <%--<a href="blog-single-post-rightsidebar.html" class="btn btn-default">Read More</a>--%>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="tab-pane fade in active" id="services2">
                    <div class="row">
                        <div class="col-lg-12 col-md-12 col-sm-12">
                            <div class="col-lg-6 col-md-6 col-sm-6">
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-6">
                                <%--  <h4 class="hs_theme_color">Morbi id pulvinar enim. Vestibulum sed .</h4>--%>
                                <div class="hs_margin_30"></div>
                                <asp:GridView ID="GvRemedyObservation" runat="server" AutoGenerateColumns="False">
                                    <Columns>
                                        <asp:TemplateField HeaderText="Cluster">
                                            <ItemTemplate>
                                                <asp:Label ID="Label5" runat="server" Text='<%#Eval("ClusterName") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Score">
                                            <ItemTemplate>
                                                <asp:Label ID="lblpercentage" runat="server" Text='<%#Eval("ClusterScore") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Suggestion">
                                            <ItemTemplate>
                                                <asp:Label ID="lblSuggestion" runat="server" Text='<%#Eval("Suggestion") %>' ForeColor='<%# System.Drawing.Color.FromName((string) Eval("Color")) %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField>
                                            <ItemTemplate>
                                                <asp:Label ID="lblColor" runat="server" Text=""></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                    </Columns>
                                </asp:GridView>
                                <div class="hs_margin_30"></div>
                                <%--        <a href="blog-single-post-rightsidebar.html" class="btn btn-default">Read More</a>--%>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="tab-pane fade in active" id="services3">
                    <div class="row">
                        <div class="col-lg-12 col-md-12 col-sm-12">
                            <div class="col-lg-6 col-md-6 col-sm-6">
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-6">
                                <%--   <h4 class="hs_theme_color">Morbi id pulvinar enim. Vestibulum sed .</h4>--%>
                                <div class="hs_margin_30"></div>
                                <asp:GridView ID="GvResult" runat="server" AutoGenerateColumns="False">
                                    <Columns>
                                        <asp:TemplateField>
                                            <ItemTemplate>
                                                <table>
                                                    <tr>
                                                        <%--   ='<%# string.IsNullOrEmpty(Eval("Result").ToString()) ? "false" : "true"%>'>--%>
                                                        <td>
                                                            <%--          <asp:Label ID="Label3" runat="server" Text='<%#(String.IsNullOrEmpty(Eval("Address3").ToString()) ? " " : "," + Eval("Address3"))%> '></asp:Label>--%>
                                                            <%--  <asp:Label ID="lblResultText" runat="server" Text='<%#Eval("Result") %>'  ForeColor='<%# System.Drawing.Color.FromName((string) Eval("Color")) %>'></asp:Label>--%>
                                                            <asp:Label ID="lblResultText" runat="server" ForeColor='<%# System.Drawing.Color.FromName((string) Eval("Color")) %>' Text='<%# Eval("Result") %>'> </asp:Label>

                                                        </td>
                                                        <td>
                                                            <asp:Label ID="Label6" runat="server" Text=""> </asp:Label>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                </asp:GridView>
                                <div class="hs_margin_30"></div>
                                <%--      <a href="blog-single-post-rightsidebar.html" class="btn btn-default">Read More</a>--%>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="tab-pane fade in active" id="services4">
                    <div class="row">
                        <div class="col-lg-12 col-md-12 col-sm-12">
                            <div class="col-lg-6 col-md-6 col-sm-6">
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-6">

                                <div class="hs_margin_30"></div>
                                <asp:GridView ID="GvSuggestion" runat="server" AutoGenerateColumns="False">
                                    <Columns>
                                        <asp:TemplateField HeaderText="Title">
                                            <ItemTemplate>
                                                <%--     <table>
                            <tr>
                                 <td>--%>
                                                <asp:Label ID="lblTitle" runat="server" Text='<%#Eval("Title") %>'></asp:Label>
                                                <%-- </td>
                                  <td>
                                    <asp:Label ID="lblSuggestionText" runat="server" Text='<%#Eval("SuggestionText") %>'></asp:Label>
                                </td>
                                </tr>
                            </table>--%>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Suggestion">
                                            <ItemTemplate>
                                                <asp:Label ID="lblSuggestionText" runat="server" Text='<%#Eval("SuggestionText") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                </asp:GridView>
                                <div class="hs_margin_30">
                                    <rsweb:ReportViewer ID="ReportViewer1" runat="server" Font-Names="Verdana" Font-Size="8pt" WaitMessageFont-Names="Verdana" WaitMessageFont-Size="14pt">
                                        <LocalReport ReportPath="Reports\AllClusters.rdlc">
                                        </LocalReport>
                                    </rsweb:ReportViewer>
                                    <asp:ObjectDataSource ID="ObjectDataSource1" runat="server"></asp:ObjectDataSource>
                                </div>
                                <%--      <a href="blog-single-post-rightsidebar.html" class="btn btn-default">Read More</a>--%>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>




        <%--     <asp:GridView ID="GvEmployeeResult" runat="server" style="margin-right: 0px" AutoGenerateColumns="False">
            <Columns>
                <asp:TemplateField  HeaderText="Assessment">
                    <ItemTemplate>                  
                                       <asp:Label ID="Label9" runat="server" Text='<%#Eval("AssessmentName") %>'></asp:Label>                            

                    </ItemTemplate>
                </asp:TemplateField>
               <asp:TemplateField  HeaderText="Cluster">
                   <ItemTemplate>
                         <asp:Label ID="Label10" runat="server" Text='<%#Eval("ClusterName") %>' ></asp:Label>
                   </ItemTemplate>
               </asp:TemplateField>
                  <asp:TemplateField  HeaderText="Score">
                   <ItemTemplate>
                             <asp:Label ID="Label11" runat="server" Text='<%#Eval("Score") %>'></asp:Label>
                   </ItemTemplate>
               </asp:TemplateField>
                   <asp:TemplateField  HeaderText="TotalQuestions">
                   <ItemTemplate>
                       <asp:Label ID="Label12" runat="server" Text='<%#Eval("TotalQuestions") %>' ></asp:Label>
                   </ItemTemplate>
               </asp:TemplateField>
                   <asp:TemplateField  HeaderText="ansscore">
                   <ItemTemplate>
                               <asp:Label ID="lblansscore" runat="server" Text='<%#Eval("ansscore") %>'></asp:Label>
                   </ItemTemplate>
               </asp:TemplateField>
           <asp:TemplateField  HeaderText="outofScore">
                   <ItemTemplate>
                              <asp:Label ID="lbloutofScore" runat="server" Text='<%#Eval("outofScore") %>'></asp:Label>
                   </ItemTemplate>
               </asp:TemplateField>
                     <asp:TemplateField  HeaderText="percentage">
                   <ItemTemplate>
                <asp:Label ID="Label13" runat="server" Text='<%#Eval("percentage") %>' ></asp:Label>
                   </ItemTemplate>
               </asp:TemplateField>
            </Columns>
        </asp:GridView>--%>
    </div>
  
        </div>
    </div>
 
</asp:Content>
