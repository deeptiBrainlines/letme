﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace HealToZeal.Company
{
    public partial class Company : System.Web.UI.MasterPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {

            if (!IsPostBack)
            {
                if (Session["DomainId"].ToString() == "92394ef2-5fa8-41ce-aef8-49219e470ef8")
                {
                    SummaryPsychographics.Visible = true;
                    stud1.Visible = true;
                    stud2.Visible = true;
                    emp1.Visible = false;
                    emp2.Visible = false;
                    Li1.Visible = true;
                }
                else
                {
                    SummaryPsychographics.Visible = true;
                    emp1.Visible = true;
                    emp2.Visible = true;
                    stud1.Visible = false;
                    stud2.Visible = false;
                    Li1.Visible = true;
                }


                if (Session["CompanyName"] != null)
                {
                    //Menu1.FindItem("User").Text = Session["CompanyName"].ToString();
                    lblusername.Text = "Welcome " + Session["CompanyName"].ToString() + " (" + DateTime.Today.ToShortDateString() + ")";
                    //LiCadre.Style.Add("display", "block");
                    //EmpPsychographics.Disabled = false;
                    EmpPsychographics.Style.Add("display", "inline-block");
                    //SummaryPsychographics.Style.Add("display", "inline-block");
                    LiLogout.Style.Add("display", "inline-block");
                    LiLogin.Style.Add("display", "none");
                    LiCLogin.Style.Add("display", "none");
                    Li1Batch.Visible = true;
                }
                else
                {
                    //  if(Session["PageName"] != null)
                    {
                        // if (Session["PageName"].ToString() == "CompanyLogin" && Session["CompanyId"]==null)
                        {
                            //disable
                            //LiCadre.Style.Add("display","none");

                            // EmpPsychographics.Disabled = true;
                            EmpPsychographics.Style.Add("display", "none");
                            //SummaryPsychographics.Style.Add("display", "none");
                            LiLogout.Style.Add("display", "none");
                            LiLogin.Style.Add("display", "inline-block");
                            LiCLogin.Style.Add("display", "inline-block");
                            Li1Batch.Visible = false;
                        }

                    }
                }
            }
        }
        protected void lbtnLogout_Click(object sender, EventArgs e)
        {
            Session.Clear();
            Session.Abandon();
            Response.Redirect("CompanyLogin.aspx");
        }
        protected void lbtnLogin_Click(object sender, EventArgs e)
        {
            try
            {
                Session.Clear();
                Session.Abandon();
                Response.Redirect("~\\Employee\\EmployeeLogin.aspx");
            }
            catch (Exception )
            {

            }
        }

        protected void lbtnCLogin_Click(object sender, EventArgs e)
        {
            try
            {
                Session.Clear();
                Session.Abandon();
                Response.Redirect("CompanyLogin.aspx");
            }
            catch (Exception )
            {

            }
        }
    }
}