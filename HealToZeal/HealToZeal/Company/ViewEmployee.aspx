﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ViewEmployee.aspx.cs" MasterPageFile="~/Company/Company.Master" Inherits="HealToZeal.Company.ViewEmployee" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .style6 {
            width: 125px;
        }

        .style7 {
            height: 26px;
        }

        .hs_pager1 {
            display: inline-block;
            list-style: none;
            padding-left: 0px;
        }

            .hs_pager1 a {
                padding: 5px 12px;
                color: #7f9aa0;
                border: 1px solid #7f9aa0;
                text-align: center;
                font-weight: bold;
                font-size: 16px;
                -webkit-transition: all 0.5s;
                -moz-transition: all 0.5s;
                -ms-transition: all 0.5s;
                -o-transition: all 0.5s;
                margin-bottom: 60px;
            }

                .hs_pager1 a:hover, .hs_pager1 a:active {
                    color: #00ac7a;
                    border: 1px solid #00ac7a;
                    -webkit-transition: all 0.5s;
                    -moz-transition: all 0.5s;
                    -ms-transition: all 0.5s;
                    -o-transition: all 0.5s;
                }

        .pagination {
            line-height: 26px;
        }


            .pagination span {
                padding: 5px;
                border: solid 1px #477B0E;
                text-decoration: none;
                white-space: nowrap;
                background: #547B2A;
            }

            .pagination a, .pagination a:visited {
                text-decoration: none;
                padding: 6px;
                white-space: nowrap;
            }

                .pagination a:hover, .pagination a:active {
                    padding: 5px;
                    border: solid 1px #9ECDE7;
                    text-decoration: none;
                    white-space: nowrap;
                    background: #486694;
                }
    </style>
    <script type="text/javascript" src="../js/jquery-1.11.0.min.js"></script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="LeftColumn" runat="server">

    <style type="text/css">

         body
    {
        font-family: Arial;
        font-size: 10pt;
    }
    .GridPager a, .GridPager span
    {
        display: block;
        height: 15px;
        width: 15px;
        font-weight: bold;
        text-align: center;
        text-decoration: none;
    }
    .GridPager a
    {
        background-color: #f5f5f5;
        color: #969696;
        border: 1px solid #969696;
    }
    .GridPager span
    {
        background-color: #A1DCF2;
        color: #000;
        border: 1px solid #3AC0F2;
    }
        .autocomplete_completionListElement {
            visibility: hidden;
            margin: 0px !important;
            /* background-color: inherit;*/ /* code commented by chetana                                    system 30 july */
            background-color: White; /*/code by chetana sytem 30 july/*/
            color: windowtext;
            border: buttonshadow;
            border-width: 1px;
            border-style: solid;
            overflow: auto;
            /*height: 300px;*/ /* code commented by chetana system 30 july */
            height: auto; /*/code by chetana sytem 30 july/*/
            max-height: 350px;
            text-align: left;
            list-style-type: none;
            font-family: Tahoma;
            font-size: 8pt;
            cursor: pointer;
            position: relative;
            z-index: 500;
            padding: 0px;
        }

        .autocomplete_highlightedListItem {
            background-color: #00AC7A;
            color: black;
            padding: 1px;
        }

        .autocomplete_listItem {
            background-color: window;
            color: windowtext;
            padding: 1px;
        }

        .titlebar {
            font-family: Tahoma;
            font-size: small;
            font-weight: normal;
            color: #D8E9FF;
            background-color: #0D4181;
        }
    </style>
    <script type="text/javascript">
        function expandcollapse(obj) {
            var div = document.getElementById(obj);
            var a = document.getElementById(obj);

            if (div.style.display == "" || div.style.display == "none") {
                div.style.display = "block";
                div.style.height = "auto";
                a.class = "";
                div.class = "panel-collapse collapse in";
            }
            else {
                div.style.display = "none";
                div.style.height = "0px";
                a.class = "collapsed";
                div.class = "panel-collapse collapse";
            }
        }

        function UpdateItemSelected() {
            //var index = $find("ctl00_ContentPlaceHolder1_AutoCompleteExtender1")._selectIndex;
            var index = $find('<%= txtEmployeeId_AutoCompleteExtender.ClientID %>')._selectIndex;
            //var value = $find("ctl00_ContentPlaceHolder1_AutoCompleteExtender1").get_completionList().childNodes[index]._value;
            var value = $find('<%= txtEmployeeId_AutoCompleteExtender.ClientID %>').get_completionList().childNodes[index]._value;

            $get('<%= HdnAutoId.ClientID %>').value = index;
            $get('<%= HdnAutoValue.ClientID %>').value = value;
            $find('<%= txtEmployeeId_AutoCompleteExtender.ClientID %>').get_element().value = value

            var UcompId = $get('<%= txtEmployeeId.ClientID %>');
            $get('<%= HdnSearch.ClientID %>').value = UcompId.value;

            $get('<%= btnSearch.ClientID %>').click();
        //    alert('hi');
            $.ajax({

                type: "POST",
                url: '~/ViewEmployee.aspx/ShowListSearch',
                data: "{'name' : '" + UcompId.value + "'}",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                beforeSend: function () {
                    Show(); // Show loader icon  
                },
                success: function (response) {

                    // Looping over emloyee list and display it  
                    //$.each(response, function (index, emp) {
                    //    $('#output').append('<p>Id: ' + emp.ID + '</p>' +
                    //                        '<p>Id: ' + emp.Name + '</p>');
                    //});

                    alert(2);
                },
                complete: function () {
                    Hide(); // Hide loader icon  
                },
                failure: function (jqXHR, textStatus, errorThrown) {
                    alert("HTTP Status: " + jqXHR.status + "; Error Text: " + jqXHR.responseText); // Display error message  
                }
            });

        }

        function UpdateItemSelectedforName() {
           
            //var index = $find("ctl00_ContentPlaceHolder1_AutoCompleteExtender1")._selectIndex;
            var index = $find('<%= AutoCompleteExtender1.ClientID %>')._selectIndex;
            //var value = $find("ctl00_ContentPlaceHolder1_AutoCompleteExtender1").get_completionList().childNodes[index]._value;
            var value = $find('<%= AutoCompleteExtender1.ClientID %>').get_completionList().childNodes[index]._value;

            $get('<%= HdnAutoId.ClientID %>').value = index;
            $get('<%= HdnAutoValue.ClientID %>').value = value;
            $find('<%= AutoCompleteExtender1.ClientID %>').get_element().value = value

            var UName = $get('<%= txtName.ClientID %>');
            $get('<%= HdnSearch.ClientID %>').value = UName.value;

            $get('<%= btnSearchName.ClientID %>').click();
          //  alert(UName.value);

           

            $.ajax({

                type: "POST",
                url: '~/ViewEmployee.aspx/ShowSearchByName',
                data:  "{'name' : '" + UName.value + "'}",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                beforeSend: function () {
                    Show(); // Show loader icon  
                },
                success: function (response) {

                    // Looping over emloyee list and display it  
                    //$.each(response, function (index, emp) {
                    //    $('#output').append('<p>Id: ' + emp.ID + '</p>' +
                    //                        '<p>Id: ' + emp.Name + '</p>');
                    //});

                    alert(2);
                },
                complete: function () {
                    Hide(); // Hide loader icon  
                },
                failure: function (jqXHR, textStatus, errorThrown) {
                    alert("HTTP Status: " + jqXHR.status + "; Error Text: " + jqXHR.responseText); // Display error message  
                }
            });

        }

        <%-- function UpdateItemSelected() {

           //var index = $find("ctl00_ContentPlaceHolder1_AutoCompleteExtender1")._selectIndex;
           var index = $find('<%=txtEmployeeId_AutoCompleteExtender%>')._selectIndex;
           //var value = $find("ctl00_ContentPlaceHolder1_AutoCompleteExtender1").get_completionList().childNodes[index]._value;
           var value = $find('<%=txtEmployeeId_AutoCompleteExtender%>').get_completionList().childNodes[index]._value;
       
           $get('<%=HdnAutoId.ClientID%>').value = index;
           $get('<%=HdnAutoValue.ClientID%>').value = value;          
           $find('<%=txtEmployeeId_AutoCompleteExtender%>').get_element().value = value;
 
           var UName = $('<%= txtEmployeeId.ClientID %>').value;          
           $get('<%=HdnSearch.ClientID %>').value = UName;       
           $get('<%= btnSearch.ClientID %>').click();     
       
    }--%>
    </script>

    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <h4 class="hs_heading" id="hs_appointment_form_link">Search </h4>
            <div class="container">
                <div class="row">
                    <table style="width: 100%">
                        <tr id="TrEmail" runat="server" visible="false">
                            <%--  Reminder Email starts here--%>
                            <td style="width: 100%; text-align: left">
                                <div class="col-lg-8 col-md-8 col-sm-7">
                                    <div class="hs_comment_form">
                                        <div class="row">
                                            <div id="DivTitle" runat="server">
                                                <div id="div7" class="alert alert-info" runat="server">
                                                    <strong>Send Reminders
                                                </div>
                                                <br />
                                                <div style="width: 300px; height: 100px; overflow-y: auto">
                                                    <asp:CheckBoxList ID="chkEmployees" runat="server"></asp:CheckBoxList>
                                                </div>
                                                <br />
                                                <div class="col-lg-6 col-md-6 col-sm-12">
                                                    <div class="input-group">
                                                        <span class="input-group-btn">
                                                            <asp:Button ID="btnAddEmailIds" runat="server" CssClass="btn btn-success pull-right" Text="Add" OnClick="btnAddEmailIds_Click" />
                                                        </span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row" id="DivMailBody" runat="server">
                                            <%--<div class="col-lg-12">--%>
                                            <%--<div class="hs_comment_form">--%>
                                            <div class="row">
                                                <div class="col-lg-12">
                                                    <div class="input-group">
                                                        <span class="input-group-btn">
                                                            <div class="btn btn-success"><i class="fa fa-user"></i></div>
                                                        </span>
                                                        <asp:TextBox ID="TxtTo" runat="server" CssClass="form-control" placeholder="Name" Width="100%"></asp:TextBox>
                                                    </div>
                                                </div>

                                                <div class="col-lg-12">
                                                    <div class="input-group">
                                                        <span class="input-group-btn">
                                                            <div class="btn btn-success">Subject</div>
                                                        </span>
                                                        <asp:TextBox ID="TxtSubject" runat="server" CssClass="form-control" Width="100%"></asp:TextBox>
                                                    </div>
                                                </div>

                                                <div class="col-lg-6 col-md-6 col-sm-12">
                                                    <div>
                                                        <asp:Label Visible="false" ID="Label15" runat="server" Text='From'></asp:Label>
                                                        <input visible="false" disabled="disabled" id="uemail" runat="server" type="text" class="form-control" placeholder="From" />
                                                    </div>
                                                </div>

                                                <div class="col-lg-12">
                                                    <div class="form-group">
                                                        <textarea id="message" runat="server" class="form-control" rows="8"></textarea>
                                                    </div>
                                                </div>
                                                <p id="err"></p>
                                                <div class="form-group">
                                                    <div class="col-lg-8 col-md-8 col-sm-12">
                                                        <div class="checkbox">
                                                            <label>
                                                                <input type="checkbox" id="hs_checkbox" class="css-checkbox lrg" checked="checked" />
                                                            </label>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-6 col-md-6 col-sm-12">
                                                        <div class="input-group">
                                                            <span class="input-group-btn">
                                                                <asp:Button ID="BtnSendEmail" runat="server" CssClass="btn btn-success pull-right" Text="Submit" OnClick="BtnSendEmail_Click" />
                                                            </span>
                                                            <span class="input-group-btn">
                                                                <asp:Button ID="BtnCancel" runat="server" CssClass="btn btn-success pull-right" Text="Cancel" OnClick="BtnCancel_Click" />
                                                            </span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <%--</div>--%>
                                            <%--</div>--%>
                                        </div>
                                    </div>
                                </div>
                            </td>
                        </tr>
                        <%--  Search starts here--%>
                        <tr>
                            <td style="width: 100%; text-align: left">
                                <div class="col-lg-8 col-md-8 col-sm-7">
                                    <div class="hs_comment_form">
                                        <div class="row">
                                            <div class="row">
                                                <div class="col-lg-12">
                                                    <table width="100%">
                                                        <tr>
                                                            <td align="left" style="width: 50%">
                                                                <div class="input-group">
                                                                    <span class="input-group-btn">
                                                                        <asp:Button ID="BtnRefreshAll" runat="server" Text="Reset" CssClass="btn btn-primary btn-xs" OnClick="BtnRefresh_Click" />
                                                                    </span>
                                                                </div>
                                                            </td>
                                                            <td align="right">
                                                                <div class="input-group">
                                                                    <span class="input-group-btn">
                                                                        <asp:Button ID="BtnReminderEmail" runat="server" Text="Send reminder email" CssClass="btn btn-primary btn-xs" OnClick="BtnReminderEmail_Click" />
                                                                    </span>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                    </table>

                                                </div>
                                                <div class="col-lg-6 col-md-6 col-sm-12">
                                                    <div class="input-group">
                                                        <span class="input-group-btn">
                                                            <div class="btn btn-success"><i class="fa fa-hand-o-right"></i></div>
                                                        </span>
                                                        <asp:HiddenField ID="HdnAutoId" runat="server" />
                                                        <asp:HiddenField ID="HdnAutoValue" runat="server" />
                                                        <asp:Button ID="btnSearch" runat="server" OnClick="btnSearch_Click"
                                                            Style="display: none" Text="" />
                                                        <asp:TextBox ID="txtEmployeeId" runat="server" CssClass="form-control" placeholder="Employee company id"  OnTextChanged="txtEmployeeId_TextChanged" AutoPostBack="true" ></asp:TextBox>
                                                        <cc1:AutoCompleteExtender ID="txtEmployeeId_AutoCompleteExtender"
                                                            runat="server" Enabled="True" OnClientItemSelected="UpdateItemSelected"
                                                            ServiceMethod="ShowListSearch"
                                                            TargetControlID="txtEmployeeId" CompletionInterval="250"
                                                            CompletionListCssClass="autocomplete_completionListElement"
                                                            CompletionListHighlightedItemCssClass="autocomplete_highlightedListItem"
                                                            CompletionListItemCssClass="autocomplete_listItem" CompletionSetCount="15"
                                                            EnableCaching="true" FirstRowSelected="true" MinimumPrefixLength="1"
                                                            UseContextKey="true">
                                                        </cc1:AutoCompleteExtender>

                                                        

                                                        <asp:HiddenField ID="HdnSearch" runat="server" />
                                                    </div>
                                                    <!-- /input-group -->
                                                </div>
                                                <!-- /.col-lg-6 -->
                                                <div class="col-lg-6 col-md-6 col-sm-12">
                                                    <div class="input-group">
                                                        <span class="input-group-btn">
                                                            <div class="btn btn-success"><i class="fa fa-filter"></i></div>
                                                        </span>
                                                        <asp:DropDownList ID="ddlFilter" runat="server" CssClass="form-control" AutoPostBack="true" OnSelectedIndexChanged="ddlFilter_SelectedIndexChanged">
                                                            <asp:ListItem>--Filter by--</asp:ListItem>
                                                            <asp:ListItem>All employees</asp:ListItem>
                                                            <asp:ListItem>Assessment submitted employees</asp:ListItem>
                                                            <asp:ListItem>Assessment Pending</asp:ListItem>
                                                        </asp:DropDownList>
                                                    </div>
                                                    <!-- /input-group -->
                                                </div>
                                                <%--<div class="col-lg-6 col-md-6 col-sm-12">
                                                        <div class="input-group">
                                                            <span class="input-group-btn">
                                                                <div class="btn btn-success"><i class="fa fa-filter"></i></div>
                                                            </span>
                                                            <asp:DropDownList ID="ddlCadre" runat="server" CssClass="form-control" AutoPostBack="true" OnSelectedIndexChanged="ddlCadre_SelectedIndexChanged">
                                                                <asp:ListItem>--Select Cadre--</asp:ListItem>
                                                                <asp:ListItem>All employees</asp:ListItem>
                                                                <asp:ListItem>Assessment submitted employees</asp:ListItem>
                                                            </asp:DropDownList>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-6 col-md-6 col-sm-12">
                                                        <div class="input-group">
                                                            <span class="input-group-btn">
                                                                <div class="btn btn-success"><i class="fa fa-filter"></i></div>
                                                            </span>
                                                            <asp:DropDownList ID="ddlDepartment" runat="server" CssClass="form-control" AutoPostBack="true" Enabled="False">
                                                                <asp:ListItem>--Select department--</asp:ListItem>
                                                                <asp:ListItem>All employees</asp:ListItem>
                                                                <asp:ListItem>Assessment submitted employees</asp:ListItem>
                                                            </asp:DropDownList>
                                                        </div>
                                                    </div>--%>
                                                <div class="col-lg-6 col-md-6 col-sm-12">
                                                    <div class="input-group">
                                                        <span class="input-group-btn">
                                                            <div class="btn btn-success"><i class="fa fa-user"></i></div>
                                                        </span>
                                                        <asp:Button ID="btnSearchName" runat="server" OnClick="btnSearchName_Click"
                                                            Style="display: none" Text="" />
                                                        <asp:TextBox ID="txtName" runat="server" CssClass="form-control" placeholder="Name" OnTextChanged="TextBox2_TextChanged" AutoPostBack="true"></asp:TextBox>
                                                        <cc1:AutoCompleteExtender ID="AutoCompleteExtender1"
                                                            runat="server" Enabled="True" OnClientItemSelected="UpdateItemSelectedforName"
                                                            ServiceMethod="ShowListSearchByName"
                                                            TargetControlID="txtName" CompletionInterval="250"
                                                            CompletionListCssClass="autocomplete_completionListElement"
                                                            CompletionListHighlightedItemCssClass="autocomplete_highlightedListItem"
                                                            CompletionListItemCssClass="autocomplete_listItem" CompletionSetCount="15"
                                                            EnableCaching="true" FirstRowSelected="true" MinimumPrefixLength="1"
                                                            UseContextKey="true">
                                                        </cc1:AutoCompleteExtender>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                    <div class="hs_margin_30"></div>
                                    <div class="clearfix"></div>
                                    <div class="hs_margin_40"></div>
                                </div>
                            </td>
                        </tr>
                        <%--  Grid view starts here--%>
                        <tr>
                            <td style="width: 100%; text-align: left">
                                <div class="col-lg-10 col-md-10 col-sm-9">
                                    <%--     <div class="hs_sub_comment_div">--%>

                                    <%--   <div class="hs_sub_comment">--%>
                                    <div class="row">
                                        <div id="divalert" class="alert alert-success" runat="server">
                                            <strong>
                                                <asp:Label ID="lblMessage" runat="server"></asp:Label>

                                            </strong>
                                        </div>
                                    </div>
                                    <%--  <div class="row">--%>
                                    <%--   <div class="col-lg-10 col-md-10 col-sm-10 col-lg-offset-2 col-md-offset-2 col-sm-offset-2">--%>
                                    <%--   <div class="hs_comment">
                                                <div class="row">--%>
                                    <asp:GridView ID="GvViewEmployee" runat="server" Width="100%" BorderStyle="None" OnRowDataBound="GvViewEmployee_RowDataBound"
                                        AutoGenerateColumns="False" AllowPaging="True"
                                        OnPageIndexChanging="GvViewEmployee_PageIndexChanging" PageSize="10"
                                        AllowSorting="True" OnRowCommand="GvViewEmployee_RowCommand">
                                     <PagerStyle CssClass="hs_pager1" />
                                        <PagerSettings Mode="NumericFirstLast" />

                                     
                                        <Columns>
                                            <asp:TemplateField>
                                                <ItemTemplate>
                                                    <div class="hs_comment">
                                                        <div class="row">
                                                            <div class="col-lg-6">
                                                                <div class="hs_comment_date">
                                                                    <ul>
                                                                        <li>
                                                                            <i class="fa fa-user"></i>
                                                                            <%--<asp:HyperLink runat="server" ID="lblEmployeeName" NavigateUrl='<%# Eval("EmployeeId_PK", "~/Company/EmployeeSummary.aspx?Id={0}") %>' Text='<%#Eval("EmployeeName") %>'/>--%>
                                                                            <%--<asp:HyperLink ID="lblEmployeeName" runat="server" Text='<%#Eval("EmployeeName") %>' NavigateUrl='<%# Eval("EmployeeId_PK", "~/Company/EmployeeSummaryResult.aspx?Id={0}") %>'></asp:HyperLink>--%>
                                                                            <asp:Label ID="lblEmployeeName" runat="server" Text='<%#Eval("FirstName") %>'>   </asp:Label>
                                                                        </li>
                                                                        <li>
                                                                            <%--<asp:Label ID="lblEmployeeCompanyId" runat="server" Text='<%#Eval("Company") %>'> </asp:Label>--%>
                                                                        </li>
                                                                        <li>
                                                                            <a>
                                                                                <asp:Label ID="lblQualification" runat="server" Text='<%#Eval("Qualification") %>'>
                                                                                </asp:Label>
                                                                            </a>
                                                                        </li>

                                                                        <li>&nbsp;
                                                                            <asp:Label Width="80px" Enabled="false" ID="LblColor" Text='<%#Eval("LblText") %>' runat="server" Height="20px" BackColor='<%# System.Drawing.Color.FromName(Eval("Color").ToString()) %>'></asp:Label>
                                                                        </li>
                                                                        <li>
                                                                            <asp:Label ID="lblEmployeeId" runat="server" Visible="false" Text='<%#Eval("EmployeeId_PK") %>'>
                                                                            </asp:Label>
                                                                        </li>
                                                                    </ul>
                                                                </div>
                                                                <a class="hs_in_relpy">
                                                                    <asp:Label ID="lblEmail" Width="80%" runat="server" Text='<%#Eval("EmailId") %>'> </asp:Label>
                                                                </a>
                                                                <%-- <li>
                                                                                        <asp:Label ID="lblEmail" Width="80%" runat="server" Text='<%#Eval("EmailId") %>'> </asp:Label>
                                                                                    </li>--%>

                                                                <%--<p>CurabiturCurabitur ullamcorper dictum risus sed elementum. Sed pretium in risus n  Morbi euismod.</p>--%>
                                                            </div>
                                                            <div class="col-sm-2">
                                                                <asp:Button ID="btnRemove" runat="server" Text="Remove" CssClass="btn btn-link" CommandArgument='<%#Eval("EmployeeId_PK") %>' CommandName="DeleteEmployee" OnClientClick="return confirm('Are you sure, you want to delete this Employee?')" />
                                                            </div>
                                                            <div class="col-sm-2">
                                                                <asp:Button ID="btnHistory" runat="server" Text="History" CssClass="btn btn-link" CommandArgument='<%#Eval("EmployeeId_PK") %>' CommandName="ViewEmployeeHistory" />
                                                            </div>
                                                            <div class="col-sm-2">
                                                                <div class="hs_comment_date">
                                                                    <h4>
                                                                        <a href="javascript:expandcollapse('div<%# Eval("EmployeeId_PK") %>');" class="collapsed">Results </a>
                                                                    </h4>
                                                                    <div id="div<%# Eval("EmployeeId_PK") %>" class="panel-collapse collapse" style="height: 0px;">
                                                                        <div>
                                                                            <asp:GridView ID="GvAssessments" AutoGenerateColumns="False" runat="server" GridLines="None" OnRowCommand="GvAssessments_RowCommand">
                                                                                <Columns>
                                                                                    <asp:TemplateField>
                                                                                        <ItemTemplate>
                                                                                            <%--    <div class="hs_comment">
                                                                                                <div class="row">
                                                                                                    <div class="col-lg-8">
                                                                                                        <div class="hs_comment_date">--%>
                                                                                            <ul>
                                                                                                <li>
                                                                                                    <asp:Button ID="btnView" runat="server" CssClass="btn btn-success" Text='<%# Eval("CreatedDate","{0:dd/MM/yyyy}") %>' CommandName="ViewResult" />

                                                                                                    <asp:Label ID="lblAssessmentId" runat="server" Text='<%#Eval("AssessmentId_FK") %>' Visible="false"> </asp:Label>

                                                                                                    <asp:Label ID="lblAssessmentInstanceId" runat="server" Text='<%#Eval("AssessmentInstanceId_FK") %>' Visible="false"> </asp:Label>

                                                                                                    <asp:Label ID="lblInstanceCompanyRelationId" runat="server" Text='<%#Eval("InstanceCompanyRelationId_FK") %>' Visible="false"> </asp:Label>

                                                                                                    <asp:Label ID="lblEmployeeId" runat="server" Text='<%#Eval("EmployeeId_FK") %>' Visible="false"> </asp:Label>
                                                                                                </li>
                                                                                            </ul>
                                                                                            <%-- </div>
                                                                                                    </div>
                                                                                                </div>
                                                                                            </div>--%>
                                                                                        </ItemTemplate>
                                                                                    </asp:TemplateField>
                                                                                </Columns>
                                                                            </asp:GridView>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>

                                                        </div>

                                                    </div>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                        </Columns>
                                    </asp:GridView>

                                    <%--  </div>
                                            </div>--%>
                                    <%-- </div>--%>
                                    <%--  </div>--%>
                                    <%--</div>--%>
                                    <%--       <div class="hs_sub_comment">
            <div class="row">
              <div class="col-lg-10 col-md-10 col-sm-10 col-lg-offset-2 col-md-offset-2 col-sm-offset-2">
                <div class="hs_comment">
                  <div class="row">
                    <div class="col-lg-2 col-md-2 col-sm-2"><img src="http://placehold.it/117x117" alt="" /></div>
                    <div class="col-lg-10 col-md-10 col-sm-10">
                      <div class="hs_comment_date">
                        <ul>
                          <li><a href=""><i class="fa fa-user"></i> Jack Wilston</a></li>
                          <li><a href=""><i class="fa fa-calendar"></i> 12-Dec-2013</a></li>
                          <li><a href=""><i class="fa fa-clock-o"></i> 2:32 PM</a></li>
                        </ul>
                      </div>
                      <a href="" class="hs_in_relpy">In reply to  Jack wilstone </a>
                      <p>CurabiturCurabitur ullamcorper dictum risus sed elementum. Sed pretium in risus n  Morbi euismod.</p>
                      <a href="" class="btn btn-success pull-right">Reply</a> </div>
                  </div>
                </div>
              </div>
            </div>
          </div>--%>
                                    <%--    </div>--%>
                                    <div class="clearfix"></div>
                                    <div class="hs_margin_30"></div>
                                    <div class="clearfix"></div>
                                    <div class="hs_margin_40"></div>
                                </div>
                            </td>
                        </tr>
                    </table>
                    <div class="col-lg-12 col-md-12 col-sm-12">
                    </div>
                </div>
            </div>
            <%--<div class="container">
  <div class="row">
    <div class="col-lg-12 col-md-12 col-sm-12">
      <div class="hs_single_profile">
        <div class="hs_single_profile_detail">
            <div>
                <asp:GridView ID="GvViewEmployee" runat="server"
                    AutoGenerateColumns="False" AllowPaging="True"
                    OnPageIndexChanging="GvViewEmployee_PageIndexChanging" PageSize="10"
                    AllowSorting="True" OnRowCommand="GvViewEmployee_RowCommand">             
                    <Columns>
                        <asp:TemplateField>
                            <ItemTemplate>
                                <h4>
                                    <asp:Label ID="lblEmployeeName" runat="server" Text='<%#Eval("EmployeeName") %>'>   </asp:Label>
                                </h4>
                                <div class="row">
                                    <div class="col-lg-4 col-md-6 col-sm-6">
                                        <i></i>
                                        Company: <a href="">
                                            <asp:Label ID="lblEmployeeCompanyId" runat="server" Text='<%#Eval("Company") %>'> </asp:Label>
                                        </a>
                                    </div>
                                    <div class="col-lg-5 col-md-6 col-sm-6">
                                        <i></i>Qualification: <a href="">
                                            <asp:Label ID="lblQualification" runat="server" Text='<%#Eval("Qualification") %>'>
                                            </asp:Label>
                                        </a>
                                    </div>
                                    <div class="col-lg-3 col-md-6 col-sm-6">

                                        <i class="fa fa-user-md"></i>Experience: 
                                             <a href="">
                                                 <asp:Label ID="lblExperienceInYears" runat="server" Text='<%#Eval("ExperienceInYears") %>' Visible="false">
                                                 </asp:Label>
                                             </a>
                                    </div>


                                </div>
                                <hr />
                                <div class="row">
                                    <div class="col-lg-3 col-md-6 col-sm-6">
                                        <i class="fa fa-eraser"></i>
                                        <a href="">
                                            <asp:ImageButton ID="ImgbtnRemove" runat="server" CommandArgument='<%#Eval("EmployeeId_PK") %>' CommandName="DeleteEmployee" AlternateText="Remove" ImageUrl="~/images/button_cancel.png" />
                                        </a>
                                    </div>
                                    <div class="col-lg-5 col-md-6 col-sm-6">
                                        <a href="">
                                            <asp:Button ID="btnEdit" runat="server" Text="Edit" CommandArgument='<%#Eval("EmployeeId_PK") %>' Visible="false" CommandName="EditEmployee" />

                                        </a>
                                    </div>

                                </div>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                </asp:GridView>
            </div>     
        </div>       
      </div>
    </div>
  </div>
  
  <div class="hs_margin_40"></div>
</div>--%>
            <%--  <div class="health_care_advice">
            <asp:GridView ID="GvViewEmployee" runat="server"
                AutoGenerateColumns="False" Width="100%"
                CellPadding="6" CellSpacing="6" AllowPaging="True"
                OnPageIndexChanging="GvViewEmployee_PageIndexChanging" PageSize="5"
                AllowSorting="True" OnRowCommand="GvViewEmployee_RowCommand">
                <PagerStyle CssClass="hs_pager1" />
                <Columns>
                    <asp:TemplateField>
                        <ItemTemplate>
                            <table style="width: 100%">--%>
            <%--     <tr>
                                    <td>
                                        <div class="col-lg-12 col-md-12 col-sm-12">
                                            <div class="hs_single_profile">
                                                <div class="hs_single_profile_detail">
                                                    <h3>
                                                        <asp:Label ID="lblEmployeeName" runat="server" Text='<%#Eval("EmployeeName") %>'>
                                                        </asp:Label>
                                                    </h3>
                                                    <div class="row">
                                                        <div class="col-lg-3 col-md-6 col-sm-6">
                                                            <i class="fa fa-user-md"></i>Experience: 
                                                                                           <a href="">
                                                                                               <asp:Label ID="lblExperienceInYears" runat="server" Text='<%#Eval("ExperienceInYears") %>' Visible="false">
                                                                                               </asp:Label>
                                                                                           </a>
                                                        </div>
                                                        <div class="col-lg-5 col-md-6 col-sm-6">
                                                            <i class="fa fa-medkit"></i>Qualification: <a href="">
                                                                <asp:Label ID="lblQualification" runat="server" Text='<%#Eval("Qualification") %>'>
                                                                </asp:Label>
                                                            </a>
                                                        </div>
                                                        <div class="col-lg-4 col-md-6 col-sm-6"><i class="fa fa-medkit"></i>
                                                            Company: <a href="">
                                                                  <asp:Label ID="lblEmployeeCompanyId" runat="server" Text='<%#Eval("Company") %>'> </asp:Label>
                                                                      </a> </div>
                                                    </div>

                                                </div>
                                            </div>
                                        </div>
                                    </td>
                                </tr>--%>
            <%--       <tr>
                                                                   <td>

                                                                       <div class="hs_comment">
                                                                           <div class="row">
                                                                       <div class="col-lg-11 col-md-10 col-sm-10">
                                                                       <div class="hs_comment_date">
                                                                           <ul>
                                                                               <li>
                                                                                 <asp:Label ID="lblEmployeeCompanyId" runat="server" Text='<%#Eval("Company") %>'> </asp:Label></li>
                                                                               <li>
                                                                                   <asp:Label ID="lblEmployeeName" runat="server" Text='<%#Eval("EmployeeName") %>'>
                                                                                   </asp:Label>

                                                                               </li>                                                                              
                                                                           </ul>
                                                                       </div>
                                                                           </div>
                                                                               </div>
                                                                           </div>
                                                                      
                                                                   </td>                                                            
                                                               </tr>--%>
            <%--        <tr>
                                                                   <td>
                                                                       <div class="hs_post">
                                                                           <asp:Label ID="lblQualification" runat="server" Text='<%#Eval("Qualification") %>'>
                                                                           </asp:Label>
                                                                               <asp:Label ID="lblOccupation" runat="server" Text='<%#Eval("Occupation") %>' Visible="false">
                                                                           </asp:Label>
                                                                       </div>
                                                                   </td>
                                                               
                                                               </tr>
                                                               <tr>
                                                                   <td>
                                                                       <div class="hs_post">
                                                                           <asp:Label ID="lblExperienceInYears" runat="server" Text='<%#Eval("ExperienceInYears") %>' Visible="false">
                                                                           </asp:Label>
                                                                            <asp:Label ID="lblPosition" runat="server" Text='<%#Eval("Position") %>' Visible="false">
                                                                           </asp:Label>
                                                                       </div>
                                                                   </td>
                                                            
                                                               </tr>
                                                               <tr>
                                                                   <td>
                                                                       <div class="hs_post">
                                                                           <asp:Button ID="btnEdit" runat="server" Text="Edit" CommandArgument='<%#Eval("EmployeeId_PK") %>' Visible="false" CommandName="EditEmployee" />

                                                                       </div>
                                                                   </td>
                                                                   <td>
                                                                       <div class="hs_post">
                                                                           <a class="fa fa-eraser">

                                                                                  <asp:ImageButton ID="ImgbtnRemove" runat="server"  CommandArgument='<%#Eval("EmployeeId_PK") %>' CommandName="DeleteEmployee" AlternateText="Remove" ImageUrl="~/images/button_cancel.png" />

                                                                           </a>
                                                                      
                                                                       </div>
                                                                   </td>
                                                               </tr>--%>
            <%--      </table>
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
            </asp:GridView>
        </div>--%>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
