﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using BE;
using DAL;
using System.Net.Mail;
using System.Net.Mime;
namespace HealToZeal.Company
{
    public partial class AddEmployees : System.Web.UI.Page
    {
        string CompanyId = "";
        EmployeeDAL objEmployeeDAL = new EmployeeDAL();
   //     string EmployeeId = "";
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                if (Session["CompanyId"] != null)
                {
                    BindCompanyEmployees();
                    BindCadre();
                }
            }
        }


        private void BindCadre()
        {
            try
            {
                 CompanyId = Session["CompanyId"].ToString();
                 DataTable DtCadre= objEmployeeDAL.CompanyCadreGetByCompanyId(CompanyId);

                 ddlCadre.DataSource = DtCadre;
                 ddlCadre.DataTextField = "Cadrename";
                 ddlCadre.DataValueField = "CadreId_PK";
                 ddlCadre.DataBind();

                 ddlCadre.Items.Insert(0, new ListItem("--SelectCadre--", "--Select Cadre--"));
                 ddlCadre.SelectedIndex = 0;
            }
            catch 
            {

            }
        }

        private void BindDeaprtment()
        {
            try
            {
                CompanyId = Session["CompanyId"].ToString();
                DataTable Dtdept = objEmployeeDAL.CompanyDeaprtmentGetByCompanyId(CompanyId);

                dddepartment.DataSource = Dtdept;
                dddepartment.DataTextField = "DepartmentName";
                dddepartment.DataValueField = "DeptId_PK";
                dddepartment.DataBind();

                dddepartment.Items.Insert(0, new ListItem("--Select Department--", "--Select Department--"));
                dddepartment.SelectedIndex = 0;
            }
            catch
            {

            }
        }

        private void BindCompanyEmployees()
        {
            try
            {
                CompanyId = Session["CompanyId"].ToString();
             DataTable DtEmployees= objEmployeeDAL.EmployeesGetByCompanyId(CompanyId,0);
             if (DtEmployees != null && DtEmployees.Rows.Count > 0)
             {
                 GvViewEmployee.DataSource = DtEmployees;
                 GvViewEmployee.DataBind();
             }
            }
            catch 
            {

            }
        }

        protected void btnAddEmployee_Click(object sender, EventArgs e)
        {
            try
            {
                InsertUpdateEmployee();
            }
            catch
            {

            }
        }

        private void InsertUpdateEmployee()
        {
            try
            {
                string password=Encryption.CreatePassword();
                EmployeeBE objEmployeeBE = new EmployeeBE();
                objEmployeeBE.propCompanyId_FK = CompanyId;
                objEmployeeBE.propEmployeeCompanyId =Convert.ToInt32(txtEmployeeCompanyId.Text);
                objEmployeeBE.propPassword =Encryption.Encrypt(password);
                objEmployeeBE.propFirstName = txtFirstName.Text;
                objEmployeeBE.propLastName = txtLastName.Text;        
                objEmployeeBE.propCadreId_FK = ddlCadre.SelectedItem.Value;
                //Code by Poonam on 25th july 2016
                objEmployeeBE.propDeptId_PK = dddepartment.SelectedItem.Value;
                //
                objEmployeeBE.propCreatedBy = CompanyId;
                objEmployeeBE.propCreatedDate = DateTime.Now.Date;              
                
                int Status = objEmployeeDAL.EmployeesInsert(objEmployeeBE);
                string message = string.Empty;
                if (Status > 0)
                {
                    string userName = txtEmployeeCompanyId.Text + "" + txtFirstName.Text;
                    message = "Employee added successfully...";
                    // email

                    System.Net.Mail.MailMessage insMail = new System.Net.Mail.MailMessage();
                  
                    System.Net.Mail.MailAddress insFrom = new MailAddress("info@planandprioritize.com");
                    insMail.From = insFrom;
                   
                    insMail.To.Add(txtEmailId.Text);

                    insMail.Subject = "You have been invited to  give the assessment .";

                    insMail.IsBodyHtml = true;
                   
                    string strBody = "<html><body><font color=\"black\"><b>Hello ,</b><BR>" + txtFirstName.Text +" "+ txtLastName.Text + "<BR>Your UserId is"+userName +" and passwoord is"+password+". <BR>Login to this link <BR> <a href='https://planandprioritize.com/frmUserLogIn.aspx' >http://planandprioritize.com/frmUserLogIn.aspx</a><BR><BR> <BR>Regards<BR>Let Me Talk2 Me.</font></body></html>";
                    insMail.Body = strBody;

                    System.Net.Mail.SmtpClient ns = new SmtpClient("relay-hosting.secureserver.net"); //"localhost";//"relay-hosting.secureserver.net";
                    ns.Send(insMail);

                }
                else
                {
                    message = "Employee can not be added...";
                }

               }              
           
            catch 
            {

            }
        }
    }
}