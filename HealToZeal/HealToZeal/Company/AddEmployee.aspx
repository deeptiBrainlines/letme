﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Company/Company.Master" AutoEventWireup="true" CodeBehind="AddEmployee.aspx.cs" Inherits="HealToZeal.Company.AddEmployee" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
      <style>
        .hs_pager1
        {
            display: inline-block;
            list-style: none;
            padding-left: 0px;
        }
        .hs_pager1 a
        {
            padding: 5px 12px;
            color: #7f9aa0;
            border: 1px solid #7f9aa0;
            text-align: center;
            font-weight: bold;
            font-size: 16px;
            -webkit-transition: all 0.5s;
            -moz-transition: all 0.5s;
            -ms-transition: all 0.5s;
            -o-transition: all 0.5s;
            margin-bottom: 60px;
        }
        .hs_pager1 a:hover, .hs_pager1 a:active
        {
            color: #00ac7a;
            border: 1px solid #00ac7a;
            -webkit-transition: all 0.5s;
            -moz-transition: all 0.5s;
            -ms-transition: all 0.5s;
            -o-transition: all 0.5s;
        }
        .pagination
        {
            line-height: 26px;
        }
        
        .pagination span
        {
            padding: 5px;
            border: solid 1px #477B0E;
            text-decoration: none;
            white-space: nowrap;
            background: #547B2A;
        }
        
        .pagination a, .pagination a:visited
        {
            text-decoration: none;
            padding: 6px;
            white-space: nowrap;
        }
        .pagination a:hover, .pagination a:active
        {
            padding: 5px;
            border: solid 1px #9ECDE7;
            text-decoration: none;
            white-space: nowrap;
            background: #486694;
        }
    </style>
    <style type="text/css">
        .style6
        {
            width: 125px;
        }
        .style7
        {
            height: 26px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="LeftColumn" runat="server">
    <%--  <div class="divClass" style="width:100%">
    
    <cc1:TabContainer ID="TabContainerEmployees" runat="server" ActiveTabIndex="1"  
            Width="100%"  BackColor="#f0f0f0"  >
    <cc1:TabPanel ID="TabPnlViewEmployee" runat="server" HeaderText="View Employees" BackColor="#f0f0f0">
         <ContentTemplate>
         <div class="divClass" >
         <asp:GridView ID="GvViewEmployee" runat="server" AutoGenerateColumns="False"  Width="100%"
                 CellPadding="6" CellSpacing="6" AllowPaging="True" 
                 onpageindexchanging="GvViewEmployee_PageIndexChanging" PageSize="3" 
                 AllowSorting="True" ><AlternatingRowStyle BackColor="#CDFECD" /><HeaderStyle BackColor="SkyBlue" /><Columns><asp:TemplateField HeaderText="CompanyId"><HeaderStyle BorderStyle="None" /><ItemStyle BorderStyle="None" /><ItemTemplate><asp:Label ID="lblEmployeeCompanyId" runat="server" Text='<%#Eval("Company") %>'>
             </asp:Label></ItemTemplate></asp:TemplateField ><asp:TemplateField HeaderText="Name"><HeaderStyle BorderStyle="None" /><ItemStyle BorderStyle="None" /><ItemTemplate><asp:Label ID="lblEmployeeName" runat="server" Text='<%#Eval("EmployeeName") %>'>
             </asp:Label></ItemTemplate></asp:TemplateField><asp:TemplateField HeaderText="Address"><HeaderStyle BorderStyle="None" /><ItemStyle BorderStyle="None" /><ItemTemplate><asp:Label ID="lblEmployeeAddress" runat="server" Text='<%#Eval("EmployeeAddress") %>'>
             </asp:Label></ItemTemplate></asp:TemplateField><asp:TemplateField HeaderText="Email id" Visible="False"><HeaderStyle BorderStyle="None" /><ItemStyle BorderStyle="None" /><ItemTemplate><asp:Label ID="lblEmailId" runat="server" Text='<%#Eval("EmailId") %>' Visible="false">
             </asp:Label></ItemTemplate></asp:TemplateField><asp:TemplateField HeaderText="Mobile no."><HeaderStyle BorderStyle="None" /><ItemStyle BorderStyle="None" /><ItemTemplate><asp:Label ID="lblMobileNo" runat="server" Text='<%#Eval("MobileNo") %>'>
             </asp:Label></ItemTemplate></asp:TemplateField><asp:TemplateField ><HeaderStyle BorderStyle="None" /><ItemStyle BorderStyle="None" /><ItemTemplate><asp:Label ID="lblEmployeeId" runat="server" Text='<%#Eval("EmployeeId_PK") %>' Visible="false"></asp:Label></ItemTemplate></asp:TemplateField><asp:TemplateField><HeaderStyle BorderStyle="None" /><ItemStyle BorderStyle="None" /><ItemTemplate><asp:Label ID="lblCompanyId_FK" runat="server" Text='<%#Eval("CompanyId_FK") %>' Visible="false" ></asp:Label></ItemTemplate></asp:TemplateField><asp:TemplateField><HeaderStyle BorderStyle="None" /><ItemStyle BorderStyle="None" /><ItemTemplate><asp:Button ID="btnEdit" runat="server" Text="Edit" CommandArgument='<%#Eval("EmployeeId_PK") %>' Visible="false" CommandName="EditEmployee" /></ItemTemplate></asp:TemplateField></Columns></asp:GridView></div></ContentTemplate>
    </cc1:TabPanel>

    <cc1:TabPanel ID="TabPnlAddUpdateEmployee" runat="server" HeaderText="Add/Update Employees" Width="100%" BackColor="#f0f0f0">
          <ContentTemplate>--%>
       <%--   <div  class="form-group">
                  <h4 class="hs_heading">
                  
                 Employee Profile
                  </h4>
          <table width="100%"  ><tr>
              <td colspan="2" >
              </td><td></td><td></td></tr><tr><td class="style6">
                      <asp:Label ID="lblCompanydomain" runat="server" Text="Cadre" ></asp:Label></td>
                      <td>
                          <asp:DropDownList ID="ddlCadre" runat="server" ValidationGroup="0" CssClass="form-control"></asp:DropDownList>

                      </td><td >
                           </td><td>

                                </td>

                                          </tr>
              <tr><td></td><td >&nbsp;</td><td>&nbsp;</td><td></td></tr><tr><td align="right" class="style6"><asp:Label ID="lblCompanyAddress" runat="server" Text="Employee Id"></asp:Label></td><td >
                      <asp:TextBox ID="txtEmployeeCompanyId" runat="server" ValidationGroup="0" CssClass="form-control"></asp:TextBox></td>
                      <td>&nbsp;</td><td >&nbsp;</td></tr><tr><td class="style6"></td><td><asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" 
                  ControlToValidate="txtEmployeeCompanyId" ErrorMessage="Enter Employee Id." 
                  ForeColor="Red" ValidationGroup="0"></asp:RequiredFieldValidator></td><td></td><td></td></tr><tr><td align="right" class="style6"><asp:Label ID="lblFirstName" runat="server" Text="First name"></asp:Label></td><td ><asp:TextBox ID="txtFirstName" runat="server" ValidationGroup="0" CssClass="txtLoginClass"></asp:TextBox></td><td>&nbsp;</td><td>&nbsp;</td></tr><tr><td class="style6"></td><td><asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" 
                   ControlToValidate="txtFirstName" ErrorMessage="Enter first name." 
                   ForeColor="Red" ValidationGroup="0"></asp:RequiredFieldValidator></td><td></td><td></td></tr><tr><td align="right" class="style6"><asp:Label ID="lblLastName" runat="server" Text="Last name"></asp:Label></td><td ><asp:TextBox ID="txtLastName" runat="server" ValidationGroup="0" CssClass="txtLoginClass"></asp:TextBox></td><td>&nbsp;</td><td >&nbsp;</td></tr><tr><td class="style6"></td><td><asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" 
                   ControlToValidate="txtLastName" ErrorMessage="Enter last name" ForeColor="Red" 
                   ValidationGroup="0"></asp:RequiredFieldValidator></td><td></td><td></td></tr><tr><td align="right" class="style6"><asp:Label ID="lblEmployeeEmailId" runat="server" Text="Email id"></asp:Label></td><td ><asp:TextBox ID="txtEmailId" runat="server" ValidationGroup="0" CssClass="txtLoginClass"></asp:TextBox></td><td ><br /></td></tr><tr><td class="style6"></td><td><asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" 
                   ControlToValidate="txtEmailId" ErrorMessage="Enter valid email id." 
                   ForeColor="Red" 
                   ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" 
                   ValidationGroup="0"></asp:RegularExpressionValidator><br /><asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" 
                   ControlToValidate="txtEmailId" ErrorMessage="Enter email id" ForeColor="Red" 
                   ValidationGroup="0"></asp:RequiredFieldValidator></td>
                       <td></td>
                       <td></td>                                                                                                                                                                                                                                                                                                                       </tr>
              <tr>
                       <td class="style6">&nbsp;</td>
                       <td >
                       <asp:Button ID="btnAddEmployee" runat="server" Text="Add employee"  CssClass="btn btn-default"
                        onclick="btnAddEmployee_Click" ValidationGroup="0"  /></td><td >&nbsp;</td><td>&nbsp;</td></tr><tr><td class="style6">&nbsp;</td><td >
                            <asp:Label ID="lblMessage" runat="server" ForeColor="Red"></asp:Label></td>
                            <td>&nbsp;</td><td>&nbsp;</td>
                     </tr>
          </table>
          </div>--%>
    <h4 class="hs_heading" id="hs_appointment_form_link">Add employee </h4>
     <div class="container">
                <div class="row">
                    <table style="width: 100%">
                        <tr>
                            <td style="width: 100%; text-align:left">
                                   <div class="col-lg-8 col-md-8 col-sm-7">
                                    <div class="hs_comment_form">
                                        <div class="row">   
                                                 <div class="col-lg-6 col-md-6 col-sm-12">
                                                     <div class="input-group">
                                                         <span class="input-group-btn"></span>
                                                         <asp:DropDownList ID="ddDepartment" runat="server" CssClass="form-control">
                                                         </asp:DropDownList>
                                                     </div>
                                                    </div>               
                                            </div>
                                        <div class="row">   
                                                 <div class="col-lg-6 col-md-6 col-sm-12">
                                                     <div class="input-group">
                                                         <span class="input-group-btn"></span>
                                                         <asp:DropDownList ID="ddlCadre" runat="server" CssClass="form-control">
                                                         </asp:DropDownList>
                                                     </div>
                                                    </div>               
                                            </div>
                                        <div class="row">   
                                                 <div class="col-lg-6 col-md-6 col-sm-12">
                                                     <div class="input-group">
                                                         <span class="input-group-btn"></span>
                                                         <asp:DropDownList ID="ddManger" runat="server" CssClass="form-control">
                                                         </asp:DropDownList>
                                                     </div>
                                                    </div>               
                                            </div>
                                          <div class="row">   
                                                 <div class="col-lg-6 col-md-6 col-sm-12">
                                                     <div class="input-group">
                                                         <span class="input-group-btn"></span>
                                                         <asp:DropDownList ID="ddlBatch" runat="server" CssClass="form-control">
                                                         </asp:DropDownList>
                                                     </div>
                                                    </div>               
                                            </div>
                                         <div class="row">   
                                                 <div class="col-lg-6 col-md-6 col-sm-12">
                                                        <div class="input-group">
                                                            <span class="input-group-btn">                                                            
                                                            </span>
                                                              <input type="text" id="EmployeeCompanyId" runat="server" name="EmployeeCompanyId"
                                            class="form-control" placeholder="Employee company id (required)" required />
                                                            </div>
                                                     </div>
                                        </div>
                                          <div class="row">   
                                                 <div class="col-lg-6 col-md-6 col-sm-12">
                                                        <div class="input-group">
                                                            <span class="input-group-btn">                                                            
                                                            </span>
                                                               <input type="text" id="FirstName" runat="server" name="fname" class="form-control"
                                            placeholder="First Name (required)" required />
                                                            </div>
                                                     </div>
                                        </div>
                                          <div class="row">   
                                                 <div class="col-lg-6 col-md-6 col-sm-12">
                                                        <div class="input-group">
                                                            <span class="input-group-btn">                                                            
                                                            </span>
                                                             <input id="LastName" name="lname" runat="server" class="form-control" placeholder="Last Name (required)"
                                            required />
                                                            </div>
                                                     </div>
                                        </div>
                                          <div class="row">   
                                                 <div class="col-lg-6 col-md-6 col-sm-12">
                                                        <div class="input-group">
                                                            <span class="input-group-btn">                                                            
                                                            </span>
                                                            <input type="text" id="EmailId" runat="server" name="EmailID" class="form-control"
                                            placeholder="Email (required)" required />
                                                            </div>
                                                     </div>
                                        </div>
                                    
                                          <div class="row">   
                                                 <div class="col-lg-6 col-md-6 col-sm-12">
                                                        <div class="input-group">
                                                            <span class="input-group-btn">                                                            
                                                            </span>
                                                             <asp:Button ID="btnAddEmployee" runat="server" Text="Add employee" CssClass="btn btn-success pull-right"
                                            OnClick="btnAddEmployee_Click" />
                                                            </div>
                                                     </div>
                                        </div>
                                        <div class="row">
                                              <div id="divalert" class="alert alert-success" runat="server">
                           <strong> <asp:Label ID="lblMessage" runat="server" ></asp:Label></strong>
                        </div>
                                        </div>
                         </div>
                         </div>
                            </td>
                        </tr>
                    </table>
                  
                    </div>
         </div>
  

</asp:Content>
