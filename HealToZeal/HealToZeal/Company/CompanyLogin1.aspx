﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="CompanyLogin1.aspx.cs" Inherits="HealToZeal.Company.CompanyLogin1" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="keywords" content="Health Care, health" />
    <meta name="description" content="Health Care HTML Template" />
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <!-- favicon links -->
    <link rel="shortcut icon" type="image/ico" href="../favicon.ico" />
    <link rel="icon" type="image/ico" href="../favicon.ico" />
    <!--Google web fonts-->
    <link href='http://fonts.googleapis.com/css?family=Raleway' rel='stylesheet' type='text/css' />

    <!-- main css -->
    <link rel="stylesheet" href="../css/main.css" media="screen" />
    <%--<link rel="stylesheet" id="theme-color" type="text/css" href="#"/>--%>
</head>
<body>

   <div class="col-lg-6 col-md-5 col-sm-12">
    
      <div class="hs_appointment_form_div" style="margin-left:20%; margin-top:20%;  width:70% ; height:70%"> <img src="http://placehold.it/512x345"  alt=""/>
        <div class="hs_appointment_form">
                        <form id="form1" runat="server" class="form-inline">
                              <h4 class="hs_heading" id="hs_appointment_form_link">Log in</h4>
                                           
                  <div class="row">
              <div class="col-lg-6 col-md-7 col-sm-6">
                     
         
                 
                    <div class="form-group">
                          <asp:Label ID="lblUserName" runat="server" Text="Username"></asp:Label>
                        <asp:TextBox ID="txtUserName" runat="server" ValidationGroup="0" CssClass="form-control" ></asp:TextBox>

                    </div>
                   
                    <div class="form-group">
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server"
                            ControlToValidate="txtUserName" ErrorMessage="Enter username." ForeColor="Red"
                            ValidationGroup="0"></asp:RequiredFieldValidator>
                    </div>
                 <br />
                    <div class="form-group">
                        <asp:Label ID="lblPassword" runat="server" Text="Password"></asp:Label>
               
            <asp:TextBox ID="txtPassword" runat="server" TextMode="Password" CssClass="form-control"
                ValidationGroup="0"></asp:TextBox>
        </div>
 
              <div class="form-group">
                  <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server"
                      ControlToValidate="txtPassword" ErrorMessage="Enter password." ForeColor="Red"
                      ValidationGroup="0"></asp:RequiredFieldValidator>
              </div>
                  </div>
                      </div>  
              
       <div class="row">
              <div class="col-lg-3 col-md-4 col-sm-3">
                                <asp:Button ID="btnLogin" runat="server" Text="Login" CssClass="btn btn-default"
            onclick="btnLogin_Click" ValidationGroup="0" />
                      <%--      <asp:ImageButton ID="ImgBtnLogin" runat="server" ImageUrl="~/images/loginimg.jpg"
                                Height="40px" Width="134px" OnClick="ImgBtnLogin_Click" />--%>
                 </div>
           </div>
                              <div class="row">
                               <div class="col-lg-8 col-md-8 col-sm-8">
                            <asp:Label ID="lblmessage" runat="server" ForeColor="Red" ></asp:Label>
                                   </div>
                 </div>
                </form>
            </div>
      </div>
   </div>
    

           
</body>
</html>
