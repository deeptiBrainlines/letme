﻿using DAL;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BE;


namespace HealToZeal.Company
{
    public partial class ImportClusterwise : System.Web.UI.Page
    {
        string ItemFilePath = "";
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        private void ImportDatafromExcel(string FileLocation)
        {
            ImportExportToExcel importExportToExcel = new ImportExportToExcel();
            DataTable dtData = importExportToExcel.ExcelToDatatable(FileLocation);
            ImportMemberData(dtData);

        }


        // public string PropQuestion { get; set; }
        private void ImportMemberData(DataTable dtData)
        {
            try
            {
                int rownumber = default(int);
                //if (Session["AdminId"] != null)
                //{
                //    adminId = Session["AdminId"].ToString();

                EmployeeAssessmentDetailsBE objQuestionMasterBE1 = new EmployeeAssessmentDetailsBE();
                EmployeeAssessmentDetailsDAL ObjEmployeeAssessmentDetailsDAL = new EmployeeAssessmentDetailsDAL();
                AnswerMasterDAL objAnswerMasterDAL = new AnswerMasterDAL();
                for (int i = 1; i < dtData.Columns.Count; i++)
                {
                    for (rownumber = 1; rownumber < dtData.Rows.Count; rownumber++)
                    {
                        DataTable dt = objAnswerMasterDAL.GetClusterID(dtData.Rows[rownumber][0].ToString());
                        if (dt.Rows.Count > 0)
                        {
                           
                            objQuestionMasterBE1.propClusterID = dt.Rows[0]["ClusterID_PK"].ToString();

                            objQuestionMasterBE1.propClusterScore = dtData.Rows[rownumber][i].ToString();
                            DataTable dtAssessment = objAnswerMasterDAL.GetAssessmentID((dtData.Rows[rownumber]["Assessment"].ToString()));

                            objQuestionMasterBE1.propAssessmentId_FK = dtAssessment.Rows[0]["AssessmentId_PK"].ToString();
                            objQuestionMasterBE1.propEmployeeId_FK = dtData.Rows[0][i].ToString();
                            objQuestionMasterBE1.propCreatedBy = dtData.Rows[0][i].ToString();
                            /// objQuestionMasterBE1.propInstanceCompanyRelationId_FK = "600A89C7-B144-4ED6-B1E8-8C8FFF6417DE";
                            objQuestionMasterBE1.propInstanceCompanyRelationId_FK = "BD293E6B-12D2-4F88-A360-6D274E6FC1D4";
                            int status = ObjEmployeeAssessmentDetailsDAL.EmployeeAssessmentDetailsImportClusterWise(objQuestionMasterBE1);

                        }
                        // }

                    }
                }
                Response.Write("Data Imported Sucessfully");
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }


        private void ValidateData(string FileLocation)
        {
            try
            {
                Boolean hasError = false;

                ImportExportToExcel importExportToExcel = new ImportExportToExcel();
                DataTable DtExcelDataTable = importExportToExcel.ExcelToDatatable(FileLocation);
                Session["DtExcelDataTable"] = DtExcelDataTable;

                if (!hasError)
                {
                    ImportDatafromExcel(FileLocation);
                    ImportExportToExcel ObjImport = new ImportExportToExcel();

                }
            }
            catch (Exception )
            {

            }
        }

        protected void btnimport_Click(object sender, EventArgs e)
        {
            try
            {

                if (exampleInputFile.HasFile)
                {
                    string ext = System.IO.Path.GetExtension(exampleInputFile.PostedFile.FileName).ToLower();
                    if (ext == ".xls" || ext == ".xlsx")
                    {


                        if (!System.IO.Directory.Exists(Server.MapPath("Data" + ItemFilePath)))
                        {
                            System.IO.Directory.CreateDirectory(Server.MapPath("Data" + ItemFilePath));
                        }

                        String datetime = DateTime.Now.Ticks.ToString();
                        String fileExt = System.IO.Path.GetExtension(exampleInputFile.FileName);


                        exampleInputFile.SaveAs(Server.MapPath("Data" + ItemFilePath) + "\\" + datetime + fileExt);
                        string path = Server.MapPath("Data" + ItemFilePath) + "\\" + datetime + fileExt;

                        if (path != "")
                        {
                            ValidateData(path);
                            exampleInputFile.Enabled = true;
                            btnimport.Enabled = true;

                        }
                    }
                    else
                    {
                    }
                }
                else
                {

                }
            }
            catch (Exception )
            {

            }
        }
    }
}