﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using BE;
using DAL;
using System.Net.Mime;
using System.Web.Mail;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Configuration;

namespace HealToZeal.Company
{
    public partial class AddEmployee : System.Web.UI.Page
    {
        string CompanyId = "";
        EmployeeDAL objEmployeeDAL = new EmployeeDAL();
        string EmployeeId = "";
        protected void Page_Load(object sender, EventArgs e)
        {
            divalert.Visible = false;
            if (!Page.IsPostBack)
            {
                if (Session["CompanyId"] != null)
                {
                    // BindCompanyEmployees(0);
                    BindCadre();
                    BindDepartment();
                    BindManager();
                    Bindbatch();
                }
            }
        }

        private void BindCadre()
        {
            try
            {

                CompanyId = Session["CompanyId"].ToString();
                DataTable DtCadre = objEmployeeDAL.CompanyCadreGetByCompanyId(CompanyId);

                ddlCadre.DataSource = DtCadre;
                ddlCadre.DataTextField = "Cadrename";
                ddlCadre.DataValueField = "CadreId_PK";
                ddlCadre.DataBind();

                ddlCadre.Items.Insert(0, new ListItem("--SelectCadre--", "--Select Cadre--"));
                ddlCadre.SelectedIndex = 0;
            }
            catch
            {

            }
        }

        private void  Bindbatch()
        {
            try
            {

                CompanyId = Session["CompanyId"].ToString();
                DataTable Dtdbatch = objEmployeeDAL.GetBatchName(CompanyId);

                ddlBatch.DataSource = Dtdbatch;
                ddlBatch.DataTextField = "BatchName";
                ddlBatch.DataValueField = "BatchID_PK";
                ddlBatch.DataBind();

                ddlBatch.Items.Insert(0, new ListItem("--SelectBatch--", "--Select Batch--"));
                ddlBatch.SelectedIndex = 0;
            }
            catch
            {

            }
        }
        private void BindManager()
        {
            try
            {

                CompanyId = Session["CompanyId"].ToString();
                DataTable DtCadre = objEmployeeDAL.EmployeeDetails(CompanyId);

                ddManger.DataSource = DtCadre;
                ddManger.DataTextField = "FullName";
                ddManger.DataValueField = "EmployeeId_PK";
                ddManger.DataBind();

                ddManger.Items.Insert(0, new ListItem("--Manager--", "--Select Manager--"));
                ddManger.SelectedIndex = 0;
            }
            catch
            {

            }
        } 

        //Code by Poonam On 25 july 2016
        private void BindDepartment()
        {
            try
            {

                CompanyId = Session["CompanyId"].ToString();
                DataTable DtDept = objEmployeeDAL.CompanyDepartmentGetByCompanyId(CompanyId);

                ddDepartment.DataSource = DtDept;
                ddDepartment.DataTextField = "Departmentname";
                ddDepartment.DataValueField = "DeptId_PK";
                ddDepartment.DataBind();

                ddDepartment.Items.Insert(0, new ListItem("--Select Department--", "--Select Department--"));
                ddDepartment.SelectedIndex = 0;
            }
            catch
            {

            }
        }
        //private void BindCompanyEmployees(int Flag)
        //{
        //    try
        //    {
        //        CompanyId = Session["CompanyId"].ToString();
        //        DataTable DtEmployees = objEmployeeDAL.EmployeesGetByCompanyId(CompanyId,Flag);
        //        if (DtEmployees != null && DtEmployees.Rows.Count > 0)
        //        {
        //            GvViewEmployee.DataSource = DtEmployees;
        //            GvViewEmployee.DataBind();
        //        }
        //    }
        //    catch
        //    {

        //    }
        //}

        protected void btnAddEmployee_Click(object sender, EventArgs e)
        {
            try
            {
                InsertUpdateEmployee();
            }
            catch
            {

            }
        }

        private void InsertUpdateEmployee()
        {
            try
            {
                //insert
                string password = Encryption.CreatePassword();

                EmployeeBE objEmployeeBE = new EmployeeBE();
                string userName = Encryption.CreateUsername();//txtEmployeeCompanyId.Text + "" + txtFirstName.Text;
                CompanyId = Session["CompanyId"].ToString();
                objEmployeeBE.propCompanyId_FK = CompanyId;
                objEmployeeBE.propEmployeeCompanyId = Convert.ToInt32(EmployeeCompanyId.Value);
                objEmployeeBE.propUserName = userName;
                objEmployeeBE.propPassword = Encryption.Encrypt(password);
                objEmployeeBE.propFirstName = FirstName.Value;
                objEmployeeBE.propLastName = LastName.Value;
                //objEmployeeBE.propEmailId = Encryption.Encrypt(EmailId.Value);
                objEmployeeBE.propEmailId = EmailId.Value;
                //objEmployeeBE.propCadreId_FK = ddlCadre.SelectedItem.Value;
                objEmployeeBE.propUpdatedDate = DateTime.Now;
               

                if (ddlCadre.SelectedValue.ToString() != "--SelectCadre--")

                {
                    objEmployeeBE.propCadreId_FK = ddlCadre.SelectedItem.Value;
                }
                else
                {
                    objEmployeeBE.propCadreId_FK = string.Empty;
                }


                
                //code by Poonam
                //   objEmployeeBE.propDeptId_PK = ddDepartment.SelectedItem.Value;
                if (ddDepartment.SelectedValue.ToString() != "--Select Department--")
                {
                    objEmployeeBE.propDeptId_PK = ddDepartment.SelectedItem.Value;
                }
                else
                {
                    objEmployeeBE.propDeptId_PK = string.Empty;
                }



                if (ddlBatch.SelectedValue.ToString() != "--SelectBatch--")
                {
                    objEmployeeBE.PropBatchID = ddlBatch.SelectedItem.Value;
                }
                else
                {
                    objEmployeeBE.PropBatchID = string.Empty;
                }
                objEmployeeBE.propCreatedBy = CompanyId;


               // objEmployeeBE.propCreatedDate = DateTime.Now.Date;
                // string message = "";
                int Status = 0;
                divalert.Visible = true;
              
                if (Session["EmployeeId"] != null)
                {
                    EmployeeId = Session["EmployeeId"].ToString();
                    Status = objEmployeeDAL.EmployeesUpdateByCompanyId(objEmployeeBE);
                }
                else
                {
                    
                        Status = objEmployeeDAL.EmployeesInsert(objEmployeeBE);


                    InsertManager();
                    lblMessage.Text = "Employee added successfully...";

                    if (Status > 0)
                    {  //added by meenakshi
                        SendEmailNewTemplate(password, userName);

                        ClearEmployeeDetails();
                        //message = "Employee added successfully...";
                        divalert.Attributes.Add("class", "alert alert-success");
                        lblMessage.Text = "Employee added successfully...";

                        //MailMessage mail = new MailMessage();
                        //// System.Web.Mail.MailMessage mail = new System.Web.Mail.MailMessage();
                        //mail.To = EmailId.Value;
                        //mail.Bcc = "priyamvada.bavadekar@letmetalk2.me";
                        //mail.From = "contact@letmetalk2.me";
                        //mail.Subject = "Welcome to the Health and Personality Insights and Actions Tool.";
                        //string getDt = DateTime.Now.Date.AddDays(7).ToString("MM/dd/yyyy");

                        //mail.Body = "<html><body><font color=\"black\">Dear " + FirstName.Value + " " + LastName.Value + ",<br/><br/>Welcome to <b> Let Me Talk2 Me: A Health and Personality Insights and Actions Tool.</b> You will be able to gain some useful insights into your own personality and learn about remedial actions in case they are needed. <b>'Let Me Talk2 Me'</b> will help you in self-discovery. It will lead on a path of self development while avoiding pitfalls of modern complex life and careers. <br/><br/>It is recommended that you use this tool periodically to monitor yourself and be aware of your health and personality parameters. <br/><br/> Please use the link below to login <br/><br/><a href='http://chetanatspl.com/Healtozealcurrent/Employee/EmployeeLogin.aspx?first=1'> http://chetanatspl.com/Healtozealcurrent/Employee/EmployeeLogin.aspx </a><br/>Your login details are as follows:<br/>UserId:<b>" + userName + "</b><br/>Password:<b>" + password + "</b><br/><br/>If you would like to take a break during assessment, please make sure you log out from the system or close the browser window for security purposes. Please do not leave the browser idle. You can always log back in whenever you like.<br/><br/>Please ensure that you complete this assessment on or before " + getDt + ". To get a maximum benefit, please repeat this exercise every 3 months and study the differentials.<br/> You will be notified for the same when the next assessment is due.<br/><br/> Thank You.<br/><br/></font></body></html>";
                        //mail.BodyFormat = MailFormat.Html;
                        //mail.Fields.Add("http://schemas.microsoft.com/cdo/configuration/smtpauthenticate", "1");    //basic authentication
                        //mail.Fields.Add("http://schemas.microsoft.com/cdo/configuration/sendusername", "HealToZeal@chetanatspl.com"); //set your username here
                        //mail.Fields.Add("http://schemas.microsoft.com/cdo/configuration/sendpassword", "HealToZeal");    //set your password here

                        //SmtpMail.SmtpServer = "hazel.arvixe.com";  //your real server goes here                        
                        //SmtpMail.Send(mail);

                        //ClearEmployeeDetails();
                        ////message = "Employee added successfully...";
                        //divalert.Attributes.Add("class", "alert alert-success");
                        //lblMessage.Text = "Employee added successfully...";


                        //added by meenakshi
                        //string userId = userName;
                        //string password1 =password;
                        //System.Net.Mail.MailMessage mail = new System.Net.Mail.MailMessage();
                        //mail.To.Add(EmailId.Value);

                        //  mail.From = new MailAddress("", "", System.Text.Encoding.UTF8);

                        //mail.Subject = "Welcome to the Health and Personality Insights and Actions Tool.";
                        //mail.SubjectEncoding = System.Text.Encoding.UTF8;

                        //string body = "Unique UserId: " + userId + " new Password is: " + password1;

                        //mail.Body = body;

                        //mail.BodyEncoding = System.Text.Encoding.UTF8;

                        //mail.IsBodyHtml = true;
                        //mail.Priority = System.Net.Mail.MailPriority.High;
                        //SmtpClient client = new SmtpClient();
                        //client.Credentials = new System.Net.NetworkCredential("");
                        //client.Port = 587;
                        //client.Host = "smtp.gmail.com";
                        //client.EnableSsl = false;

                        //client.Send(mail);

                    }
                    else
                    {
                        //    divalert.Attributes.Add("class", "alert alert-danger");
                        //message = "Employee can not be added...";
                        lblMessage.Text = "Employee can not be added...";
                    }
                    lblMessage.Text = "Employee  added...";

                }
                //lblMessage.Text = message;
            }
            catch (Exception ex)
            {
                ex.InnerException.ToString();
            }
        }

        private void SendEmail(string password, string userName)
        {
            System.Net.Mail.MailMessage mail = new System.Net.Mail.MailMessage();
            // System.Web.Mail.MailMessage mail = new System.Web.Mail.MailMessage();

            mail.To.Add(EmailId.Value);
            mail.CC.Add("contact@letmetalk2.me");
            mail.Bcc.Add("priyamvada.bavadekar@letmetalk2.me");
            // mail.From = "contact@letmetalk2.me";
            // mail.From = new MailAddress("contact@letmetalk2.me", "Let me talk to me", System.Text.Encoding.UTF8);
            mail.Subject = "Welcome to the Health and Personality Insights and Actions Tool.";
            string getDt = DateTime.Now.Date.AddDays(7).ToString("MM/dd/yyyy");

            mail.Body = "<html><body><font color=\"black\">Dear " + FirstName.Value + " " + LastName.Value + ",<br/><br/>Welcome to <b> Let Me Talk To Me: A tool to quickly and discreetly assess your mindfulness.</b> You will be able to gain some useful insights into your own personality and learn about remedial actions in case they are needed. <b>'Let Me Talk2 Me'</b> will help you in self-discovery. It will lead on a path of self reflections while avoiding pitfalls of modern complex life and careers. <br/><br/>It is recommended that you use this tool periodically to monitor yourself and be aware of your health and personality parameters. <br/><br/> Please use the link below to login <br/><br/><a href='http://letmetalk2.me.hazel.arvixe.com/Employee/EmployeeLogin.aspx?first=1'> http://letmetalk2.me.hazel.arvixe.com/Employee/EmployeeLogin.aspx </a><br/>Your login details are as follows:<br/>UserId:<b>" + userName + "</b><br/>Password:<b>" + password + "</b><br/><br/>If you would like to take a break during assessment, please make sure you log out from the system or close the browser window for security purposes. Please do not leave the browser idle. You can always log back in whenever you like.<br/><br/>Please ensure that you complete this assessment on or before " + getDt + ". To get a maximum benefit, please repeat this exercise every 3 months and study the differentials.<br/> You will be notified for the same when the next assessment is due.<br/><br/>If you wish to join the ecosystem of mindfulness, please pre-register with us.<a href='http://letmetalk2.me/pre-register'>http://letmetalk2.me/pre-register</a></br>Thank You.<br/><br/></font></body></html>";
            mail.IsBodyHtml = true;

            mail.Priority = System.Net.Mail.MailPriority.High;
            SmtpClient client = new SmtpClient();
            // client.Credentials = new System.Net.NetworkCredential("contact@letmetalk2.me", "contact123");
            //  client.Port = 587;
            // client.Host = "dallas137.arvixeshared.com";
            client.EnableSsl = false;



            client.Send(mail);
        }

        private void SendEmailNewTemplate(string password, string userName)
        {
            //string DefaultImagePath = HttpContext.Current.Server.MapPath(@"~\UIFolder\dist\img\logotemp.png");
            //byte[] imageArray = System.IO.File.ReadAllBytes(DefaultImagePath);
            //string base64ImageRepresentation = Convert.ToBase64String(imageArray);
            System.Net.Mail.MailMessage mail = new System.Net.Mail.MailMessage();

            mail.To.Add(EmailId.Value);
            mail.CC.Add("contact@letmetalk2.me");
            mail.Bcc.Add("priyamvada.bavadekar@letmetalk2.me");
            mail.Subject = "Welcome to the Health and Personality Insights and Actions Tool.";
            string getDt = DateTime.Now.Date.AddDays(7).ToString("MM/dd/yyyy");
            StringBuilder body = new StringBuilder();
            body.AppendLine("<html><body><table width='100%' border='0' align='center' cellpadding='0' cellspacing='0' style='border: 2px solid #b3c8c8 !important'>");
            body.AppendLine("<tr><td><table width='100%' border='0' cellspacing='0' cellpadding='0'><tr>");
            body.AppendLine("<td valign='top'><table width='90%' border='0' align='center' cellpadding='0' cellspacing='0'> <tr>");
            body.AppendLine("<td  style='font-family:Verdana, Arial, Helvetica, sans-serif; font-size:12px; line-height:1.5; text-align:justify;'>&nbsp;</td>");
            body.AppendLine("</tr><tr><td  style='font-family:Verdana, Arial, Helvetica, sans-serif;  font-size:13px; line-height:1.8; text-align:justify;'><div style='font-size:18px; color:#5b6766; font-weight:800;'>Dear "+FirstName.Value+" ("+FirstName.Value+"), </div>");
            body.AppendLine("<p><span  style='color:#829a9a; font-weight:800; font-size:16px; margin-bottom:0; padding-bottom:0;'>Welcome to Let Me Talk To Me :<br /></span><span style='font-size:13px; color:#829a9a; font-weight:800;'> A Health and Personality Insights and Actions Tool</span>");
            body.AppendLine(" <hr />Your organization will be able to gain some useful insights into your team's mental health well being and learn about remedial actions in case they are needed. <strong>'Let Me Talk To Me'</strong> will help your employees in self-discovery. It will lead on a path of self development while avoiding pitfalls of modern complex life and careers.<br /><br />");
            body.AppendLine("It is recommended that one  uses this tool periodically to monitor self and be aware of one's health and  personality parameters. <br />");
            body.AppendLine("</p><p style='color:#829a9a; font-weight:800; font-size:14px; margin-bottom:0; padding-bottom:0;'>Your login details are as follows</p>");
            body.AppendLine("<strong>User Id:</strong> "+userName+ "<br /><strong>Password:</strong> "+password+"");
            body.AppendLine("<div style='clear: both; '></div>"); 
            body.AppendLine("<p style='color:#829a9a; font-weight:800; font-size:14px; margin-bottom:0; padding-bottom:0;'>Please use the link below to login </p>");
            body.AppendLine("<a href='"+ ConfigurationManager.AppSettings.Get("Loginurl") + "?first=1'  target='_blank' style='background-color: #5b756c;padding:8px 10px;text-align:center; text-decoration:none; display: inline-block; font-size:15px; color: #fff;border-radius: 25px; margin-top:10px;'>letmetalk2.me</a>");
            body.AppendLine("</td></tr><tr><td  style='font-family:Verdana, Arial, Helvetica, sans-serif; font-size:12px; line-height:1.5; text-align:justify;'>&nbsp;</td>");
            body.AppendLine("</tr></table></td></tr><tr><td height='140' bgcolor='#b3c8c8' style='font-family:Verdana, Arial, Helvetica, sans-serif; font-size:20px; color:#fff;'><table width='90%' border='0' align='center' cellpadding='0' cellspacing='0'>");
            body.AppendLine("<tr><td width='23%'><img src='" + ConfigurationManager.AppSettings.Get("ImagePath") + @"\UIFolder\dist\img\logotemp.png' width ='120' height='122' />");
            body.AppendLine("<td width='2%'>&nbsp;</td><td width='75%' align='left'><table width='100%' border='0' align='left' cellpadding='0' cellspacing='0'>");
            body.AppendLine("<tr><td style='color:#5b756c'><strong>let me talk team</strong></td></tr>");
            body.AppendLine("<tr><td height='28'style='font-size:14px; border-bottom:1px dotted #5b756c; padding-bottom:5px; color:#5b756c'>self test program</td>");
            body.AppendLine("</tr><tr><td style='font-size:12px; padding-top:5px; color:#5b756c'>https://www.letmetalk2.me/</td></tr><tr>");
            body.AppendLine("<td height='20' style='font-size:12px; color:#5b756c'><span style='font-size:12px; padding-top:5px;'>©2020 let me talk. all rights reserved.</span></td>");
            body.AppendLine("</tr></table></td></tr></table></td></tr></table></td></tr></table></body></html>");

            mail.Body = body.ToString();
            //mail.Body = "<html><body><font color=\"black\">Dear " + FirstName.Value + " " + LastName.Value + ",<br/><br/>Welcome to <b> Let Me Talk2 Me: A tool to quickly and discreetly assess your mindfulness.</b> You will be able to gain some useful insights into your own personality and learn about remedial actions in case they are needed. <b>'Let Me Talk2 Me'</b> will help you in self-discovery. It will lead on a path of self reflections while avoiding pitfalls of modern complex life and careers. <br/><br/>It is recommended that you use this tool periodically to monitor yourself and be aware of your health and personality parameters. <br/><br/> Please use the link below to login <br/><br/><a href='http://letmetalk2.me.hazel.arvixe.com/Employee/EmployeeLogin.aspx?first=1'> http://letmetalk2.me.hazel.arvixe.com/Employee/EmployeeLogin.aspx </a><br/>Your login details are as follows:<br/>UserId:<b>" + userName + "</b><br/>Password:<b>" + password + "</b><br/><br/>If you would like to take a break during assessment, please make sure you log out from the system or close the browser window for security purposes. Please do not leave the browser idle. You can always log back in whenever you like.<br/><br/>Please ensure that you complete this assessment on or before " + getDt + ". To get a maximum benefit, please repeat this exercise every 3 months and study the differentials.<br/> You will be notified for the same when the next assessment is due.<br/><br/>If you wish to join the ecosystem of mindfulness, please pre-register with us.<a href='http://letmetalk2.me/pre-register'>http://letmetalk2.me/pre-register</a></br>Thank You.<br/><br/></font></body></html>";
            mail.IsBodyHtml = true;

            mail.Priority = System.Net.Mail.MailPriority.High;
            SmtpClient client = new SmtpClient();
            client.EnableSsl = false;
            client.Send(mail);
        }

        private void InsertManager()
        {
            EmployeeBE objEmployeeBE = new EmployeeBE();

            objEmployeeBE.propFirstName = FirstName.Value;
            objEmployeeBE.propLastName = LastName.Value;
             
           if(ddManger.SelectedValue.ToString()!="--Manager--")
           {
            objEmployeeBE.propMangerID = ddManger.SelectedItem.Value;
           }
           else
           {
               objEmployeeBE.propMangerID=string.Empty;
           }
            int status = objEmployeeDAL.EmployeesInsertManager(objEmployeeBE);
        }

        private void ClearEmployeeDetails()
        {
            EmailId.Value = "";
            EmployeeCompanyId.Value = "";
            FirstName.Value = "";
            LastName.Value = "";
            ddlCadre.SelectedIndex = 0;
            ddDepartment.SelectedIndex = 0;
        }



        //protected void GvViewEmployee_PageIndexChanging(object sender, GridViewPageEventArgs e)
        //{
        //    try
        //    {
        //        GvViewEmployee.PageIndex = e.NewPageIndex;
        //        BindCompanyEmployees(0);
        //    }
        //    catch 
        //    {

        //    }
        //}


        //protected void GvViewEmployee_RowCommand(object sender, GridViewCommandEventArgs e)
        //{
        //    try
        //    {
        //        EmployeeId  = e.CommandArgument.ToString();
        //        //add session
        //        Session["EmployeeId"] = EmployeeId ;
        //        if (e.CommandName == "EditEmployee")
        //        {
        //            BindQuestion();
        //        }

        //    }
        //    catch 
        //    {

        //    }
        //}
    }
}