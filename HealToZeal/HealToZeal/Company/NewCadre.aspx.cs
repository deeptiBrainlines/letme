﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DAL;
using BE;
using System.Data;

namespace HealToZeal.Company
{
    public partial class NewCadre : System.Web.UI.Page
    {
        CompanyCadreDAL ObjCadreDAL = new CompanyCadreDAL();
        string CompanyId = "";

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Session["CompanyId"] != null)
                {
                    //BindCadre();
                    BindCompanyCadre(0);
                    divalert.Visible = false;
                }
            }
        }

        private void BindCompanyCadre(int Flag)
        {
            try
            {
                CompanyId = Session["CompanyId"].ToString();
                DataTable DtCadre = ObjCadreDAL.CompanyCadreGetByCompanyId(CompanyId);              
               
                if (DtCadre != null && DtCadre.Rows.Count > 0)
                {
                    GvCadre.DataSource = DtCadre;
                    GvCadre.DataBind();
                }                
            }
            catch
            {

            }
        }

        protected void BtnAddNew_Click(object sender, EventArgs e)
        {
            try
            {
                PnlAddNewCadre.Visible = true;
                BtnSubmit.Visible = true;
                BtnUpdate.Visible = false;
            }
            catch (Exception )
            {

            }
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            try
            {
            }
            catch(Exception )
            {

            }
        }

        protected void BtnRefreshAll_Click(object sender, EventArgs e)
        {
            try
            {
            }
            catch (Exception )
            {

            }
        }

        protected void GvCadre_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
            }
            catch (Exception )
            {

            }
        }

        protected void GvCadre_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                string CadreId = e.CommandArgument.ToString();
                //add session
                //   Session["EmployeeId"] = EmployeeId;
                if (e.CommandName == "DeleteCadre")
                {
                    CompanyId = Session["CompanyId"].ToString();
                    CompanyCadreDAL objCadreDAL = new CompanyCadreDAL();
                    int status = objCadreDAL.CadreDelete(CadreId, CompanyId);

                    divalert.Visible = true;
                    if (status > 0)
                    {
                        lblMessage.Text = "Cadre removed successfully...";
                        BindCompanyCadre(0);
                    }
                    else
                    {
                        divalert.Attributes.Add("class", "alert alert-danger");
                        lblMessage.Text = "Can not remove cadre...";
                    }
                    //DeleteEmployee(EmployeeId, CompanyId);
                }
                else if(e.CommandName=="ViewCadre")
                {
                    
                    DataTable DtCadre = ObjCadreDAL.CompanyCadreGetByCadreId(CadreId);
                    if(DtCadre!=null)
                    {
                        if(DtCadre.Rows.Count>0)
                        {
                            PnlAddNewCadre.Visible = true;
                            BtnUpdate.Visible = true;
                            BtnSubmit.Visible = false;
                            TxtNewCadreName.Text = DtCadre.Rows[0]["CadreName"].ToString();
                        }
                    }
                }
               
            }
            catch (Exception )
            {

            }
        }

        protected void BtnSubmit_Click(object sender, EventArgs e)
        {
            try
            {
               if(TxtNewCadreName.Text !="")
               {
                   CompanyCadreBE ObjBE = new CompanyCadreBE();
                   ObjBE.propCadreName = TxtNewCadreName.Text;
                   ObjBE.propCreatedBy = Session["CompanyId"].ToString();
                   ObjBE.propCompanyId_FK = Session["CompanyId"].ToString();
                   int status = ObjCadreDAL.CompanyCadreInsert(ObjBE);
                   if(status >0)
                   {
                       BindCompanyCadre(0);
                       TxtNewCadreName.Text = "";
                       PnlAddNewCadre.Visible = false;
                   }
               }
            }
            catch (Exception )
            {

            }
        }

        protected void BtnUpdate_Click(object sender, EventArgs e)
        {

        }
    }
}