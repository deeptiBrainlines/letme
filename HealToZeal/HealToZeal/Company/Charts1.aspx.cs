﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.DataVisualization.Charting;
using System.Data;
using DAL;

namespace HealToZeal.Company
{
    public partial class Charts1 : System.Web.UI.Page
    {
        EmployeeDAL objEmployeeDAL = new EmployeeDAL();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                Bindbatch();
            }
          
            

        }
        private void Bindbatch()
        {
            try
            {
                string CompanyId = "";
                CompanyId = Session["CompanyId"].ToString();
                DataTable Dtdbatch = objEmployeeDAL.GetBatchName(CompanyId);

                ddlbatch.DataSource = Dtdbatch;
                ddlbatch.DataTextField = "BatchName";
                ddlbatch.DataValueField = "BatchID_PK";
                ddlbatch.DataBind();
                ddlbatch.Items.Insert(0, new ListItem("--SelectBatch--", "--Select Batch--"));
                //ddlbatch.Items.Insert(0, new System.Web.UI.WebControls.ListItem("--SelectBatch--", "--Select Batch--"));
                ddlbatch.SelectedIndex = 0;
            }
            catch
            {

            }
        }


       

        protected void ddlbatch_SelectedIndexChanged(object sender, EventArgs e)
        {
            string batchid = ddlbatch.SelectedValue;
            Session["batch"] = batchid;
            DataTable DtEmployees = objEmployeeDAL.Percentageget(batchid);
            if (DtEmployees.Rows.Count > 0)
            {
                for (int j = 0; j < DtEmployees.Rows.Count; j++)
                {

                    string[] XPointMember = new string[DtEmployees.Rows.Count];
                    int[] YPointMember = new int[DtEmployees.Rows.Count];

                    for (int count = 0; count < DtEmployees.Rows.Count; count++)
                    {
                        //storing Values for X axis  
                        XPointMember[count] = DtEmployees.Rows[count]["Color"].ToString();
                        //storing values for Y Axis  
                        YPointMember[count] = Convert.ToInt32(DtEmployees.Rows[count]["percentages"]);

                    }
                    Chart1.Series[0].Points.DataBindXY(XPointMember, YPointMember);


                    foreach (Series charts in Chart1.Series)
                    {
                        foreach (DataPoint point in charts.Points)
                        {
                            switch (point.AxisLabel)
                            {
                                case "green": point.Color = System.Drawing.Color.LightGreen; break;
                                case "Amber1": point.Color = System.Drawing.Color.Yellow; break;
                                case "Amber2": point.Color = System.Drawing.Color.Orange; break;
                                case "red": point.Color = System.Drawing.Color.Red; break;

                            }
                            point.Label = string.Format("{0:0} - {1}", point.YValues[0], point.AxisLabel);

                        }
                    }

                    //Setting width of line  
                    Chart1.Series[0].BorderWidth = 10;
                    //setting Chart type   
                    Chart1.Series[0].ChartType = SeriesChartType.Pie;


                    //foreach (Series charts in Chart1.Series)
                    //{
                    //    foreach (DataPoint point in charts.Points)
                    //    {
                    //        switch (point.AxisLabel)
                    //        {
                    //            case "Q1": point.Color = System.Drawing.Color.LightGreen; break;
                    //            case "Q2": point.Color = System.Drawing.Color.Yellow; break;
                    //            case "Q3": point.Color = System.Drawing.Color.YellowGreen; break;
                    //            case "Q4": point.Color = System.Drawing.Color.Red; break;
                    //        }
                    //        point.Label = string.Format("{0:0} - {1}", point.YValues[0], point.AxisLabel);

                    //    }
                    //}


                    //string Color = DtEmployees.Rows[0]["Color"].ToString();
                    //string counts = DtEmployees.Rows[0]["counts"].ToString();
                    //string Totalcount = DtEmployees.Rows[0]["Totalcount"].ToString();
                    //string percentage = DtEmployees.Rows[0]["percentage"].ToString();


                    //string red = DtEmployees.Rows[0]["percentagered"].ToString();
                    //string Amber2 = DtEmployees.Rows[0]["percentageAmber2"].ToString();
                    //string Amber1 = DtEmployees.Rows[0]["percentageAmber1"].ToString();
                    //string green = DtEmployees.Rows[0]["percentagegreen"].ToString();

                    // int red1 = Convert.ToInt32(red);
                    //int amber2 = Convert.ToInt32(Amber2);
                    //int amber1 = Convert.ToInt32(Amber1);
                    //int green1 = Convert.ToInt32(green);

                    //double[] yValues = { red1, amber2, amber1, green1 };
                    //string[] xValues = { "Red", "Amber2", "Amber1", "Green" };

                   // Chart1.Series["Default"].Points.DataBindXY(xValues, yValues);
                    //Chart1.Series["Default"].Label = "#PERCENT";
                    //Chart1.Series["Default"].Points[0].Color = System.Drawing.Color.Yellow;
                    //Chart1.Series["Default"].Points[1].Color = System.Drawing.Color.YellowGreen;
                    //Chart1.Series["Default"].Points[2].Color = System.Drawing.Color.LightGreen;
                    //Chart1.Series["Default"].Points[3].Color = System.Drawing.Color.Red;

                    Chart1.Series["Default"].ChartType = SeriesChartType.Pie;
                    Chart1.Series["Default"]["PieLabelStyle"] = "enable";
                    Chart1.ChartAreas["ChartArea1"].Area3DStyle.Enable3D = true;
                    Chart1.Legends[0].Enabled = true;

                    //addded for onclick event
                    //Chart1.Series[0].Points.DataBindXY(xValues, yValues);
                    Chart1.Series[0].ChartType = SeriesChartType.Pie;
                    Chart1.ChartAreas["ChartArea1"].Area3DStyle.Enable3D = true;
                    Chart1.Legends[0].Enabled = true;

                    Chart1.Series[0].LegendUrl = "Charts1.aspx";
                    Chart1.Series[0].LabelUrl = "Charts1.aspx";
                    Chart1.Series[0].Url = "Charts1.aspx";

                    Chart1.Series[0].LegendPostBackValue = "#VALY-#VALX";
                    Chart1.Series[0].LabelPostBackValue = "#VALY-#VALX";
                    Chart1.Series[0].PostBackValue = "#VALY-#VALX";

                    Chart1.Series[0].LegendMapAreaAttributes = "target=\"_blank\"";
                    Chart1.Series[0].LabelMapAreaAttributes = "target=\"_blank\"";
                    Chart1.Series[0].MapAreaAttributes = "target=\"_blank\"";

                    //Session["perred1"] = red;
                    //Session["peramber2"] = Amber2;
                    //Session["peramber1"] = Amber1;
                    //Session["pergreen"] = green;
                }

               
            }

            //int red1 = Convert.ToInt32(Session["perred1"]);
            //int amber2 = Convert.ToInt32(Session["peramber2"]);
            //int amber1 = Convert.ToInt32(Session["peramber1"]);
            //int green1 = Convert.ToInt32(Session["pergreen"]);



        }

        protected void Chart1_Click(object sender, ImageMapEventArgs e)
        {
            HttpContext.Current.Session["VAL"] = e.PostBackValue;
            Response.Redirect("ShowResult.aspx");

        }
    }
}