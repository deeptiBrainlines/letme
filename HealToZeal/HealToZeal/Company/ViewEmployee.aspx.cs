﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DAL;
using System.Data;
using System.Web.Mail;
using System.Net.Mail;
using System.Text.RegularExpressions;

namespace HealToZeal.Company
{
    public partial class ViewEmployee : System.Web.UI.Page
    {
        EmployeeDAL objEmployeeDAL = new EmployeeDAL();
        string CompanyId = "";

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                Session["EmployeeId"] = null;
                if (Session["CompanyId"] != null)
                {
                    //BindCadre();
                    BindCompanyEmployees(0);
                    divalert.Visible = false;
                }
            }
        }
        /* private void BindCadre()
         {
             try
             {
                 CompanyId = Session["CompanyId"].ToString();
                 DataTable DtCadre = objEmployeeDAL.CompanyCadreGetByCompanyId(CompanyId);

                 ddlCadre.DataSource = DtCadre;
                 ddlCadre.DataTextField = "Cadrename";
                 ddlCadre.DataValueField = "CadreId_PK";
                 ddlCadre.DataBind();

                 ddlCadre.Items.Insert(0, new ListItem("--SelectCadre--", "--Select Cadre--"));
                 ddlCadre.SelectedIndex = 0;
             }
             catch
             {

             }
         }*/
        private void BindCompanyEmployees(int Flag)
        {
            try
            {
                CompanyId = Session["CompanyId"].ToString();
                DataTable DtEmployees = objEmployeeDAL.EmployeesGetByCompanyId(CompanyId, Flag);
                if (DtEmployees.Columns.Contains("Color"))
                {

                }
                else
                {
                    DtEmployees.Columns.Add("Color", typeof(string));
                    DtEmployees.Columns.Add("LblText", typeof(string));
                }

                for (int a = 0; a < DtEmployees.Rows.Count; a++)
                {
                    DataTable Dtcolor = objEmployeeDAL.SpDatewiseScoreByEmployeeCompanyId(DtEmployees.Rows[a]["EmployeeId_PK"].ToString());

                    if (Dtcolor == null)
                    {
                        DtEmployees.Rows[a]["Color"] = "#ffffff";
                        DtEmployees.Rows[a]["LblText"] = "Test not attempted";
                    }
                    else
                    {
                        if (Dtcolor.Rows.Count == 0)
                        {
                            DtEmployees.Rows[a]["Color"] = "#ffffff";
                            DtEmployees.Rows[a]["LblText"] = "Test not attempted";
                        }
                        else
                        {
                            DataRow[] result = Dtcolor.Select("Color='#e43c37'");
                            if (result.Length > 0)
                            {
                                DtEmployees.Rows[a]["Color"] = "#e43c37";
                                DtEmployees.Rows[a]["LblText"] = "";
                            }
                            else if (result.Length == 0)
                            {
                                DataRow[] result1 = Dtcolor.Select("Color='#f4fb73'");
                                if (result1.Length > 0)
                                {
                                    DtEmployees.Rows[a]["Color"] = "#f4fb73";
                                    DtEmployees.Rows[a]["LblText"] = "";
                                }
                                else
                                {
                                    DtEmployees.Rows[a]["Color"] = "#88e46d";
                                    DtEmployees.Rows[a]["LblText"] = "";
                                }
                            }
                        }

                        //#f4fb73 - yellow
                        // #88e46d -green
                        //#e43c37 - red
                    }

                }
                // 
                if (DtEmployees != null && DtEmployees.Rows.Count > 0)
                {
                    GvViewEmployee.DataSource = DtEmployees;
                    GvViewEmployee.DataBind();
                }

                for (int x = 0; x < GvViewEmployee.Rows.Count; x++)
                {
                    Label LblColor = (Label)GvViewEmployee.Rows[x].FindControl("LblColor");
                    if (LblColor.Text == "Test not attempted")
                    {
                        LblColor.Width = 120;
                    }
                }
            }
            catch(Exception e)
            {
                e.InnerException.ToString();
            }
        }


        private void BindCompanyEmployeesddlsearch(int Flag)
        {
            try
            {
                CompanyId = Session["CompanyId"].ToString();
                DataTable DtEmployees = objEmployeeDAL.EmployeesGetByCompanyIdddlsearch(CompanyId, Flag);
                if (DtEmployees.Columns.Contains("Color"))
                {

                }
                else
                {
                    DtEmployees.Columns.Add("Color", typeof(string));
                    DtEmployees.Columns.Add("LblText", typeof(string));
                }

                for (int a = 0; a < DtEmployees.Rows.Count; a++)
                {
                    DataTable Dtcolor = objEmployeeDAL.SpDatewiseScoreByEmployeeCompanyId(DtEmployees.Rows[a]["EmployeeId_PK"].ToString());

                    if (Dtcolor == null)
                    {
                        DtEmployees.Rows[a]["Color"] = "#ffffff";
                        DtEmployees.Rows[a]["LblText"] = "Test not attempted";
                    }
                    else
                    {
                        if (Dtcolor.Rows.Count == 0)
                        {
                            DtEmployees.Rows[a]["Color"] = "#ffffff";
                            DtEmployees.Rows[a]["LblText"] = "Test not attempted";
                        }
                        else
                        {
                            DataRow[] result = Dtcolor.Select("Color='#e43c37'");
                            if (result.Length > 0)
                            {
                                DtEmployees.Rows[a]["Color"] = "#e43c37";
                                DtEmployees.Rows[a]["LblText"] = "";
                            }
                            else if (result.Length == 0)
                            {
                                DataRow[] result1 = Dtcolor.Select("Color='#f4fb73'");
                                if (result1.Length > 0)
                                {
                                    DtEmployees.Rows[a]["Color"] = "#f4fb73";
                                    DtEmployees.Rows[a]["LblText"] = "";
                                }
                                else
                                {
                                    DtEmployees.Rows[a]["Color"] = "#88e46d";
                                    DtEmployees.Rows[a]["LblText"] = "";
                                }
                            }
                        }

                        //#f4fb73 - yellow
                        // #88e46d -green
                        //#e43c37 - red
                    }

                }
                // 
                if (DtEmployees != null && DtEmployees.Rows.Count > 0)
                {
                    GvViewEmployee.DataSource = DtEmployees;
                    GvViewEmployee.DataBind();
                }

                for (int x = 0; x < GvViewEmployee.Rows.Count; x++)
                {
                    Label LblColor = (Label)GvViewEmployee.Rows[x].FindControl("LblColor");
                    if (LblColor.Text == "Test not attempted")
                    {
                        LblColor.Width = 120;
                    }
                }
            }
            catch (Exception e)
            {
                e.InnerException.ToString();
            }
        }

        protected void ddlFilter_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (ddlFilter.SelectedIndex != 0)
                {
                    GvViewEmployee.PageIndex = 0;
                    Session["FilterBy"] = "Filter";
                    BindCompanyEmployeesddlsearch(ddlFilter.SelectedIndex);
                }
                txtEmployeeId.Text = "";
                txtName.Text = "";
            }
            catch
            {

            }
        }

        protected void GvViewEmployee_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                GvViewEmployee.PageIndex = e.NewPageIndex;

                CompanyId = Session["CompanyId"].ToString();
                if (Session["FilterBy"] != null)
                {
                    if (Session["FilterBy"].ToString().Equals("Filter"))
                    {
                        BindCompanyEmployees(ddlFilter.SelectedIndex);
                    }
                    else if (Session["FilterBy"].ToString().Equals("EmployeeId"))
                    {
                        DataTable DtEmployees = objEmployeeDAL.EmployeesGetByCompanyId(CompanyId, Convert.ToInt32(HdnSearch.Value.ToString()));
                        if (DtEmployees != null && DtEmployees.Rows.Count > 0)
                        {
                            GvViewEmployee.DataSource = DtEmployees;
                            GvViewEmployee.DataBind();
                        }
                    }
                    else if (Session["FilterBy"].ToString().Equals("EmployeeName"))
                    {
                        DataTable DtEmployees = objEmployeeDAL.EmployeeGetByName(CompanyId, HdnSearch.Value.ToString());
                        if (DtEmployees != null && DtEmployees.Rows.Count > 0)
                        {
                            GvViewEmployee.DataSource = DtEmployees;
                            GvViewEmployee.DataBind();
                            Session["FilterBy"] = "EmployeeName";
                        }
                    }
                    else if (Session["FilterBy"].ToString().Equals("CompanyCadre"))
                    {
                        //BindCompanyEmployeesByCadreID(ddlCadre.SelectedValue);
                    }
                    else
                    {
                        BindCompanyEmployees(0);
                    }
                }
                else
                {
                    BindCompanyEmployees(0);
                }


            }
            catch
            {

            }
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            try
            {
                if (HdnSearch.Value != "")
                {
                    if (txtEmployeeId.Text != "")
                    {
                        CompanyId = Session["CompanyId"].ToString();
                        DataTable DtEmployees = objEmployeeDAL.EmployeesGetByCompanyId(CompanyId, Convert.ToInt32(HdnSearch.Value.ToString()));
                        if (DtEmployees.Columns.Contains("Color"))
                        {

                        }
                        else
                        {
                            DtEmployees.Columns.Add("Color", typeof(string));
                            DtEmployees.Columns.Add("LblText", typeof(string));
                            // DtEmployees.Columns.Add("LblWidth", typeof(string));
                        }

                        for (int a = 0; a < DtEmployees.Rows.Count; a++)
                        {
                            DataTable Dtcolor = objEmployeeDAL.SpDatewiseScoreByEmployeeCompanyId(DtEmployees.Rows[a]["EmployeeId_PK"].ToString());

                            if (Dtcolor == null)
                            {
                                DtEmployees.Rows[a]["Color"] = "#ffffff";
                                DtEmployees.Rows[a]["LblText"] = "Test not attempted";
                                //DtEmployees.Rows[a]["LblWidth"] = "120px";
                            }
                            else
                            {
                                if (Dtcolor.Rows.Count == 0)
                                {
                                    DtEmployees.Rows[a]["Color"] = "#ffffff";
                                    DtEmployees.Rows[a]["LblText"] = "Test not attempted";
                                    // DtEmployees.Rows[a]["LblWidth"] = "120px";
                                }
                                else
                                {

                                    DataRow[] result = Dtcolor.Select("Color='#e43c37'");
                                    if (result.Length > 0)
                                    {
                                        DtEmployees.Rows[a]["Color"] = "#e43c37";
                                        DtEmployees.Rows[a]["LblText"] = "";
                                        //DtEmployees.Rows[a]["LblWidth"] = "90px";
                                    }
                                    else if (result.Length == 0)
                                    {
                                        DataRow[] result1 = Dtcolor.Select("Color='#f4fb73'");
                                        if (result1.Length > 0)
                                        {
                                            DtEmployees.Rows[a]["Color"] = "#f4fb73";
                                            DtEmployees.Rows[a]["LblText"] = "";
                                            // DtEmployees.Rows[a]["LblWidth"] = "90px";
                                        }
                                        else
                                        {
                                            DtEmployees.Rows[a]["Color"] = "#88e46d";
                                            DtEmployees.Rows[a]["LblText"] = "";
                                            //DtEmployees.Rows[a]["LblWidth"] = "90px";
                                        }
                                    }
                                }

                                //#f4fb73 - yellow
                                // #88e46d -green
                                //#e43c37 - red
                            }

                        }
                        // 
                        if (DtEmployees != null && DtEmployees.Rows.Count > 0)
                        {
                            GvViewEmployee.PageIndex = 0;
                            GvViewEmployee.DataSource = DtEmployees;
                            GvViewEmployee.DataBind();
                            Session["FilterBy"] = "EmployeeId";
                        }

                        for (int x = 0; x < GvViewEmployee.Rows.Count; x++)
                        {
                            Label LblColor = (Label)GvViewEmployee.Rows[x].FindControl("LblColor");
                            if (LblColor.Text == "Test not attempted")
                            {
                                LblColor.Width = 120;
                            }
                        }
                        ddlFilter.SelectedIndex = 0;
                        txtName.Text = "";
                    }
                }

            }
            catch
            {

            }
        }

        protected void btnSearchName_Click(object sender, EventArgs e)
        {
            try
            {
                if (HdnSearch.Value != "")
                {
                    if (txtName.Text != "")
                    {
                        CompanyId = Session["CompanyId"].ToString();
                        DataTable DtEmployees = objEmployeeDAL.EmployeeGetByName(CompanyId, HdnSearch.Value.ToString());


                        if (DtEmployees.Columns.Contains("Color"))
                        {

                        }
                        else
                        {
                            DtEmployees.Columns.Add("Color", typeof(string));
                            DtEmployees.Columns.Add("LblText", typeof(string));
                            // DtEmployees.Columns.Add("LblWidth", typeof(string));
                        }

                        for (int a = 0; a < DtEmployees.Rows.Count; a++)
                        {
                            DataTable Dtcolor = objEmployeeDAL.SpDatewiseScoreByEmployeeCompanyId(DtEmployees.Rows[a]["EmployeeId_PK"].ToString());

                            if (Dtcolor == null)
                            {
                                DtEmployees.Rows[a]["Color"] = "#ffffff";
                                DtEmployees.Rows[a]["LblText"] = "Test not attempted";
                                //DtEmployees.Rows[a]["LblWidth"] = "120px";
                            }
                            else
                            {
                                if (Dtcolor.Rows.Count == 0)
                                {
                                    DtEmployees.Rows[a]["Color"] = "#ffffff";
                                    DtEmployees.Rows[a]["LblText"] = "Test not attempted";
                                    // DtEmployees.Rows[a]["LblWidth"] = "120px";
                                }
                                else
                                {


                                    DataRow[] result = Dtcolor.Select("Color='#e43c37'");
                                    if (result.Length > 0)
                                    {
                                        DtEmployees.Rows[a]["Color"] = "#e43c37";
                                        DtEmployees.Rows[a]["LblText"] = "";
                                        //DtEmployees.Rows[a]["LblWidth"] = "90px";
                                    }
                                    else if (result.Length == 0)
                                    {
                                        DataRow[] result1 = Dtcolor.Select("Color='#f4fb73'");
                                        if (result1.Length > 0)
                                        {
                                            DtEmployees.Rows[a]["Color"] = "#f4fb73";
                                            DtEmployees.Rows[a]["LblText"] = "";
                                            // DtEmployees.Rows[a]["LblWidth"] = "90px";
                                        }
                                        else
                                        {
                                            DtEmployees.Rows[a]["Color"] = "#88e46d";
                                            DtEmployees.Rows[a]["LblText"] = "";
                                            //DtEmployees.Rows[a]["LblWidth"] = "90px";
                                        }
                                    }
                                }

                                //#f4fb73 - yellow
                                // #88e46d -green
                                //#e43c37 - red
                            }

                        }
                        // 
                        if (DtEmployees != null && DtEmployees.Rows.Count > 0)
                        {
                            GvViewEmployee.PageIndex = 0;
                            GvViewEmployee.DataSource = DtEmployees;
                            GvViewEmployee.DataBind();
                            Session["FilterBy"] = "EmployeeId";
                        }

                        for (int x = 0; x < GvViewEmployee.Rows.Count; x++)
                        {
                            Label LblColor = (Label)GvViewEmployee.Rows[x].FindControl("LblColor");
                            if (LblColor.Text == "Test not attempted")
                            {
                                LblColor.Width = 120;
                            }
                        }
                        ddlFilter.SelectedIndex = 0;
                        txtName.Text = "";

                    }
                }

            }
            catch
            {

            }
        }

        static EmployeeDAL obj = new EmployeeDAL();
        private string EmployeeId;

        [System.Web.Services.WebMethod]
        [System.Web.Script.Services.ScriptMethod]
       
        public static string[] ShowListSearch(string prefixText, int count)
        {
            //   DataTable DtEmployees = obj.EmployeesGetByCompanyIdsearch(HttpContext.Current.Session["CompanyId"].ToString(), Convert.ToInt32(prefixText));
            DataTable DtEmployees = obj.EmployeesGetByCompanyId(HttpContext.Current.Session["CompanyId"].ToString(), Convert.ToInt32(prefixText));

            List<string> items = new List<string>(DtEmployees.Rows.Count);

            if (DtEmployees.Rows.Count > 0)
            {
                for (int i = 0; i < DtEmployees.Rows.Count; i++)
                {
                    items.Add(DtEmployees.Rows[i]["EmployeeCompanyId"].ToString());
                }
            }

           
            return items.ToArray();
        }

        public   void showRecord(List<string> list)
        {
            GvViewEmployee.DataSource = list;
            GvViewEmployee.DataBind();
        }
        [System.Web.Services.WebMethod]
        [System.Web.Script.Services.ScriptMethod]
        public static string[] ShowListSearchByName(string prefixText, int count)
        {
            DataTable DtEmployees = obj.EmployeeGetByName(HttpContext.Current.Session["CompanyId"].ToString(), prefixText);
            List<string> items = new List<string>(DtEmployees.Rows.Count);

            if (DtEmployees.Rows.Count > 0)
            {
                for (int i = 0; i < DtEmployees.Rows.Count; i++)
                {
                    // items.Add(ds.Tables[0].Rows[i][1].ToString() + " " + ds.Tables[0].Rows[i]["MiddleName"].ToString() + " " + ds.Tables[0].Rows[i][3].ToString());                
                    items.Add(DtEmployees.Rows[i]["EmployeeName"].ToString());
                }
            }
            //ViewEmployee pt = new ViewEmployee();
           // pt.showRecord(items);
            return items.ToArray();
        }

        [System.Web.Services.WebMethod]
        [System.Web.Script.Services.ScriptMethod]
        public static string ShowSearchByName(string name)
        {

            // DataTable DtEmployees = obj.EmployeeGetByName(HttpContext.Current.Session["CompanyId"].ToString());
            //List<string> items = new List<string>(DtEmployees.Rows.Count);

            //if (DtEmployees.Rows.Count > 0)
            //{
            //    for (int i = 0; i < DtEmployees.Rows.Count; i++)
            //    {
            //        // items.Add(ds.Tables[0].Rows[i][1].ToString() + " " + ds.Tables[0].Rows[i]["MiddleName"].ToString() + " " + ds.Tables[0].Rows[i][3].ToString());                
            //        items.Add(DtEmployees.Rows[i]["EmployeeName"].ToString());
            //    }
            //}
            // ViewEmployee pt = new ViewEmployee();
            // pt.showRecord(items);
            //  return items.ToArray();
            return "";
        }
        protected void TextBox2_TextChanged(object sender, EventArgs e)
        {
            //int sum = Convert.ToInt32(TextBox1.Text) + Convert.ToInt32(TextBox2.Text);
            //lblsum.Text = "The Sum = " + sum.ToString();
            //TextBox1.Text = "";
            //TextBox2.Text = "";
            string name = txtName.Text;
            var arr = name.Split(' ');
            string newname = arr[0];
            Session["newname"] = newname;
            CompanyId = Session["CompanyId"].ToString();
            DataTable DtEmployees = objEmployeeDAL.EmployeeGetByName(CompanyId, Session["newname"].ToString());


            if (DtEmployees.Columns.Contains("Color"))
            {

            }
            else
            {
                DtEmployees.Columns.Add("Color", typeof(string));
                DtEmployees.Columns.Add("LblText", typeof(string));
                // DtEmployees.Columns.Add("LblWidth", typeof(string));
            }

            for (int a = 0; a < DtEmployees.Rows.Count; a++)
            {
                DataTable Dtcolor = objEmployeeDAL.SpDatewiseScoreByEmployeeCompanyId(DtEmployees.Rows[a]["EmployeeId_PK"].ToString());

                if (Dtcolor == null)
                {
                    DtEmployees.Rows[a]["Color"] = "#ffffff";
                    DtEmployees.Rows[a]["LblText"] = "Test not attempted";
                    //DtEmployees.Rows[a]["LblWidth"] = "120px";
                }
                else
                {
                    if (Dtcolor.Rows.Count == 0)
                    {
                        DtEmployees.Rows[a]["Color"] = "#ffffff";
                        DtEmployees.Rows[a]["LblText"] = "Test not attempted";
                        // DtEmployees.Rows[a]["LblWidth"] = "120px";
                    }
                    else
                    {


                        DataRow[] result = Dtcolor.Select("Color='#e43c37'");
                        if (result.Length > 0)
                        {
                            DtEmployees.Rows[a]["Color"] = "#e43c37";
                            DtEmployees.Rows[a]["LblText"] = "";
                            //DtEmployees.Rows[a]["LblWidth"] = "90px";
                        }
                        else if (result.Length == 0)
                        {
                            DataRow[] result1 = Dtcolor.Select("Color='#f4fb73'");
                            if (result1.Length > 0)
                            {
                                DtEmployees.Rows[a]["Color"] = "#f4fb73";
                                DtEmployees.Rows[a]["LblText"] = "";
                                // DtEmployees.Rows[a]["LblWidth"] = "90px";
                            }
                            else
                            {
                                DtEmployees.Rows[a]["Color"] = "#88e46d";
                                DtEmployees.Rows[a]["LblText"] = "";
                                //DtEmployees.Rows[a]["LblWidth"] = "90px";
                            }
                        }
                    }

                    //#f4fb73 - yellow
                    // #88e46d -green
                    //#e43c37 - red
                }

            }
            // 
            if (DtEmployees != null && DtEmployees.Rows.Count > 0)
            {
                GvViewEmployee.PageIndex = 0;
                GvViewEmployee.DataSource = DtEmployees;
                GvViewEmployee.DataBind();
                Session["FilterBy"] = "EmployeeId";
            }

            for (int x = 0; x < GvViewEmployee.Rows.Count; x++)
            {
                Label LblColor = (Label)GvViewEmployee.Rows[x].FindControl("LblColor");
                if (LblColor.Text == "Test not attempted")
                {
                    LblColor.Width = 120;
                }
            }
            ddlFilter.SelectedIndex = 0;
            //txtName.Text = "";
        }

   

        protected void GvViewEmployee_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                GridViewRow row = (GridViewRow)(((Control)e.CommandSource).NamingContainer);
                Label lblEmployeeName = (Label)row.FindControl("lblEmployeeName");
                EmployeeId = e.CommandArgument.ToString();
                //add session
                //   Session["EmployeeId"] = EmployeeId;
                if (e.CommandName == "DeleteEmployee")
                {
                    CompanyId = Session["CompanyId"].ToString();
                    DeleteEmployee(EmployeeId, CompanyId);
                }
                else if (e.CommandName == "ViewEmployee")
                {
                    Response.Redirect("EmployeeSummary.aspx?Id=" + EmployeeId);
                }
                else if (e.CommandName == "ViewEmployeeNew")
                {
                    Response.Redirect("EmployeeSummaryNew.aspx?Id=" + EmployeeId);
                }
                else if (e.CommandName == "ViewEmployeeHistory")
                {
                    //Response.Redirect("EmployeeHistory.aspx");
                   Response.Redirect("EmployeeHistory.aspx?Id=" + EmployeeId );
                   // Response.Redirect("EmployeeHistory.aspx?Id=" + EmployeeId + "&EmployeeName=" + lblEmployeeName.Text);
                }
                Session["EmployeeId"] = EmployeeId;
            }
            catch(Exception )
            {
                //ex.InnerException.ToString();
            }
        }

        private void DeleteEmployee(string EmployeeId, string CompanyId)
        {
            try
            {
                EmployeeDAL objEmployeeDAL = new EmployeeDAL();
                int status = objEmployeeDAL.EmployeesDelete(EmployeeId, CompanyId);
                divalert.Visible = true;
                if (status > 0)
                {
                    lblMessage.Text = "Employee removed successfully...";
                    BindCompanyEmployees(0);
                }
                else
                {
                    divalert.Attributes.Add("class", "alert alert-danger");
                    lblMessage.Text = "Can not remove employee...";
                }
            }
            catch
            {

            }
        }

        /* protected void ddlCadre_SelectedIndexChanged(object sender, EventArgs e)
         {
             try
             {
                 if (ddlCadre.SelectedIndex != 0)
                 {
                     GvViewEmployee.PageIndex = 0;
                 BindCompanyEmployeesByCadreID( ddlCadre.SelectedValue);
                 Session["FilterBy"] = "CompanyCadre";
                 }
                 ddlDepartment.SelectedIndex = 0;
                 ddlFilter.SelectedIndex = 0;
                 txtEmployeeId.Text = "";
                 txtName.Text = "";
               
             }
             catch 
             {
                
             }
         }*/

        private void BindCompanyEmployeesByCadreID(string CadreId)
        {
            try
            {
                DataTable DtEmployees = objEmployeeDAL.EmployeesGetByCadreId(Session["CompanyId"].ToString(), CadreId);
                if (DtEmployees.Columns.Contains("Color"))
                {

                }
                else
                {
                    DtEmployees.Columns.Add("Color", typeof(string));
                    DtEmployees.Columns.Add("LblText", typeof(string));
                    // DtEmployees.Columns.Add("LblWidth", typeof(string));
                }

                for (int a = 0; a < DtEmployees.Rows.Count; a++)
                {
                    DataTable Dtcolor = objEmployeeDAL.SpDatewiseScoreByEmployeeCompanyId(DtEmployees.Rows[a]["EmployeeId_PK"].ToString());

                    if (Dtcolor == null)
                    {
                        DtEmployees.Rows[a]["Color"] = "#ffffff";
                        DtEmployees.Rows[a]["LblText"] = "Test not attempted";
                        //DtEmployees.Rows[a]["LblWidth"] = "120px";
                    }
                    else
                    {
                        if (Dtcolor.Rows.Count == 0)
                        {
                            DtEmployees.Rows[a]["Color"] = "#ffffff";
                            DtEmployees.Rows[a]["LblText"] = "Test not attempted";
                            // DtEmployees.Rows[a]["LblWidth"] = "120px";
                        }
                        else
                        {


                            DataRow[] result = Dtcolor.Select("Color='#e43c37'");
                            if (result.Length > 0)
                            {
                                DtEmployees.Rows[a]["Color"] = "#e43c37";
                                DtEmployees.Rows[a]["LblText"] = "";
                                //DtEmployees.Rows[a]["LblWidth"] = "90px";
                            }
                            else if (result.Length == 0)
                            {
                                DataRow[] result1 = Dtcolor.Select("Color='#f4fb73'");
                                if (result1.Length > 0)
                                {
                                    DtEmployees.Rows[a]["Color"] = "#f4fb73";
                                    DtEmployees.Rows[a]["LblText"] = "";
                                    // DtEmployees.Rows[a]["LblWidth"] = "90px";
                                }
                                else
                                {
                                    DtEmployees.Rows[a]["Color"] = "#88e46d";
                                    DtEmployees.Rows[a]["LblText"] = "";
                                    //DtEmployees.Rows[a]["LblWidth"] = "90px";
                                }
                            }
                        }

                        //#f4fb73 - yellow
                        // #88e46d -green
                        //#e43c37 - red
                    }

                }
                // 
                if (DtEmployees != null && DtEmployees.Rows.Count > 0)
                {
                    GvViewEmployee.DataSource = DtEmployees;
                    GvViewEmployee.DataBind();
                }

                for (int x = 0; x < GvViewEmployee.Rows.Count; x++)
                {
                    Label LblColor = (Label)GvViewEmployee.Rows[x].FindControl("LblColor");
                    if (LblColor.Text == "Test not attempted")
                    {
                        LblColor.Width = 120;
                    }
                }

                /*if (DtEmployee != null && DtEmployee.Rows.Count > 0)
                {


                    GvViewEmployee.DataSource = DtEmployee;
                    GvViewEmployee.DataBind();
                }*/
            }
            catch
            {

            }
        }

        protected void BtnRefresh_Click(object sender, EventArgs e)
        {
            try
            {
                Session["FilterBy"] = null;
                BindCompanyEmployees(0);
                //ddlCadre.SelectedIndex = 0;
                //ddlDepartment.SelectedIndex = 0;
                ddlFilter.SelectedIndex = 0;
                txtEmployeeId.Text = "";
                txtName.Text = "";
            }
            catch
            {

            }
        }

        protected void BtnReminderEmail_Click(object sender, EventArgs e)
        {
            try
            {
                TrEmail.Visible = true;
                CompanyId = Session["CompanyId"].ToString();
                DataTable DtEmployees = objEmployeeDAL.EmployeesGetByCompanyId_Email(CompanyId);

                if (DtEmployees != null)
                {
                    if (DtEmployees.Rows.Count > 0)
                    {
                        //string EmailId = "";
                        //for (int i = 0; i < DtEmployees.Rows.Count; i++)
                        //{
                        //    if (EmailId != "")
                        //    {
                        //        EmailId = EmailId + ";" + DtEmployees.Rows[i]["EmailId"].ToString();
                        //    }
                        //    else
                        //    {
                        //        EmailId = DtEmployees.Rows[i]["EmailId"].ToString();
                        //    }
                        //}
                        //TxtTo.Text = EmailId;

                        chkEmployees.DataSource = DtEmployees;
                        chkEmployees.DataTextField = "FirstName"; //changed EmployeeName
                        chkEmployees.DataValueField = "EmailId";
                        chkEmployees.DataBind();
                    }
                }

            //    DataTable DtEmployees = objEmployeeDAL.EmployeesGetEmployeesNotSubmited(new Guid(CompanyId));

                if (DtEmployees != null)
                {
                    if (DtEmployees.Rows.Count > 0)
                    {
                        for (int i = 0; i < DtEmployees.Rows.Count; i++)
                        {
                            System.Net.Mail.MailMessage mail = new System.Net.Mail.MailMessage();
                            mail.To.Add(DtEmployees.Rows[i]["EmailId"].ToString());
                            mail.Bcc.Add("priyamvada.bavadekar@letmetalk2.me");

                           // mail.From = new MailAddress("contact@letmetalk2.me", "Let me talk to me", System.Text.Encoding.UTF8);
                            mail.Subject = "Reminder -  Health and Personality Insights and Actions Tool.";
                            string strBody = "<html><body><font color=\"black\">Dear " + DtEmployees.Rows[i]["FirstName"].ToString()
                                + ",<br/><br/>This is a gentle reminder to complete the mindfulness assessments from Let Me Talk2 Me.<br/><br/>You will be able to gain some useful insights into your mindfulness levels, state of mind and learn about remedial actions in case they are needed. <b>'Let Me Talk2 Me'</b> will help you in self-discovery. It will lead on a path of self development while avoiding pitfalls of modern complex life and careers. <br/><br/>It is recommended that you use this tool periodically to monitor yourself and be aware of your health and mindfulness parameters. <br/> Your login details are as follows: <br/><br/> UserId:  <b>" + DtEmployees.Rows[i]["UserName"].ToString() + "</b> <br/><br/>  Password: <b> " + Encryption.Decrypt(DtEmployees.Rows[i]["Password"].ToString()) + "</b> <br/>Please use the link below to login <br/><br/> <a href='http://letmetalk2.me.hazel.arvixe.com/Employee/EmployeeLogin.aspx ' >http://letmetalk2.me.hazel.arvixe.com/Employee/EmployeeLogin.aspx </a><BR/> <BR/>Thank You.<BR/><br/> Best Regards, <br/><br/> Let Me Talk to me Team.</font></body></html>";

                            mail.Body = strBody;//"<html><body><font color=\"black\">Dear " + FirstName.Value + " " + LastName.Value + ",<br/><br/>Welcome to <b> Let Me Talk2 Me: A Health and Personality Insights and Actions Tool.</b> You will be able to gain some useful insights into your mindfulness levels, state of mind and learn about remedial actions in case they are needed. <b>'Let Me Talk2 Me'</b> will help you in self-discovery. It will lead on a path of self development while avoiding pitfalls of modern complex life and careers. <br/><br/><br/><br/>It is recommended that you use this tool periodically to monitor yourself and be aware of your health and personality parameters. <br/><br/>Your login details are as follows:<br/><br/>UserId:<b>" + userName + "</b><br/><br/>Password:<b>" + password + "</b><br/><br/>If you would like to take a break during assessment, please make sure you log out from the system or close the browser window for security purposes. Please do not leave the browser idle. You can always log back in whenever you like.<br/>Please use the link below to login <br/><br/><a href='http://chetanatspl.com/HealToZeal1/Employee/EmployeeLogin.aspx?first=1'> http://chetanatspl.com/HealToZeal1/Employee/EmployeeLogin.aspx </a><br/><br/> Thank You.<br/><br/></font></body></html>";
                            mail.IsBodyHtml = true;

                            mail.Priority = System.Net.Mail.MailPriority.High;
                            SmtpClient client = new SmtpClient();
                         //   client.Credentials = new System.Net.NetworkCredential("contact@letmetalk2.me", "contact123");
                         //   client.Port = 587;
                           // client.Host = "hazel.arvixe.com";
                            client.EnableSsl = false;

                            client.Send(mail);

                            divalert.Attributes.Add("class", "alert alert-success");

                            try
                            {
                               // System.Web.Mail.SmtpMail.Send(mail);
                                //       System.Web.Mail.SmtpMail.Send("HealToZeal@chetanatspl.com", "info@healtozeal.com", "reminders", DtEmployees.Rows[i]["FullName"].ToString() + "has received reminder mail");
                                SendReceiptEmail(DtEmployees.Rows[i]["FirstName"].ToString());
                            }
                            catch (Exception )
                            {

                            }
                            finally
                            {

                            }
                        }
                        //sent 
                        //   ScriptManager.RegisterClientScriptBlock(this.Page, GetType(), "openColorBox", "<script> alert('Reminders have been sent successfully...'); </script>;", true);
                        ScriptManager.RegisterClientScriptBlock(this.Page, this.GetType(), "alerter", "<script type='text/javascript'>alert('Reminders have been sent successfully...');</script>", false);

                    }
                    else
                    {
                        //no users 
                        // ScriptManager.RegisterClientScriptBlock(this.Page, GetType(), "openColorBox", "<script> alert('All employees have submitted assessments or No employees are present.'); </script>;", true);
                        ScriptManager.RegisterClientScriptBlock(this.Page, this.GetType(), "alerter", "<script type='text/javascript'>alert('All employees have submitted assessments or No employees are present.');</script>", false);
                    }
                }

            }
            catch (Exception ex)
            {
                ex.InnerException.ToString();
            }

        }

        private void SendReceiptEmail(string UserName)
        {
            try
            {

                System.Net.Mail.MailMessage mail = new System.Net.Mail.MailMessage();
                // System.Web.Mail.MailMessage mail = new System.Web.Mail.MailMessage();

                mail.To.Add("priyamvada.bavadekar@letmetalk2.me");
                mail.Bcc.Add("priyamvada.bavadekar@letmetalk2.me");
                // mail.From = "contact@letmetalk2.me";
               //mail.From = new MailAddress("contact@letmetalk2.me", "Let me talk to me", System.Text.Encoding.UTF8);
                mail.Subject = "Reminders sent";
                string getDt = DateTime.Now.Date.AddDays(7).ToString("MM/dd/yyyy");

                mail.Body = "<html><body><font color=\"black\">Hello Mam, <br/><br/> <b>" + UserName + "</b> has received the reminder email for assessment. <br/><br/> Thank You.<BR/><br/> Best Regards, <br/><br/> Let me talk to me Team.</font></body></html>";
                mail.IsBodyHtml = true;

                mail.Priority = System.Net.Mail.MailPriority.High;
                SmtpClient client = new SmtpClient();
            //    client.Credentials = new System.Net.NetworkCredential("contact@letmetalk2.me", "contact123");
           //     client.Port = 587;
            //    client.Host = "hazel.arvixe.com";
                client.EnableSsl = false;

                client.Send(mail);

                divalert.Attributes.Add("class", "alert alert-success");
                //System.Web.Mail.MailMessage mail = new System.Web.Mail.MailMessage();    
                //mail.To = "priyamvada@chetanasystems.com";
                //mail.From = "HealToZeal@chetanatspl.com";
                //mail.Subject = "Reminders sent";
                //mail.Body = "<html><body><font color=\"black\">Hello Mam, <br/><br/> <b>" + UserName + "</b> has received the reminder email for assessment. <br/><br/> Thank You.<BR/><br/> Best Regards, <br/><br/> HealToZealTeam.</font></body></html>";
                //mail.BodyFormat = MailFormat.Html;
                //mail.Fields.Add("http://schemas.microsoft.com/cdo/configuration/smtpauthenticate", "1");    //basic authentication
                //mail.Fields.Add("http://schemas.microsoft.com/cdo/configuration/sendusername", "HealToZeal@chetanatspl.com"); //set your username here
                //mail.Fields.Add("http://schemas.microsoft.com/cdo/configuration/sendpassword", "HealToZeal");    //set your password here

                //System.Web.Mail.SmtpMail.SmtpServer = "holly.arvixe.com";  //your real server goes here                        
                //System.Web.Mail.SmtpMail.Send(mail);
            }
            catch (Exception)
            {
            
            }
        }

        protected void BtnSendEmail_Click(object sender, EventArgs e)
        {
            try
            {
                
                //client.Send(mail);
                /*  System.Web.Mail.MailMessage mail = new System.Web.Mail.MailMessage();
                  mail.From = Session["CompanyEmailId"].ToString();
                  mail.To = TxtTo.Text;
                  mail.Subject = TxtSubject.Text;
                  if (message.Value != "")
                  {
                      mail.Body = message.Value;
                  }
                  mail.BodyFormat = MailFormat.Html;
                  mail.Fields.Add("http://schemas.microsoft.com/cdo/configuration/smtpauthenticate", "1");    //basic authentication
                  mail.Fields.Add("http://schemas.microsoft.com/cdo/configuration/sendusername", "HealToZeal@chetanatspl.com"); //set your username here
                  mail.Fields.Add("http://schemas.microsoft.com/cdo/configuration/sendpassword", "HealToZeal");    //set your password here
                  System.Web.Mail.SmtpMail.SmtpServer = "holly.arvixe.com";  //your real server goes here                        
                  System.Web.Mail.SmtpMail.Send(mail);*/

               System.Net.Mail.MailMessage Mailmessage = new System.Net.Mail.MailMessage();
              Mailmessage.From = new MailAddress(Session["CompanyEmailId"].ToString());
                Mailmessage.Bcc.Add("priyamvada.bavadekar@letmetalk2.me");
                if (TxtTo.Text != "")
                {
                    string[] EmaiIds = Regex.Split(TxtTo.Text, ";");
                    for (int i = 0; i < EmaiIds.Length; i++)
                    {
                        if (EmaiIds[i].ToString() != "")
                        {
                            Match EmailMatch = Regex.Match(EmaiIds[i].Trim(), @"^[a-zA-Z][\w\.-]*[a-zA-Z0-9]@[a-zA-Z0-9][\w\.-]*[a-zA-Z0-9]\.[a-zA-Z][a-zA-Z\.]*[a-zA-Z]$", RegexOptions.IgnoreCase);
                            if (EmailMatch.Success)
                            {
                                Mailmessage.To.Add(EmaiIds[i].Trim());
                            }
                        }
                    }
                   
                    Mailmessage.Subject = TxtSubject.Text;
                    Mailmessage.IsBodyHtml = true;

                    if (message.Value != "")
                    {
                        Mailmessage.Body = message.Value;
                    }

                    //Mailmessage.Body = EmailBody;

                    Mailmessage.IsBodyHtml = true;

                    Mailmessage.Priority = System.Net.Mail.MailPriority.High;
                    SmtpClient client = new SmtpClient();
                    //client.Credentials = new System.Net.NetworkCredential("contact@letmetalk2.me", "contact123");
                    //client.Port = 587;
                    //client.Host = "dallas137.arvixeshared.com";
                    client.EnableSsl = false;

                    client.Send(Mailmessage);

                    //System.Net.Mail.SmtpClient smtp = new SmtpClient();
                    //System.Net.NetworkCredential myCredential = new System.Net.NetworkCredential("contact@letmetalk2.me", "contact123");
                    //smtp.Host = "hazel.arvixe.com";   //Have specified the smtp host name
                    //smtp.UseDefaultCredentials = false;
                    //smtp.Credentials = myCredential;
                    //try
                    //{
                    //    smtp.Send(Mailmessage);
                    //}
                    //catch (Exception ex)
                    //{
                    //    ex.InnerException.ToString();

                    //}
                    //finally
                    //{
                    //    Mailmessage.Dispose();
                    //}
                }
                else
                {

                }

                TrEmail.Visible = false;
                ScriptManager.RegisterClientScriptBlock(this.Page, GetType(), "openColorBox", "<script> alert('We have received your meeting request. We will get back to you very soon'); </script>;", true);
            }

            catch (Exception )
            {
               // throw ex;
            }
        }

        protected void BtnCancel_Click(object sender, EventArgs e)
        {
            try
            {
                TxtSubject.Text = "";
                TxtTo.Text = "";
                message.Value = "";
                TrEmail.Visible = false;
            }
            catch
            {

            }
        }

        protected void GvViewEmployee_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                Label lblEmployeeId = (Label)e.Row.FindControl("lblEmployeeId");
                GridView GvAssessments = (GridView)e.Row.FindControl("GvAssessments");
                AssessmentDAL objAssessmentDAL = new AssessmentDAL();

                DataTable DtAssessments = objAssessmentDAL.EmployeeAssementsGetForCompany(lblEmployeeId.Text);

                GvAssessments.DataSource = DtAssessments;
                GvAssessments.DataBind();

            }
            catch
            {


            }
        }

        protected void GvAssessments_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                if (e.CommandName == "ViewResult")
                {
                    GridViewRow gvRow = (GridViewRow)(((Button)e.CommandSource).NamingContainer);
                    Label lblAssessmentInstanceId = (Label)gvRow.FindControl("lblAssessmentInstanceId");
                    Label lblInstanceCompanyRelationId = (Label)gvRow.FindControl("lblInstanceCompanyRelationId");
                    Label lblEmployeeId = (Label)gvRow.FindControl("lblEmployeeId");
                    Label lblAssessmentId = (Label)gvRow.FindControl("lblAssessmentId");
                    Session["AssessmentId"] = lblAssessmentId.Text;
                    Session["AssessmentInstanceId"] = lblAssessmentInstanceId.Text;
                    Session["InstanceCompanyRelationId"] = lblInstanceCompanyRelationId.Text;
                    Session["EmployeeId"] = lblEmployeeId.Text;

                    Response.Redirect("EmployeeSummaryResult.aspx");

                }
            }
            catch
            {

            }
        }

        protected void btnAddEmailIds_Click(object sender, EventArgs e)
        {
              TxtTo.Text="";
            foreach (ListItem chkItem in chkEmployees.Items)
            {
                if (chkItem.Selected)
                {
                    TxtTo.Text += chkItem.Value+";";
                }
            }
            TxtTo.Text.TrimEnd(';');
        }

        protected void txtEmployeeId_TextChanged(object sender, EventArgs e)
        {
            int compid =Convert.ToInt32(txtEmployeeId.Text);
           // var arr = compid.Split(' ');
          //  string newname = arr[0];
           // Session["newname"] = newname;
            CompanyId = Session["CompanyId"].ToString();
            DataTable DtEmployees = objEmployeeDAL.EmployeesGetByCompanyIdsearch(CompanyId, compid);


            if (DtEmployees.Columns.Contains("Color"))
            {

            }
            else
            {
                DtEmployees.Columns.Add("Color", typeof(string));
                DtEmployees.Columns.Add("LblText", typeof(string));
                // DtEmployees.Columns.Add("LblWidth", typeof(string));
            }

            for (int a = 0; a < DtEmployees.Rows.Count; a++)
            {
                DataTable Dtcolor = objEmployeeDAL.SpDatewiseScoreByEmployeeCompanyId(DtEmployees.Rows[a]["EmployeeId_PK"].ToString());

                if (Dtcolor == null)
                {
                    DtEmployees.Rows[a]["Color"] = "#ffffff";
                    DtEmployees.Rows[a]["LblText"] = "Test not attempted";
                    //DtEmployees.Rows[a]["LblWidth"] = "120px";
                }
                else
                {
                    if (Dtcolor.Rows.Count == 0)
                    {
                        DtEmployees.Rows[a]["Color"] = "#ffffff";
                        DtEmployees.Rows[a]["LblText"] = "Test not attempted";
                        // DtEmployees.Rows[a]["LblWidth"] = "120px";
                    }
                    else
                    {


                        DataRow[] result = Dtcolor.Select("Color='#e43c37'");
                        if (result.Length > 0)
                        {
                            DtEmployees.Rows[a]["Color"] = "#e43c37";
                            DtEmployees.Rows[a]["LblText"] = "";
                            //DtEmployees.Rows[a]["LblWidth"] = "90px";
                        }
                        else if (result.Length == 0)
                        {
                            DataRow[] result1 = Dtcolor.Select("Color='#f4fb73'");
                            if (result1.Length > 0)
                            {
                                DtEmployees.Rows[a]["Color"] = "#f4fb73";
                                DtEmployees.Rows[a]["LblText"] = "";
                                // DtEmployees.Rows[a]["LblWidth"] = "90px";
                            }
                            else
                            {
                                DtEmployees.Rows[a]["Color"] = "#88e46d";
                                DtEmployees.Rows[a]["LblText"] = "";
                                //DtEmployees.Rows[a]["LblWidth"] = "90px";
                            }
                        }
                    }

                    //#f4fb73 - yellow
                    // #88e46d -green
                    //#e43c37 - red
                }

            }
            // 
            if (DtEmployees != null && DtEmployees.Rows.Count > 0)
            {
                GvViewEmployee.PageIndex = 0;
                GvViewEmployee.DataSource = DtEmployees;
                GvViewEmployee.DataBind();
                Session["FilterBy"] = "EmployeeId";
            }

            for (int x = 0; x < GvViewEmployee.Rows.Count; x++)
            {
                Label LblColor = (Label)GvViewEmployee.Rows[x].FindControl("LblColor");
                if (LblColor.Text == "Test not attempted")
                {
                    LblColor.Width = 120;
                }
            }
            ddlFilter.SelectedIndex = 0;
            //txtName.Text = "";
        }


        //protected void BtnRefreshAll_Click(object sender, EventArgs e)
        //{
        //    try
        //    {
        //        Session["FilterBy"] = null;
        //        BindCompanyEmployees(0);
        //        ddlCadre.SelectedIndex = 0;
        //        ddlDepartment.SelectedIndex = 0;
        //        ddlFilter.SelectedIndex = 0;
        //        txtEmployeeId.Text = "";
        //        txtName.Text = "";
        //    }
        //    catch
        //    {

        //    }
        //}

        //protected void BtnRefresh_Click(object sender, EventArgs e)
        //{
        //    try
        //    {
        //        Session["FilterBy"] = null;
        //        BindCompanyEmployees(0);
        //        ddlCadre.SelectedIndex = 0;
        //        ddlDepartment.SelectedIndex = 0;
        //        ddlFilter.SelectedIndex = 0;
        //        txtEmployeeId.Text = "";
        //        txtName.Text = "";
        //    }
        //    catch
        //    {

        //    }
        //}
    }
}