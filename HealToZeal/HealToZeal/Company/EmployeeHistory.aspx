﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Company/Company.Master" AutoEventWireup="true" CodeBehind="EmployeeHistory.aspx.cs" Inherits="HealToZeal.Company.EmployeeHistory" %>

<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=11.0.0.0, Culture=neutral, PublicKeyToken=89845dcd8080cc91" Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="LeftColumn" runat="server">
    <div class="container">
        <h5 class="hs_heading" id="hs_appointment_form_link">
                 <asp:LinkButton ID="lbBack" runat="server" PostBackUrl="~/Company/ViewEmployee.aspx" CssClass="btn btn-link btn-lg">Back</asp:LinkButton></h5>
          <h4 class="hs_heading" id="hs_appointment_form_link">  <asp:Label ID="lblName" runat="server" Text=""></asp:Label>mindfulness trend
        </h4>

        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12">
                <rsweb:ReportViewer ID="ReportViewer1" runat="server" Font-Names="Verdana" Font-Size="8pt" Height="500px" WaitMessageFont-Names="Verdana" WaitMessageFont-Size="14pt" Width="1000px" ShowBackButton="False" ShowCredentialPrompts="False" ShowDocumentMapButton="False" ShowExportControls="False" ShowFindControls="False" ShowPageNavigationControls="False" ShowParameterPrompts="False" ShowPrintButton="False" ShowPromptAreaButton="False" ShowRefreshButton="False" ShowToolBar="False" ShowWaitControlCancelLink="False" ShowZoomControl="False" AsyncRendering="False">
                    <LocalReport ReportPath="Reports\EmployeeHistory.rdlc">
                        <DataSources>
                            <rsweb:ReportDataSource DataSourceId="ObjectDataSource1" Name="DataSet1" />
                        </DataSources>
                    </LocalReport>
                </rsweb:ReportViewer>
                <asp:ObjectDataSource ID="ObjectDataSource1" runat="server" OldValuesParameterFormatString="original_{0}" SelectMethod="GetData" TypeName="HealToZeal.AllClustersTableAdapters.Sp_EmployeeHistoryTableAdapter">
                    <SelectParameters>
                        <asp:QueryStringParameter DbType="Guid" Name="EmployeeId" QueryStringField="Id" />
                    </SelectParameters>
                </asp:ObjectDataSource>
            </div>
        </div>
         <%--<div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12">
                <rsweb:ReportViewer ID="ReportViewer2" runat="server" Font-Names="Verdana" Font-Size="8pt" Height="500px" WaitMessageFont-Names="Verdana" WaitMessageFont-Size="14pt" Width="1000px" ShowBackButton="False" ShowCredentialPrompts="False" ShowDocumentMapButton="False" ShowExportControls="False" ShowFindControls="False" ShowPageNavigationControls="False" ShowParameterPrompts="False" ShowPrintButton="False" ShowPromptAreaButton="False" ShowRefreshButton="False" ShowToolBar="False" ShowWaitControlCancelLink="False" ShowZoomControl="False" AsyncRendering="False">
                    <LocalReport ReportPath="Reports\EmployeeHistory.rdlc">
                        <DataSources>
                            <rsweb:ReportDataSource DataSourceId="ObjectDataSource1" Name="DataSet1" />
                        </DataSources>
                    </LocalReport>
                </rsweb:ReportViewer>
                <asp:ObjectDataSource ID="ObjectDataSource2" runat="server" OldValuesParameterFormatString="original_{1}" SelectMethod="GetData" TypeName="HealToZeal.AllClustersTableAdapters.Sp_EmployeeHistoryTableAdapter">
                    <SelectParameters>
                        <asp:QueryStringParameter DbType="Guid" Name="EmployeeId" QueryStringField="Id" />
                    </SelectParameters>
                </asp:ObjectDataSource>
            </div>
        </div>--%>
    </div>
</asp:Content>
