﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="LoginUserCntrl.ascx.cs"
    Inherits="HealToZeal.UserControl.LoginUserCntrl" %>


    
<li id="LiName" runat="server"><a><strong>Welcome : </strong><span>
    <asp:Label ID="LblName" runat="server"></asp:Label></span></a> </li>
<li class="dropdown">
    <div id="DV" runat="server" class="dropdown">
        <a data-toggle="dropdown" class="dropdown-toggle" href="#">
            <span class="glyphicon glyphicon-cog"> </span>
            <span class="visible-xs-inline">Doctor Information </span></a>
        <ul role="menu" class="dropdown-menu">
            <li class="dr-pick">
                <img src="../images/avatar.jpg" />
            </li>
            <li>
            <a>
                <div class="form-group">
                    <input type="text" id="UserNameVal" runat="server" name="UserName" class="form-control"
                        placeholder="User Name (required)" required />
                </div>
            </a>
            </li>
            <li>
            <a>
                <div class="form-group">
                    <input type="password" id="PasswordVal" runat="server" name="Password" class="form-control"
                        placeholder="Password (required)" required />
                </div>
            </a>
            </li>
            <li>
            <a>
                <asp:Label ID="LblEmail" runat="server"></asp:Label>
            </a>
            </li>
            <li class="divider"></li>
            <%--<li id="LiDr" runat="server"><a href="../DoctorProfile.aspx"><span class="glyphicon glyphicon-user">
            </span>My Profile</a></li>--%>
            <li class="divider"></li>
            <li>
              <%--  <asp:Button ID="ImgBtnLogin" runat="server" Text="Add employee" CssClass="btn btn-default"
                    OnClick="ImgBtnLogin_Click" />--%>
            </li>
        </ul>
    </div>
</li>
