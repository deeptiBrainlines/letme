﻿<%@ Page Title="" Language="C#" Async="true" MasterPageFile="~/Employee/Employee.Master" AutoEventWireup="true" CodeBehind="EmpRegistrationFormOld.aspx.cs" Inherits="HealToZeal.Employee.EmpRegistrationForm" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .style1 {
            width: 122px;
            font-size: medium;
        }

        .style2 {
            width: 200px;
        }

        .style3 {
            text-align: center;
            text-decoration: underline;
            font-family: Arial, Helvetica, sans-serif;
            font-size: large;
        }

        .gmailbutton {
            background-color: #ff0000;
            color: white;
            width: 150px;
        }
    </style>

    <script type = "text/javascript">
        function ValidateCheckBox(sender, args) {
            if (document.getElementById("<%=chkTandD.ClientID %>").checked == true) {
                args.IsValid = true;
            } else {
                args.IsValid = false;
            }
        }
    </script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="EmployeeColumn" runat="server">
    <div class="container">
        <div class="col-md-12">
            <div class="col-md-3"></div>
            <div class="col-md-6 well">
                <div>

                    <table style="width=100%; align-content: center;">
                        <caption class="style3">
                            <h2><strong>Registration Form </strong></h2>
                        </caption>
                        <%-- <tr>
                        <td class="style1"></td>
                        <td class="style2"></td>
                       
                    </tr>--%>
                        <%-- <tr>
                <td>
                    <script>
                        window.fbAsyncInit = function () {
                            FB.init({
                                appId: '2447416122199968',
                                xfbml: true,
                                version: 'v4.0'
                            });
                            FB.AppEvents.logPageView();
                        };

                        (function (d, s, id) {
                            var js, fjs = d.getElementsByTagName(s)[0];
                            if (d.getElementById(id)) { return; }
                            js = d.createElement(s); js.id = id;
                            js.src = "https://connect.facebook.net/en_US/sdk.js";
                            fjs.parentNode.insertBefore(js, fjs);
                        }(document, 'script', 'facebook-jssdk'));
                    </script>
                </td>
            </tr>--%>

                        <div id="fb-root"></div>
                        <script async defer crossorigin="anonymous" src="https://connect.facebook.net/en_US/sdk.js#xfbml=1&version=v5.0&appId=682470882264872&autoLogAppEvents=1"></script>
                        <tr>
                            <td class="style1">
                                <b>
                                    <asp:Label ID="lblfirstname" runat="server" Text="First Name:"></asp:Label></b>
                            </td>
                            <td class="style2">
                                <asp:TextBox ID="txtfirstname" runat="server" Height="22px" MaxLength="50"
                                    Width="158px"></asp:TextBox>

                            </td>

                            <td colspan="2">
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server"
                                    ControlToValidate="txtfirstname" ErrorMessage="* Please Enter your First Name"
                                    ForeColor="#CC0000"></asp:RequiredFieldValidator></td>
                            <td>
                                <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" TargetControlID="txtfirstname" FilterType="LowercaseLetters,UppercaseLetters,Custom" runat="server"></asp:FilteredTextBoxExtender>
                            </td>
                        </tr>


                        <tr>
                            <td class="style1">
                                <b>
                                    <asp:Label ID="lbllastname" runat="server" Text="Last Name:"></asp:Label></b>
                            </td>
                            <td class="style2">
                                <asp:TextBox ID="txtlastname" runat="server" Height="22px" MaxLength="50"
                                    Width="158px"></asp:TextBox>

                            </td>
                            <td>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server"
                                    ControlToValidate="txtlastname" ErrorMessage="* Please Enter your Last Name"
                                    ForeColor="#CC0000"></asp:RequiredFieldValidator></td>
                            <td>
                                <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" TargetControlID="txtlastname" FilterType="LowercaseLetters,UppercaseLetters,Custom" runat="server"></asp:FilteredTextBoxExtender>
                            </td>

                        </tr>

                        <%--    <tr>
                        <td class="style1">
                            <b>
                                <asp:Label ID="Label1" runat="server" Text="Employee CompanyID. :"></asp:Label></b>
                        </td>
                        <td class="style2">
                            <asp:TextBox ID="txtEmpCompID" runat="server" Height="22px" MaxLength="10"
                                Width="158px"></asp:TextBox>
                        </td>
                        <td>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator11" runat="server"
                                ControlToValidate="txtEmpCompID" ErrorMessage="Please Enter your Phone No"
                                ForeColor="#CC0000"></asp:RequiredFieldValidator>
                        </td>
                        <td>
                            <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender4" TargetControlID="txtEmpCompID" FilterType="Numbers" runat="server"></asp:FilteredTextBoxExtender>
                        </td>
                    </tr>--%>

                        <tr>
                            <td class="style1">
                                <b>
                                    <asp:Label ID="lblemail" runat="server" Text="Email:"></asp:Label></b>
                            </td>
                            <td class="style2">
                                <asp:TextBox ID="txtemail" runat="server" Height="22px" MaxLength="50"
                                    Width="158px"></asp:TextBox>
                            </td>
                            <td>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server"
                                    ControlToValidate="txtemail" ErrorMessage="* Please Enter your Email ID"
                                    ForeColor="#CC0000"></asp:RequiredFieldValidator></td>
                            <td>
                                <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="txtemail" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" ErrorMessage="Invalid Email Id"></asp:RegularExpressionValidator>

                            </td>

                        </tr>


                        <tr>
                            <td class="style1">
                                <b>
                                    <asp:Label ID="lblphoneno" runat="server" Text="Mobile No. :"></asp:Label></b>
                            </td>
                            <td class="style2">
                                <asp:TextBox ID="txtphoneno" runat="server" Height="22px" MaxLength="10"
                                    Width="158px"></asp:TextBox></td>
                            <td>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server"
                                    ControlToValidate="txtphoneno" ErrorMessage="* Please Enter your Phone No"
                                    ForeColor="#CC0000"></asp:RequiredFieldValidator></td>
                            <td>
                                <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender3" TargetControlID="txtphoneno" FilterType="Numbers" runat="server"></asp:FilteredTextBoxExtender>

                            </td>

                        </tr>

                        <br />
                        <br />

                        <tr>
                            <td></td>
                            <td class="">
                                <asp:CheckBox ID="chkTandD" runat="server"  Height="22px" MaxLength="50" />&nbsp;&nbsp;&nbsp;<label>Terms and Disclaimer</label>
                         <asp:CustomValidator ID="CustomValidator1" runat="server" ErrorMessage="* Please Select checkbox to agree for Terms and Disclaimer"
                                    ForeColor="#CC0000" ClientValidationFunction ="ValidateCheckBox"></asp:CustomValidator><br />
                            </td>


                        </tr>
                        <tr>
                            <td></td>
                            <td>By clicking Register, you agree to our Terms, Data Policy and Cookie Policy.
                            </td>
                        </tr>

                        <tr>
                            <td class="style1"></td>

                            <%--   <fb:login-button
                scope="public_profile,email" onlogin="checkLoginState();">
            </fb:login-button>--%>
                            <td></td>
                            <asp:Label ID="lblMessage" runat="server" Text="Label" Visible="false"></asp:Label>
                        </tr>
                        <tr>
                            <td></td>
                            <td>
                                <asp:Button ID="Button1" runat="server" Text="Register" CssClass="btn btn-info btn-md" OnClick="btnsubmit_Click" />



                            </td>
                        </tr>
                        <tr>
                            <td></td>
                            <td>
                                <br />
                                <asp:ImageButton ID="ImageButton1" runat="server" ToolTip="Login With Facebook" ImageUrl="~/dist/img/icons8-facebook-circled-48.png" CausesValidation="false" OnClick="FBLogin_Click" Height="36px" Width="36px" BorderWidth="1" BorderColor="#cccccc" />
                                &nbsp;
            
                  <asp:ImageButton ID="ImageButton2" runat="server" ToolTip="Login With Gmail" ImageUrl="~/dist/img/icons8-gmail-48.png" CausesValidation="false" OnClick="btngmaillogin_Click" Height="36px" Width="36px" BorderWidth="1" BorderColor="#cccccc" />

                            </td>
                        </tr>


                    </table>
                </div>
                <%-- <asp:ImageButton ID="btnsubmit" runat="server" ToolTip="Register" ImageUrl="~/dist/img/icons8-signin-64.png" OnClick="btnsubmit_Click" />--%>
            </div>
            <div class="col-md-3"></div>


            <%-- <asp:Button ID="btnsubmit" runat="server" Text="Register" CssClass="btn-info" OnClick="btnsubmit_Click" />--%>

            <%-- <div class="col-md-6">
                <div>
                    <h2>Use our another serives to Login</h2>--%>
            <%--<asp:Button ID="FBLogin" runat="server" Text="Login With Facebook" CssClass="btn-info" OnClick="FBLogin_Click" CausesValidation="false" Class="facebookLogin" />
            
        <asp:Button ID="btngmaillogin" runat="server" Text="Login With Gmail"  OnClick="btngmaillogin_Click" CausesValidation="false" CssClass="btnLogInGoogle"/>--%>
            <%-- <asp:ImageButton ID="FBLogin" runat="server" ToolTip="Login With Facebook" ImageUrl="~/dist/img/icons8-facebook-circled-48.png" CausesValidation="false" OnClick="FBLogin_Click" />
                    <asp:ImageButton ID="btngmaillogin" runat="server" ToolTip="Login With Gmail" ImageUrl="~/dist/img/icons8-gmail-48.png" CausesValidation="false" OnClick="btngmaillogin_Click" />



                </div>

            </div>--%>
        </div>
    </div>
</asp:Content>
