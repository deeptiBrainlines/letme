﻿using BE;
using DAL;
using System;
using System.Data;
using System.Web.UI;

namespace HealToZeal.Employee
{
    public partial class EmployeeMessagePaid : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Session["Heading"] = "instructions";
            if (!Page.IsPostBack)
            {
                if (Session["EmployeeId"] != null)
                {
                    EmployeeDAL objEmployeeDAL = new EmployeeDAL();
                    EmployeeBE objEmployeeBE = new EmployeeBE();
                    objEmployeeBE.propEmployeeId_PK = Convert.ToString(Session["EmployeeId"]);
                    DataTable empDT = objEmployeeDAL.GetEmployeebyEmpid(objEmployeeBE);
                    if (empDT != null && empDT.Rows.Count > 0)
                    {
                        //if ( Convert.ToString(empDT.Rows[0]["TestFlag"]) != "free")
                        //{
                        //    Response.Redirect("FirstLoginEmailVerifiedFreeTest.aspx", false);
                        //}

                    }
                    else
                    {

                    }
                }
                else
                {
                    Response.Redirect("EmployeeLogin.aspx");
                }

            }
        }

        protected void btnNext_Click(object sender, EventArgs e)
        {
            try
            {
                if (Request.QueryString["first"] != null)
                {
                    Response.Redirect("EmployeeHome.aspx?first=1", false);
                }
                else if (Request.QueryString["Assessment"] != null)
                {
                    Response.Redirect("EmployeeHome.aspx?Assessment=" + Request.QueryString["Assessment"].ToString());
                }
                else if (Request.QueryString["FeedBack"] != null)
                {
                    Response.Redirect("EmployeeHome.aspx?FeedBack=" + Request.QueryString["FeedBack"].ToString());
                }
                else
                {
                    Response.Redirect("EmployeeHome.aspx");

                }
            }
            catch (Exception ex)
            {
               // log.Error(ex.Message.ToString() + " " + ex.StackTrace.ToString());
            }

        }
    }
}