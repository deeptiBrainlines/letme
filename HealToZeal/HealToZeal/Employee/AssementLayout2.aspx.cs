﻿using BE;
using DAL;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net.Mail;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace HealToZeal.Employee
{
    public partial class AssementLayout2 : System.Web.UI.Page
    {
        log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        AssessmentQuestionRelationDAL objAssessmentQuestionRelationDAL = new AssessmentQuestionRelationDAL();
        EmployeeAssessmentDetailsDAL objEmployeeAssessmentDetailsDAL = new EmployeeAssessmentDetailsDAL();
        EmployeeAssessmentDetailsBE objEmployeeAssessmentDetailsBE = new EmployeeAssessmentDetailsBE();
        AnswerMasterDAL objAnswerMasterDAL = new AnswerMasterDAL();
        DataTable DtSaveAnswer = new DataTable();
        public static List<int> arrayqueCntList = new List<int>();
        string AssessmentId = "";
        string QuestionId = "";
        string EmployeeId = "";
        int position = 0;
        int Default = 1;
        public static int maxNum = 0;
        public static int queCount = 1;
        RandomGenerator randomGenerator = new RandomGenerator();

        protected void Page_Load(object sender, EventArgs e)
        {
            Session["Heading"] = "assessment";
            divalert.Visible = false;
            string batchID = Session["BatchID"].ToString();
            if (!Page.IsPostBack)
            {
                Session["Pagename"] = null;
                try
                {
                    if (Session["EmployeeId"] != null)
                    {
                        //assessment id total question count
                        //AssessmentId = "AAD38F93-0DC7-4EA1-8A61-6B95816AED6B"; //assessment session
                        EmployeeId = Session["EmployeeId"].ToString();

                        //check whether assessment is submitted or not
                        EmployeeAssessmentsHistoryDAL objEmployeeAssessmentsHistoryDAL = new EmployeeAssessmentsHistoryDAL();
                        DataTable DtAssessmentSubmitted = objEmployeeAssessmentsHistoryDAL.EmployeeAssementsGet(EmployeeId, batchID);

                        if (DtAssessmentSubmitted != null && DtAssessmentSubmitted.Rows.Count > 0)
                        {
                            if (DtAssessmentSubmitted.Select("AssessmentSubmitted=0").Length > 0)
                            {
                                if (Session["AssessmentInstanceId"] != null)
                                {
                                    DataTable DtTotalQuestionCount = objEmployeeAssessmentDetailsDAL.EmployeeAssessmentDetailsGetCount(EmployeeId, Session["AssessmentInstanceId"].ToString(), Session["AssessmentInstanceCompanyId"].ToString());
                                    if (DtTotalQuestionCount != null && DtTotalQuestionCount.Rows.Count > 0)
                                    {
                                        Session["TotalQuestionCount"] = DtTotalQuestionCount.Rows[0][0].ToString();
                                        maxNum = Convert.ToInt32(Session["TotalQuestionCount"].ToString());
                                        //if (Session["Position"] == null)
                                        //{
                                        //    btnPrevious.Enabled = false;
                                        //}
                                    }
                                    DataTable dtEmployees = new DataTable();
                                    dtEmployees = objEmployeeAssessmentDetailsDAL.EmployeeAssessmentDetailsGet(Session["AssessmentInstanceId"].ToString(), position, Session["EmployeeId"].ToString(), Session["AssessmentInstanceCompanyId"].ToString());
                                    if (dtEmployees != null && dtEmployees.Rows.Count > 0)
                                    {
                                        //  Session["TotalAnswers"] = Convert.ToInt32(dtEmployees.Rows[0]["TotalAnswers"].ToString());

                                        //    position = Convert.ToInt32(dtEmployees.Rows[0]["TotalAnswers"].ToString());

                                        //string str = dtEmployees.Rows[0]["TotalAnswers"].ToString();
                                        //str = str.TrimEnd(str[str.Length - 1]) + "0";
                                        //position = Convert.ToInt32(str);

                                        int count = ((Convert.ToInt32(dtEmployees.Rows[0]["TotalAnswers"].ToString()) / 1) * 1);
                                        position = randomGenerator.RandomNumber(1, maxNum);

                                        //position = count;
                                        Session["Position"] = position;
                                    }
                                    position = GetAlternateNumber(1, maxNum);
                                    Session["Position"] = position;

                                    if (Session["Position"] == null)
                                    {
                                        //btnPrevious.Enabled = false;
                                        //btnPrevious1.Enabled = false;
                                    }
                                    arrayqueCntList.Add(position);
                                    BindTestQuestions(position);
                                    //27 jan 2015
                                    BindSavedAnswers(position);
                                }
                                else
                                {

                                    Response.Redirect("EmployeeAssessments.aspx");
                                }
                                //
                            }
                            else
                            {
                                Response.Redirect("EmployeeHome.aspx?Assessment=Y");
                            }
                        }
                        //else
                        //{

                        //}
                    }
                    else
                    {
                        Response.Redirect("EmployeeLogin.aspx");
                    }
                }
                catch (Exception ex)
                {
                    log.Error(ex.Message.ToString() + " " + ex.StackTrace.ToString());
                }
            }
        }

        private void BindTestQuestions(int Position)
        {
            try
            {
                ////AssessmentId = Session["AssessmentId"].ToString();
                ////AssessmentId = "AAD38F93-0DC7-4EA1-8A61-6B95816AED6B";

                //if (Session["Position"] != null)
                //{
                //    position = Convert.ToInt32(Session["Position"].ToString());
                //}
                ////if (position > 0)
                ////{
                ////    PopulatePager(position, (position / 10));
                ////}

                DataTable dtAssessmentQuestions = objAssessmentQuestionRelationDAL.AssessmentQuestionsRelationGet(Session["EmployeeId"].ToString(), Position, Session["AssessmentInstanceId"].ToString(), Session["AssessmentInstanceCompanyId"].ToString());
                DlistQuestions1.DataSource = dtAssessmentQuestions;
                DlistQuestions1.DataBind();

                //CheckPosition();
                BindAnswers();
                queCount++;
                //if (Session["Position"] != null)
                //Session["Position"] = Convert.ToInt32(Session["Position"].ToString()) + DlistQuestions1.Items.Count ;
                //else
                //Session["Position"] = DlistQuestions1.Items.Count;
            }
            catch (Exception ex)
            {
                log.Error(ex.Message.ToString() + " " + ex.StackTrace.ToString());
            }
        }

        private void BindAnswers()
        {
            try
            {
                foreach (DataListItem questions in DlistQuestions1.Items)
                {
                    QuestionId = ((Label)questions.FindControl("lblQuestionId")).Text;
                    DataTable DtAnswers = objAnswerMasterDAL.AnswerMasterGetWithDescriptionByQuestionId(QuestionId);
                    ((RadioButtonList)questions.FindControl("rblistAnswers")).DataSource = DtAnswers;
                    ((RadioButtonList)questions.FindControl("rblistAnswers")).DataTextField = "AnswerShort";
                    ((RadioButtonList)questions.FindControl("rblistAnswers")).DataValueField = "AnswerId_PK";
                    ((RadioButtonList)questions.FindControl("rblistAnswers")).DataBind();
                }
            }
            catch (Exception ex)
            {
                log.Error(ex.Message.ToString() + " " + ex.StackTrace.ToString());
            }
        }

        protected void btnNext_Click(object sender, EventArgs e)
        {
            try
            {
                if (Session["EmployeeId"] != null)
                {
                    try
                    {
                        //save answers 
                        int status = SaveAnswersSession();
                        if (status > 0)
                        {
                            if (Session["Position"] != null)
                            {
                                //if (((rptPager.Items.Count - 2) * 10) == Convert.ToInt32(Session["Position"].ToString()))
                                //{

                                //}
                                //else
                                //{
                                //Session["Position"] = Convert.ToInt32(Session["Position"].ToString()) + DlistQuestions1.Items.Count;
                                Session["Position"] = randomGenerator.RandomNumber(1, maxNum);

                                //}                          
                               // btnPrevious.Enabled = true;
                                // btnPrevious1.Enabled = true;
                            }
                            else
                            {
                                Session["Position"] = DlistQuestions1.Items.Count;
                            }

                            BindTestQuestions(Convert.ToInt32(Session["Position"].ToString()));
                            BindSavedAnswers(Convert.ToInt32(Session["Position"].ToString()));

                            int Count = Convert.ToInt32(Session["Position"].ToString()) + DlistQuestions1.Items.Count;
                            //if (Convert.ToInt32(Session["TotalQuestionCount"].ToString()) == Convert.ToInt32(Session["Position"].ToString()))
                            if (Convert.ToInt32(Session["TotalQuestionCount"].ToString()) == queCount)
                            {
                                Session["TotalQuestionCount"] = null;
                                Session["Position"] = null;
                                //send mail 
                                EmployeeAssessmentsHistoryBE objEmployeeAssessmentsHistoryBE = new EmployeeAssessmentsHistoryBE();
                                EmployeeAssessmentsHistoryDAL objEmployeeAssessmentsHistoryDAL = new EmployeeAssessmentsHistoryDAL();

                                objEmployeeAssessmentsHistoryBE.propEmployeeId_FK = EmployeeId;
                                //objEmployeeAssessmentsHistoryBE.propAssessmentId_FK=
                                objEmployeeAssessmentsHistoryBE.propAssessmentSubmitted = "Y";
                                objEmployeeAssessmentsHistoryBE.propCreatedBy = EmployeeId;
                                objEmployeeAssessmentsHistoryBE.propCreatedDate = DateTime.Now.Date;
                                objEmployeeAssessmentsHistoryBE.propAssessmentInstanceId_FK = Session["AssessmentInstanceId"].ToString();
                                objEmployeeAssessmentsHistoryBE.propInstanceCompanyRelationId_FK = Session["AssessmentInstanceCompanyId"].ToString();

                                status = objEmployeeAssessmentsHistoryDAL.EmployeeAssessmentsHistoryInsert(objEmployeeAssessmentsHistoryBE);
                                lblMessage.Text = "Assessment submitted successfully...";


                                if (status > 0)
                                {
                                    //Session["EmployeeId"]

                                    CompanyDetailsDAL objCompanyDetailsDAL = new CompanyDetailsDAL();
                                    DataTable DtCompany = objCompanyDetailsDAL.CompanyDetailsGetByEmployeeId(Session["EmployeeId"].ToString());
                                    //CompanyName
                                    System.Net.Mail.MailMessage mail = new System.Net.Mail.MailMessage();
                                    // System.Web.Mail.MailMessage mail = new System.Web.Mail.MailMessage();

                                    mail.To.Add("priyamvada.bavadekar@letmetalk2.me");
                                    mail.Bcc.Add("priyamvada.bavadekar@letmetalk2.me");
                                    // mail.From = "contact@letmetalk2.me";
                                    //mail.From = new MailAddress("contact@letmetalk2.me", "Let me talk to me", System.Text.Encoding.UTF8);
                                    mail.Subject = "Assessment submitted by user";
                                    string getDt = DateTime.Now.Date.AddDays(7).ToString("MM/dd/yyyy");

                                    mail.Body = "<html><body><font color=\"black\">Hello Mam, <br/><br/> <b>" + Session["UserName"].ToString() + "</b> with EmployeeId " + Session["EmployeeCompanyId"].ToString() + " of company " + DtCompany.Rows[0]["CompanyName"].ToString() + " has submitted the assessment successfully. <br/><br/> Thank You.<BR/><br/> Best Regards, <br/><br/> Let me talk to me Team.</font></body></html>";
                                    mail.IsBodyHtml = true;

                                    mail.Priority = System.Net.Mail.MailPriority.High;
                                    SmtpClient client = new SmtpClient();
                                    //client.Credentials = new System.Net.NetworkCredential("contact@letmetalk2.me", "contact123");
                                    //client.Port = 587;
                                    //client.Host = "hazel.arvixe.com";
                                    client.EnableSsl = false;

                                    client.Send(mail);


                                    //message = "Employee added successfully...";
                                    divalert.Attributes.Add("class", "alert alert-success");
                                    lblMessage.Text = "Employee added successfully...";
                                    //System.Web.Mail.MailMessage mail = new System.Web.Mail.MailMessage();
                                    //mail.To = "priyamvada@chetanasystems.com";
                                    //mail.From = "HealToZeal@chetanatspl.com";
                                    //mail.Subject = "Assessment submitted by user";
                                    //mail.Body = "<html><body><font color=\"black\">Hello Mam, <br/><br/> <b>" + Session["UserName"].ToString() + "</b> with EmployeeId " + Session["EmployeeCompanyId"].ToString() + " of company " + DtCompany.Rows[0]["CompanyName"].ToString() + " has submitted the assessment successfully. <br/><br/> Thank You.<BR/><br/> Best Regards, <br/><br/> HealToZealTeam.</font></body></html>";
                                    //mail.BodyFormat = MailFormat.Html;
                                    //mail.Fields.Add("http://schemas.microsoft.com/cdo/configuration/smtpauthenticate", "1");    //basic authentication
                                    //mail.Fields.Add("http://schemas.microsoft.com/cdo/configuration/sendusername", "HealToZeal@chetanatspl.com"); //set your username here
                                    //mail.Fields.Add("http://schemas.microsoft.com/cdo/configuration/sendpassword", "HealToZeal");    //set your password here

                                    //System.Web.Mail.SmtpMail.SmtpServer = "holly.arvixe.com";  //your real server goes here                        
                                    //System.Web.Mail.SmtpMail.Send(mail);

                                    /*  System.Net.Mail.MailMessage insMail = new System.Net.Mail.MailMessage();
                                       System.Net.Mail.MailAddress insFrom = new MailAddress("HealToZeal@chetanatspl.com", "HealToZeal");
                                       insMail.From = insFrom;
                                       insMail.To.Add("priyamvada@chetanasystems.com");
                                       insMail.Subject = "Assessment submitted by user";
                                       insMail.IsBodyHtml = true;
                                       string strBody = "<html><body><font color=\"black\">Hello Mam <br/><br/> <b>" + Session["UserName"].ToString() + "</b> with EmployeeId " + Session["EmployeeCompanyId"].ToString() + " has submitted the assessment successfully. <br/><br/> Thank You.<BR/><br/> Best Regards, <br/><br/> HealToZealTeam.</font></body></html>";
                                       insMail.Body = strBody;                               
                                       System.Net.Mail.SmtpClient ns = new System.Net.Mail.SmtpClient("holly.arvixe.com");

                                       ns.Send(insMail);*/
                                    //
                                    // Response.Redirect("EmployeeThanksMessage.aspx?Assessment=submited",false);

                                    //ask for another test
                                    string batchID = Session["BatchID"].ToString();
                                    DataTable DtAssessmentSubmitted = objEmployeeAssessmentsHistoryDAL.EmployeeAssementsGet(EmployeeId, batchID);

                                    if (DtAssessmentSubmitted != null && DtAssessmentSubmitted.Rows.Count > 0)
                                    {
                                        if (DtAssessmentSubmitted.Select("AssessmentSubmitted=0").Length > 0)
                                        {
                                            Response.Redirect("SuggestionFrnd.aspx");
                                            // Response.Redirect("AssessmentMessage.aspx");
                                            //  ScriptManager.RegisterClientScriptBlock(this, GetType(), "MessageBox", "DisplayColorBox('AssessmentMessage.aspx');", true);
                                        }
                                        else
                                        {
                                            Response.Redirect("EmployeeFeedback.aspx", false);
                                        }
                                    }
                                }
                                else
                                {
                                    //lblmessage.Text = "Can not submit test...";
                                }
                            }
                            else if (Convert.ToInt32(Session["TotalQuestionCount"].ToString()) == Count)
                            {
                                //  btnNext.Text = "Submit";
                            }
                            if (Session["Position"] != null)
                            {
                               // btnPrevious.Enabled = true;
                                // btnPrevious1.Enabled = true;
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        log.Error(ex.Message.ToString() + " " + ex.StackTrace.ToString());
                    }
                }
                else
                {
                    Response.Redirect("EmployeeLogin.aspx", false);
                }

            }
            catch (Exception ex)
            {
                // ex1.InnerException.ToString();
                log.Error(ex.Message.ToString() + " " + ex.StackTrace.ToString());
                //Response.Redirect("EmployeeLogin.aspx",false );
            }
        }

        private int SaveAnswersSession()
        {
            try
            {
                int status = 0;
                // objEmployeeAssessmentDetailsBE.propAssessmentId_FK = "9465B829-997E-42F1-85D2-128FF3E5B6C6";// AAD38F93 -0DC7-4EA1-8A61-6B95816AED6B"; //AssessmentId 
                EmployeeId = Session["EmployeeId"].ToString();
                objEmployeeAssessmentDetailsBE.propEmployeeId_FK = EmployeeId;
                //    objEmployeeAssessmentDetailsBE.propEmployeeId_FK = "C885389E-8F6D-4B6D-AAC3-57EE93336C25"; // logged in user
                objEmployeeAssessmentDetailsBE.propCreatedBy = EmployeeId;
                //  objEmployeeAssessmentDetailsBE.propCreatedBy = "C885389E-8F6D-4B6D-AAC3-57EE93336C25";
                objEmployeeAssessmentDetailsBE.propCreatedDate = DateTime.Now.Date;

                foreach (DataListItem answerItem in DlistQuestions1.Items)
                {
                    QuestionId = ((Label)answerItem.FindControl("lblQuestionId")).Text;
                    //  string AnswerId = ((RadioButtonList)answerItem.FindControl("rblistAnswers")).SelectedItem.Value;
                    string AnswerId = "";
                    RadioButtonList rblistAnswers = (RadioButtonList)answerItem.FindControl("rblistAnswers");
                    foreach (ListItem rdo in rblistAnswers.Items)
                    {
                        if (rdo.Selected)
                        {
                            AnswerId = rdo.Value;
                        }
                    }
                    objEmployeeAssessmentDetailsBE.propAnswerId_FK = AnswerId;
                    objEmployeeAssessmentDetailsBE.propQuestionId_FK = QuestionId;
                    objEmployeeAssessmentDetailsBE.propInstanceCompanyRelationId_FK = Session["AssessmentInstanceCompanyId"].ToString();
                    status = objEmployeeAssessmentDetailsDAL.EmployeeAssessmentDetailsInsert(objEmployeeAssessmentDetailsBE);
                    divalert.Visible = true;
                    {
                        if (status <= 0)
                        {
                            // ScriptManager.RegisterStartupScript(this, this.GetType(), "starScript", "DisplayColorBox('WarningMessage.aspx');", true);  
                            divalert.Attributes.Add("class", "alert alert-danger");
                            lblMessage.Text = "Please select answer";
                            break;
                        }
                        else
                        {
                            // divalert.Attributes.Add("class", "alert alert-success");
                            divalert.Visible = false;
                            lblMessage.Text = "";
                        }
                    }
                }
                return status;
            }
            catch (Exception ex)
            {
                log.Error(ex.Message.ToString() + " " + ex.StackTrace.ToString());
                return 0;
            }
        }

        protected void btnPrevious_Click(object sender, EventArgs e)
        {
            try
            {
                int Count = Convert.ToInt32(Session["Position"].ToString()) + DlistQuestions1.Items.Count;
                if (Convert.ToInt32(Session["TotalQuestionCount"].ToString()) == Count)
                {
                    SaveAnswersSession();
                }

                if (Session["Position"] != null)
                {

                    // Session["Position"]
                    Count = Convert.ToInt32(Session["Position"].ToString()) - Default;
                    if (Count == 0)
                    {
                        //btnPrevious.Enabled = false;
                        // btnPrevious1.Enabled = false;
                    }
                    else
                    {
                       // btnPrevious.Enabled = true;
                        //btnPrevious1.Enabled = true;
                    }
                }
                Session["Position"] = Count;
                BindTestQuestions(Convert.ToInt32(Count));
                BindSavedAnswers(Convert.ToInt32(Count));
            }
            catch (Exception ex)
            {
                log.Error(ex.Message.ToString() + " " + ex.StackTrace.ToString());
                Response.Redirect("EmployeeLogin.aspx", false);
            }
        }

        private void BindSavedAnswers(int Position)
        {
            try
            {
                EmployeeAssessmentDetailsDAL objEmployeeAssessmentDetailsDAL = new EmployeeAssessmentDetailsDAL();
                DataTable DtEmployeeAnswers = new DataTable();

                AssessmentId = "9465B829-997E-42F1-85D2-128FF3E5B6C6";//"AAD38F93-0DC7-4EA1-8A61-6B95816AED6B"; //AssessmentId commented by meenakshi on 2019-01-10
                EmployeeId = Session["EmployeeId"].ToString();

                //27 jan 2015
                //if (Session["Position"] != null)
                //{
                //    position = Convert.ToInt32(Session["Position"].ToString());
                //}

                DtEmployeeAnswers = objEmployeeAssessmentDetailsDAL.EmployeeAssessmentDetailsGet(Session["AssessmentInstanceId"].ToString(), Position, EmployeeId, Session["AssessmentInstanceCompanyId"].ToString());

                foreach (DataListItem answerItem in DlistQuestions1.Items)
                {
                    QuestionId = ((Label)answerItem.FindControl("lblQuestionId")).Text;
                    //  string AnswerId = ((RadioButtonList)answerItem.FindControl("rblistAnswers")).SelectedItem.Value;

                    DataRow[] DrAnswer = DtEmployeeAnswers.Select("QuestionId_FK='" + QuestionId + "'");
                    if (DrAnswer.Length > 0 && DrAnswer != null)
                    {
                        RadioButtonList rblistAnswers = (RadioButtonList)answerItem.FindControl("rblistAnswers");
                        foreach (ListItem rdo in rblistAnswers.Items)
                        {
                            if (DrAnswer[0][1].ToString() == rdo.Value.ToString())
                            {
                                rdo.Selected = true;
                            }
                        }
                        FocusControlOnPageLoad(rblistAnswers.ClientID, this.Page);
                    }
                }

                int percentage = 0;
                // Progressbar.Attributes.Add("class", "progress-bar progress-bar-success");
               // Progressbar1.Attributes.Add("class", "progress-bar progress-bar-success");

                if (DtEmployeeAnswers != null && DtEmployeeAnswers.Rows.Count > 0)
                {
                    percentage = ((Convert.ToInt32(DtEmployeeAnswers.Rows[0]["TotalAnswers"].ToString()) * 100) / Convert.ToInt32(Session["TotalQuestionCount"].ToString()));

                }
                else
                {
                    //  percentage = ((Convert.ToInt32(Session["Position"]) * 100) / Convert.ToInt32(Session["TotalQuestionCount"].ToString()));
                    percentage = ((Convert.ToInt32(queCount * 100) / Convert.ToInt32(Session["TotalQuestionCount"].ToString())));

                }
                //Progressbar.Attributes.Add("style", "width:" + percentage + "%");
                // Progressbar.InnerText = percentage.ToString() + " % of " + Session["TotalQuestionCount"].ToString() + " Questions";

                //Progressbar1.Attributes.Add("style", "width:" + percentage + "%");
                //Progressbar1.InnerText = percentage.ToString() + " % of " + Session["TotalQuestionCount"].ToString() + " Questions";
                if (percentage == 0)
                {
                    // Progressbar.Attributes.Add("class", "progress-bar progress-bar-danger");
                    //Progressbar.Attributes.Add("style", "width:100%");
                    // Progressbar.InnerText = percentage.ToString() + " % of " + Session["TotalQuestionCount"].ToString() + " Questions";

                   // Progressbar1.Attributes.Add("class", "progress-bar progress-bar-danger");
                    //Progressbar1.Attributes.Add("style", "width:100%");
                   // Progressbar1.InnerText = percentage.ToString() + " % of " + Session["TotalQuestionCount"].ToString() + " Questions";

                    //lnkBtnJumpto.Visible = false;
                }
            }
            catch (Exception ex)
            {
                ex.InnerException.ToString();
                log.Error(ex.Message.ToString() + " " + ex.StackTrace.ToString());
            }
        }

        protected void DlistQuestions_ItemDataBound(object sender, DataListItemEventArgs e)
        {
            try
            {
                RadioButtonList rblistAnswers = (RadioButtonList)e.Item.FindControl("rblistAnswers");
                Label lblQuestionId = (Label)e.Item.FindControl("lblQuestionId");

                QuestionId = lblQuestionId.Text;
                DataTable DtAnswers = objAnswerMasterDAL.AnswerMasterGetWithDescriptionByQuestionId(QuestionId);
                //DataTable DtAnswers = objAnswerMasterDAL.AnswerMasterGetByQuestionId(QuestionId);
                rblistAnswers.DataSource = DtAnswers;
                rblistAnswers.DataTextField = "Answer";
                rblistAnswers.DataValueField = "AnswerId_PK";
                rblistAnswers.DataBind();
            }
            catch (Exception ex2)
            {
                ex2.InnerException.ToString();
                log.Error(ex2.Message.ToString() + " " + ex2.StackTrace.ToString());
            }
        }

        protected void Page_Changed(object sender, EventArgs e)
        {
            int pageIndex = int.Parse((sender as LinkButton).CommandArgument);

            int pagePosition = (pageIndex * 10) - 10;

            BindTestQuestions(pagePosition);
            BindSavedAnswers(pagePosition);
            //  this.GetCustomersPageWise(pageIndex);
        }

        protected void rblistAnswers_SelectedIndexChanged(object sender, EventArgs e)
        {

            //   string AnswerId = ((RadioButtonList)answerItem.FindControl("rblistAnswers")).SelectedItem.Value;
            // Session["Value"] = rblistAnswers.SelectedValue.ToString();

            try
            {
                if (Session["EmployeeId"] != null)
                {
                    //save answers 
                    int status = SaveAnswersSession();
                    if (status > 0)
                    {
                        if (Session["Position"] != null)
                        {
                            //if (((rptPager.Items.Count - 2) * 10) == Convert.ToInt32(Session["Position"].ToString()))
                            //{

                            //}
                            //else
                            //{


                            // Session["Position"] = Convert.ToInt32(Session["Position"].ToString()) + DlistQuestions.Items.Count;

                            int queNumber = GetAlternateNumber(1, maxNum);
                            Session["Position"] = queNumber;
                            //}                          
                            //btnPrevious.Enabled = true;
                            // btnPrevious1.Enabled = true;
                        }
                        else
                        {
                            Session["Position"] = DlistQuestions1.Items.Count;
                        }


                        BindTestQuestions(Convert.ToInt32(Session["Position"].ToString()));
                        BindSavedAnswers(Convert.ToInt32(Session["Position"].ToString()));


                        int Count = Convert.ToInt32(Session["Position"].ToString()) + DlistQuestions1.Items.Count;
                        //if (Convert.ToInt32(Session["TotalQuestionCount"].ToString()) == Convert.ToInt32(Session["Position"].ToString()))
                        if (Convert.ToInt32(Session["TotalQuestionCount"].ToString()) == Convert.ToInt32(queCount.ToString()))
                        {
                            Session["TotalQuestionCount"] = null;
                            Session["Position"] = null;
                            //send mail 
                            EmployeeAssessmentsHistoryBE objEmployeeAssessmentsHistoryBE = new EmployeeAssessmentsHistoryBE();
                            EmployeeAssessmentsHistoryDAL objEmployeeAssessmentsHistoryDAL = new EmployeeAssessmentsHistoryDAL();

                            objEmployeeAssessmentsHistoryBE.propEmployeeId_FK = EmployeeId;
                            //objEmployeeAssessmentsHistoryBE.propAssessmentId_FK=
                            objEmployeeAssessmentsHistoryBE.propAssessmentSubmitted = "Y";
                            objEmployeeAssessmentsHistoryBE.propCreatedBy = EmployeeId;
                            objEmployeeAssessmentsHistoryBE.propCreatedDate = DateTime.Now.Date;
                            objEmployeeAssessmentsHistoryBE.propAssessmentInstanceId_FK = Session["AssessmentInstanceId"].ToString();
                            objEmployeeAssessmentsHistoryBE.propInstanceCompanyRelationId_FK = Session["AssessmentInstanceCompanyId"].ToString();

                            status = objEmployeeAssessmentsHistoryDAL.EmployeeAssessmentsHistoryInsert(objEmployeeAssessmentsHistoryBE);
                            lblMessage.Text = "Assessment submitted successfully...";
                            //lblMessageInfo.Text = "Assessment submitted successfully...";
                            //ClientScript.RegisterStartupScript(this.GetType(), "Pop", "openModalQuestion();", true);

                            if (status > 0)
                            {
                                //Session["EmployeeId"]

                                CompanyDetailsDAL objCompanyDetailsDAL = new CompanyDetailsDAL();
                                DataTable DtCompany = objCompanyDetailsDAL.CompanyDetailsGetByEmployeeId(Session["EmployeeId"].ToString());
                                //CompanyName
                                System.Net.Mail.MailMessage mail = new System.Net.Mail.MailMessage();
                                // System.Web.Mail.MailMessage mail = new System.Web.Mail.MailMessage();

                                mail.To.Add("priyamvada.bavadekar@letmetalk2.me");
                                mail.Bcc.Add("priyamvada.bavadekar@letmetalk2.me");
                                // mail.From = "contact@letmetalk2.me";
                                mail.From = new MailAddress("contact@letmetalk2.me", "Let me talk to me", System.Text.Encoding.UTF8);
                                mail.Subject = "Assessment submitted by user";
                                string getDt = DateTime.Now.Date.AddDays(7).ToString("MM/dd/yyyy");

                                mail.Body = "<html><body><font color=\"black\">Hello Mam, <br/><br/> <b>" + Session["UserName"].ToString() + "</b> with EmployeeId " + Session["EmployeeCompanyId"].ToString() + " of company " + DtCompany.Rows[0]["CompanyName"].ToString() + " has submitted the assessment successfully. <br/><br/> Thank You.<BR/><br/> Best Regards, <br/><br/> Let me talk to me Team.</font></body></html>";
                                mail.IsBodyHtml = true;

                                mail.Priority = System.Net.Mail.MailPriority.High;
                                SmtpClient client = new SmtpClient();
                                //client.Credentials = new System.Net.NetworkCredential("contact@letmetalk2.me", "contact123");
                                //client.Port = 587;
                                //client.Host = "hazel.arvixe.com";
                                client.EnableSsl = false;

                                client.Send(mail);


                                //message = "Employee added successfully...";
                                divalert.Attributes.Add("class", "alert alert-success");
                                lblMessage.Text = "Employee added successfully...";

                                //ask for another test
                                if (Session["BatchID"] != null && Session["BatchID"].ToString() != "")
                                {
                                    string batchID = Session["BatchID"].ToString();
                                    DataTable DtAssessmentSubmitted = objEmployeeAssessmentsHistoryDAL.EmployeeAssementsGet(EmployeeId, batchID);

                                    if (DtAssessmentSubmitted != null && DtAssessmentSubmitted.Rows.Count > 0)
                                    {
                                        if (DtAssessmentSubmitted.Select("AssessmentSubmitted=0").Length > 0)
                                        {
                                            Response.Redirect("SuggestionFrnd.aspx");
                                            // Response.Redirect("AssessmentMessage.aspx");
                                            //  ScriptManager.RegisterClientScriptBlock(this, GetType(), "MessageBox", "DisplayColorBox('AssessmentMessage.aspx');", true);
                                        }
                                        else
                                        {
                                            Response.Redirect("EmployeeFeedback.aspx", false);
                                        }
                                    }
                                }
                            }
                            else
                            {
                                //lblmessage.Text = "Can not submit test...";
                            }
                        }
                        else if (Convert.ToInt32(Session["TotalQuestionCount"].ToString()) == Count)
                        {
                            //  btnNext.Text = "Submit";
                        }
                        if (Session["Position"] != null)
                        {
                           // btnPrevious.Enabled = true;
                            //  btnPrevious1.Enabled = true;
                        }
                    }
                }
                else
                {
                    Response.Redirect("EmployeeLogin.aspx", false);
                }

            }
            catch (Exception ex1)
            {
                ex1.InnerException.ToString();
                //Response.Redirect("EmployeeLogin.aspx",false );
                log.Error(ex1.Message.ToString() + " " + ex1.StackTrace.ToString());
            }
        }

        public void FocusControlOnPageLoad(string ClientID, System.Web.UI.Page page)
        {
            ClientScriptManager clientScript = this.Page.ClientScript;
            clientScript.RegisterClientScriptBlock(this.GetType(), "DlistQuestions1",

                           @"<script> 

          function ScrollView()

          {
             var el = document.getElementById('" + ClientID + @"')
             if (el != null)
             {        
                el.scrollIntoView();
                el.focus();
             }
          }

          window.onload = ScrollView;

          </script>");

        }

        public int GetAlternateNumber(int min, int max)
        {
            int randomNumber;
            var generator = new RandomGenerator();
            randomNumber = generator.RandomNumber(min, max);

            for (int i = 0; i < arrayqueCntList.Count; i++)
            {
                if (!arrayqueCntList.Contains(randomNumber))
                {
                    Session["Position"] = randomNumber;
                    break;
                }
                else if (arrayqueCntList.Contains(randomNumber))
                {
                    randomNumber = randomGenerator.RandomNumber(1, maxNum);
                    i = 0;
                }
            }

            arrayqueCntList.Add(randomNumber);
            return randomNumber;
        }

    }
}