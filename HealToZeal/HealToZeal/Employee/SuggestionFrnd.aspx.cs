﻿using BE;
using DAL;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net.Mail;
using System.Net.Mime;
using System.Web;
using System.Web.Mail;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace HealToZeal.Employee
{
    public partial class SuggestionFrnd : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                if (Session["EmployeeId"] == null)
                {
                    Response.Redirect("EmployeeLogin.aspx", false);
                }
                Session["Heading"] = "";
            }
            
        }


        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            EmployeeDAL objEmployeeDAL = new EmployeeDAL();
            List<SuggestFriend> suggestionFrndList = new List<SuggestFriend>();
            string userEmail = string.Empty;
            if (Session["UserName"] != null || Session["Email"] != null)
            {
                if (Session["UserName"].ToString() != "")
                {
                    userEmail = Session["UserName"].ToString();
                   
                }
                else
                {
                    userEmail = Session["Email"].ToString();
                }
            }

            if (txtFirstName1.Text != "")
            {
                SuggestFriend suggestionFrnd = new SuggestFriend();
                suggestionFrnd.FirstName = txtFirstName1.Text.ToString();
                suggestionFrnd.LastName = txtLastName1.Text.ToString();
                suggestionFrnd.Email = txtEmail1.Text.ToString();
                suggestionFrnd.CreatedBy = ""; //Session["UserName"].ToString();
                suggestionFrndList.Add(suggestionFrnd);
            }
            if (txtFirstName2.Text != "")
            {
                SuggestFriend suggestionFrnd2 = new SuggestFriend();
                suggestionFrnd2.FirstName = txtFirstName2.Text.ToString();
                suggestionFrnd2.LastName = txtLastName2.Text.ToString();
                suggestionFrnd2.Email = txtEmail2.Text.ToString();
                suggestionFrnd2.CreatedBy = "";// Session["UserName"].ToString();
                suggestionFrndList.Add(suggestionFrnd2);
            }
            if (suggestionFrndList != null && suggestionFrndList.Count > 0)
            {
                int result = objEmployeeDAL.EmployeesSuggestFrndInsert(suggestionFrndList);
                foreach (SuggestFriend suggestFriend in suggestionFrndList)
                {
                    SendActivationEmail(suggestFriend);
                }
            }
            Response.Redirect("UserResultReport.aspx");
        }
        private void SendActivationEmail(SuggestFriend suggestFriend)
        {

            try
            {
                //string DefaultImagePath = HttpContext.Current.Server.MapPath(@"~\UIFolder\dist\img\logotemp.png");
                //byte[] imageArray = System.IO.File.ReadAllBytes(DefaultImagePath);
                //string base64ImageRepresentation = Convert.ToBase64String(imageArray);

                System.Net.Mail.MailMessage mail = new System.Net.Mail.MailMessage();
                // System.Web.Mail.MailMessage mail = new System.Web.Mail.MailMessage();
                var DecintellURl = ConfigurationManager.AppSettings.Get("Loginurl");
                // string verificationcode = Session["verificationcode"].ToString();
                mail.To.Add(suggestFriend.Email.ToString());
                mail.CC.Add("contact@letmetalk2.me");
                // mail.Bcc.Add("priyamvada.bavadekar@letmetalk2.me");
                // mail.From = "contact@letmetalk2.me";
                // mail.From = new MailAddress("contact@letmetalk2.me", "Let me talk to me", System.Text.Encoding.UTF8);
                mail.Subject = "Refer Friend";

                string getDt = DateTime.Now.Date.AddDays(7).ToString("MM/dd/yyyy");
                string body = string.Empty;
                
                body += "<table width =\"" + "100%" + "\" border=\"" + "0" + "\"  cellpadding=\"" + "0" + "\" cellspacing=\"" + "0" + "\" style=\"" + "border:2px solid #b3c8c8 !important" + "\" >";
                body += "<tr><td><table width =\"" + "100%" + "\" border =\"" + "0" + "\" cellspacing=\"" + "0" + "\" cellpadding = \"" + "0" + "\" >";
                body += "<tr> <td valign =\"" + "top" + "\" ><table width =\"" + "90%" + "\"  border =\"" + "0" + "\" align=\"" + "center" + "\"  cellpadding = \"" + "0" + "\" cellspacing =\"" + "0" + "\" >";
                body += "<tr> <td  style =\"" + "font-family:Verdana, Arial, Helvetica, sans-serif; font-size:12px; line-height:1.5; text-align:justify;" + "\"  > &nbsp;</ td >";
                body += "</tr><tr><td style =\"" + "font-family:Verdana, Arial, Helvetica, sans-serif; font-size:13px; line-height:1.7; text-align:justify;" + "\"  ><div style =\"" + "font-size:18px; color:#5b6766; font-weight:800;" + "\" > hi " + Convert.ToString(suggestFriend.FirstName) + ", </div >";
                body += "<p> i  just found something I would like share with you as I found it useful. </p>";
                body += "<p> let  me talk To Me reflects our state of mind with a easy simple tool and shows us our own reflection about how mindful we are. </p>";
                body += "<p> it  really helped to me pin down some pain points and got an opportunity to reflect on the same. It allows one to look at oneself, from a very different angle of wellness. And on top of it all, I could sense my mind better, for the first time, the area that otherwise was taboo. <br/><br/>";
                body += "Useful for organizations as well as individuals, IIT Bombay is already using this for wellness of their students.</ p >";
                body += "<p> I am sure everyone should go though at least once to check what it is! </p>";
                body += "<p> here is the link you may would like to visit</p>";
                body += "<a href = " + ConfigurationManager.AppSettings.Get("Loginurl") + " style=\"" + "background-color: #5b756c; padding: 8px 10px;";
                body += "text-align: center; text-decoration: none; display: inline-block; font-size: 15px; color: #fff; border-radius: 25px;\"" + ">letmetalk2.me</a>";
                body += "</td> </tr><tr><td  style=\"" + "font-family:Verdana, Arial, Helvetica, sans-serif; font-size:12px; line-height:1.5; text-align:justify;" + "\" > &nbsp;</ td >";
                body += "</tr></table></td> </tr> <tr>";
                body += "<td height=\"" + "140" + "\" bgcolor=\"" + "#b3c8c8" + "\" style=\"" + "font-family:Verdana, Arial, Helvetica, sans-serif; font-size:20px; color:#fff;" + "\"><table width=\"" + "90%" + "\" border=\"" + "0" + "\" align=\"" + "center" + "\" cellpadding=\"" + "0" + "\" cellspacing =\"" + "0" + "\">";
                body+="<tr><td width=\"" + "23%" + "\"><img src='" + ConfigurationManager.AppSettings.Get("ImagePath") + @"\UIFolder\dist\img\logotemp.png' width ='120' height='122' /></td>";
                body += "<td width=\"" + "2%" + "\"\"> &nbsp;</ td >";
                body += "<td width=\"" + "75%" + "\" align=\"" + "left" + "\" ><table width=\"" + "100%" + "\" border=\"" + "0" + "\" align=\"" + "left" + "\" cellpadding=\"" + "0" + "\" cellspacing=\"" + "0" + "\" >";
                body += "<tr><td style=\"" + "color:#5b756c" + "\" ><strong > let me talk team </ strong ></td >";
                body += "</tr><tr> <td height =\"" + "28" + "\" style =\"" + "font-size:14px; border-bottom:1px dotted #5b756c; padding-bottom:5px; color:#5b756c" + "\"\" > self test program</ td >";
                body += "</tr><tr><td style=\"" + "font-size:12px; padding-top:5px; color:#5b756c" + "\" > https://www.letmetalk2.me/ </ td >";
                body += "</tr><tr> <td height=\"" + "20" + "\" style =\"" + "font-size:12px; color:#5b756c" + "\" ><span style =\"" + "font-size:12px; padding-top:5px;" + "\" >©2020 let me talk.all rights reserved.</ span ></ td >";
                body += " </tr></table></td></tr> </table></td> </tr></table></td></tr></table>";

                mail.Body = body;
                mail.IsBodyHtml = true;

                //mail.Body = String.Format(@"<img src=""cid:{0}""  width='120'  height='122' />", att.ContentId);

                mail.Priority = System.Net.Mail.MailPriority.High;
                SmtpClient client = new SmtpClient();
                // client.Credentials = new System.Net.NetworkCredential("contact@letmetalk2.me", "contact123");
                //  client.Port = 587;
                // client.Host = "dallas137.arvixeshared.com";
                client.EnableSsl = false;
                client.Send(mail);
                //log.Info(" Send Mail Successfully.");
            }
            catch (Exception )
            {
                // Response.Write(@"<script language='javascript'>alert(" + ex.Message.ToString() + ");</script>");
                //lblMessage1.Text = ex.Message.ToString();
                //ClientScript.RegisterStartupScript(this.GetType(), "Pop", "openModal();", true);
            }

        }
    }
}