﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using DAL;
using System.Drawing;
using System.Web.Mail;
using Microsoft.Reporting.WebForms;
using System.Net.Mail;
using iTextSharp.text;
using iTextSharp.text.pdf;
using System.IO;

namespace HealToZeal.Employee
{
    public partial class UserResult : System.Web.UI.Page
    {
        DataTable dt = new DataTable();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                if (Session["EmployeeId"] != null)
                {

                    BindReport();
                    //ReportViewer1.ProcessingMode = ProcessingMode.Local;

                    //ReportViewer1.LocalReport.ReportPath = Server.MapPath("Reports/AllScoresByAssessmentForEmployee.rdlc");
                    //Document pdfDoc = new Document(PageSize.A4, 25, 10, 25, 10);
                    //string pdfName = "test";
                    //PdfWriter.GetInstance(pdfDoc, new FileStream(Server.MapPath("~/PDFsaved/" + pdfName + ".pdf"), FileMode.Create));
                    //PdfWriter pdfWriter = PdfWriter.GetInstance(pdfDoc, Response.OutputStream);
                    //pdfDoc.Open();
                    //byte[] file;
                    // file = System.IO.File.ReadAllBytes(Server.MapPath("~/Images/12933.jpg"));//ImagePath  
                    //iTextSharp.text.Image jpg = iTextSharp.text.Image.GetInstance(file);
                    //jpg.ScaleToFit(550F, 200F);//Set width and height in float  
                    // pdfDoc.Add(jpg);
                    //  pdfDoc.Add(image);

                    //Paragraph para = new Paragraph(BindReport);
                    //pdfDoc.Add(para);


                    //string text = "you are successfully created PDF file.";


                    //pdfWriter.CloseStream = false;
                    //pdfDoc.Close();
                    //Response.Buffer = true;
                    //Response.ContentType = "application/pdf";
                    //Response.AddHeader("content-disposition", "attachment;filename=Example.pdf");
                    //Response.Cache.SetCacheability(HttpCacheability.NoCache);
                    //Response.Write(pdfDoc);

                }
            }
        }

        private void BindReport()
        {
            try
            {
                EmployeeDAL objEmployeeDAL = new EmployeeDAL();

                DataTable DtEmployeeDetails = objEmployeeDAL.EmployeeAssessmentSubmittedGetByEmployeeId(Session["EmployeeId"].ToString());
                for (int i = 0; i < DtEmployeeDetails.Rows.Count; i++)
                {
                    if (i == 0)
                    {
                        RptrEmployee.DataSource = DtEmployeeDetails;
                        RptrEmployee.DataBind();

                        //DataTable Dt = objEmployeeDAL.EmployeeAssessmentClusterwiseReport(Convert.ToInt32(Session["EmployeeCompanyId"].ToString()));
                        //DataTable DtResult = new DataTable();//Dt;
                        //DtResult = Dt.Select(" Result<>'' or Suggestion<>''", "Result Desc").CopyToDataTable();
                        //GvResult.DataSource = DtResult;
                        //GvResult.DataBind();

                        //Session["AssessmentInstanceCompanyId"]
                        DataTable Dt = objEmployeeDAL.EmployeeResultGetForEmployee(Session["EmployeeId"].ToString(), Session["AssessmentInstanceId"].ToString(), Session["AssessmentInstanceCompanyId"].ToString());


                        DataTable DtCount = new DataTable();
                        int CountYellow = (int)Dt.Compute("Count(Color)", "Color='#f4fb73' and IsPositive='N'");
                        int CountRed = (int)Dt.Compute("Count(Color)", "Color='#e43c37'");
                        int CountGreen = (int)Dt.Compute("Count(Color)", "Color='#88e46d'");

                        // show button
                        if (CountRed > 0)
                        {
                            Ttle.Visible = false;
                            Ttle1.Visible = true;
                            Ttle3.Visible = true;
                            LblMessageTitle.Text = "Action items";
                            LblMessageTitle1.Text = "Action items";
                            lblMessage.Text = "Improve your lifestyle.We suggest a repeat test in 3 months.We strongly recommend an immediate meeting with a professional counselor.";
                            LblMessageUp.Text = "Improve your lifestyle.We suggest a repeat test in 3 months.We strongly recommend an immediate meeting with a professional counselor.";
                            divalert.Visible = true;
                            divalert1.Visible = true;
                            //commented on 7 Jan 16  BtnSetUpMeeting.Visible = true;
                            divSuggestion.Visible = true;
                            //commented on 7 jan 16  btnCounsel.Visible = true;
                            if (CountGreen > 0)
                            {
                                Ttle.Visible = true;
                            }
                        }
                        else if ((CountYellow > 0) && (CountYellow == 1))
                        {
                            Ttle.Visible = false;
                            Ttle1.Visible = true;
                            Ttle3.Visible = true;
                            //p1.InnerText = "Improve your lifestyle.";
                            //p2.InnerText = " We suggest a repeat test in 3 months.";
                            //p3.InnerText = "If you feel you need to talk to someone, set up a meeting with a professional counselor";
                            LblMessageTitle.Text = "Action items";
                            LblMessageTitle1.Text = "Action items";

                            lblMessage.Text = " Improve your lifestyle. We suggest a repeat test in 3 months. If you feel you need to talk to someone,";
                            LblMessageUp.Text = " Improve your lifestyle. We suggest a repeat test in 3 months. If you feel you need to talk to someone,";
                            divalert.Visible = true;
                            divalert1.Visible = true;
                            //commented on 7 Jan 16  BtnSetUpMeeting.Visible = true;
                            divSuggestion.Visible = true;
                            //commented on 7 jan 16 btnCounsel.Visible = true;
                            if (CountGreen > 0)
                            {
                                Ttle.Visible = true;
                            }
                        }
                        else if (CountYellow > 1)
                        {
                            Ttle.Visible = false;
                            Ttle1.Visible = true;
                            Ttle3.Visible = true;
                            LblMessageTitle.Text = "Action items";
                            LblMessageTitle1.Text = "Action items";

                            lblMessage.Text = "Improve your lifestyle. We suggest a repeat test in 3 months. We recommend meeting a professional counselor.";
                            LblMessageUp.Text = "Improve your lifestyle. We suggest a repeat test in 3 months. We recommend meeting a professional counselor.";
                            divalert.Visible = true;
                            divalert1.Visible = true;
                            //commented on 7 Jan 16   BtnSetUpMeeting.Visible = true;
                            divSuggestion.Visible = true;
                            //commented on 7 jan 16 btnCounsel.Visible = true;
                            if (CountGreen > 0)
                            {
                                Ttle.Visible = true;
                            }
                        }
                        else if (CountGreen > 0)
                        {
                            Ttle.Visible = true;
                            Ttle1.Visible = true;
                            Ttle3.Visible = true;
                            divalert.Visible = true;
                            divalert1.Visible = true;
                            GvSuggestion.Visible = true;
                            ReportViewer1.Visible = true;
                            //commented on 7 jan 16 btnCounsel.Visible = false;
                        }
                        else
                        {
                            LblMessageTitle.Text = "Congratulations!";
                            LblMessageTitle1.Text = "Congratulations!";

                            lblMessage.Text = "You seem to be well placed from psychologic health perspective. Keep doing your good work. We recommend you repeat the test after 6 months so as to get alerts if any.";
                            LblMessageUp.Text = "You seem to be well placed from psychologic health perspective. Keep doing your good work. We recommend you repeat the test after 6 months so as to get alerts if any.";

                            Ttle1.Visible = false;
                            // divalert.Visible = false;
                            //divalert1.Visible = false;

                            //commented on 7 jan 16  BtnSetUpMeeting.Visible = false;
                            Ttle3.Visible = false;
                            divSuggestion.Visible = true;
                            //commented on 7 jan 16  btnCounsel.Visible = false;
                        }



                        DataTable DtPositive = new DataTable();//Dt;
                        DataRow[] DrowPositive = Dt.Select("Color='#88e46d'", "");
                        if (DrowPositive.Length > 0)
                        {
                            DtPositive = Dt.Select("Color='#88e46d'", "").CopyToDataTable();
                            if (DtPositive != null && DtPositive.Rows.Count > 0)
                            {
                                GvPositive.DataSource = DtPositive;
                                GvPositive.DataBind();
                            }
                            else
                            {
                                GvPositive.DataSource = null;
                                GvPositive.DataBind();
                            }
                        }
                        else
                        {
                            GvPositive.DataSource = null;
                            GvPositive.DataBind();
                        }
                        DataRow[] DrowNegative = Dt.Select("Color='#e43c37' Or Color='#f4fb73'", "");
                        if (DrowNegative.Length > 0)
                        {
                            DtPositive = Dt.Select("(Color='#e43c37' Or Color='#f4fb73') and (Ispositive='N')", "").CopyToDataTable();
                            if (DtPositive != null && DtPositive.Rows.Count > 0)
                            {
                                GvResult.DataSource = DtPositive;
                                GvResult.DataBind();
                            }
                            else
                            {
                                GvResult.DataSource = null;
                                GvResult.DataBind();
                            }
                        }
                        else
                        {
                            GvResult.DataSource = null;
                            GvResult.DataBind();
                        }


                        //code added by meenakshi


                        DataTable DtSuggestions = objEmployeeDAL.SuggestionMasterGetRandom();
                        DataView view = new DataView(DtSuggestions);
                        DataTable distinctValues = view.ToTable(true, "Title");

                        if (distinctValues != null)
                        {
                            if (distinctValues.Rows.Count > 0)
                            {
                                //  Ttle3.Visible = true;
                                GvSuggestion.DataSource = distinctValues; //DtSuggestions;
                                GvSuggestion.DataBind();
                            }
                            else
                            {
                                //Ttle3.Visible = false;
                            }
                        }
                        else
                        {
                            // Ttle3.Visible = false;
                        }


                        //DataTable dt = objEmployeeDAL.EmployeeResultGet(Session["EmployeeId"].ToString());
                        //GvResult.DataSource = dt;
                        //GvResult.DataBind();                

                        for (int a = 0; a < GvSuggestion.Rows.Count; a++)
                        {
                            Label lblSuggestionTitle = GvSuggestion.Rows[a].FindControl("lblSuggestionTitle") as Label;
                            if (lblSuggestionTitle != null)
                            {
                                DataTable DtSuggestionText = DtSuggestions.Select("Title='" + lblSuggestionTitle.Text + "'").CopyToDataTable();
                                GridView Gdv = (GridView)GvSuggestion.Rows[a].FindControl("GdvSuggestionText");
                                Gdv.DataSource = DtSuggestionText;
                                Gdv.DataBind();
                            }
                        }
                    }


                }

                addColumns();
                DataTable DtClusterPercentage = new DataTable();
                EmployeeAssessmentsHistoryDAL objEmployeeDAL1 = new EmployeeAssessmentsHistoryDAL();
                DtClusterPercentage = objEmployeeDAL1.EmployeeHistory(Session["EmployeeId"].ToString());

                for (int i = 0; i < DtClusterPercentage.Rows.Count; i++)
                {

                    dt.Rows.Add(DtClusterPercentage.Rows[i]["EmployeeCompanyId"],
                        DtClusterPercentage.Rows[i]["AssessmentName"],
                   DtClusterPercentage.Rows[i]["AssessmentId_FK"],
                   DtClusterPercentage.Rows[i]["num"],
                   DtClusterPercentage.Rows[i]["ClusterName"],
                   DtClusterPercentage.Rows[i]["ClusterScore"],
                   DtClusterPercentage.Rows[i]["CreatedDate"], DtClusterPercentage.Rows[i]["EmployeeName"],
                   DtClusterPercentage.Rows[i]["ClusterPercentage"],
                   DtClusterPercentage.Rows[i]["Color"],
                   DtClusterPercentage.Rows[i]["IsPositive"]);

                    ReportViewer1.Visible = true;

                    ReportViewer1.LocalReport.DataSources.Clear();

                    ReportViewer1.LocalReport.DataSources.Add(new Microsoft.Reporting.WebForms.ReportDataSource("DataSet1", dt));

                }
            }


            catch (Exception )
            {
                //    //ex.InnerException.ToString();
            }



        }
        public void addColumns()
        {
            dt.Columns.Add("EmployeeCompanyId", typeof(string));
            dt.Columns.Add("AssessmentName", typeof(string));
            dt.Columns.Add("AssessmentId_FK", typeof(string));
            dt.Columns.Add("num", typeof(int));
            dt.Columns.Add("ClusterName", typeof(string));
            dt.Columns.Add("ClusterScore", typeof(string));
            dt.Columns.Add("CreatedDate", typeof(DateTime));
            dt.Columns.Add("EmployeeName", typeof(string));
            dt.Columns.Add("ClusterPercentage", typeof(double));
            dt.Columns.Add("Color", typeof(string));
            dt.Columns.Add("IsPositive", typeof(char));


        }
        protected void ddlAssessment_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                ReportViewer1.LocalReport.Refresh();
                BindReport();
            }
            catch
            {

            }
        }

        protected void btnCounsel_Click(object sender, EventArgs e)
        {
            try
            {
                DivSetUpMeeting.Focus();
                DivSetUpMeeting.Visible = true;
                DivEmail.Visible = true;

                DivMailBody.Visible = false;
                DivTitle.Visible = false;

                EmployeeDAL objEmployeeDAL = new EmployeeDAL();
                DataTable DtEmployeeDetails = objEmployeeDAL.EmployeeAssessmentSubmittedGetByEmployeeId(Session["EmployeeId"].ToString());
                uemail.Value = DtEmployeeDetails.Rows[0]["EmailId"].ToString();
                uemail1.Value = DtEmployeeDetails.Rows[0]["EmailId"].ToString();
            }
            catch (Exception )
            {

            }
        }

        protected void BtnSendEmail_Click(object sender, EventArgs e)
        {
            try
            {
                System.Net.Mail.MailMessage mail = new System.Net.Mail.MailMessage();

                mail.To.Add("priyamvada.bavadekar@letmetalk2.me");
                mail.Bcc.Add("priyamvada.bavadekar@letmetalk2.me");
                // mail.From = "contact@letmetalk2.me";
                if (uemail.Value != "")
                {
                    // mail.From = uemail.Value;

                }
                else if (uemail1.Value != "")
                {
                    // mail.From = uemail1.Value;
                }
                // mail.From = new MailAddress("contact@letmetalk2.me", "Let me talk to me", System.Text.Encoding.UTF8);
                mail.Subject = "User want to contact for appointment.";
                string getDt = DateTime.Now.Date.AddDays(7).ToString("MM/dd/yyyy");
                if (message.Value != "")
                {
                    mail.Body = message.Value;
                }
                else if (message1.Value != "")
                {
                    mail.Body = message1.Value;
                }
                mail.IsBodyHtml = true;
                mail.Priority = System.Net.Mail.MailPriority.High;
                SmtpClient client = new SmtpClient();
                //client.Credentials = new System.Net.NetworkCredential("contact@letmetalk2.me", "contact123");
                //client.Port = 587;
                //client.Host = "hazel.arvixe.com";
                client.EnableSsl = false;

                client.Send(mail);

                //    System.Web.Mail.MailMessage mail = new System.Web.Mail.MailMessage();

                //    if(uemail.Value !="")
                //    {
                //        mail.From = uemail.Value;
                //    }
                //    else if (uemail1.Value != "")
                //    {
                //        mail.From = uemail1.Value;
                //    }

                //    mail.To = "prajakta.mulay@chetanasystems.com";
                //    mail.Subject = "User want to contact for appointment.";
                //    if (message.Value != "")
                //    {
                //        mail.Body = message.Value;
                //    }
                //    else if (message1.Value != "")
                //    {
                //        mail.Body = message1.Value;
                //    }


                //    mail.BodyFormat = MailFormat.Html;
                //    mail.Fields.Add("http://schemas.microsoft.com/cdo/configuration/smtpauthenticate", "1");    //basic authentication
                //    mail.Fields.Add("http://schemas.microsoft.com/cdo/configuration/sendusername", "HealToZeal@chetanatspl.com"); //set your username here
                //    mail.Fields.Add("http://schemas.microsoft.com/cdo/configuration/sendpassword", "HealToZeal");    //set your password here
                //    System.Web.Mail.SmtpMail.SmtpServer = "holly.arvixe.com";  //your real server goes here                        
                //    System.Web.Mail.SmtpMail.Send(mail);
                //    DivEmail.Visible = false;
                //    DivSetUpMeeting.Visible = false;
                //    DivMailBody.Visible = false;
                //    DivTitle.Visible = false;
                //    ScriptManager.RegisterClientScriptBlock(this.Page, GetType(), "openColorBox", "<script> alert('We have received your meeting request. We will get back to you very soon'); </script>;", true);
                //
            }
            catch (Exception )
            {

            }
        }

        protected void BtnSetUpMeeting_Click(object sender, EventArgs e)
        {
            try
            {
                DivTitle.Focus();
                DivMailBody.Visible = true;
                DivTitle.Visible = true;

                DivEmail.Visible = false;
                DivSetUpMeeting.Visible = false;

                EmployeeDAL objEmployeeDAL = new EmployeeDAL();
                DataTable DtEmployeeDetails = objEmployeeDAL.EmployeeAssessmentSubmittedGetByEmployeeId(Session["EmployeeId"].ToString());
                uemail.Value = DtEmployeeDetails.Rows[0]["EmailId"].ToString();

            }
            catch (Exception )
            {

            }
        }

        //protected void btnSubmit_Click(object sender, EventArgs e)
        //{ 
        //    EmployeeDAL objEmployeeDAL = new EmployeeDAL();

        //    DataTable DtEmployeeDetails = objEmployeeDAL.EmployeeAssessmentSubmittedGetByEmployeeId(Convert.ToInt32(txtEmployeeId.Text));
        //    RptrEmployee.DataSource= DtEmployeeDetails;
        //    RptrEmployee.DataBind();

        //  DataTable Dt=  objEmployeeDAL.EmployeeAssessmentClusterwiseReport(Convert.ToInt32(txtEmployeeId.Text));

        //  //GvStandard.DataSource = Dt;
        //  //GvStandard.DataBind();

        //  //GvEmployeeResult.DataSource = Dt;
        //  //GvEmployeeResult.DataBind();

        //  DataTable DtResult = new DataTable();//Dt;
        //  DtResult = Dt.Select(" Result<>'' or Suggestion<>''","Result Desc").CopyToDataTable();
        //  GvResult.DataSource = DtResult;
        //  GvResult.DataBind();

        //  //GvResult.DataSource = Dt;
        //  //GvResult.DataBind();

        //  //DataTable DtRemedyObservation = new DataTable();//Dt;
        //  //DtRemedyObservation = Dt.Select(" Suggestion<>''").CopyToDataTable();
        //  //GvRemedyObservation.DataSource = DtRemedyObservation;
        //  //GvRemedyObservation.DataBind();



        ////  DataTable DtStndard = new DataTable();//Dt;
        //////  DtStndard = Dt.Select("( ClusterName='Stress' and AssessmentName='Standard stress Assessment') OR( AssessmentName='Standard Mood Assessment' AND  ClusterName='Mood')").CopyToDataTable();
        ////  DtStndard = Dt.Select("ClusterName<>'Anxiety' AND ClusterName<>'Mood' AND ClusterName<>'Stress'").CopyToDataTable();
        ////  DataTable DtSubStndard = new DataTable();
        ////  DtSubStndard = Dt.Select("( ClusterName='Stress' and AssessmentName='Standard stress Assessment') OR( AssessmentName='Standard Mood Assessment' AND  ClusterName='Mood')OR( AssessmentName='Standard Anxiety Assessment' AND  ClusterName='Anxiety')").CopyToDataTable();
        ////   DtSubStndard.Merge(DtStndard);

        ////  GvStandard.DataSource = DtSubStndard;
        ////  GvStandard.DataBind();

        ////  DataTable DtRemedyObservation = objEmployeeDAL.AssessmentFinalResult(Convert.ToInt32(txtEmployeeId.Text));
        ////  DataTable DtRemedy = new DataTable();//Dt;
        ////  //  DtStndard = Dt.Select("( ClusterName='Stress' and AssessmentName='Standard stress Assessment') OR( AssessmentName='Standard Mood Assessment' AND  ClusterName='Mood')").CopyToDataTable();
        ////  DtRemedy = DtRemedyObservation.Select("ClusterName<>'Anxiety' AND ClusterName<>'Mood' AND ClusterName<>'Stress'").CopyToDataTable();
        ////  DataTable DtRemedyStandard = new DataTable();
        ////  DtRemedyStandard = DtRemedyObservation.Select("( ClusterName='Stress' and AssessmentName='Standard stress Assessment') OR( AssessmentName='Standard Mood Assessment' AND  ClusterName='Mood')OR( AssessmentName='Standard Anxiety Assessment' AND  ClusterName='Anxiety')").CopyToDataTable();
        ////  object ObjAvg = DtRemedyStandard.Compute("Avg(Percentage)", "");
        ////  DtRemedyStandard.Merge(DtRemedy);
        ////  GvRemedyObservation.DataSource = DtRemedyStandard;
        ////  GvRemedyObservation.DataBind();
        ////  DataTable Flag = objEmployeeDAL.ResultTextMasterGet(Convert.ToInt32(ObjAvg.ToString()));

        ////  GvResult.DataSource = Flag;
        ////  GvResult.DataBind();

        //  DataTable DtSuggestions = objEmployeeDAL.SuggestionMasterGetRandom();
        //  GvSuggestion.DataSource = DtSuggestions;
        //  GvSuggestion.DataBind();



        //}
    }
}