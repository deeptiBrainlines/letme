﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master/LetMeTalk.Master" AutoEventWireup="true" CodeBehind="EmployeeLogin.aspx.cs" Inherits="HealToZeal.Employee.EmployeeLogin2" Async="true" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <script type="text/javascript">
        function openModalLogin() {
            $('[id*=ModalLogin]').modal('show');
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div style="clear: both;"></div>
    <div>
        <section class="content">
            <div class="row">
                <div class="col-md-11 col-centered">
                    <div class="nav-tabs-custom">
                        <ul class="nav nav-tabs">
                            <li class="active"><a href="EmployeeLogin.aspx">sign in</a></li>
                            <%--<li><a href="EmpRegistrationForm.aspx?InstanceCompanyRelationId=141C5CFB-4666-4576-B5C9-FADA9522DD2A&CouponId=1">sign up</a></li>--%>
                            <li><a href="EmpRegistrationForm.aspx?InstanceCompanyRelationId=3FB35DBD-00B3-4704-BA2C-BC7DC3F244AF&CouponId=1">sign up</a></li>
                        </ul>
                        <div class="tab-content">
                            <div class="bdr3">
                                <div class="col-md-10 col-centered" style="padding: 30px 0 0 0;">
                                    <div class="col-md-4 col-sm-4">
                                        <div>
                                            <%--<a href="#" class="social-btn"> <img src="../UiFolder/dist/img/social1.png" class="socialico"> Continue with Google </a>--%>
                                            <asp:ImageButton ID="ImgBtnGmail" runat="server" CssClass="social-btnw img-responsive social-btns-bdr" ToolTip="Login With Gmail" ImageUrl="../UIFolder/dist/img/bt-google.png" CausesValidation="true" OnClick="ImgBtnGmail_Click" formnovalidate />


                                        </div>
                                        <div class="clearfix"></div>
                                        <div>
                                            <asp:ImageButton ID="ImageButton1" runat="server" CssClass=" social-btnw img-responsive mt20 social-btns-bdr" ToolTip="Login With Facebook" ImageUrl="../UIFolder/dist/img/bt-facebook.png" CausesValidation="true" />

                                            <%--<a href="#" class="social-btn"> <img src="../UiFolder/dist/img/social2.png" class="socialico"> Continue with Facebook </a> --%>
                                        </div>
                                        <div class="clearfix"></div>
                                        <div>
                                            <%--<a href="#" class="social-btn"> <img src="../UiFolder/dist/img/social3.png" class="socialico"> Continue with Linkedin </a>--%>
                                            <asp:ImageButton ID="imgBtnLinkedin" CssClass=" social-btnw img-responsive mt20 social-btns-bdr" runat="server" ImageUrl="../UIFolder/dist/img/bt-linkdin.png" CausesValidation="true" ToolTip="Continue with Linkedin" />

                                            <center>
                      <h1 class="mview mt0">or</h1>
                    </center>
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-sm-3">
                                        <center>
                    <img src="../UiFolder/dist/img/or.png" class="dview">
                  </center>
                                    </div>
                                    <div class="col-md-5 col-sm-5">
                                        <div class="form-group">
                                            <%--<input name="" type="text" class="form-control" placeholder="username or email">--%>
                                            <input type="text" id="UserName" runat="server" name="UserName" class="form-control" placeholder="username or email" required />
                                        </div>
                                        <div class="form-group">
                                            <%--<input name="" type="password" class="form-control" placeholder="password">--%>
                                            <input type="password" id="Password" runat="server" name="Password" class="form-control" placeholder="Password" required />
                                        </div>
                                        <div class="form-group">
                                            <div class="form-check">
                                                <input type="checkbox" class="form-check-input" id="exampleCheck1">
                                                <label class="form-check-label" for="exampleCheck1" style="font-size: 20px; font-weight: 500;">remember me</label>
                                                <%-- <a href="accept-continue.html" class="loginbtn pull-right">sign in</a>--%>
                                                <asp:Button ID="btnLogin" runat="server" Text="sign in" CssClass="loginbtn pull-right" OnClick="btnLogin_Click" />
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label id="message" runat="server"></label>
                                        </div>
                                        <%--<div class="form-group">
                    <h4><a href="#" class="reset">reset password</a></h4>
                  </div>--%>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
    <div id="ModalLogin" class="modal fade" role="dialog">
        <div class="modal-dialog modal-md">

            <!-- Modal content-->
            <div class="modal-content">

                <div class="modal-body">
                    <div class="col-md-12 mt30">
                        <h4 class="modal-title">message</h4>
                        <p class="w400">
                            <asp:Label ID="lblMessage1" runat="server"></asp:Label>
                        </p>
                    </div>
                </div>
                <div class="modal-footer">
                    <a href="">
                        <img src="../UIFolder/dist/img/close.png" data-dismiss="modal" class="popicon"></a>
                </div>
            </div>

        </div>
    </div>
</asp:Content>
