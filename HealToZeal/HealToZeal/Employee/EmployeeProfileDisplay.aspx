﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master/LetMeTalkWithMenu.Master" AutoEventWireup="true" CodeBehind="EmployeeProfileDisplay.aspx.cs" Inherits="HealToZeal.Employee.EmployeeProfileDisplay" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style>
        .validationerror {
            font-size: 11px !important;
            text-align: left !important;
            margin-top: 3px !important;
            color: #FF0000 !important;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="row">
        <div class="col-md-11 col-centered">
            <div class="boardp bdr3 mb20">
                <div class="col-md-12 pad0">

                   <%-- <h2 class="heading pull-left mt0">profile edit</h2>--%>
                    <h4 class="pull-right mt8"><span class="w600">employee id:</span> <span class="w400">
                        <asp:Label ID="lblEmpId" runat="server"></asp:Label></span></h4>


                </div>
                <div style="clear: both;"></div>
                <%--<div class="icon-m10">
                    <a href="profile-edit.html">
                        <img src="../UIFolder/dist/img/edit.png" class="pull-left img-responsive mr10">
                        <h3 class="mt0 txtwhite w400">edit profile</h3>
                    </a>
                </div>--%>

                <div>
                    <div class="row">
                        <div id="style-3" style="margin-top: 0 important;">
                            <%-- <div class="col-md-5 col-sm-5">

                                <div class="table-responsive">
                                    <table width="100%" cellspacing="0" cellpadding="0">
                                        <tr>
                                            <td>
                                                <h3 class="mt0"><span class="w600">name:</span> <span class="w400">
                                                    <asp:Label ID="lblFirstLastName" runat="server"></asp:Label>
                                                </span></h3>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <h3 class="mt0"><span class="w600">employee id:</span> <span class="w400">
                                                    <asp:Label ID="lblEmpId" runat="server"></asp:Label></span></h3>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <h3 class="mt0"><span class="w600">deparment:</span> <span class="w400">
                                                    <asp:Label ID="lblDepartment" runat="server"></asp:Label></span></h3>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <h3 class="mt0"><span class="w600">carde:</span> <span class="w400">
                                                    <asp:Label ID="lblCadre" runat="server"></asp:Label></span></h3>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <h3 class="mt0"><span class="w600">email id:</span> <span class="w400">
                                                    <asp:Label ID="lblEmailId" runat="server"></asp:Label></span></h3>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>&nbsp;</td>
                                        </tr>

                                        <tr>
                                            <td>
                                                <h3 class="mt0"><span class="w600">phone no:</span> <span class="w400">
                                                    <asp:Label ID="lblContactno" runat="server"></asp:Label></span></h3>
                                            </td>
                                        </tr>


                                    </table>

                                </div>
                            </div>--%>
                        </div>
                        <div class="col-md-12 col-sm-12">

                            <div class="row">

                                <div class="col-md-4">
                                    <div class="form-group">
                                        <h4 class="fwnormal mb">first name</h4>
                                        <asp:TextBox ID="txtFirstName" runat="server" CssClass="form-control" placeholder="First name" ValidationGroup="0"></asp:TextBox>

                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" Display="Dynamic" ErrorMessage="Enter first name" ControlToValidate="txtFirstName" ForeColor="Red" ValidationGroup="0">*</asp:RequiredFieldValidator>

                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <h4 class="fwnormal mb">last name</h4>
                                        <asp:TextBox ID="txtLastName" runat="server" CssClass="form-control" placeholder="Last name"
                                            ValidationGroup="0"></asp:TextBox>

                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="Enter last name" Display="Dynamic" ControlToValidate="txtLastName" ForeColor="Red" ValidationGroup="0">*</asp:RequiredFieldValidator>

                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <h4 class="fwnormal mb">user name</h4>
                                        <asp:TextBox ID="txtUserName" runat="server" CssClass="form-control" placeholder="User name"
                                            ValidationGroup="0"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ErrorMessage="Enter username" ControlToValidate="txtUserName" Display="Dynamic" ForeColor="Red">*</asp:RequiredFieldValidator>

                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <h4 class="fwnormal mb">email id</h4>
                                        <asp:TextBox ID="txtEmailId" runat="server" CssClass="form-control" placeholder="EmailId"
                                            ValidationGroup="0"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ErrorMessage="Enter email id" Display="Dynamic" ControlToValidate="txtEmailId" ForeColor="Red" ValidationGroup="0">*</asp:RequiredFieldValidator>
                                        <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ErrorMessage="Enter proper email id" Display="Dynamic" ControlToValidate="txtEmailId" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" ForeColor="Red" ValidationGroup="0">*</asp:RegularExpressionValidator>

                                    </div>
                                </div>

                                <div class="col-md-4">
                                    <div class="form-group">
                                        <h4 class="fwnormal mb">date of birth </h4>
                                        <div class="input-group date" id="datetimepicker1">
                                            <asp:TextBox ID="txtBirthDate" runat="server" CssClass="form-control" placeholder="BirthDate" ValidationGroup="0" onblur="ValidateDOB();"></asp:TextBox>
                                            <span class="input-group-addon textimg"><i class="fa fa-calendar" aria-hidden="true"></i></span>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ErrorMessage="*" ControlToValidate="txtBirthDate" Display="Dynamic" ForeColor="Red" ValidationGroup="0"></asp:RequiredFieldValidator>
                                           <%-- <asp:CompareValidator ID="CompareValidator1" runat="server"
                                                ControlToValidate="txtBirthDate" Display="Dynamic" ErrorMessage="Invalid Date"
                                                Operator="DataTypeCheck" Type="Date">
                                            </asp:CompareValidator>--%>
                                            <%--<asp:validatorcalloutextender id="CompareValidator1_ValidatorCalloutExtender"
                                                runat="server" enabled="True" targetcontrolid="CompareValidator1">
                            </asp:validatorcalloutextender>--%>
                                        </div>
                                        <sup style="line-height: -10px !important; top: 0em;">you can directly enter DOB in DD/MM/YYYY format</sup><br />
                                        <span id="lblError" class="validationerror"></span>
                                    </div>
                                </div>


                                <div class="col-md-4">
                                    <div class="form-group">
                                        <h4 class="fwnormal mb">gender</h4>
                                        <table width="150">
                                            <tbody>
                                                <tr>
                                                    <td>
                                                        <div class="radiobox">
                                                            <asp:RadioButtonList ID="rblGender" runat="server" RepeatDirection="Horizontal" CssClass="form-control1" ValidationGroup="0">
                                                                 
                                                                <asp:ListItem Value="M">&nbsp Male &nbsp</asp:ListItem>
                                                                
                                                                <asp:ListItem Value="F">&nbsp Female</asp:ListItem>
                                                            </asp:RadioButtonList>
                                                        </div>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <div style="clear: both;"></div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <h4 class="fwnormal mb">education</h4>
                                        <asp:DropDownList ID="ddlEducation" runat="server" CssClass="form-control"
                                            ValidationGroup="0">
                                            <asp:ListItem>--Select education--</asp:ListItem>
                                            <asp:ListItem>PostGraduate</asp:ListItem>
                                            <asp:ListItem>Graduate</asp:ListItem>
                                            <asp:ListItem>Doctorate</asp:ListItem>
                                        </asp:DropDownList>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator7" runat="server" ErrorMessage="Select education" ForeColor="Red" InitialValue="--Select education--" ValidationGroup="0" ControlToValidate="ddlEducation">*</asp:RequiredFieldValidator>

                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <h4 class="fwnormal mb">experience</h4>
                                        <asp:DropDownList ID="ddlExperience" runat="server" CssClass="form-control"
                                            ValidationGroup="0">
                                            <asp:ListItem>--Select experience--</asp:ListItem>
                                            <asp:ListItem>0-3 years</asp:ListItem>
                                            <asp:ListItem>3-7years</asp:ListItem>
                                            <asp:ListItem>7-10years</asp:ListItem>
                                            <asp:ListItem>10-15years</asp:ListItem>
                                            <asp:ListItem>15 and above</asp:ListItem>
                                        </asp:DropDownList>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator8" runat="server" ErrorMessage="Select experience" ControlToValidate="ddlExperience" ForeColor="Red" InitialValue="--Select experience--" ValidationGroup="0">*</asp:RequiredFieldValidator>

                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <h4 class="fwnormal mb">occupation</h4>
                                        <asp:TextBox ID="txtOccupation" runat="server" CssClass="form-control" placeholder="Occupation" ValidationGroup="0"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator9" runat="server" ErrorMessage="*" ControlToValidate="txtOccupation" Display="Dynamic" ForeColor="Red" ValidationGroup="0"></asp:RequiredFieldValidator>

                                    </div>
                                </div>
                                <div style="clear: both;"></div>




                                <div class="col-md-4">
                                    <div class="form-group">
                                        <h4 class="fwnormal mb">position</h4>
                                        <asp:DropDownList ID="ddlPosition" runat="server" CssClass="form-control" ValidationGroup="0">
                                            <asp:ListItem>--Select position--</asp:ListItem>
                                            <asp:ListItem>Junior level</asp:ListItem>
                                            <asp:ListItem>middle management</asp:ListItem>
                                            <asp:ListItem>Senior management</asp:ListItem>
                                        </asp:DropDownList>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator11" runat="server" ErrorMessage="Select position" InitialValue="--Select position--" Display="Dynamic" ControlToValidate="ddlPosition" ForeColor="Red" ValidationGroup="0">*</asp:RequiredFieldValidator>

                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <h4 class="fwnormal mb">country</h4>
                                        <asp:DropDownList ID="ddlCountry" runat="server"
                                            OnSelectedIndexChanged="ddlCountry_SelectedIndexChanged"
                                            AutoPostBack="True" CssClass="form-control" ValidationGroup="0">
                                        </asp:DropDownList>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ErrorMessage="*" InitialValue="--Select country--" Display="Dynamic" ControlToValidate="ddlCountry" ForeColor="Red" ValidationGroup="0"></asp:RequiredFieldValidator>

                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <h4 class="fwnormal mb">city</h4>
                                        <asp:DropDownList ID="ddlCity" runat="server" CssClass="form-control" ValidationGroup="0"></asp:DropDownList>
                                    </div>
                                </div>
                                <div class="col-md-8">
                                    <div class="form-group">
                                        <h4 class="fwnormal mb">address</h4>
                                        <asp:TextBox ID="txtAddress" runat="server" CssClass="form-control" placeholder="Address" ValidationGroup="0"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <h4 class="fwnormal mb">zipcode</h4>
                                        <asp:TextBox ID="txtZipcode" runat="server" CssClass="form-control" placeholder="Zip code"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <h4 class="fwnormal mb">contact no</h4>
                                        <asp:TextBox ID="txtContactNo" runat="server" CssClass="form-control" placeholder="Contact no" ValidationGroup="0"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <h4 class="fwnormal mb">mobile no</h4>
                                        <asp:TextBox ID="txtMobileNo" runat="server" CssClass="form-control" placeholder="Mobile no"
                                            ValidationGroup="0"></asp:TextBox>
                                    </div>
                                </div>

                                <div style="clear: both;"></div>
                                <div style="display: table;" class="col-centered ">

                                    <asp:Button ID="btnEditProfile" CausesValidation="true" runat="server" Text="Submit" CssClass="loginbtn pull-right"  OnClick="btnEditProfile_Click" ValidationGroup="0" />

                                    <asp:Button ID="btnCancel" runat="server" CssClass="loginbtn pull-right mr6" Text="Cancel" OnClick="btnCancel_Click" />
                                </div>


                            </div>


                        </div>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="clearfix"></div>
            </div>
            <div class="clearfix"></div>
        </div>
        <div class="clearfix"></div>
    </div>
    <div class="clearfix"></div>
    <script type="text/javascript">

        function ValidateDOB() {
            var dateString = $("input[id$='txtBirthDate']").val();
            var lblError = $("#lblError");
            var parts = dateString.split("/");
            var dtDOB = new Date(parts[1] + "/" + parts[0] + "/" + parts[2]);
            var dtCurrent = new Date();
            lblError.html("Eligibility 18 years ONLY.")
            if (dtCurrent.getFullYear() - dtDOB.getFullYear() < 18) {
                return false;
            }

            if (dtCurrent.getFullYear() - dtDOB.getFullYear() == 18) {

                //CD: 11/06/2018 and DB: 15/07/2000. Will turned 18 on 15/07/2018.
                if (dtCurrent.getMonth() < dtDOB.getMonth()) {
                    return false;
                }
                if (dtCurrent.getMonth() == dtDOB.getMonth()) {
                    //CD: 11/06/2018 and DB: 15/06/2000. Will turned 18 on 15/06/2018.
                    if (dtCurrent.getDate() < dtDOB.getDate()) {
                        return false;
                    }
                }
            }
            lblError.html("");
            return true;
        }
    </script>
</asp:Content>
