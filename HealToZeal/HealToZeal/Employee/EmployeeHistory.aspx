﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Employee/Employee.Master" AutoEventWireup="true" CodeBehind="EmployeeHistory.aspx.cs" Inherits="HealToZeal.Employee.EmployeeHistory" %>

<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=11.0.0.0, Culture=neutral, PublicKeyToken=89845dcd8080cc91" Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="EmployeeColumn" runat="server">
    <div class="container">
        <h4 class="hs_heading" id="hs_appointment_form_link">
            <asp:Label ID="lblName" runat="server" Text=""></asp:Label>My mindfulness trend
        </h4>
         <%--<h4 class="hs_heading" id="hs_appointment_form_link"> First Report</h4>--%>
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12">
              
                <rsweb:ReportViewer ID="ReportViewer1" runat="server" Font-Names="Verdana" Font-Size="8pt" Height="500px" WaitMessageFont-Names="Verdana" WaitMessageFont-Size="14pt" Width="1000px" ShowBackButton="False" ShowCredentialPrompts="False" ShowDocumentMapButton="False" ShowExportControls="False" ShowFindControls="False" ShowPageNavigationControls="False" ShowParameterPrompts="False" ShowPrintButton="False" ShowPromptAreaButton="False" ShowRefreshButton="False" ShowToolBar="False" ShowWaitControlCancelLink="False" ShowZoomControl="False" AsyncRendering="False">
                    <LocalReport ReportPath="Reports\EmployeeHistory.rdlc">
                        <DataSources>
                            <rsweb:ReportDataSource DataSourceId="ObjectDataSource1" Name="DataSet1" />
                        </DataSources>
                    </LocalReport>
                </rsweb:ReportViewer>
                <asp:ObjectDataSource ID="ObjectDataSource1" runat="server" OldValuesParameterFormatString="original_{0}" SelectMethod="GetData" TypeName="HealToZeal.AllClustersTableAdapters.Sp_EmployeeHistoryTableAdapter">
                    <SelectParameters>
                        <asp:SessionParameter DbType="Guid" Name="EmployeeId" SessionField="EmployeeId" />
                    </SelectParameters>
                </asp:ObjectDataSource>
            </div>
             <%--<div class="col-lg-12 col-md-12 col-sm-12">
                  <h4 class="hs_heading" id="hs_appointment_form_link"> Second Report</h4>
                <rsweb:ReportViewer ID="ReportViewer2" runat="server" Font-Names="Verdana" Font-Size="8pt" Height="500px" WaitMessageFont-Names="Verdana" WaitMessageFont-Size="14pt" Width="1000px" ShowBackButton="False" ShowCredentialPrompts="False" ShowDocumentMapButton="False" ShowExportControls="False" ShowFindControls="False" ShowPageNavigationControls="False" ShowParameterPrompts="False" ShowPrintButton="False" ShowPromptAreaButton="False" ShowRefreshButton="False" ShowToolBar="False" ShowWaitControlCancelLink="False" ShowZoomControl="False" AsyncRendering="False">
                    <LocalReport ReportPath="Reports\EmployeeHistory.rdlc">
                        <DataSources>
                            <rsweb:ReportDataSource DataSourceId="ObjectDataSource1" Name="DataSet1" />
                        </DataSources>
                    </LocalReport>
                </rsweb:ReportViewer>
                <asp:ObjectDataSource ID="ObjectDataSource2" runat="server" OldValuesParameterFormatString="original_{1}" SelectMethod="GetData" TypeName="HealToZeal.AllClustersTableAdapters.Sp_EmployeeHistoryTableAdapter">
                    <SelectParameters>
                        <asp:SessionParameter DbType="Guid" Name="EmployeeId" SessionField="EmployeeId" />
                    </SelectParameters>
                </asp:ObjectDataSource>
            </div>--%>
        </div>
    </div>
</asp:Content>
