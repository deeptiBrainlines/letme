﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DAL;
using System.Data;
using System.Net;
using System.Text;
using System.Configuration;
using System.IO;
using Newtonsoft.Json;
using BE;
using System.Net.Http;
using static HealToZeal.Employee.EmployeeLogin2;

namespace HealToZeal.Employee
{
    public partial class EmployeeAssessments1 : System.Web.UI.Page
    {
        log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        protected void Page_Load(object sender, EventArgs e)
        {
            Session["Heading"] = "";
            if (!Page.IsPostBack)
            {
                if (Session["EmployeeId"] != null)
                {
                    BindSubmittedAssessments();

                }
                //if ((Session.Contents.Count > 0) && (Session["SignWith"] != null) && (Session["SignWith"].ToString() == "google"))
                //{
                //  string  googleplus_redirect_url = ConfigurationManager.AppSettings.Get("googleplus_redirect_url");
                //    string googleplus_client_id = ConfigurationManager.AppSettings.Get("googleplus_client_id");
                //    string googleplus_client_secret = ConfigurationManager.AppSettings.Get("googleplus_client_secret");
                //    string de = Encryption.Decrypt("TZNgBLKIkBc6EEofyJUhRw==");
                //    try
                //    {
                       
                //        var url = Request.Url.Query;
                //        if (url != "")
                //        {
                //            string queryString = url.ToString();
                //            char[] delimiterChars = { '=' };
                //            string[] words = queryString.Split(delimiterChars);
                //            string code = words[1];

                //            if (code != null)
                //            {
                //                //get the access token 
                //                HttpWebRequest webRequest = (HttpWebRequest)WebRequest.Create("https://accounts.google.com/o/oauth2/token");
                //                webRequest.Method = "POST";
                //               string Parameters = "code=" + code + "&client_id=" + googleplus_client_id + "&client_secret=" + googleplus_client_secret + "&redirect_uri=" + googleplus_redirect_url + "&grant_type=authorization_code";
                //                byte[] byteArray = Encoding.UTF8.GetBytes(Parameters);
                //                webRequest.ContentType = "application/x-www-form-urlencoded";
                //                webRequest.ContentLength = byteArray.Length;
                //                Stream postStream = webRequest.GetRequestStream();
                //                // Add the post data to the web request
                //                postStream.Write(byteArray, 0, byteArray.Length);
                //                postStream.Close();

                //                WebResponse response = webRequest.GetResponse();
                //                postStream = response.GetResponseStream();
                //                StreamReader reader = new StreamReader(postStream);
                //                string responseFromServer = reader.ReadToEnd();

                //                GooglePlusAccessToken serStatus = JsonConvert.DeserializeObject<GooglePlusAccessToken>(responseFromServer);

                //                if (serStatus != null)
                //                {
                //                    string accessToken = string.Empty;
                //                    accessToken = serStatus.access_token;

                //                    if (!string.IsNullOrEmpty(accessToken))
                //                    {
                //                        // This is where you want to add the code if login is successful.
                //                        getgoogleplususerdataSerSignIn(accessToken);
                //                    }
                //                }

                //            }
                //        }
                //    }
                //    catch (Exception ex)
                //    {
                //        log.Error(ex.Message.ToString() + " " + ex.StackTrace.ToString());
                //        // Response.Redirect("index.aspx");
                //    }
                //}

            }
        }

        //private async void getgoogleplususerdataSerSignIn(string access_token)
        //{
        //    try
        //    {
        //        EmployeeDAL objEmployeeDAL = new EmployeeDAL();
        //        Userclass userAllInfo = new Userclass();
        //        HttpClient client = new HttpClient();
        //       // string CompanyId = "";
        //        var urlProfile = "https://www.googleapis.com/oauth2/v1/userinfo?alt=json&access_token=" + access_token;

        //        client.CancelPendingRequests();
        //        HttpResponseMessage output = await client.GetAsync(urlProfile);

        //        if (output.IsSuccessStatusCode)
        //        {
        //            string outputData = await output.Content.ReadAsStringAsync();
        //            GoogleUserOutputData serStatus = JsonConvert.DeserializeObject<GoogleUserOutputData>(outputData);

        //            if (serStatus != null)
        //            {
        //                // You will get the user information here.
        //                userAllInfo.id = serStatus.id;
        //                userAllInfo.first_name = serStatus.given_name;
        //                userAllInfo.last_name = serStatus.family_name;
        //                userAllInfo.Emails = serStatus.email;
        //                userAllInfo.locale = serStatus.locale;
        //                userAllInfo.picture = serStatus.picture;
        //                userAllInfo.name = serStatus.name;

                        
        //                try
        //                {

        //                    string Password = Encryption.CreatePassword();
        //                    string UserName = userAllInfo.Emails;
        //                    string pass = Encryption.Encrypt(Password);
        //                    string verificationcode = Guid.NewGuid().ToString();
        //                    EmployeeBE objEmployeeBE = new EmployeeBE();

        //                    // CompanyId = "987109F8-B639-4FE0-85F3-0FCFEFF63B5E";
        //                    //CompanyId = Session["CompanyId"].ToString();
        //                    //string BatchID = Session["BatchID"].ToString();
        //                    objEmployeeBE.propUserName = UserName;
        //                   // objEmployeeBE.propPassword = pass;
        //                    //objEmployeeBE.propCompanyId_FK = Session["CompanyId"].ToString();
        //                    objEmployeeBE.propFirstName = userAllInfo.first_name;
        //                    objEmployeeBE.propLastName = userAllInfo.last_name;
        //                    objEmployeeBE.propEmailId = userAllInfo.Emails;
        //                    //objEmployeeBE.PropBatchID = "372BA8D3-F52A-4EDA-9CA2-B5968010A96C";
        //                  //  objEmployeeBE.PropBatchID = Session["BatchID"].ToString();
        //                    //objEmployeeBE.verificationcode = verificationcode;
        //                    // Session["verificationcode"] = verificationcode;
        //                    //  objEmployeeBE.propOccupation = ddloccupation.SelectedItem.Text;
        //                    //objEmployeeBE.propExperienceInYears = txtexp.Text;
        //                    Session["Email"] = userAllInfo.Emails;


                            
                            
                                
        //                        DataTable dt = objEmployeeDAL.GetEmployeeByEmail(objEmployeeBE);
        //                    if (dt != null && dt.Rows.Count > 0 && dt.Rows[0]["UserName"].ToString()== objEmployeeBE.propEmailId.ToString())
        //                    {
        //                        Session["CompanyId"] = dt.Rows[0]["CompanyID_FK"].ToString();
        //                        Session["EmployeeID"] = dt.Rows[0]["EmployeeId_PK"].ToString();
        //                        Session["BatchID"] = dt.Rows[0]["BatchID_FK"].ToString();
        //                        BindSubmittedAssessments();
        //                    }
        //                    else
        //                    {
        //                        lblMessage1.Text = "Please Sign Up";
        //                        ClientScript.RegisterStartupScript(this.GetType(), "Pop", "openModalAssessments();", true);
        //                        //Response.Redirect("EmpRegistrationForm.aspx?InstanceCompanyRelationId=3FB35DBD-00B3-4704-BA2C-BC7DC3F244AF&CouponId=1");
        //                    }


        //                }
        //                catch (Exception e)
        //                {
        //                    log.Error(e.Message.ToString() + " " + e.StackTrace.ToString());
        //                }
        //            }
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        log.Error(ex.Message.ToString() + " " + ex.StackTrace.ToString());
        //    }
        //}


        //private void BindSubmittedAssessments()
        //{

        //    try
        //    {
        //        string batchId = string.Empty;
        //        // Button lnkBtn;
        //        if (Session["BatchID"] != null)
        //        {
        //             batchId = Convert.ToString(Session["BatchID"].ToString());
        //        }
        //        EmployeeAssessmentsHistoryDAL objEmployeeAssessmentsHistoryDAL = new EmployeeAssessmentsHistoryDAL();
        //        DataTable DtAssessments = objEmployeeAssessmentsHistoryDAL.EmployeeAssementsGet(Session["EmployeeId"].ToString(), batchId);
        //        if (DtAssessments != null && DtAssessments.Rows.Count > 0)
        //        {
        //            //for (int i = 0; i < DtAssessments.Rows.Count; i++)
        //            //{
        //            //    DtAssessments.Rows.Remove(DtAssessments.Rows[0]);
        //            //}
        //            string id = Session["EmployeeId"].ToString();
        //            for (int j = 0; j < DtAssessments.Rows.Count; j++)
        //            {
        //                if ((DtAssessments.Rows[j]["CreatedDate"]) == DBNull.Value)
        //                {
        //                    DtAssessments.Rows.Remove(DtAssessments.Rows[j]);
        //                }
        //            }

        //             var LastUpdate = DtAssessments.AsEnumerable().Max(r => r.Field<DateTime>("CreatedDate"));


        //            for (int i = 0; i < DtAssessments.Rows.Count; i++)
        //            {


        //                if ((LastUpdate == Convert.ToDateTime(DtAssessments.Rows[i]["CreatedDate"])) && ((bool)DtAssessments.Rows[i]["AssessmentSubmitted"]) == false)
        //                {
        //                    DataColumnCollection columns = DtAssessments.Columns;
        //                    if (columns.Contains("LastUpadte"))
        //                    {
        //                        if (Convert.ToDateTime(DtAssessments.Rows[i]["CreatedDate"]) == LastUpdate)
        //                        {

        //                            DtAssessments.Rows[i]["LastUpadte"] = LastUpdate;
        //                            DtAssessments.Rows[i]["CreatedDate"] = DateTime.Now.Date;
        //                        }

        //                    }
        //                    else
        //                    {

        //                        DtAssessments.Columns.Add("LastUpadte", typeof(DateTime));
        //                        DtAssessments.Rows[i]["LastUpadte"] = LastUpdate;

        //                    }
        //                }
        //                else
        //                {
        //                    DataColumnCollection columns = DtAssessments.Columns;
        //                    if (columns.Contains("LastUpadte"))
        //                    {

        //                    }
        //                    else
        //                    {
        //                        DtAssessments.Columns.Add("LastUpadte", typeof(DateTime));
        //                    }

        //                }

        //            }


        //            if (DtAssessments != null && DtAssessments.Rows.Count > 0)
        //            {
        //                //// DtAssessments.Columns.Remove('SR')
        //                // DataView view = DtAssessments.DefaultView;
        //                // view.Sort = "CreatedDate ASC";
        //                // DataTable sortedDate = view.ToTable();

        //                for (int i = 0; i < DtAssessments.Rows.Count; i++)
        //                {
        //                    DtAssessments.Rows[i]["CreatedDate"] = DateTime.Now.Date;
        //                }


        //                DlistAssessments.DataSource = DtAssessments;
        //                DlistAssessments.DataBind();

        //            }
        //        }
        //    }
        //    catch(Exception ex)
        //    {
        //        log.Error(ex.Message.ToString() + " " + ex.StackTrace.ToString());
        //    }
        //}

        private void BindSubmittedAssessments()
        {

            try
            {
                // Button lnkBtn;
                string batchId = Session["BatchID"].ToString();
                EmployeeAssessmentsHistoryDAL objEmployeeAssessmentsHistoryDAL = new EmployeeAssessmentsHistoryDAL();
                DataTable DtAssessments = objEmployeeAssessmentsHistoryDAL.EmployeeAssementsGet(Session["EmployeeId"].ToString(), batchId);
                string id = Session["EmployeeId"].ToString();
                string AssessmentInstanceId_FreeTest = ConfigurationManager.AppSettings.Get("AssessmentInstanceId_FreeTest");
                if (DtAssessments != null && DtAssessments.Rows.Count > 0)
                {
                    for (int j = 0; j < DtAssessments.Rows.Count; j++)
                    {
                        //if ((DtAssessments.Rows[j]["CreatedDate"]) == DBNull.Value)
                        //{
                        //    DtAssessments.Rows.Remove(DtAssessments.Rows[j]);
                        //}

                        if(Convert.ToString(Session["FlagTest"]) != "free" && Convert.ToString(DtAssessments.Rows[j]["AssessmentInstanceId_FK"]).ToUpper() == AssessmentInstanceId_FreeTest.ToString())
                        {
                            DtAssessments.Rows.Remove(DtAssessments.Rows[j]);
                        }
                        if (Convert.ToString(Session["FlagTest"]) == "free" && Convert.ToString(DtAssessments.Rows[j]["AssessmentInstanceId_FK"]).ToUpper() != AssessmentInstanceId_FreeTest.ToString())
                        {
                            DtAssessments.Rows.Remove(DtAssessments.Rows[j]);
                        }
                    }

                    var LastUpdate = DtAssessments.AsEnumerable().Max(r => r.Field<DateTime>("CreatedDate"));


                    for (int i = 0; i < DtAssessments.Rows.Count; i++)
                    {
                        if ((LastUpdate == Convert.ToDateTime(DtAssessments.Rows[i]["CreatedDate"])) && ((bool)DtAssessments.Rows[i]["AssessmentSubmitted"]) == false)
                        {
                            DataColumnCollection columns = DtAssessments.Columns;
                            if (columns.Contains("LastUpadte"))
                            {
                                if (Convert.ToDateTime(DtAssessments.Rows[i]["CreatedDate"]) == LastUpdate)
                                {

                                    DtAssessments.Rows[i]["LastUpadte"] = LastUpdate;
                                }

                            }
                            else
                            {

                                DtAssessments.Columns.Add("LastUpadte", typeof(DateTime));
                                DtAssessments.Rows[i]["LastUpadte"] = LastUpdate;

                            }
                        }
                        else
                        {
                            DataColumnCollection columns = DtAssessments.Columns;
                            if (columns.Contains("LastUpadte"))
                            {

                            }
                            else
                            {
                                DtAssessments.Columns.Add("LastUpadte", typeof(DateTime));
                            }

                        }

                    }
                    if (Convert.ToString(Session["FlagTest"]) == "free")
                    {
                        for (int j = 0; j < DtAssessments.Rows.Count; j++)
                        {
                            if (Convert.ToBoolean(DtAssessments.Rows[j]["AssessmentSubmitted"]) == false && (DtAssessments.Rows[j]["LastUpadte"]) == DBNull.Value)
                            {
                                DtAssessments.Rows.Remove(DtAssessments.Rows[j]);
                            }
                        }
                    }

                    if (DtAssessments != null && DtAssessments.Rows.Count > 0)
                    {
                        //// DtAssessments.Columns.Remove('SR')
                        // DataView view = DtAssessments.DefaultView;
                        // view.Sort = "CreatedDate ASC";
                        // DataTable sortedDate = view.ToTable();
                        for (int j = 0; j < DtAssessments.Rows.Count; j++)
                        {
                            if (Convert.ToBoolean(DtAssessments.Rows[j]["AssessmentSubmitted"]) == true)
                            {
                                Response.Redirect("UserResultReport.aspx");
                            }
                        }

                        DlistAssessments.DataSource = DtAssessments;
                        DlistAssessments.DataBind();

                    }
                }

            }
            catch
            {

            }
        }


        protected void DlistAssessments_ItemCommand(object source, DataListCommandEventArgs e)
        {
            Session["AssessmentInstanceId"] = e.CommandArgument.ToString();
            Label lblAssessmentInstanceCompanyId = (Label)DlistAssessments.Items[e.Item.ItemIndex].FindControl("lblAssessmentInstanceCompanyId");

            Session["AssessmentInstanceCompanyId"] = lblAssessmentInstanceCompanyId.Text;
            if (e.CommandName == "ViewReport")
            {
                if (Session["FlagTest"]!=null && Session["FlagTest"].ToString() == "free")
                {
                    Response.Redirect("UserResultReport.aspx");
                }
                else
                {
                    Response.Redirect("UserPaidTestReport.aspx", false);
                }
                //Response.Redirect("UserResult.aspx");
            }
            else if (e.CommandName == "ViewTest")
            {
                Response.Redirect("Questions.aspx");
               // Response.Redirect("Assessment.aspx");
                //   Response.Redirect("DatewiseScore.aspx");
            }
        }
    }
}