﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Assessment.aspx.cs" MasterPageFile="~/Employee/Employee.Master" Inherits="HealToZeal.Employee.Assessment" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<%--<asp:Content ID="Content2" ContentPlaceHolderID="EmployeeColumn" runat="server">
    <link href="../Styles/Employee.css" rel="stylesheet" type="text/css" />
  <div class="divClass" style="height:500px;">
      <asp:Label ID="lblmessage" runat="server" ForeColor="Red"></asp:Label>
      <h4 style="text-align:center">   <asp:Label ID="lblAssessment" runat="server" Text="Assessment"></asp:Label></h4>
   
    <table style="height:80%">
    <tr style="height:90%">
    <td colspan="4">
    <div style="height:100%; overflow:scroll;">
         <asp:DataList ID="DlistQuestions" runat="server" 
              onitemdatabound="DlistQuestions_ItemDataBound">
              <AlternatingItemStyle BackColor="LightGray" CssClass="AlternateRowClass"  />
             <SeparatorStyle BorderColor="Black" BorderStyle="Solid"  Width="100%" />
        <ItemTemplate>
       
       <div id="maincontainer" >
        <table>
        <tr>
        <td style="padding-bottom:10px;padding-top:10px">
            <asp:Label ID="lblSrNo" runat="server" Text='<%#Eval("SrNo") %>'></asp:Label>
             <asp:Label ID="lblQuestionId" runat="server" Text='<%#Eval("QuestionId_FK") %>' Visible="false"></asp:Label>
            <asp:Label ID="lblQuestion" runat="server" Text='<%#Eval("Question") %>'></asp:Label>
        </td>
        </tr>
        <tr>
        <td  >
            <asp:RadioButtonList ID="rblistAnswers" runat="server" RepeatDirection="Vertical">
            </asp:RadioButtonList>
        </td>
        </tr>
        </table>
        </div>
        </ItemTemplate>
        </asp:DataList>
    </div>     
    </td>
    </tr>
    <tr style="height:10%">
    <td>
       <asp:Button ID="btnPrevious" runat="server" Text="Previous"  CssClass="btnLoginClass"
            onclick="btnPrevious_Click" />
    </td>
    <td>
        <asp:Button ID="btnNext" runat="server" Text="Next" CssClass="btnLoginClass" onclick="btnNext_Click" />
    </td>
    <td>
        &nbsp;</td>
    </tr>
    </table>
  
    </div>
</asp:Content>--%>
<asp:Content ID="Content2" ContentPlaceHolderID="EmployeeColumn" runat="server" >
    <br />
   
    <link href="../Styles/Employee.css" rel="stylesheet" type="text/css" />
      <link href="../Styles/slider.css" rel="stylesheet" type="text/css">
    <script src="../Scripts/jquery.colorbox-min.js" type="text/javascript"></script>   
    <script src="../Scripts/jquery.colorbox.js" type="text/javascript"></script>
    <script type="text/javascript">
        function DisplayColorBox(url) {

            $.fn.colorbox({ href: url, iframe: true, scrolling: true, width: '300px', position: 'Fixed', height: '150px', overlayClose: false });
            //, onClosed: function () { parent.location.reload(false); }
            return false;
        }
    </script>
    <div class="container">
        <div class="">

            <div class=" col-lg-12 col-md-12 col-sm-12" >
                <div class="" style=" margin-top:-30px;">
                    <h4 class="hs_heading" id="hs_appointment_form_link" >Mindfulnesstest
                    </h4>
                </div>
            </div>

        <%--    <div class="row">
                <ul class="pager">
                    <li class="previous">
                        <a href="#">
                            <asp:Button ID="btnPrevious1" runat="server" Text="Previous" CssClass="btn btn-success" OnClick="btnPrevious_Click" />

                        </a></li>
                    <li>
                        <div class="  col-lg-9">
                            <a href="#">
                                <div class="progress progress-striped active">

                                    <div runat="server" id="Progressbar1" class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100"><span class="sr-only"></span></div>

                                </div>
                            </a>
                        </div>
                    </li>
                    <li class="next"><a href="#">
                        <asp:Button ID="btnNext1" runat="server" Text="Next" CssClass="btn btn-success" OnClick="btnNext_Click" />
                    </a></li>
                </ul>
            </div>--%>

             <div class="row">
                    <div class="col-lg-2"></div>
                    <div class="col-lg-8" style=" margin-top:-30px;">
                        <div class="progress progress-striped active">
                            <div runat="server" id="Progressbar1" class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100">
                                <span class="sr-only"></span>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-2"></div>
                </div>
               <div class="row" >
                    <ul class="pager" style=" margin-top:-20px;">
                        <li class="previous"><a href="#">
                          <asp:Button ID="btnPrevious1" runat="server" Text="Previous" CssClass="btn btn-success" OnClick="btnPrevious_Click" />
                        </a></li>
                        <li></li>
                        <li class="next"><a href="#">
                             <asp:Button ID="btnNext1" runat="server" Text="Next" CssClass="btn btn-success" OnClick="btnNext_Click" />
                        </a></li>
                    </ul>
                </div>
               
            <div class="row" id="focus">
                <div class="col-lg-9">
                    <div id="divalert" class="alert alert-success" runat="server">
                        <strong>
                            <asp:Label ID="lblMessage" runat="server"></asp:Label></strong>
                    </div>
                </div>
                <%--   <div class="col-lg-3">
                 <asp:LinkButton ID="lnkBtnJumpto" runat="server" CssClass="btn btn-link btn-lg" OnClick="lnkBtnJumpto_Click">Go to Unanswered Questions </asp:LinkButton>
                </div>   --%>
            </div>
        </div>
        <div class="row">
            <div id="persistMe" class="col-lg-12 col-md-12 col-sm-12" onscroll="javascript:SetDivPosition()" >
                <asp:DataList ID="DlistQuestions" runat="server" OnItemDataBound="DlistQuestions_ItemDataBound" Width="100%">
                    <ItemTemplate>
                        <div class="hs_comment">
                            <div class="row">
                                <div class="col-lg-11 col-md-11 col-sm-10">
                                    <div class="hs_comment_date" style=" margin:-30px -10px -10px -10px">
                                        <ul>
                                            <li></li>
                                            <li >
                                                <asp:Label ID="lblQuestionId" runat="server" Text='<%#Eval("QuestionId_FK") %>' Visible="false" onclick="getfocus()"></asp:Label>
                                            </li>
                                        </ul>
                                        <p  >
                                            <h4 >
                                                <asp:Label ID="lblSrNo" runat="server" Text='<%#Eval("SrNo")+". " %>'></asp:Label>
                                                <a class="hs_in_relpy">
                                                    <asp:Label ID="lblQuestion" runat="server" Text='<%#Eval("Question") %>'></asp:Label>
                                                </a>
                                            </h4>
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="hs_sub_comment_div">
                            <div class="hs_sub_comment">
                                <div class="row">
                                    <div class="col-lg-10 col-md-10 col-sm-10 col-lg-offset-2 col-md-offset-2 col-sm-offset-2">

                                        <div class="hs_comment">
                                            <div class="row">
                                                <div class="col-lg-11 col-md-10 col-sm-10">
                                                    <%--  <i class="fa fa-paperclip"></i>--%>
                                                    <p>
                                                        <asp:RadioButtonList ID="rblistAnswers" runat="server" RepeatDirection="Vertical" CssClass="radio-inline" OnSelectedIndexChanged="rblistAnswers_SelectedIndexChanged" AutoPostBack="true" >
                                                        </asp:RadioButtonList>
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        
                    </ItemTemplate>
                </asp:DataList>
                <%-- <div class="row">
                    <asp:Repeater ID="rptPager" runat="server">
                        <ItemTemplate>
                            <asp:LinkButton ID="lnkPage" runat="server" Text='<%#Eval("Text") %>' CommandArgument='<%# Eval("Value") %>'                            
                          OnClick="Page_Changed"  ></asp:LinkButton>
                        </ItemTemplate>
                    </asp:Repeater>
                </div>--%>
                <div style="clear:both"></div>
                <div class=" row" >
                    <ul class="pager" >
                        <li class="previous"><a href="#">
                            <asp:Button ID="btnPrevious" runat="server" Text="Previous" CssClass="btn btn-success" OnClick="btnPrevious_Click" />
                        </a></li>
                        <li></li>
                        <li class="next"><a href="#">
                            <asp:Button ID="btnNext" runat="server" Text="Next" CssClass="btn btn-success" OnClick="btnNext_Click" />
                        </a></li>
                    </ul>
                </div>
                <div class="row">
                    <div class="col-lg-2"></div>
                    <div class="col-lg-8">
                        <div class="progress progress-striped active">
                            <div runat="server" id="Progressbar" class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100">
                                <span class="sr-only"></span>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-2"></div>
                </div>
            </div>
        </div>
    </div>
     
 <%--  <script type="text/javascript">
 
       $('.focus').focus();
 
</script>--%>
  
    <script type="text/javascript">
function GetDivPosition()
{
  var strCook = document.cookie;
  if(strCook.indexOf("!~")!=0){
   var intS = strCook.indexOf("!~");

var intE = strCook.indexOf("~!");

var strPos = strCook.substring(intS+2,intE);

document.getElementById("persistMe").scrollTop = strPos;

}

}

window.onload = function()
{
  GetDivPosition();
}

function SetDivPosition(){

var intY = document.getElementById("persistMe").scrollTop;

document.title = intY;

document.cookie = "yPos=!~" + intY + "~!";

}

</script>
</asp:Content>
