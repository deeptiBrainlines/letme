﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Employee/Employee.Master" AutoEventWireup="true" CodeBehind="EmployeeFeedback.aspx.cs" Inherits="HealToZeal.Employee.EmployeeFeedback" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="EmployeeColumn" runat="server">
      <div class="container">
          <h4 class="hs_heading" id="hs_appointment_form_link">Feedback </h4>
          <div class="row">
              <div id="divalert" class="alert alert-success" runat="server">
                  <strong>
                      <asp:Label ID="lblMessage" runat="server"></asp:Label></strong>
              </div>
          </div>
  <div class="row">
    <div class="col-lg-12 col-md-12 col-sm-12">       
        <asp:DataList ID="DlistQuestions" runat="server" 
            onitemdatabound="DlistQuestions_ItemDataBound" >
            <ItemTemplate>
                <div class="hs_comment">
                    <div class="row">                        
                        <div class="col-lg-11 col-md-11 col-sm-10">
                            <div class="hs_comment_date">
                                <ul>
                                    <li></li>
                                    <li>
                                        <asp:Label ID="lblQuestionId" runat="server" Text='<%#Eval("FeedbackQuestionId_PK") %>' Visible="false"></asp:Label>
                                    </li>                             
                                </ul>
                                <p>
                                    <h4>
                                        <asp:Label ID="lblSrNo" runat="server" Text='<%#Eval("SrNo")+". " %>'></asp:Label>
                                        <a class="hs_in_relpy">
                                            <asp:Label ID="lblQuestion" runat="server" Text='<%#Eval("FeedbackQuestion") %>'></asp:Label>
                                        </a>
                                    </h4>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="hs_sub_comment_div">
                    <div class="hs_sub_comment">
                        <div class="row">
                            <div class="col-lg-10 col-md-10 col-sm-10 col-lg-offset-2 col-md-offset-2 col-sm-offset-2">

                                <div class="hs_comment">
                                    <div class="row">
                                        <div class="col-lg-11 col-md-10 col-sm-10">
                                            <%--  <i class="fa fa-paperclip"></i>--%>
                                            <p>
                                                <asp:RadioButtonList ID="rblistAnswers" runat="server" RepeatDirection="Vertical" CssClass="radio-inline">
                                                </asp:RadioButtonList>  
                                                                   <asp:TextBox ID="txtFeedback" runat="server" CssClass="form-control" Width="100%" TextMode="MultiLine" ></asp:TextBox>
                                            </p>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>                   
                </div>    
                <div class="clearfix"></div>
      <div class="hs_margin_40"></div>          
            </ItemTemplate>
        </asp:DataList>
       
            <div class="hs_team_member_detail">
                <a href="#">
                    <asp:Button ID="btnSubmit" runat="server" Text="Submit" CssClass="btn btn-success"
                        OnClick="btnSubmit_Click" />
                </a><a href="#">
                    <asp:Button ID="btnCancel" runat="server" Text="Cancel" CssClass="btn btn-success"
                        OnClick="btnCancel_Click" />
                </a>
            </div>
           </div>
      </div>
        </div>
  
</asp:Content>
