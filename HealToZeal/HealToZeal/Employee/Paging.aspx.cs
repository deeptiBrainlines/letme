﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using BE;
using DAL;
using System.Web.Mail;
using System.Net.Mail;

namespace HealToZeal.Employee
{
    public partial class Paging : System.Web.UI.Page
    {
        AssessmentQuestionRelationDAL objAssessmentQuestionRelationDAL = new AssessmentQuestionRelationDAL();
        EmployeeAssessmentDetailsDAL objEmployeeAssessmentDetailsDAL = new EmployeeAssessmentDetailsDAL();
        EmployeeAssessmentDetailsBE objEmployeeAssessmentDetailsBE = new EmployeeAssessmentDetailsBE();
        AnswerMasterDAL objAnswerMasterDAL = new AnswerMasterDAL();
        DataTable DtSaveAnswer = new DataTable();

        //string AssessmentId = "";
        //string QuestionId = "";
        //string EmployeeId = "";
        //int position = 0;
        int PageSize = 10;      

        protected void Page_Load(object sender, EventArgs e)
        {

        }
        private void GetCustomersPageWise(int pageIndex)
        {
            //string constring = ConfigurationManager.ConnectionStrings["constring"].ConnectionString;
            //using (SqlConnection con = new SqlConnection(constring))
            //{
            //    using (SqlCommand cmd = new SqlCommand("GetCustomersPageWise", con))
            //    {
            //        cmd.CommandType = CommandType.StoredProcedure;
            //        cmd.Parameters.AddWithValue("@PageIndex", pageIndex);
            //        cmd.Parameters.AddWithValue("@PageSize", PageSize);
            //        cmd.Parameters.Add("@RecordCount", SqlDbType.Int, 4);
            //        cmd.Parameters["@RecordCount"].Direction = ParameterDirection.Output;
            //        con.Open();
            //        IDataReader idr = cmd.ExecuteReader();
            //        DlistQuestions.DataSource = idr;
            //        DlistQuestions.DataBind();
            //        idr.Close();
            //        con.Close();
            //        int recordCount = Convert.ToInt32(cmd.Parameters["@RecordCount"].Value);
            //        this.PopulatePager(recordCount, pageIndex);
            //    }
            //}
        }
        protected void Page_Changed(object sender, EventArgs e)
        {
            int pageIndex = int.Parse((sender as LinkButton).CommandArgument);
            this.GetCustomersPageWise(pageIndex);
        }
        private void PopulatePager(int recordCount, int currentPage)
        {
            List<ListItem> pages = new List<ListItem>();
            int startIndex, endIndex;
            int pagerSpan = 5;

            //Calculate the Start and End Index of pages to be displayed.
            double dblPageCount = (double)((decimal)recordCount / Convert.ToDecimal(PageSize));
            int pageCount = (int)Math.Ceiling(dblPageCount);
            startIndex = currentPage > 1 && currentPage + pagerSpan - 1 < pagerSpan ? currentPage : 1;
            endIndex = pageCount > pagerSpan ? pagerSpan : pageCount;
            if (currentPage > pagerSpan % 2)
            {
                if (currentPage == 2)
                {
                    endIndex = 5;
                }
                else
                {
                    endIndex = currentPage + 2;
                }
            }
            else
            {
                endIndex = (pagerSpan - currentPage) + 1;
            }

            if (endIndex - (pagerSpan - 1) > startIndex)
            {
                startIndex = endIndex - (pagerSpan - 1);
            }

            if (endIndex > pageCount)
            {
                endIndex = pageCount;
                startIndex = ((endIndex - pagerSpan) + 1) > 0 ? (endIndex - pagerSpan) + 1 : 1;
            }

            //Add the First Page Button.
            if (currentPage > 1)
            {
                pages.Add(new ListItem("First", "1"));
            }

            //Add the Previous Button.
            if (currentPage > 1)
            {
                pages.Add(new ListItem("<<", (currentPage - 1).ToString()));
            }

            for (int i = startIndex; i <= endIndex; i++)
            {
                pages.Add(new ListItem(i.ToString(), i.ToString(), i != currentPage));
            }

            //Add the Next Button.
            if (currentPage < pageCount)
            {
                pages.Add(new ListItem(">>", (currentPage + 1).ToString()));
            }

            //Add the Last Button.
            if (currentPage != pageCount)
            {
                pages.Add(new ListItem("Last", pageCount.ToString()));
            }
            rptPager.DataSource = pages;
            rptPager.DataBind();
        }
    }
}