﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="UserResult.aspx.cs" MasterPageFile="~/Employee/Employee.Master" Inherits="HealToZeal.Employee.UserResult" %>
<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=11.0.0.0, Culture=neutral, PublicKeyToken=89845dcd8080cc91" Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>
<%@ Register Assembly="System.Web.DataVisualization, Version=4.0.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" Namespace="System.Web.UI.DataVisualization.Charting" TagPrefix="asp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="EmployeeColumn" runat="server">    
    <div class="container">
        <h5>
           <asp:LinkButton ID="lbBack" runat="server" PostBackUrl="~/Employee/EmployeeAssessments.aspx" CssClass="btn btn-link btn-lg">Back</asp:LinkButton></h5>
   
        <h4 class="hs_heading" id="hs_appointment_form_link">My mindfulness report
        </h4>
                
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12">
                <div class="hs_single_profile">
                    <div class="hs_single_profile_detail">
                        <asp:Repeater ID="RptrEmployee" runat="server">
                            <ItemTemplate>
                                <h3>
                                    <asp:Label ID="lblName" runat="server" Text='<%# Eval("EmployeeName") %>'></asp:Label>
                                </h3>
                                <div class="row">
                                    <div class="col-lg-3 col-md-6 col-sm-6">
                                        <i class="fa fa-calendar"></i>Date:
                                            <a href="">
                                                <asp:Label ID="lblDate" runat="server" Text='<%# DateTime.Now.ToString("dd/MM/yyyy") %>'></asp:Label>
                                            </a>
                                    </div>
                                    <div class="col-lg-3 col-md-6 col-sm-6">
                                        <i class="fa fa-user-md"></i>Gender:
                                            <a href="">
                                                <asp:Label ID="lblGender" runat="server" Text='<%# Eval("Gender") %>'>
                                                </asp:Label>
                                            </a>
                                    </div>
                                    <div class="col-lg-4 col-md-6 col-sm-6">
                                        <i class="fa fa-calendar"></i>Assessment Submitted Date: 
                                           <a href="">
                                               <asp:Label ID="lblSubmittedDate" runat="server" Text='<%#  DataBinder.Eval(Container.DataItem, "CreatedDate", "{0:dd/MM/yyyy}") %>'></asp:Label>
                                           </a>

                                    </div>
                                </div>
                            </ItemTemplate>
                        </asp:Repeater>
                    </div>
                </div>
            </div>
        </div>
        
        <div class="row">
            <asp:Panel ID="PnlReport" runat="server" Font-Size="Medium">
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12">
                        <asp:UpdatePanel ID="UpPnl" runat="server">
                        <ContentTemplate>                
                <%--<div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12">--%>
                    <div class="hs_contact">                       
                      <%--<div class="row">--%>
                            <div id="divalert1"  runat="server">                                
                                <strong>                                   
                                    <asp:Label ID="LblMessageTitle1" runat="server" ></asp:Label>                                        
                                <br />
                                 <asp:Label ID="LblMessageUp" runat="server"></asp:Label></strong>
                                <asp:Button ID="BtnSetUpMeeting" CssClass="btn btn-link" runat="server" Text="Contact for Appointment" OnClick="BtnSetUpMeeting_Click"  Visible="false"/>
                            </div>
                        <%--</div> --%>
                        <div class="" id="DivTitle" runat="server" visible="false">
                            <div id="div7" class="alert alert-info" runat="server">
                                <strong>
                                    <asp:Label ID="Label14" runat="server" Text='Call 91-9850864727 or please write an email with your contact number. We will get back to you soon.'></asp:Label>
                                </strong>
                            </div>
                        </div>
                        <div class="row" id="DivMailBody" runat="server" visible="false">
                            <div class="col-lg-8 col-md-7 col-sm-7">
                                <div class="hs_comment_form">
                                    <div class="row">
                                        <div class="col-lg-6 col-md-6 col-sm-12">
                                            <div>
                                                <asp:Label Visible="false" ID="Label15" runat="server" Text='From'></asp:Label>
                                                <input visible="false" disabled="disabled" id="uemail" runat="server" type="text" class="form-control" placeholder="From" />
                                            </div>
                                        </div>
                                        <div class="col-lg-6 col-md-6 col-sm-12">
                                            <div>
                                                <asp:Label ID="Label16" Visible="false" runat="server" Text='To'></asp:Label>
                                                <input disabled="disabled" runat="server" visible="false" id="Text4" type="text" class="form-control" placeholder="healtozeal@chetanaTSPL.com" />
                                            </div>
                                        </div>
                                        <div class="col-lg-12">
                                            <div class="form-group">
                                                <textarea id="message" runat="server" class="form-control" rows="8"></textarea>
                                            </div>
                                        </div>
                                        <p id="err"></p>
                                        <div class="form-group">
                                            <div class="col-lg-8 col-md-8 col-sm-12">
                                                <div class="checkbox">
                                                    <label>
                                                        <input type="checkbox" id="hs_checkbox" class="css-checkbox lrg" checked="checked" />
                                                    </label>
                                                </div>
                                            </div>
                                            <div class="col-lg-4 col-md-4 col-sm-12">
                                                <asp:Button ID="Button3" runat="server" CssClass="btn btn-success pull-right" Text="Submit" OnClick="BtnSendEmail_Click" />

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                        <%--</div>
                </div>--%>
            </ContentTemplate>
        </asp:UpdatePanel>
                        <div class="hs_contact">
                           <%-- <ul>
                                <li>--%>
                                    <h4 class="hs_heading" id="Ttle" runat="server">Summary of attributes which are assets in performance</h4>
                                    <asp:GridView ID="GvPositive" runat="server" AutoGenerateColumns="False" GridLines="None">
                                        <Columns>
                                            <asp:TemplateField>
                                                <ItemTemplate>
                                                    <p>                                                        
                                                        <asp:Label ID="lblColor" runat="server" Text="" Height="20px" Width="20px" BackColor='<%# System.Drawing.Color.FromName((string) Eval("Color")) %>'> </asp:Label>
                                                        <asp:Label ID="lblResultText" runat="server" Text='<%#Eval("DisplayResultText")%> '> </asp:Label>
                                                    </p>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                        </Columns>
                                    </asp:GridView>

                               <%-- </li>
                                 <li>--%>
                                    <p>
                                     <h4><strong>Note:</strong> </h4>
                                            <%--If you have scored below 50% then you are in yellow zone which is alert zone and if you have scored above 50% , then you are in Green zone which is the safe zone and you are doing good in that area.--%>
                                        If the bullet is showing green, then you are doing well, if it is yellow you need to take appropriate action.
                                        <p>
                                        </p>
                                        <p>
                                        </p>
                                        <%-- </li>
                                <li>--%>
                                        <h4 id="Ttle1" runat="server" class="hs_heading">Summary of factors which may have adverse long term effects on work and life</h4>
                                        <asp:GridView ID="GvResult" runat="server" AutoGenerateColumns="False" GridLines="None">
                                            <Columns>
                                                <asp:TemplateField>
                                                    <ItemTemplate>
                                                        <p>
                                                            <asp:Label ID="lblColor" runat="server" BackColor='<%# System.Drawing.Color.FromName((string) Eval("Color")) %>' Height="20px" Text="" Width="20px"> </asp:Label>
                                                            <asp:Label ID="lblResultText" runat="server" Text='<%#Eval("DisplayResultText")%> '> </asp:Label>
                                                        </p>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                            </Columns>
                                        </asp:GridView>
                                        <%--</li>
                                <li>--%>
                                        <p>
                                            <h4><strong>How to interpret:</strong> </h4>
                                            Individual&nbsp;factor&nbsp;scores&nbsp;(Ability to withstand pressures,&nbsp;Emotional balance,&nbsp;self assurance)&nbsp;is&nbsp;a&nbsp;profile&nbsp;as&nbsp;on&nbsp;a&nbsp;particular&nbsp;date.<br /> A&nbsp;score&nbsp;higher&nbsp;than&nbsp;the&nbsp;score&nbsp;on&nbsp;a&nbsp;previous&nbsp;date&nbsp;means&nbsp;improvement.&nbsp;&nbsp;Actions&nbsp;which&nbsp;helped&nbsp;in&nbsp;this&nbsp;should&nbsp;continue.<br /> A&nbsp;score&nbsp;lower&nbsp;than&nbsp;previous&nbsp;score&nbsp;means&nbsp;that&nbsp;there&nbsp;is&nbsp;deterioration.&nbsp;Urgent&nbsp;professional&nbsp;help&nbsp; recommended.&nbsp;You&nbsp;need&nbsp;to&nbsp;seek&nbsp;an&nbsp;appointment&nbsp;with&nbsp;a&nbsp;professional&nbsp;counselor.&nbsp;
                                            <p>
                                            </p>
                                            <br />
                                            <rsweb:ReportViewer ID="ReportViewer1" runat="server" BackColor="White" Font-Names="Verdana" Font-Size="8pt" Height="500px" ShowBackButton="False" ShowCredentialPrompts="False" ShowDocumentMapButton="False" ShowExportControls="False" ShowFindControls="False" ShowPageNavigationControls="False" ShowParameterPrompts="False" ShowPrintButton="False" ShowPromptAreaButton="False" ShowRefreshButton="False" ShowToolBar="False" ShowWaitControlCancelLink="False" ShowZoomControl="False" WaitMessageFont-Names="Verdana" WaitMessageFont-Size="14pt" Width="1000px">
                                                <LocalReport ReportPath="Reports\AllScoresByAssessmentForEmployee.rdlc">
                                                    <DataSources>
                                                        <rsweb:ReportDataSource DataSourceId="ObjectDataSource1" Name="DataSet1" />
                                                    </DataSources>
                                                </LocalReport>
                                            </rsweb:ReportViewer>
                                       
                                            <asp:ObjectDataSource ID="ObjectDataSource1" runat="server" OldValuesParameterFormatString="original_{0}" SelectMethod="GetData" TypeName="HealToZeal.AllClustersTableAdapters.SpAllScoreByEmployeeIdAndAssessmentIdForEmployeeTableAdapter">
                                                <SelectParameters>
                                                    <asp:SessionParameter DbType="Guid" Name="EmployeeId_FK" SessionField="EmployeeId" />
                                                      <asp:SessionParameter DbType="Guid" Name="AssessmentInstanceId_FK" SessionField="AssessmentInstanceId" />                                                    
                                                    <asp:SessionParameter DbType="Guid" Name="AssessmentInstanceCompanyId" SessionField="AssessmentInstanceCompanyId" />
                                                </SelectParameters>
                                            </asp:ObjectDataSource>
                                       
                                            <%-- </li> 
                                 <li>--%>
                                            <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                                <ContentTemplate>
                                                    <div class="row">
                                                        <div id="divalert" runat="server">
                                                            <strong>
                                                            <asp:Label ID="LblMessageTitle" runat="server"></asp:Label>
                                                            <br />
                                                            <asp:Label ID="lblMessage" runat="server"></asp:Label>
                                                            </strong>
                                                            <asp:Button ID="btnCounsel" runat="server" CssClass="btn btn-link" OnClick="btnCounsel_Click" Text="Contact for Appointment" Visible="false" />
                                                        </div>
                                                    </div>
                                                    <div id="DivSetUpMeeting" runat="server" class="row" visible="false">
                                                        <div id="div1" runat="server" class="alert alert-info">
                                                            <strong>
                                                            <asp:Label ID="Label1" runat="server" Text="Call 91-9850864727 or please write an email with your contact number. We will get back to you soon."></asp:Label>
                                                            </strong>
                                                        </div>
                                                    </div>
                                                    <div id="DivEmail" runat="server" class="row" visible="false">
                                                        <div class="col-lg-8 col-md-7 col-sm-7">
                                                            <div class="hs_comment_form">
                                                                <div class="row">
                                                                    <div class="col-lg-6 col-md-6 col-sm-12">
                                                                        <div>
                                                                            <asp:Label ID="Label3" runat="server" Text="From" Visible="false"></asp:Label>
                                                                            <input visible="false" disabled="disabled" id="uemail10" runat="server" type="text" class="form-control" placeholder="From" />
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-lg-6 col-md-6 col-sm-12">
                                                                        <div>
                                                                            <asp:Label ID="Label4" runat="server" Text="To" Visible="false"></asp:Label>
                                                                            <input disabled="disabled" runat="server" visible="false" id="uemail1" type="text" class="form-control" placeholder="healtozeal@chetanaTSPL.com" />
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-lg-12">
                                                                        <div class="form-group">
                                                                            <textarea id="message1" runat="server" class="form-control" rows="8"></textarea>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-lg-4 col-md-4 col-sm-12 pull-right">
                                                                        <asp:Button ID="BtnSendEmail" runat="server" CssClass="btn btn-success pull-right" OnClick="BtnSendEmail_Click" Text="Submit" />
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </ContentTemplate>
                                            </asp:UpdatePanel>
                                            <%--</li>                               
                            </ul>--%>
                                            <p>
                                            </p>
                                            <p>
                                            </p>
                                            <p>
                                            </p>
                                            <p>
                                            </p>
                                            <p>
                                            </p>
                                            <p>
                                            </p>
                                            <p>
                                            </p>
                                            <p>
                                            </p>
                                        </p>
                                        </p>
                        </div>
                    </div>
                </div>
                <div id="divSuggestion" runat="server" class="col-lg-12 col-md-12 col-sm-12">
                    <div class="hs_contact">
                        <ul>
                            <li>
                                <h4 class="hs_heading" id="Ttle3" runat="server">Suggestions</h4>
                                <asp:GridView ID="GvSuggestion" runat="server" AutoGenerateColumns="False" GridLines="None">
                                    <Columns>
                                        <asp:TemplateField>
                                            <ItemTemplate>
                                                <p>                                                   
                                                    <asp:Label ID="lblSuggestionTitle" runat="server" Text='<%# Eval("Title") %>' Visible="false"></asp:Label>&nbsp;&nbsp;
                                                </p>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField>
                                            <ItemTemplate>
                                                <asp:GridView ID="GdvSuggestionText" runat="server" AutoGenerateColumns="False" GridLines="None">
                                                    <Columns>
                                                        <asp:TemplateField>
                                                            <ItemTemplate>
                                                                <p>
                                                                    <%-- <asp:Label ID="lblSuggestionText" runat="server" Text='<%# Eval("Title")+"- "+Eval("Suggestions") %>'></asp:Label>--%>
                                                                    <asp:Label ID="lblSuggestionText" runat="server" Text='<%# Eval("Suggestions") %>'></asp:Label>
                                                                </p>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                    </Columns>
                                                </asp:GridView>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                </asp:GridView>
                            </li>
                        </ul>
                    </div>
                </div>
               <%-- <asp:GridView ID="GvRemedyObservation" runat="server" AutoGenerateColumns="False" GridLines="None" BackColor="#d2e9ff">
                    <Columns>
                        <asp:TemplateField HeaderText="Observations" HeaderStyle-Font-Bold="true" HeaderStyle-Font-Size="Medium" HeaderStyle-ForeColor="Maroon">
                            <ItemTemplate>
                                <table>
                                    <tr>
                                        <td style="width: 40px; vertical-align: top">
                                            <asp:Label ID="lblColor" runat="server" Text="" Height="20px" Width="20px" BackColor='<%# System.Drawing.Color.FromName((string) Eval("Color")) %>'> </asp:Label>
                                        </td>
                                        <td style="width: 800px; text-align: left">
                                            <asp:Label ID="lblSuggestion" runat="server" Text='<%#Eval("Suggestion") %>'></asp:Label>
                                        </td>
                                    </tr>
                                </table>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                </asp:GridView>
                <asp:GridView ID="GvStandard" runat="server" AutoGenerateColumns="False" GridLines="None" BackColor="#d2e9ff" Width="840px">
                    <Columns>
                        <asp:TemplateField HeaderText="Assessment">
                            <ItemTemplate>
                                <asp:Label ID="lblAssessmentName" runat="server" Text='<%#Eval("AssessmentName") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Cluster">
                            <ItemTemplate>
                                <asp:Label ID="lblClusterName" runat="server" Text='<%#Eval("ClusterName") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Score">
                            <ItemTemplate>
                                <asp:Label ID="lblScore" runat="server" Text='<%#Eval("ClusterScore") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="NoOfQuestions">
                            <ItemTemplate>
                                <asp:Label ID="lblTotalQuestions" runat="server" Text='<%#Eval("NoofQuestions") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="NoOfMaxScoredAnswers">
                            <ItemTemplate>
                                <asp:Label ID="Label2" runat="server" Text='<%#Eval("AnswerCount") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                </asp:GridView>
                <asp:GridView ID="GvEmployeeResult" runat="server" Style="margin-right: 0px" AutoGenerateColumns="False" GridLines="None">
                    <Columns>
                        <asp:TemplateField HeaderText="Assessment">
                            <ItemTemplate>
                               
                                <asp:Label ID="Label9" runat="server" Text='<%#Eval("AssessmentName") %>'></asp:Label>
                               
                       
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Cluster">
                            <ItemTemplate>
                                <asp:Label ID="Label10" runat="server" Text='<%#Eval("ClusterName") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Score">
                            <ItemTemplate>
                                <asp:Label ID="Label11" runat="server" Text='<%#Eval("Score") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="TotalQuestions">
                            <ItemTemplate>
                                <asp:Label ID="Label12" runat="server" Text='<%#Eval("TotalQuestions") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="ansscore">
                            <ItemTemplate>
                                <asp:Label ID="lblansscore" runat="server" Text='<%#Eval("ansscore") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="outofScore">
                            <ItemTemplate>
                                <asp:Label ID="lbloutofScore" runat="server" Text='<%#Eval("outofScore") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="percentage">
                            <ItemTemplate>
                                <asp:Label ID="Label13" runat="server" Text='<%#Eval("percentage") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                </asp:GridView>--%>

                    <div class="row">
            <div  class="col-lg-12 col-md-12 col-sm-12" style="font-size: medium; font-weight:bold">
                  <p>
                              This report is based on the psychology / psychometric principles, in general and follow some established assessment methods. But this is not a comprehensive clinical report or advise validated by a clinical expert.  This report is not a clinical diagnosis or assessment. CLAIRVOYANCE MINDWARE PRIVATE LIMITED does not warrant accuracy, reliability of this report from clinical perspective.

CLAIRVOYANCE MINDWARE PRIVATE LIMITED will not be responsible or liable to anyone.for any loss, damage, cost or expense incurred or arising due to use or reference or inferences made based on  this this report.

This report has been prepared in good faith on the basis of information available and individual responses received at the date of assessment to lead the respondent to discovery of state of mindfulness.
</p>
            </div>
        </div>
            </asp:Panel>

        </div>
    </div>

   
</asp:Content>

