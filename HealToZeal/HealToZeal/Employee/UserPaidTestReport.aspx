﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master/LetMeTalkWithMenu.Master" AutoEventWireup="true" CodeBehind="UserPaidTestReport.aspx.cs" Inherits="HealToZeal.Employee.UserPaidTestReport" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="col-md-11 pad0 col-centered">
        <div class="boardp bdrgrey mb20 whitebg" style="padding-left:0; padding-right:0;">

           <!-- Heading 1 -->
            <div >
                <div class="col-md-12">
              <div class="col-md-5 pad0 col-sm-5">
                  <asp:DropDownList ID="ddlCluster" runat="server" CssClass="form-control f20 w400" AutoPostBack="true" ></asp:DropDownList>
                <%--<select name="dropDown" id="dropDown" class="form-control f20 w400" >
                  <option value="#heading1">asset in performance</option>
                  <option value="#heading2">ability to withstand pressure</option>
                  <option value="#heading3">emotional balance</option>
                </select>--%>
              </div>
              <div class="icon-m10 pull-right"> <a href="#"> <img src="../UIFolder/dist/img/cloud.png" class="pull-left mr10 img-responsive"> </a> <a href="#"> <img src="../UIFolder/dist/img/down.png" class="pull-left img-responsive"> </a> </div>
             </div>
                    <div class="clearfix"></div>
                <div id="divContainer" runat="server">
            <%--  <div class="mt20" style="margin-bottom:20px;">
                <div class="col-md-6 col-sm-6">
                  <div class="col-md-12"> <img src="../UIFolder/dist/img/assets.png" class="img-responsive mt30 mb20"> </div>
                </div>
                <div class="col-md-6 col-sm-6">
                  <div style="margin-top:0 !important;;">
                    <div>
                      <h3 class="w600 mt0 mb0">asset in performance</h3>
                      <div class="col-md-11 pad0 mt0">
                        <h3  style="line-height:35px; font-weight:100;">
                          <ul>
                            <li style="margin-bottom:20px;"> There are many variations of
                              passages of Lorem Ipsum
                              available. </li>
                            <li>But the majority have
                              suffered alteration in some form,
                              by injected humour, or randomised
                              words which don't look even
                              slightly believable. </li>
                          </ul>
                        </h3>
                      </div>
                    </div>
                    <div class="clearfix"></div>
                  </div>
                </div>
                <div class="clearfix"></div>
              </div>--%>
                    </div>
            </div>
            <div class="clearfix"></div>
            
            <%-- <!-- Heading 2 -->
            <div class="boardp mb20 whitebg boxshadow" id="heading2">
              <div class="mb20" style="margin-top:20px;">
                <div class="col-md-6 col-sm-6">
                  <div style="margin-top:0 !important;">
                    <div>
                      <h3 class="w600 mt0 mb0">ability to withstand pressure</h3>
                      <div class="col-md-11 pad0 mt0">
                        <h3 style="line-height:35px; font-weight:100;">
                        <ul>
                          <li style="margin-bottom:20px;"> There are many variations of
                            passages of Lorem Ipsum
                            available. </li>
                          <li>But the majority have
                            suffered alteration in some form,
                            by injected humour, or randomised
                            words which don't look even
                            slightly believable. </li>
                        </ul>
                        </h3>
                      </div>
                    </div>
                    <div class="clearfix"></div>
                  </div>
                </div>
                <div class="col-md-6 col-sm-6">
                  <div class="col-md-12"> <img src="../UIFolder/dist/img/report1.png" class="img-responsive mt30"> </div>
                </div>
                <div class="clearfix"></div>
              </div>
            </div>
            <div class="clearfix"></div>--%>
            
             <!-- Heading 3 -->
            <%--<div class="col-md-12" id="heading3">
              <div class="mt20 mb30">
                <div class="col-md-6 col-sm-6">
                  <div class="col-md-12"> <img src="dist/img/report2.png" class="img-responsive mt30 mb20"> </div>
                </div>
                <div class="col-md-6 col-sm-6">
                  <div style="margin-top:0 !important;">
                    <div>
                      <h3 class="w600 mt0 mb0">emotional balance</h3>
                      <div class="col-md-11 pad0 mt0">
                        <h3  style="line-height:35px; font-weight:100;">
                        <ul>
                          <li style="margin-bottom:20px;"> There are many variations of
                            passages of Lorem Ipsum
                            available. </li>
                          <li>But the majority have
                            suffered alteration in some form,
                            by injected humour, or randomised
                            words which don't look even
                            slightly believable. </li>
                        </ul>
                        </h3>
                      </div>
                    </div>
                    <div class="clearfix"></div>
                  </div>
                </div>
              </div>
            </div>
            <div class="clearfix"></div>--%>
            

            <div class="clearfix"></div>
        </div>
        <div class="clearfix"></div>
    </div>
    <div class="clearfix"></div>

       <div class="clearfix"></div>
    <script>
       
        var dropDownValue = document.getElementById("<%= ddlCluster.ClientID %>");
        dropDownValue.onchange = function () {
            if (this.selectedIndex !== 0) {
                window.location.href = this.value;
            }
        };
</script>
</asp:Content>
