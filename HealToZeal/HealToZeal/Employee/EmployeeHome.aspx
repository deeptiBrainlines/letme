﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master/LetMeTalkWithMenu.Master" AutoEventWireup="true" CodeBehind="EmployeeHome.aspx.cs" Inherits="HealToZeal.Employee.EmployeeHome2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    
    <script type="text/javascript">
        function openModalProfile() {
            $('[id*=myModalProfile]').modal('show');
        }

    </script>
    <script type="text/javascript">        
        function DisplayColorBox(url) {
            $.fn.colorbox({ href: url, iframe: true, scrolling: true, width: '1000px', position: 'Fixed', height: '650px', overlayClose: false });
            //, onClosed: function () { parent.location.reload(false); }
            return false;
        }
        function DisplayColorBox1(url) {

            $.fn.colorbox({ href: url, iframe: true, scrolling: true, width: '600px', position: 'Fixed', height: '450px', overlayClose: false });
            //, onClosed: function () { parent.location.reload(false); }
            return false;
        }
        function checkButton() {
            if (document.form1.CheckBox1.checked == true) {
                alert("Yes,My Data can be shared with my company HR/Admin");
            }
        }
           $(function () {
                $("#datepicker").datepicker({ format: 'mm/dd/yyyy' })
                    .on('changeDate', function (ev) {
                        ValidateDOB();  // triggers the validation test
                        // '$(this)' refers to '$("#datepicker")'
                    });
            });
        function ValidateDOB() {
            var dateString = $("input[id$='txtBirthDate']").val();
            var lblError = $("#lblError");
            var parts = dateString.split("/");
            var dtDOB = new Date(parts[0] + "/" +  parts[1]+ "/" + parts[2]);
            var dtCurrent = new Date();
            lblError.html("Eligibility 18 years ONLY.")
            if (dtCurrent.getFullYear() - dtDOB.getFullYear() < 18) {
                return false;
            }

            if (dtCurrent.getFullYear() - dtDOB.getFullYear() == 18) {

                //CD: 11/06/2018 and DB: 15/07/2000. Will turned 18 on 15/07/2018.
                if (dtCurrent.getMonth() < dtDOB.getMonth()) {
                    return false;
                }
                if (dtCurrent.getMonth() == dtDOB.getMonth()) {
                    //CD: 11/06/2018 and DB: 15/06/2000. Will turned 18 on 15/06/2018.
                    if (dtCurrent.getDate() < dtDOB.getDate()) {
                        return false;
                    }
                }
            }
            lblError.html("");
            return true;
        }

    </script>
    <style type="text/css">
        .ajax__calendar_container {
            z-index: 9999;
            position: static;
            background-color: white;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <div class="row">
        <div class="col-md-11 col-centered">
            <div class="boardp bdr3">
                <asp:Panel ID="pnlEmployeeBasicDetails" runat="server" Visible="False">
                    <div id="DivDisplay" runat="server" visible="false" class="row">
                        <div class="col-lg-12 col-md-12 col-sm-12">
                            <h4></h4>
                            <div class="row hs_how_we_are">
                                <div class="col-lg-12 col-md-12 col-sm-12">
                                    <%--<img src="../images/Let Me Talk2 Me Poster.jpg" />  --%>
                                    <img src="../images/letmetalk2m2new.jpg" />
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div id="divalert" class="alert alert-success" runat="server">
                            <strong>
                                <asp:Label ID="lblMessage" CssClass="validationerror" runat="server"></asp:Label></strong>
                        </div>
                    </div>
                    <div class="row mt10">

                        <div class="col-md-4 col-sm-4">
                            <center><img src="../UIFolder/dist/img/profile-pic.png" class="img-responsive"></center>

                            <div style="display: table;" class="col-centered mt30">

                                <a href="#">
                                    <img src="../UIFolder/dist/img/profileico.png" class="pull-left mr10"></a>

                                <a href="#">
                                    <img src="../UIFolder/dist/img/cameraico.png"></a>

                                <div class="clearfix"></div>



                            </div>

                            <div class="clearfix"></div>
                            <%--<div style="display: table;" class="col-centered mt30 mb20">

                                <asp:Button ID="btnSubmit" runat="server" Text="submit" CssClass="loginbtn pull-right"
                                    OnClick="btnSubmit_Click" OnClientClick="return ValidateDOB();" ValidationGroup="0" />

                                <asp:Button ID="btnReset" runat="server" Text="reset" CssClass="loginbtn pull-right mr6"
                                    OnClick="btnReset_Click" />
                            </div>--%>
                            <div class="clearfix"></div>
                        </div>

                        <div class="col-md-8 col-sm-8">
                            <div>

                                <div class="row">
                                    <%--<div class="col-md-12">
                                        <div class="heading">add profile details</div>
                                    </div>--%>
                                    <div class="col-md-6">
                                        <div class="form-group" style="margin-bottom: 0;">
                                            <h4 class="fwnormal mb mt0">date of birth </h4>
                                            <%-- <asp:TextBox ID="txtBirthDate" runat="server" CssClass="form-control" placeholder="Birthdate"ValidationGroup="0"></asp:TextBox>--%>
                                            <div class="input-group date" id="datepicker">
                                                <asp:TextBox ID="txtBirthDate" runat="server" CssClass="form-control" placeholder="birthdate" onblur="ValidateDOB();"></asp:TextBox>
                                                <span class="input-group-addon textimg"><i class="fa fa-calendar" aria-hidden="true"></i></span>

                                            </div>
                                            <%-- <ajaxToolkit:CalendarExtender ID="CalendarExtender1" runat="server" CssClass="ajax__calendar_container" PopupPosition="BottomRight" Format="MM/dd/yyyy" TargetControlID="txtBirthDate">
                                            </ajaxToolkit:CalendarExtender>--%>
                                            <sup style="line-height: -10px !important; top: 0em;">you can directly enter DOB in MM/DD/YYYY format</sup><br>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ErrorMessage="Select birthdate" ControlToValidate="txtBirthDate" ValidationGroup="0" CssClass="validationerror">*</asp:RequiredFieldValidator>

                                            <%--<asp:CompareValidator ID="CompareValidator1" runat="server" CssClass="validationerror"
                                                ControlToValidate="txtBirthDate" Display="Dynamic" ErrorMessage="* Invalid Date"
                                                Operator="DataTypeCheck" Type="Date"></asp:CompareValidator>
                                            </asp:CompareValidator>--%>
                                           <%-- <asp:Label ID="lblError" ForeColor="Red" runat="server"></asp:Label>--%>
                                            <%--<asp:ValidatorCalloutExtender ID="ValidatorCalloutExtender1"
                                                runat="server" Enabled="True" TargetControlID="CompareValidator1">
                                            </asp:ValidatorCalloutExtender>--%>
                                            <span class="validationerror" id="lblError"></span>

                                        </div>


                                    </div>

                                    <div class="col-md-6">
                                        <div class="form-group" style="margin-bottom: 0;">
                                            <h4 class="fwnormal mb mt0">gender</h4>
                                            <table width="150" class="mb20">
                                                <tbody>
                                                    <tr>
                                                        <td>
                                                            <asp:RadioButtonList ID="rblGender" runat="server" RepeatDirection="Horizontal"
                                                                ValidationGroup="0">
                                                                <asp:ListItem Value="M" Selected="True">&nbsp; male &nbsp;</asp:ListItem>
                                                                <asp:ListItem Value="F">&nbsp;female</asp:ListItem>
                                                            </asp:RadioButtonList>
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                    <div style="clear: both;"></div>
                                    <div class="col-md-6">
                                        <div class="form-group" style="margin-bottom: 10px;">
                                            <h4 class="fwnormal mb mt0">education</h4>
                                            <asp:DropDownList ID="ddlEducation" runat="server" CssClass="form-control "
                                                ValidationGroup="0">
                                                <asp:ListItem>--select education--</asp:ListItem>
                                                <asp:ListItem>post graduate</asp:ListItem>
                                                <asp:ListItem>graduate</asp:ListItem>
                                                <asp:ListItem>doctorate</asp:ListItem>
                                            </asp:DropDownList>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator7" runat="server" ErrorMessage="* select education" CssClass="validationerror" InitialValue="--select education--" ValidationGroup="0" ControlToValidate="ddlEducation">*</asp:RequiredFieldValidator>

                                        </div>

                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group" style="margin-bottom: 10px;">
                                            <h4 class="fwnormal mb mt0">occupation</h4>
                                            <asp:TextBox ID="txtOccupation" runat="server" CssClass="form-control" placeholder="occupation"
                                                ValidationGroup="0"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="enter occupation" ControlToValidate="txtOccupation" CssClass="validationerror" ValidationGroup="0">*</asp:RequiredFieldValidator>

                                        </div>
                                    </div>

                                    <div class="col-md-6">
                                        <div class="form-group" style="margin-bottom: 10px;">
                                            <h4 class="fwnormal mb mt0">experience</h4>
                                            <asp:DropDownList ID="ddlExperience" runat="server" CssClass="form-control"
                                                ValidationGroup="0">
                                                <asp:ListItem>-- select experience --</asp:ListItem>
                                                <asp:ListItem>0-3 years</asp:ListItem>
                                                <asp:ListItem>3-7 years</asp:ListItem>
                                                <asp:ListItem>7-10 years</asp:ListItem>
                                                <asp:ListItem>10-15 years</asp:ListItem>
                                                <asp:ListItem>15 and above</asp:ListItem>
                                            </asp:DropDownList>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator8" runat="server" ErrorMessage="* select experience" ControlToValidate="ddlExperience" CssClass="validationerror" InitialValue="--select experience--" ValidationGroup="0">*</asp:RequiredFieldValidator>

                                        </div>
                                    </div>

                                    <div class="col-md-6">
                                        <div class="form-group" style="margin-bottom: 10px;">
                                            <h4 class="fwnormal mb mt0">position</h4>
                                            <asp:DropDownList ID="ddlPosition" runat="server" CssClass="form-control"
                                                ValidationGroup="0">
                                                <asp:ListItem>--select position--</asp:ListItem>
                                                <asp:ListItem>junior level</asp:ListItem>
                                                <asp:ListItem>middle management</asp:ListItem>
                                                <asp:ListItem>senior management</asp:ListItem>
                                            </asp:DropDownList>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator9" runat="server" ErrorMessage="* select position" ControlToValidate="ddlPosition" CssClass="validationerror" InitialValue="--select position--" ValidationGroup="0">*</asp:RequiredFieldValidator>

                                        </div>
                                    </div>
                                    <div class="col-md-12">

                                        <asp:CheckBox ID="CheckBox1" runat="server" Text="" />
                                        <asp:Label ID="Label2" runat="server" Text="yes,my data can be shared with my company HR/Admin"></asp:Label>


                                        <div style="clear: both;"></div>
                                        <div style="clear: both;"></div>

                                    </div>

                                    <div style="clear: both;"></div>
                                    <div style="display: table;" class="col-centered mt30 mb20">

                                <asp:Button ID="btnSubmit" runat="server" Text="submit" CssClass="loginbtn pull-right"
                                    OnClick="btnSubmit_Click" OnClientClick="return ValidateDOB();" ValidationGroup="0" />

                                <asp:Button ID="btnReset" runat="server" Text="reset" CssClass="loginbtn pull-right mr6"
                                    OnClick="btnReset_Click" />
                            </div>


                                </div>






                            </div>


                        </div>
                    </div>
                </asp:Panel>
                <div class="clearfix"></div>
            </div>
            <div class="clearfix"></div>
        </div>
        <div class="clearfix"></div>
    </div>
    <div class="clearfix"></div>

    <div class="clearfix"></div>
    <div id="myModalProfile" class="modal" role="dialog">
        <div class="modal-dialog modal-md">
            <div class="modal-content">

                <div class="modal-body">
                    <div class="col-md-12 mt30">
                        <h4 class="modal-title">message</h4>
                        <p class="w400">
                            <asp:Label ID="lblMessage1" runat="server"></asp:Label>
                        </p>
                    </div>
                </div>
                <div class="modal-footer">
                    <a href="">
                        <img src="../UIFolder/dist/img/close.png" data-dismiss="modal" class="popicon"></a>
                </div>
            </div>
        </div>
    </div>

</asp:Content>
