﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace HealToZeal.Employee
{
    public partial class Employee1 : System.Web.UI.MasterPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
               if (Session["Pagename"] != null)
                {
                    d.Visible = false;
                    dd.Visible = false;
                }
                else
                {
                    d.Visible = true;
                    dd.Visible = true;
                }
                if (Session["UserName"] != null)
                {
                    //  lblTitle.Text = Session["UserName"].ToString();
                    //Menu1.FindItem("User").Text = Session["UserName"].ToString();
                    lblusername.Text = "Welcome " + Session["UserName"].ToString();
                }
            }
            
        }

        protected void lbLogout_Click(object sender, EventArgs e)
        {
            try
            {
                Session.Clear();
                Session.Abandon();
                Response.Redirect("EmployeeLogin.aspx");
            }
            catch
            {
            }
        }
    }
}