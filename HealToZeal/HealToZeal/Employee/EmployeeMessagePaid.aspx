﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master/LetMeTalk.Master" AutoEventWireup="true" CodeBehind="EmployeeMessagePaid.aspx.cs" Inherits="HealToZeal.Employee.EmployeeMessagePaid" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div style="clear: both;"></div>

    <section class="content">
        <div>
            <div class="col-md-11 col-centered">
                <div class="col-md-12 boardp bdr3">
                    <%--<div class="col-md-12 pad0">
                        <div class="text-left heading" style="padding-top: 0; margin-top: 0;">instructions</div>
                    </div>--%>
                    <br />
                    <p>
                        <strong>Welcome to <b>Let Me Talk To Me: A Health and Personality Insights and Actions Tool.</b> You will be able to gain some useful insights into your own personality and learn about remedial actions in case they are needed. <b>'Let Me Talk2 Me'</b>  will help you in self-discovery. It will lead on a path of self development while avoiding pitfalls of modern complex life and careers.
                        </strong>
                    </p>
                    <p>
                        You are being asked to fill in some information about yourself.  Your personal information will not be shared with anyone else.
                    </p>
                    <p>
                        <br />
                        Once you fill in this small form, you will be presented with a series of questions and answer options and you will be required to <u>answer all of them</u>.  You might find some repetition of the questions but it is deliberate and needed to get the right results out of this important exercise.
                    </p>
                    <p>
                        <br />
                        If you like, you can log out at any time and come back to answer remaining the questions. But your assessment will not be marked “submitted”, until you complete the same. So please make sure you complete the assessment and click the “Submit” button. There would be a small feedback form at the end and that’s it! Your <b>Mindfulness Profile</b> will be displayed as soon as you complete the test. You can always log back in to view the same at leisure anytime later.
                    </p>
                    <p>
                        <br />
                        So here is your journey of self discovery!! Hope you find it very very useful.
                    </p>
                    <p>
                        <strong>Thank you very much!</strong>
                    </p>



                    
                        <asp:Button ID="btnNext" runat="server" class=" loginbtn pull-right " Text="next" OnClick="btnNext_Click" />
                   
                   <div style="clear:both"></div>
                </div>
            </div>
        </div>
    </section>
    <div style="clear:both"></div>
</asp:Content>
