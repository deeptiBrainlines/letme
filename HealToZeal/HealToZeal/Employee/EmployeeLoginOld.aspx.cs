﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DAL;
using System.Data;
using System.Net;
using System.Text;
using System.IO;
using System.Web.Script.Serialization;
using BE;

using Google.Apis.Auth.OAuth2;
using System.Threading;
using Google.Apis.Util.Store;
using Google.Apis.Gmail.v1;
using Google.Apis.Services;
using Org.BouncyCastle.Utilities.IO;
using Newtonsoft.Json;
using System.Net.Http;
using System.Net.Mail;

namespace HealToZeal.Employee
{
    public partial class EmployeeLogin : System.Web.UI.Page
    {
        string CompanyId = "";
        EmployeeDAL objEmployeeDAL = new EmployeeDAL();
       // string EmployeeId = "";
        string googleplus_client_id = "961545322562-359k8koggghm7n5jtgnb4hmvugtomfo3.apps.googleusercontent.com";
        string googleplus_client_secret = "g1B83i1jfVBSn-jxMnqVjkS7";
        string googleplus_redirect_url = "https://localhost:44352/Employee/EmployeeLogin.aspx";
       // string googleplus_redirect_url = "http://apps.brainlines.net/LMT2M_test/Employee/EmployeeLogin.aspx";
        string Parameters;
        Userclass userAllInfo = new Userclass();

        protected void Page_Load(object sender, EventArgs e)
        {

            //string InstanceCompanyRelationId = Request.QueryString["InstanceCompanyRelationId"];
            //string CouponId = Request.QueryString["CouponId"];
            //DataTable compstatus = objEmployeeDAL.GetCompanyIDBatchID(InstanceCompanyRelationId);
            //for (int i = 0; i <= compstatus.Rows.Count; i++)
            //{
            //    Session["CompanyId"] = compstatus.Rows[i]["CompanyID_FK"].ToString();
            //    Session["BatchID"] = compstatus.Rows[i]["BatchID_Fk"].ToString();
            //}
                string de = Encryption.Decrypt("TZNgBLKIkBc6EEofyJUhRw==");
            if ((Session.Contents.Count > 0) && (Session["loginWith"] != null) && (Session["loginWith"].ToString() == "google"))
            {
                try
                {
                    

                    var url = Request.Url.Query;
                    if (url != "")
                    {
                        string queryString = url.ToString();
                        char[] delimiterChars = { '=' };
                        string[] words = queryString.Split(delimiterChars);
                        string code = words[1];

                        if (code != null)
                        {
                            //get the access token 
                            HttpWebRequest webRequest = (HttpWebRequest)WebRequest.Create("https://accounts.google.com/o/oauth2/token");
                            webRequest.Method = "POST";
                            Parameters = "code=" + code + "&client_id=" + googleplus_client_id + "&client_secret=" + googleplus_client_secret + "&redirect_uri=" + googleplus_redirect_url + "&grant_type=authorization_code";
                            byte[] byteArray = Encoding.UTF8.GetBytes(Parameters);
                            webRequest.ContentType = "application/x-www-form-urlencoded";
                            webRequest.ContentLength = byteArray.Length;
                            Stream postStream = webRequest.GetRequestStream();
                            // Add the post data to the web request
                            postStream.Write(byteArray, 0, byteArray.Length);
                            postStream.Close();

                            WebResponse response = webRequest.GetResponse();
                            postStream = response.GetResponseStream();
                            StreamReader reader = new StreamReader(postStream);
                            string responseFromServer = reader.ReadToEnd();

                            GooglePlusAccessToken serStatus = JsonConvert.DeserializeObject<GooglePlusAccessToken>(responseFromServer);

                            if (serStatus != null)
                            {
                                string accessToken = string.Empty;
                                accessToken = serStatus.access_token;

                                if (!string.IsNullOrEmpty(accessToken))
                                {
                                    // This is where you want to add the code if login is successful.
                                    getgoogleplususerdataSer(accessToken);
                                }
                            }

                        }
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message, ex);
                   // Response.Redirect("index.aspx");
                }
            }

            //if (Request.QueryString["error"] == "access_denied")
            //{
            //    ClientScript.RegisterClientScriptBlock(this.GetType(), "alert", "alert('Access denied.')", true);
            //}
        

        }
      
        public class Userclass
        {
            public string id
            {
                get;
                set;
            }
            public string name
            {
                get;
                set;
            }
            public string given_name
            {
                get;
                set;
            }
            public string family_name
            {
                get;
                set;
            }
            public string link
            {
                get;
                set;
            }
            public string picture
            {
                get;
                set;
            }
            public string gender
            {
                get;
                set;
            }
            public string locale
            {
                get;
                set;
            }
            public string first_name
            {
                get;
                set;
            }
            public string last_name
            {
                get;
                set;
            }
            public String Emails { get; set; }

        }

        public class Email
        {
            public string Value { get; set; }
            public string Type { get; set; }
        }

        public void GetToken(string code)
        {
            //string poststring = "grant_type=authorization_code&code=" + code + "&client_id=" + clientid + "&client_secret=" + clientsecret + "&redirect_uri=" + redirection_url + "";
            //var request = (HttpWebRequest)WebRequest.Create(url);
            //request.ContentType = "application/x-www-form-urlencoded";
            //request.Method = "POST";
            //UTF8Encoding utfenc = new UTF8Encoding();
            //byte[] bytes = utfenc.GetBytes(poststring);
            //Stream outputstream = null;
            //try
            //{
            //    request.ContentLength = bytes.Length;
            //    outputstream = request.GetRequestStream();
            //    outputstream.Write(bytes, 0, bytes.Length);
            //}
            //catch (Exception ex) { }
            //try
            //{
            //    var response = (HttpWebResponse)request.GetResponse();
            //    var streamReader = new StreamReader(response.GetResponseStream());
            //    string responseFromServer = streamReader.ReadToEnd();
            //    JavaScriptSerializer js = new JavaScriptSerializer();
            //    Tokenclass obj = js.Deserialize<Tokenclass>(responseFromServer);
            //    GetuserProfile(obj.access_token);
            //}
            //catch (Exception ex)
            //{

            //}
        }


        protected void Login(object sender, EventArgs e)
        {
            // GoogleConnect.Authorize("profile", "email");
        }


        public void GetuserProfile(string accesstoken)
        {
            // string url = "https://www.googleapis.com/oauth2/v2/userinfo?alt=json&access_token=" + accesstoken + "";
            ////string url1 = "https://www.googleapis.com/oauth2/v2/userinfo?alt=json&access_token=" + accesstoken + "";

          
          //  imgprofile.ImageUrl = userAllInfo.picture;
          //  lblid.Text = userAllInfo.Emails;
          //  lblgender.Text = userAllInfo.gender;
          //  lbllocale.Text = userAllInfo.locale;
          //  lblname.Text = userAllInfo.name;
          ////  lblid.Text = userinfo.Emails.Find(email => email.Type == "account").Value;
          //  hylprofile.NavigateUrl = userAllInfo.link;

            String str = userAllInfo.name;

            char[] spearator = { ',', ' ' };

            // using the method 
            String[] strlist = str.Split(spearator,
               StringSplitOptions.RemoveEmptyEntries);

            foreach (String s in strlist)
            {
                // string Firstname = s;
                //string Lastname = s;
            }

            try
            {

                //  Guid EmployeeId_PK;
                //  Guid.TryParse("EmployeeId_PK", out EmployeeId_PK);
                //insert
                string password = Encryption.CreatePassword();
                string userName = Encryption.CreateUsername();
                string pass = Encryption.Encrypt(password);
                string verificationcode = Guid.NewGuid().ToString();
                EmployeeBE objEmployeeBE = new EmployeeBE();

                CompanyId = "987109F8-B639-4FE0-85F3-0FCFEFF63B5E";
                objEmployeeBE.propUserName = userName;
                objEmployeeBE.propPassword = pass;
                objEmployeeBE.propCompanyId_FK = CompanyId;
                objEmployeeBE.propFirstName = userAllInfo.given_name;
                objEmployeeBE.propLastName = userAllInfo.family_name;
                objEmployeeBE.propEmailId = userAllInfo.Emails;
                objEmployeeBE.PropBatchID = "372BA8D3-F52A-4EDA-9CA2-B5968010A96C";
                //objEmployeeBE.verificationcode = verificationcode;
                // Session["verificationcode"] = verificationcode;
                //  objEmployeeBE.propOccupation = ddloccupation.SelectedItem.Text;
                //objEmployeeBE.propExperienceInYears = txtexp.Text;

                int Status = 0;

                Status = objEmployeeDAL.EmployeesRegistrationGmailInsert(objEmployeeBE);

                System.Net.Mail.MailMessage mail = new System.Net.Mail.MailMessage();


                mail.To.Add(userAllInfo.Emails);
                mail.CC.Add("contact@letmetalk2.me");
                mail.Bcc.Add("priyamvada.bavadekar@letmetalk2.me");
                // mail.From = "contact@letmetalk2.me";
                // mail.From = new MailAddress("contact@letmetalk2.me", "Let me talk to me", System.Text.Encoding.UTF8);
                mail.Subject = "Welcome to the Health and Personality Insights and Actions Tool.";
                string getDt = DateTime.Now.Date.AddDays(7).ToString("MM/dd/yyyy");

                //mail.Body = "<html><body><font color=\"black\">Dear " + FirstName + " " + LastName + ",<br/><br/>Welcome to <b> Let Me Talk2 Me: A tool to quickly and discreetly assess your mindfulness.</b> You will be able to gain some useful insights into your own personality and learn about remedial actions in case they are needed. <b>'Let Me Talk2 Me'</b> will help you in self-discovery. It will lead on a path of self reflections while avoiding pitfalls of modern complex life and careers. <br/><br/>It is recommended that you use this tool periodically to monitor yourself and be aware of your health and personality parameters. <br/><br/> Please use the link below to login <br/><br/><a href='http://letmetalk2.me.hazel.arvixe.com/Employee/EmployeeLogin.aspx?first=1'> http://letmetalk2.me.hazel.arvixe.com/Employee/EmployeeLogin.aspx </a><br/>Your login details are as follows:<br/>UserId:<b>" + UserName + "</b><br/>Password:<b>" + Password + "</b><br/><br/>If you would like to take a break during assessment, please make sure you log out from the system or close the browser window for security purposes. Please do not leave the browser idle. You can always log back in whenever you like.<br/><br/>Please ensure that you complete this assessment on or before " + getDt + ". To get a maximum benefit, please repeat this exercise every 3 months and study the differentials.<br/> You will be notified for the same when the next assessment is due.<br/><br/>If you wish to join the ecosystem of mindfulness, please pre-register with us.<a href='http://letmetalk2.me/pre-register'>http://letmetalk2.me/pre-register</a></br>Thank You.<br/><br/></font></body></html>";

                mail.Body = "<html><body><font color=\"black\">Dear " + userAllInfo.given_name + " " + userAllInfo.family_name + ",<br/><br/>Welcome to <b> Let Me Talk2 Me: A tool to quickly and discreetly assess your mindfulness.</b> You will be able to gain some useful insights into your own personality and learn about remedial actions in case they are needed. <b>'Let Me Talk2 Me'</b> will help you in self-discovery. It will lead on a path of self reflections while avoiding pitfalls of modern complex life and careers. <br/><br/>It is recommended that you use this tool periodically to monitor yourself and be aware of your health and personality parameters. <br/><br/> Please use the link below to login <br/><br/><a href='http://localhost:44352/Employee/EmployeeLogin.aspx?first=1'> http://localhost:44352/Employee/EmployeeLogin.aspx </a><br/>Your login details are as follows:<br/>UserId:<b>" + UserName + "</b><br/>Password:<b>" + Password + "</b><br/><br/>If you would like to take a break during assessment, please make sure you log out from the system or close the browser window for security purposes. Please do not leave the browser idle. You can always log back in whenever you like.<br/><br/>Please ensure that you complete this assessment on or before " + getDt + ". To get a maximum benefit, please repeat this exercise every 3 months and study the differentials.<br/> You will be notified for the same when the next assessment is due.<br/><br/>If you wish to join the ecosystem of mindfulness, please pre-register with us.<a href='http://letmetalk2.me/pre-register'>http://letmetalk2.me/pre-register</a></br>Thank You.<br/><br/></font></body></html>";

                mail.IsBodyHtml = true;

                mail.Priority = System.Net.Mail.MailPriority.High;
                SmtpClient client = new SmtpClient();
                // client.Credentials = new System.Net.NetworkCredential("contact@letmetalk2.me", "contact123");
                //  client.Port = 587;
                // client.Host = "dallas137.arvixeshared.com";
                client.EnableSsl = false;
                client.Send(mail);



                Response.Redirect("EmployeeLogin.aspx");
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('Registration successfull')", true);

                //lblMessage.Text = "Employee added successfully...";


            }
            catch (Exception ex)
            {
                ex.InnerException.ToString();
            }
        }


        protected void btnLogin_Click(object sender, EventArgs e)
        {
            try
            {
                EmployeeDAL objEmployeeDAL = new EmployeeDAL();

                DataTable DtEmployeeDetails = objEmployeeDAL.EmployeesLogin(UserName.Value, Encryption.Encrypt(Password.Value));
                string pass = Encryption.Decrypt(Password.Value);
                string username = Encryption.Decrypt(UserName.Value);
                if (DtEmployeeDetails != null && DtEmployeeDetails.Rows.Count > 0)
                {
                    Session["UserName"] = UserName.Value;
                    //Session["DtCompanyDetails"] = DtCompanyDetails;
                    Session["EmployeeId"] = DtEmployeeDetails.Rows[0]["EmployeeId_PK"].ToString();
                    Session["EmployeeCompanyId"] = DtEmployeeDetails.Rows[0]["EmployeeCompanyId"].ToString();
                    Session["BatchID"] = DtEmployeeDetails.Rows[0]["BatchID_Fk"].ToString();
                    //  Session["CompanyId"] = DtEmployeeDetails.Rows[0]["CompanyId_FK"].ToString();

                    if (Request.QueryString["first"] != null)
                    {
                        // Response.Redirect("EmployeeHome.aspx?first=1");
                        if (DtEmployeeDetails.Rows[0]["Age"].ToString() != "" && DtEmployeeDetails.Rows[0]["Occupation"].ToString() != "" && DtEmployeeDetails.Rows[0]["Gender"].ToString() != "" && DtEmployeeDetails.Rows[0]["ExperienceInYears"].ToString() != "" && DtEmployeeDetails.Rows[0]["Position"].ToString() != "" && DtEmployeeDetails.Rows[0]["Qualification"].ToString() != "")
                        {
                            EmployeeAssessmentsHistoryDAL objAssessmentHistory = new EmployeeAssessmentsHistoryDAL();
                            DataTable DtAssessmentHistory = objAssessmentHistory.EmployeeAssessmentsHistoryAssessmentSubmited(Session["EmployeeId"].ToString(), "");

                            if (DtAssessmentHistory != null && DtAssessmentHistory.Rows.Count > 0)
                            {
                                Response.Redirect("EmployeeAssessments.aspx");
                            }
                            else
                            {
                                //   Response.Redirect("Assessment.aspx");
                                Response.Redirect("EmployeeAssessments.aspx");
                                // Response.Redirect("EmployeeMessage.aspx?first=1");
                                //Response.Redirect("Assessment.aspx");
                            }
                        }
                        else
                        {
                            Response.Redirect("EmployeeMessage.aspx?first=1");
                        }
                        // Response.Redirect("EmployeeMessage.aspx?first=1");
                    }
                    else
                    {
                        if (DtEmployeeDetails.Rows[0]["Age"].ToString() != "" && DtEmployeeDetails.Rows[0]["Occupation"].ToString() != "" && DtEmployeeDetails.Rows[0]["Gender"].ToString() != "" && DtEmployeeDetails.Rows[0]["ExperienceInYears"].ToString() != "" && DtEmployeeDetails.Rows[0]["Position"].ToString() != "" && DtEmployeeDetails.Rows[0]["Qualification"].ToString() != "")
                        {
                            EmployeeAssessmentsHistoryDAL objAssessmentHistory = new EmployeeAssessmentsHistoryDAL();
                            DataTable DtAssessmentHistory = objAssessmentHistory.EmployeeAssessmentsHistoryAssessmentSubmited(Session["EmployeeId"].ToString(), "");
                            if (DtAssessmentHistory != null && DtAssessmentHistory.Rows.Count > 0)
                            {
                                Response.Redirect("EmployeeAssessments.aspx");
                            }
                            else
                            {
                                //  Response.Redirect("Assessment.aspx");
                                Response.Redirect("EmployeeAssessments.aspx");
                            }
                        }
                        else
                        {
                            Response.Redirect("EmployeeMessage.aspx?first=1");
                        }

                    }
                }
                else
                {
                    message.InnerText = "Invalid username/password...";
                }
            }
            catch (Exception ex)
            {
                message.InnerText = ex.Message;
                ex.InnerException.ToString();
            }

        }

        protected void ImgBtnLogin_Click(object sender, ImageClickEventArgs e)
        {
            try
            {
                EmployeeDAL objEmployeeDAL = new EmployeeDAL();

                DataTable DtEmployeeDetails = objEmployeeDAL.EmployeesLogin(UserName.Value, Encryption.Encrypt(Password.Value));

                if (DtEmployeeDetails != null && DtEmployeeDetails.Rows.Count > 0)
                {
                    Session["UserName"] = UserName.Value;
                    //Session["DtCompanyDetails"] = DtCompanyDetails;
                    Session["EmployeeId"] = DtEmployeeDetails.Rows[0]["EmployeeId_PK"].ToString();
                    Session["EmployeeCompanyId"] = DtEmployeeDetails.Rows[0]["EmployeeCompanyId"].ToString();

                    if (Request.QueryString["first"] != null)
                    {
                        EmployeeAssessmentsHistoryDAL objAssessmentHistory = new EmployeeAssessmentsHistoryDAL();
                        DataTable DtAssessmentHistory = objAssessmentHistory.EmployeeAssessmentsHistoryAssessmentSubmited(Session["EmployeeId"].ToString(), "");

                        if (DtAssessmentHistory != null && DtAssessmentHistory.Rows.Count > 0)
                        {
                            Response.Redirect("UserResult.aspx");
                        }
                        else
                        {
                            Response.Redirect("Assessment.aspx");
                        }

                        Response.Redirect("EmployeeMessage.aspx?first=1");

                    }
                    else
                    {
                        //   Response.Redirect("EmployeeHome.aspx?first=1");
                        EmployeeAssessmentsHistoryDAL objAssessmentHistory = new EmployeeAssessmentsHistoryDAL();
                        DataTable DtAssessmentHistory = objAssessmentHistory.EmployeeAssessmentsHistoryAssessmentSubmited(Session["EmployeeId"].ToString(), "");
                        if (DtAssessmentHistory != null && DtAssessmentHistory.Rows.Count > 0)
                        {
                            Response.Redirect("UserResult.aspx");
                        }
                        else
                        {
                            Response.Redirect("Assessment.aspx");
                        }
                        // Response.Redirect("EmployeeMessage.aspx", false);
                    }
                }
                else
                {
                    message.InnerText = "Invalid username/password...";
                }
            }
            catch (Exception ex)
            {
                message.InnerText = ex.Message;
            }

        }


        static string[] Scopes = { GmailService.Scope.GmailReadonly };
        static string ApplicationName = "TestGmail";

        //public void GetData()
        //{
        //    UserCredential credential;
        //    var path = @"C:\Users\Shree\Downloads\credentials.json";
        //    using (var stream =
        //        new FileStream(path, FileMode.Open, FileAccess.Read))
        //    {
        //        // The file token.json stores the user's access and refresh tokens, and is created
        //        // automatically when the authorization flow completes for the first time.
        //        // string credPath = @"C:\Users\Shree\Downloads\credentials1.json";
        //        string credPath = System.Environment.GetFolderPath(
        //             System.Environment.SpecialFolder.Personal);
        //        credential = GoogleWebAuthorizationBroker.AuthorizeAsync(
        //            GoogleClientSecrets.Load(stream).Secrets,
        //            Scopes,
        //            "user",
        //            CancellationToken.None,
        //            new FileDataStore(credPath, true)).Result;
        //        Console.WriteLine("Credential file saved to: " + credPath);
        //    }

        //    // Create Gmail API service.
        //    var service = new GmailService(new BaseClientService.Initializer()
        //    {
        //        HttpClientInitializer = credential,
        //        ApplicationName = ApplicationName,
        //    });

        //    // Define parameters of request.
        //    UsersResource.LabelsResource.ListRequest request = service.Users.Labels.List("me");

        //    var strList = request.Execute().ToString();
        //    // IList<Label> labels = request
        //    //Console.WriteLine("Labels:");
        //    //if (labels != null && labels.Count > 0)
        //    //{
        //    //    foreach (var labelItem in labels)
        //    //    {
        //    //        Console.WriteLine("{0}", labelItem.Name);
        //    //    }
        //    //}
        //    //else
        //    //{
        //    //    Console.WriteLine("No labels found.");
        //    //}
        //    //Console.Read();
        //}


        private async void getgoogleplususerdataSer(string access_token)
        {
            try
            {
                HttpClient client = new HttpClient();
                var urlProfile = "https://www.googleapis.com/oauth2/v1/userinfo?alt=json&access_token=" + access_token;

                client.CancelPendingRequests();
                HttpResponseMessage output = await client.GetAsync(urlProfile);

                if (output.IsSuccessStatusCode)
                {
                    string outputData = await output.Content.ReadAsStringAsync();
                    GoogleUserOutputData serStatus = JsonConvert.DeserializeObject<GoogleUserOutputData>(outputData);

                    if (serStatus != null)
                    {
                        // You will get the user information here.
                        userAllInfo.id = serStatus.id;
                        userAllInfo.first_name = serStatus.given_name;
                        userAllInfo.last_name = serStatus.family_name;
                        userAllInfo.Emails = serStatus.email;
                        userAllInfo.locale = serStatus.locale;
                        userAllInfo.picture = serStatus.picture;
                        userAllInfo.name = serStatus.name;

                        String str = userAllInfo.name;

                        char[] spearator = { ',', ' ' };

                        // using the method 
                        String[] strlist = str.Split(spearator,
                           StringSplitOptions.RemoveEmptyEntries);

                        foreach (String s in strlist)
                        {
                            // string Firstname = s;
                            //string Lastname = s;
                        }

                        try
                        {

                            //  Guid EmployeeId_PK;
                            //  Guid.TryParse("EmployeeId_PK", out EmployeeId_PK);
                            //insert
                            string Password = Encryption.CreatePassword();
                            string UserName = userAllInfo.Emails;
                            string pass = Encryption.Encrypt(Password);
                            string verificationcode = Guid.NewGuid().ToString();
                            EmployeeBE objEmployeeBE = new EmployeeBE();

                            // CompanyId = "987109F8-B639-4FE0-85F3-0FCFEFF63B5E";
                            CompanyId= Session["CompanyId"].ToString() ;
                            string BatchID =Session["BatchID"].ToString() ;
                            objEmployeeBE.propUserName = UserName;
                            objEmployeeBE.propPassword = pass;
                            objEmployeeBE.propCompanyId_FK = Session["CompanyId"].ToString();
                            objEmployeeBE.propFirstName = userAllInfo.first_name;
                            objEmployeeBE.propLastName = userAllInfo.last_name;
                            objEmployeeBE.propEmailId = userAllInfo.Emails;
                            //objEmployeeBE.PropBatchID = "372BA8D3-F52A-4EDA-9CA2-B5968010A96C";
                            objEmployeeBE.PropBatchID = Session["BatchID"].ToString();
                            //objEmployeeBE.verificationcode = verificationcode;
                            // Session["verificationcode"] = verificationcode;
                            //  objEmployeeBE.propOccupation = ddloccupation.SelectedItem.Text;
                            //objEmployeeBE.propExperienceInYears = txtexp.Text;
                            Session["Email"] = userAllInfo.Emails;

                          
                            int Status = 0;

                            Status = objEmployeeDAL.EmployeesRegistrationGmailInsert(objEmployeeBE);

                            if (Status > 0)
                            {
                               // Response.Write(@"<script language='javascript'>alert('The following errors have occurred: \n" + "You have Registerd successfully, Please check Email for Assessment Password " + " .');</script>");
                                //System.Net.Mail.MailMessage mail = new System.Net.Mail.MailMessage();
                                //mail.To.Add(userAllInfo.Emails);
                                //mail.CC.Add("contact@letmetalk2.me");
                                //mail.Bcc.Add("priyamvada.bavadekar@letmetalk2.me");
                                //// mail.From = "contact@letmetalk2.me";
                                //// mail.From = new MailAddress("contact@letmetalk2.me", "Let me talk to me", System.Text.Encoding.UTF8);
                                //mail.Subject = "Welcome to the Health and Personality Insights and Actions Tool.";
                                //string getDt = DateTime.Now.Date.AddDays(7).ToString("MM/dd/yyyy");

                                ////mail.Body = "<html><body><font color=\"black\">Dear " + FirstName + " " + LastName + ",<br/><br/>Welcome to <b> Let Me Talk2 Me: A tool to quickly and discreetly assess your mindfulness.</b> You will be able to gain some useful insights into your own personality and learn about remedial actions in case they are needed. <b>'Let Me Talk2 Me'</b> will help you in self-discovery. It will lead on a path of self reflections while avoiding pitfalls of modern complex life and careers. <br/><br/>It is recommended that you use this tool periodically to monitor yourself and be aware of your health and personality parameters. <br/><br/> Please use the link below to login <br/><br/><a href='http://letmetalk2.me.hazel.arvixe.com/Employee/EmployeeLogin.aspx?first=1'> http://letmetalk2.me.hazel.arvixe.com/Employee/EmployeeLogin.aspx </a><br/>Your login details are as follows:<br/>UserId:<b>" + UserName + "</b><br/>Password:<b>" + Password + "</b><br/><br/>If you would like to take a break during assessment, please make sure you log out from the system or close the browser window for security purposes. Please do not leave the browser idle. You can always log back in whenever you like.<br/><br/>Please ensure that you complete this assessment on or before " + getDt + ". To get a maximum benefit, please repeat this exercise every 3 months and study the differentials.<br/> You will be notified for the same when the next assessment is due.<br/><br/>If you wish to join the ecosystem of mindfulness, please pre-register with us.<a href='http://letmetalk2.me/pre-register'>http://letmetalk2.me/pre-register</a></br>Thank You.<br/><br/></font></body></html>";

                                //mail.Body = "<html><body><font color=\"black\">Dear " + userAllInfo.given_name + " " + userAllInfo.family_name + ",<br/><br/>Welcome to <b> Let Me Talk2 Me: A tool to quickly and discreetly assess your mindfulness.</b> You will be able to gain some useful insights into your own personality and learn about remedial actions in case they are needed. <b>'Let Me Talk2 Me'</b> will help you in self-discovery. It will lead on a path of self reflections while avoiding pitfalls of modern complex life and careers. <br/><br/>It is recommended that you use this tool periodically to monitor yourself and be aware of your health and personality parameters. <br/><br/> Please use the link below to login <br/><br/><a href='http://apps.brainlines.net/LMT2M_test/Employee/EmployeeLogin.aspx?first=1'> http://apps.brainlines.net/LMT2M_test/Employee/EmployeeLogin.aspx </a><br/>Your login details are as follows:<br/>UserId:<b>" + UserName + "</b><br/>Password:<b>" + Password + "</b><br/><br/>If you would like to take a break during assessment, please make sure you log out from the system or close the browser window for security purposes. Please do not leave the browser idle. You can always log back in whenever you like.<br/><br/>Please ensure that you complete this assessment on or before " + getDt + ". To get a maximum benefit, please repeat this exercise every 3 months and study the differentials.<br/> You will be notified for the same when the next assessment is due.<br/><br/>If you wish to join the ecosystem of mindfulness, please pre-register with us.<a href='http://letmetalk2.me/pre-register'>http://letmetalk2.me/pre-register</a></br>Thank You.<br/><br/></font></body></html>";

                                //mail.IsBodyHtml = true;

                                //mail.Priority = System.Net.Mail.MailPriority.High;
                                //SmtpClient client1 = new SmtpClient();
                                //// client.Credentials = new System.Net.NetworkCredential("contact@letmetalk2.me", "contact123");
                                ////  client.Port = 587;
                                //// client.Host = "dallas137.arvixeshared.com";
                                //client1.EnableSsl = true;
                                //client1.Send(mail);



                                //Response.Redirect("EmployeeLogin.aspx");
                                //string message = "Please Pay the money to get passcode for Assessment";
                                //string script = "window.onload = function(){ alert('";
                                //script += message;
                                //script += "')};";
                                //ClientScript.RegisterStartupScript(this.GetType(), "SuccessMessage", script, true);

                            }
                            else
                            {
                                Response.Write(@"<script language='javascript'>alert('Alert: \n" + "EmailID Already Exists,Plese provide another Email ID" + " .');</script>");
                              //  Response.Redirect("EmployeeLogin.aspx");
                            }


                            DataTable dt = objEmployeeDAL.GetEmployeeByEmail(objEmployeeBE);
                            Session["CompanyId"] = dt.Rows[0]["CompanyID_FK"].ToString();
                            Session["EmployeeID"] = dt.Rows[0]["EmployeeId_PK"].ToString();
                            Response.Redirect("PaymentGateway.aspx");
                            //imgprofile.ImageUrl = userAllInfo.picture;
                            //lblid.Text = userAllInfo.Emails;
                            //lblgender.Text = userAllInfo.gender;
                            //lbllocale.Text = userAllInfo.locale;
                            //lblname.Text = userAllInfo.name;
                            ////  lblid.Text = userinfo.Emails.Find(email => email.Type == "account").Value;
                            //hylprofile.NavigateUrl = userAllInfo.link;


                        }
                        catch(Exception e)
                        {
                            e.InnerException.ToString();
                        }
                        }
                }
            }
            catch (Exception )
            {
                //catching the exception
            }
        }



    }
}
      


