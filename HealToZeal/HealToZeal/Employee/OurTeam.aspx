﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Employee/Employee.Master" AutoEventWireup="true" CodeBehind="OurTeam.aspx.cs" Inherits="HealToZeal.Employee.OurTeam" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="EmployeeColumn" runat="server">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12">
                <h4></h4>
                <div class="row hs_how_we_are" style="font-size: medium">
                    <div class="col-lg-12 col-md-12 col-sm-12">
                        <div id="divalert" class="alert alert-dismissable" runat="server">

                            <p>
                                <h4><strong><u>LetMeTalk2Me Team</u></strong> </h4>
                                <br />
                                <b>Priyamvada Bavadekar </b>- CEO
                                <br />
                                 <p>
                                    ¤  Over 15 years of valuable experience (including 9 years of entrepreneurship) in IT Consultancy Services, Project Management and Client Relationship Management
                                </p>
                                <p>
                                    ¤  An enterprising leader with skills in mentoring and motivating individuals towards maximising productivity as well as in forming cohesive team environments
                                </p>
                                <br />
                                <b>Hemant Karandikar </b>– Advisor and Mentor
                                <br />
                                <p>
                                    ¤  Coaching to CEOs & management teams through a combination of onsite and web based support through Learning Leadership set up for work based workouts and coaching
                                </p>
                                <br />
                                <b>Vandana Kulkarni </b>– Counselor 20 years of experience
                                 <br />
                                 <p>¤  Associated with IT companies as a Counselor </p>

                                 <p>¤  Workshops and seminars facilitator </p>   

                                <br />
                                <b>Kavitagauri Joshi</b>– Clinical Psychologist
                                <br />
                                <p>
                                    ¤  Independent  counselor and assessment expert for various age groups
                                </p>
                               
                                <p>
                                    ¤  Masters in Clinical Psychology; working with a Mental Health Institute Workshops and seminars facilitator for a plethora of mental health related issues
                                </p>
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</asp:Content>
