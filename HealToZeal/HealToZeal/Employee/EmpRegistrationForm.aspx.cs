﻿using BE;
using DAL;
using Facebook;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace HealToZeal.Employee
{
    public partial class EmpRegistrationForm1 : System.Web.UI.Page
    {
        log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        EmployeeDAL objEmployeeDAL = new EmployeeDAL();
        string googleplus_client_id = string.Empty;
        string googleplus_client_secret = string.Empty;
        string googleplus_redirect_url = string.Empty;
        //string googleplus_client_id = "961545322562-359k8koggghm7n5jtgnb4hmvugtomfo3.apps.googleusercontent.com";
        //string googleplus_client_secret = "g1B83i1jfVBSn-jxMnqVjkS7";
        // string googleplus_redirect_url = "https://localhost:44352/Employee/EmployeeLogin.aspx";
        // string googleplus_redirect_url = "http://apps.brainlines.net/LMT2M_test/Employee/EmployeeLogin.aspx";

        protected void Page_Load(object sender, EventArgs e)
        {

            if (!IsPostBack)
            {
                if (Request.QueryString.Count > 0 && Request.QueryString["InstanceCompanyRelationId"].ToString() != "" && Request.QueryString["CouponId"].ToString() != "")
                {
                    string InstanceCompanyRelationId = Request.QueryString["InstanceCompanyRelationId"];
                    string CouponId = Request.QueryString["CouponId"];

                    // lblMessage.Text = "welcome" + CompanyId + " " + CouponId;

                    // CompanyId = "987109F8-B639-4FE0-85F3-0FCFEFF63B5E";

                    DataTable compstatus = objEmployeeDAL.GetCompanyIDBatchID(InstanceCompanyRelationId);

                    if (compstatus != null && compstatus.Rows.Count > 0)
                    {
                        Session["CompanyId"] = compstatus.Rows[0]["CompanyId_FK"].ToString();
                        Session["BatchID"] = compstatus.Rows[0]["BatchID_Fk"].ToString();
                    }
                }
                //CompanyId = "987109F8-B639-4FE0-85F3-0FCFEFF63B5E";

                //CompanyId = Request.QueryString["CompanyId"];
                //reset();

            }

            if (Request.QueryString["code"] != null)
            {
                string accessCode = Request.QueryString["code"].ToString();

                var fb = new FacebookClient();

                // throws OAuthException 
                dynamic result = fb.Post("oauth/access_token", new
                {

                    client_id = "your_app_id",

                    client_secret = "your_app_secret",

                    redirect_uri = "http://localhost:8779/FacebookDemo.aspx",

                    code = accessCode

                });

                var accessToken = result.access_token;
            }
        }

        protected void btnsubmit_Click(object sender, EventArgs e)
        {
            try
            {
                if (chkTandD.Checked == true)
                {
                    InsertUpdateEmployee();
                }
                else
                {
                    lblChkMsg.Text = "* please check accept T&C";
                }

            }
            catch (Exception ex1)
            {

                lblMessage1.Text = ex1.InnerException.ToString();
                ClientScript.RegisterStartupScript(this.GetType(), "Pop", "openModal();", true);
            }

        }

        private void InsertUpdateEmployee()
        {
            try
            {
                //  Guid EmployeeId_PK;
                //  Guid.TryParse("EmployeeId_PK", out EmployeeId_PK);
                //insert
                string password = Encryption.CreatePassword();
                string userName = Encryption.CreateUsername();
                string pass = Encryption.Encrypt(password);
                string verificationcode = Guid.NewGuid().ToString();
                EmployeeBE objEmployeeBE = new EmployeeBE();

                //  CompanyId = "987109F8-B639-4FE0-85F3-0FCFEFF63B5E";
                objEmployeeBE.propUserName = userName;
                objEmployeeBE.propPassword = pass;
                objEmployeeBE.propCompanyId_FK = Convert.ToString(Session["CompanyId"].ToString());
                objEmployeeBE.propFirstName = txtfirstname.Text;
                objEmployeeBE.propLastName = txtlastname.Text;
                objEmployeeBE.propEmailId = txtemail.Text;
                objEmployeeBE.propMobileNo = txtphoneno.Text;
                objEmployeeBE.PropBatchID = Convert.ToString(Session["BatchID"].ToString());
                objEmployeeBE.Verificationcode = verificationcode;
                // objEmployeeBE.propEmployeeCompanyId =Convert.ToInt16( txtEmpCompID.Text);
                Session["verificationcode"] = verificationcode;
                Session["UserFullName"] = txtfirstname.Text.ToString().Trim()+" "+txtlastname.Text.ToString().Trim();
                //  objEmployeeBE.propOccupation = ddloccupation.SelectedItem.Text;
                //objEmployeeBE.propExperienceInYears = txtexp.Text;
                if (chkTandD.Checked == true)
                {

                    objEmployeeBE.PropTandD = "True";
                }
                else
                {
                    objEmployeeBE.PropTandD = "False";
                }
                int Status = 0;

                Status = objEmployeeDAL.EmployeesRegistrationInsert(objEmployeeBE);
                log.Info(Status.ToString());
                if (Status > 0)
                {
                    // ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('You have ARegisterd successfully, Please check Email for Email Activation)", true);
                    lblMessage1.Text = "you have registerd successfully, please check email for activation";
                    ClientScript.RegisterStartupScript(this.GetType(), "Pop", "openModal();", true);
                    //  string jquery = "ShowMsg();";
                    //ClientScript.RegisterStartupScript(typeof(Page), "a key","<script type=\"text/javascript\">" + jquery + "</script>");
                    //ClientScript.RegisterStartupScript(GetType(), "Javascript", "javascript: ShowMsg(); ", true);

                    SendActivationEmailNewTemplate();
                    Response.Redirect("Emailverificationsuccessmessage.aspx",false);
                }
                else
                {
                    lblMessage1.Text = "email id already exists, please provide another email id";
                    ClientScript.RegisterStartupScript(this.GetType(), "Pop", "openModal();", true);
                    // string jquery = "ShowMsg();";
                    // ClientScript.RegisterStartupScript(typeof(Page), "a key", "<script type=\"text/javascript\">" + jquery + "</script>");
                    // ClientScript.RegisterStartupScript(GetType(), "Javascript", "javascript: ShowMsg(); ", true);
                    // Response.Write(@"<script language='javascript'>alert('Alert: \n" + "EmailID Already Exists,Please provide another Email ID" + " .');</script>");
                    // ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('EmailID Already Exists,Plese provide another Email ID)", true);
                }

                //ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('Please check Email for Email Activation)", true);

                //lblMessage.Text = "Employee added successfully...";
                //string message = "Please check Email for Email Activation";
                //string script = "window.onload = function(){ alert('";
                //script += message;
                //script += "')};";
                //ClientScript.RegisterStartupScript(this.GetType(), "SuccessMessage", script, true);

            }
            catch (Exception ex)
            {
                lblMessage1.Text = ex.Message.ToString();
                ClientScript.RegisterStartupScript(this.GetType(), "Pop", "openModal();", true);
            }
        }

        private void SendActivationEmail()
        {
            log.Info("enter function SendActivationEmail");
            try
            {
                System.Net.Mail.MailMessage mail = new System.Net.Mail.MailMessage();
                // System.Web.Mail.MailMessage mail = new System.Web.Mail.MailMessage();
                var DecintellURl = ConfigurationManager.AppSettings.Get("Activationurl");
                string verificationcode = Session["verificationcode"].ToString();
                mail.To.Add(txtemail.Text);
                mail.CC.Add("contact@letmetalk2.me");
                mail.Bcc.Add("priyamvada.bavadekar@letmetalk2.me");
                // mail.From = "contact@letmetalk2.me";
                // mail.From = new MailAddress("contact@letmetalk2.me", "Let me talk to me", System.Text.Encoding.UTF8);
                mail.Subject = "Account Activation";
                string getDt = DateTime.Now.Date.AddDays(7).ToString("MM/dd/yyyy");
                string body = "<html><body><font color=\"black\">Hello " + txtfirstname.Text.Trim() + ",";
                body += "<br/><br/>Please click the following link to activate your account";
                //mail.Body = body + "<br /><a href=" + DecintellURl + "" + verificationcode+ " > Link </ a >";
                mail.Body = body + " <br/><br/><a href='" + DecintellURl + "" + verificationcode + "' >" + DecintellURl + "" + verificationcode + " </a><br/></br>Thank You.<br/><br/></font></body></html>";

                mail.IsBodyHtml = true;

                mail.Priority = System.Net.Mail.MailPriority.High;
                SmtpClient client = new SmtpClient();
                // client.Credentials = new System.Net.NetworkCredential("contact@letmetalk2.me", "contact123");
                //  client.Port = 587;
                // client.Host = "dallas137.arvixeshared.com";
                client.EnableSsl = false;
                client.Send(mail);
                log.Info(" Send Mail Successfully.");
            }
            catch (Exception ex)
            {
                // Response.Write(@"<script language='javascript'>alert(" + ex.Message.ToString() + ");</script>");
                lblMessage1.Text = ex.Message.ToString();
                ClientScript.RegisterStartupScript(this.GetType(), "Pop", "openModal();", true);
            }

        }
        private void SendActivationEmailNewTemplate()
        {
            try
            {
                //string DefaultImagePath = HttpContext.Current.Server.MapPath(@"~\UIFolder\dist\img\logotemp.png");
                //byte[] imageArray = System.IO.File.ReadAllBytes(DefaultImagePath);
                //string base64ImageRepresentation = Convert.ToBase64String(imageArray);

                System.Net.Mail.MailMessage mail = new System.Net.Mail.MailMessage();
                var DecintellURl = ConfigurationManager.AppSettings.Get("Activationurl");
                string verificationcode = Session["verificationcode"].ToString();
                mail.To.Add(txtemail.Text);
                mail.CC.Add("contact@letmetalk2.me");
                mail.Bcc.Add("priyamvada.bavadekar@letmetalk2.me");
                mail.Subject = "Account Activation";
                string getDt = DateTime.Now.Date.AddDays(7).ToString("MM/dd/yyyy");
                StringBuilder body = new StringBuilder();
                body.Append("<body>");
                body.Append("<table width='600' border='0' align='center' cellpadding='0' cellspacing='0' style='border:2px solid #b3c8c8 !important'>");
                body.AppendLine("<tr>");
                body.AppendLine("<td><table width='100%' border='0' cellspacing='0' cellpadding='0'>");
                body.AppendLine("<tr>");
                body.AppendLine("<td valign='top'><table width='90%' border='0' align='center' cellpadding='0' cellspacing='0'>");
                body.AppendLine("<tr>");
                body.AppendLine("<td  style='font-family:Verdana, Arial, Helvetica, sans-serif; font-size:12px; line-height:1.5; text-align:justify;'&nbsp;</td>");
                body.AppendLine("</tr>");
                body.AppendLine("<tr>");
                body.AppendLine("<td  style='font-family:Verdana, Arial, Helvetica, sans-serif;  font-size:13px; line-height:1.8; text-align:justify;'><div style='font-size:18px; color:#5b6766; font-weight:800;'>Hello "+ txtfirstname.Text.Trim() + ", </div>");
                body.AppendLine("<div style='clear:both;'></div>");
                body.AppendLine("<p style='color:#829a9a; font-weight:800; font-size:14px; margin-bottom:0; padding-bottom:0;'>Please click the following link to activate your account  </p>");
                body.AppendLine("<a href='"+DecintellURl+ "" + verificationcode + "' target='_blank' ");
                body.AppendLine("style ='background-color: #5b756c;");
                body.AppendLine("padding: 8px 10px;");
                body.AppendLine("text-align: center;");
                body.AppendLine("text-decoration: none;");
                body.AppendLine("display: inline-block;");
                body.AppendLine("font-size: 15px;");
                body.AppendLine("color: #fff;");
                body.AppendLine("border-radius: 25px; margin-top:10px;'");
                body.AppendLine(">Click here</a>");
                body.AppendLine("</td>");
                body.AppendLine("</tr>");
                body.AppendLine("<tr>");
                body.AppendLine("<td  style='font-family:Verdana, Arial, Helvetica, sans-serif; font-size:12px; line-height:1.5; text-align:justify;'>&nbsp;</td>");
                body.AppendLine("</tr>");
                body.AppendLine("</table></td>");
                body.AppendLine("</tr>");
                body.AppendLine("<tr>");
                body.AppendLine("<td height='140' bgcolor='#b3c8c8' style='font-family:Verdana, Arial, Helvetica, sans-serif; font-size:20px; color:#fff;'><table width='90%' border='0' align='center' cellpadding='0' cellspacing='0'>");
                body.AppendLine("<tr>");
                body.AppendLine("<td width='23%'><img src='"+ConfigurationManager.AppSettings.Get("ImagePath") + @"\UIFolder\dist\img\logotemp.png' width ='120' height='122' /></td>");
                body.AppendLine("<td width='2%'>&nbsp;</td>");
                body.AppendLine("<td width='75%' align='left'><table width='100%' border='0' align='left' cellpadding='0' cellspacing='0'>");
                body.AppendLine("<tr>");
                body.AppendLine("<td style='color:#5b756c'><strong>let me talk team</strong></td>");
                body.AppendLine("</tr>");
                body.AppendLine("<tr>");
                body.AppendLine("<td height='28' style='font-size:14px; border-bottom:1px dotted #5b756c; padding-bottom:5px; color:#5b756c'>self test program</td>");
                body.AppendLine("</tr>");
                body.AppendLine("<tr>");
                body.AppendLine("<td style='font-size:12px; padding-top:5px; color:#5b756c'>http://score.letmetalk2.me/</td>");
                body.AppendLine("</tr>");
                body.AppendLine("<tr>");
                body.AppendLine("<td height='20' style='font-size:12px; color:#5b756c'><span style='font-size:12px; padding-top:5px;'>©2020 let me talk. all rights reserved.</span></td>");
                body.AppendLine("</tr>");
                body.AppendLine("</table></td>");
                body.AppendLine("</tr>");
                body.AppendLine("</table></td>");
                body.AppendLine("</tr>");
                body.AppendLine("</table></td>");
                body.AppendLine("</tr>");
                body.AppendLine("</table>");
                body.AppendLine("</body>");
                mail.Body = body.ToString();
                mail.IsBodyHtml = true;

                mail.Priority = System.Net.Mail.MailPriority.High;
                //SmtpClient client = new SmtpClient();
                //client.EnableSsl = false;
                //client.Send(mail);
                //log.Info(" Send Mail Successfully.");

                SmtpClient client = new SmtpClient();
                client.EnableSsl = false;
                client.UseDefaultCredentials = true;
                client.DeliveryMethod = SmtpDeliveryMethod.Network;
                client.Send(mail);
                log.Info(" Send Mail Successfully.");
            }
            catch (Exception ex)
            {
                // Response.Write(@"<script language='javascript'>alert(" + ex.Message.ToString() + ");</script>");
                lblMessage1.Text = ex.Message.ToString();
                ClientScript.RegisterStartupScript(this.GetType(), "Pop", "openModal();", true);
            }

        }
        protected void btngmaillogin_Click(object sender, EventArgs e)
        {
            // lletmetalk5@gmail.com Letmetalk@123
            googleplus_redirect_url = ConfigurationManager.AppSettings.Get("Loginurl");
            googleplus_client_id = ConfigurationManager.AppSettings.Get("googleplus_client_id");
            googleplus_client_secret = ConfigurationManager.AppSettings.Get("googleplus_client_secret");
            var Googleurl = "https://accounts.google.com/o/oauth2/auth?response_type=code&redirect_uri=" + googleplus_redirect_url + "&scope=https://www.googleapis.com/auth/userinfo.email%20https://www.googleapis.com/auth/userinfo.profile&client_id=" + googleplus_client_id;
            Session["loginWith"] = "google";
            Constant.UserFlagLoginOrSingup = "signup";
            Response.Redirect(Googleurl);

            //string clientid = "831162440368-gjpqkqbdj5mik8adij6s313qt51kqjor.apps.googleusercontent.com";
            ////your client secret  
            //string clientsecret = "8xN5P5Gqtmk1_hwzEHQm22uT";
            //////your redirection url  
            //string redirection_url = "http://localhost:44352/Employee/EmployeeLogin.aspx";
            //string url = "https://accounts.google.com/o/oauth2/v2/auth?scope=profile&include_granted_scopes=true&redirect_uri=" + redirection_url + "&response_type=code&client_id=" + clientid + "";
            //Response.Redirect(url);

        }

        protected void FBLogin_Click(object sender, EventArgs e)
        {

            var fb = new FacebookClient();

            var loginUrl = fb.GetLoginUrl(new
            {


                client_id = "682470882264872",
                redirect_uri = "https://localhost:44352/Employee/oauth-redirect.aspx",
                //redirect_uri = "http://apps.brainlines.net/LMT2M_test/Employee/oauth-redirect.aspx",

                //response_type = "code",b96294d92d87f37d53fe64a993ff6989

                scope = "email" /// Add other permissions as needed

            });
            Response.Redirect(loginUrl.AbsoluteUri);
            {
            }


            //string clientId = "2447416122199968";
            //string redirectUrl = "https://localhost:44352/Employee/oauth-redirect.aspx";
            //Response.Redirect(string.Format("https://graph.facebook.com/oauth/authorize?client_id={0}&redirect_uri={1}", clientId, redirectUrl));

        }
    }
}