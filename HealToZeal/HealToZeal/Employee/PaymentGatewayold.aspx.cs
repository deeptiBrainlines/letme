﻿using BE;
using DAL;
using System;
using System.Data;
using System.Net.Mail;
namespace HealToZeal.Employee
{
    public partial class PaymentGateway : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Session["EmployeeID"].ToString() != null)
                {
                    EmployeeDAL objEmployeeDAL = new EmployeeDAL();
                    EmployeeBE objEmployeeBE = new EmployeeBE();
                    objEmployeeBE.propCompanyId_FK = Session["CompanyId"].ToString();
                    //objEmployeeBE.verificationcode = Session["verificationcode"].ToString();
                    objEmployeeBE.propEmployeeId_PK = Session["EmployeeID"].ToString();
                    DataTable dt1 = objEmployeeDAL.GetEmployeePrice(objEmployeeBE);

                    lblprice.Text = dt1.Rows[0]["CouponAmount"].ToString();
                    lblname.Text = dt1.Rows[0]["CouponName"].ToString();
                    lblEmailid.Text = dt1.Rows[0]["EmailId"].ToString();
                    lblFirstname.Text = dt1.Rows[0]["FirstName"].ToString();
                    lblLastname.Text = dt1.Rows[0]["LastName"].ToString();
                    lblpercentage.Text = dt1.Rows[0]["CouponDiscountPercentage"].ToString();
                }
                else
                {

                }
            }
        }

        protected void paymentgateway_Click(object sender, EventArgs e)
        {

            //getEmployees();
            // ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('Please check Email for Assessment Login)", true);


            EmployeeDAL objEmployeeDAL = new EmployeeDAL();
            EmployeeBE objEmployeeBE = new EmployeeBE();

            objEmployeeBE.propEmployeeId_PK = Session["EmployeeID"].ToString();


            DataTable dt = objEmployeeDAL.GetEmployeebyEmpid(objEmployeeBE);

            string FirstName = dt.Rows[0]["FirstName"].ToString();
            string LastName = dt.Rows[0]["LastName"].ToString();
            string Email = dt.Rows[0]["EmailId"].ToString();
            string Password1 = dt.Rows[0]["Password"].ToString();
            String Password = Encryption.Decrypt(Password1);
            string UserName = dt.Rows[0]["UserName"].ToString();
            Session["CompanyId"] = dt.Rows[0]["CompanyID_FK"].ToString();

            System.Net.Mail.MailMessage mail = new System.Net.Mail.MailMessage();


            mail.To.Add(Email);
            mail.CC.Add("contact@letmetalk2.me");
            mail.Bcc.Add("priyamvada.bavadekar@letmetalk2.me");
            // mail.From = "contact@letmetalk2.me";
            // mail.From = new MailAddress("contact@letmetalk2.me", "Let me talk to me", System.Text.Encoding.UTF8);
            mail.Subject = "Welcome to the Health and Personality Insights and Actions Tool.";
            string getDt = DateTime.Now.Date.AddDays(7).ToString("MM/dd/yyyy");

            //mail.Body = "<html><body><font color=\"black\">Dear " + FirstName + " " + LastName + ",<br/><br/>Welcome to <b> Let Me Talk2 Me: A tool to quickly and discreetly assess your mindfulness.</b> You will be able to gain some useful insights into your own personality and learn about remedial actions in case they are needed. <b>'Let Me Talk2 Me'</b> will help you in self-discovery. It will lead on a path of self reflections while avoiding pitfalls of modern complex life and careers. <br/><br/>It is recommended that you use this tool periodically to monitor yourself and be aware of your health and personality parameters. <br/><br/> Please use the link below to login <br/><br/><a href='http://letmetalk2.me.hazel.arvixe.com/Employee/EmployeeLogin.aspx?first=1'> http://letmetalk2.me.hazel.arvixe.com/Employee/EmployeeLogin.aspx </a><br/>Your login details are as follows:<br/>UserId:<b>" + UserName + "</b><br/>Password:<b>" + Password + "</b><br/><br/>If you would like to take a break during assessment, please make sure you log out from the system or close the browser window for security purposes. Please do not leave the browser idle. You can always log back in whenever you like.<br/><br/>Please ensure that you complete this assessment on or before " + getDt + ". To get a maximum benefit, please repeat this exercise every 3 months and study the differentials.<br/> You will be notified for the same when the next assessment is due.<br/><br/>If you wish to join the ecosystem of mindfulness, please pre-register with us.<a href='http://letmetalk2.me/pre-register'>http://letmetalk2.me/pre-register</a></br>Thank You.<br/><br/></font></body></html>";

            mail.Body = "<html><body><font color=\"black\">Dear " + FirstName + " " + LastName + ",<br/><br/>Welcome to <b> Let Me Talk2 Me: A tool to quickly and discreetly assess your mindfulness.</b> You will be able to gain some useful insights into your own personality and learn about remedial actions in case they are needed. <b>'Let Me Talk2 Me'</b> will help you in self-discovery. It will lead on a path of self reflections while avoiding pitfalls of modern complex life and careers. <br/><br/>It is recommended that you use this tool periodically to monitor yourself and be aware of your health and personality parameters. <br/><br/> Please use the link below to login <br/><br/><a href='http://apps.brainlines.net/LMT2M_Test/Employee/EmployeeLogin.aspx?first=1'> http://apps.brainlines.net/LMT2M_Test/Employee/EmployeeLogin.aspx </a><br/>Your login details are as follows:<br/>UserId:<b>" + UserName + "</b><br/>Password:<b>" + Password + "</b><br/><br/>If you would like to take a break during assessment, please make sure you log out from the system or close the browser window for security purposes. Please do not leave the browser idle. You can always log back in whenever you like.<br/><br/>Please ensure that you complete this assessment on or before " + getDt + ". To get a maximum benefit, please repeat this exercise every 3 months and study the differentials.<br/> You will be notified for the same when the next assessment is due.<br/><br/>If you wish to join the ecosystem of mindfulness, please pre-register with us.<a href='http://letmetalk2.me/pre-register'>http://letmetalk2.me/pre-register</a></br>Thank You.<br/><br/></font></body></html>";

            mail.IsBodyHtml = true;

            mail.Priority = System.Net.Mail.MailPriority.High;
            SmtpClient client = new SmtpClient();
            // client.Credentials = new System.Net.NetworkCredential("contact@letmetalk2.me", "contact123");
            //  client.Port = 587;
            // client.Host = "dallas137.arvixeshared.com";
            client.EnableSsl = false;
            client.Send(mail);

            string message = "Please check Email for Assessment Login";
            string script = "window.onload = function(){ alert('";
            script += message;
            script += "')};";
            ClientScript.RegisterStartupScript(this.GetType(), "SuccessMessage", script, true);
            //Response.Redirect("EmployeeLogin.aspx");
        }

    }
}