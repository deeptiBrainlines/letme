﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master/LetMeTalkWithMenu.Master" AutoEventWireup="true" CodeBehind="SuggestionFrnd.aspx.cs" Inherits="HealToZeal.Employee.SuggestionFrnd" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="row">
        <div class="col-md-11 col-centered">
            <div class="col-md-12 boardphome bg2 bdr2">
                <div id="style-3">
                    <div class="row">
                        <div class="col-md-9 col-sm-9 col-centered">
                            <h1 class="txtwhite w400">You did great!</h1>
                            <h2 class="txtwhite w400 mt0">Why not let a few loved ones benefit too?</h2>
                            <div class="clearfix"></div>
                            <div class="row">
                                <div id="frndDetails1">
                                    <div class="col-md-6 col-sm-6">
                                        <div class="form-group">
                                            <h4 class="fwnormal mb txtwhite">first name</h4>
                                            <asp:TextBox ID="txtFirstName1" CssClass="form-control" runat="server"></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-sm-6">
                                        <div class="form-group">
                                            <h4 class="fwnormal mb txtwhite">last name</h4>
                                            <asp:TextBox ID="txtLastName1" CssClass="form-control" runat="server"></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="col-md-12 col-sm-12">
                                        <div class="form-group">
                                            <h4 class="fwnormal mb txtwhite">email</h4>
                                            <asp:TextBox ID="txtEmail1" CssClass="form-control" onblur="isEmail(this.value);" runat="server"></asp:TextBox>
                                        </div>
                                    </div>
                                </div>
                               
                                <div style="clear:both;"></div>
                                     
                                 <div class="col-md-12 col-sm-12"> <div style="border-bottom:1px solid #fff;"></div></div>
                                <div id="frndDetails2">
                                    <div class="col-md-6 col-sm-6">
                                        <div class="form-group">
                                            <h4 class="fwnormal mb txtwhite">first name</h4>
                                            <asp:TextBox ID="txtFirstName2" CssClass="form-control" runat="server"></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-sm-6">
                                        <div class="form-group">
                                            <h4 class="fwnormal mb txtwhite">last name</h4>
                                            <asp:TextBox ID="txtLastName2" CssClass="form-control" runat="server"></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="col-md-12 col-sm-12">
                                        <div class="form-group">
                                            <h4 class="fwnormal mb txtwhite">email</h4>
                                            <asp:TextBox ID="txtEmail2" CssClass="form-control" onblur="isEmail(this.value);" runat="server"></asp:TextBox>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                         <asp:Label ID="lblMessage" CssClass ="validationerror" runat="server"></asp:Label>
                    </div>
                    <div class="force-overflow"></div>
                </div>
                <div class="clearfix"></div>
                <div class="row" style="padding-right: 18px;">
                    <div class="col-md-9 col-sm-9 mt30 col-centered">
                        <div>
                            <div class="clearfix"></div>
                            <h4 class="mt0">
                                <a href="UserResultReport.aspx" class="w400" style="color: #B6D7DA;">skip ></a>
                                <%--<a href="UserResultReport.aspx" class="mood-btn pull-right">submit</a>--%>
                                <asp:Button Text="submit" runat="server" CssClass="mood-btn pull-right" ID="btnSubmit" OnClientClick="return CheckValidation();" OnClick="btnSubmit_Click" />
                            </h4>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
            <div class="clearfix"></div>
        </div>


    </div>
    <script type="text/javascript">
        function CheckValidation() {
            if ($("input[id$='txtEmail1']").val()==$("input[id$='txtEmail2']").val()) {
                lblMessage = "Do not enter same email id";
                return false;
            }
            else {
                return true;
            }
        }
        function isEmail(email) {
            var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
            //return regex.test(email);
            if (!regex.test(email)) {
                lblMessage = "please enter valid email";
           return false;
        }else{
           return true;
        }
        }
    </script>
</asp:Content>
