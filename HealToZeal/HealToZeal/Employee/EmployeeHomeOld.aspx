﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Employee/Employee.Master" AutoEventWireup="true" CodeBehind="EmployeeHomeOld.aspx.cs" Inherits="HealToZeal.Employee.EmployeeHome" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="EmployeeColumn" runat="server">
    <!-- Custom Stylesheet -->
    <!-- Slider Stylesheet -->

    <script type="text/javascript">        
        function DisplayColorBox(url) {
            $.fn.colorbox({ href: url, iframe: true, scrolling: true, width: '1000px', position: 'Fixed', height: '650px', overlayClose: false });
            //, onClosed: function () { parent.location.reload(false); }
            return false;
            //$(cntrl).colorbox({      
            //        iframe: true,
            //        opacity:1,
            //        href: '~/Pathway.aspx',
            //        width: '1100px',
            //        height: '600px',
            //        overlayClose: false   ,
            //        border: '10px',
            //         margin: '200px',
            //         inline:'true',
            //         transition:'elastic',
            //});
        }
        function DisplayColorBox1(url) {

            $.fn.colorbox({ href: url, iframe: true, scrolling: true, width: '600px', position: 'Fixed', height: '450px', overlayClose: false });
            //, onClosed: function () { parent.location.reload(false); }
            return false;
            //$(cntrl).colorbox({      
            //        iframe: true,
            //        opacity:1,
            //        href: '~/Pathway.aspx',
            //        width: '1100px',
            //        height: '600px',
            //        overlayClose: false   ,
            //        border: '10px',
            //         margin: '200px',
            //         inline:'true',
            //         transition:'elastic',
            //});
        }
    </script>
    <style type="text/css">
        .ajax__calendar_container {
            z-index: 9999;
            position: static;
            background-color: white;
        }
    </style>

    <div class="container">
        <div id="DivDisplay" runat="server" visible="false" class="row">
            <div class="col-lg-12 col-md-12 col-sm-12">
                <h4></h4>
                <div class="row hs_how_we_are">
                    <div class="col-lg-12 col-md-12 col-sm-12">
                        <%--<img src="../images/Let Me Talk2 Me Poster.jpg" />  --%>
                        <img src="../images/letmetalk2m2new.jpg" />
                    </div>
                </div>
            </div>
        </div>

        <h4></h4>
        <div class="row">
            <div id="divalert" class="alert alert-success" runat="server">
                <strong>
                    <asp:Label ID="lblMessage" runat="server"></asp:Label></strong>
            </div>
        </div>
        <div class="row">
            <table style="width: 100%">
                <tr>
                    <td style="width: 100%; text-align: left">
                        <asp:Panel ID="pnlEmployeeBasicDetails" runat="server" Visible="False">
                            <div class="col-lg-8 col-md-8 col-sm-7">
                                <%--<div class="hs_comment_form">--%>
                                <div class="row">

                                    <asp:ValidationSummary ID="ValidationSummary1" runat="server" ForeColor="Red" ValidationGroup="0" />
                                    <%--</div>--%>
                                </div>
                            </div>
                            <div class="col-lg-8 col-md-8 col-sm-7">
                                <h4 class="hs_heading" id="hs_appointment_form_link">Basic details</h4>
                                <div class="hs_comment_form">
                                    <div class="row">
                                        <div class="col-lg-6 col-md-6 col-sm-12">
                                            <div class="input-group">
                                                <span class="input-group-btn"></span>
                                                <asp:TextBox ID="txtBirthDate" runat="server" CssClass="form-control" placeholder="Birthdate"
                                                    ValidationGroup="0"></asp:TextBox>
                                                &nbsp; Tip: you can directly enter DOB in MM\DD\YYYY format
                                                <cc1:CalendarExtender ID="txtBirthDate_CalendarExtender" runat="server" CssClass="ajax__calendar_container" PopupPosition="BottomRight" Format="MM/dd/yyyy" TargetControlID="txtBirthDate">
                                                </cc1:CalendarExtender>
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="Select birthdate" ControlToValidate="txtBirthDate" ValidationGroup="0" ForeColor="Red">*</asp:RequiredFieldValidator>

                                                <asp:CompareValidator ID="CompareValidator1" runat="server"
                                                    ControlToValidate="txtBirthDate" Display="Dynamic" ErrorMessage="Invalid Date"
                                                    Operator="DataTypeCheck" Type="Date">
                                                </asp:CompareValidator>
                                                <cc1:ValidatorCalloutExtender ID="CompareValidator1_ValidatorCalloutExtender"
                                                    runat="server" Enabled="True" TargetControlID="CompareValidator1">
                                                </cc1:ValidatorCalloutExtender>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-lg-6 col-md-6 col-sm-12">
                                            <div class="input-group">
                                                <span class="input-group-btn"></span>
                                                <asp:TextBox ID="txtOccupation" runat="server" CssClass="form-control" placeholder="Occupation"
                                                    ValidationGroup="0"></asp:TextBox>
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="Enter occupation" ControlToValidate="txtOccupation" ForeColor="Red" ValidationGroup="0">*</asp:RequiredFieldValidator>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-lg-6 col-md-6 col-sm-12">
                                            <div class="input-group">
                                                <span class="input-group-btn"></span>
                                                <asp:RadioButtonList ID="rblGender" runat="server" RepeatDirection="Horizontal"
                                                    ValidationGroup="0">
                                                    <asp:ListItem Value="M">Male</asp:ListItem>
                                                    <asp:ListItem Value="F">Female</asp:ListItem>
                                                </asp:RadioButtonList>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-lg-6 col-md-6 col-sm-12">
                                            <div class="input-group">
                                                <span class="input-group-btn"></span>
                                                <asp:DropDownList ID="ddlEducation" runat="server" CssClass="form-control"
                                                    ValidationGroup="0">
                                                    <asp:ListItem>--Select education--</asp:ListItem>
                                                    <asp:ListItem>PostGraduate</asp:ListItem>
                                                    <asp:ListItem>Graduate</asp:ListItem>
                                                    <asp:ListItem>Doctorate</asp:ListItem>
                                                </asp:DropDownList>
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ErrorMessage="Select education" ForeColor="Red" InitialValue="--Select education--" ValidationGroup="0" ControlToValidate="ddlEducation">*</asp:RequiredFieldValidator>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-lg-6 col-md-6 col-sm-12">
                                            <div class="input-group">
                                                <span class="input-group-btn"></span>
                                                <asp:DropDownList ID="ddlExperience" runat="server" CssClass="form-control"
                                                    ValidationGroup="0">
                                                    <asp:ListItem>--Select experience--</asp:ListItem>
                                                    <asp:ListItem>0-3 years</asp:ListItem>
                                                    <asp:ListItem>3-7years</asp:ListItem>
                                                    <asp:ListItem>7-10years</asp:ListItem>
                                                    <asp:ListItem>10-15years</asp:ListItem>
                                                    <asp:ListItem>15 and above</asp:ListItem>
                                                </asp:DropDownList>
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ErrorMessage="Select experience" ControlToValidate="ddlExperience" ForeColor="Red" InitialValue="--Select experience--" ValidationGroup="0">*</asp:RequiredFieldValidator>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-lg-6 col-md-6 col-sm-12">
                                            <div class="input-group">
                                                <span class="input-group-btn"></span>
                                                <asp:DropDownList ID="ddlPosition" runat="server" CssClass="form-control"
                                                    ValidationGroup="0">
                                                    <asp:ListItem>--Select position--</asp:ListItem>
                                                    <asp:ListItem>Junior level</asp:ListItem>
                                                    <asp:ListItem>middle management</asp:ListItem>
                                                    <asp:ListItem>Senior management</asp:ListItem>
                                                </asp:DropDownList>
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ErrorMessage="Select position" ControlToValidate="ddlPosition" ForeColor="Red" InitialValue="--Select position--" ValidationGroup="0">*</asp:RequiredFieldValidator>
                                            </div>

                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-lg-6 col-md-6 col-sm-12">
                                            <div class="input-group">
                                                <span class="input-group-btn"></span>

                                                <asp:CheckBox ID="CheckBox1" runat="server" Text="" />
                                                <asp:Label ID="Label1" runat="server" Text="Yes,My Data can be shared with my company HR/Admin"></asp:Label>
                                            </div>

                                        </div>
                                    </div>



                                    <div class="row">
                                        <div class="col-lg-6 col-md-6 col-sm-12">
                                            <div class="input-group">
                                                <span class="input-group-btn"></span>
                                                <asp:Button ID="btnSubmit" runat="server" Text="Submit" CssClass="btn btn-success pull-right"
                                                    OnClick="btnSubmit_Click" ValidationGroup="0" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </asp:Panel>
                    </td>
                </tr>
            </table>
        </div>
    </div>

    <%--   <div class="web_page_body" id="slidediv" runat="server">
            <div id="da-slider" class="da-slider hidden-xs">
                <div class="da-slide">
                    <h2>Let Me Talk2 Me</h2>                   
                    <!--<a href="#" class="da-link">Read more</a>-->
                    <div class="da-img"><img src="../images/psycologyimg.jpg" alt="image01" /></div>
                </div>
                <div class="da-slide">
                    <h2>Mental Health</h2>                   
                    <!--<a href="#" class="da-link">Read more</a>-->
                    <div class="da-img"><img src="../images/psycologyimg1.jpg" alt="image01" /></div>
                </div>
                <div class="da-slide">   
                    <h2>Let Me Talk2 Me</h2>                
                    <!--<a href="#" class="da-link">Read more</a>-->
                    <div class="da-img"><img src="../images/psycologyimg2.jpg" alt="image01" /></div>
                </div>
                <div class="da-slide"> 
                    <h2>Let Me Talk2 Me</h2>                  
                    <!--<a href="#" class="da-link">Read more</a>-->
                    <div class="da-img"><img src="../images/psycologyimg4.jpg" alt="image01" /></div>
                </div>
                <nav class="da-arrows">
                    <span class="da-arrows-prev"></span>
                    <span class="da-arrows-next"></span>
                </nav>
            </div>
            
    	
    </div>
    --%>

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->

    <!-- Include all compiled plugins (below), or include individual files as needed -->


    <!-- Slider Start -->
    <%--    <script type="text/javascript" src="../Scripts/modernizr.custom.28468.js"></script>
	<script type="text/javascript" src="../Scripts/jquery.cslider.js"></script>
	<script type="text/javascript">
	    $(function () {

	        $('#da-slider').cslider({
	            autoplay: true,
	            bgincrement: 450
	        });

	    });
    </script>--%>
    <!-- Slider end -->
    <script type="text/javascript">
        function checkButton() {
            if (document.form1.CheckBox1.checked == true) {
                alert("Yes,My Data can be shared with my company HR/Admin");
            }

        }

    </script>
</asp:Content>

