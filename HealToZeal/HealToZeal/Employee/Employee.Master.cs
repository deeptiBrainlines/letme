﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DAL;
using System.Data;

namespace HealToZeal.Employee
{
    public partial class Employee : System.Web.UI.MasterPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                if (Session["Pagename"] != null)
                {
                    //d.Visible = false;
                    //dd.Visible = false;
                }
                else
                {
                    //d.Visible = true;
                    //dd.Visible = true;
                }
                if (Session["UserName"] != null)
                {
                    //  lblTitle.Text = Session["UserName"].ToString();
                    //Menu1.FindItem("User").Text = Session["UserName"].ToString();
                    lblusername.Text ="User Id " + Session["EmployeeCompanyId"].ToString() + " (" + DateTime.Today.ToShortDateString() + ")";
                    LiPprofile.Style.Add("display", "inline-block");
                 //  LiPsychographicTest.Style.Add("display", "inline-block");
                    LiReport.Style.Add("display", "inline-block");
                    LiEmployeeHistory.Style.Add("display", "inline-block");  

                     LiFeedback.Style.Add("display", "inline-block");
                    LiLogout.Style.Add("display", "inline-block");
                    LiLogin.Style.Add("display", "none");
                    LiEmployeeHistory.Style.Add("display", "inline-block");

                    //check for employee authority
                    EmployeeDAL objEmployeeDAL = new EmployeeDAL();
                 DataTable dtParent= objEmployeeDAL.ParentEmployeeRelationshipGetParent(new Guid(Session["EmployeeId"].ToString()));
                 if (dtParent != null)
                 {
                     if (dtParent.Rows.Count > 0)
                     {
                         LiViewEmployee.Style.Add("display", "inline-block"); 
                     }
                     else
                     {
                         LiViewEmployee.Style.Add("display", "none");                       
                     }
                 }
                }
                else
                {
                    LiPprofile.Style.Add("display", "none");
                    //LiPsychographicTest.Style.Add("display", "none");
                    LiViewEmployee.Style.Add("display", "none");
                    LiReport.Style.Add("display", "none");
                    LiFeedback.Style.Add("display", "none");
                    LiLogout.Style.Add("display", "none");
                    LiLogin.Style.Add("display", "inline-block");
                    LiEmployeeHistory.Style.Add("display", "none");
                }
            }
        }

        //protected void lbLogout_Click(object sender, EventArgs e)
        //{
        //    try
        //    {
        //        Session.Clear();
        //        Session.Abandon();
        //        Response.Redirect("EmployeeLogin.aspx");
        //    }
        //    catch
        //    {
        //    }
        //}

        //protected void btnLogout_Click(object sender, EventArgs e)
        //{
        //    Session.Clear();
        //    Session.Abandon();
        //    Response.Redirect("EmployeeLogin.aspx");
        //}

        protected void lbtnLogout_Click(object sender, EventArgs e)
        {
            Session.Clear();
            Session.Abandon();
            Response.Redirect("EmployeeLogin.aspx");
        }

        protected void lbtnLogin_Click(object sender, EventArgs e)
        {
            Session.Clear();
            Session.Abandon();
            Response.Redirect("EmployeeLogin.aspx");
        }
    }
}