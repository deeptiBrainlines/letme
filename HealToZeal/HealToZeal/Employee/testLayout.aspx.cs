﻿using BE;
using DAL;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace HealToZeal.Employee
{
    public partial class testLayout : System.Web.UI.Page
    {
        AssessmentQuestionRelationDAL objAssessmentQuestionRelationDAL = new AssessmentQuestionRelationDAL();
        EmployeeAssessmentDetailsDAL objEmployeeAssessmentDetailsDAL = new EmployeeAssessmentDetailsDAL();
        EmployeeAssessmentDetailsBE objEmployeeAssessmentDetailsBE = new EmployeeAssessmentDetailsBE();
        AnswerMasterDAL objAnswerMasterDAL = new AnswerMasterDAL();
        DataTable DtSaveAnswer = new DataTable();
        public static List<int> arrayqueCntList = new List<int>();
        string AssessmentId = string.Empty;
        string QuestionId = string.Empty;
        string EmployeeId = string.Empty;
        int position = 0;
       // int Default = 1;
        public static int maxNum = 0;
        public static int queCount = 1;
        RandomGenerator randomGenerator = new RandomGenerator();
        protected void Page_Load(object sender, EventArgs e)
        {
            
            string batchID = string.Empty;
            if (Session["BatchID"] != null && Session["BatchID"].ToString() != "")
            {
                batchID = Convert.ToString(Session["BatchID"].ToString());
            }
            if (!Page.IsPostBack)
            {
                Session["Pagename"] = null;
                try
                {
                    if (Session["EmployeeId"] != null)
                    {
                        //assessment id total question count
                        //AssessmentId = "AAD38F93-0DC7-4EA1-8A61-6B95816AED6B"; //assessment session
                       string EmployeeId = Convert.ToString(Session["EmployeeId"].ToString());

                        //check whether assessment is submitted or not
                        EmployeeAssessmentsHistoryDAL objEmployeeAssessmentsHistoryDAL = new EmployeeAssessmentsHistoryDAL();
                        DataTable DtAssessmentSubmitted = objEmployeeAssessmentsHistoryDAL.EmployeeAssementsGet(EmployeeId, batchID);

                        if (DtAssessmentSubmitted != null && DtAssessmentSubmitted.Rows.Count > 0)
                        {
                            if (DtAssessmentSubmitted.Select("AssessmentSubmitted=0").Length > 0)
                            {
                                if (Session["AssessmentInstanceId"] != null)
                                {
                                    DataTable DtTotalQuestionCount = objEmployeeAssessmentDetailsDAL.EmployeeAssessmentDetailsGetCount(EmployeeId, Session["AssessmentInstanceId"].ToString(), Session["AssessmentInstanceCompanyId"].ToString());
                                    if (DtTotalQuestionCount != null && DtTotalQuestionCount.Rows.Count > 0)
                                    {
                                        Session["TotalQuestionCount"] = DtTotalQuestionCount.Rows[0][0].ToString();
                                        maxNum = Convert.ToInt32(Session["TotalQuestionCount"].ToString());
                                        //if (Session["Position"] == null)
                                        //{
                                        //    btnPrevious.Enabled = false;
                                        //}
                                    }
                                    DataTable dtEmployees = new DataTable();
                                    dtEmployees = objEmployeeAssessmentDetailsDAL.EmployeeAssessmentDetailsGet(Session["AssessmentInstanceId"].ToString(), position, Session["EmployeeId"].ToString(), Session["AssessmentInstanceCompanyId"].ToString());
                                    if (dtEmployees != null && dtEmployees.Rows.Count > 0)
                                    {
                                        //  Session["TotalAnswers"] = Convert.ToInt32(dtEmployees.Rows[0]["TotalAnswers"].ToString());

                                        //    position = Convert.ToInt32(dtEmployees.Rows[0]["TotalAnswers"].ToString());

                                        //string str = dtEmployees.Rows[0]["TotalAnswers"].ToString();
                                        //str = str.TrimEnd(str[str.Length - 1]) + "0";
                                        //position = Convert.ToInt32(str);

                                        int count = ((Convert.ToInt32(dtEmployees.Rows[0]["TotalAnswers"].ToString()) / 1) * 1);
                                        position = randomGenerator.RandomNumber(1, maxNum);

                                        //position = count;
                                        Session["Position"] = position;
                                    }
                                    Questions questions = new Questions();
                                    position = questions.GetAlternateNumber(1, maxNum);
                                    Session["Position"] = position;



                                    BindTestQuestions(position);
                                    //27 jan 2015
                                    BindSavedAnswers(position);
                                }
                                else
                                {

                                    Response.Redirect("EmployeeAssessments.aspx");
                                }
                                //
                            }
                            else
                            {
                                Response.Redirect("EmployeeHome.aspx?Assessment=Y");
                            }
                        }
                        //else
                        //{

                        //}
                    }
                    else
                    {
                        Response.Redirect("EmployeeLogin.aspx");
                    }
                }
                catch (Exception )
                {
                   // log.Error(ex.Message.ToString() + " " + ex.StackTrace.ToString());
                }
            }
        }
        public void BindSavedAnswers(int Position)
        {
            try
            {
                EmployeeAssessmentDetailsDAL objEmployeeAssessmentDetailsDAL = new EmployeeAssessmentDetailsDAL();
                DataTable DtEmployeeAnswers = new DataTable();

                AssessmentId = "9465B829-997E-42F1-85D2-128FF3E5B6C6";//"AAD38F93-0DC7-4EA1-8A61-6B95816AED6B"; //AssessmentId commented by meenakshi on 2019-01-10
                EmployeeId = Session["EmployeeId"].ToString();

                //27 jan 2015
                //if (Session["Position"] != null)
                //{
                //    position = Convert.ToInt32(Session["Position"].ToString());
                //}

                DtEmployeeAnswers = objEmployeeAssessmentDetailsDAL.EmployeeAssessmentDetailsGet(Session["AssessmentInstanceId"].ToString(), Position, EmployeeId, Session["AssessmentInstanceCompanyId"].ToString());

                foreach (DataListItem answerItem in DlistQuestionsLayout2.Items)
                {
                    QuestionId = ((Label)answerItem.FindControl("lblQuestionId")).Text;
                    //  string AnswerId = ((RadioButtonList)answerItem.FindControl("rblistAnswers")).SelectedItem.Value;

                    DataRow[] DrAnswer = DtEmployeeAnswers.Select("QuestionId_FK='" + QuestionId + "'");
                    if (DrAnswer.Length > 0 && DrAnswer != null)
                    {
                        RadioButtonList rblistAnswers = (RadioButtonList)answerItem.FindControl("rblistAnswers");
                        foreach (ListItem rdo in rblistAnswers.Items)
                        {
                            if (DrAnswer[0][1].ToString() == rdo.Value.ToString())
                            {
                                rdo.Selected = true;
                            }
                        }
                        FocusControlOnPageLoad(rblistAnswers.ClientID, this.Page);
                    }
                }

                int percentage = 0;
                // Progressbar.Attributes.Add("class", "progress-bar progress-bar-success");
                //Progressbar1.Attributes.Add("class", "progress-bar progress-bar-success");

                if (DtEmployeeAnswers != null && DtEmployeeAnswers.Rows.Count > 0)
                {
                    percentage = ((Convert.ToInt32(DtEmployeeAnswers.Rows[0]["TotalAnswers"].ToString()) * 100) / Convert.ToInt32(Session["TotalQuestionCount"].ToString()));

                }
                else
                {
                    //  percentage = ((Convert.ToInt32(Session["Position"]) * 100) / Convert.ToInt32(Session["TotalQuestionCount"].ToString()));
                    percentage = ((Convert.ToInt32(queCount * 100) / Convert.ToInt32(Session["TotalQuestionCount"].ToString())));

                }
                //Progressbar.Attributes.Add("style", "width:" + percentage + "%");
                // Progressbar.InnerText = percentage.ToString() + " % of " + Session["TotalQuestionCount"].ToString() + " Questions";

                //Progressbar1.Attributes.Add("style", "width:" + percentage + "%");
                //Progressbar1.InnerText = percentage.ToString() + " % of " + Session["TotalQuestionCount"].ToString() + " Questions";
                if (percentage == 0)
                {
                    // Progressbar.Attributes.Add("class", "progress-bar progress-bar-danger");
                    //Progressbar.Attributes.Add("style", "width:100%");
                    // Progressbar.InnerText = percentage.ToString() + " % of " + Session["TotalQuestionCount"].ToString() + " Questions";

                    //Progressbar1.Attributes.Add("class", "progress-bar progress-bar-danger");
                    //Progressbar1.Attributes.Add("style", "width:100%");
                    //Progressbar1.InnerText = percentage.ToString() + " % of " + Session["TotalQuestionCount"].ToString() + " Questions";

                    //lnkBtnJumpto.Visible = false;
                }
            }
            catch (Exception ex)
            {
                ex.InnerException.ToString();
                //log.Error(ex.Message.ToString() + " " + ex.StackTrace.ToString());
            }
        }
        public void FocusControlOnPageLoad(string ClientID, System.Web.UI.Page page)
        {
            ClientScriptManager clientScript = this.Page.ClientScript;
            clientScript.RegisterClientScriptBlock(this.GetType(), "DlistQuestionsLayout2",

                           @"<script> 

          function ScrollView()

          {
             var el = document.getElementById('" + ClientID + @"')
             if (el != null)
             {        
                el.scrollIntoView();
                el.focus();
             }
          }

          window.onload = ScrollView;

          </script>");

        }

        public void BindTestQuestions(int Position)
        {
            try
            {
                ////AssessmentId = Session["AssessmentId"].ToString();
                ////AssessmentId = "AAD38F93-0DC7-4EA1-8A61-6B95816AED6B";

                //if (Session["Position"] != null)
                //{
                //    position = Convert.ToInt32(Session["Position"].ToString());
                //}
                ////if (position > 0)
                ////{
                ////    PopulatePager(position, (position / 10));
                ////}

                DataTable dtAssessmentQuestions = objAssessmentQuestionRelationDAL.AssessmentQuestionsRelationGet(Session["EmployeeId"].ToString(), Position, Session["AssessmentInstanceId"].ToString(), Session["AssessmentInstanceCompanyId"].ToString());
                DlistQuestionsLayout2.DataSource = dtAssessmentQuestions;
                DlistQuestionsLayout2.DataBind();
                //DataList1.DataSource = dtAssessmentQuestions;
                //DataList1.DataBind();
                //CheckPosition();
                BindAnswers();
                queCount++;
                //if (Session["Position"] != null)
                //Session["Position"] = Convert.ToInt32(Session["Position"].ToString()) + DlistQuestions.Items.Count ;
                //else
                //Session["Position"] = DlistQuestions.Items.Count;
            }
            catch (Exception )
            {
                //log.Error(ex.Message.ToString() + " " + ex.StackTrace.ToString());
            }
        }
        private void BindAnswers()
        {
            try
            {
                foreach (DataListItem questions in DlistQuestionsLayout2.Items)
                {
                    QuestionId = ((Label)questions.FindControl("lblQuestionId")).Text;
                    DataTable DtAnswers = objAnswerMasterDAL.AnswerMasterGetWithDescriptionByQuestionId(QuestionId);
                    ((RadioButtonList)questions.FindControl("rblistAnswers")).DataSource = DtAnswers;
                    ((RadioButtonList)questions.FindControl("rblistAnswers")).DataTextField = "AnswerShort";
                    ((RadioButtonList)questions.FindControl("rblistAnswers")).DataValueField = "AnswerId_PK";
                    ((RadioButtonList)questions.FindControl("rblistAnswers")).DataBind();
                }
            }
            catch (Exception )
            {
               // log.Error(ex.Message.ToString() + " " + ex.StackTrace.ToString());
            }
        }
        protected void DlistQuestions_ItemDataBound(object sender, DataListItemEventArgs e)
        {

        }
    }
}