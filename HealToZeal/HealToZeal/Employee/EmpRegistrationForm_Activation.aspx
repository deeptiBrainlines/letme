﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master/LetMeTalk.Master" AutoEventWireup="true" CodeBehind="EmpRegistrationForm_Activation.aspx.cs" Inherits="HealToZeal.Employee.EmpRegistrationForm_Activation1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">
        function openModalAlert() {
            $('[id*=modalAlert]').modal('show');
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
 <div style="clear:both;"></div>
    <div class="col-md-11 col-centered ">
        <div style="clear:both;"></div>
        <div class="bdr3" style="padding-bottom: 30px;">
            <div class="col-md-10 col-centered" style="padding: 50px 0;">
                <h1>
                    <asp:Literal ID="ltMessage" runat="server" /></h1>
                <h2>
                    <asp:Literal ID="ltpayment" runat="server" /></h2>
                <center>
                <h2>Thank you for your Email Activation </h2></center>
            </div>
        </div>
    </div>
    <div id="modalAlert" class="modal" role="dialog">
                <div class="modal-dialog modal-md">
                    <div class="modal-content">
                       
                       <div class="modal-body">
                            <div class="col-md-12 mt30">
                                <h4 class="modal-title">message</h4>
                                <p class="w400">
                                    <asp:Label ID="lblMessage1" runat="server"></asp:Label>
                                </p>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <a href="EmployeeLogin.aspx">
                                <img src="../UIFolder/dist/img/close.png"  class="popicon"></a>
                        </div>
                    </div>
                </div>
            </div>
</asp:Content>
