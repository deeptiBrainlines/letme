﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="EmployeeAssessmentsOld.aspx.cs" MasterPageFile="~/Employee/Employee.Master" Inherits="HealToZeal.Employee.EmployeeAssessments" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="EmployeeColumn" runat="server">
 
    <div class="container">
        <h4 class="hs_heading" id="hs_appointment_form_link">My mindfulness report
        </h4>
  <div class="row">
    <div class="col-lg-12 col-md-12 col-sm-12">  
        
            
        <asp:DataList ID="DlistAssessments" runat="server" Width="100%" OnItemCommand="DlistAssessments_ItemCommand">
            <HeaderTemplate>                
                  <%--  <div class="hs_sub_comment">--%>                     
                    
            </HeaderTemplate>
            <ItemTemplate>                              
                <div class="hs_sub_comment_div">
                  <%--  <div class="hs_sub_comment">--%>
                        <div class="row">
                            <div class="col-lg-10 col-md-10 col-sm-10 col-lg-offset-2 col-md-offset-2 col-sm-offset-2">
                                <div class="hs_comment">                                 
                                    <div class="row">
                                        <div class="col-lg-11 col-md-10 col-sm-10">
                                            <div class="hs_comment_date">
                                                <ul>
                                                    <li>
                                                        <asp:Label ID="lblSrNo" runat="server" Text='<%#Eval("SrNo") %>'></asp:Label>
                                                    </li>
                                                 <%--   <li>
                                                        <asp:Label ID="lblAssessment" runat="server" Text='<%#Eval("AssessmentName") %>'></asp:Label>
                                                    </li>--%>
                                                    <li>
                                                        <asp:Label ID="lblDateSubmitted" runat="server" Text= '<%# "On "+ DataBinder.Eval(Container.DataItem, "CreatedDate", "{0:dd/MM/yyyy}") %>'></asp:Label>
                                                    </li>
                                                    <li>
                                                        <asp:Label ID="lblAssessmentId" runat="server" Text='<%#Eval("AssessmentInstanceId_FK") %>' Visible="false"></asp:Label>
                                                    </li>
                                                     <li>
                                                        <asp:Label ID="lblAssessmentInstanceCompanyId" runat="server" Text='<%#Eval("InstanceCompanyRelationId_PK") %>' Visible="false"></asp:Label>
                                                    </li>
                                                    <li>
                                                        <a href="">
                                                            <asp:Button ID="btnView" runat="server" Visible='<%#Convert.ToBoolean(Eval("AssessmentSubmitted"))%>'    Text="View" CommandArgument='<%#Eval("AssessmentInstanceId_FK") %>' CommandName="ViewReport" CssClass="btn btn-success" />

                                                        </a>
                                                        <a href="">
                                                            <asp:Button ID="btnGraph" runat="server" Visible='<%# ((Eval("AssessmentSubmitted").Equals(false)) && (Eval("LastUpadte")==DBNull.Value)) %>'  Text="Expired"  CssClass="btn btn-success" />
                                                          
                                                        </a>
                                                         
                                                       <a href="">
                                                            <asp:Button ID="btnExpired" runat="server" Visible='<%# (Eval("AssessmentSubmitted").Equals(false) && (Eval("LastUpadte")!=DBNull.Value)) %>'  Text="Take a new test" CommandArgument='<%#Eval("AssessmentInstanceId_FK") %>' CommandName="ViewTest" CssClass="btn btn-success" />
                                                          
                                                        </a>
                                                       
                                                    </li>
                                                </ul>
                                                <%--  <i class="fa fa-paperclip"></i>--%>
                                                <p>
                                                </p>

                                            </div>
                                        </div>
                                    </div>
                             

                            </div>
                        </div>
                    <%--</div> --%>                  
                </div>    
                <div class="clearfix"></div>
      <div class="hs_margin_40"></div>          
            </ItemTemplate>
        </asp:DataList>
      
           </div>
      </div>
        </div>

</asp:Content>