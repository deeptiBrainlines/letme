﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master/LetMeTalkWithMenu.Master" AutoEventWireup="true" CodeBehind="Parameter.aspx.cs" Inherits="HealToZeal.Employee.Parameter" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="row">
        <div class="col-md-11 col-centered">
            <div class="boardp bdrgrey mb20 whitebg">
                <div class="icon-m10 pull-right">
                    <a href="#">
                        <img src="dist/img/cloud.png" class="pull-left mr20 img-responsive">
                    </a>
                    <a href="#">
                        <img src="dist/img/down.png" class="pull-left img-responsive">
                    </a>

                </div>
                <div class="scrollbar" id="style-3" style="margin-top: 0 important;">
                    <div class="pb28">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="table-responsive">
                                    <table width="100%" cellspacing="0" cellpadding="0" class="table">
                                        <tr>
                                            <th>PARAMETER</th>
                                            <th>YOUR SCORE</th>
                                            <th>STANDARD</th>
                                        </tr>
                                        <tr>
                                            <td>Ability to withstand pressure</td>
                                            <td>
                                                <img src="dist/img/orange.png" /></td>
                                            <td>
                                                <img src="dist/img/green.png" /></td>
                                        </tr>
                                        <tr>
                                            <td>Emotional balance</td>
                                            <td>
                                                <img src="dist/img/red.png"></td>
                                            <td>
                                                <img src="dist/img/orange.png" /></td>
                                        </tr>
                                        <tr>
                                            <td>Confidence</td>
                                            <td>
                                                <img src="dist/img/green.png" /></td>
                                            <td>
                                                <img src="dist/img/green.png" /></td>
                                        </tr>
                                        <tr>
                                            <td>Decision making</td>
                                            <td>
                                                <img src="dist/img/orange.png" /></td>
                                            <td>
                                                <img src="dist/img/orange.png" /></td>
                                        </tr>
                                        <tr>
                                            <td>Flexibility</td>
                                            <td>
                                                <img src="dist/img/green.png" /></td>
                                            <td>
                                                <img src="dist/img/green.png" /></td>
                                        </tr>
                                        <tr>
                                            <td>Resilence</td>
                                            <td>
                                                <img src="dist/img/red.png"></td>
                                            <td>
                                                <img src="dist/img/orange.png" /></td>
                                        </tr>
                                        <tr>
                                            <td>Self assurance</td>
                                            <td>
                                                <img src="dist/img/orange.png" /></td>
                                            <td>
                                                <img src="dist/img/orange.png" /></td>
                                        </tr>
                                    </table>

                                </div>

                                <div class="clearfix"></div>





                            </div>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                </div>

                <div class="clearfix"></div>
                <div class="col-md-12">
                    <div class="row">
                        <div class="col-md-6 pad0">
                            <div class="roundcenter">
                                <img src="dist/img/red.png" class="mr10">
                                <img src="dist/img/orange.png" class="mr10">
                                <img src="dist/img/green.png" class="mr10">
                            </div>
                        </div>

                        <div class="col-md-6">
                            <h4>

                                <a href="#" class="textlink bottxtlink w400">click here for a detailed analysis >></a>

                            </h4>
                        </div>
                    </div>
                </div>

                <div class="clearfix"></div>
            </div>
            <div class="clearfix"></div>
        </div>
        <div class="clearfix"></div>
    </div>
    <div class="clearfix"></div>
</asp:Content>
