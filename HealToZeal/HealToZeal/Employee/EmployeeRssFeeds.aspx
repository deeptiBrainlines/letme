﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Employee/Employee.Master" AutoEventWireup="true" CodeBehind="EmployeeRssFeeds.aspx.cs" Inherits="HealToZeal.Employee.EmployeeRssFeeds" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script runat="server">
    public void ProcessRSSItem(string rssURL)
    {
        System.Net.WebRequest myRequest = System.Net.WebRequest.Create(rssURL);
        System.Net.WebResponse myResponse = myRequest.GetResponse();
 
        System.IO.Stream rssStream = myResponse.GetResponseStream();
        System.Xml.XmlDocument rssDoc = new System.Xml.XmlDocument();
        rssDoc.Load(rssStream);
 
        System.Xml.XmlNodeList rssItems = rssDoc.SelectNodes("rss/channel/item");
 
        string title = "";
        string link = "";
        string description = "";
 
        for (int i = 0; i < rssItems.Count; i++)
        {
            System.Xml.XmlNode rssDetail;
 
            rssDetail = rssItems.Item(i).SelectSingleNode("title");
            if (rssDetail != null)
            {
                title = rssDetail.InnerText;
            }
            else
            {
                title = "";
            }
 
            rssDetail = rssItems.Item(i).SelectSingleNode("link");
            if (rssDetail != null)
            {
                link = rssDetail.InnerText;
            }
            else
            {
                link = "";
            }
 
            rssDetail = rssItems.Item(i).SelectSingleNode("description");
            if (rssDetail != null)
            {
                description = rssDetail.InnerText;
            }
            else
            {
                description = "";
            }
 
            Response.Write("<p><b><a href='" + link + "' target='new'>" + title + "</a></b><br/>");
            Response.Write(description + "</p>");
        }
    }
</script>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="EmployeeColumn" runat="server">
    
     <div>
            <%        
                string rssURL = "";
                // string  rssURL = "https://medlineplus.gov/feeds/news_en.xml";
                //// Response.Write("<font size=5><b>Site: " + rssURL + "</b></font><Br />");
                // ProcessRSSItem(rssURL);
                Response.Write("<hr />");

                rssURL = "https://medlineplus.gov/feeds/news_en.xml";
                //  Response.Write("<font size=5><b>Site: " + rssURL + "</b></font><Br />");
                ProcessRSSItem(rssURL);

            %>
        </div>
</asp:Content>
