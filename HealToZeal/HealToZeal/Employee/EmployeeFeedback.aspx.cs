﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BE;
using DAL;
using System.Data;

namespace HealToZeal.Employee
{
    public partial class EmployeeFeedback : System.Web.UI.Page
    {
        string EmployeeId = "";
        UsersFeedbackDAL objUsersFeedbackDAL = new UsersFeedbackDAL();
        string QuestionId = "";

        protected void Page_Load(object sender, EventArgs e)
        {
            Session["Heading"] = "";
            divalert.Visible = false;
            if (!Page.IsPostBack)
            {
                //Session["Pagename"] = null;
                //EmployeeId = Session["EmployeeId"].ToString();
                BindFeedbackQuestions();
            }
        }

        private void BindFeedbackQuestions()
        {
            try
            {
               DataTable DtQuestions= objUsersFeedbackDAL.FeedbackQuestionMasterGet();
               if (DtQuestions != null && DtQuestions.Rows.Count > 0)
               {
                   DlistQuestions.DataSource = DtQuestions;
                   DlistQuestions.DataBind();
               }
            }
            catch 
            {
               
            }
        }

        protected void DlistQuestions_ItemDataBound(object sender, DataListItemEventArgs e)
        {
            try
            {               
                RadioButtonList rblistAnswers = (RadioButtonList)e.Item.FindControl("rblistAnswers");
                Label lblQuestionId = (Label)e.Item.FindControl("lblQuestionId");

                TextBox  txtFeedback = (TextBox )e.Item.FindControl("txtFeedback");
                QuestionId = lblQuestionId.Text;

                DataTable DtAnswers = objUsersFeedbackDAL.FeedbackAnswerMasterGetById(QuestionId);
               
                if (DtAnswers != null && DtAnswers.Rows.Count > 0)
                {
                    rblistAnswers.DataSource = DtAnswers;
                    rblistAnswers.DataTextField = "FeedbackAnswer";
                    rblistAnswers.DataValueField = "FeedbackAnswerId_PK";
                    rblistAnswers.DataBind();
                    rblistAnswers.Visible = true ;
                    txtFeedback.Visible = false ;
                }
                else
                {
                    rblistAnswers.Visible = false;
                    txtFeedback.Visible = true;
                }
                //DataTable DtAnswers = objAnswerMasterDAL.AnswerMasterGetByQuestionId(QuestionId);
               
            }
            catch 
            {

            }
        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            try
            {
               int Status= SaveFeedBackAnswers();
               if (Status > 0)
               {
                   //Response.Redirect("EmployeeHome.aspx?FeedBack=1");
                   Response.Redirect("EmployeeAssessments.aspx");
               }
               else
               { 
                    
               }
            }
            catch 
            {

            }
        }

        private int SaveFeedBackAnswers()
        {
            int status = 0;
            try
            {            
                  UsersFeedbackBE objUsersFeedbackBE = new UsersFeedbackBE();
              
                EmployeeId = Session["EmployeeId"].ToString();
                objUsersFeedbackBE.propEmployeeId_FK = EmployeeId;            
                objUsersFeedbackBE.propCreatedBy = EmployeeId;
                objUsersFeedbackBE.propCreatedDate = DateTime.Now.Date;

                foreach(DataListItem answerItem in DlistQuestions.Items)
                {
                    QuestionId = ((Label)answerItem.FindControl("lblQuestionId")).Text;
                    string AnswerId = "";
                    RadioButtonList rblistAnswers = (RadioButtonList)answerItem.FindControl("rblistAnswers");
                    foreach (ListItem rdo in rblistAnswers.Items)
                    {
                        if (rdo.Selected)
                        {
                            AnswerId = rdo.Value;
                        }
                    }
                    TextBox txtFeedback = (TextBox)answerItem.FindControl("txtFeedback");
                    if (txtFeedback.Text.Length <= 0)
                    {
                        objUsersFeedbackBE.propFeedbackAnswerId_FK = AnswerId;

                    }
                    else
                    {
                        objUsersFeedbackBE.propFeedbackAnswerId_FK = Guid.Empty.ToString()  ;
                    }
                        objUsersFeedbackBE.propFeedback = txtFeedback.Text;                    
                    objUsersFeedbackBE.propFeedbackQuestionId_FK = QuestionId;
                    divalert.Visible = true;

                    status = objUsersFeedbackDAL.UsersFeedbackInsert(objUsersFeedbackBE);
                    {
                        if (status <= 0)
                        {
                          
                            divalert.Attributes.Add("class", "alert alert-warning");
                            lblMessage.Text = "Please select answer";
                            break;
                        }
                        else
                        {                      
                            lblMessage.Text = "";
                        }
                    }
                }               
            }
            catch 
            {
                
            }
            return status;
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            try
            {
                Response.Redirect("EmployeeHome.aspx");
            }
            catch 
            {   

            }
        }
    }
}