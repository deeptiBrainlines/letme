﻿using BE;
using DAL;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace HealToZeal.Employee
{
    public partial class EmpRegistrationForm_Activation1 : System.Web.UI.Page
    {
        log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        EmployeeDAL objEmployeeDAL = new EmployeeDAL();
        EmployeeBE objEmployeeBE = new EmployeeBE();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                EmployeeDAL objEmployeeDAL = new EmployeeDAL();
                // string verificationcode = !string.IsNullOrEmpty(Request.QueryString["verificationcode"]) ? Request.QueryString["verificationcode"] : Guid.Empty.ToString();

                updatevarificationflag();


            }
        }

        private void getCouponPrice()
        {
           // int Status = 0;
            try
            {

            }
            catch (Exception e)
            {
                e.InnerException.ToString();
            }
        }

        private void updatevarificationflag()
        {
            int Status = 0;
            try
            {
                if (Request.QueryString["verificationcode"] != null)
                {
                    string verificationcode = !string.IsNullOrEmpty(Request.QueryString["verificationcode"]) ? Request.QueryString["verificationcode"] : Guid.Empty.ToString();

                    EmployeeDAL objEmployeeDAL = new EmployeeDAL();
                    EmployeeBE objEmployeeBE = new EmployeeBE();
                    objEmployeeBE.Verificationcode = Convert.ToString(verificationcode);
                    DataTable dtStatus = objEmployeeDAL.GetEmployee(objEmployeeBE);
                    if (dtStatus != null && dtStatus.Rows.Count > 0 && Convert.ToString(dtStatus.Rows[0]["VarificationFlag"]) == "")
                    {
                        Status = objEmployeeDAL.UpdateVarificationFlag(objEmployeeBE);
                        Session["verificationcode"] = verificationcode;


                        objEmployeeBE.VerificationFlag = "1";
                        objEmployeeBE.PaymentFlag = "0";
                        objEmployeeBE.Verificationcode = verificationcode;


                        DataTable dt = objEmployeeDAL.GetEmployee(objEmployeeBE);
                        if (dt != null && dt.Rows.Count > 0)
                        {
                            string FirstName = dt.Rows[0]["FirstName"].ToString();
                            string LastName = dt.Rows[0]["LastName"].ToString();
                            string Email = dt.Rows[0]["EmailId"].ToString();
                            string Password1 = dt.Rows[0]["Password"].ToString();
                            String Password = Encryption.Decrypt(Password1);
                            string UserName = dt.Rows[0]["UserName"].ToString();
                            Session["CompanyId"] = dt.Rows[0]["CompanyID_FK"].ToString();
                            Session["EmployeeID"] = dt.Rows[0]["EmployeeId_PK"].ToString();
                            Session["UserFullName"] = FirstName + " " + LastName;
                            // Response.Redirect("PaymentGateway.aspx");
                            SendActivationEmailNewTemplate(Email, FirstName, LastName, UserName, Password);
                        }

                       // Response.Redirect("EmployeeLogin.aspx");
                        Response.Redirect("FirstLoginEmailVerifiedFreeTest.aspx");
                    }
                    else
                    {
                        if (dtStatus.Rows.Count == 0)
                        {
                            Response.Redirect("EmployeeLogin.aspx");
                        }
                        else
                        {
                            lblMessage1.Text = "you have already used Link";
                            ClientScript.RegisterStartupScript(this.GetType(), "Pop", "openModalAlert();", true);
                        }
                    }
                }
            }
            catch (Exception e)
            {
                log.Error(e.Message.ToString() + " " + e.StackTrace.ToString());
                //lblMessage1.Text = e.InnerException.ToString();
                //ClientScript.RegisterStartupScript(this.GetType(), "Pop", "openModal1();", true);
            }
        }


        private void SendActivationEmailNewTemplate(string Email,string FirstName,string LastName,string UserName,string Password)
        {
            try
            {
                //string DefaultImagePath = HttpContext.Current.Server.MapPath(@"~\UIFolder\dist\img\logotemp.png");
                //byte[] imageArray = System.IO.File.ReadAllBytes(DefaultImagePath);
                //string base64ImageRepresentation = Convert.ToBase64String(imageArray);

                System.Net.Mail.MailMessage mail = new System.Net.Mail.MailMessage();
                var DecintellURl = ConfigurationManager.AppSettings.Get("Activationurl");
                string verificationcode = Session["verificationcode"].ToString();
                mail.To.Add(Email);
                mail.CC.Add("contact@letmetalk2.me");
                mail.Bcc.Add("priyamvada.bavadekar@letmetalk2.me");
                mail.Subject = "Welcome to the Health and Personality Insights and Actions Tool.";
                string getDt = DateTime.Now.Date.AddDays(7).ToString("MM/dd/yyyy");
                //StringBuilder body = new StringBuilder();
                //body.Append("<body>");
                //body.Append("<table width='600' border='0' align='center' cellpadding='0' cellspacing='0' style='border:2px solid #b3c8c8 !important'>");
                //body.AppendLine("<tr>");
                //body.AppendLine("<td><table width='100%' border='0' cellspacing='0' cellpadding='0'>");
                //body.AppendLine("<tr>");
                //body.AppendLine("<td valign='top'><table width='90%' border='0' align='center' cellpadding='0' cellspacing='0'>");
                //body.AppendLine("<tr>");
                //body.AppendLine("<td  style='font-family:Verdana, Arial, Helvetica, sans-serif; font-size:12px; line-height:1.5; text-align:justify;'&nbsp;</td>");
                //body.AppendLine("</tr>");
                //body.AppendLine("<tr>");
                //body.AppendLine("<td  style='font-family:Verdana, Arial, Helvetica, sans-serif;  font-size:13px; line-height:1.8; text-align:justify;'><div style='font-size:18px; color:#5b6766; font-weight:800;'>Hello " + FirstName + ", </div>");
                //body.AppendLine("<div style='clear:both;'></div>");
                //body.AppendLine("<p style='color:#829a9a; font-weight:800; font-size:14px; margin-bottom:0; padding-bottom:0;'>Please click the following link to activate your account  </p>");
                //body.AppendLine("<a href='" + DecintellURl + "" + verificationcode + "' target='_blank' ");
                //body.AppendLine("style ='background-color: #5b756c;");
                //body.AppendLine("padding: 8px 10px;");
                //body.AppendLine("text-align: center;");
                //body.AppendLine("text-decoration: none;");
                //body.AppendLine("display: inline-block;");
                //body.AppendLine("font-size: 15px;");
                //body.AppendLine("color: #fff;");
                //body.AppendLine("border-radius: 25px; margin-top:10px;'");
                //body.AppendLine(">Click here</a>");
                //body.AppendLine("</td>");
                //body.AppendLine("</tr>");
                //body.AppendLine("<tr>");
                //body.AppendLine("<td  style='font-family:Verdana, Arial, Helvetica, sans-serif; font-size:12px; line-height:1.5; text-align:justify;'>&nbsp;</td>");
                //body.AppendLine("</tr>");
                //body.AppendLine("</table></td>");
                //body.AppendLine("</tr>");
                //body.AppendLine("<tr>");
                //body.AppendLine("<td height='140' bgcolor='#b3c8c8' style='font-family:Verdana, Arial, Helvetica, sans-serif; font-size:20px; color:#fff;'><table width='90%' border='0' align='center' cellpadding='0' cellspacing='0'>");
                //body.AppendLine("<tr>");
                //body.AppendLine("<td width='23%'><img src='" + ConfigurationManager.AppSettings.Get("ImagePath") + @"\UIFolder\dist\img\logotemp.png' width ='120' height='122' /></td>");
                //body.AppendLine("<td width='2%'>&nbsp;</td>");
                //body.AppendLine("<td width='75%' align='left'><table width='100%' border='0' align='left' cellpadding='0' cellspacing='0'>");
                //body.AppendLine("<tr>");
                //body.AppendLine("<td style='color:#5b756c'><strong>let me talk team</strong></td>");
                //body.AppendLine("</tr>");
                //body.AppendLine("<tr>");
                //body.AppendLine("<td height='28' style='font-size:14px; border-bottom:1px dotted #5b756c; padding-bottom:5px; color:#5b756c'>self test program</td>");
                //body.AppendLine("</tr>");
                //body.AppendLine("<tr>");
                //body.AppendLine("<td style='font-size:12px; padding-top:5px; color:#5b756c'>http://score.letmetalk2.me/</td>");
                //body.AppendLine("</tr>");
                //body.AppendLine("<tr>");
                //body.AppendLine("<td height='20' style='font-size:12px; color:#5b756c'><span style='font-size:12px; padding-top:5px;'>©2020 let me talk. all rights reserved.</span></td>");
                //body.AppendLine("</tr>");
                //body.AppendLine("</table></td>");
                //body.AppendLine("</tr>");
                //body.AppendLine("</table></td>");
                //body.AppendLine("</tr>");
                //body.AppendLine("</table></td>");
                //body.AppendLine("</tr>");
                //body.AppendLine("</table>");
                //body.AppendLine("</body>");
                //mail.Body = body.ToString();
               // string loginurl = ConfigurationManager.ConnectionStrings["Loginurl"].ConnectionString;
                var loginurl = ConfigurationManager.AppSettings.Get("Loginurl");
                mail.Body = "<html><body><font color=\"black\">Dear " + FirstName + " " + LastName + ",<br/><br/>Welcome to <b> Let Me Talk To Me: A tool to quickly and discreetly assess your mindfulness.</b> You will be able to gain some useful insights into your own personality and learn about remedial actions in case they are needed. <b>'Let Me Talk2 Me'</b> will help you in self-discovery. It will lead on a path of self reflections while avoiding pitfalls of modern complex life and careers. <br/><br/>It is recommended that you use this tool periodically to monitor yourself and be aware of your health and personality parameters. <br/><br/> Please use the link below to login <br/><br/><a href='" + loginurl + "?first=1'>" + loginurl + " </a><br/>Your login details are as follows:<br/>UserId:<b>" + UserName + "</b><br/>Password:<b>" + Password + "</b><br/><br/>If you would like to take a break during assessment, please make sure you log out from the system or close the browser window for security purposes. Please do not leave the browser idle. You can always log back in whenever you like.<br/><br/>Please ensure that you complete this assessment on or before " + getDt + ". To get a maximum benefit, please repeat this exercise every 3 months and study the differentials.<br/> You will be notified for the same when the next assessment is due.<br/><br/>If you wish to join the ecosystem of mindfulness, please pre-register with us.<a href='http://score.letmetalk2.me/pre-register'>http://score.letmetalk2.me/pre-register</a></br>Thank You.<br/><br/></font></body></html>";
                mail.IsBodyHtml = true;

                mail.Priority = System.Net.Mail.MailPriority.High;
                //SmtpClient client = new SmtpClient();
                //client.EnableSsl = false;
                //client.Send(mail);
                //log.Info(" Send Mail Successfully.");
                SmtpClient client = new SmtpClient();
                client.EnableSsl = false;
                client.UseDefaultCredentials = true;
                client.DeliveryMethod = SmtpDeliveryMethod.Network;
                client.Send(mail);
                log.Info(" Send Mail Successfully.");

            }
            catch (Exception ex)
            {
                // Response.Write(@"<script language='javascript'>alert(" + ex.Message.ToString() + ");</script>");
                lblMessage1.Text = ex.Message.ToString();
                ClientScript.RegisterStartupScript(this.GetType(), "Pop", "openModal();", true);
            }

        }

        private void GetEmployeeDetails()
        {
            int Status = 0;
            try
            {
                string loginurl = ConfigurationManager.ConnectionStrings["Loginurl"].ConnectionString;

                EmployeeDAL objEmployeeDAL = new EmployeeDAL();
                EmployeeBE objEmployeeBE = new EmployeeBE();
                objEmployeeBE.VerificationFlag = "1";
                objEmployeeBE.PaymentFlag = "1";
                DataTable dt = objEmployeeDAL.GetEmployeeDetails(objEmployeeBE);
                string FirstName = dt.Rows[0]["FirstName"].ToString();
                string LastName = dt.Rows[0]["LastName"].ToString();
                string Email = dt.Rows[0]["EmailId"].ToString();
                string UserName = dt.Rows[0]["UserName"].ToString();
                string Password = dt.Rows[0]["Password"].ToString();
                Session["EmployeeID"] = dt.Rows[0]["EmployeeId_PK"].ToString();
                Status = objEmployeeDAL.UpdateVarificationFlag(objEmployeeBE);
                System.Net.Mail.MailMessage mail = new System.Net.Mail.MailMessage();


                mail.To.Add(Email);
                mail.CC.Add("contact@letmetalk2.me");
                mail.Bcc.Add("priyamvada.bavadekar@letmetalk2.me");
                // mail.From = "contact@letmetalk2.me";
                // mail.From = new MailAddress("contact@letmetalk2.me", "Let me talk to me", System.Text.Encoding.UTF8);
                mail.Subject = "Welcome to the Health and Personality Insights and Actions Tool.";
                string getDt = DateTime.Now.Date.AddDays(7).ToString("MM/dd/yyyy");

                // mail.Body = "<html><body><font color=\"black\">Dear " + FirstName + " " + LastName + ",<br/><br/>Welcome to <b> Let Me Talk2 Me: A tool to quickly and discreetly assess your mindfulness.</b> You will be able to gain some useful insights into your own personality and learn about remedial actions in case they are needed. <b>'Let Me Talk2 Me'</b> will help you in self-discovery. It will lead on a path of self reflections while avoiding pitfalls of modern complex life and careers. <br/><br/>It is recommended that you use this tool periodically to monitor yourself and be aware of your health and personality parameters. <br/><br/> Please use the link below to login <br/><br/><a href='http://letmetalk2.me.hazel.arvixe.com/Employee/EmployeeLogin.aspx?first=1'> http://letmetalk2.me.hazel.arvixe.com/Employee/EmployeeLogin.aspx </a><br/>Your login details are as follows:<br/>UserId:<b>" + UserName + "</b><br/>Password:<b>" + Password + "</b><br/><br/>If you would like to take a break during assessment, please make sure you log out from the system or close the browser window for security purposes. Please do not leave the browser idle. You can always log back in whenever you like.<br/><br/>Please ensure that you complete this assessment on or before " + getDt + ". To get a maximum benefit, please repeat this exercise every 3 months and study the differentials.<br/> You will be notified for the same when the next assessment is due.<br/><br/>If you wish to join the ecosystem of mindfulness, please pre-register with us.<a href='http://letmetalk2.me/pre-register'>http://letmetalk2.me/pre-register</a></br>Thank You.<br/><br/></font></body></html>";
                mail.Body = "<html><body><font color=\"black\">Dear " + FirstName + " " + LastName + ",<br/><br/>Welcome to <b> Let Me Talk To Me: A tool to quickly and discreetly assess your mindfulness.</b> You will be able to gain some useful insights into your own personality and learn about remedial actions in case they are needed. <b>'Let Me Talk2 Me'</b> will help you in self-discovery. It will lead on a path of self reflections while avoiding pitfalls of modern complex life and careers. <br/><br/>It is recommended that you use this tool periodically to monitor yourself and be aware of your health and personality parameters. <br/><br/> Please use the link below to login <br/><br/><a href='" + loginurl + "?first=1'>" + loginurl + " </a><br/>Your login details are as follows:<br/>UserId:<b>" + UserName + "</b><br/>Password:<b>" + Password + "</b><br/><br/>If you would like to take a break during assessment, please make sure you log out from the system or close the browser window for security purposes. Please do not leave the browser idle. You can always log back in whenever you like.<br/><br/>Please ensure that you complete this assessment on or before " + getDt + ". To get a maximum benefit, please repeat this exercise every 3 months and study the differentials.<br/> You will be notified for the same when the next assessment is due.<br/><br/>If you wish to join the ecosystem of mindfulness, please pre-register with us.<a href='http://letmetalk2.me/pre-register'>http://letmetalk2.me/pre-register</a></br>Thank You.<br/><br/></font></body></html>";

                mail.IsBodyHtml = true;

                mail.Priority = System.Net.Mail.MailPriority.High;
                SmtpClient client = new SmtpClient();
                // client.Credentials = new System.Net.NetworkCredential("contact@letmetalk2.me", "contact123");
                //  client.Port = 587;
                // client.Host = "dallas137.arvixeshared.com";
                client.EnableSsl = false;
                client.Send(mail);


            }
            catch (Exception e)
            {
                e.InnerException.ToString();
            }
        }

    }
}