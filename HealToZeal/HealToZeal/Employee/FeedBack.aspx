﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master/LetMeTalkWithMenu.Master" AutoEventWireup="true" CodeBehind="FeedBack.aspx.cs" Inherits="HealToZeal.Employee.FeedBack" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style>
        .iconpadleft{
            padding-left:50px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="row">
        <div class="col-md-11 col-centered boardphome-bt">
            <div class="boardp bdr3" style="height: auto !important;">

                <div style="padding: 0 20px;">
                    <div class="col-md-12 col-centered mt20">
                        <div style="margin-bottom: 0px; border-bottom: 3px solid #FFFFFF; padding-bottom: 15px;">
                            <div style="font-size: 36px; line-height: 1;" class="w600">your feedback<span style="font-size: 22px; line-height: 1; font-weight: 500"> is highly appreciated and will help us to improve our ability to serve you and other users. </span></div>
                        </div>
                        <div style="clear: both;"></div>
                    </div>
                    <div class="row">
                        <div id="divalert" class="alert alert-success" runat="server">
                            <strong>
                                <asp:Label ID="lblMessage" runat="server"></asp:Label></strong>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-12 col-md-12 col-sm-12">
                            <asp:DataList ID="DlistQuestions" runat="server" OnItemDataBound="DlistQuestions_ItemDataBound">
                                <ItemTemplate>
                                    <div class="col-md-12 col-centered">
                                        <div style="border-bottom: 3px solid #fff;">
                                        <h3 style="line-height: 1.1;">
                                           <div>
                                                    <asp:Label ID="lblSrNo" runat="server" Text='<%#Eval("SrNo")+". " %>'></asp:Label>
                                                    <asp:Label ID="lblQuestionId" runat="server" Text='<%#Eval("FeedbackQuestionId_PK") %>' Visible="false"></asp:Label>
                                                    <asp:Label ID="lblQuestion" runat="server" Text='<%#Eval("FeedbackQuestion") %>' ></asp:Label>

                                               <div class="clearfix"></div>
                                                    <div>
                                                        <div class="ans mt20" style="height: auto !important;">
                                                            
                                                                <asp:RadioButtonList ID="rblistAnswers" runat="server" RepeatDirection="Horizontal" CssClass="radio-inline ">
                                                                </asp:RadioButtonList>
                                                                <asp:TextBox ID="txtFeedback" runat="server" Rows="20" Columns="20" CssClass="form-control" Width="100%"></asp:TextBox>

                                                                <div class="clearfix"></div>
                                                                
                                                                <br>
                                                        </div>
                                                    </div>
                                                </div>
                                        </h3>
                                        </div>
                                        <div style="clear:both;">
                                    </div>

                                    <div class="clearfix"></div>

                                </ItemTemplate>
                            </asp:DataList>

                            <div class="pull-right mt30">
                                <a href="#">
                                    <asp:Button ID="btnSubmit" runat="server" Text="Submit" CssClass="loginbtn"
                                        OnClick="btnSubmit_Click" />
                                </a><a href="#">
                                    <asp:Button ID="btnCancel" runat="server" Text="Cancel" CssClass="loginbtn"
                                        OnClick="btnCancel_Click" />
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div style="clear: both;"></div>
        </div>
    </div>
</asp:Content>
