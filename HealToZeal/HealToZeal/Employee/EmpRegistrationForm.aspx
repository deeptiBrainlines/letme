﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master/LetMeTalk.Master" AutoEventWireup="true" CodeBehind="EmpRegistrationForm.aspx.cs" Inherits="HealToZeal.Employee.EmpRegistrationForm1" %>

<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="Server">
    <script type="text/javascript">
        function openModal() {
            $('[id*=myModal]').modal('show');
        }
       
    </script>
</asp:Content>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div style="clear: both;"></div>
    <div>
        <section class="content">
            <div class="row">
                <div class="col-md-11 col-centered">
                    <div class="nav-tabs-custom">
                        <ul class="nav nav-tabs">
                            <li><a href="EmployeeLogin.aspx">sign in</a></li>
                            <%--<li class="active"><a href="EmpRegistrationForm?InstanceCompanyRelationId=141C5CFB-4666-4576-B5C9-FADA9522DD2A&CouponId=1">sign up</a></li>--%>
                            <li class="active"><a href="EmpRegistrationForm?InstanceCompanyRelationId=3FB35DBD-00B3-4704-BA2C-BC7DC3F244AF&CouponId=1">sign up</a></li>
                        </ul>
                        <div class="tab-content">
                            <div class="bdr3" style="padding-top: 10px;">
                                <div class="col-md-12">
                                    <div class="row">
                                        <div class="col-md-9 col-centered">
                                            <div class="col-md-6 col-sm-6">
                                                <div class="form-group mb0">
                                                    <h4 class="fwnormal mb">first name</h4>
                                                    <%-- <input name="" type="text" class="form-control">--%>
                                                    <asp:TextBox ID="txtfirstname" runat="server" MaxLength="50" class="form-control"></asp:TextBox>
                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ControlToValidate="txtfirstname" ErrorMessage="* please enter your first name" ForeColor="#CC0000"></asp:RequiredFieldValidator>

                                                    <%-- <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" TargetControlID="txtfirstname" FilterType="LowercaseLetters,UppercaseLetters,Custom" runat="server"></asp:FilteredTextBoxExtender>--%>
                                                    <%-- <ajaxToolkit:HtmlEditorExtender ID="replyBody_HtmlEditorExtender" runat="server" Enabled="True" OnImageUploadComplete="saveFile"  ClientIDMode="AutoID" EnableSanitization="true"  TargetControlID="replyBody">--%>
                                                </div>
                                            </div>
                                            <div class="col-md-6 col-sm-6">
                                                <div class="form-group mb0">
                                                    <h4 class="fwnormal mb ">last name</h4>
                                                    <%--<input name="" type="text" class="form-control">--%>
                                                    <asp:TextBox ID="txtlastname" runat="server" MaxLength="50" class="form-control"></asp:TextBox>
                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtlastname" ErrorMessage="* please enter your last name" ForeColor="#CC0000"></asp:RequiredFieldValidator>

                                                    <%--                         <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" TargetControlID="txtlastname" FilterType="LowercaseLetters,UppercaseLetters,Custom" runat="server"></asp:FilteredTextBoxExtender>--%>
                                                </div>
                                            </div>
                                            <div class="col-md-6 col-sm-6">
                                                <div class="form-group mb0">
                                                    <h4 class="fwnormal mb ">mobile no.</h4>
                                                    <%--<input name="" type="text" class="form-control">--%>
                                                    <asp:TextBox ID="txtphoneno" runat="server" MaxLength="10"  class="form-control"></asp:TextBox>

                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="txtphoneno" ErrorMessage="* please enter your phone no" ForeColor="#CC0000"></asp:RequiredFieldValidator>
                                                    <div style="clear: both;"></div>
                                                    <asp:RegularExpressionValidator ID="RegularExpressionValidator2" ControlToValidate="txtphoneno" runat="server" ForeColor="#CC0000" ErrorMessage="Only Numbers allowed" ValidationExpression="\d+"></asp:RegularExpressionValidator>
                                                </div>
                                            </div>

                                            <div class="col-md-6 col-sm-6">
                                                <div class="form-group mb0">
                                                    <h4 class="fwnormal mb">email</h4>
                                                    <%-- <input name="" type="text" class="form-control">--%>
                                                    <asp:TextBox ID="txtemail" runat="server" class="form-control"> </asp:TextBox>

                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="txtemail" ErrorMessage="* please enter your email id" ForeColor="#CC0000"></asp:RequiredFieldValidator>
                                                    <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="txtemail" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" ForeColor="#CC0000" ErrorMessage="* Invalid Email Id"></asp:RegularExpressionValidator>
                                                </div>
                                            </div>

                                            <%-- <div class="col-md-6 col-sm-6">
                                                <div class="form-group">
                                                    <asp:Label ID="lblMessage" runat="server" Text="Label" Visible="false"></asp:Label>
                                                </div>
                                            </div>--%>

                                            <div class="clearfix"></div>
                                            <div class="col-md-8 col-sm-8">
                                                <div class="form-group">
                                                    <div class="form-check pull-left">
                                                        <asp:CheckBox ID="chkTandD" runat="server" Height="22px" MaxLength="50" />
                                                        <label class="fwnormal mb" style="font-size: 18px; margin-right: 30px;">accept T&amp;C </label>

                                                        <%--<a href="#" class="loginbtn" style="margin-right:30px;">sign up</a>--%>
                                                        <asp:Button ID="Button1" runat="server" Style="margin-right: 30px;" Text="sign up" CssClass="loginbtn" OnClick="btnsubmit_Click" />
                                                    </div>
                                                    <h4 class="txtwhite" style="float: left;">OR</h4>
                                                </div>
                                                <div style="clear: both;"></div>
                                                <asp:Label ID="lblChkMsg" runat="server" ForeColor="#CC0000"></asp:Label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                    <div class="col-md-11 col-centered mt30">
                                        <div class="col-md-3">
                                            <div class="bdr2 dview"></div>
                                        </div>
                                        <div class="col-md-6 pad0">
                                            <center>
                      <h4 style="margin-top:-5px;">SIGN-UP USING A DIFFERENT ACCOUNT</h4>
                    </center>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="bdr2 dview"></div>
                                        </div>
                                    </div>
                                    <div class="col-md-11 pb20 col-sm-12 col-centered">
                                        <div class="col-md-4 socailbt0">

                                            <asp:ImageButton ID="ImageButton1" runat="server" CssClass=" social-btnw img-responsive mb20 social-btns-bdr" ToolTip="Login With Facebook" ImageUrl="../UIFolder/dist/img/bt-facebook.png" CausesValidation="false" OnClick="FBLogin_Click" />
                                        </div>
                                        <div class="col-md-4 socailbt0">
                                            <asp:ImageButton ID="ImageButton2" runat="server" CssClass="social-btnw img-responsive mb20 social-btns-bdr" ToolTip="Login With Gmail" ImageUrl="../UIFolder/dist/img/bt-google.png" CausesValidation="false" OnClick="btngmaillogin_Click" />
                                        </div>
                                        <div class="col-md-4 socailbt0">
                                            <asp:ImageButton CssClass=" social-btnw img-responsive social-btns-bdr" runat="server" ImageUrl="../UIFolder/dist/img/bt-linkdin.png" ToolTip="Continue with Linkedin" />
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                </div>
            </div>
            <div id="myModal" class="modal" role="dialog">
                <div class="modal-dialog modal-md">
                    <div class="modal-content">

                        <div class="modal-body">
                            <div class="col-md-12 mt30">
                                <h4 class="modal-title">message</h4>
                                <p class="w400">
                                    <asp:Label ID="lblMessage1" runat="server"></asp:Label>
                                </p>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <a href="">
                                <img src="../UIFolder/dist/img/close.png" data-dismiss="modal" class="popicon"></a>
                        </div>
                    </div>
                </div>
            </div>

        </section>

    </div>


</asp:Content>
