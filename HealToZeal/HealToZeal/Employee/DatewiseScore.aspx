﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="DatewiseScore.aspx.cs" MasterPageFile="~/Employee/Employee.Master" Inherits="HealToZeal.Employee.DatewiseScore" %>

<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=11.0.0.0, Culture=neutral, PublicKeyToken=89845dcd8080cc91" Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="EmployeeColumn" runat="server">
       
             <h5>  <asp:LinkButton ID="lbBack" runat="server" PostBackUrl="~/Employee/EmployeeAssessments.aspx" CssClass="btn btn-link btn-lg">Back</asp:LinkButton></h5>
    <h4 class="hs_heading" id="hs_appointment_form_link">Graph
    </h4>
      
      <div>
        <br />
&nbsp;<rsweb:ReportViewer ID="ReportViewer1" runat="server" Font-Names="Verdana" Font-Size="8pt" Height="483px" WaitMessageFont-Names="Verdana" WaitMessageFont-Size="14pt" Width="746px" ShowBackButton="False" ShowFindControls="False" ShowPageNavigationControls="False" BackColor="White" AsyncRendering="False" ShowParameterPrompts="False" ShowPrintButton="False" ShowPromptAreaButton="False" ShowRefreshButton="False" ShowToolBar="False" ShowWaitControlCancelLink="False" ShowZoomControl="False">
            <LocalReport ReportPath="Reports\DatewiseScore.rdlc">
                <DataSources>
                    <rsweb:ReportDataSource DataSourceId="ObjectDataSource1" Name="DataSet1" />
                </DataSources>
            </LocalReport>
        </rsweb:ReportViewer>
        <asp:ObjectDataSource ID="ObjectDataSource1" runat="server" OldValuesParameterFormatString="original_{0}" SelectMethod="GetData" TypeName="HealToZeal.DatewiseScoreTableAdapters.SpDatewiseScoreByEmployeeCompanyIdTableAdapter">
            <SelectParameters>
                <asp:SessionParameter DefaultValue="" Name="EmployeeId" SessionField="EmployeeId" DbType="Guid" />
            </SelectParameters>
        </asp:ObjectDataSource>
    </div>
    <div>
          <rsweb:ReportViewer ID="ReportViewer2" runat="server" AsyncRendering="False" Font-Names="Verdana" Font-Size="8pt" Height="478px" WaitMessageFont-Names="Verdana" WaitMessageFont-Size="14pt" Width="1089px" ShowBackButton="False" ShowExportControls="False" ShowFindControls="False" ShowPageNavigationControls="False" ShowPrintButton="False" ShowRefreshButton="False" ShowToolBar="False" ShowWaitControlCancelLink="False" ShowZoomControl="False">
              <LocalReport ReportPath="Reports\DatewiseAllScores.rdlc">
                  <DataSources>
                      <rsweb:ReportDataSource DataSourceId="ObjectDataSource2" Name="DataSet1" />
                  </DataSources>
              </LocalReport>
          </rsweb:ReportViewer>
          <asp:ObjectDataSource ID="ObjectDataSource2" runat="server" OldValuesParameterFormatString="original_{0}" SelectMethod="GetData" TypeName="HealToZeal.AllClustersTableAdapters.SpAllScoreByEmployeeCompanyIdTableAdapter">
              <SelectParameters>
                  <asp:SessionParameter DbType="Guid" Name="EmployeeId" SessionField="EmployeeId" />
              </SelectParameters>
          </asp:ObjectDataSource>
    </div>
</asp:Content>
