﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master/LetMeTalkWithMenu.Master" AutoEventWireup="true" CodeBehind="testLayout.aspx.cs" Inherits="HealToZeal.Employee.testLayout" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="row">

        <div class="col-md-11 col-centered boardphome-bt">
            <div class="row">

                <div class="col-md-11 col-centered">
                    <!-- First LayOut-->
                    <div class="col-md-12 pad0">
                        <div class="heading text-left">assessment</div>
                    </div>
                    <div style="clear: both;"></div>
                    <!-- First LayOut-->

                    <div class="row">

                        <asp:DataList ID="DlistQuestionsLayout2" runat="server" OnItemDataBound="DlistQuestions_ItemDataBound" Width="100%">
                            <ItemTemplate>
                                <div class="subtitle">
                                    <asp:Label ID="lblQuestion" runat="server" CssClass="quest" Text='<%#Eval("Question") %>'></asp:Label>

                                </div>
                                <div class="col-md-12 boardphome2 bg1 mt20">
                                    <div class="row mb20">
                                        <div class="col-md-11 col-sm-11 col-centered">
                                            <div class="pull-right">
                                                <%--<asp:Repeater ID="ansRepeater" runat="server">--%>
                                                    <div>
                                                        <%--<a href="#" class="mood-btn">Always </a>--%>
                                                        <asp:Button ID="btn1" CssClass="mood-btn" Text='<%#Eval("Question") %>' runat="server" />
                                                    </div>

                                                    <div class="clearfix"></div>
                                                    <div>
                                                        <%-- <a href="#" class="mood-btn">Rarely </a>--%>
                                                        <asp:Button ID="Button1" CssClass="mood-btn" Text='<%#Eval("Question") %>' runat="server" />
                                                    </div>
                                                    <div class="clearfix"></div>
                                                    <div>
                                                        <%-- <a href="#" class="mood-btn">Never </a>--%>
                                                        <asp:Button ID="Button2" CssClass="mood-btn" Text='<%#Eval("Question") %>' runat="server" />
                                                    </div>
                                                    <div class="clearfix"></div>
                                                    <div>
                                                        <%--<a href="#" class="mood-btn">Sometimes </a>--%>
                                                        <asp:Button ID="Button3" CssClass="mood-btn" Text='<%#Eval("Question") %>' runat="server" />
                                                    </div>
                                               <%-- </asp:Repeater>--%>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                    <div style="position: absolute; z-index: 100; width: 100%;" class="absolute">

                                        <div class="progress progress-striped active">
                                            <div runat="server" id="Progressbar1" class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div style="clear: both;"></div>
                            </ItemTemplate>
                        </asp:DataList>

                    
                    <asp:DataList ID="DlistQuestions" runat="server" OnItemDataBound="DlistQuestions_ItemDataBound" Width="100%">
                        <ItemTemplate>
                            <!-- First LayOut-->
                            <div class="mt20">
                                <div class="col-md-5 pad0">
                                    <div style="position: absolute; z-index: 100; width: 100%;" class="absolute">
                                        <div class="progress progress-striped active">
                                            <div runat="server" id="Div1" class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100">
                                            </div>
                                        </div>
                                    </div>
                                    <img src="../UIFolder/dist/img/ques1.jpg" class="img-responsive">
                                    <span class="sr-only"></span>
                                </div>

                                <div class="col-md-7">
                                    <div class="parent que-lm">
                                        `
                                <div>
                                    <div class="quest">
                                        <asp:Label ID="lblQuestionId" runat="server" Text='<%#Eval("QuestionId_FK") %>' Visible="false" onclick="getfocus()"></asp:Label>
                                        <asp:Label ID="lblSrNo" runat="server" Text='<%#Eval("SrNo")+". " %>'></asp:Label>
                                        <asp:Label ID="lblQuestion" runat="server" CssClass="quest" Text='<%#Eval("Question") %>'></asp:Label>
                                    </div>

                                    <div class="ans mt8">
                                        <asp:RadioButtonList ID="rblistAnswers" runat="server" RepeatDirection="Vertical" CssClass="radio-inline" OnSelectedIndexChanged="rblistAnswers_SelectedIndexChanged" AutoPostBack="true">
                                        </asp:RadioButtonList>
                                    </div>
                                    <div class="clearfix"></div>
                                    <div style="margin-bottom: 50px;" class="mt8">
                                        <asp:Button ID="btnPrevious" runat="server" Text="previous" CssClass="sm-bt pull-left" OnClick="btnPrevious_Click" />
                                        <asp:Button ID="btnNext" runat="server" Text="next" CssClass="sm-bt pull-right" OnClick="btnNext_Click" />
                                    </div>
                                </div>
                                    </div>
                                </div>
                            </div>
                            <div style="clear: both;"></div>
                            <!-- Second LayOut-->
                            <div class="row">

                                <div class="subtitle">
                                    <asp:Label ID="lblQuestion1" runat="server" CssClass="quest" Text='<%#Eval("Question") %>'></asp:Label>
                                </div>
                                <div class="col-md-12 boardphome2 bg1 mt20">
                                    <div class="row mb20">
                                        <div class="col-md-11 col-sm-11 col-centered">
                                            <div >
                                                <asp:Repeater ID="ansRepeater" runat="server">
                                                    <ItemTemplate>
                                                        <div>
                                                            <%--<a href="#" class="mood-btn">Always </a>--%>
                                                            <asp:Button ID="btn1" CssClass="mood-btn" Text='<%#Eval("AnswerShort") %>' runat="server" />
                                                        </div>

                                                        <div class="clearfix"></div>
                                                        <%-- <div>
                                                     <a href="#" class="mood-btn">Rarely </a>
                                                </div>
                                                <div class="clearfix"></div>
                                                <div>
                                                     <a href="#" class="mood-btn">Never </a>
                                                </div>
                                                <div class="clearfix"></div>
                                                <div>
                                                    <a href="#" class="mood-btn">Sometimes </a>
                                                </div>--%>
                                                    </ItemTemplate>
                                                </asp:Repeater>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                    <div style="position: absolute; z-index: 100; width: 100%;" class="absolute">

                                        <div class="progress progress-striped active">
                                            <div runat="server" id="Progressbar1" class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div style="clear: both;"></div>
                            </div>
                            <div style="clear: both;"></div>
                            <!-- Third LayOut-->
                            <div class="row">
                                <div class="boardp bdr3 parent" style="height: auto;">

                                    <div class="col-md-10 col-centered" style="margin-top: 40px; margin-bottom: 50px;">
                                        <div style="font-size: 36px; line-height: 1.2;" class="w600">
                                            <%-- I am open to getting
feedbacks about my
work in front of my co-workers.--%><asp:Label ID="Label1" runat="server" CssClass="quest" Text='<%#Eval("Question") %>'></asp:Label>

                                        </div>
                                    </div>

                                    <div class="col-md-11 col-centered" style="margin-top: 50px; margin-bottom: 40px;">


                                        <div style="clear: both;"></div>

                                        <div class="col-md-3 col-sm-3">
                                            <center><img src="../UIFolder/dist/img/Icons-03.png" class="img-responsive mb20"></center>
                                        </div>
                                        <div class="col-md-3 col-sm-3">
                                            <center><img src="../UIFolder/dist/img/Icons-04.png" class="img-responsive mb20"></center>
                                        </div>
                                        <div class="col-md-3 col-sm-3">
                                            <center><img src="../UIFolder/dist/img/Icons-05.png" class="img-responsive mb20"></center>
                                        </div>
                                        <div class="col-md-3 col-sm-3">
                                            <center><img src="../UIFolder/dist/img/Icons-06.png" class="img-responsive mb20"></center>
                                        </div>
                                        <div style="clear: both;"></div>

                                    </div>

                                    <div style="clear: both;"></div>
                                    <div class="clearfix"></div>
                                </div>
                                <div class="col-md-12 pad0" style="bottom: 0;">
                                    <div style="position: absolute; z-index: 100; width: 100%;" class="absolute">
                                        <div style="clear: both;"></div>
                                        <div class="progress">
                                            <div class="progress-bar" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 30%;" data-toggle="tooltip" data-placement="top" title="You're doing good! 60 more to go..">
                                                <span class="sr-only">20% Complete</span>
                                                <span class="progress-type"></span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </ItemTemplate>
                    </asp:DataList>

                    </div>
                </div>

            </div>
        </div>
    </div>
   
</asp:Content>
