﻿using BE;
using DAL;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace HealToZeal.Employee
{
    public partial class EmployeeMessage1 : System.Web.UI.Page
    {
        //log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        protected void Page_Load(object sender, EventArgs e)
        {

            if (!Page.IsPostBack)
            {
                Session["Heading"] = "instructions";
                if (Session["EmployeeId"] != null)
                {
                    EmployeeDAL objEmployeeDAL = new EmployeeDAL();
                    EmployeeBE objEmployeeBE = new EmployeeBE();
                    objEmployeeBE.propEmployeeId_PK = Convert.ToString(Session["EmployeeId"]);
                    DataTable empDT = objEmployeeDAL.GetEmployeebyEmpid(objEmployeeBE);
                    if (empDT != null && empDT.Rows.Count > 0 )
                    {
                        //if ( Convert.ToString(empDT.Rows[0]["TestFlag"]) != "free")
                        //{
                        //    Response.Redirect("FirstLoginEmailVerifiedFreeTest.aspx", false);
                        //}
                        
                    }
                    else
                    {

                    }
                }
                else
                {
                    Response.Redirect("EmployeeLogin.aspx");
                }
                
            }
        }

        protected void btnNext_Click(object sender, EventArgs e)
        {
            try
            {
                if (Request.QueryString["first"] != null)
                {
                    Response.Redirect("EmployeeHome.aspx?first=1", false);
                }
                else if (Request.QueryString["Assessment"] != null)
                {
                    Response.Redirect("EmployeeHome.aspx?Assessment=" + Request.QueryString["Assessment"].ToString());
                }
                else if (Request.QueryString["FeedBack"] != null)
                {
                    Response.Redirect("EmployeeHome.aspx?FeedBack=" + Request.QueryString["FeedBack"].ToString());
                }
                else
                {
                    Response.Redirect("EmployeeHome.aspx");

                }
            }
            catch (Exception ex)
            {
                //log.Error(ex.Message.ToString() + " " + ex.StackTrace.ToString());
            }

        }
    }
}