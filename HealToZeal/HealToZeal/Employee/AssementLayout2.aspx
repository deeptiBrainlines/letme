﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master/LetMeTalkWithMenu.Master" AutoEventWireup="true" CodeBehind="AssementLayout2.aspx.cs" Inherits="HealToZeal.Employee.AssementLayout2" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <link href="../Styles/Employee.css" rel="stylesheet" type="text/css" />
    <link href="../Styles/slider.css" rel="stylesheet" type="text/css">
    <script src="../Scripts/jquery.colorbox-min.js" type="text/javascript"></script>
    <script src="../Scripts/jquery.colorbox.js" type="text/javascript"></script>
    <script type="text/javascript">
        function DisplayColorBox(url) {

            $.fn.colorbox({ href: url, iframe: true, scrolling: true, width: '300px', position: 'Fixed', height: '150px', overlayClose: false });
            //, onClosed: function () { parent.location.reload(false); }
            return false;
        }
        function openModalQuestion() {
            $('[id*=openModalQuestion]').modal('show');
        }
    </script>

    <div class="row">
        <div class="col-lg-9">
            <div id="divalert" class="alert alert-success" runat="server">
                <strong>
                    <asp:Label ID="lblMessage" runat="server"></asp:Label>
                </strong>
            </div>
        </div>
        <div class="col-md-11 col-centered">
            <%--<div class="col-md-12 pad0">
                <div class="heading text-left">assessment</div>
            </div>--%>
            <div style="clear: both;"></div>
            <!-- First LayOut-->
            <div class="mt20">
                <div class="col-md-11 col-centered">
                    <asp:DataList ID="DlistQuestions1" runat="server"  Width="100%">
                        <ItemTemplate>
                            <div class="subtitle">
                                I willingly accept tasks beyond the scope of my job role and<br>
                                I am prepared to learn new skills.
                                <%#Eval("Question") %>>
                            </div>
                            <div class="col-md-12 boardphome2 bg1 mt20">
                                <div class="row mb20">
                                    <div class="col-md-11 col-sm-11 col-centered">
                                        <div class="pull-right">
                                            <div><a href="#" class="mood-btn">Always </a></div>
                                            <div class="clearfix"></div>
                                            <div><a href="#" class="mood-btn">Rarely </a></div>
                                            <div class="clearfix"></div>
                                            <div><a href="#" class="mood-btn">Never </a></div>
                                            <div class="clearfix"></div>
                                            <div><a href="#" class="mood-btn">Sometimes </a></div>
                                        </div>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                                <div style="position: absolute; z-index: 100; width: 100%;" class="absolute">

                                    <div class="progress">
                                        <div class="progress-bar" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 30%;" data-toggle="tooltip" data-placement="top" title="You're doing good! 60 more to go..">
                                            <span class="sr-only">20% Complete</span>
                                            <span class="progress-type"></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </ItemTemplate>
                    </asp:DataList>
                    <div style="clear: both;"></div>

                </div>
            </div>
            <div style="clear: both;"></div>
        </div>
    </div>


    <script type="text/javascript">
        function GetDivPosition() {
            var strCook = document.cookie;
            if (strCook.indexOf("!~") != 0) {
                var intS = strCook.indexOf("!~");

                var intE = strCook.indexOf("~!");

                var strPos = strCook.substring(intS + 2, intE);

                document.getElementById("persistMe").scrollTop = strPos;

            }
        }

        window.onload = function () {
            GetDivPosition();
        }

        function SetDivPosition() {

            var intY = document.getElementById("persistMe").scrollTop;

            document.title = intY;

            document.cookie = "yPos=!~" + intY + "~!";

        }

    </script>
</asp:Content>
