﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DAL;
using System.Data;

namespace HealToZeal.Employee
{
    public partial class EmployeeAssessments : System.Web.UI.Page
    {

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                if (Session["EmployeeId"] != null)
                {
                    BindSubmittedAssessments();

                }

            }
        }

        private void BindSubmittedAssessments()
        {

            try
            {
               // Button lnkBtn;
                string batchId = Session["BatchID"].ToString();
                EmployeeAssessmentsHistoryDAL objEmployeeAssessmentsHistoryDAL = new EmployeeAssessmentsHistoryDAL();
                DataTable DtAssessments = objEmployeeAssessmentsHistoryDAL.EmployeeAssementsGet(Session["EmployeeId"].ToString(), batchId);
                string id = Session["EmployeeId"].ToString();
                for(int j=0;j<DtAssessments.Rows.Count;j++)
                {
                    if ((DtAssessments.Rows[j]["CreatedDate"]) == DBNull.Value)
                    {
                        DtAssessments.Rows.Remove(DtAssessments.Rows[j]);
                    }
                }

                var LastUpdate = DtAssessments.AsEnumerable().Max(r => r.Field<DateTime>("CreatedDate"));


                for (int i = 0; i < DtAssessments.Rows.Count; i++)
                {
                    if ((LastUpdate == Convert.ToDateTime(DtAssessments.Rows[i]["CreatedDate"])) && ((bool)DtAssessments.Rows[i]["AssessmentSubmitted"]) == false)
                    {
                        DataColumnCollection columns = DtAssessments.Columns;
                        if (columns.Contains("LastUpadte"))
                        {
                            if (Convert.ToDateTime(DtAssessments.Rows[i]["CreatedDate"]) == LastUpdate)
                            {
                                
                                DtAssessments.Rows[i]["LastUpadte"] = LastUpdate;
                            }

                        }
                        else
                        {

                              DtAssessments.Columns.Add("LastUpadte", typeof(DateTime));
                              DtAssessments.Rows[i]["LastUpadte"] = LastUpdate;

                        }
                    }
                    else
                    {
                        DataColumnCollection columns = DtAssessments.Columns;
                        if (columns.Contains("LastUpadte"))
                        {

                        }
                        else
                        {
                            DtAssessments.Columns.Add("LastUpadte", typeof(DateTime));
                        }

                    }

                }


                if (DtAssessments != null && DtAssessments.Rows.Count > 0)
                {
                   //// DtAssessments.Columns.Remove('SR')
                   // DataView view = DtAssessments.DefaultView;
                   // view.Sort = "CreatedDate ASC";
                   // DataTable sortedDate = view.ToTable();

                    DlistAssessments.DataSource = DtAssessments;
                    DlistAssessments.DataBind();

                }

            }
            catch
            {

            }
        }

        protected void DlistAssessments_ItemCommand(object source, DataListCommandEventArgs e)
        {
            Session["AssessmentInstanceId"] = e.CommandArgument.ToString();
            Label lblAssessmentInstanceCompanyId = (Label)DlistAssessments.Items[e.Item.ItemIndex].FindControl("lblAssessmentInstanceCompanyId");

            Session["AssessmentInstanceCompanyId"] = lblAssessmentInstanceCompanyId.Text;
            if (e.CommandName == "ViewReport")
            {
                Response.Redirect("UserResult.aspx");
            }
            else if (e.CommandName == "ViewTest")
            {
                Response.Redirect("Assessment.aspx");
                //   Response.Redirect("DatewiseScore.aspx");
            }
        }
    }
}