﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DAL;
using System.Data;
using System.Web.Mail;
using System.Net.Mail;
using System.Text.RegularExpressions;

namespace HealToZeal.Employee
{
    public partial class ViewEmployee : System.Web.UI.Page
    {
        EmployeeDAL objEmployeeDAL = new EmployeeDAL();
        //string CompanyId = "";

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {            
                if (Session["EmployeeId"] != null)
                {
                    //BindCadre();
                    BindCompanyEmployees(0);
                    divalert.Visible = false;
                }
            }
        }

        /* private void BindCadre()
         {
             try
             {
                 CompanyId = Session["CompanyId"].ToString();
                 DataTable DtCadre = objEmployeeDAL.CompanyCadreGetByCompanyId(CompanyId);

                 ddlCadre.DataSource = DtCadre;
                 ddlCadre.DataTextField = "Cadrename";
                 ddlCadre.DataValueField = "CadreId_PK";
                 ddlCadre.DataBind();

                 ddlCadre.Items.Insert(0, new ListItem("--SelectCadre--", "--Select Cadre--"));
                 ddlCadre.SelectedIndex = 0;
             }
             catch
             {

             }
         }*/
        private void BindCompanyEmployees(int Flag)
        {
            try
            {
                DataTable DtEmployees = objEmployeeDAL.ParentEmployeeRelationshipGetByParentId(new Guid(Session["EmployeeId"].ToString()), Flag);
                if (DtEmployees.Columns.Contains("Color"))
                {

                }
                else
                {
                    DtEmployees.Columns.Add("Color", typeof(string));
                    DtEmployees.Columns.Add("LblText", typeof(string));
                }

                for (int a = 0; a < DtEmployees.Rows.Count; a++)
                {
                    DataTable Dtcolor = objEmployeeDAL.SpDatewiseScoreByEmployeeCompanyId(DtEmployees.Rows[a]["EmployeeId_PK"].ToString());

                    if (Dtcolor == null)
                    {
                        DtEmployees.Rows[a]["Color"] = "#ffffff";
                        DtEmployees.Rows[a]["LblText"] = "Test not attempted";
                    }
                    else
                    {
                        if (Dtcolor.Rows.Count == 0)
                        {
                            DtEmployees.Rows[a]["Color"] = "#ffffff";
                            DtEmployees.Rows[a]["LblText"] = "Test not attempted";
                        }
                        else
                        {
                            DataRow[] result = Dtcolor.Select("Color='#e43c37'");
                            if (result.Length > 0)
                            {
                                DtEmployees.Rows[a]["Color"] = "#e43c37";
                                DtEmployees.Rows[a]["LblText"] = "";
                            }
                            else if (result.Length == 0)
                            {
                                DataRow[] result1 = Dtcolor.Select("Color='#f4fb73'");
                                if (result1.Length > 0)
                                {
                                    DtEmployees.Rows[a]["Color"] = "#f4fb73";
                                    DtEmployees.Rows[a]["LblText"] = "";
                                }
                                else
                                {
                                    DtEmployees.Rows[a]["Color"] = "#88e46d";
                                    DtEmployees.Rows[a]["LblText"] = "";
                                }
                            }
                        }

                        //#f4fb73 - yellow
                        // #88e46d -green
                        //#e43c37 - red
                    }

                }
                // 
                if (DtEmployees != null )
                {
                    GvViewEmployee.DataSource = DtEmployees;
                    GvViewEmployee.DataBind();
                }

                for (int x = 0; x < GvViewEmployee.Rows.Count; x++)
                {
                    Label LblColor = (Label)GvViewEmployee.Rows[x].FindControl("LblColor");
                    if (LblColor.Text == "Test not attempted")
                    {
                        LblColor.Width = 120;
                    }
                }
            }
            catch
            {

            }
        }

        protected void ddlFilter_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (ddlFilter.SelectedIndex != 0)
                {
                    GvViewEmployee.PageIndex = 0;
                    Session["FilterBy"] = "Filter";
                    BindCompanyEmployees(ddlFilter.SelectedIndex);
                }
                txtEmployeeId.Text = "";
                txtName.Text = "";
            }
            catch
            {

            }
        }

        protected void GvViewEmployee_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                GvViewEmployee.PageIndex = e.NewPageIndex;
              
                if (Session["FilterBy"] != null)
                {
                    if (Session["FilterBy"].ToString().Equals("Filter"))
                    {
                        BindCompanyEmployees(ddlFilter.SelectedIndex);
                    }
                    else if (Session["FilterBy"].ToString().Equals("EmployeeId"))
                    {
                        DataTable DtEmployees = objEmployeeDAL.ParentEmployeeRelationshipGetByParentId(new Guid(Session["EmployeeId"].ToString()), Convert.ToInt32(HdnSearch.Value.ToString()));
                        if (DtEmployees != null && DtEmployees.Rows.Count > 0)
                        {
                            GvViewEmployee.DataSource = DtEmployees;
                            GvViewEmployee.DataBind();
                        }
                    }
                    else if (Session["FilterBy"].ToString().Equals("EmployeeName"))
                    {
                        DataTable DtEmployees = objEmployeeDAL.EmployeeGetByNameByEmployeeParentId(new Guid(Session["EmployeeId"].ToString()), HdnSearch.Value.ToString());
                        if (DtEmployees != null && DtEmployees.Rows.Count > 0)
                        {
                            GvViewEmployee.DataSource = DtEmployees;
                            GvViewEmployee.DataBind();
                            Session["FilterBy"] = "EmployeeName";
                        }
                    }
                    else if (Session["FilterBy"].ToString().Equals("CompanyCadre"))
                    {
                        //BindCompanyEmployeesByCadreID(ddlCadre.SelectedValue);
                    }
                    else
                    {
                        BindCompanyEmployees(0);
                    }
                }
                else
                {
                    BindCompanyEmployees(0);
                }


            }
            catch
            {

            }
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            try
            {
                if (HdnSearch.Value != "")
                {
                    if (txtEmployeeId.Text != "")
                    {                      
                        DataTable DtEmployees = objEmployeeDAL.ParentEmployeeRelationshipGetByParentId(new Guid(Session["EmployeeId"].ToString()), Convert.ToInt32(HdnSearch.Value.ToString()));
                        if (DtEmployees.Columns.Contains("Color"))
                        {

                        }
                        else
                        {
                            DtEmployees.Columns.Add("Color", typeof(string));
                            DtEmployees.Columns.Add("LblText", typeof(string));
                            // DtEmployees.Columns.Add("LblWidth", typeof(string));
                        }

                        for (int a = 0; a < DtEmployees.Rows.Count; a++)
                        {
                            DataTable Dtcolor = objEmployeeDAL.SpDatewiseScoreByEmployeeCompanyId(DtEmployees.Rows[a]["EmployeeId_PK"].ToString());

                            if (Dtcolor == null)
                            {
                                DtEmployees.Rows[a]["Color"] = "#ffffff";
                                DtEmployees.Rows[a]["LblText"] = "Test not attempted";
                                //DtEmployees.Rows[a]["LblWidth"] = "120px";
                            }
                            else
                            {
                                if (Dtcolor.Rows.Count == 0)
                                {
                                    DtEmployees.Rows[a]["Color"] = "#ffffff";
                                    DtEmployees.Rows[a]["LblText"] = "Test not attempted";
                                    // DtEmployees.Rows[a]["LblWidth"] = "120px";
                                }
                                else
                                {


                                    DataRow[] result = Dtcolor.Select("Color='#e43c37'");
                                    if (result.Length > 0)
                                    {
                                        DtEmployees.Rows[a]["Color"] = "#e43c37";
                                        DtEmployees.Rows[a]["LblText"] = "";
                                        //DtEmployees.Rows[a]["LblWidth"] = "90px";
                                    }
                                    else if (result.Length == 0)
                                    {
                                        DataRow[] result1 = Dtcolor.Select("Color='#f4fb73'");
                                        if (result1.Length > 0)
                                        {
                                            DtEmployees.Rows[a]["Color"] = "#f4fb73";
                                            DtEmployees.Rows[a]["LblText"] = "";
                                            // DtEmployees.Rows[a]["LblWidth"] = "90px";
                                        }
                                        else
                                        {
                                            DtEmployees.Rows[a]["Color"] = "#88e46d";
                                            DtEmployees.Rows[a]["LblText"] = "";
                                            //DtEmployees.Rows[a]["LblWidth"] = "90px";
                                        }
                                    }
                                }

                                //#f4fb73 - yellow
                                // #88e46d -green
                                //#e43c37 - red
                            }

                        }
                        // 
                        if (DtEmployees != null && DtEmployees.Rows.Count > 0)
                        {
                            GvViewEmployee.PageIndex = 0;
                            GvViewEmployee.DataSource = DtEmployees;
                            GvViewEmployee.DataBind();
                            Session["FilterBy"] = "EmployeeId";
                        }

                        for (int x = 0; x < GvViewEmployee.Rows.Count; x++)
                        {
                            Label LblColor = (Label)GvViewEmployee.Rows[x].FindControl("LblColor");
                            if (LblColor.Text == "Test not attempted")
                            {
                                LblColor.Width = 120;
                            }
                        }
                        ddlFilter.SelectedIndex = 0;
                        txtName.Text = "";
                    }
                }

            }
            catch
            {

            }
        }

        protected void btnSearchName_Click(object sender, EventArgs e)
        {
            try
            {
                if (HdnSearch.Value != "")
                {
                    if (txtName.Text != "")
                    {
                       // CompanyId = Session["CompanyId"].ToString();
                        DataTable DtEmployees = objEmployeeDAL.EmployeeGetByNameByEmployeeParentId(new Guid(Session["EmployeeId"].ToString()), HdnSearch.Value.ToString());
                        
                        if (DtEmployees.Columns.Contains("Color"))
                        {

                        }
                        else
                        {
                            DtEmployees.Columns.Add("Color", typeof(string));
                            DtEmployees.Columns.Add("LblText", typeof(string));
                            // DtEmployees.Columns.Add("LblWidth", typeof(string));
                        }

                        for (int a = 0; a < DtEmployees.Rows.Count; a++)
                        {
                            DataTable Dtcolor = objEmployeeDAL.SpDatewiseScoreByEmployeeCompanyId(DtEmployees.Rows[a]["EmployeeId_PK"].ToString());

                            if (Dtcolor == null)
                            {
                                DtEmployees.Rows[a]["Color"] = "#ffffff";
                                DtEmployees.Rows[a]["LblText"] = "Test not attempted";
                                //DtEmployees.Rows[a]["LblWidth"] = "120px";
                            }
                            else
                            {
                                if (Dtcolor.Rows.Count == 0)
                                {
                                    DtEmployees.Rows[a]["Color"] = "#ffffff";
                                    DtEmployees.Rows[a]["LblText"] = "Test not attempted";
                                    // DtEmployees.Rows[a]["LblWidth"] = "120px";
                                }
                                else
                                {


                                    DataRow[] result = Dtcolor.Select("Color='#e43c37'");
                                    if (result.Length > 0)
                                    {
                                        DtEmployees.Rows[a]["Color"] = "#e43c37";
                                        DtEmployees.Rows[a]["LblText"] = "";
                                        //DtEmployees.Rows[a]["LblWidth"] = "90px";
                                    }
                                    else if (result.Length == 0)
                                    {
                                        DataRow[] result1 = Dtcolor.Select("Color='#f4fb73'");
                                        if (result1.Length > 0)
                                        {
                                            DtEmployees.Rows[a]["Color"] = "#f4fb73";
                                            DtEmployees.Rows[a]["LblText"] = "";
                                            // DtEmployees.Rows[a]["LblWidth"] = "90px";
                                        }
                                        else
                                        {
                                            DtEmployees.Rows[a]["Color"] = "#88e46d";
                                            DtEmployees.Rows[a]["LblText"] = "";
                                            //DtEmployees.Rows[a]["LblWidth"] = "90px";
                                        }
                                    }
                                }

                                //#f4fb73 - yellow
                                // #88e46d -green
                                //#e43c37 - red
                            }

                        }
                        // 
                        if (DtEmployees != null && DtEmployees.Rows.Count > 0)
                        {
                            GvViewEmployee.PageIndex = 0;
                            GvViewEmployee.DataSource = DtEmployees;
                            GvViewEmployee.DataBind();
                            Session["FilterBy"] = "EmployeeId";
                        }

                        for (int x = 0; x < GvViewEmployee.Rows.Count; x++)
                        {
                            Label LblColor = (Label)GvViewEmployee.Rows[x].FindControl("LblColor");
                            if (LblColor.Text == "Test not attempted")
                            {
                                LblColor.Width = 120;
                            }
                        }
                        ddlFilter.SelectedIndex = 0;
                        txtName.Text = "";

                    }
                }

            }
            catch
            {

            }
        }

        static EmployeeDAL obj = new EmployeeDAL();
        private string EmployeeId;

        [System.Web.Services.WebMethod]
        [System.Web.Script.Services.ScriptMethod]

        public static string[] ShowListSearch(string prefixText, int count)
        {
            DataTable DtEmployees = obj.ParentEmployeeRelationshipGetByParentId(new Guid(HttpContext.Current.Session["EmployeeId"].ToString()), Convert.ToInt32(prefixText));

            List<string> items = new List<string>(DtEmployees.Rows.Count);

            if (DtEmployees.Rows.Count > 0)
            {
                for (int i = 0; i < DtEmployees.Rows.Count; i++)
                {
                    items.Add(DtEmployees.Rows[i]["EmployeeCompanyId"].ToString());
                }
            }

            return items.ToArray();
        }

        [System.Web.Services.WebMethod]
        [System.Web.Script.Services.ScriptMethod]
        public static string[] ShowListSearchByName(string prefixText, int count)
        {
            DataTable DtEmployees = obj.EmployeeGetByNameByEmployeeParentId(new Guid(HttpContext.Current.Session["EmployeeId"].ToString()), prefixText);
            List<string> items = new List<string>(DtEmployees.Rows.Count);

            if (DtEmployees.Rows.Count > 0)
            {
                for (int i = 0; i < DtEmployees.Rows.Count; i++)
                {
                    // items.Add(ds.Tables[0].Rows[i][1].ToString() + " " + ds.Tables[0].Rows[i]["MiddleName"].ToString() + " " + ds.Tables[0].Rows[i][3].ToString());                
                    items.Add(DtEmployees.Rows[i]["EmployeeName"].ToString());
                }
            }

            return items.ToArray();
       
        }

        protected void GvViewEmployee_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                EmployeeId = e.CommandArgument.ToString();
                GridViewRow row = (GridViewRow)(((Control)e.CommandSource).NamingContainer);
                  Label lblEmployeeName = (Label)row.FindControl("lblEmployeeName");
                //add session
                //   Session["EmployeeId"] = EmployeeId;
                //if (e.CommandName == "DeleteEmployee")
                //{
                //    CompanyId = Session["CompanyId"].ToString();
                //    DeleteEmployee(EmployeeId, CompanyId);
                //}
                 if (e.CommandName == "ViewEmployee")
                {
                    Response.Redirect("EmployeeSummary.aspx?Id=" + EmployeeId);
                }
                else if (e.CommandName == "ViewEmployeeNew")
                {
                    Response.Redirect("EmployeeSummaryNew.aspx?Id=" + EmployeeId);
                }
                else if (e.CommandName == "ViewEmployeeHistory")
                {
                    Response.Redirect("ViewEmployeeHistory.aspx?EmployeeId=" + EmployeeId + "&EmployeeName=" + lblEmployeeName.Text);
                }
            }
            catch
            {

            }
        }       

        /* protected void ddlCadre_SelectedIndexChanged(object sender, EventArgs e)
         {
             try
             {
                 if (ddlCadre.SelectedIndex != 0)
                 {
                     GvViewEmployee.PageIndex = 0;
                 BindCompanyEmployeesByCadreID( ddlCadre.SelectedValue);
                 Session["FilterBy"] = "CompanyCadre";
                 }
                 ddlDepartment.SelectedIndex = 0;
                 ddlFilter.SelectedIndex = 0;
                 txtEmployeeId.Text = "";
                 txtName.Text = "";
               
             }
             catch 
             {
                
             }
         }*/

        private void BindCompanyEmployeesByCadreID(string CadreId)
        {
            try
            {
                DataTable DtEmployees = objEmployeeDAL.EmployeesGetByCadreId(Session["CompanyId"].ToString(), CadreId);
                if (DtEmployees.Columns.Contains("Color"))
                {

                }
                else
                {
                    DtEmployees.Columns.Add("Color", typeof(string));
                    DtEmployees.Columns.Add("LblText", typeof(string));
                    // DtEmployees.Columns.Add("LblWidth", typeof(string));
                }

                for (int a = 0; a < DtEmployees.Rows.Count; a++)
                {
                    DataTable Dtcolor = objEmployeeDAL.SpDatewiseScoreByEmployeeCompanyId(DtEmployees.Rows[a]["EmployeeId_PK"].ToString());

                    if (Dtcolor == null)
                    {
                        DtEmployees.Rows[a]["Color"] = "#ffffff";
                        DtEmployees.Rows[a]["LblText"] = "Test not attempted";
                        //DtEmployees.Rows[a]["LblWidth"] = "120px";
                    }
                    else
                    {
                        if (Dtcolor.Rows.Count == 0)
                        {
                            DtEmployees.Rows[a]["Color"] = "#ffffff";
                            DtEmployees.Rows[a]["LblText"] = "Test not attempted";
                            // DtEmployees.Rows[a]["LblWidth"] = "120px";
                        }
                        else
                        {


                            DataRow[] result = Dtcolor.Select("Color='#e43c37'");
                            if (result.Length > 0)
                            {
                                DtEmployees.Rows[a]["Color"] = "#e43c37";
                                DtEmployees.Rows[a]["LblText"] = "";
                                //DtEmployees.Rows[a]["LblWidth"] = "90px";
                            }
                            else if (result.Length == 0)
                            {
                                DataRow[] result1 = Dtcolor.Select("Color='#f4fb73'");
                                if (result1.Length > 0)
                                {
                                    DtEmployees.Rows[a]["Color"] = "#f4fb73";
                                    DtEmployees.Rows[a]["LblText"] = "";
                                    // DtEmployees.Rows[a]["LblWidth"] = "90px";
                                }
                                else
                                {
                                    DtEmployees.Rows[a]["Color"] = "#88e46d";
                                    DtEmployees.Rows[a]["LblText"] = "";
                                    //DtEmployees.Rows[a]["LblWidth"] = "90px";
                                }
                            }
                        }

                        //#f4fb73 - yellow
                        // #88e46d -green
                        //#e43c37 - red
                    }

                }
                // 
                if (DtEmployees != null && DtEmployees.Rows.Count > 0)
                {
                    GvViewEmployee.DataSource = DtEmployees;
                    GvViewEmployee.DataBind();
                }

                for (int x = 0; x < GvViewEmployee.Rows.Count; x++)
                {
                    Label LblColor = (Label)GvViewEmployee.Rows[x].FindControl("LblColor");
                    if (LblColor.Text == "Test not attempted")
                    {
                        LblColor.Width = 120;
                    }
                }

                /*if (DtEmployee != null && DtEmployee.Rows.Count > 0)
                {


                    GvViewEmployee.DataSource = DtEmployee;
                    GvViewEmployee.DataBind();
                }*/
            }
            catch
            {

            }
        }

        protected void BtnRefresh_Click(object sender, EventArgs e)
        {
            try
            {
                Session["FilterBy"] = null;
                BindCompanyEmployees(0);
                //ddlCadre.SelectedIndex = 0;
                // ddlDepartment.SelectedIndex = 0;
                ddlFilter.SelectedIndex = 0;
                txtEmployeeId.Text = "";
                txtName.Text = "";
            }
            catch
            {

            }
        }

        protected void BtnReminderEmail_Click(object sender, EventArgs e)
        {
            try
            {
                TrEmail.Visible = true;
              //  CompanyId = Session["CompanyId"].ToString();
                DataTable DtEmployees = objEmployeeDAL.ParentEmployeeRelationshipGetByParentId(new Guid(Session["EmployeeId"].ToString()), 3);

                if (DtEmployees != null)
                {
                    if (DtEmployees.Rows.Count > 0)
                    {
                        string EmailId = "";
                        for (int i = 0; i < DtEmployees.Rows.Count; i++)
                        {
                            if (EmailId != "")
                            {
                                EmailId = EmailId + ";" + DtEmployees.Rows[i]["EmailId"].ToString();
                            }
                            else
                            {
                                EmailId = DtEmployees.Rows[i]["EmailId"].ToString();
                            }
                        }
                        TxtTo.Text = EmailId;
                    }
                }
            }
            catch
            {

            }

        }

        protected void BtnSendEmail_Click(object sender, EventArgs e)
        {
            try
            {
                /*  System.Web.Mail.MailMessage mail = new System.Web.Mail.MailMessage();
                  mail.From = Session["CompanyEmailId"].ToString();
                  mail.To = TxtTo.Text;
                  mail.Subject = TxtSubject.Text;
                  if (message.Value != "")
                  {
                      mail.Body = message.Value;
                  }
                  mail.BodyFormat = MailFormat.Html;
                  mail.Fields.Add("http://schemas.microsoft.com/cdo/configuration/smtpauthenticate", "1");    //basic authentication
                  mail.Fields.Add("http://schemas.microsoft.com/cdo/configuration/sendusername", "HealToZeal@chetanatspl.com"); //set your username here
                  mail.Fields.Add("http://schemas.microsoft.com/cdo/configuration/sendpassword", "HealToZeal");    //set your password here
                  System.Web.Mail.SmtpMail.SmtpServer = "holly.arvixe.com";  //your real server goes here                        
                  System.Web.Mail.SmtpMail.Send(mail);*/

                System.Net.Mail.MailMessage Mailmessage = new System.Net.Mail.MailMessage();
                Mailmessage.From = new MailAddress(Session["CompanyEmailId"].ToString());

                if (TxtTo.Text != "")
                {
                    string[] EmaiIds = Regex.Split(TxtTo.Text, ";");
                    for (int i = 0; i < EmaiIds.Length; i++)
                    {
                        if (EmaiIds[i].ToString() != "")
                        {
                            Match EmailMatch = Regex.Match(EmaiIds[i].Trim(), @"^[a-zA-Z][\w\.-]*[a-zA-Z0-9]@[a-zA-Z0-9][\w\.-]*[a-zA-Z0-9]\.[a-zA-Z][a-zA-Z\.]*[a-zA-Z]$", RegexOptions.IgnoreCase);
                            if (EmailMatch.Success)
                            {
                                Mailmessage.To.Add(EmaiIds[i].Trim());
                            }
                        }
                    }

                    Mailmessage.Subject = TxtSubject.Text;
                    Mailmessage.IsBodyHtml = true;

                    if (message.Value != "")
                    {
                        Mailmessage.Body = message.Value;
                    }

                    //Mailmessage.Body = EmailBody;

                    System.Net.Mail.SmtpClient smtp = new SmtpClient();
                    System.Net.NetworkCredential myCredential = new System.Net.NetworkCredential("LMS@chetanatspl.com", "LMS123");
                    smtp.Host = "holly.arvixe.com";   //Have specified the smtp host name
                    smtp.UseDefaultCredentials = false;
                    smtp.Credentials = myCredential;
                    try
                    {
                        smtp.Send(Mailmessage);
                    }
                    catch (Exception )
                    {

                    }
                    finally
                    {
                        Mailmessage.Dispose();
                    }
                }
                else
                {

                }

                TrEmail.Visible = false;
                ScriptManager.RegisterClientScriptBlock(this.Page, GetType(), "openColorBox", "<script> alert('We have received your meeting request. We will get back to you very soon'); </script>;", true);
            }

            catch (Exception ex)
            {
                throw ex;
            }
        }

        protected void BtnCancel_Click(object sender, EventArgs e)
        {
            try
            {
                TxtSubject.Text = "";
                TxtTo.Text = "";
                message.Value = "";
                TrEmail.Visible = false;
            }
            catch
            {

            }
        }

        protected void GvViewEmployee_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                Label lblEmployeeId = (Label)e.Row.FindControl("lblEmployeeId");
                GridView GvAssessments = (GridView)e.Row.FindControl("GvAssessments");
                AssessmentDAL objAssessmentDAL = new AssessmentDAL();

                DataTable DtAssessments = objAssessmentDAL.EmployeeAssementsGetForCompany(lblEmployeeId.Text);

                GvAssessments.DataSource = DtAssessments;
                GvAssessments.DataBind();

            }
            catch
            {


            }
        }

        protected void GvAssessments_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                if (e.CommandName == "ViewResult")
                {
                    GridViewRow gvRow = (GridViewRow)(((Button)e.CommandSource).NamingContainer);
                    Label lblAssessmentInstanceId = (Label)gvRow.FindControl("lblAssessmentInstanceId");
                    Label lblInstanceCompanyRelationId = (Label)gvRow.FindControl("lblInstanceCompanyRelationId");
                    Label lblEmployeeId = (Label)gvRow.FindControl("lblEmployeeId");
                    Label lblAssessmentId = (Label)gvRow.FindControl("lblAssessmentId");
                    Session["AssessmentId"] = lblAssessmentId.Text;
                    Session["AssessmentInstanceId"] = lblAssessmentInstanceId.Text;
                    Session["InstanceCompanyRelationId"] = lblInstanceCompanyRelationId.Text;
                    //Session["EmployeeId"] = lblEmployeeId.Text;

                    Response.Redirect("ViewUserResult.aspx?EmployeeId="+lblEmployeeId.Text, false);
                }
            }
            catch
            {

            }
        }

        //protected void BtnRefreshAll_Click(object sender, EventArgs e)
        //{
        //    try
        //    {
        //        Session["FilterBy"] = null;
        //        BindCompanyEmployees(0);
        //        ddlCadre.SelectedIndex = 0;
        //        ddlDepartment.SelectedIndex = 0;
        //        ddlFilter.SelectedIndex = 0;
        //        txtEmployeeId.Text = "";
        //        txtName.Text = "";
        //    }
        //    catch
        //    {

        //    }
        //}

        //protected void BtnRefresh_Click(object sender, EventArgs e)
        //{
        //    try
        //    {
        //        Session["FilterBy"] = null;
        //        BindCompanyEmployees(0);
        //        ddlCadre.SelectedIndex = 0;
        //        ddlDepartment.SelectedIndex = 0;
        //        ddlFilter.SelectedIndex = 0;
        //        txtEmployeeId.Text = "";
        //        txtName.Text = "";
        //    }
        //    catch
        //    {

        //    }
        //}
    }
}