﻿using System;
using System.Data;
using DAL;


namespace HealToZeal.Employee
{
    public partial class EmployeeHistory : System.Web.UI.Page
    {
        DataTable dt = new DataTable();
        DataTable dt2 = new DataTable();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["EmployeeId"] != null)
            {
                if (Request.QueryString["EmployeeId"] != null)
                {
                }
            }
                addColumns();
                EmployeeAssessmentsHistoryDAL objEmployeeDAL = new EmployeeAssessmentsHistoryDAL();
                DataTable DtClusterPercentage = new DataTable();

                DtClusterPercentage = objEmployeeDAL.EmployeeHistory(Session["EmployeeId"].ToString());

                for (int i = 0; i < DtClusterPercentage.Rows.Count; i++)
                {

                    dt.Rows.Add(DtClusterPercentage.Rows[i]["EmployeeCompanyId"], 
                        DtClusterPercentage.Rows[i]["AssessmentName"],
                   DtClusterPercentage.Rows[i]["AssessmentId_FK"],
                   DtClusterPercentage.Rows[i]["num"],
                   DtClusterPercentage.Rows[i]["ClusterName"],
                   DtClusterPercentage.Rows[i]["ClusterScore"],
                   DtClusterPercentage.Rows[i]["CreatedDate"], DtClusterPercentage.Rows[i]["EmployeeName"],
                   DtClusterPercentage.Rows[i]["ClusterPercentage"],
                   DtClusterPercentage.Rows[i]["Color"],
                   DtClusterPercentage.Rows[i]["IsPositive"],
                   DtClusterPercentage.Rows[i]["AssessmentInstanceId_FK"], 
                   DtClusterPercentage.Rows[i]["AssessmentInstanceName"]);


                    ReportViewer1.Visible = true;

                    ReportViewer1.LocalReport.DataSources.Clear();

                    ReportViewer1.LocalReport.DataSources.Add(new Microsoft.Reporting.WebForms.ReportDataSource("DataSet1", dt));

                    //        //if(i>7)
                    //        //{
                    //        //    dt2.Rows.Add(DtClusterPercentage.Rows[i]["EmployeeCompanyId"], DtClusterPercentage.Rows[i]["AssessmentName"],
                    //        //    DtClusterPercentage.Rows[i]["AssessmentId_FK"],
                    //        //    DtClusterPercentage.Rows[i]["num"],
                    //        //    DtClusterPercentage.Rows[i]["ClusterName"],
                    //        //    DtClusterPercentage.Rows[i]["ClusterScore"],
                    //        //    DtClusterPercentage.Rows[i]["CreatedDate"], DtClusterPercentage.Rows[i]["EmployeeName"],
                    //        //    DtClusterPercentage.Rows[i]["ClusterPercentage"],
                    //        //    DtClusterPercentage.Rows[i]["Color"], DtClusterPercentage.Rows[i]["IsPositive"], DtClusterPercentage.Rows[i]["AssessmentInstanceId_FK"], DtClusterPercentage.Rows[i]["AssessmentInstanceName"]);
                    //        //    ReportViewer2.Visible = true;

                    //        //    ReportViewer2.LocalReport.DataSources.Clear();

                    //        //    ReportViewer2.LocalReport.DataSources.Add(new Microsoft.Reporting.WebForms.ReportDataSource("DataSet1", dt2));
                    //        //}

                    //  }

                }
           
        }


        public void addColumns()
        {
            dt.Columns.Add("EmployeeCompanyId", typeof(string));
            dt.Columns.Add("AssessmentName", typeof(string));
            dt.Columns.Add("AssessmentId_FK", typeof(string));
            dt.Columns.Add("num", typeof(int));
            dt.Columns.Add("ClusterName", typeof(string));
            dt.Columns.Add("ClusterScore", typeof(string));
            dt.Columns.Add("CreatedDate", typeof(DateTime));
            dt.Columns.Add("EmployeeName", typeof(string));
            dt.Columns.Add("ClusterPercentage", typeof(double));
            dt.Columns.Add("Color", typeof(string));
            dt.Columns.Add("IsPositive", typeof(char));
            dt.Columns.Add("AssessmentInstanceId_FK", typeof(string));
            dt.Columns.Add("AssessmentInstanceName", typeof(string));


            //dt2.Columns.Add("EmployeeCompanyId", typeof(string));
            //dt2.Columns.Add("AssessmentName", typeof(string));
            //dt2.Columns.Add("AssessmentId_FK", typeof(string));
            //dt2.Columns.Add("num", typeof(int));
            //dt2.Columns.Add("ClusterName", typeof(string));
            //dt2.Columns.Add("ClusterScore", typeof(string));
            //dt2.Columns.Add("CreatedDate", typeof(DateTime));
            //dt2.Columns.Add("EmployeeName", typeof(string));
            //dt2.Columns.Add("ClusterPercentage", typeof(double));
            //dt2.Columns.Add("Color", typeof(string));
            //dt2.Columns.Add("IsPositive", typeof(char));
            //dt2.Columns.Add("AssessmentInstanceId_FK", typeof(string));
            //dt2.Columns.Add("AssessmentInstanceName", typeof(string));
        }
    }
}