﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master/LetMeTalkWithMenu.Master" AutoEventWireup="true" CodeBehind="Questions.aspx.cs" Inherits="HealToZeal.Employee.Questions" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style>
        .que-lm {
            margin-left: 20px;
        }

        @media only screen and (min-width:301px) and (max-width:768px) {
            .que-lm {
                margin-left: 0;
            }
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <link href="../Styles/Employee.css" rel="stylesheet" type="text/css" />
    <link href="../Styles/slider.css" rel="stylesheet" type="text/css">
    <script src="../Scripts/jquery.colorbox-min.js" type="text/javascript"></script>
    <script src="../Scripts/jquery.colorbox.js" type="text/javascript"></script>
    <script type="text/javascript">
        function DisplayColorBox(url) {

            $.fn.colorbox({ href: url, iframe: true, scrolling: true, width: '300px', position: 'Fixed', height: '150px', overlayClose: false });
            //, onClosed: function () { parent.location.reload(false); }
            return false;
        }
        function openModalQuestion() {
            $('[id*=openModalQuestion]').modal('show');
        }
    </script>

    <div >
        <%--<div class="col-lg-9">
            <div id="divalert" class="alert alert-success" runat="server">
                <strong>
                    <asp:Label ID="lblMessage" runat="server"></asp:Label></strong>
            </div>
        </div>--%>
        <div class="col-md-11 pad0 col-centered " >
           <Label ID="Label1"  >Total Questions : </Label> <asp:Label ID="lblexamCount"  runat="server"></asp:Label>
            </div>
        <div class="col-md-11 col-centered  bdr3" style="padding-top:0px !important; ">
            <div class="row">
                <div style=" width: 100%;" >
                                        <div class="progress progress-striped active">
                                            <div runat="server" id="Progressbar"  class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100">
                                                <span class="sr-only"></span>
                                            </div>
                                        </div>
                                    </div>
                <div class="col-md-11 col-centered">
                    <!-- First LayOut-->
                    <%--<div class="col-md-12 pad0">
                        <div class="heading text-left">assessment</div>
                    </div>--%>
                    <div style="clear: both;"></div>

                    <asp:DataList ID="DlistQuestions" runat="server" OnItemDataBound="DlistQuestions_ItemDataBound" Width="100%">
                        <ItemTemplate>
                            <!-- First LayOut-->
                            <div class="mt20" id="queLayout1" runat="server">
                                <div class="col-md-5 pad0 mb30">
                                    
                                    <img src="../UIFolder/dist/img/ques1.jpg" class="img-responsive">
                                    <span class="sr-only"></span>
                                </div>

                                <div class="col-md-7">
                                    <div class="parent">
                                        `
                                <div>
                                    <div class="quest mt20 wordwrap">
                                        <asp:Label ID="lblQuestionId" runat="server" Text='<%#Eval("QuestionId_FK") %>' Visible="false" onclick="getfocus()"></asp:Label>
                                        <%--<asp:Label ID="lblSrNo" runat="server" Text='<%#Eval("SrNo")+". " %>'></asp:Label>--%>
                                        <asp:Label ID="lblQuestion" runat="server" CssClass="quest" Text='<%#Eval("Question") %>'></asp:Label>
                                    </div>

                                    <div class="ans mt8">
                                        <asp:RadioButtonList ID="rblistAnswers" runat="server" RepeatDirection="Vertical" CssClass="radio-inline" OnSelectedIndexChanged="rblistAnswers_SelectedIndexChanged" AutoPostBack="true">
                                        </asp:RadioButtonList>
                                    </div>
                                    <div class="clearfix"></div>
                                  <%--  <div style="margin-bottom: 50px;" class="mt8">
                                         <asp:Button ID="btnPrevious" runat="server" Text="previous" CssClass="sm-bt pull-left" OnClick="btnPrevious_Click" />
                                        <asp:Button ID="btnNext" runat="server" Text="next" CssClass="sm-bt pull-right" OnClick="btnNext_Click" />
                                    </div>--%>
                                </div>
                                    </div>
                                </div>
                            </div>
                            <div style="clear: both;"></div>
                            <!-- Second LayOut-->
                            <div class="col-md-12">
                            <div id="queLayout2" class="row" runat="server">

                                <div class="subtitle mt20  wordwrap">
                                    <%--<asp:Label ID="Label1" runat="server" CssClass="quest" Text='<%#Eval("SrNo")+". " %>'></asp:Label>--%>
                                    <asp:Label ID="lblQuestion1" runat="server" CssClass="quest" Text='<%#Eval("Question") %>'></asp:Label>
                                </div>
                                <div class="col-md-12 boardphome2 bg1 mt20" >
                                    <div class=" mb20" style="float: left; width: 100%;">
                                        <div class="col-md-11 col-sm-11 col-centered">
                                            <div class="col-md-12 mood-btn">
                                                <asp:RadioButtonList ID="rblistAnswers1" runat="server" RepeatDirection="Vertical" CssClass="radio-inline " OnSelectedIndexChanged="rblistAnswers_SelectedIndexChanged" AutoPostBack="true">
                                                </asp:RadioButtonList>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                   <%-- <div style="position: absolute; z-index: 100; width: 100%;" class="absolute">

                                        <div class="progress progress-striped active">
                                            <div runat="server" id="Progressbar1" class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100">
                                            </div>
                                        </div>
                                    </div>--%>
                                </div>
                                <div style="clear: both;"></div>
                            </div>
                                </div>
                            <div style="clear: both;"></div>
                            <!-- Third LayOut-->
                            <div id="queLayout3" runat="server" class="row">
                                <div class="col-md-12 col-centered boardphome-bt">

                                    <div class="boardp  parent" style="height: auto;">
                                        <div>
                                            <div class="col-md-6 col-sm-6">

                                                <img src="../UIFolder/dist/img/paidtestimg1.png" class="img-responsive paidtestimg">
                                            </div>

                                            <div class="col-md-6 col-sm-6" style="margin-top: 10px; margin-bottom: 20px;">
                                                <div style="font-size: 30px; line-height: 1.2;" class="w600 wordwrap">
                                                   <%-- <asp:Label ID="Label2" runat="server" Text='<%#Eval("SrNo")+". " %>'></asp:Label>--%>
                                                    <asp:Label ID="Label3" runat="server" CssClass="quest" Text='<%#Eval("Question") %>'></asp:Label>
                                                </div>
                                                <div class="mt8">
                                                 <asp:RadioButtonList ID="rblistAnswers2" runat="server" RepeatDirection="Vertical" CssClass="radio-inline" OnSelectedIndexChanged="rblistAnswers_SelectedIndexChanged" AutoPostBack="true">
                                        </asp:RadioButtonList>
                                                    </div>
                                            </div>
                                        </div>


                                        <div style="clear: both;"></div>
                                        <%--<div class="col-md-10 col-centered">
                                             <asp:RadioButtonList ID="rblistAnswers2" runat="server" RepeatDirection="Vertical" CssClass="radio-inline" OnSelectedIndexChanged="rblistAnswers_SelectedIndexChanged" AutoPostBack="true">
                                        </asp:RadioButtonList>
                                        </div>--%>

                                        <div style="clear: both;"></div>

                                    </div>

                                    <div class="clearfix"></div>
                                    <%--<div class="col-md-12 pad0" style="bottom: 0;">
                                        <div style="position: absolute; z-index: 100; width: 100%;" class="absolute">
                                            <div style="clear: both;"></div>
                                            <div class="progress">
                                                <div class="progress progress-striped active">
                                            <div runat="server" id="Progressbar2" class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100">
                                            </div>
                                        </div>
                                            </div>
                                        </div>
                                    </div>--%>
                                </div>


                            </div>
                            <div style="clear: both;"></div>
                            <!-- Fourth LayOut-->
                            <div id="queLayout4" runat="server" class="row">
                                <div class="col-md-12 col-centered boardphome-bt">

                                    <div class="boardp parent" style="height: auto;">


                                        <div class=" col-centered">
                                            <div class="col-md-6 col-sm-6" style="margin-top: 10px; margin-bottom: 20px;">
                                                <div style="font-size: 30px; line-height: 1.2;" class="w600  wordwrap">
                                                    <asp:Label ID="Label5" runat="server" CssClass="quest" Text='<%#Eval("Question") %>'></asp:Label>

                                                </div>
                                                <div class="mt8">
                                                <asp:RadioButtonList ID="rblistAnswers3" runat="server" RepeatDirection="Vertical" CssClass="radio-inline" OnSelectedIndexChanged="rblistAnswers_SelectedIndexChanged" AutoPostBack="true">
                                        </asp:RadioButtonList>
                                                    </div>
                                            </div>
                                            <div class="col-md-6 col-sm-6">

                                                <img src="../UIFolder/dist/img/paidtestimg2.png" class="img-responsive mt8 dview">
                                            </div>

                                            <div style="clear: both;"></div>
                                           <%-- <div class="col-md-11">
                                                 <asp:RadioButtonList ID="rblistAnswers3" runat="server" RepeatDirection="Vertical" CssClass="radio-inline" OnSelectedIndexChanged="rblistAnswers_SelectedIndexChanged" AutoPostBack="true">
                                        </asp:RadioButtonList>

                                            </div>--%>
                                            <img src="../UIFolder/dist/img/paidtestimg2.png" class="img-responsive mt8 mview mb20">
                                        </div>



                                        <div style="clear: both;"></div>

                                    </div>
                                    <div>
                                    </div>
                                    <div class="clearfix"></div>
                                    <%--<div class="col-md-12 pad0" style="bottom: 0;">
                                        <div style="position: absolute; z-index: 100; width: 100%;" class="absolute">
                                            <div style="clear: both;"></div>
                                            <div class="progress">
                                                <div class="progress progress-striped active">
                                            <div runat="server" id="Progressbar3" class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100">
                                            </div>
                                        </div>
                                            </div>
                                        </div>
                                    </div>--%>
                                </div>

                            </div>
                            <div style="clear: both;"></div>

                            <!-- Third LayOut  not use-->
                            <%-- <div class="row">
                                <div class="boardp bdr3 parent" style="height: auto;">

                                    <div class="col-md-10 col-centered" style="margin-top: 40px; margin-bottom: 50px;">
                                        <div style="font-size: 36px; line-height: 1.2;" class="w600">
                                            <asp:Label ID="Label1" runat="server" CssClass="quest" Text='<%#Eval("Question") %>'></asp:Label>

                                        </div>
                                    </div>

                                    <div class="col-md-11 col-centered" style="margin-top: 10px; margin-bottom: 20px;">


                                        <div style="clear: both;"></div>
                                         <div class="col-md-12 " >
                                                 <asp:RadioButtonList ID="rblistAnswers2" runat="server" RepeatDirection="Vertical" CssClass="radio-inline " OnSelectedIndexChanged="rblistAnswers_SelectedIndexChanged" AutoPostBack="true">
                                        </asp:RadioButtonList>
                                             </div>
                                        <div class="col-md-3 col-sm-3">
                                            <center><img src="../UIFolder/dist/img/Icons-03.png" class="img-responsive mb20"></center>
                                        </div>
                                        <div class="col-md-3 col-sm-3">
                                            <center><img src="../UIFolder/dist/img/Icons-04.png" class="img-responsive mb20"></center>
                                        </div>
                                        <div class="col-md-3 col-sm-3">
                                            <center><img src="../UIFolder/dist/img/Icons-05.png" class="img-responsive mb20"></center>
                                        </div>
                                        <div class="col-md-3 col-sm-3">
                                            <center><img src="../UIFolder/dist/img/Icons-06.png" class="img-responsive mb20"></center>
                                        </div>
                                        <div style="clear: both;"></div>

                                    </div>

                                    <div style="clear: both;"></div>
                                    <div class="clearfix"></div>
                                </div>
                                <div class="col-md-12 pad0" style="bottom: 0;">
                                    <div style="position: absolute; z-index: 100; width: 100%;" class="absolute">
                                        <div style="clear: both;"></div>
                                        <div class="progress">
                                            <div class="progress-bar" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 30%;" data-toggle="tooltip" data-placement="top" title="You're doing good! 60 more to go..">
                                                <span class="sr-only">20% Complete</span>
                                                <span class="progress-type"></span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>--%>
                        </ItemTemplate>
                    </asp:DataList>

                </div>

            </div>
        </div>
        <div id="openModalQuestion" class="modal fade" role="dialog">
            <div class="modal-dialog modal-md">

                <!-- Modal content-->
                <div class="modal-content">

                    <div class="modal-body">
                        <div class="col-md-12 mt30">
                            <h4 class="modal-title">message</h4>
                            <p class="w400">
                                <asp:Label ID="lblMessageInfo" runat="server"></asp:Label>
                            </p>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <a href="">
                            <img src="../UIFolder/dist/img/close.png" data-dismiss="modal" class="popicon"></a>
                    </div>
                </div>

            </div>
        </div>
    </div>

    <script type="text/javascript">
        function GetDivPosition() {
            var strCook = document.cookie;
            if (strCook.indexOf("!~") != 0) {
                var intS = strCook.indexOf("!~");

                var intE = strCook.indexOf("~!");

                var strPos = strCook.substring(intS + 2, intE);

                document.getElementById("persistMe").scrollTop = strPos;

            }
        }

        window.onload = function () {
            GetDivPosition();
        }

        function SetDivPosition() {

            var intY = document.getElementById("persistMe").scrollTop;

            document.title = intY;

            document.cookie = "yPos=!~" + intY + "~!";

        }

    </script>
</asp:Content>
