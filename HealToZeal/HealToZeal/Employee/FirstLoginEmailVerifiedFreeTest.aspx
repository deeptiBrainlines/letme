﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master/LetMeTalkWithMenu.Master" AutoEventWireup="true" CodeBehind="FirstLoginEmailVerifiedFreeTest.aspx.cs" Inherits="HealToZeal.Employee.FirstLoginEmailVarifiedFreeTest" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">
        function openModal3() {
            $('[id*=myModal3]').modal('show');
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="row">
        <div class="col-md-11 col-centered">
          <div class="boardp bdr3 mb20"> 
          <div class="row" style="margin:60px 0 20px 0;">
<div class="col-md-7 col-sm-7 col-centered">
<h2 class="mt0"><span class="w600"><asp:Label ID="lblFirstname" runat="server"></asp:Label>&nbsp <asp:Label ID="lblLastname" runat="server"></asp:Label></span></h2>
<h4 class="txtwhite w600 mt8">email verified successfully</h4>
<p class="mt0 w500 mb20">please proceed to pay the below amount to get the assessment passcode or take a free test.</p>
<asp:Button ID="freetest" runat="server" Text="free test" CssClass="loginbtn mb20" OnClick="freetest_Click" /> 
    <%--<a href="#" class="loginbtn mb20" style="background:#c1c1c1; cursor:default">proceed to pay now</a>--%>
    <asp:Button ID="btn" runat="server" Text="proceed to pay now" 
         CssClass="loginbtn mb20" OnClick="btn_Click" /> 
</div>
<div class="pb28">



</div>
</div>
            
              <div class="clearfix"></div>
            </div>
            <div class="clearfix"></div>
          </div>
          <div class="clearfix"></div>
        </div>
    <div id="myModal3" class="modal fade" role="dialog">
                <div class="modal-dialog modal-md">

                    <!-- Modal content-->
                    <div class="modal-content">

                        <div class="modal-body">
                            <div class="col-md-12 mt30">
                                <h4 class="modal-title">message</h4>
                                <p class="w400">
                                   <asp:Label ID="lblMessage1" runat="server" ></asp:Label>
                                </p>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <a href="https://localhost:44352/Employee/EmployeeLogin">
                                <img src="../UIFolder/dist/img/close.png"  class="popicon"></a>
                        </div>
                    </div>

                </div>
            </div>
</asp:Content>