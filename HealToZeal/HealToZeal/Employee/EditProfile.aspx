﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="EditProfile.aspx.cs" MasterPageFile="~/Employee/Employee.Master" Inherits="HealToZeal.Employee.EditProfile" %>

  <asp:ContentPlaceHolder ID="EmployeeColumn" runat="server">
     <div>
           <table class="style1">
            <tr>
                <td>
                    &nbsp;</td>
                <td>
                    <asp:Label ID="lblTitle" runat="server" Text="Add Employee Profile"></asp:Label>
                </td>
                <td>
                    &nbsp;</td>
                    <td></td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="lblCompanyName" runat="server" Text="Company name"></asp:Label>
                </td>
                <td>
                    <asp:TextBox ID="txtCompanyName" runat="server" Enabled="False"></asp:TextBox>
                </td>
                <td>
                    <asp:Label ID="lblCompanydomain" runat="server" Text="Domain"></asp:Label>
                </td>
                <td>
                    <asp:DropDownList ID="ddlDomain" runat="server">
                    </asp:DropDownList>
                </td>
            </tr>
           
       
            <tr>
                <td>
                    <asp:Label ID="lblEmployeeId" runat="server" Text="Employee Id"></asp:Label>
                </td>
                <td>
                    <asp:TextBox ID="txtEmployeeId" runat="server"></asp:TextBox>
                </td>
                  <td>
                    <asp:Label ID="lblEmailId" runat="server" Text="Email Id"></asp:Label>
                </td>
                <td>
                    <asp:TextBox ID="txtEmailId" runat="server"></asp:TextBox>
                </td>
            </tr>
                     <tr>
                <td>
                    <asp:Label ID="lblJobprofile" runat="server" Text="Job profile"></asp:Label>
                </td>
                <td>
                    <asp:TextBox ID="txtJobprofile" runat="server"></asp:TextBox>
                </td>
                <td>
                    <asp:Label ID="lblExperience" runat="server" Text="Experience"></asp:Label>
                </td>
                <td>
                    <asp:TextBox ID="txtExperience" runat="server"></asp:TextBox>
                </td>
            </tr>
                 <tr>
                <td>
                    <asp:Label ID="lblFirstName" runat="server" Text="First name"></asp:Label>
                </td>
                <td>
                    <asp:TextBox ID="txtFirstName" runat="server"></asp:TextBox>
                </td>
                <td>
                    <asp:Label ID="lblLastName" runat="server" Text="Last name"></asp:Label>
                </td>
                <td>
                    <asp:TextBox ID="txtLastName" runat="server"></asp:TextBox>
                </td>
            </tr>
                <tr>
                <td>
                    <asp:Label ID="lblAddress" runat="server" Text="Address"></asp:Label>
                </td>
                <td>
                    <asp:TextBox ID="txtAddress" runat="server"></asp:TextBox>
                </td>
              <td>
                    <asp:Label ID="lblZipcode" runat="server" Text="Zip code"></asp:Label>
                </td>
                <td>
                    <asp:TextBox ID="txtZipcode" runat="server"></asp:TextBox>
                </td>
            </tr>
                <tr>
                <td>
                    <asp:Label ID="lblCountry" runat="server" Text="Country"></asp:Label>
                </td>
                <td>
                   
                </td>
              <td>
                    <asp:Label ID="lblCity" runat="server" Text="City"></asp:Label>
                </td>
                <td>
                    
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="lblContactNo" runat="server" Text="Contact no"></asp:Label>
                </td>
                <td>
                    <asp:TextBox ID="txtContactNo" runat="server"></asp:TextBox>
                </td>
              <td>
                    <asp:Label ID="lblMobileNo" runat="server" Text="Mobile no"></asp:Label>
                </td>
                <td>
                    <asp:TextBox ID="txtMobileNo" runat="server"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>
                    &nbsp;</td>
                <td>
                    <asp:Button ID="btnEditProfile" runat="server" Text="Edit Profile" 
                        onclick="btnEditProfile_Click" />
                </td>
                <td>
                    &nbsp;</td>
                    <td>
                    &nbsp;</td>
            </tr>
            <tr>
                <td>
                    &nbsp;</td>
                <td>
                    &nbsp;</td>
                <td>
                    &nbsp;</td>
                    <td>
                    &nbsp;</td>
            </tr>
        </table>
    
    </div>
    </asp:ContentPlaceHolder>
