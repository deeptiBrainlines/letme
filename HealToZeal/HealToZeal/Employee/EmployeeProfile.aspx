﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Employee/Employee.Master" AutoEventWireup="true" CodeBehind="EmployeeProfile.aspx.cs" Inherits="HealToZeal.Employee.EmployeeProfile" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="EmployeeColumn" runat="server">

    <style type="text/css">
        .ajax__calendar_container {
            z-index: 9999;
            position: static;
            background-color: white;
        }
    </style>
    <div class="container">
        <div class="row">
            <div id="divalert" class="alert alert-success" runat="server">
                <strong>
                    <asp:Label ID="lblMessage" runat="server"></asp:Label>
                </strong>
            </div>
        </div>
        <h4 class="hs_heading" id="hs_appointment_form_link">My profile
        </h4>
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12">
                <div class="hs_single_profile">
                    <div class="hs_single_profile_detail">
                        <h3></h3>
                        <div class="row">
                            <div class="col-lg-3 col-md-6 col-sm-6">
                                <i class="fa fa-user-md"></i>Name: 
                <a href="">
                    <asp:Label ID="lblName" runat="server"></asp:Label>
                </a>
                            </div>

                            <div class="col-lg-3 col-md-6 col-sm-6">
                                <i class="fa fa-medkit"></i>Employee ID: 
                <a href="">
                    <asp:Label ID="lblEmployeeId" runat="server"></asp:Label>
                </a>
                            </div>
                            <div class="col-lg-3 col-md-6 col-sm-6">
                                <i class="fa fa-medkit"></i>Department: 
                <a href="">
                    <asp:Label ID="lblDepartment" runat="server" Text="N/A"></asp:Label>
                </a>
                            </div>
                            <div class="col-lg-3 col-md-6 col-sm-6">
                                <i class="fa fa-medkit"></i>Cadre: 
                <a href="">
                    <asp:Label ID="lblCadre" runat="server"></asp:Label>
                </a>
                            </div>

                        </div>
                        <hr>
                        <div class="row">
                            <div class="col-lg-4 col-md-6 col-sm-6">
                                <i class="fa fa-envelope"></i>Email ID: 
                <a href="">
                    <asp:Label Style="width: 100%" ID="lblEmailId" runat="server"></asp:Label>
                </a>
                            </div>
                            <div class="col-lg-4 col-md-6 col-sm-6">
                                <i class="fa fa-phone"></i>Phone No: 
                <a href="">
                    <asp:Label ID="lblContactno" runat="server"></asp:Label>
                </a>
                            </div>
                            <%--  <div class="col-lg-6 col-md-6 col-sm-6"> Get connect with him:
              <div class="hs_profile_social">
                <ul>
                  <li><a href=""><i class="fa fa-facebook"></i></a></li>
                  <li><a href=""><i class="fa fa-twitter"></i></a></li>
                  <li><a href=""><i class="fa fa-linkedin"></i></a></li>
                  <li><a href=""><i class="fa fa-skype"></i></a></li>
                </ul>
              </div>
            </div>--%>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="portfolio-details">
                <div class="col-md-4 col-sm-6">
                </div>
            </div>
            <div class="col-lg-8 col-md-8 col-sm-7">
                <div class="hs_comment_form">
                    <div class="row">
                        <div class="col-lg-8 col-md-8 col-sm-7">
                            <div class="row">
                                <asp:ValidationSummary ID="ValidationSummary1" runat="server" ForeColor="Red" ValidationGroup="0" />
                            </div>
                        </div>
                        <%--   <div class="row">

                      </div>--%>

                        <div class="col-lg-6 col-md-6 col-sm-12">
                            <div class="input-group">
                                <span class="input-group-btn">
                                    <div class="btn btn-success"><i class="fa fa-user-md"></i></div>
                                </span>
                                <asp:TextBox ID="txtFirstName" runat="server" CssClass="form-control" placeholder="First name" ValidationGroup="0"></asp:TextBox>
                            </div>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" Display="Dynamic" ErrorMessage="Enter first name" ControlToValidate="txtFirstName" ForeColor="Red" ValidationGroup="0">*</asp:RequiredFieldValidator>

                            <!-- /input-group -->
                        </div>

                        <!-- /.col-lg-6 -->
                        <div class="col-lg-6 col-md-6 col-sm-12">
                            <div class="input-group">
                                <span class="input-group-btn">
                                    <div class="btn btn-success"><i class="fa fa-user-md"></i></div>
                                </span>
                                <asp:TextBox ID="txtLastName" runat="server" CssClass="form-control" placeholder="Last name"
                                    ValidationGroup="0"></asp:TextBox>
                            </div>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="Enter last name" Display="Dynamic" ControlToValidate="txtLastName" ForeColor="Red" ValidationGroup="0">*</asp:RequiredFieldValidator>

                            <!-- /input-group -->
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-12">
                            <div class="input-group">
                                <span class="input-group-btn">
                                    <div class="btn btn-success"><i class="fa fa-user-md"></i></div>
                                </span>
                                <asp:TextBox ID="txtUserName" runat="server" CssClass="form-control" placeholder="User name"
                                    ValidationGroup="0"></asp:TextBox>
                            </div>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ErrorMessage="Enter username" ControlToValidate="txtUserName" Display="Dynamic" ForeColor="Red">*</asp:RequiredFieldValidator>

                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-12">
                            <div class="input-group">
                                <span class="input-group-btn">
                                    <div class="btn btn-success"><i class="fa fa-envelope"></i></div>
                                </span>
                                <asp:TextBox ID="txtEmailId" runat="server" CssClass="form-control" placeholder="EmailId"
                                    ValidationGroup="0"></asp:TextBox>
                            </div>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ErrorMessage="Enter email id" Display="Dynamic" ControlToValidate="txtEmailId" ForeColor="Red" ValidationGroup="0">*</asp:RequiredFieldValidator>
                            <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ErrorMessage="Enter proper email id" Display="Dynamic" ControlToValidate="txtEmailId" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" ForeColor="Red" ValidationGroup="0">*</asp:RegularExpressionValidator>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-12">
                            <div class="input-group">
                                <span class="input-group-btn">
                                    <div class="btn btn-success"><i class="fa fa-home"></i></div>
                                </span>
                                <asp:DropDownList ID="ddlCountry" runat="server"
                                    OnSelectedIndexChanged="ddlCountry_SelectedIndexChanged"
                                    AutoPostBack="True" CssClass="form-control" ValidationGroup="0">
                                </asp:DropDownList>
                            </div>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ErrorMessage="*" InitialValue="--Select country--" Display="Dynamic" ControlToValidate="ddlCountry" ForeColor="Red" ValidationGroup="0"></asp:RequiredFieldValidator>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-12">
                            <div class="input-group">
                                <span class="input-group-btn">
                                    <div class="btn btn-success"><i class="fa fa-calendar"></i></div>
                                </span>
                                <asp:TextBox ID="txtBirthDate" runat="server" CssClass="form-control" placeholder="BirthDate" ValidationGroup="0"></asp:TextBox>
                                &nbsp; Tip: you can directly enter DOB in MM\DD\YYYY format
                                <cc1:CalendarExtender ID="txtBirthDate_CalendarExtender" runat="server" PopupPosition="BottomRight" Format="dd/MM/yyyy" CssClass="ajax__calendar_container"
                                    Enabled="True" TargetControlID="txtBirthDate">
                                </cc1:CalendarExtender>
                            </div>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ErrorMessage="*" ControlToValidate="txtBirthDate" Display="Dynamic" ForeColor="Red" ValidationGroup="0"></asp:RequiredFieldValidator>
                            <asp:CompareValidator ID="CompareValidator1" runat="server"
                                ControlToValidate="txtBirthDate" Display="Dynamic" ErrorMessage="Invalid Date"
                                Operator="DataTypeCheck" Type="Date">
                            </asp:CompareValidator>
                            <cc1:ValidatorCalloutExtender ID="CompareValidator1_ValidatorCalloutExtender"
                                runat="server" Enabled="True" TargetControlID="CompareValidator1">
                            </cc1:ValidatorCalloutExtender>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-12">
                            <div class="input-group">
                                <span class="input-group-btn">
                                    <div class="btn btn-success"><i class="fa fa-home"></i></div>
                                </span>
                                <asp:DropDownList ID="ddlCity" runat="server" CssClass="form-control" ValidationGroup="0">
                                </asp:DropDownList>
                            </div>


                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-12">
                            <div class="input-group">
                                <span class="input-group-btn">
                                    <div class="btn btn-success"><i class="fa fa-square"></i></div>
                                </span>
                                <asp:RadioButtonList ID="rblGender" runat="server" RepeatDirection="Horizontal" CssClass="form-control1" ValidationGroup="0">
                                    <asp:ListItem Value="M">Male</asp:ListItem>
                                    <asp:ListItem Value="F">Female</asp:ListItem>
                                </asp:RadioButtonList>
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-12">
                            <div class="input-group">
                                <span class="input-group-btn">
                                    <div class="btn btn-success"><i class="fa fa-home"></i></div>
                                </span>
                                <asp:TextBox ID="txtAddress" runat="server" CssClass="form-control" placeholder="Address" ValidationGroup="0"></asp:TextBox>
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-12">
                            <div class="input-group">
                                <span class="input-group-btn">
                                    <div class="btn btn-success"><i class="fa fa-square"></i></div>
                                </span>
                                <asp:DropDownList ID="ddlEducation" runat="server" CssClass="form-control" ValidationGroup="0">
                                    <asp:ListItem>--Select education--</asp:ListItem>
                                    <asp:ListItem>PostGraduate</asp:ListItem>
                                    <asp:ListItem>Graduate</asp:ListItem>
                                    <asp:ListItem>Doctorate</asp:ListItem>
                                </asp:DropDownList>
                            </div>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator8" runat="server" ErrorMessage="Select education" InitialValue="--Select education--" Display="Dynamic" ControlToValidate="ddlEducation" ForeColor="Red" ValidationGroup="0">*</asp:RequiredFieldValidator>

                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-12">
                            <div class="input-group">
                                <span class="input-group-btn">
                                    <div class="btn btn-success"><i class="fa fa-envelope"></i></div>
                                </span>
                                <asp:TextBox ID="txtZipcode" runat="server" CssClass="form-control" placeholder="Zip code"></asp:TextBox>

                            </div>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-12">
                            <div class="input-group">
                                <span class="input-group-btn">
                                    <div class="btn btn-success"><i class="fa fa-square"></i></div>
                                </span>
                                <asp:TextBox ID="txtOccupation" runat="server" CssClass="form-control" placeholder="Occupation" ValidationGroup="0"></asp:TextBox>
                            </div>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator9" runat="server" ErrorMessage="*" ControlToValidate="txtOccupation" Display="Dynamic" ForeColor="Red" ValidationGroup="0"></asp:RequiredFieldValidator>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-12">
                            <div class="input-group">
                                <span class="input-group-btn">
                                    <div class="btn btn-success"><i class="fa fa-phone"></i></div>
                                </span>
                                <asp:TextBox ID="txtContactNo" runat="server" CssClass="form-control" placeholder="Contact no"
                                    ValidationGroup="0"></asp:TextBox>
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-12">
                            <div class="input-group">
                                <span class="input-group-btn">
                                    <div class="btn btn-success"><i class="fa fa-square"></i></div>
                                </span>
                                <asp:DropDownList ID="ddlExperience" runat="server" CssClass="form-control" ValidationGroup="0">
                                    <asp:ListItem>--Select experience--</asp:ListItem>
                                    <asp:ListItem>0-3 years</asp:ListItem>
                                    <asp:ListItem>3-7 years</asp:ListItem>
                                    <asp:ListItem>7-10 years</asp:ListItem>
                                    <asp:ListItem>10-15 years</asp:ListItem>
                                    <asp:ListItem>15 and above</asp:ListItem>
                                </asp:DropDownList>
                            </div>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator10" runat="server" ErrorMessage="Select experience" InitialValue="--Select experience--" Display="Dynamic" ControlToValidate="ddlExperience" ForeColor="Red" ValidationGroup="0">*</asp:RequiredFieldValidator>

                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-12">
                            <div class="input-group">
                                <span class="input-group-btn">
                                    <div class="btn btn-success"><i class="fa fa-mobile-phone"></i></div>
                                </span>
                                <asp:TextBox ID="txtMobileNo" runat="server" CssClass="form-control" placeholder="Mobile no"
                                    ValidationGroup="0"></asp:TextBox>
                            </div>
                        </div>
                        

                        <div class="col-lg-6 col-md-6 col-sm-12">
                            <div class="input-group">
                                <span class="input-group-btn">
                                    <div class="btn btn-success"><i class="fa fa-square"></i></div>
                                </span>
                                <asp:DropDownList ID="ddlPosition" runat="server" CssClass="form-control" ValidationGroup="0">
                                    <asp:ListItem>--Select position--</asp:ListItem>
                                    <asp:ListItem>Junior level</asp:ListItem>
                                    <asp:ListItem>middle management</asp:ListItem>
                                    <asp:ListItem>Senior management</asp:ListItem>
                                </asp:DropDownList>
                            </div>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator11" runat="server" ErrorMessage="Select position" InitialValue="--Select position--" Display="Dynamic" ControlToValidate="ddlPosition" ForeColor="Red" ValidationGroup="0">*</asp:RequiredFieldValidator>

                        </div>
                     <div >
                            <div >
                                <span >
                                    <div ><i ></i></div>
                                </span>
                                <asp:DropDownList ID="ddCadre" runat="server"  >
                                   
                                </asp:DropDownList>
                            </div>
                            <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator12" runat="server" ErrorMessage="Select Cadre" InitialValue="--Select Cadre--" Display="Dynamic" ControlToValidate="ddCadre" ForeColor="Red" ValidationGroup="0">*</asp:RequiredFieldValidator>--%>

                        </div>
                        <div >
                            <div >
                                <span >
                                    <div ><i ></i></div>
                                </span>
                                <asp:DropDownList ID="ddDeapartment" runat="server">
                                 </asp:DropDownList>
                            </div>
                            <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator7" runat="server" ErrorMessage="Select Deaprtment" InitialValue="--Select Deaprtment--" Display="Dynamic" ControlToValidate="ddDeapartment" ForeColor="Red" ValidationGroup="0">*</asp:RequiredFieldValidator>--%>

                        </div>
                        <div class="row">
                            <div class="col-lg-6 col-md-6 col-sm-12">
                                <div class="input-group">
                                </div>
                            </div>

                        </div>
                        <div class="row">
                            <div class="col-lg-6 col-md-6 col-sm-12">
                                <div class="input-group">
                                    <span class="input-group-btn">
                                        <asp:Button ID="btnEditProfile" CausesValidation="true"  runat="server" Text="Submit" CssClass="btn btn-success pull-right"
                                            OnClick="btnEditProfile_Click" ValidationGroup="0" />
                                    </span>
                                    <span class="input-group-btn">
                                        <asp:Button ID="btnCancel" runat="server" CssClass="btn btn-success pull-right"
                                            Text="Cancel" OnClick="btnCancel_Click" />
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>

        </div>
        <div class="clearfix"></div>
        <div class="hs_margin_30"></div>
        <div class="clearfix"></div>
        <div class="hs_margin_40"></div>
    </div>





</asp:Content>
