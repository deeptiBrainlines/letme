﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master/LetMeTalkWithMenu.Master" AutoEventWireup="true" CodeBehind="UserResultReport.aspx.cs" Inherits="HealToZeal.Employee.UserResultReport" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
     <script type="text/javascript">
        function openMessageModal() {
            $('[id*=MessageModal]').modal('show');
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
   <div class="row">
        <div class="col-md-11 col-centered">
            <div class="boardp bdrgrey mb20 whitebg">
               <%-- <div class="pull-left heading">report</div>--%>
                <div class="icon-m10 pull-right">
                    <a href="#">
                        <img src="../UIFolder/dist/img/cloud.png" class="pull-left  img-responsive">
                    </a>
                    <a href="#">
                        <img src="../UIFolder/dist/img/down.png" class="pull-left img-responsive">
                    </a>

                </div>
                <div  id="style-3" style="margin-top: 0 important;">
                    <div class="pb28">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="table-responsive">
                                    
                                    <asp:Repeater ID="cpRepeater" runat="server">
                                        <HeaderTemplate>
                                            <table width="100%" cellspacing="0" cellpadding="0" class="table">
                                                <thead>
                                                    <tr>
                                                        <th>PARAMETER</th>
                                                        <th>YOUR SCORE</th>
                                                        <%-- <th>STANDARD</th>--%>
                                                    </tr>
                                                </thead>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <tr>
                                                <td>
                                                    <asp:Label ID="lblSrNo" runat="server" Text='<%# Eval("ClusterName") %>' /></td>
                                                <td>
                                                    <%--<div style="background:#fe9d34; width:20px; height:20px; border-radius:20px;"></div>--%>
                                                    <asp:Label ID="lblColor" runat="server" BackColor='<%# System.Drawing.Color.FromName((string) Eval("Color")) %>' Height="20px" Text="" Width="20px" Style="width: 20px; height: 20px; border-radius: 20px;"> </asp:Label>
                                                </td>
                                                <%-- <td>
                                                    <img src="../UIFolder/dist/img/green.png" /></td>--%>
                                            </tr>

                                        </ItemTemplate>
                                        <FooterTemplate>
                                            </table>
                                        </FooterTemplate>
                                    </asp:Repeater>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                </div>

                <div class="clearfix"></div>
                <div class="col-md-12">
                    <div class="row">
                        <div class="col-md-6 pad0">
                            <div class="roundcenter">

                                <span class="mytooltip tooltip-effect-2">
                                    <span class="tooltip-item">
                                       <%-- <img src="../UIFolder/dist/img/red.png">--%>
                                        <div style="background:#e43c37; width:20px; height:20px; border-radius:20px;"></div>
                                    </span>
                                    <span class="tooltip-content clearfix">

                                        <span class="tooltip-text">
                                            <div>
                                                <h2>
                                                    <div style="background: #e43c37; width: 30px; height: 30px; border-radius: 20px; float: left;"></div>

                                                    &nbsp;<strong>
Title</strong></h2>
                                                <p style="font-size: 15px;" class="w400">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation</p>
                                                <br>
                                            </div>

                                        </span>
                                    </span>

                                </span>

                                <span class="mytooltip tooltip-effect-2">
                                    <span class="tooltip-item">
                                        <%--<img src="../UIFolder/dist/img/orange.png">--%>
                                         <div style="background:#f4fb73; width:20px; height:20px; border-radius:20px;"></div>
                                    </span>
                                    <span class="tooltip-content clearfix">

                                        <span class="tooltip-text">
                                            <div>
                                                <h2>
                                                    <div style="background: #f4fb73; width: 30px; height: 30px; border-radius: 20px; float: left;"></div>

                                                    &nbsp;<strong>
Title</strong></h2>
                                                <p style="font-size: 15px;" class="w400">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation</p>
                                                <br>
                                            </div>

                                        </span>
                                    </span>

                                </span>


                                <span class="mytooltip tooltip-effect-2">
                                    <span class="tooltip-item">
                                        <%--<img src="../UIFolder/dist/img/green.png">--%>
                                         <div style="background:#88e46d; width:20px; height:20px; border-radius:20px;"></div>
                                    </span>
                                    <span class="tooltip-content clearfix">

                                        <span class="tooltip-text">
                                            <div>
                                                <h2>
                                                    <div style="background: #88e46d; width: 30px; height: 30px; border-radius: 20px; float: left;"></div>

                                                    &nbsp;<strong>
Title</strong></h2>
                                                <p style="font-size: 15px;" class="w400">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation</p>
                                                <br>
                                            </div>

                                        </span>
                                    </span>

                                </span>
                            </div>
                        </div>

                        <div class="col-md-6">
                            <h4>

                                <%--<a href="#" class="textlink bottxtlink w400">click here for a detailed premium service >></a>--%>
                                <asp:Button CssClass="textlink bottxtlink w400" ID="btnPremiumService" runat="server" Text="click here for premium service" OnClick="btnPremiumService_Click" />
                            </h4>
                        </div>
                    </div>
                </div>

                <div class="clearfix"></div>
            </div>
            <div class="clearfix"></div>
        </div>
        <div class="clearfix"></div>
    </div>
    <div class="clearfix"></div>

    <div id="MessageModal" class="modal fade" role="dialog">
        <div class="modal-dialog modal-md">

            <!-- Modal content-->
            <div class="modal-content">

                <div class="modal-body">
                    <div class="col-md-12 mt30">
                        <h4 class="modal-title">message</h4>
                        <p class="w400">
                            <asp:Label ID="lblMessage1" runat="server"></asp:Label>
                        </p>
                    </div>
                </div>
                <div class="modal-footer">
                    <a href="">
                        <img src="../UIFolder/dist/img/close.png" data-dismiss="modal" class="popicon"></a>
                </div>
            </div>

        </div>
    </div>

    <div class="clearfix"></div>
</asp:Content>
