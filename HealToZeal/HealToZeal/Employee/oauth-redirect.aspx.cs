﻿using Facebook;
using System;
using System.Web;
using System.IO;
using System.Net;
using System.Collections.Generic;
using Newtonsoft.Json;
using System.Web.Script.Serialization;
using System.Net.Mail;
using DAL;
using BE;
using System.Data;

namespace HealToZeal.Employee
{
    public partial class oauth_redirect : System.Web.UI.Page
    {
        string CompanyId = "";
        EmployeeDAL objEmployeeDAL = new EmployeeDAL();
        protected void Page_Load(object sender, EventArgs e)
        {
            //string InstanceCompanyRelationId = Request.QueryString["InstanceCompanyRelationId"];
            //string CouponId = Request.QueryString["CouponId"];
            //DataTable compstatus = objEmployeeDAL.GetCompanyIDBatchID(InstanceCompanyRelationId);
            //for (int i = 0; i <= compstatus.Rows.Count; i++)
            //{
            //    Session["CompanyId"] = compstatus.Rows[i]["CompanyId_FK"].ToString();
            //    Session["BatchID"] = compstatus.Rows[i]["BatchID_Fk"].ToString();
            //}

            if (Request.QueryString["code"] != null)
            {
                string accessCode = Request.QueryString["code"].ToString();

                var fb = new FacebookClient();

                // throws OAuthException 
                dynamic result = fb.Post("oauth/access_token", new
                {

                    client_id = "682470882264872",

                    client_secret = "f24df6e723342715f9c557ba2313f418",

                   // redirect_uri = "http://apps.brainlines.net/LMT2M_test/Employee/oauth-redirect.aspx",
                    redirect_uri = "https://localhost:44352/Employee/oauth-redirect.aspx",
                    code = accessCode

                });

                var accessToken = result.access_token;
                var expires = result.expires;

                // Store the access token in the session
                Session["AccessToken"] = accessToken;

                // update the facebook client with the access token 
                fb.AccessToken = accessToken;
                
                // Calling Graph API for user info
                dynamic me = fb.Get("me?fields=id,name,email,gender,photos,location");

                string id = me.id; // You can store it in the database
                string name = me.name;
                string email = me.email;
                string gender = me.gender;
                var image = me.image;
                string birthday = me.birthday;
                string FirstName = "";
                string LastName = "";
                var locations = me.location;

                //string UserLocation = locations[1];

                //  imgprofile.ImageUrl = image;
                //  lblid.Text = email;
                //  lblgender.Text = gender;
                ////  lbllocale.Text = UserLocation;
                //  lblname.Text = name;
                //String[] spearator = { ","};
                //String[] strlist = name.Split(spearator,
                //StringSplitOptions.RemoveEmptyEntries);

                string[] name1 = name.Split(' ');
                foreach (string comma in name1)
                {
                     FirstName = name1[0];
                     LastName = name1[1];

                }
                string Email = email;
                string Password = Encryption.CreatePassword();
                string UserName = Email;
                InsertUpdateEmployee(FirstName, LastName, Email ,Password ,UserName);

                //string Password1 = dt.Rows[0]["Password"].ToString();
                //String Password = Encryption.Decrypt(Password1);
                //string UserName = dt.Rows[0]["UserName"].ToString();


            }
            else if (Request.QueryString["error"] != null)
            {
                string error = Request.QueryString["error"];
                string errorReason = Request.QueryString["error_reason"];
                string errorDescription = Request.QueryString["error_description"];
            }

            else
            {
                // User not connected
            }
        }

        public class Userclass
        {
            public string id
            {
                get;
                set;
            }
            public string name
            {
                get;
                set;
            }
            public string given_name
            {
                get;
                set;
            }
            public string family_name
            {
                get;
                set;
            }
            public string link
            {
                get;
                set;
            }
            public string picture
            {
                get;
                set;
            }
            public string gender
            {
                get;
                set;
            }
            public string locale
            {
                get;
                set;
            }
        }

        private string GetAccessToken()
        {
            if (HttpRuntime.Cache["access_token"] == null)
            {
                Dictionary<string, string> args = GetOauthTokens(Request.Params["code"]);
                HttpRuntime.Cache.Insert("access_token", args["access_token"], null, DateTime.Now.AddMinutes(Convert.ToDouble(args["expires"])), TimeSpan.Zero);
            }
            return HttpRuntime.Cache["access_token"].ToString();
        }
        private Dictionary<string, string> GetOauthTokens(string code)
        {
            Dictionary<string, string> tokens = new Dictionary<string, string>();

            try
            {
                string clientId = "2447416122199968";
               // string redirectUrl = "https://192.227.69.198/LMT2M_test/Employee/oauth-redirect.aspx";
                string redirectUrl = "https://localhost:44352/Employee/oauth-redirect.aspx";

                string clientSecret = "omsairam14";
                string scope = "read_friendlists,user_status";
                string url = string.Format("https://graph.facebook.com/oauth/access_token?client_id={0}&redirect_url={1}&client_secret={2}&code={3}&scope={4}",
                clientId, redirectUrl, clientSecret, code, scope);
                //string url = "https://graph.facebook.com/oauth/access_token?=" + code + "";
                HttpWebRequest request = WebRequest.Create(url) as HttpWebRequest;


                request.Credentials = CredentialCache.DefaultCredentials;
                WebResponse response = request.GetResponse();
                Stream dataStream = response.GetResponseStream();
                StreamReader reader = new StreamReader(dataStream);
                string responseFromServer = reader.ReadToEnd();
                reader.Close();
                response.Close();
                JavaScriptSerializer js = new JavaScriptSerializer();
                Userclass userinfo = js.Deserialize<Userclass>(responseFromServer);
                imgprofile.ImageUrl = userinfo.picture;
                lblid.Text = userinfo.id;
                lblgender.Text = userinfo.gender;
                lbllocale.Text = userinfo.locale;
                lblname.Text = userinfo.name;
                hylprofile.NavigateUrl = userinfo.link;
                
            }
            catch(Exception )
            {

            }
            //try
            //{
            //    using (HttpWebResponse response = request.GetResponse() as HttpWebResponse)
            //    {
            //        StreamReader reader = new StreamReader(response.GetResponseStream());
            //        string retVal = reader.ReadToEnd();
            //        foreach (string token in retVal.Split('&'))
            //        {
            //            tokens.Add(token.Substring(0, token.IndexOf("=")),
            //                token.Substring(token.IndexOf("=") + 1, token.Length - token.IndexOf("=") - 1));
            //        }
            //    }
            //}
            //catch (Exception ex)
            //{
            //    ex.InnerException.ToString();
            //}

            return tokens;


        }
        private void InsertUpdateEmployee(string FirstName , string LastName , string Email , string Password , string UserName )
        {
            try
            {
                //  Guid EmployeeId_PK;
                //  Guid.TryParse("EmployeeId_PK", out EmployeeId_PK);
                //insert
                //string Password = Encryption.CreatePassword();
                //string UserName = Encryption.CreateUsername();
                string pass = Encryption.Encrypt(Password);
               // string verificationcode = Guid.NewGuid().ToString();
                EmployeeBE objEmployeeBE = new EmployeeBE();

                CompanyId = Session["CompanyId"].ToString();

                objEmployeeBE.propUserName = UserName;
                objEmployeeBE.propPassword = pass;
                objEmployeeBE.propCompanyId_FK = CompanyId;
                objEmployeeBE.propFirstName = FirstName;
                objEmployeeBE.propLastName = LastName;
                objEmployeeBE.propEmailId = Email;
                objEmployeeBE.PropBatchID = Session["BatchID"].ToString();

                Session["Email"] = Email;

                int Status = 0;
                Status = objEmployeeDAL.EmployeesRegistrationInsert1(objEmployeeBE);

                if (Status > 0)
                {
                  //  Response.Write(@"<script language='javascript'>alert('The following errors have occurred: \n" + "You have Registerd successfully, Please check Email for Assessment Password " + " .');</script>");

                    //System.Net.Mail.MailMessage mail = new System.Net.Mail.MailMessage();
                    //mail.To.Add(Email);
                    //mail.CC.Add("contact@letmetalk2.me");
                    //mail.Bcc.Add("priyamvada.bavadekar@letmetalk2.me");
                    //// mail.From = "contact@letmetalk2.me";
                    //// mail.From = new MailAddress("contact@letmetalk2.me", "Let me talk to me", System.Text.Encoding.UTF8);
                    //mail.Subject = "Welcome to the Health and Personality Insights and Actions Tool.";
                    //string getDt = DateTime.Now.Date.AddDays(7).ToString("MM/dd/yyyy");

                    ////mail.Body = "<html><body><font color=\"black\">Dear " + FirstName + " " + LastName + ",<br/><br/>Welcome to <b> Let Me Talk2 Me: A tool to quickly and discreetly assess your mindfulness.</b> You will be able to gain some useful insights into your own personality and learn about remedial actions in case they are needed. <b>'Let Me Talk2 Me'</b> will help you in self-discovery. It will lead on a path of self reflections while avoiding pitfalls of modern complex life and careers. <br/><br/>It is recommended that you use this tool periodically to monitor yourself and be aware of your health and personality parameters. <br/><br/> Please use the link below to login <br/><br/><a href='http://letmetalk2.me.hazel.arvixe.com/Employee/EmployeeLogin.aspx?first=1'> http://letmetalk2.me.hazel.arvixe.com/Employee/EmployeeLogin.aspx </a><br/>Your login details are as follows:<br/>UserId:<b>" + UserName + "</b><br/>Password:<b>" + Password + "</b><br/><br/>If you would like to take a break during assessment, please make sure you log out from the system or close the browser window for security purposes. Please do not leave the browser idle. You can always log back in whenever you like.<br/><br/>Please ensure that you complete this assessment on or before " + getDt + ". To get a maximum benefit, please repeat this exercise every 3 months and study the differentials.<br/> You will be notified for the same when the next assessment is due.<br/><br/>If you wish to join the ecosystem of mindfulness, please pre-register with us.<a href='http://letmetalk2.me/pre-register'>http://letmetalk2.me/pre-register</a></br>Thank You.<br/><br/></font></body></html>";

                    //mail.Body = "<html><body><font color=\"black\">Dear " + FirstName + " " + LastName + ",<br/><br/>Welcome to <b> Let Me Talk2 Me: A tool to quickly and discreetly assess your mindfulness.</b> You will be able to gain some useful insights into your own personality and learn about remedial actions in case they are needed. <b>'Let Me Talk2 Me'</b> will help you in self-discovery. It will lead on a path of self reflections while avoiding pitfalls of modern complex life and careers. <br/><br/>It is recommended that you use this tool periodically to monitor yourself and be aware of your health and personality parameters. <br/><br/> Please use the link below to login <br/><br/><a href='http://apps.brainlines.net/LMT2M_test/Employee/EmployeeLogin.aspx?first=1'> http://apps.brainlines.net/LMT2M_test/Employee/EmployeeLogin.aspx </a><br/>Your login details are as follows:<br/>UserId:<b>" + UserName + "</b><br/>Password:<b>" + Password + "</b><br/><br/>If you would like to take a break during assessment, please make sure you log out from the system or close the browser window for security purposes. Please do not leave the browser idle. You can always log back in whenever you like.<br/><br/>Please ensure that you complete this assessment on or before " + getDt + ". To get a maximum benefit, please repeat this exercise every 3 months and study the differentials.<br/> You will be notified for the same when the next assessment is due.<br/><br/>If you wish to join the ecosystem of mindfulness, please pre-register with us.<a href='http://letmetalk2.me/pre-register'>http://letmetalk2.me/pre-register</a></br>Thank You.<br/><br/></font></body></html>";

                    //mail.IsBodyHtml = true;

                    //mail.Priority = System.Net.Mail.MailPriority.High;
                    //SmtpClient client = new SmtpClient();
                    //// client.Credentials = new System.Net.NetworkCredential("contact@letmetalk2.me", "contact123");
                    ////  client.Port = 587;
                    //// client.Host = "dallas137.arvixeshared.com";
                    //client.EnableSsl = false;
                    //client.Send(mail);
                  // Response.Redirect("PaymentGateway.aspx");
                }
                else
                {
                    Response.Write(@"<script language='javascript'>alert('Alert: \n" + "EmailID Already Exists,Plese provide another Email ID" + " .');</script>");
                }


                DataTable dt = objEmployeeDAL.GetEmployeeByEmail(objEmployeeBE);
                Session["CompanyId"] = dt.Rows[0]["CompanyID_FK"].ToString();
                Session["EmployeeID"] = dt.Rows[0]["EmployeeId_PK"].ToString();
                Response.Redirect("PaymentGateway.aspx");
                

            }
            catch (Exception ex)
            {
                ex.InnerException.ToString();
            }
           
        }

    }

}