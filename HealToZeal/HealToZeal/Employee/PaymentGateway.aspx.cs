﻿using BE;
using DAL;
using System;
using System.Configuration;
using System.Data;
using System.Net.Mail;
using System.Text;

namespace HealToZeal.Employee
{
    public partial class PaymentGateway1 : System.Web.UI.Page
    {

        protected void Page_Load(object sender, EventArgs e)
        {
            Session["Heading"] = "";
            if (!IsPostBack)
            {
                if (Session["EmployeeID"].ToString() != null)
                {
                    EmployeeDAL objEmployeeDAL = new EmployeeDAL();
                    EmployeeBE objEmployeeBE = new EmployeeBE();
                    objEmployeeBE.propCompanyId_FK = Session["CompanyId"].ToString();
                    //objEmployeeBE.verificationcode = Session["verificationcode"].ToString();
                    objEmployeeBE.propEmployeeId_PK = Session["EmployeeID"].ToString();
                    DataTable dt1 = objEmployeeDAL.GetEmployeePrice(objEmployeeBE);

                    double discount = Convert.ToInt32(dt1.Rows[0]["CouponDiscountPercentage"].ToString());
                    double calculateDiscount = Convert.ToDouble(discount)/100;
                    double calculatePrice = calculateDiscount * Convert.ToDouble(dt1.Rows[0]["CouponAmount"].ToString());
                    lblprice.Text = calculatePrice.ToString();
                    lblname.Text = dt1.Rows[0]["CouponName"].ToString();
                    lblEmailid.Text = dt1.Rows[0]["EmailId"].ToString();
                    lblFirstname.Text = dt1.Rows[0]["FirstName"].ToString();
                    lblLastname.Text = dt1.Rows[0]["LastName"].ToString();
                    lblpercentage.Text = dt1.Rows[0]["CouponDiscountPercentage"].ToString();
                }
                else
                {

                }
            }
        }

        protected void paymentgateway_Click(object sender, EventArgs e)
        {

            //getEmployees();
            // ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('Please check Email for Assessment Login)", true);

            if (Session["EmployeeID"] != null)
            {
                EmployeeDAL _objEmployeeDAL = new EmployeeDAL();
                EmployeeBE _objEmployeeBE = new EmployeeBE();
                _objEmployeeBE.propEmployeeId_PK = Session["EmployeeID"].ToString();
                _objEmployeeBE.propTestFlag = "Paid";
                Int32 result = _objEmployeeDAL.UpdateTestFlag(_objEmployeeBE);
               
                    EmployeeDAL objEmployeeDAL = new EmployeeDAL();
                    EmployeeBE objEmployeeBE = new EmployeeBE();

                    objEmployeeBE.propEmployeeId_PK = Session["EmployeeID"].ToString();


                    DataTable dt = objEmployeeDAL.GetEmployeebyEmpid(objEmployeeBE);

                    string FirstName = dt.Rows[0]["FirstName"].ToString();
                    string LastName = dt.Rows[0]["LastName"].ToString();
                    string Email = dt.Rows[0]["EmailId"].ToString();
                    string Password1 = dt.Rows[0]["Password"].ToString();
                    String Password = Encryption.Decrypt(Password1);
                    string UserName = dt.Rows[0]["UserName"].ToString();
                    Session["CompanyId"] = dt.Rows[0]["CompanyID_FK"].ToString();

                    System.Net.Mail.MailMessage mail = new System.Net.Mail.MailMessage();

                    var loginUrl = ConfigurationManager.AppSettings.Get("Loginurl");
                    mail.To.Add(Email);
                    mail.CC.Add("contact@letmetalk2.me");
                    mail.Bcc.Add("priyamvada.bavadekar@letmetalk2.me");
                    // mail.From = "contact@letmetalk2.me";
                    // mail.From = new MailAddress("contact@letmetalk2.me", "Let me talk to me", System.Text.Encoding.UTF8);
                    mail.Subject = "Welcome to the Health and Personality Insights and Actions Tool.";
                    string getDt = DateTime.Now.Date.AddDays(7).ToString("MM/dd/yyyy");

                    //mail.Body = "<html><body><font color=\"black\">Dear " + FirstName + " " + LastName + ",<br/><br/>Welcome to <b> Let Me Talk2 Me: A tool to quickly and discreetly assess your mindfulness.</b> You will be able to gain some useful insights into your own personality and learn about remedial actions in case they are needed. <b>'Let Me Talk2 Me'</b> will help you in self-discovery. It will lead on a path of self reflections while avoiding pitfalls of modern complex life and careers. <br/><br/>It is recommended that you use this tool periodically to monitor yourself and be aware of your health and personality parameters. <br/><br/> Please use the link below to login <br/><br/><a href='http://letmetalk2.me.hazel.arvixe.com/Employee/EmployeeLogin.aspx?first=1'> http://letmetalk2.me.hazel.arvixe.com/Employee/EmployeeLogin.aspx </a><br/>Your login details are as follows:<br/>UserId:<b>" + UserName + "</b><br/>Password:<b>" + Password + "</b><br/><br/>If you would like to take a break during assessment, please make sure you log out from the system or close the browser window for security purposes. Please do not leave the browser idle. You can always log back in whenever you like.<br/><br/>Please ensure that you complete this assessment on or before " + getDt + ". To get a maximum benefit, please repeat this exercise every 3 months and study the differentials.<br/> You will be notified for the same when the next assessment is due.<br/><br/>If you wish to join the ecosystem of mindfulness, please pre-register with us.<a href='http://letmetalk2.me/pre-register'>http://letmetalk2.me/pre-register</a></br>Thank You.<br/><br/></font></body></html>";

                    //mail.Body = "<html><body><font color=\"black\">Dear " + FirstName + " " + LastName + ",<br/><br/>Welcome to <b> Let Me Talk2 Me: A tool to quickly and discreetly assess your mindfulness.</b> You will be able to gain some useful insights into your own personality and learn about remedial actions in case they are needed. <b>'Let Me Talk2 Me'</b> will help you in self-discovery. It will lead on a path of self reflections while avoiding pitfalls of modern complex life and careers. <br/><br/>It is recommended that you use this tool periodically to monitor yourself and be aware of your health and personality parameters. <br/><br/> Please use the link below to login <br/><br/><a href='"+ loginUrl + "?first=1'> "+ loginUrl + " </a><br/>Your login details are as follows:<br/>UserId:<b>" + UserName + "</b><br/>Password:<b>" + Password + "</b><br/><br/>If you would like to take a break during assessment, please make sure you log out from the system or close the browser window for security purposes. Please do not leave the browser idle. You can always log back in whenever you like.<br/><br/>Please ensure that you complete this assessment on or before " + getDt + ". To get a maximum benefit, please repeat this exercise every 3 months and study the differentials.<br/> You will be notified for the same when the next assessment is due.<br/><br/>If you wish to join the ecosystem of mindfulness, please pre-register with us.<a href='http://letmetalk2.me/pre-register'>http://letmetalk2.me/pre-register</a></br>Thank You.<br/><br/></font></body></html>";
                    StringBuilder body = new StringBuilder();
                    body.Append("<html><body><table width='100%' border='0' align='center' cellpadding='0' cellspacing='0' style='border: 2px solid #b3c8c8 !important'>");
                    body.Append("<tr><td><table width='100%' border='0' cellspacing='0' cellpadding='0'><tr>");
                    body.Append("<td valign='top'><table width='90%' border='0' align='center' cellpadding='0' cellspacing='0'> <tr>");
                    body.Append("<td  style='font-family:Verdana, Arial, Helvetica, sans-serif; font-size:12px; line-height:1.5; text-align:justify;'>&nbsp;</td>");
                    body.Append(" </tr><tr><td  style='font-family:Verdana, Arial, Helvetica, sans-serif;  font-size:13px; line-height:1.8; text-align:justify;'><div style='font-size:18px; color:#5b6766; font-weight:800;'>Dear " + FirstName + " " + LastName + ", </div>");
                    body.Append("<p><span  style='color:#829a9a; font-weight:800; font-size:16px; margin-bottom:0; padding-bottom:0;'>Welcome to Let Me Talk to Me :<br /></span><span style='font-size:13px; color:#829a9a; font-weight:800;'> A Health and Personality Insights and Actions Tool</span>");
                    body.Append(" <hr/>Welcome to Let Me Talk to Me: A tool to quickly and discreetly assess your mindfulness. You will be able to gain some useful insights into your own personality and learn about remedial actions in case they are needed. 'Let Me Talk to Me' will help you in self-discovery. It will lead on a path of self reflections while avoiding pitfalls of modern complex life and careers.");
                    body.Append(".<br /><br />");
                    body.Append("It is recommended that you use this tool periodically to monitor yourself and be aware of your health and personality parameters. <br />");
                    body.Append("</p><p style='color:#829a9a; font-weight:800; font-size:14px; margin-bottom:0; padding-bottom:0;'>Your login details are as follows</p>");
                    body.Append("<strong>User Id:</strong> "+UserName+"<br /><strong>Password:</strong> "+Password+"");
                    body.Append("<div style='clear: both; '></div>");
                    body.Append("<p style='color:#829a9a; font-weight:800; font-size:14px; margin-bottom:0; padding-bottom:0;'>Please use the link below to login </p>");
                    body.Append("<a href='" + ConfigurationManager.AppSettings.Get("Loginurl") + "?first=1'  target='_blank' style='background-color: #5b756c;padding:8px 10px;text-align:center; text-decoration:none; display: inline-block; font-size:15px; color: #fff;border-radius: 25px; margin-top:10px;'>letmetalk2.me</a>");
                    body.Append("</td></tr><tr><td  style='font-family:Verdana, Arial, Helvetica, sans-serif; font-size:12px; line-height:1.5; text-align:justify;'>&nbsp;<p>If you would like to take a break during assessment, please make sure you log out from the system or close the browser window for security purposes. Please do not leave the browser idle. You can always log back in whenever you like.");
                    body.Append("Please ensure that you complete this assessment on or before 02-23-2021. To get a maximum benefit, please repeat this exercise every 3 months and study the differentials.");
                    body.Append("You will be notified for the same when the next assessment is due.");
                    body.Append("If you wish to join the ecosystem of mindfulness, please pre-register with us.<a href='http://letmetalk2.me/pre-register'>http://letmetalk2.me/pre-register</a></br>Thank You.</p></td>");
                    body.Append("</tr></table></td></tr><tr><td height='140' bgcolor='#b3c8c8' style='font-family:Verdana, Arial, Helvetica, sans-serif; font-size:20px; color:#fff;'><table width='90%' border='0' align='center' cellpadding='0' cellspacing='0'>");
                    body.Append("<tr><td width='23%'><img src='" + ConfigurationManager.AppSettings.Get("ImagePath") + @"\UIFolder\dist\img\logotemp.png' width ='120' height='122' />");
                    body.Append("<td width='2%'>&nbsp;</td><td width='75%' align='left'><table width='100%' border='0' align='left' cellpadding='0' cellspacing='0'>");
                    body.Append("<tr><td style='color:#5b756c'><strong>let me talk team</strong></td></tr>");
                    body.Append("<tr><td height='28'style='font-size:14px; border-bottom:1px dotted #5b756c; padding-bottom:5px; color:#5b756c'>self test program</td>");
                    body.Append("</tr><tr><td style='font-size:12px; padding-top:5px; color:#5b756c'>https://www.letmetalk2.me/</td></tr><tr>");
                    body.Append("<td height='20' style='font-size:12px; color:#5b756c'><span style='font-size:12px; padding-top:5px;'>©2020 let me talk. all rights reserved.</span></td>");
                    body.Append("</tr></table></td></tr></table></td></tr></table></td></tr></table></body></html>");
                    mail.Body = body.ToString();
                    mail.IsBodyHtml = true;

                    mail.Priority = System.Net.Mail.MailPriority.High;
                    SmtpClient client = new SmtpClient();
                    // client.Credentials = new System.Net.NetworkCredential("contact@letmetalk2.me", "contact123");
                    //  client.Port = 587;
                    // client.Host = "dallas137.arvixeshared.com";
                    client.EnableSsl = false;
                    client.Send(mail);

                    string message = "Please check Email for Assessment Login";
                    string script = "window.onload = function(){ alert('";
                    script += message;
                    script += "')};";
                    ClientScript.RegisterStartupScript(this.GetType(), "SuccessMessage", script, true);
            }
            //Response.Redirect("EmployeeLogin.aspx");
        }

    }
}