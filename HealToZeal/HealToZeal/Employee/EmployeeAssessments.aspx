﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master/LetMeTalkWithMenu.Master" AutoEventWireup="true" CodeBehind="EmployeeAssessments.aspx.cs" Inherits="HealToZeal.Employee.EmployeeAssessments1" Async="true"   %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">
     function openModalAssessments() {
            $('[id*=ModalAssessments]').modal('show');
        }
        </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="row">
        <div class="col-md-11 col-centered">
            <div class="col-md-12  bdr3 mb20 pad0" style="padding-bottom: 0px !important;">

                <div class="boardp" style="padding-top: 0;">
                    <div>

                        <asp:DataList ID="DlistAssessments" runat="server" Width="100%" OnItemCommand="DlistAssessments_ItemCommand">
                            <HeaderTemplate>
                                <%--  <div class="hs_sub_comment">--%>
                            </HeaderTemplate>
                            <ItemTemplate>

                                <div class="pb28 mt30">
                                    <div class="col-md-9 col-sm-8">

                                        <img src="../UIFolder/dist/img/profile-page.png" class="pull-left mr30 img-responsive mb5">
                                        <h3 class="mt30 mb0 w600">
                                            <%--<asp:Label ID="lblSrNo" runat="server" Text='<%#Eval("SrNo") %>'></asp:Label>--%>
                                            <asp:Label ID="lblSrNo" runat="server" Text='1'></asp:Label>
                                            <asp:Label ID="lblDateSubmitted" runat="server" Text='<%# "On "+ DataBinder.Eval(Container.DataItem, "CreatedDate", "{0:dd/MM/yyyy}") %>'></asp:Label><asp:Label ID="lblAssessmentId" runat="server" Text='<%#Eval("AssessmentInstanceId_FK") %>' Visible="false"></asp:Label>
                                            <asp:Label ID="lblAssessmentInstanceCompanyId" runat="server" Text='<%#Eval("InstanceCompanyRelationId_PK") %>' Visible="false"></asp:Label></h3>

                                    </div>
                                    <div class="col-md-3  col-sm-4 mt30 pad0">
                                        <a href="">
                                            <asp:Button ID="btnView" runat="server" Visible='<%#Convert.ToBoolean(Eval("AssessmentSubmitted"))%>' Text="View" CommandArgument='<%#Eval("AssessmentInstanceId_FK") %>' CommandName="ViewReport" CssClass="loginbtn" />

                                        </a>
                                        <a href="">
                                            <asp:Button ID="btnGraph" runat="server" Visible='<%# ((Eval("AssessmentSubmitted").Equals(false)) && (Eval("LastUpadte")==DBNull.Value)) %>' Text="expired" CssClass="loginbtn" />

                                        </a>

                                        <a href="">
                                            <asp:Button ID="btnExpired" runat="server" Visible='<%# (Eval("AssessmentSubmitted").Equals(false) && (Eval("LastUpadte")!=DBNull.Value)) %>' Text="start test" CommandArgument='<%#Eval("AssessmentInstanceId_FK") %>' CommandName="ViewTest" CssClass="loginbtn" />

                                        </a>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>

                                <%--<ul>
                                                                            <li>
                                                                                <asp:Label ID="lblSrNo" runat="server" Text='<%#Eval("SrNo") %>'></asp:Label>
                                                                            </li>--%>

                                <%--<li>
                                                                                <asp:Label ID="lblDateSubmitted" runat="server" Text='<%# "On "+ DataBinder.Eval(Container.DataItem, "CreatedDate", "{0:dd/MM/yyyy}") %>'></asp:Label>
                                                                            </li>
                                                                            <li>
                                                                                <asp:Label ID="lblAssessmentId" runat="server" Text='<%#Eval("AssessmentInstanceId_FK") %>' Visible="false"></asp:Label>
                                                                            </li>
                                                                            <li>
                                                                                <asp:Label ID="lblAssessmentInstanceCompanyId" runat="server" Text='<%#Eval("InstanceCompanyRelationId_PK") %>' Visible="false"></asp:Label>
                                                                            </li>--%>
                                <%--<li>
                                                                                <a href="">
                                                                                    <asp:Button ID="btnView" runat="server" Visible='<%#Convert.ToBoolean(Eval("AssessmentSubmitted"))%>' Text="View" CommandArgument='<%#Eval("AssessmentInstanceId_FK") %>' CommandName="ViewReport" CssClass="loginbtn" />

                                                                                </a>
                                                                                <a href="">
                                                                                    <asp:Button ID="btnGraph" runat="server" Visible='<%# ((Eval("AssessmentSubmitted").Equals(false)) && (Eval("LastUpadte")==DBNull.Value)) %>' Text="Expired" CssClass="loginbtn" />

                                                                                </a>

                                                                                <a href="">
                                                                                    <asp:Button ID="btnExpired" runat="server" Visible='<%# (Eval("AssessmentSubmitted").Equals(false) && (Eval("LastUpadte")!=DBNull.Value)) %>' Text="Take a new test" CommandArgument='<%#Eval("AssessmentInstanceId_FK") %>' CommandName="ViewTest" CssClass="loginbtn" />

                                                                                </a>

                                                                            </li>
                                                                        </ul>--%>
                                <%--  <i class="fa fa-paperclip"></i>--%>
                                <p>
                                </p>

                                </div>
                                                                </div>
                                                            </div>


                                                        </div>
                                                    </div>
                                                    <%--</div> --%>
                                                </div>
                                                <div class="clearfix"></div>
                                <div class="hs_margin_40"></div>
                            </ItemTemplate>
                        </asp:DataList>

                    </div>
                </div>
            </div>
        </div>
    </div>
     <div id="openModalAssessments" class="modal fade" role="dialog">
                <div class="modal-dialog modal-md">

                    <!-- Modal content-->
                    <div class="modal-content">

                        <div class="modal-body">
                            <div class="col-md-12 mt30">
                                <h4 class="modal-title">message</h4>
                                <p class="w400">
                                   <asp:Label ID="lblMessage1" runat="server" ></asp:Label>
                                </p>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <a href="EmpRegistrationForm.aspx?InstanceCompanyRelationId=3FB35DBD-00B3-4704-BA2C-BC7DC3F244AF&CouponId=1">
                                <img src="../UIFolder/dist/img/close.png" class="popicon"></a>
                        </div>
                    </div>

                </div>
            </div>
</asp:Content>
