﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using BE;
using DAL;
using System.Web.Mail;
using System.Net.Mail;
using System.Text;
using System.Configuration;

namespace HealToZeal.Employee
{
    public partial class Questions : System.Web.UI.Page
    {
        log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        AssessmentQuestionRelationDAL objAssessmentQuestionRelationDAL = new AssessmentQuestionRelationDAL();
        EmployeeAssessmentDetailsDAL objEmployeeAssessmentDetailsDAL = new EmployeeAssessmentDetailsDAL();
        EmployeeAssessmentDetailsBE objEmployeeAssessmentDetailsBE = new EmployeeAssessmentDetailsBE();
        AnswerMasterDAL objAnswerMasterDAL = new AnswerMasterDAL();
        DataTable DtSaveAnswer = new DataTable();
        public static List<int> arrayqueCntList = new List<int>();
        string AssessmentId = string.Empty;
        string QuestionId = string.Empty;
        string EmployeeId = string.Empty;
        int position = 0;
        int Default = 1;
        public static int maxNum = 0;
        public static int queCount = 0;
        public static string CurrentLayout = string.Empty;
        RandomGenerator randomGenerator = new RandomGenerator();
        public static List<string> queList = new List<string>();
        public static int queLayoutIndex;

        protected void Page_Load(object sender, EventArgs e)
        {
            //Session["Heading"] = "assessment";
           // divalert.Visible = false;
            string batchID = string.Empty;
            if (Session["BatchID"] != null && Session["BatchID"].ToString() != "")
            {
                batchID = Convert.ToString(Session["BatchID"].ToString());
            }
            if (!Page.IsPostBack)
            {
                queList.Clear();
                queList.Add("queLayout1");
                queList.Add("queLayout2");
                queList.Add("queLayout3");
                queList.Add("queLayout4");
                queLayoutIndex = 0;
                queCount = 0;
                arrayqueCntList.Clear();
                Session["Pagename"] = null;
                try
                {
                    if (Session["EmployeeId"] != null)
                    {
                        //assessment id total question count
                        //AssessmentId = "AAD38F93-0DC7-4EA1-8A61-6B95816AED6B"; //assessment session
                        EmployeeId = Convert.ToString(Session["EmployeeId"].ToString());

                        //check whether assessment is submitted or not
                        EmployeeAssessmentsHistoryDAL objEmployeeAssessmentsHistoryDAL = new EmployeeAssessmentsHistoryDAL();
                        DataTable DtAssessmentSubmitted = objEmployeeAssessmentsHistoryDAL.EmployeeAssementsGet(EmployeeId, batchID);

                        if (DtAssessmentSubmitted != null && DtAssessmentSubmitted.Rows.Count > 0)
                        {
                            if (DtAssessmentSubmitted.Select("AssessmentSubmitted=0").Length > 0)
                            {
                                DataTable DtTotalQuestionCount = new DataTable();
                                //if (Convert.ToString(Session["FlagTest"]) == "free")

                                //DtTotalQuestionCount = objEmployeeAssessmentDetailsDAL.EmployeeFreeAssessmentDetailsGetCount(EmployeeId);

                                //Session["TotalQuestionCount"] = DtTotalQuestionCount.Rows[0][0].ToString();
                                //maxNum = Convert.ToInt32(Session["TotalQuestionCount"].ToString()) - 1;


                                if (Session["AssessmentInstanceId"] != null && Session["AssessmentInstanceCompanyId"] != null)
                                {
                                    DataTable AssessmentInstanceDt= objEmployeeAssessmentDetailsDAL.AssessmentInstanceName(Session["AssessmentInstanceId"].ToString());
                                    Session["Heading"] = Convert.ToString(AssessmentInstanceDt.Rows[0][0]);

                                    DtTotalQuestionCount = objEmployeeAssessmentDetailsDAL.EmployeeAssessmentDetailsGetCount(EmployeeId, Session["AssessmentInstanceId"].ToString(), Session["AssessmentInstanceCompanyId"].ToString());

                                    if (DtTotalQuestionCount != null && DtTotalQuestionCount.Rows.Count > 0)
                                    {
                                        Session["TotalQuestionCount"] = DtTotalQuestionCount.Rows[0][0].ToString();
                                        maxNum = Convert.ToInt32(Session["TotalQuestionCount"].ToString());
                                        //if (Session["Position"] == null)
                                        //{
                                        //    btnPrevious.Enabled = false;
                                        //}
                                    }
                                }
                                else
                                {
                                    Response.Redirect("EmployeeAssessments.aspx", false);
                                }

                                DataTable dtEmployeesAssementDetails = new DataTable();
                                //  dtEmployees = objEmployeeAssessmentDetailsDAL.EmployeeAssessmentDetailsGet(Session["AssessmentInstanceId"].ToString(), position, Session["EmployeeId"].ToString(), Session["AssessmentInstanceCompanyId"].ToString());
                                dtEmployeesAssementDetails = objEmployeeAssessmentDetailsDAL.SpEmployeeAssessmentDetailsGetAll(Session["AssessmentInstanceId"].ToString(), position, Session["EmployeeId"].ToString(), Session["AssessmentInstanceCompanyId"].ToString());
                                if (dtEmployeesAssementDetails != null && dtEmployeesAssementDetails.Rows.Count > 0)
                                {
                                    //  Session["TotalAnswers"] = Convert.ToInt32(dtEmployees.Rows[0]["TotalAnswers"].ToString());
                                    //    position = Convert.ToInt32(dtEmployees.Rows[0]["TotalAnswers"].ToString());
                                    //string str = dtEmployees.Rows[0]["TotalAnswers"].ToString();
                                    //str = str.TrimEnd(str[str.Length - 1]) + "0";
                                    //position = Convert.ToInt32(str);

                                    foreach (DataRow dr in dtEmployeesAssementDetails.AsEnumerable())
                                    {
                                        arrayqueCntList.Add(Convert.ToInt32(dr["SrNo"]));
                                    }

                                    int count = ((Convert.ToInt32(dtEmployeesAssementDetails.Rows.Count) / 1) * 1);
                                    // position = GetAlternateNumber(0, maxNum);
                                    queCount = count;
                                    //position = count;
                                    Session["Position"] = position;
                                }
                                position = GetAlternateNumber(0, maxNum);
                                Session["Position"] = position;

                                if (Session["Position"] == null)
                                {
                                    //btnPrevious.Enabled = false;
                                    //btnPrevious1.Enabled = false;
                                }

                                BindTestQuestions(position);
                                //27 jan 2015
                                BindSavedAnswers(position);
                                //}
                                //else
                                //{
                                //    Response.Redirect("EmployeeAssessments.aspx");
                                //}
                                //
                            }
                            else
                            {
                                Response.Redirect("EmployeeHome.aspx?Assessment=Y");
                            }
                        }
                        //else
                        //{

                        //}
                    }
                    else
                    {
                        Response.Redirect("EmployeeLogin.aspx");
                    }
                }
                catch (Exception ex)
                {
                    log.Error(ex.Message.ToString() + " " + ex.StackTrace.ToString());
                }
            }
        }

        public void BindTestQuestions(int Position)
        {
            try
            {
                ////AssessmentId = Session["AssessmentId"].ToString();
                ////AssessmentId = "AAD38F93-0DC7-4EA1-8A61-6B95816AED6B";

                //if (Session["Position"] != null)
                //{
                //    position = Convert.ToInt32(Session["Position"].ToString());
                //}
                ////if (position > 0)
                ////{
                ////    PopulatePager(position, (position / 10));
                ////}
                DataTable dtAssessmentQuestions = new DataTable();
                //if (Convert.ToString(Session["FlagTest"]) == "free")
                //{
                //     dtAssessmentQuestions = objAssessmentQuestionRelationDAL.AssessmentFreeQuestionsRelationGet(Session["EmployeeId"].ToString(), Position, Session["AssessmentInstanceId"].ToString(), Session["AssessmentInstanceCompanyId"].ToString());
                //}
                //else
                //{
                dtAssessmentQuestions = objAssessmentQuestionRelationDAL.AssessmentQuestionsRelationGet(Session["EmployeeId"].ToString(), Position, Session["AssessmentInstanceId"].ToString(), Session["AssessmentInstanceCompanyId"].ToString());
                //}
                DlistQuestions.DataSource = dtAssessmentQuestions;
                DlistQuestions.DataBind();

                //CheckPosition();
                BindAnswers();
                ShowAlternateQuestionLayout();
                queCount++;
                //if (Session["Position"] != null)
                //Session["Position"] = Convert.ToInt32(Session["Position"].ToString()) + DlistQuestions.Items.Count ;
                //else
                //Session["Position"] = DlistQuestions.Items.Count;
            }
            catch (Exception ex)
            {
                log.Error(ex.Message.ToString() + " " + ex.StackTrace.ToString());
            }
        }

        public void BindAnswers()
        {
            try
            {
                DataTable DtAnswers = new DataTable();
                foreach (DataListItem questions in DlistQuestions.Items)
                {
                    QuestionId = ((Label)questions.FindControl("lblQuestionId")).Text;
                    DtAnswers = objAnswerMasterDAL.AnswerMasterGetWithDescriptionByQuestionId(QuestionId);
                    ((RadioButtonList)questions.FindControl("rblistAnswers")).DataSource = DtAnswers;
                    ((RadioButtonList)questions.FindControl("rblistAnswers")).DataTextField = "Answer";
                    ((RadioButtonList)questions.FindControl("rblistAnswers")).DataValueField = "AnswerId_PK";
                    ((RadioButtonList)questions.FindControl("rblistAnswers")).DataBind();

                    ((RadioButtonList)questions.FindControl("rblistAnswers1")).DataSource = DtAnswers;
                    ((RadioButtonList)questions.FindControl("rblistAnswers1")).DataTextField = "Answer";
                    ((RadioButtonList)questions.FindControl("rblistAnswers1")).DataValueField = "AnswerId_PK";
                    ((RadioButtonList)questions.FindControl("rblistAnswers1")).DataBind();

                    ((RadioButtonList)questions.FindControl("rblistAnswers2")).DataSource = DtAnswers;
                    ((RadioButtonList)questions.FindControl("rblistAnswers2")).DataTextField = "Answer";
                    ((RadioButtonList)questions.FindControl("rblistAnswers2")).DataValueField = "AnswerId_PK";
                    ((RadioButtonList)questions.FindControl("rblistAnswers2")).DataBind();
                    //((Repeater)questions.FindControl("ansRepeater")).DataSource = DtAnswers;
                    //((Repeater)questions.FindControl("ansRepeater")).DataBind();

                    ((RadioButtonList)questions.FindControl("rblistAnswers3")).DataSource = DtAnswers;
                    ((RadioButtonList)questions.FindControl("rblistAnswers3")).DataTextField = "Answer";
                    ((RadioButtonList)questions.FindControl("rblistAnswers3")).DataValueField = "AnswerId_PK";
                    ((RadioButtonList)questions.FindControl("rblistAnswers3")).DataBind();
                    //((Repeater)questions.FindControl("ansRepeater")).DataSource = DtAnswers;
                    //((Repeater)questions.FindControl("ansRepeater")).DataBind();
                }

            }
            catch (Exception ex)
            {
                log.Error(ex.Message.ToString() + " " + ex.StackTrace.ToString());
            }
        }

        protected void btnNext_Click(object sender, EventArgs e)
        {
            try
            {
                if (Session["EmployeeId"] != null)
                {
                    try
                    {
                        //save answers 
                        int status = SaveAnswersSession();
                        if (status > 0)
                        {
                            if (Session["Position"] != null)
                            {
                                //if (((rptPager.Items.Count - 2) * 10) == Convert.ToInt32(Session["Position"].ToString()))
                                //{

                                //}
                                //else
                                //{
                                //Session["Position"] = Convert.ToInt32(Session["Position"].ToString()) + DlistQuestions.Items.Count;
                                Session["Position"] = GetAlternateNumber(0, maxNum);

                                //}                          
                                //btnPrevious.Enabled = true;
                                // btnPrevious1.Enabled = true;
                            }
                            else
                            {
                                Session["Position"] = DlistQuestions.Items.Count;
                            }

                            BindTestQuestions(Convert.ToInt32(Session["Position"].ToString()));
                            BindSavedAnswers(Convert.ToInt32(Session["Position"].ToString()));

                            int Count = Convert.ToInt32(Session["Position"].ToString()) + DlistQuestions.Items.Count;
                            //if (Convert.ToInt32(Session["TotalQuestionCount"].ToString()) == Convert.ToInt32(Session["Position"].ToString()))
                            if (Convert.ToInt32(Session["TotalQuestionCount"].ToString()) == queCount)
                            {
                                Session["TotalQuestionCount"] = null;
                                Session["Position"] = null;
                                //send mail 
                                EmployeeAssessmentsHistoryBE objEmployeeAssessmentsHistoryBE = new EmployeeAssessmentsHistoryBE();
                                EmployeeAssessmentsHistoryDAL objEmployeeAssessmentsHistoryDAL = new EmployeeAssessmentsHistoryDAL();

                                objEmployeeAssessmentsHistoryBE.propEmployeeId_FK = EmployeeId;
                                //objEmployeeAssessmentsHistoryBE.propAssessmentId_FK=
                                objEmployeeAssessmentsHistoryBE.propAssessmentSubmitted = "Y";
                                objEmployeeAssessmentsHistoryBE.propCreatedBy = EmployeeId;
                                objEmployeeAssessmentsHistoryBE.propCreatedDate = DateTime.Now.Date;
                                objEmployeeAssessmentsHistoryBE.propAssessmentInstanceId_FK = Session["AssessmentInstanceId"].ToString();
                                objEmployeeAssessmentsHistoryBE.propInstanceCompanyRelationId_FK = Session["AssessmentInstanceCompanyId"].ToString();

                                status = objEmployeeAssessmentsHistoryDAL.EmployeeAssessmentsHistoryInsert(objEmployeeAssessmentsHistoryBE);
                                //lblMessage.Text = "Assessment submitted successfully...";


                                if (status > 0)
                                {
                                    //Session["EmployeeId"]

                                    CompanyDetailsDAL objCompanyDetailsDAL = new CompanyDetailsDAL();
                                    DataTable DtCompany = objCompanyDetailsDAL.CompanyDetailsGetByEmployeeId(Session["EmployeeId"].ToString());
                                    //CompanyName
                                    System.Net.Mail.MailMessage mail = new System.Net.Mail.MailMessage();
                                    // System.Web.Mail.MailMessage mail = new System.Web.Mail.MailMessage();

                                    mail.To.Add("priyamvada.bavadekar@letmetalk2.me");
                                    mail.Bcc.Add("priyamvada.bavadekar@letmetalk2.me");
                                    // mail.From = "contact@letmetalk2.me";
                                    //mail.From = new MailAddress("contact@letmetalk2.me", "Let me talk to me", System.Text.Encoding.UTF8);
                                    mail.Subject = "Assessment submitted by user";
                                    string getDt = DateTime.Now.Date.AddDays(7).ToString("MM/dd/yyyy");

                                    mail.Body = "<html><body><font color=\"black\">Hello Mam, <br/><br/> <b>" + Session["UserName"].ToString() + "</b> with EmployeeId " + Session["EmployeeCompanyId"].ToString() + " of company " + DtCompany.Rows[0]["CompanyName"].ToString() + " has submitted the assessment successfully. <br/><br/> Thank You.<BR/><br/> Best Regards, <br/><br/> Let me talk to me Team.</font></body></html>";
                                    mail.IsBodyHtml = true;

                                    mail.Priority = System.Net.Mail.MailPriority.High;
                                    SmtpClient client = new SmtpClient();
                                    //client.Credentials = new System.Net.NetworkCredential("contact@letmetalk2.me", "contact123");
                                    //client.Port = 587;
                                    //client.Host = "hazel.arvixe.com";
                                    client.EnableSsl = false;

                                    client.Send(mail);


                                    //message = "Employee added successfully...";
                                    //divalert.Attributes.Add("class", "alert alert-success");
                                    //lblMessage.Text = "Employee added successfully...";


                                    //System.Web.Mail.MailMessage mail = new System.Web.Mail.MailMessage();
                                    //mail.To = "priyamvada@chetanasystems.com";
                                    //mail.From = "HealToZeal@chetanatspl.com";
                                    //mail.Subject = "Assessment submitted by user";
                                    //mail.Body = "<html><body><font color=\"black\">Hello Mam, <br/><br/> <b>" + Session["UserName"].ToString() + "</b> with EmployeeId " + Session["EmployeeCompanyId"].ToString() + " of company " + DtCompany.Rows[0]["CompanyName"].ToString() + " has submitted the assessment successfully. <br/><br/> Thank You.<BR/><br/> Best Regards, <br/><br/> HealToZealTeam.</font></body></html>";
                                    //mail.BodyFormat = MailFormat.Html;
                                    //mail.Fields.Add("http://schemas.microsoft.com/cdo/configuration/smtpauthenticate", "1");    //basic authentication
                                    //mail.Fields.Add("http://schemas.microsoft.com/cdo/configuration/sendusername", "HealToZeal@chetanatspl.com"); //set your username here
                                    //mail.Fields.Add("http://schemas.microsoft.com/cdo/configuration/sendpassword", "HealToZeal");    //set your password here

                                    //System.Web.Mail.SmtpMail.SmtpServer = "holly.arvixe.com";  //your real server goes here                        
                                    //System.Web.Mail.SmtpMail.Send(mail);

                                    /*  System.Net.Mail.MailMessage insMail = new System.Net.Mail.MailMessage();
                                       System.Net.Mail.MailAddress insFrom = new MailAddress("HealToZeal@chetanatspl.com", "HealToZeal");
                                       insMail.From = insFrom;
                                       insMail.To.Add("priyamvada@chetanasystems.com");
                                       insMail.Subject = "Assessment submitted by user";
                                       insMail.IsBodyHtml = true;
                                       string strBody = "<html><body><font color=\"black\">Hello Mam <br/><br/> <b>" + Session["UserName"].ToString() + "</b> with EmployeeId " + Session["EmployeeCompanyId"].ToString() + " has submitted the assessment successfully. <br/><br/> Thank You.<BR/><br/> Best Regards, <br/><br/> HealToZealTeam.</font></body></html>";
                                       insMail.Body = strBody;                               
                                       System.Net.Mail.SmtpClient ns = new System.Net.Mail.SmtpClient("holly.arvixe.com");

                                       ns.Send(insMail);*/
                                    //
                                    // Response.Redirect("EmployeeThanksMessage.aspx?Assessment=submited",false);

                                    //ask for another test
                                    string batchID = Session["BatchID"].ToString();
                                    DataTable DtAssessmentSubmitted = objEmployeeAssessmentsHistoryDAL.EmployeeAssementsGet(EmployeeId, batchID);

                                    if (DtAssessmentSubmitted != null && DtAssessmentSubmitted.Rows.Count > 0)
                                    {
                                        if (DtAssessmentSubmitted.Select("AssessmentSubmitted=0").Length > 0)
                                        {
                                            Response.Redirect("SuggestionFrnd.aspx");
                                            // Response.Redirect("AssessmentMessage.aspx");
                                            //  ScriptManager.RegisterClientScriptBlock(this, GetType(), "MessageBox", "DisplayColorBox('AssessmentMessage.aspx');", true);
                                        }
                                        else
                                        {
                                            //Response.Redirect("EmployeeFeedback.aspx", false);
                                            Response.Redirect("FeedBack.aspx", false);
                                        }
                                    }
                                }
                                else
                                {
                                    //lblmessage.Text = "Can not submit test...";
                                    lblMessageInfo.Text = "Can not submit test...";
                                    ClientScript.RegisterStartupScript(this.GetType(), "Pop", "openModalQuestion();", true);
                                }
                            }
                            else if (Convert.ToInt32(Session["TotalQuestionCount"].ToString()) == Count)
                            {
                                //  btnNext.Text = "Submit";
                            }
                            if (Session["Position"] != null)
                            {
                                //btnPrevious.Enabled = true;
                                // btnPrevious1.Enabled = true;
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        log.Error(ex.Message.ToString() + " " + ex.StackTrace.ToString());
                    }
                }
                else
                {
                    Response.Redirect("EmployeeLogin.aspx", false);
                }

            }
            catch (Exception ex)
            {
                // ex1.InnerException.ToString();
                log.Error(ex.Message.ToString() + " " + ex.StackTrace.ToString());
                //Response.Redirect("EmployeeLogin.aspx",false );
            }
        }

        private int SaveAnswersSession()
        {
            try
            {
                int status = 0;
                // objEmployeeAssessmentDetailsBE.propAssessmentId_FK = "9465B829-997E-42F1-85D2-128FF3E5B6C6";// AAD38F93 -0DC7-4EA1-8A61-6B95816AED6B"; //AssessmentId 

                EmployeeId = Session["EmployeeId"].ToString();
                objEmployeeAssessmentDetailsBE.propEmployeeId_FK = EmployeeId;
                //    objEmployeeAssessmentDetailsBE.propEmployeeId_FK = "C885389E-8F6D-4B6D-AAC3-57EE93336C25"; // logged in user
                objEmployeeAssessmentDetailsBE.propCreatedBy = EmployeeId;
                //  objEmployeeAssessmentDetailsBE.propCreatedBy = "C885389E-8F6D-4B6D-AAC3-57EE93336C25";
                objEmployeeAssessmentDetailsBE.propCreatedDate = DateTime.Now.Date;

                foreach (DataListItem answerItem in DlistQuestions.Items)
                {
                    QuestionId = ((Label)answerItem.FindControl("lblQuestionId")).Text;
                    //  string AnswerId = ((RadioButtonList)answerItem.FindControl("rblistAnswers")).SelectedItem.Value;
                    string AnswerId = "";
                    RadioButtonList rblistAnswers = (RadioButtonList)answerItem.FindControl("rblistAnswers");
                    foreach (ListItem rdo in rblistAnswers.Items)
                    {
                        if (rdo.Selected)
                        {
                            AnswerId = rdo.Value;
                        }
                    }
                    RadioButtonList rblistAnswers1 = (RadioButtonList)answerItem.FindControl("rblistAnswers1");
                    foreach (ListItem rdo in rblistAnswers1.Items)
                    {
                        if (rdo.Selected)
                        {
                            AnswerId = rdo.Value;
                        }
                    }
                    RadioButtonList rblistAnswers2 = (RadioButtonList)answerItem.FindControl("rblistAnswers2");
                    foreach (ListItem rdo in rblistAnswers2.Items)
                    {
                        if (rdo.Selected)
                        {
                            AnswerId = rdo.Value;
                        }
                    }
                    RadioButtonList rblistAnswers3 = (RadioButtonList)answerItem.FindControl("rblistAnswers3");
                    foreach (ListItem rdo in rblistAnswers3.Items)
                    {
                        if (rdo.Selected)
                        {
                            AnswerId = rdo.Value;
                        }
                    }
                    objEmployeeAssessmentDetailsBE.propAnswerId_FK = AnswerId;
                    objEmployeeAssessmentDetailsBE.propQuestionId_FK = QuestionId;
                    objEmployeeAssessmentDetailsBE.propInstanceCompanyRelationId_FK = Session["AssessmentInstanceCompanyId"].ToString();
                    status = objEmployeeAssessmentDetailsDAL.EmployeeAssessmentDetailsInsert(objEmployeeAssessmentDetailsBE);
                   // divalert.Visible = true;
                    {
                        if (status <= 0)
                        {
                            // ScriptManager.RegisterStartupScript(this, this.GetType(), "starScript", "DisplayColorBox('WarningMessage.aspx');", true);  
                            //divalert.Attributes.Add("class", "alert alert-danger");
                            //lblMessage.Text = "Please select answer";
                            lblMessageInfo.Text = "Please select answer";
                            ClientScript.RegisterStartupScript(this.GetType(), "Pop", "openModalQuestion();", true);
                            break;
                        }
                        else
                        {
                            // divalert.Attributes.Add("class", "alert alert-success");
                           // divalert.Visible = false;
                           // lblMessage.Text = "";
                        }
                    }
                }
                return status;
            }
            catch (Exception ex)
            {
                log.Error(ex.Message.ToString() + " " + ex.StackTrace.ToString());
                return 0;
            }
        }

        protected void btnPrevious_Click(object sender, EventArgs e)
        {
            try
            {
                int Count = Convert.ToInt32(Session["Position"].ToString()) + DlistQuestions.Items.Count;
                if (Convert.ToInt32(Session["TotalQuestionCount"].ToString()) == Count)
                {
                    SaveAnswersSession();
                }

                if (Session["Position"] != null)
                {

                    // Session["Position"]
                    Count = Convert.ToInt32(Session["Position"].ToString()) - Default;
                    if (Count == 0)
                    {
                        // btnPrevious.Enabled = false;
                        // btnPrevious1.Enabled = false;
                    }
                    else
                    {
                        // btnPrevious.Enabled = true;
                        //btnPrevious1.Enabled = true;
                    }
                }
                Session["Position"] = Count;
                BindTestQuestions(Convert.ToInt32(Count));
                BindSavedAnswers(Convert.ToInt32(Count));
            }
            catch (Exception ex)
            {
                log.Error(ex.Message.ToString() + " " + ex.StackTrace.ToString());
                Response.Redirect("EmployeeLogin.aspx", false);
            }
        }

        public void BindSavedAnswers(int Position)
        {
            try
            {
                EmployeeAssessmentDetailsDAL objEmployeeAssessmentDetailsDAL = new EmployeeAssessmentDetailsDAL();
                DataTable DtEmployeeAnswers = new DataTable();

                AssessmentId = "9465B829-997E-42F1-85D2-128FF3E5B6C6";//"AAD38F93-0DC7-4EA1-8A61-6B95816AED6B"; //AssessmentId commented by meenakshi on 2019-01-10
                EmployeeId = Session["EmployeeId"].ToString();

                //27 jan 2015
                //if (Session["Position"] != null)
                //{
                //    position = Convert.ToInt32(Session["Position"].ToString());
                //}

                DtEmployeeAnswers = objEmployeeAssessmentDetailsDAL.EmployeeAssessmentDetailsGet(Session["AssessmentInstanceId"].ToString(), Position, EmployeeId, Session["AssessmentInstanceCompanyId"].ToString());

                foreach (DataListItem answerItem in DlistQuestions.Items)
                {
                    QuestionId = ((Label)answerItem.FindControl("lblQuestionId")).Text;
                    //  string AnswerId = ((RadioButtonList)answerItem.FindControl("rblistAnswers")).SelectedItem.Value;

                    DataRow[] DrAnswer = DtEmployeeAnswers.Select("QuestionId_FK='" + QuestionId + "'");
                    if (DrAnswer.Length > 0 && DrAnswer != null)
                    {
                        RadioButtonList rblistAnswers = (RadioButtonList)answerItem.FindControl("rblistAnswers");
                        foreach (ListItem rdo in rblistAnswers.Items)
                        {
                            if (DrAnswer[0][1].ToString() == rdo.Value.ToString())
                            {
                                rdo.Selected = true;
                            }
                        }
                        FocusControlOnPageLoad(rblistAnswers.ClientID, this.Page);
                    }
                }


                int percentage = 0;
                //System.Web.UI.HtmlControls.HtmlGenericControl Progressbar = DlistQuestions.Items[0].FindControl("Progressbar") as System.Web.UI.HtmlControls.HtmlGenericControl;
                //System.Web.UI.HtmlControls.HtmlGenericControl Progressbar1 = DlistQuestions.Items[0].FindControl("Progressbar1") as System.Web.UI.HtmlControls.HtmlGenericControl;
                //System.Web.UI.HtmlControls.HtmlGenericControl Progressbar2 = DlistQuestions.Items[0].FindControl("Progressbar2") as System.Web.UI.HtmlControls.HtmlGenericControl;
                //System.Web.UI.HtmlControls.HtmlGenericControl Progressbar3 = DlistQuestions.Items[0].FindControl("Progressbar3") as System.Web.UI.HtmlControls.HtmlGenericControl;

                Progressbar.Attributes.Add("class", "progress-bar progress-bar-success");
                //Progressbar1.Attributes.Add("class", "progress-bar progress-bar-success");
                //Progressbar2.Attributes.Add("class", "progress-bar progress-bar-success");
                //Progressbar3.Attributes.Add("class", "progress-bar progress-bar-success");

                if (DtEmployeeAnswers != null && DtEmployeeAnswers.Rows.Count > 0)
                {
                    percentage = ((Convert.ToInt32(DtEmployeeAnswers.Rows[0]["TotalAnswers"].ToString()) * 100) / Convert.ToInt32(Session["TotalQuestionCount"].ToString()));

                }
                else
                {
                    //  percentage = ((Convert.ToInt32(Session["Position"]) * 100) / Convert.ToInt32(Session["TotalQuestionCount"].ToString()));
                    percentage = ((Convert.ToInt32(queCount * 100) / Convert.ToInt32(Session["TotalQuestionCount"].ToString())));

                }
                lblexamCount.Text = Convert.ToString(Session["TotalQuestionCount"].ToString());
                Progressbar.Attributes.Add("style", "text-align: center; width:" + percentage + "% ");
                //Progressbar.Attributes.Add("style", "text-align: center;");
                Progressbar.InnerText = percentage.ToString() + " % ";

                //Progressbar1.Attributes.Add("style", "width:" + percentage + "%");
                //Progressbar1.InnerText = percentage.ToString() + " % of " + Session["TotalQuestionCount"].ToString() + " Questions";

                //Progressbar2.Attributes.Add("style", "width:" + percentage + "%");
                //Progressbar2.InnerText = percentage.ToString() + " % of " + Session["TotalQuestionCount"].ToString() + " Questions";

                //Progressbar3.Attributes.Add("style", "width:" + percentage + "%");
                //Progressbar3.InnerText = percentage.ToString() + " % of " + Session["TotalQuestionCount"].ToString() + " Questions";

                if (percentage == 0)
                {
                    Progressbar.Attributes.Add("class", "progress-bar progress-bar-danger");
                    Progressbar.Attributes.Add("style", "width:100%");
                    Progressbar.InnerText = percentage.ToString() + " % of " + Session["TotalQuestionCount"].ToString() + " Questions";

                    //Progressbar1.Attributes.Add("class", "progress-bar progress-bar-danger");
                    //Progressbar1.Attributes.Add("style", "width:100%");
                    //Progressbar1.InnerText = percentage.ToString() + " % of " + Session["TotalQuestionCount"].ToString() + " Questions";

                    //Progressbar2.Attributes.Add("class", "progress-bar progress-bar-danger");
                    //Progressbar2.Attributes.Add("style", "width:100%");
                    //Progressbar2.InnerText = percentage.ToString() + " % of " + Session["TotalQuestionCount"].ToString() + " Questions";

                    //Progressbar3.Attributes.Add("class", "progress-bar progress-bar-danger");
                    //Progressbar3.Attributes.Add("style", "width:100%");
                    //Progressbar3.InnerText = percentage.ToString() + " % of " + Session["TotalQuestionCount"].ToString() + " Questions";

                    //lnkBtnJumpto.Visible = false;
                }
            }
            catch (Exception ex)
            {
                ex.InnerException.ToString();
                log.Error(ex.Message.ToString() + " " + ex.StackTrace.ToString());
            }
        }

        protected void DlistQuestions_ItemDataBound(object sender, DataListItemEventArgs e)
        {
            try
            {
                RadioButtonList rblistAnswers = (RadioButtonList)e.Item.FindControl("rblistAnswers");
                Label lblQuestionId = (Label)e.Item.FindControl("lblQuestionId");

                QuestionId = lblQuestionId.Text;
                DataTable DtAnswers = objAnswerMasterDAL.AnswerMasterGetWithDescriptionByQuestionId(QuestionId);
                //DataTable DtAnswers = objAnswerMasterDAL.AnswerMasterGetByQuestionId(QuestionId);
                rblistAnswers.DataSource = DtAnswers;
                rblistAnswers.DataTextField = "Answer";
                rblistAnswers.DataValueField = "AnswerId_PK";
                rblistAnswers.DataBind();
            }
            catch (Exception ex2)
            {
                ex2.InnerException.ToString();
                log.Error(ex2.Message.ToString() + " " + ex2.StackTrace.ToString());
            }
        }

        protected void Page_Changed(object sender, EventArgs e)
        {
            int pageIndex = int.Parse((sender as LinkButton).CommandArgument);

            int pagePosition = (pageIndex * 10) - 10;

            BindTestQuestions(pagePosition);
            BindSavedAnswers(pagePosition);
            //  this.GetCustomersPageWise(pageIndex);
        }

        //protected void rblistAnswers_SelectedIndexChanged(object sender, EventArgs e)
        //{

        //    //   string AnswerId = ((RadioButtonList)answerItem.FindControl("rblistAnswers")).SelectedItem.Value;
        //    // Session["Value"] = rblistAnswers.SelectedValue.ToString();

        //    try
        //    {
        //        if (Session["EmployeeId"] != null)
        //        {
        //            //save answers 
        //            int status = SaveAnswersSession();
        //            if (status > 0)
        //            {
        //                if (Session["Position"] != null)
        //                {
        //                    //if (((rptPager.Items.Count - 2) * 10) == Convert.ToInt32(Session["Position"].ToString()))
        //                    //{

        //                    //}
        //                    //else
        //                    //{

        //                    // Session["Position"] = Convert.ToInt32(Session["Position"].ToString()) + DlistQuestions.Items.Count;
        //                    //if (Convert.ToInt32(Session["TotalQuestionCount"].ToString()) > Convert.ToInt32(arrayqueCntList.Count.ToString()))
        //                    //{
        //                    int previousNo = Convert.ToInt32( Session["Position"]);
        //                        int queNumber = GetAlternateNumber(0, maxNum);
        //                        Session["Position"] = queNumber;
        //                    if(previousNo== queNumber)
        //                    {
        //                        arrayqueCntList.Add(arrayqueCntList.Count + 1);
        //                    }
        //                    //}
        //                    //}                          
        //                    //btnPrevious.Enabled = true;
        //                    // btnPrevious1.Enabled = true;
        //                }
        //                else
        //                {
        //                    Session["Position"] = DlistQuestions.Items.Count;
        //                }

        //                if (Convert.ToInt32(Session["Position"].ToString()) < Convert.ToInt32(Session["TotalQuestionCount"].ToString()))
        //                {
        //                    BindTestQuestions(Convert.ToInt32(Session["Position"].ToString()));
        //                    BindSavedAnswers(Convert.ToInt32(Session["Position"].ToString()));
        //                }

        //                int Count = Convert.ToInt32(arrayqueCntList.Count + DlistQuestions.Items.Count);
        //                //if (Convert.ToInt32(Session["TotalQuestionCount"].ToString()) == Convert.ToInt32(Session["Position"].ToString()))
        //                if (Convert.ToInt32(Session["TotalQuestionCount"].ToString())+1 == Convert.ToInt32(arrayqueCntList.Count))
        //                {
        //                    Session["TotalQuestionCount"] = null;
        //                    Session["Position"] = null;
        //                    //send mail 
        //                    EmployeeAssessmentsHistoryBE objEmployeeAssessmentsHistoryBE = new EmployeeAssessmentsHistoryBE();
        //                    EmployeeAssessmentsHistoryDAL objEmployeeAssessmentsHistoryDAL = new EmployeeAssessmentsHistoryDAL();

        //                    objEmployeeAssessmentsHistoryBE.propEmployeeId_FK = EmployeeId;
        //                    //objEmployeeAssessmentsHistoryBE.propAssessmentId_FK=
        //                    objEmployeeAssessmentsHistoryBE.propAssessmentSubmitted = "Y";
        //                    objEmployeeAssessmentsHistoryBE.propCreatedBy = EmployeeId;
        //                    objEmployeeAssessmentsHistoryBE.propCreatedDate = DateTime.Now.Date;
        //                    objEmployeeAssessmentsHistoryBE.propAssessmentInstanceId_FK = Session["AssessmentInstanceId"].ToString();
        //                    objEmployeeAssessmentsHistoryBE.propInstanceCompanyRelationId_FK = Session["AssessmentInstanceCompanyId"].ToString();

        //                    status = objEmployeeAssessmentsHistoryDAL.EmployeeAssessmentsHistoryInsert(objEmployeeAssessmentsHistoryBE);
        //                    // lblMessage.Text = "Assessment submitted successfully...";
        //                    lblMessageInfo.Text = "Assessment submitted successfully...";
        //                    ClientScript.RegisterStartupScript(this.GetType(), "Pop", "openModalQuestion();", true);

        //                    if (status > 0)
        //                    {
        //                        //Session["EmployeeId"]
        //                        queCount = 1;
        //                        CompanyDetailsDAL objCompanyDetailsDAL = new CompanyDetailsDAL();
        //                        DataTable DtCompany = objCompanyDetailsDAL.CompanyDetailsGetByEmployeeId(Session["EmployeeId"].ToString());
        //                        //CompanyName
        //                        SendEmailNewTemplate(DtCompany);


        //                        //message = "Employee added successfully...";
        //                        //divalert.Attributes.Add("class", "alert alert-success");
        //                        //lblMessage.Text = "Employee added successfully...";

        //                        //ask for another test
        //                        if (Session["BatchID"] != null && Session["BatchID"].ToString() != "")
        //                        {
        //                            string batchID = Session["BatchID"].ToString();
        //                            DataTable DtAssessmentSubmitted = objEmployeeAssessmentsHistoryDAL.EmployeeAssementsGet(EmployeeId, batchID);

        //                            if (DtAssessmentSubmitted != null && DtAssessmentSubmitted.Rows.Count > 0)
        //                            {
        //                                if (DtAssessmentSubmitted.Select("AssessmentSubmitted=0").Length > 0)
        //                                {
        //                                    if (Convert.ToString(Session["FlagTest"]) == "free")
        //                                    {
        //                                        Response.Redirect("SuggestionFrnd.aspx");
        //                                    }
        //                                    // Response.Redirect("AssessmentMessage.aspx");
        //                                    //  ScriptManager.RegisterClientScriptBlock(this, GetType(), "MessageBox", "DisplayColorBox('AssessmentMessage.aspx');", true);
        //                                }
        //                                else
        //                                {
        //                                    Response.Redirect("Feedback.aspx", false);
        //                                }
        //                            }
        //                        }
        //                    }
        //                    else
        //                    {
        //                        //lblmessage.Text = "Can not submit test...";
        //                        lblMessageInfo.Text = "Can not submit test...";
        //                        ClientScript.RegisterStartupScript(this.GetType(), "Pop", "openModalQuestion();", true);
        //                    }
        //                }
        //                else if (Convert.ToInt32(Session["TotalQuestionCount"].ToString()) == Count)
        //                {
        //                    //  btnNext.Text = "Submit";
        //                }
        //                if (Session["Position"] != null)
        //                {
        //                    //btnPrevious.Enabled = true;
        //                    //  btnPrevious1.Enabled = true;
        //                }
        //            }
        //        }
        //        else
        //        {
        //            Response.Redirect("EmployeeLogin.aspx", false);
        //        }

        //    }
        //    catch (Exception ex1)
        //    {
        //        ex1.InnerException.ToString();
        //        //Response.Redirect("EmployeeLogin.aspx",false );
        //        log.Error(ex1.Message.ToString() + " " + ex1.StackTrace.ToString());
        //    }
        //}

        protected void rblistAnswers_SelectedIndexChanged(object sender, EventArgs e)
        {
            //try
            //{
                if (Session["EmployeeId"] != null)
                {
                   
                    int status = SaveAnswersSession();
                    if (status > 0)
                    {
                        if(Session["Position"] != null && arrayqueCntList.Count< Convert.ToInt32(Session["TotalQuestionCount"].ToString()))
                        {
                            int queNumber = GetAlternateNumber(0, maxNum);
                            Session["Position"] = queNumber;

                            BindTestQuestions(Convert.ToInt32(Session["Position"].ToString()));
                            BindSavedAnswers(Convert.ToInt32(Session["Position"].ToString()));
                        }
                        else
                        {
                            Session["TotalQuestionCount"] = null;
                            Session["Position"] = null;

                            EmployeeAssessmentsHistoryBE objEmployeeAssessmentsHistoryBE = new EmployeeAssessmentsHistoryBE();
                            EmployeeAssessmentsHistoryDAL objEmployeeAssessmentsHistoryDAL = new EmployeeAssessmentsHistoryDAL();

                            objEmployeeAssessmentsHistoryBE.propEmployeeId_FK = EmployeeId;
                            objEmployeeAssessmentsHistoryBE.propAssessmentSubmitted = "Y";
                            objEmployeeAssessmentsHistoryBE.propCreatedBy = EmployeeId;
                            objEmployeeAssessmentsHistoryBE.propCreatedDate = DateTime.Now.Date;
                            objEmployeeAssessmentsHistoryBE.propAssessmentInstanceId_FK = Session["AssessmentInstanceId"].ToString();
                            objEmployeeAssessmentsHistoryBE.propInstanceCompanyRelationId_FK = Session["AssessmentInstanceCompanyId"].ToString();

                            status = objEmployeeAssessmentsHistoryDAL.EmployeeAssessmentsHistoryInsert(objEmployeeAssessmentsHistoryBE);
                            lblMessageInfo.Text = "Assessment submitted successfully...";
                            ClientScript.RegisterStartupScript(this.GetType(), "Pop", "openModalQuestion();", true);

                            if (status > 0)
                            {
                                queCount = 1;
                                CompanyDetailsDAL objCompanyDetailsDAL = new CompanyDetailsDAL();
                                DataTable DtCompany = objCompanyDetailsDAL.CompanyDetailsGetByEmployeeId(Session["EmployeeId"].ToString());
                                SendEmailNewTemplate(DtCompany);

                                if (Session["BatchID"] != null && Session["BatchID"].ToString() != "")
                                {
                                    string batchID = Session["BatchID"].ToString();
                                    DataTable DtAssessmentSubmitted = objEmployeeAssessmentsHistoryDAL.EmployeeAssementsGet(EmployeeId, batchID);

                                    if (DtAssessmentSubmitted != null && DtAssessmentSubmitted.Rows.Count > 0)
                                    {
                                        if (DtAssessmentSubmitted.Select("AssessmentSubmitted=0").Length > 0)
                                        {
                                            if (Convert.ToString(Session["FlagTest"]) == "free")
                                            {
                                                Response.Redirect("SuggestionFrnd.aspx", false);
                                            }
                                            else
                                            {
                                                Response.Redirect("Feedback.aspx", false);
                                            }
                                        }
                                        else
                                        {
                                            Response.Redirect("Feedback.aspx", false);
                                        }
                                    }
                                }
                            }
                            else
                            {
                                lblMessageInfo.Text = "Can not submit test...";
                                ClientScript.RegisterStartupScript(this.GetType(), "Pop", "openModalQuestion();", true);
                            }
                        }
                        
                    }
                }
                else
                {
                    Response.Redirect("EmployeeLogin.aspx", false);
                }

            //}
            //catch (Exception ex1)
            //{
            //    ex1.InnerException.ToString();
            //    log.Error(ex1.Message.ToString() + " " + ex1.StackTrace.ToString());
            //}
        }

        private void SendEmail(DataTable DtCompany)
        {
            System.Net.Mail.MailMessage mail = new System.Net.Mail.MailMessage();
            // System.Web.Mail.MailMessage mail = new System.Web.Mail.MailMessage();

            mail.To.Add("priyamvada.bavadekar@letmetalk2.me");
            mail.Bcc.Add("priyamvada.bavadekar@letmetalk2.me");
            // mail.From = "contact@letmetalk2.me";
            mail.From = new MailAddress("contact@letmetalk2.me", "Let me talk to me", System.Text.Encoding.UTF8);
            mail.Subject = "Assessment submitted by user";
            string getDt = DateTime.Now.Date.AddDays(7).ToString("MM/dd/yyyy");

            mail.Body = "<html><body><font color=\"black\">Hello Mam, <br/><br/> <b>" + Session["UserName"].ToString() + "</b> with EmployeeId " + Session["EmployeeCompanyId"].ToString() + " of company " + DtCompany.Rows[0]["CompanyName"].ToString() + " has submitted the assessment successfully. <br/><br/> Thank You.<BR/><br/> Best Regards, <br/><br/> Let me talk to me Team.</font></body></html>";
            mail.IsBodyHtml = true;

            mail.Priority = System.Net.Mail.MailPriority.High;
            SmtpClient client = new SmtpClient();
            //client.Credentials = new System.Net.NetworkCredential("contact@letmetalk2.me", "contact123");
            //client.Port = 587;
            //client.Host = "hazel.arvixe.com";
            client.EnableSsl = false;

            client.Send(mail);
        }
        private void SendEmailNewTemplate(DataTable DtCompany)
        {

            //string DefaultImagePath = HttpContext.Current.Server.MapPath(@"~\UIFolder\dist\img\logotemp.png");
            //byte[] imageArray = System.IO.File.ReadAllBytes(DefaultImagePath);
            //string base64ImageRepresentation = Convert.ToBase64String(imageArray);
            System.Net.Mail.MailMessage mail = new System.Net.Mail.MailMessage();
            // System.Web.Mail.MailMessage mail = new System.Web.Mail.MailMessage();

            mail.To.Add("priyamvada.bavadekar@letmetalk2.me");
            mail.Bcc.Add("priyamvada.bavadekar@letmetalk2.me");
            // mail.From = "contact@letmetalk2.me";
            mail.From = new MailAddress("contact@letmetalk2.me", "Let me talk to me", System.Text.Encoding.UTF8);
            mail.Subject = "Assessment submitted by user";
            string getDt = DateTime.Now.Date.AddDays(7).ToString("MM/dd/yyyy");
            StringBuilder body = new StringBuilder();
            body.AppendLine("<body><table width='600' border='0' align='center' cellpadding='0' cellspacing='0' style='border:2px solid #b3c8c8 !important'>");
            body.AppendLine("<tr><td><table width='100%' border='0' cellspacing='0' cellpadding='0'><tr>");
            body.AppendLine("<td valign='top'><table width='90%' border='0' align='center' cellpadding='0' cellspacing='0'><tr>");
            body.AppendLine("<td  style='font-family:Verdana, Arial, Helvetica, sans-serif; font-size:12px; line-height:1.5; text-align:justify;'>&nbsp;</td>");
            body.AppendLine("</tr><tr><td  style='font-family:Verdana, Arial, Helvetica, sans-serif;  font-size:12px; line-height:1.8;'><div style='font-size:18px; color:#5b6766; font-weight:600;'>Hello Mam, </div>");
            body.AppendLine("<div style='clear:both;'></div><br />  <span style='color:#829a9a; font-size:14px; margin-bottom:0; padding-bottom:0;'><strong style='font-size:16px;'>" + Session["UserName"].ToString() + "</strong>");
            body.AppendLine("with Employee Id <strong style='font-size:16px;'>" + Session["EmployeeCompanyId"].ToString() + "</strong> of company <strong style='font-size:16px;'>" + DtCompany.Rows[0]["CompanyName"].ToString() + "</strong>");
            body.AppendLine("<br /><strong style='font-size:16px; color:#009900;'> has submitted the assessment successfully.</strong> </span></td></tr><tr>");
            body.AppendLine("<td  style='font-family:Verdana, Arial, Helvetica, sans-serif; color:#829a9a; font-size:14px; margin-bottom:0; padding-bottom:0;'>&nbsp;</td>");
            body.AppendLine("</tr><tr><td  style='font-family:Verdana, Arial, Helvetica, sans-serif; color:#5b6766; font-size:14px; margin-bottom:0; padding-bottom:0;'>Thank You.");
            body.AppendLine("<br /><br />Best Regards,</td></tr><tr><td  style='font-family:Verdana, Arial, Helvetica, sans-serif; font-size:12px; line-height:1.5; text-align:justify;'>&nbsp;</td>");
            body.AppendLine("</tr></table></td></tr><tr><td height='140' bgcolor='#b3c8c8' style='font-family:Verdana, Arial, Helvetica, sans-serif; font-size:20px; color:#fff;'><table width='90%' border='0' align='center' cellpadding='0' cellspacing='0'>");
            body.AppendLine("<tr><td width='23%'><img src='" + ConfigurationManager.AppSettings.Get("ImagePath") + @"\UIFolder\dist\img\logotemp.png' width ='120' height='122' /><td width='2%'>&nbsp;</td>");
            body.AppendLine("<td width='75%' align='left'><table width='100%' border='0' align='left' cellpadding='0' cellspacing='0'><tr>");
            body.AppendLine("<td style='color:#5b756c'><strong>let me talk team</strong></td></tr><tr>");
            body.AppendLine("<td height='28' style='font-size:14px; border-bottom:1px dotted #5b756c; padding-bottom:5px; color:#5b756c'>self test program</td>");
            body.AppendLine("</tr><tr><td style='font-size:12px; padding-top:5px; color:#5b756c'>https://www.letmetalk2.me/</td></tr><tr>");
            body.AppendLine("<td height='20' style='font-size:12px; color:#5b756c'><span style='font-size:12px; padding-top:5px;'>©2020 let me talk. all rights reserved.</span></td>");
            body.AppendLine("</tr></table></td></tr></table></td></tr></tr></table></td></tr></table></body>");

            mail.Body = body.ToString();
            //mail.Body = "<html><body><font color=\"black\">Hello Mam, <br/><br/> <b>" + Session["UserName"].ToString() + "</b> with EmployeeId " + Session["EmployeeCompanyId"].ToString() + " of company " + DtCompany.Rows[0]["CompanyName"].ToString() + " has submitted the assessment successfully. <br/><br/> Thank You.<BR/><br/> Best Regards, <br/><br/> Let me talk to me Team.</font></body></html>";
            mail.IsBodyHtml = true;

            mail.Priority = System.Net.Mail.MailPriority.High;
            SmtpClient client = new SmtpClient();
            //client.Credentials = new System.Net.NetworkCredential("contact@letmetalk2.me", "contact123");
            //client.Port = 587;
            //client.Host = "hazel.arvixe.com";
            client.EnableSsl = false;

            client.Send(mail);
        }
        public void FocusControlOnPageLoad(string ClientID, System.Web.UI.Page page)
        {
            ClientScriptManager clientScript = this.Page.ClientScript;
            clientScript.RegisterClientScriptBlock(this.GetType(), "DlistQuestionsLayout2",

                           @"<script> 

          function ScrollView()

          {
             var el = document.getElementById('" + ClientID + @"')
             if (el != null)
             {        
                el.scrollIntoView();
                el.focus();
             }
          }

          window.onload = ScrollView;

          </script>");

        }

        public int GetAlternateNumber(int min, int max)
        {
            int randomNumber=0;
            var generator = new RandomGenerator();
            

            for (int i = 0; i < arrayqueCntList.Count; i++)
            {
                randomNumber = generator.RandomNumber(min, max);
                if (!arrayqueCntList.Contains(randomNumber))
                {
                    Session["Position"] = randomNumber;
                    break;
                }
                else if (arrayqueCntList.Contains(randomNumber))
                {
                    i = 0;
                }
            }
            arrayqueCntList.Add(randomNumber);
            return randomNumber;
        }

        protected void DlistQuestionsLayout2_ItemDataBound(object sender, DataListItemEventArgs e)
        {
            try
            {
                RadioButtonList rblistAnswers = (RadioButtonList)e.Item.FindControl("rblistAnswers2");
                Label lblQuestionId = (Label)e.Item.FindControl("lblQuestionId");

                QuestionId = lblQuestionId.Text;
                DataTable DtAnswers = objAnswerMasterDAL.AnswerMasterGetWithDescriptionByQuestionId(QuestionId);
                //DataTable DtAnswers = objAnswerMasterDAL.AnswerMasterGetByQuestionId(QuestionId);
                rblistAnswers.DataSource = DtAnswers;
                rblistAnswers.DataTextField = "Answer";
                rblistAnswers.DataValueField = "AnswerId_PK";
                rblistAnswers.DataBind();
            }
            catch (Exception ex2)
            {
                ex2.InnerException.ToString();
                log.Error(ex2.Message.ToString() + " " + ex2.StackTrace.ToString());
            }
        }

        public void ShowAlternateQuestionLayout()
        {

            try
            {
                //queLayout2.Visible = false;
                //queLayout2.Style["display"] = "none";

                string layout = queList[queLayoutIndex].ToString();
                for (int j = 0; j < queList.Count; j++)
                {
                    if (queLayoutIndex == j)
                    {
                        System.Web.UI.HtmlControls.HtmlGenericControl div = DlistQuestions.Items[0].FindControl(layout) as System.Web.UI.HtmlControls.HtmlGenericControl;
                        div.Visible = true;
                    }
                    else
                    {
                        string layout1 = queList[j].ToString();
                        System.Web.UI.HtmlControls.HtmlGenericControl div1 = DlistQuestions.Items[0].FindControl(layout1) as System.Web.UI.HtmlControls.HtmlGenericControl;
                        div1.Visible = false;
                    }
                }
                if (queLayoutIndex == 3)
                {
                    queLayoutIndex = 0;
                }
                else
                {
                    queLayoutIndex++;
                }
                //if (CurrentLayout == "")
                //{
                //    CurrentLayout = layout;
                //    System.Web.UI.HtmlControls.HtmlGenericControl div = DlistQuestions.Items[0].FindControl(layout) as System.Web.UI.HtmlControls.HtmlGenericControl;
                //    div.Visible = false;
                //    break;
                //}
                //else if(CurrentLayout !=layout)
                //{
                //    CurrentLayout = layout;
                //    System.Web.UI.HtmlControls.HtmlGenericControl div = DlistQuestions.Items[0].FindControl(layout) as System.Web.UI.HtmlControls.HtmlGenericControl;
                //    div.Visible = false;
                //    break;
                //}
                //else
                //{
                //    i = 0;
                //}

            }
            catch (Exception ex)
            {

            }

        }

    }
    public class RandomGenerator
    {
        // Instantiate random number generator.  
        // It is better to keep a single Random instance 
        // and keep using Next on the same instance.  
        private readonly Random _random = new Random();
        // Generates a random number within a range.      
        public int RandomNumber(int min, int max)
        {
            return _random.Next(min, max);
        }
    }

}