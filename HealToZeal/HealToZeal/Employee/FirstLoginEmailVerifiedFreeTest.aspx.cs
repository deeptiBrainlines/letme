﻿using BE;
using DAL;
using System;
using System.Configuration;
using System.Data;
using System.Net.Mail;

namespace HealToZeal.Employee
{
    public partial class FirstLoginEmailVarifiedFreeTest : System.Web.UI.Page
    {
        log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if ((Session["EmployeeID"] != null && Session["EmployeeID"].ToString() != null && Session["CompanyId"] != null))
                {
                    try
                    {
                        EmployeeDAL objEmployeeDAL = new EmployeeDAL();
                        EmployeeBE objEmployeeBE = new EmployeeBE();
                        objEmployeeBE.propCompanyId_FK = Session["CompanyId"].ToString();
                        //objEmployeeBE.verificationcode = Session["verificationcode"].ToString();
                        objEmployeeBE.propEmployeeId_PK = Session["EmployeeID"].ToString();
                        DataTable dt1 = objEmployeeDAL.GetEmployeePrice(objEmployeeBE);

                        if (dt1 != null && dt1.Rows.Count > 0)
                        {
                            //lblprice.Text = dt1.Rows[0]["CouponAmount"].ToString();
                            //lblname.Text = dt1.Rows[0]["CouponName"].ToString();
                            //lblEmailid.Text = dt1.Rows[0]["EmailId"].ToString();
                            lblFirstname.Text = dt1.Rows[0]["FirstName"].ToString();
                            lblLastname.Text = dt1.Rows[0]["LastName"].ToString();
                            //lblpercentage.Text = dt1.Rows[0]["CouponDiscountPercentage"].ToString();
                        }
                    }
                    catch (Exception ex)
                    {

                        log.Error(ex.Message.ToString() + " " + ex.StackTrace.ToString());
                    }

                }
                else
                {

                }
            }
        }

        protected void paymentgateway_Click(object sender, EventArgs e)
        {

            //getEmployees();
            // ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('Please check Email for Assessment Login)", true);

            //var LoginURl = ConfigurationManager.AppSettings.Get("Loginurl");
            //EmployeeDAL objEmployeeDAL = new EmployeeDAL();
            //EmployeeBE objEmployeeBE = new EmployeeBE();

            //objEmployeeBE.propEmployeeId_PK = Session["EmployeeID"].ToString();
            //objEmployeeBE.propEmployeeId_PK = Convert.ToString(Session["EmployeeID"].ToString());
            //objEmployeeBE.PaymentFlag = "1";

            //int UpdatePaidTestFlag = objEmployeeDAL.UpdatePaidTestFlag(objEmployeeBE);
            //if (UpdatePaidTestFlag >= 0)
            //{

            //    DataTable dt = objEmployeeDAL.GetEmployeebyEmpid(objEmployeeBE);
            //    if (dt != null && dt.Rows.Count > 0)
            //    {
            //        string FirstName = dt.Rows[0]["FirstName"].ToString();
            //        string LastName = dt.Rows[0]["LastName"].ToString();
            //        string Email = dt.Rows[0]["EmailId"].ToString();
            //        string Password1 = dt.Rows[0]["Password"].ToString();
            //        String Password = Encryption.Decrypt(Password1);
            //        string UserName = dt.Rows[0]["UserName"].ToString();
            //        Session["CompanyId"] = dt.Rows[0]["CompanyID_FK"].ToString();
            //        Session["FlagTest"] = "Paid";
            //        System.Net.Mail.MailMessage mail = new System.Net.Mail.MailMessage();


            //        mail.To.Add(Email);
            //        mail.CC.Add("contact@letmetalk2.me");
            //        mail.Bcc.Add("priyamvada.bavadekar@letmetalk2.me");
            //        // mail.From = "contact@letmetalk2.me";
            //        // mail.From = new MailAddress("contact@letmetalk2.me", "Let me talk to me", System.Text.Encoding.UTF8);
            //        mail.Subject = "Welcome to the Health and Personality Insights and Actions Tool.";
            //        string getDt = DateTime.Now.Date.AddDays(7).ToString("MM/dd/yyyy");

            //        //mail.Body = "<html><body><font color=\"black\">Dear " + FirstName + " " + LastName + ",<br/><br/>Welcome to <b> Let Me Talk2 Me: A tool to quickly and discreetly assess your mindfulness.</b> You will be able to gain some useful insights into your own personality and learn about remedial actions in case they are needed. <b>'Let Me Talk2 Me'</b> will help you in self-discovery. It will lead on a path of self reflections while avoiding pitfalls of modern complex life and careers. <br/><br/>It is recommended that you use this tool periodically to monitor yourself and be aware of your health and personality parameters. <br/><br/> Please use the link below to login <br/><br/><a href='http://letmetalk2.me.hazel.arvixe.com/Employee/EmployeeLogin.aspx?first=1'> http://letmetalk2.me.hazel.arvixe.com/Employee/EmployeeLogin.aspx </a><br/>Your login details are as follows:<br/>UserId:<b>" + UserName + "</b><br/>Password:<b>" + Password + "</b><br/><br/>If you would like to take a break during assessment, please make sure you log out from the system or close the browser window for security purposes. Please do not leave the browser idle. You can always log back in whenever you like.<br/><br/>Please ensure that you complete this assessment on or before " + getDt + ". To get a maximum benefit, please repeat this exercise every 3 months and study the differentials.<br/> You will be notified for the same when the next assessment is due.<br/><br/>If you wish to join the ecosystem of mindfulness, please pre-register with us.<a href='http://letmetalk2.me/pre-register'>http://letmetalk2.me/pre-register</a></br>Thank You.<br/><br/></font></body></html>";

            //        mail.Body = "<html><body><font color=\"black\">Dear " + FirstName + " " + LastName + ",<br/><br/>Welcome to <b> Let Me Talk2 Me: A tool to quickly and discreetly assess your mindfulness.</b> You will be able to gain some useful insights into your own personality and learn about remedial actions in case they are needed. <b>'Let Me Talk2 Me'</b> will help you in self-discovery. It will lead on a path of self reflections while avoiding pitfalls of modern complex life and careers. <br/><br/>It is recommended that you use this tool periodically to monitor yourself and be aware of your health and personality parameters. <br/><br/> Please use the link below to login <br/><br/><a href='" + LoginURl + "'> " + LoginURl + " </a><br/>Your login details are as follows:<br/>UserId:<b>" + UserName + "</b><br/>Password:<b>" + Password + "</b><br/><br/>If you would like to take a break during assessment, please make sure you log out from the system or close the browser window for security purposes. Please do not leave the browser idle. You can always log back in whenever you like.<br/><br/>Please ensure that you complete this assessment on or before " + getDt + ". To get a maximum benefit, please repeat this exercise every 3 months and study the differentials.<br/> You will be notified for the same when the next assessment is due.<br/><br/>If you wish to join the ecosystem of mindfulness, please pre-register with us.<a href='http://letmetalk2.me/pre-register'>http://letmetalk2.me/pre-register</a></br>Thank You.<br/><br/></font></body></html>";

            //        mail.IsBodyHtml = true;

            //        mail.Priority = System.Net.Mail.MailPriority.High;
            //        SmtpClient client = new SmtpClient();
            //        // client.Credentials = new System.Net.NetworkCredential("contact@letmetalk2.me", "contact123");
            //        //  client.Port = 587;
            //        // client.Host = "dallas137.arvixeshared.com";
            //        client.EnableSsl = false;
            //        try
            //        {
            //            client.Send(mail);
            //        }
            //        catch (Exception ex)
            //        {
            //            log.Error(ex.Message.ToString() + " " + ex.StackTrace.ToString());
            //            lblMessage1.Text = ex.Message.ToString();
            //            ClientScript.RegisterStartupScript(this.GetType(), "Pop", "openModal3();", true);

            //        }
            //        lblMessage1.Text = "Please check Email for Assessment Login";
            //        ClientScript.RegisterStartupScript(this.GetType(), "Pop", "openModal3();", true);

            //    }
            //}
            ////string message = "Please check Email for Assessment Login";
            ////string script = "window.onload = function(){ alert('";
            ////script += message;
            ////script += "')};";
            ////ClientScript.RegisterStartupScript(this.GetType(), "SuccessMessage", script, true);
           Response.Redirect("PaymentGateway.aspx");
        }

        /// <summary>[Priyanka Raut] 18-Aug-2020 As per anuradha mam requirement write method for on freetest click event pass username redirect to login window</summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs" /> instance containing the event data.</param>
        protected void freetest_Click(object sender, EventArgs e)
        {
            EmployeeDAL objEmployeeDAL = new EmployeeDAL();
            EmployeeBE objEmployeeBE = new EmployeeBE();
            if (Session["EmployeeID"] != null)
            {
                try
                {
                    objEmployeeBE.propEmployeeId_PK = Convert.ToString(Session["EmployeeID"].ToString());
                    objEmployeeBE.propTestFlag = Convert.ToString("free");

                    //int UpdateTestFlag = objEmployeeDAL.UpdateTestFlag(objEmployeeBE);
                    //if (UpdateTestFlag >= 0)
                    //{
                        DataTable dt = objEmployeeDAL.GetEmployeebyFreeTestEmpid(objEmployeeBE);
                        //if (dt != null && dt.Rows.Count > 0)
                        //{
                        //    Session["FlagTest"] = "free";
                        //    string AssessmentInstanceId_FreeTest = ConfigurationManager.AppSettings.Get("AssessmentInstanceId_FreeTest");
                        //    for (int j = 0; j < dt.Rows.Count; j++)
                        //    {
                        //        if (Convert.ToString(Session["FlagTest"]) != "free" && Convert.ToString(dt.Rows[j]["AssessmentInstanceId_FK"]).ToUpper() == AssessmentInstanceId_FreeTest.ToString())
                        //        {
                        //            dt.Rows.Remove(dt.Rows[j]);
                        //        }
                        //        if (Convert.ToString(Session["FlagTest"]) == "free" && Convert.ToString(dt.Rows[j]["AssessmentInstanceId_FK"]).ToUpper() != AssessmentInstanceId_FreeTest.ToString())
                        //        {
                        //            dt.Rows.Remove(dt.Rows[j]);
                        //        }

                        //        //if (Convert.ToString(dt.Rows[j]["AssessmentInstanceId_FK"]).ToUpper() !=AssessmentInstanceId_FreeTest.ToString())
                        //        //{
                        //        //    dt.Rows.Remove(dt.Rows[j]);
                        //        //}
                        //    }
                        //}
                        if (dt != null && dt.Rows.Count > 0)
                        {
                            string FirstName = dt.Rows[0]["FirstName"].ToString();
                            string LastName = dt.Rows[0]["LastName"].ToString();
                            string Email = dt.Rows[0]["EmailId"].ToString();
                            string Password1 = dt.Rows[0]["Password"].ToString();
                            String Password = Encryption.Decrypt(Password1);
                            string UserName = dt.Rows[0]["UserName"].ToString();
                           // Session["CompanyId"] = dt.Rows[0]["CompanyID_FK"].ToString();
                            Session["UserName"] = dt.Rows[0]["UserName"].ToString();
                            Session["Password"] = Password;
                            Session["FlagTest"] = "free";
                            Session["EmployeeId"] = dt.Rows[0]["EmployeeId_PK"].ToString();
                            Session["EmployeeCompanyId"] = dt.Rows[0]["EmployeeCompanyId"].ToString();
                            Session["BatchID"] = dt.Rows[0]["BatchID_Fk"].ToString();
                            Session["AssessmentInstanceId"] = dt.Rows[0]["AssessmentInstanceId_FK"].ToString();
                            Session["AssessmentInstanceCompanyId"] = dt.Rows[0]["InstanceCompanyRelationId"].ToString();
                            Session["CompanyId"] = dt.Rows[0]["FreeTestCompanyId"].ToString();
                            Session["AssessmentId"] = dt.Rows[0]["AssessmentId_FK"].ToString();
                            Response.Redirect("EmployeeMessage.aspx", false);
                        }
                   // }
                    //else
                    //{
                    //    Response.Redirect("FirstLoginEmailVarifiedFreeTest.aspx", false);
                    //}
                }
                catch (Exception ex)
                {

                    log.Error(ex.ToString() + " " + ex.StackTrace.ToString());
                }

            }
            else
            {
                Response.Redirect("EmployeeLogin.aspx");
            }
        }

        protected void btn_Click(object sender, EventArgs e)
        {
            Response.Redirect("PaymentGateway.aspx");
        }
    }
}