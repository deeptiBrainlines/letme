﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace HealToZeal.Employee
{
    public partial class AssessmentMessage : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                if (Session["EmployeeId"] != null)
                {

                }
                else
                {
                    Response.Redirect("EmployeeLogin.aspx", false);
                }
            }
        }

        protected void btnNext_Click(object sender, EventArgs e)
        {
            try
            {
                Response.Redirect("EmployeeAssessments.aspx",false);
            }
            catch 
            {
            }
        }

        protected void btnFeedback_Click(object sender, EventArgs e)
        {
            try
            {
                Response.Redirect("EmployeeFeedback.aspx",false);
            }
            catch 
            {
                
            }
        }

    }
}