﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="EmployeeMessageold.aspx.cs" Inherits="HealToZeal.Employee.EmployeeMessage" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="keywords" content="Health Care, health" />
    <meta name="description" content="Health Care HTML Template" />
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <!-- favicon links -->
    <link rel="shortcut icon" type="image/ico" href="../favicon.ico" />
    <link rel="icon" type="image/ico" href="../favicon.ico" />
    <!--Google web fonts-->
    <link href='http://fonts.googleapis.com/css?family=Raleway' rel='stylesheet' type='text/css' />
    <!-- main css -->
    <link rel="stylesheet" href="../css/main.css" media="screen" />
    <%--<link rel="stylesheet" id="theme-color" type="text/css" href="#"/>--%>
    <!--page title-->
    <title>Let Me Talk2 Me</title>

</head>
<!--
**********************************************************************************************************
    Copyright (c) 2014 Himanshu Softtech.
********************************************************************************************************** -->
<body>

    <!--Pre loader start-->
    <div id="preloader">
        <div id="status">
            <img src="../images/loader.gif" id="preloader_image" width="36" height="36" alt="loading image" />
        </div>
    </div>
    <!--pre loader end-->
    <!-- color picker start -->

    <!-- color picker end -->
    <!--header start-->
    <header id="hs_header">
  <div class="container">
    <div class="row">
      <div class="col-lg-12 col-md-12 col-sm-12 clearfix">
        <div class="col-lg-2 col-md-2 col-sm-12">      

          <div id="hs_logo" >
           <%--    <a href="#"> <img src="../images/logo.png" alt="" /> </a> --%>
              <a href="http://www.letmetalk2.me/"><img alt="letmetalktome" src="../images/logo1.png"/></a>

           <a style="font-family:  Calibri;color:white; font-size:large; font-weight:bold;  text-transform:uppercase"> LetMeTalk2me
           </a>

          </div>
          <!-- #logo --> 
        </div>
        <div class="col-lg-8 col-md-8 col-sm-12">
          <button type="button" class="hs_nav_toggle navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">Menu<i class="fa fa-bars"></i></button>
     
        </div>
        <div class="col-lg-2 col-md-2 col-sm-4">
       
          <div class="hs_search_box">
          
<%--            <form class="form-inline" role="form">--%>
              <div class="form-group has-success has-feedback">
                <input type="text" class="form-control" id="inputSuccess4" placeholder="Search">
                <span class="glyphicon glyphicon-search form-control-feedback"></span> </div>
          <%--  </form>--%>
          </div>
           <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
        </button>
       
          <!-- #logo --> 
        </div>
      </div>
      <!-- .col-md-12 --> 
    </div>
    <!-- .row --> 
  </div>
  <!-- .container --> 
  
</header>
    <!--header end-->
    <!--slider start-->
    <h4></h4>
    <form runat="server">
         <%-- <table>  
                    <tr>  
                        <td>  
                            <asp:Image ID="imgprofile" runat="server" Height="100px" Width="100px" /> </td>  
                    </tr>  
                    <tr>  
                        <td> Id </td>  
                        <td>  
                            <asp:Label ID="lblid" runat="server" Text=""></asp:Label>  
                        </td>  
                    </tr>  
                    <tr>  
                        <td> Name </td>  
                        <td>  
                            <asp:Label ID="lblname" runat="server" Text=""></asp:Label>  
                        </td>  
                    </tr>  
                    <tr>  
                        <td> Gender </td>  
                        <td>  
                            <asp:Label ID="lblgender" runat="server" Text=""></asp:Label>  
                        </td>  
                    </tr>  
                    <tr>  
                        <td> locale </td>  
                        <td>  
                            <asp:Label ID="lbllocale" runat="server" Text=""></asp:Label>  
                        </td>  
                    </tr>  
                    <tr>  
                        <td> link </td>  
                        <td>  
                            <asp:HyperLink ID="hylprofile" runat="server">Profile link</asp:HyperLink>  
                        </td>  
                    </tr>  
                </table>  --%>
        <div class="container">
            <div class="row">
                <div class="alert alert-dismissable">
                    <br />
                    <br />
                    <p>
                        <strong>Welcome to <b>Let Me Talk2 Me: A Health and Personality Insights and Actions Tool.</b> You will be able to gain some useful insights into your own personality and learn about remedial actions in case they are needed. <b>'Let Me Talk2 Me'</b>  will help you in self-discovery. It will lead on a path of self development while avoiding pitfalls of modern complex life and careers.
                        </strong>
                    </p>
                    <p>
                        You are being asked to fill in some information about yourself.  Your personal information will not be shared with anyone else.
                    </p>
                    <p>
                        <br />
                        <br />
                        Once you fill in this small form, you will be presented with a series of questions and answer options and you will be required to <u>answer all of them</u>.  You might find some repetition of the questions but it is deliberate and needed to get the right results out of this important exercise.
                    </p>
                    <p>
                        <br />
                        <br />
                        If you like, you can log out at any time and come back to answer remaining the questions. But your assessment will not be marked “submitted”, until you complete the same. So please make sure you complete the assessment and click the “Submit” button. There would be a small feedback form at the end and that’s it! Your <b>Mindfulness Profile</b> will be displayed as soon as you complete the test. You can always log back in to view the same at leisure anytime later.
                    </p>
                    <p>
                        <br />
                        <br />
                        So here is your journey of self discovery!! Hope you find it very very useful.
                    </p>
                    <p>
                        <strong>Thank you very much!</strong>
                    </p>

                </div>
            </div>
            <p>
                <asp:Button ID="btnNext" runat="server" class="btn btn-success pull-right btn-lg" Text="Next" OnClick="btnNext_Click" />
            </p>
            <div class="hs_margin_60"></div>
        </div>

    </form>
    <!--layer slider ends-->
    <!--slider end-->
    <footer id="hs_footer">
  <div class="container">
    <div class="hs_footer_content">
   
      <div class="row">
        <div class="col-lg-8 col-md-8 col-sm-8">
          <div class="row">
            <div class="hs_footer_about_us">
              <div class="col-lg-3 col-md-4 col-sm-12 col-md-12 col-sm-12">
            
<%--                <a href="../index.html"><img src="../images/logo.png" alt="logo" width="180" height="41" /></a>--%>
                    <a><img src="../images/logo1.png"  />   
               </a>
           <a style="font-family:  Calibri;color:white; font-size:large; font-weight:bold;  text-transform:uppercase"> LetMeTalk2Me
           </a>
              </div>
            
            </div>
          </div>

        </div>
  
      </div>
    </div>
  </div>
</footer>
    <div class="hs_copyright">
        Copyright © 2018 Clairvoyance Mindware Private Limited
    </div>

    <!--main js file start-->
    <script type="text/javascript" src="../js/jquery-1.11.0.min.js"></script>
    <script type="text/javascript" src="../js/bootstrap.js"></script>
    <script type="text/javascript" src="../js/owl.carousel.js"></script>
    <script type="text/javascript" src="../js/jquery.bxslider.js"></script>
    <script type="text/javascript" src="../js/jquery.mixitup.min.js"></script>
    <script type="text/javascript" src="../js/smoothscroll.js"></script>
    <script type="text/javascript" src="../js/single-0.1.0.js"></script>
    <script type="text/javascript" src="../js/custom.js"></script>
    <!--main js file end-->
</body>
</html>


