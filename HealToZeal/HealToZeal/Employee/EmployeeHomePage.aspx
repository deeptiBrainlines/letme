﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="EmployeeHomePage.aspx.cs" Inherits="HealToZeal.Employee.EmployeeHomePage" %>

<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>Let me talk</title>
<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
<!-- Bootstrap 3.3.5 -->
<link rel="stylesheet" href="../UIFolder/bootstrap/css/bootstrap.min.css">
<!-- Font Awesome -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
<link href="https://fonts.googleapis.com/css2?family=Ubuntu:ital,wght@0,300;0,400;0,500;0,700;1,300;1,400;1,500;1,700&display=swap" rel="stylesheet">
<!-- Ionicons -->
<link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
<!-- Custom CSS -->
<link rel="stylesheet" href="../UIFolder/dist/css/skin-bluegrey.css">
<link rel="stylesheet" href="../UIFolder/dist/css/custom.css">
<!-- Theme style -->
<link rel="stylesheet" href="../UIFolder/dist/css/AdminLTE.min.css">
<!-- Time picker -->
<link rel="stylesheet" href="../UIFolder/plugins/timepicker/bootstrap-timepicker.min.css">
<style>
.content{background:#FFFFFF;}
@media screen and (min-width: 320px) and (max-width: 980px) {
.layout-boxed{background:#FFFFFF !important;}
}
</style>
</head>
<body class="hold-transition layout-boxed sidebar-mini">
<div class="wrapper">
  <div style="clear:both;"></div>
  <section class="content">
    <div class="row">
      <div class="col-md-11 col-centered">
      <div class="col-md-12 col-centered" style="margin-bottom:60px;">
      <div class="col-md-7"></div>
      <div class="col-md-5"  style="padding:40px 0 0 0;">
      <div class="col-md-12"><div class="btn-center"><a href="" class="btns-outline mr10">Sign Up</a><a href="" class="btns-outline">Sign In</a></div></div></div>
      <div>
        <div class="col-md-7"><center><img src="../UIFolder/dist/img/home-img.png" class="img-responsive"></center></div>
        <div class="col-md-5"><div class="col-md-10 pad0"><p class="mt-text">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Lorem ipsum dolor sit amet.</p>
        <a href="" class="homebtns">take a free test</a>
        </div></div>
        <div style="clear:both;"></div>
      </div></div>
      <div style="clear:both;"></div>
      </div>
    </div>
    <footer class="main-footer" style=" background:#FFFFFF !important;">
      <center>
        COPYRIGHT © 2018 CLAIRVOYANCE MINDWARE PRIVATE LIMITED
      </center>
    </footer>
  </section>
</div>
<!-- jQuery 2.1.4 -->
<script src="../UIFolder/plugins/jQuery/jQuery-2.1.4.min.js"></script>
<!-- Bootstrap 3.3.5 -->
<script src="../UIFolder/bootstrap/js/bootstrap.min.js"></script>
<!-- Mutiple date picker -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/js/bootstrap-datepicker.js"></script>
<script>
        $('.date').datepicker({
  /*multidate: true,*/
	format: 'dd-mm-yyyy'
});

        </script>
<!-- SlimScroll -->
<script src="../UIFolder/plugins/slimScroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="../UIFolder/plugins/fastclick/fastclick.min.js"></script>
<!-- AdminLTE App -->
<script src="../UIFolder/dist/js/app.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="../UIFolder/dist/js/demo.js"></script>
<script>
$(document).ready(function () {
          if (!$.browser.webkit) {
              $('.wrapper').html('<p>Sorry! Non webkit users. :(</p>');
          }
      });
</script>
</body>
</html>
