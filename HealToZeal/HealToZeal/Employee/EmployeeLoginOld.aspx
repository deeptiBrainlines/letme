﻿<%@ Page Language="C#" AutoEventWireup="true" Async="true" CodeBehind="EmployeeLoginOld.aspx.cs" Inherits="HealToZeal.Employee.EmployeeLogin" %>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="keywords" content="Health Care, health" />
    <meta name="description" content="Health Care HTML Template" />
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <!-- favicon links -->
    <link rel="shortcut icon" type="image/ico" href="../favicon.ico" />
    <link rel="icon" type="image/ico" href="../favicon.ico" />
    <!--Google web fonts-->
    <link href='http://fonts.googleapis.com/css?family=Raleway' rel='stylesheet' type='text/css' />
    <!-- main css -->
    <link rel="stylesheet" href="../css/main.css" media="screen" />
    <%--<link rel="stylesheet" id="theme-color" type="text/css" href="#"/>--%>
    <!--page title-->
    <title>LetMeTalk2me</title>

</head>
<!--
**********************************************************************************************************
    Copyright (c) 2014 Himanshu Softtech.
********************************************************************************************************** -->
<body>
    <!--Pre loader start-->
    <div id="preloader">
        <div id="status">
            <img src="../images/loader.gif" id="preloader_image" width="36" height="36" alt="loading image" />
        </div>
    </div>
    <!--pre loader end-->
    <!-- color picker start -->
    <div id="style-switcher" class="hs_color_set">
        <div>
            <h3>color options</h3>
            <ul class="colors">
                <li>
                    <p class='colorchange' id='color'>
                    </p>
                </li>
                <li>
                    <p class='colorchange' id='color2'>
                    </p>
                </li>
                <li>
                    <p class='colorchange' id='color3'>
                    </p>
                </li>
                <li>
                    <p class='colorchange' id='color4'>
                    </p>
                </li>
                <li>
                    <p class='colorchange' id='color5'>
                    </p>
                </li>
                <li>
                    <p class='colorchange' id='style'>
                    </p>
                </li>
            </ul>
            <h3>Theme Option</h3>
            <select class="rock_themeoption" onchange="location = this.options[this.selectedIndex].value;">
                <option>Select Option</option>
                <option value="one-page.html">Single Page</option>
                <option value="index.html">Multi Pages</option>
            </select>
            <p>
            </p>
        </div>
        <div class="bottom">
            <a href="" class="settings"><i class="fa fa-gear"></i></a>
        </div>
    </div>
    <!-- color picker end -->
    <!--header start-->
    <header id="hs_header">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 clearfix">
                    <div class="col-lg-2 col-md-2 col-sm-12">
                        <div id="hs_logo">
                           <a href="http://www.letmetalk2.me/"><img alt="letmetalktome" src="../images/logo1.png"/></a>

                            <a style="font-family: Calibri; color: white; font-size: large; font-weight: bold; text-transform: uppercase">LetMeTalk2Me
                            </a>
                        </div>
                        <!-- #logo -->
                    </div>

                    <div class="col-lg-8 col-md-8 col-sm-12">
                            <button type="button" class="hs_nav_toggle navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">Menu<i class="fa fa-bars"></i></button>
                            <nav>
                                <ul class="hs_menu collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                                    <li><a class="active" href="EmployeeHome.aspx">Home</a>                                       
                                    </li>
                                    
                                   <%-- <li><a class="active" href="ViewEmployee.aspx">EmployeeMindfulnesss</a>
                                        <ul>
                                            <li><a href="ViewEmployee.aspx">Search </a></li>
                                            <li><a href="AddEmployee.aspx">Add</a></li>
                                        </ul>
                                    </li>                               

                                    <li><a class="active" href="AssessmentResults.aspx">SummaryMindfulnesss</a></li>    
                                    <li><a class="active" href="NewCadre.aspx">Cadre</a></li>  --%>                                        
                                    <%--<li><a href="#">About us</a>
                                    <ul>
                                            <li><a href="AboutUs.aspx">About us </a></li>
                                            <li><a href="OurTeam.aspx">Team</a></li>
                                        </ul>
                                        </li>
                                    <li><a href="ContactUs.aspx">Contact</a></li>--%>
                                  <%--  <li><a href="EmployeeRssFeeds.aspx">Rss Feeds</a></li>--%>
                                </ul>
                            </nav>
                        </div>



                   <%-- <div class="col-lg-8 col-md-8 col-sm-12">
                        <button type="button" class="hs_nav_toggle navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">Menu<i class="fa fa-bars"></i></button>--%>
                        <%--   <nav>
            <ul class="hs_menu collapse navbar-collapse" id="bs-example-navbar-collapse-1">
              <li><a class="active" href="CompanyHome.aspx">Home</a></li>
              <li><a class="active" href="AddEmployee.aspx">Employee</a></li>         
              <li><a href="../profile.html">our profile</a></li>
              <li><a href="../blog-categories.html">our blog</a></li>
              <li><a href="../contact.html">Contact</a></li>
            </ul>
          </nav>--%>
                 <%--   </div>--%>
                    <div class="col-lg-2 col-md-2 col-sm-4">
                        <%--<div class="hs_social">
            <ul>
              <li><a href=""><i class="fa fa-facebook"></i></a></li>
              <li><a href=""><i class="fa fa-twitter"></i></a></li>
              <li><a href=""><i class="fa fa-google-plus"></i></a></li>
              <li><a href="" id="hs_search"><i class="fa fa-search"></i></a></li>
            </ul>
          </div>--%>
                        <div class="hs_search_box">

                            <form class="form-inline" role="form">
                                <div class="form-group has-success has-feedback">
                                    <input type="text" class="form-control" id="inputSuccess4" placeholder="Search">
                                    <span class="glyphicon glyphicon-search form-control-feedback"></span>
                                </div>
                            </form>
                        </div>
                    <%--    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>--%>

                        <!-- #logo -->
                    </div>
                </div>
                <!-- .col-md-12 -->
            </div>
            <!-- .row -->
        </div>
        <!-- .container -->

    </header>
    <!--header end-->
    <!--slider start-->
    <div id="carousel-example-generic" class="carousel slide carousel-fade" data-ride="carousel">
        <!-- Indicators -->
        <%--   <ol class="carousel-indicators">
            <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
            <li data-target="#carousel-example-generic" data-slide-to="1"></li>
        </ol>--%>
        <!-- Wrapper for slides -->
        <%-- <div class="carousel-inner">
    <div class="item active"> <img class="animated fadeInDown" src="http://placehold.it/1366x550" alt="..." />
      <div class="carousel-caption">
        <h1 class="hs_slider_title animated bounceInDown">Meet Our Doctor’s Team.</h1>
        <p class="lead animated pulse">Suspendisse eu sem tortor Etiam rhoncus viverra mi ac tempor turpis rutrum in. 
          Vivamus porttitor rhoncus tellus, id ultricies eros gravida ut.</p>
        <a href="#hs_meat_doc" class="btn btn-default hs_slider_button animated fadeInLeftBig">More Info</a> <a href="" class="btn btn-success animated fadeInRightBig">Download</a> </div>
    </div>
    <div class="item"> <img class="animated fadeInDown" src="http://placehold.it/1366x550" alt="..." />
      <div class="carousel-caption">
        <h1 class="hs_slider_title animated bounceInDown">Book your Appointment</h1>
        <p class="lead animated pulse">Suspendisse eu sem tortor Etiam rhoncus viverra mi ac tempor turpis rutrum in. 
          Vivamus porttitor rhoncus tellus, id ultricies eros gravida ut.</p>
        <a href="#hs_appointment_form_link" class="btn btn-default hs_slider_button animated fadeInLeftBig">More Info</a> <a href="" class="btn btn-success animated fadeInRightBig">Download</a> </div>
    </div>
  </div>--%>
    </div>
    <!--layer slider ends-->
    <!--slider end-->
    <div class="container-fluid">
        <div class="container">
            <div class="row">
                <!--Book an Appointment start-->
                <div class="col-lg-6-new col-md-5 col-sm-12">
                    <%--  <h4 class="hs_heading" id="hs_appointment_form_link">
                  
                </h4>--%>
                    <br />
                    <%--       <div class="hs_appointment_form_div">--%>
                    <%-- <img src="http://placehold.it/512x345" width="512" height="387" alt="" />--%>
                    <div style="height: 100%; width: 100%">
                        <form id="form1" runat="server">
                            <div id="myTabContent" class="tab-content">
                                <%--  <div class="tab-pane fade in active" id="AddEditEmp">--%>
                                <%--   <div class="row">--%>
                                <%--        <div class="col-lg-6-new col-md-6 col-sm-6">--%>
                                <h4 class="hs_theme_color">Log in</h4>
                                <table>
                                    <tr>
                                        <td style="height: 50px">
                                            <div class="form-group">
                                                <input type="text" id="UserName" runat="server" name="UserName"
                                                    class="form-control" placeholder="User name(required)" required />
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="height: 50px">
                                            <div class="form-group">
                                                <input type="password" id="Password" runat="server" name="Password"
                                                    class="form-control" placeholder="Password(required)" required />
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="height: 50px">
                                            <div class="form-group">
                                                <asp:Button ID="btnLogin" runat="server" Text="Login" CssClass="btn btn-default"
                                                    OnClick="btnLogin_Click" ValidationGroup="0" />
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="height: 50px">
                                            <div class="form-group">
                                                <label id="message" runat="server"></label>
                                            </div>
                                        </td>
                                    </tr>
                                </table>
                                <%--<table>
                                    <tr>
                                        <td>
                                            <asp:Image ID="imgprofile" runat="server" Height="100px" Width="100px" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Id </td>
                                        <td>
                                            <asp:Label ID="lblid" runat="server" Text=""></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Name </td>
                                        <td>
                                            <asp:Label ID="lblname" runat="server" Text=""></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Gender </td>
                                        <td>
                                            <asp:Label ID="lblgender" runat="server" Text=""></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>locale </td>
                                        <td>
                                            <asp:Label ID="lbllocale" runat="server" Text=""></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>link </td>
                                        <td>
                                            <asp:HyperLink ID="hylprofile" runat="server">Profile link</asp:HyperLink>
                                        </td>
                                    </tr>
                                </table>--%>
                                <%-- </div>--%>
                                <%-- </div>--%>
                                <%--</div>--%>
                            </div>
                        </form>

                        <%-- <div class="row">
                <div class="col-lg-6 col-md-7 col-sm-6">
                
                <div class="form-group">
                  <input type="text" class="form-control" id="slider_fname" name="fname" placeholder="full Name  ( required )" required>
                </div>
                <div class="form-group">
                  <input type="text" id="slider_phone" name="phone" class="form-control"  placeholder="Phone (required)" required>
                </div>
                <div class="form-group">
                  <input type="email" id="slider_email" name="email" class="form-control"  placeholder="Email (required)" required>
                </div>
                <div class="form-group">
                  <input type="date" id="slider_date" name="date" class="form-control">
                </div>
              </div>
            </div>--%>
                        <%--<div class="row">
              <div class="col-lg-3 col-md-4 col-sm-3">
                <button type="button" id="slider_book_apo" class="btn btn-default">Submit</button>
              </div>
              <div class="col-lg-8 col-md-8 col-sm-8">
                <p>Aenean facilisis sodales est nec gravida. Morbi vitae purus non est facilisis.</p>
              </div>
            </div>--%>
                        <p id="appointment_err">
                        </p>

                    </div>
                    <%--  </div>--%>
                </div>
                <!--Book an Appointment end-->
            </div>
            <!--Up Coming Events start-->
            <%--<div class="row">
    <div class="col-lg-12 col-md-12 col-sm-12">
      <h4 class="hs_heading">Up Coming Events</h4>
      <div class="up_coming_events">
        <div id="up_coming_events_slider" class="owl-carousel owl-theme">
          <div class="up_coming_events_slider_item">
            <div class="row">
              <div class="col-lg-12 col-md-12 col-sm-12">
                <div class="hs_event_date">
                  <h3>14</h3>
                  <p>Feb</p>
                </div>
              </div>
            </div>
            <div class="hs_event_div">
              <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12">
                  <div class="col-lg-6 col-md-5 col-sm-6"> <img src="http://placehold.it/160x243" alt="" /> </div>
                  <div class="col-lg-6 col-md-7 col-sm-12">
                    <h4>Pelln sque vitae dolor non.</h4>
                    <p>Cras sodaleut ligula, velit enim quis, neatis feugiat ante. Ut arcu nulla.Cras velit ligula, sodaleut enim quis, venenatis feugiat ante. lus facilisis nisl. </p>
                    <a href="../blog-single-post-rightsidebar.html" class="btn btn-default pull-right">Read More</a> </div>
                </div>
              </div>
            </div>
          </div>
          <div class="up_coming_events_slider_item">
            <div class="row">
              <div class="col-lg-12">
                <div class="hs_event_date">
                  <h3>23</h3>
                  <p>Feb</p>
                </div>
              </div>
            </div>
            <div class="hs_event_div">
              <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12">
                  <div class="col-lg-6 col-md-5 col-sm-6"> <img src="http://placehold.it/160x243" alt="" /> </div>
                  <div class="col-lg-6 col-md-7 col-sm-12">
                    <h4>Pelln sque vitae dolor non.</h4>
                    <p>Cras sodaleut ligula, velit enim quis, neatis feugiat ante. Ut arcu nulla.Cras velit ligula, sodaleut enim quis, venenatis feugiat ante. lus facilisis nisl. </p>
                    <a href="../blog-single-post-rightsidebar.html" class="btn btn-default pull-right">Read More</a> </div>
                </div>
              </div>
            </div>
          </div>
          <div class="up_coming_events_slider_item">
            <div class="row">
              <div class="col-lg-12 col-md-12 col-sm-12">
                <div class="hs_event_date">
                  <h3>24</h3>
                  <p>Feb</p>
                </div>
              </div>
            </div>
            <div class="hs_event_div">
              <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12">
                  <div class="col-lg-6 col-md-5 col-sm-6"> <img src="http://placehold.it/160x243" alt="" /> </div>
                  <div class="col-lg-6 col-md-7 col-sm-12">
                    <h4>Pelln sque vitae dolor non.</h4>
                    <p>Cras sodaleut ligula, velit enim quis, neatis feugiat ante. Ut arcu nulla.Cras velit ligula, sodaleut enim quis, venenatis feugiat ante. lus facilisis nisl. </p>
                    <a href="../blog-single-post-rightsidebar.html" class="btn btn-default pull-right">Read More</a> </div>
                </div>
              </div>
            </div>
          </div>
          <div class="up_coming_events_slider_item">
            <div class="row">
              <div class="col-lg-12 col-md-12 col-sm-12">
                <div class="hs_event_date">
                  <h3>14</h3>
                  <p>Feb</p>
                </div>
              </div>
            </div>
            <div class="hs_event_div">
              <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12">
                  <div class="col-lg-6 col-md-5 col-sm-6"> <img src="http://placehold.it/160x243" alt="" /> </div>
                  <div class="col-lg-6 col-md-7 col-sm-12">
                    <h4>Pelln sque vitae dolor non.</h4>
                    <p>Cras sodaleut ligula, velit enim quis, neatis feugiat ante. Ut arcu nulla.Cras velit ligula, sodaleut enim quis, venenatis feugiat ante. lus facilisis nisl. </p>
                    <a href="../blog-single-post-rightsidebar.html" class="btn btn-default pull-right">Read More</a> </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="customNavigation text-right"> <a class="btn_prev prev"><i class="fa fa-chevron-left"></i></a> <a class="btn_next next"><i class="fa fa-chevron-right"></i></a> </div>
      </div>
    </div>
  </div>--%>
            <!--Up Coming Events end-->
            <div class="hs_margin_40">
            </div>
            <!--Our Doctor Team start-->
            <%--<div class="row">
    <div class="col-lg-12 col-md-12 col-sm-12">
      <h4 class="hs_heading" id="hs_meat_doc">Our Doctor Team</h4>
      <div class="our_doctor_team">
        <div id="our_doctor_team_slider" class="owl-carousel owl-theme">
          <div class="our_doctor_team_slider_item"> <img src="http://placehold.it/300x362" alt="" />
            <div class="hs_team_member_detail">
              <h3>Dr Johnathan Treat</h3>
              <p>Quisque vitae interdum ipsum. Nulla eget mpernulla. Proin lacinia urna </p>
              <a href="../profile-single.html" class="btn btn-default">Read More</a> </div>
          </div>
          <div class="our_doctor_team_slider_item"> <img src="http://placehold.it/300x362" alt="" />
            <div class="hs_team_member_detail">
              <h3>Dr. Edwin Spindrift</h3>
              <p>Quisque vitae interdum ipsum. Nulla eget mpernulla. Proin lacinia urna </p>
              <a href="../profile-single.html" class="btn btn-default">Read More</a> </div>
          </div>
          <div class="our_doctor_team_slider_item"> <img src="http://placehold.it/300x362" alt="" />
            <div class="hs_team_member_detail">
              <h3>Dr Johnathan Treat</h3>
              <p>Quisque vitae interdum ipsum. Nulla eget mpernulla. Proin lacinia urna </p>
              <a href="../profile-single.html" class="btn btn-default">Read More</a> </div>
          </div>
          <div class="our_doctor_team_slider_item"> <img src="http://placehold.it/300x362" alt="" />
            <div class="hs_team_member_detail">
              <h3>Dr. Edwin Spindrift</h3>
              <p>Quisque vitae interdum ipsum. Nulla eget mpernulla. Proin lacinia urna </p>
              <a href="../profile-single.html" class="btn btn-default">Read More</a> </div>
          </div>
        </div>
        <div class="customNavigation text-right"> <a class="btn_prev prev"><i class="fa fa-chevron-left"></i></a> <a class="btn_next next"><i class="fa fa-chevron-right"></i></a> </div>
      </div>
    </div>
  </div>--%>
            <!--Our Doctor Team end-->
            <div class="clearfix">
            </div>
            <!--Meet Our Partners start-->
            <%--<div class="row">
    <div class="col-lg-12 col-md-12 col-sm-12">
      <h4 class="hs_heading">Meet Our Partners</h4>
      <div class="our_partners">
        <div id="our_partners_slider" class="owl-carousel owl-theme">
          <div class="our_partners_slider_item"> <img src="../images/partner/partner1.png" alt="" width="142" height="40"  /> </div>
          <div class="our_partners_slider_item"> <img src="../images/partner/partner2.png" alt="" width="142" height="40"  /> </div>
          <div class="our_partners_slider_item"> <img src="../images/partner/partner3.png" alt="" width="142" height="40"  /> </div>
          <div class="our_partners_slider_item"> <img src="../images/partner/partner4.png" alt="" width="142" height="40"  /> </div>
          <div class="our_partners_slider_item"> <img src="../images/partner/partner1.png" alt="" width="142" height="40"  /> </div>
        </div>
        <div class="customNavigation text-right"> <a class="btn_prev prev"><i class="fa fa-chevron-left"></i></a> <a class="btn_next next"><i class="fa fa-chevron-right"></i></a> </div>
      </div>
    </div>
  </div>--%>
            <!--Meet Our Partners end-->
            <div class="hs_margin_60">
            </div>
        </div>
    </div>
    <footer id="hs_footer">
        <div class="container">
            <div class="hs_footer_content">
                <%--  <div class="row">
        <div class="col-lg-12">
          <div class="hs_footer_menu">
            <ul>
              <li><a href="../index.html">Home</a></li>
              <li><a href="../about.html">About</a></li>
              <li><a href="../services.html">Services</a></li>
              <li><a href="../blog-categories.html">Blog</a></li>
              <li><a href="../profile.html">Our profile</a></li>
              <li><a href="../contact.html">Contact</a></li>
            </ul>
          </div>
        </div>
      </div>--%>
                <div class="row">
                    <div class="col-lg-8 col-md-8 col-sm-8">
                        <div class="row">
                            <div class="hs_footer_about_us">
                                <div class="col-lg-3 col-md-4 col-sm-12 col-md-12 col-sm-12">
                                    <%--   <h4 class="hs_heading">About Us</h4>--%>
              <%--                      <a href="../index.html">
                                        <img src="../images/logo.png" alt="logo" width="180" height="41" /></a>--%>
                                     <a href="#"> 
                   <%--<img src="../images/logo.png" alt="" />--%>
                   <img src="../images/logo1.png"  />   
               </a>
           <a style="font-family:  Calibri;color:white; font-size:large; font-weight:bold;  text-transform:uppercase"> LetMeTalk2me
           </a>
                                </div>
                                <%-- <div class="col-lg-9 col-md-8 col-sm-12 hs_about_us">
                <div class="hs_margin_60"></div>
                <p>Aenean facilisis sodales est neciMorbi vitapurus on Est facilisisro convallis commodo velante, tiam ltricies lputate.Aenean facilisis sodales est neciMorbi vitapurus on Est facilisisro convallis commodo velante, tiam ltricies lputate. </p>
              </div>--%>
                            </div>
                        </div>
                        <%--          <div class="row">
            <div class="col-lg-4 col-md-3 col-sm-12">
              <h4 class="hs_heading">Get in touch !</h4>
              <div class="hs_contact_detail">
                <p><i class="fa fa-map-marker"></i> 13/2 Elizabeth Street Melbourne VIC 3000, Australia</p>
                <p><i class="fa fa-mobile-phone"></i> +61 3 8376 6284</p>
                <div class="clearfix"></div>
                <div class="hs_social">
                  <ul>
                    <li><a href=""><i class="fa fa-facebook"></i></a></li>
                    <li><a href=""><i class="fa fa-twitter"></i></a></li>
                    <li><a href=""><i class="fa fa-google-plus"></i></a></li>
                    <li><a href=""><i class="fa fa-linkedin"></i></a></li>
                  </ul>
                </div>
              </div>
            </div>
            <div class="col-lg-8 col-md-9 col-sm-12">
              <h4 class="hs_heading">Useful Links</h4>
              <div class="clearfix"></div>
              <div class="hs_footer_link">
                <ul>
                  <li><a href="../services.html">Pediatric Clinic</a></li>
                  <li><a href="../services.html">Dental Clinic</a></li>
                  <li><a href="../services.html">General Surgery</a></li>
                  <li><a href="../services.html">Physiotherapy</a></li>
                  <li><a href="../services.html">Ltricies lputate</a></li>
                </ul>
              </div>
              <div class="hs_footer_link">
                <ul>
                  <li><a href="../blog-categories.html">Blog Categories</a></li>
                  <li><a href="../services-two-column.html">services Two Column</a></li>
                  <li><a href="../blog-single-post.html">Blog Single Post</a></li>
                  <li><a href="../services.html">services Three Column</a></li>
                  <li><a href="../blog-single-post-leftsidebar.html">Blog Leftsidebar</a></li>
                </ul>
              </div>
              <div class="hs_footer_link">
                <ul>
                  <li><a href="../blog-single-post-rightsidebar.html">Blog Rightsidebar</a></li>
                  <li><a href="../typography.html">Typography</a></li>
                  <li><a href="../elements.html">Elements</a></li>
                  <li><a href="../columns.html">columns</a></li>
                  <li><a href="../icon.html">icon</a></li>
                </ul>
              </div>
            </div>
          </div>--%>
                    </div>
                    <%--     <div class="col-lg-4 col-md-4 col-sm-4">
          <h4 class="hs_heading">Twitter Widget</h4>
          <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 hs_twitter_widget">
              <ul>
                <li> <i class="fa fa-twitter"></i> <a href="">
                  <p>@healthcare...  Suspende potenti. Etiam ullamcorper scelerisque bibendumonec cursus,  eleifend semper. <br>
                    <br>
                    <strong>17 hours ago</strong> </p>
                  </a> </li>
                <li> <i class="fa fa-twitter"></i> <a href="">
                  <p>@Hsoftindia...  Suspende potenti. Etiam ullamcorper scelerisque bibendumonec cursus,  eleifend semper. <br>
                    <br>
                    <strong>2 min ago</strong> </p>
                  </a> </li>
              </ul>
            </div>
          </div>
        </div>--%>
                </div>
            </div>
        </div>
    </footer>
    <div class="hs_copyright">
        Copyright © 2018 Clairvoyance Mindware Private Limited
    </div>
    <!--main js file start-->
    <script type="text/javascript" src="../js/jquery-1.11.0.min.js"></script>
    <script type="text/javascript" src="../js/bootstrap.js"></script>
    <script type="text/javascript" src="../js/owl.carousel.js"></script>
    <script type="text/javascript" src="../js/jquery.bxslider.js"></script>
    <script type="text/javascript" src="../js/jquery.mixitup.min.js"></script>
    <script type="text/javascript" src="../js/smoothscroll.js"></script>
    <script type="text/javascript" src="../js/single-0.1.0.js"></script>
    <script type="text/javascript" src="../js/custom.js"></script>
    <!--main js file end-->
</body>
</html>
