﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using DAL;
using System.Drawing;
using System.Web.Mail;

namespace HealToZeal.Employee
{
    public partial class ViewUserResult : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                if (Session["EmployeeId"] != null)
                {
                    if (Request.QueryString["EmployeeId"] != null)
                    {
                        BindReport();
                    }                 
                }
            }
        }
        private void BindReport()
        {
            try
            {
                EmployeeDAL objEmployeeDAL = new EmployeeDAL();

                DataTable DtEmployeeDetails = objEmployeeDAL.EmployeeAssessmentSubmittedGetByEmployeeId(Request.QueryString["EmployeeId"].ToString());
                RptrEmployee.DataSource = DtEmployeeDetails;
                RptrEmployee.DataBind();

                //DataTable Dt = objEmployeeDAL.EmployeeAssessmentClusterwiseReport(Convert.ToInt32(Session["EmployeeCompanyId"].ToString()));
                //DataTable DtResult = new DataTable();//Dt;
                //DtResult = Dt.Select(" Result<>'' or Suggestion<>''", "Result Desc").CopyToDataTable();
                //GvResult.DataSource = DtResult;
                //GvResult.DataBind();

                //Session["AssessmentInstanceCompanyId"]
                DataTable Dt = objEmployeeDAL.EmployeeResultGetForEmployee(Request.QueryString["EmployeeId"].ToString(), Session["AssessmentInstanceId"].ToString(), Session["InstanceCompanyRelationId"].ToString());
                DataTable DtCount = new DataTable();

                int CountYellow = (int)Dt.Compute("Count(Color)", "Color='#f4fb73' and IsPositive='N'");
                int CountRed = (int)Dt.Compute("Count(Color)", "Color='#e43c37'");
                int CountGreen = (int)Dt.Compute("Count(Color)", "Color='#88e46d'");

                // show button
                if (CountRed > 0)
                {
                    Ttle.Visible = false;
                    Ttle1.Visible = true;
                    Ttle3.Visible = true;
                    LblMessageTitle.Text = "Action items";
                    LblMessageTitle1.Text = "Action items";
                    lblMessage.Text = "Improve your lifestyle.We suggest a repeat test in 3 months.We strongly recommend an immediate meeting with a professional counselor.";
                    LblMessageUp.Text = "Improve your lifestyle.We suggest a repeat test in 3 months.We strongly recommend an immediate meeting with a professional counselor.";
                    divalert.Visible = true;
                    divalert1.Visible = true;
                    //commented on 7 Jan 16  BtnSetUpMeeting.Visible = true;
                    divSuggestion.Visible = true;
                    //commented on 7 jan 16  btnCounsel.Visible = true;
                    if (CountGreen > 0)
                    {
                        Ttle.Visible = true;
                    }
                }
                else if ((CountYellow > 0) && (CountYellow == 1))
                {
                    Ttle.Visible = false;
                    Ttle1.Visible = true;
                    Ttle3.Visible = true;
                    //p1.InnerText = "Improve your lifestyle.";
                    //p2.InnerText = " We suggest a repeat test in 3 months.";
                    //p3.InnerText = "If you feel you need to talk to someone, set up a meeting with a professional counselor";
                    LblMessageTitle.Text = "Action items";
                    LblMessageTitle1.Text = "Action items";

                    lblMessage.Text = " Improve your lifestyle. We suggest a repeat test in 3 months. If you feel you need to talk to someone,";
                    LblMessageUp.Text = " Improve your lifestyle. We suggest a repeat test in 3 months. If you feel you need to talk to someone,";
                    divalert.Visible = true;
                    divalert1.Visible = true;
                    //commented on 7 Jan 16  BtnSetUpMeeting.Visible = true;
                    divSuggestion.Visible = true;
                    //commented on 7 jan 16 btnCounsel.Visible = true;
                    if (CountGreen > 0)
                    {
                        Ttle.Visible = true;
                    }
                }
                else if (CountYellow > 1)
                {
                    Ttle.Visible = false;
                    Ttle1.Visible = true;
                    Ttle3.Visible = true;
                    LblMessageTitle.Text = "Action items";
                    LblMessageTitle1.Text = "Action items";

                    lblMessage.Text = "Improve your lifestyle. We suggest a repeat test in 3 months. We recommend meeting a professional counselor.";
                    LblMessageUp.Text = "Improve your lifestyle. We suggest a repeat test in 3 months. We recommend meeting a professional counselor.";
                    divalert.Visible = true;
                    divalert1.Visible = true;
                    //commented on 7 Jan 16   BtnSetUpMeeting.Visible = true;
                    divSuggestion.Visible = true;
                    //commented on 7 jan 16 btnCounsel.Visible = true;
                    if (CountGreen > 0)
                    {
                        Ttle.Visible = true;
                    }
                }
                else if (CountGreen > 0)
                {
                    Ttle.Visible = true;
                    Ttle1.Visible = false;
                    Ttle3.Visible = false;
                    divalert.Visible = false;
                    divalert1.Visible = false;
                    GvSuggestion.Visible = false;
                    //commented on 7 jan 16 btnCounsel.Visible = false;
                }
                else
                {
                    LblMessageTitle.Text = "Congratulations!";
                    LblMessageTitle1.Text = "Congratulations!";

                    lblMessage.Text = "You seem to be well placed from psychologic health perspective. Keep doing your good work. We recommend you repeat the test after 6 months so as to get alerts if any.";
                    LblMessageUp.Text = "You seem to be well placed from psychologic health perspective. Keep doing your good work. We recommend you repeat the test after 6 months so as to get alerts if any.";

                    Ttle1.Visible = false;
                    // divalert.Visible = false;
                    //divalert1.Visible = false;

                    //commented on 7 jan 16  BtnSetUpMeeting.Visible = false;
                    Ttle3.Visible = false;
                    divSuggestion.Visible = false;
                    //commented on 7 jan 16  btnCounsel.Visible = false;
                }

                //DataRow[] DrowPositive = Dt.Select(" IsPositive='Y' and Color='#88e46d'", "");

                //if(DrowPositive.Length >0)
                //{
                //    DataTable DtPositive = new DataTable();//Dt;
                //    DtPositive = Dt.Select(" IsPositive='Y' and Color='#88e46d'", "").CopyToDataTable();

                //    if (DtPositive != null)
                //    {
                //        if (DtPositive.Rows.Count > 0)
                //        {
                //            //Ttle.Visible = true;
                //            GvPositive.DataSource = DtPositive;
                //            GvPositive.DataBind();
                //        }
                //        else
                //        {
                //            //Ttle.Visible = false;
                //        }
                //    }
                //    else
                //    {
                //       // Ttle.Visible = false;
                //    }
                //}
                //else
                //{
                //    GvPositive.DataSource = null;
                //    GvPositive.DataBind();
                //    //Ttle.Visible = false;
                //}


                //DataRow[] DrowNegative = Dt.Select(" IsPositive='N'", "");
                //if(DrowNegative.Length >0)
                //{
                //    DataTable DtPositive = Dt.Select(" IsPositive='N'", "").CopyToDataTable();
                //    if (DtPositive != null)
                //    {
                //        if (DtPositive.Rows.Count > 0)
                //        {
                //           // Ttle1.Visible = true;
                //            GvResult.DataSource = DtPositive;
                //            GvResult.DataBind();
                //        }
                //        else
                //        {
                //            //Ttle1.Visible = false;
                //        }
                //    }
                //    else
                //    {
                //       // Ttle1.Visible = false;
                //    }
                //}
                //else
                //{
                //   // Ttle1.Visible = false;
                //    GvResult.DataSource = null;
                //    GvResult.DataBind();
                //}              

                DataTable DtPositive = new DataTable();//Dt;
                DataRow[] DrowPositive = Dt.Select("Color='#88e46d'", "");
                if (DrowPositive.Length > 0)
                {
                    DtPositive = Dt.Select("Color='#88e46d'", "").CopyToDataTable();
                    if (DtPositive != null && DtPositive.Rows.Count > 0)
                    {
                        GvPositive.DataSource = DtPositive;
                        GvPositive.DataBind();
                    }
                    else
                    {
                        GvPositive.DataSource = null;
                        GvPositive.DataBind();
                    }
                }
                else
                {
                    GvPositive.DataSource = null;
                    GvPositive.DataBind();
                }
                DataRow[] DrowNegative = Dt.Select("Color='#e43c37' Or Color='#f4fb73'", "");
                if (DrowNegative.Length > 0)
                {
                    DtPositive = Dt.Select("(Color='#e43c37' Or Color='#f4fb73') and (Ispositive='N')", "").CopyToDataTable();
                    if (DtPositive != null && DtPositive.Rows.Count > 0)
                    {
                        GvResult.DataSource = DtPositive;
                        GvResult.DataBind();
                    }
                    else
                    {
                        GvResult.DataSource = null;
                        GvResult.DataBind();
                    }
                }
                else
                {
                    GvResult.DataSource = null;
                    GvResult.DataBind();
                }

                DataTable DtSuggestions = objEmployeeDAL.SuggestionMasterGetRandom();
                DataView view = new DataView(DtSuggestions);
                DataTable distinctValues = view.ToTable(true, "Title");

                if (distinctValues != null)
                {
                    if (distinctValues.Rows.Count > 0)
                    {
                        //  Ttle3.Visible = true;
                        GvSuggestion.DataSource = distinctValues; //DtSuggestions;
                        GvSuggestion.DataBind();
                    }
                    else
                    {
                        //Ttle3.Visible = false;
                    }
                }
                else
                {
                    // Ttle3.Visible = false;
                }


                //DataTable dt = objEmployeeDAL.EmployeeResultGet(Session["EmployeeId"].ToString());
                //GvResult.DataSource = dt;
                //GvResult.DataBind();                

                for (int a = 0; a < GvSuggestion.Rows.Count; a++)
                {
                    Label lblSuggestionTitle = GvSuggestion.Rows[a].FindControl("lblSuggestionTitle") as Label;
                    if (lblSuggestionTitle != null)
                    {
                        DataTable DtSuggestionText = DtSuggestions.Select("Title='" + lblSuggestionTitle.Text + "'").CopyToDataTable();
                        GridView Gdv = (GridView)GvSuggestion.Rows[a].FindControl("GdvSuggestionText");
                        Gdv.DataSource = DtSuggestionText;
                        Gdv.DataBind();
                    }
                }
            }
            catch
            {

            }
        }

        protected void ddlAssessment_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                ReportViewer1.LocalReport.Refresh();
                BindReport();
            }
            catch
            {

            }
        }

        protected void btnCounsel_Click(object sender, EventArgs e)
        {
            try
            {
                DivSetUpMeeting.Focus();
                DivSetUpMeeting.Visible = true;
                DivEmail.Visible = true;

                DivMailBody.Visible = false;
                DivTitle.Visible = false;

                EmployeeDAL objEmployeeDAL = new EmployeeDAL();
                DataTable DtEmployeeDetails = objEmployeeDAL.EmployeeAssessmentSubmittedGetByEmployeeId(Session["EmployeeId"].ToString());
                uemail.Value = DtEmployeeDetails.Rows[0]["EmailId"].ToString();
                uemail1.Value = DtEmployeeDetails.Rows[0]["EmailId"].ToString();
            }
            catch (Exception )
            {

            }
        }

        protected void BtnSendEmail_Click(object sender, EventArgs e)
        {
            try
            {
                System.Web.Mail.MailMessage mail = new System.Web.Mail.MailMessage();

                if (uemail.Value != "")
                {
                    mail.From = uemail.Value;
                }
                else if (uemail1.Value != "")
                {
                    mail.From = uemail1.Value;
                }

                mail.To = "prajakta.mulay@chetanasystems.com";
                mail.Subject = "User want to contact for appointment.";
                if (message.Value != "")
                {
                    mail.Body = message.Value;
                }
                else if (message1.Value != "")
                {
                    mail.Body = message1.Value;
                }


                mail.BodyFormat = MailFormat.Html;
                mail.Fields.Add("http://schemas.microsoft.com/cdo/configuration/smtpauthenticate", "1");    //basic authentication
                mail.Fields.Add("http://schemas.microsoft.com/cdo/configuration/sendusername", "HealToZeal@chetanatspl.com"); //set your username here
                mail.Fields.Add("http://schemas.microsoft.com/cdo/configuration/sendpassword", "HealToZeal");    //set your password here
                SmtpMail.SmtpServer = "holly.arvixe.com";  //your real server goes here                        
                SmtpMail.Send(mail);
                DivEmail.Visible = false;
                DivSetUpMeeting.Visible = false;
                DivMailBody.Visible = false;
                DivTitle.Visible = false;
                ScriptManager.RegisterClientScriptBlock(this.Page, GetType(), "openColorBox", "<script> alert('We have received your meeting request. We will get back to you very soon'); </script>;", true);
            }
            catch (Exception )
            {

            }
        }

        protected void BtnSetUpMeeting_Click(object sender, EventArgs e)
        {
            try
            {
                DivTitle.Focus();
                DivMailBody.Visible = true;
                DivTitle.Visible = true;

                DivEmail.Visible = false;
                DivSetUpMeeting.Visible = false;

                EmployeeDAL objEmployeeDAL = new EmployeeDAL();
                DataTable DtEmployeeDetails = objEmployeeDAL.EmployeeAssessmentSubmittedGetByEmployeeId(Session["EmployeeId"].ToString());
                uemail.Value = DtEmployeeDetails.Rows[0]["EmailId"].ToString();
            }
            catch (Exception )
            {

            }
        }
    }
}