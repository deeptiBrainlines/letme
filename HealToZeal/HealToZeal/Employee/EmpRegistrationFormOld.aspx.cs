﻿using BE;
using DAL;
using DocumentFormat.OpenXml.VariantTypes;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Facebook;


namespace HealToZeal.Employee
{
    public partial class EmpRegistrationForm : System.Web.UI.Page
    {
        string CompanyId = "";
        EmployeeDAL objEmployeeDAL = new EmployeeDAL();
        string EmployeeId = "";
        string googleplus_client_id = "961545322562-359k8koggghm7n5jtgnb4hmvugtomfo3.apps.googleusercontent.com";
       // string googleplus_client_secret = "g1B83i1jfVBSn-jxMnqVjkS7";
       string googleplus_redirect_url = "https://localhost:44352/Employee/EmployeeLogin.aspx";
       // string googleplus_redirect_url = "http://apps.brainlines.net/LMT2M_test/Employee/EmployeeLogin.aspx";
       // string Parameters;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                string InstanceCompanyRelationId = Request.QueryString["InstanceCompanyRelationId"];
                string CouponId = Request.QueryString["CouponId"];
                lblMessage.Text = "welcome" + CompanyId + " " + CouponId;

                // CompanyId = "987109F8-B639-4FE0-85F3-0FCFEFF63B5E";

                DataTable compstatus = objEmployeeDAL.GetCompanyIDBatchID(InstanceCompanyRelationId);


                Session["CompanyId"] = compstatus.Rows[0]["CompanyId_FK"].ToString();
                Session["BatchID"] = compstatus.Rows[0]["BatchID_Fk"].ToString();


                //CompanyId = "987109F8-B639-4FE0-85F3-0FCFEFF63B5E";

                //CompanyId = Request.QueryString["CompanyId"];
                //reset();

            }

            if (Request.QueryString["code"] != null)
            {
                string accessCode = Request.QueryString["code"].ToString();

                var fb = new FacebookClient();

                // throws OAuthException 
                dynamic result = fb.Post("oauth/access_token", new
                {

                    client_id = "your_app_id",

                    client_secret = "your_app_secret",

                    redirect_uri = "http://localhost:8779/FacebookDemo.aspx",

                    code = accessCode

                });

                var accessToken = result.access_token;
            }


        }

        protected void btnsubmit_Click(object sender, EventArgs e)
        {
            try
            {
                InsertUpdateEmployee();
              
            }
            catch (Exception ex1)
            {
                ex1.InnerException.ToString();
            }
            
        }

        public void reset()
        {
            lblfirstname.Text = "";
            lbllastname.Text = "";
            lblemail.Text = "";
            lblphoneno.Text = "";
        }

        private void InsertUpdateEmployee()
        {
            try
            {
                //  Guid EmployeeId_PK;
                //  Guid.TryParse("EmployeeId_PK", out EmployeeId_PK);
                //insert
                string password = Encryption.CreatePassword();
                string userName = Encryption.CreateUsername();
                string pass = Encryption.Encrypt(password);
                string verificationcode = Guid.NewGuid().ToString();
                EmployeeBE objEmployeeBE = new EmployeeBE();

              //  CompanyId = "987109F8-B639-4FE0-85F3-0FCFEFF63B5E";
                objEmployeeBE.propUserName = userName;
                objEmployeeBE.propPassword = pass;
                objEmployeeBE.propCompanyId_FK = Session["CompanyId"].ToString();
                objEmployeeBE.propFirstName = txtfirstname.Text;
                objEmployeeBE.propLastName = txtlastname.Text;
                objEmployeeBE.propEmailId = txtemail.Text;
                objEmployeeBE.PropBatchID = Session["BatchID"].ToString();
                objEmployeeBE.Verificationcode = verificationcode;
               // objEmployeeBE.propEmployeeCompanyId =Convert.ToInt16( txtEmpCompID.Text);
                Session["verificationcode"] = verificationcode;
                //  objEmployeeBE.propOccupation = ddloccupation.SelectedItem.Text;
                //objEmployeeBE.propExperienceInYears = txtexp.Text;
                if (chkTandD.Checked == true)
                {

                    objEmployeeBE.PropTandD = "True";
                }
                else
                {
                    objEmployeeBE.PropTandD = "False";
                }
                int Status = 0;

                Status = objEmployeeDAL.EmployeesRegistrationInsert(objEmployeeBE);

                if (Status > 0) {
                    // ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('You have ARegisterd successfully, Please check Email for Email Activation)", true);
                    Response.Write(@"<script language='javascript'>alert('Alert: \n" + "You have Registerd successfully, Please check Email for Activation" + " .');</script>");
                    SendActivationEmail();
                   Response.Redirect("Emailvarificationsuccessmessage.aspx");
                }
                else
                {
                    Response.Write(@"<script language='javascript'>alert('Alert: \n" + "EmailID Already Exists,Please provide another Email ID" + " .');</script>");
                    // ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('EmailID Already Exists,Plese provide another Email ID)", true);
                }

                //ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('Please check Email for Email Activation)", true);

                //lblMessage.Text = "Employee added successfully...";
                //string message = "Please check Email for Email Activation";
                //string script = "window.onload = function(){ alert('";
                //script += message;
                //script += "')};";
                //ClientScript.RegisterStartupScript(this.GetType(), "SuccessMessage", script, true);

            }
            catch (Exception ex)
            {
                ex.InnerException.ToString();
            }
        }


        private void SendActivationEmail()
        {

            System.Net.Mail.MailMessage mail = new System.Net.Mail.MailMessage();
            // System.Web.Mail.MailMessage mail = new System.Web.Mail.MailMessage();
            var DecintellURl = ConfigurationManager.AppSettings.Get("Activationurl");
            string verificationcode = Session["verificationcode"].ToString();
            mail.To.Add(txtemail.Text);
            mail.CC.Add("contact@letmetalk2.me");
            mail.Bcc.Add("priyamvada.bavadekar@letmetalk2.me");
            // mail.From = "contact@letmetalk2.me";
            // mail.From = new MailAddress("contact@letmetalk2.me", "Let me talk to me", System.Text.Encoding.UTF8);
            mail.Subject = "Account Activation";
            string getDt = DateTime.Now.Date.AddDays(7).ToString("MM/dd/yyyy");
            string body = "Hello " + txtfirstname.Text.Trim() + ",";
            body += "<br /><br />Please click the following link to activate your account";
            mail.Body = body + "<br />" + DecintellURl + "" + verificationcode;
            mail.IsBodyHtml = true;

            mail.Priority = System.Net.Mail.MailPriority.High;
            SmtpClient client = new SmtpClient();
            // client.Credentials = new System.Net.NetworkCredential("contact@letmetalk2.me", "contact123");
            //  client.Port = 587;
            // client.Host = "dallas137.arvixeshared.com";
            client.EnableSsl = false;
            client.Send(mail);
           
        }

        protected void btngmaillogin_Click(object sender, EventArgs e)
        {


            var Googleurl = "https://accounts.google.com/o/oauth2/auth?response_type=code&redirect_uri=" + googleplus_redirect_url + "&scope=https://www.googleapis.com/auth/userinfo.email%20https://www.googleapis.com/auth/userinfo.profile&client_id=" + googleplus_client_id;
            Session["loginWith"] = "google";
            Response.Redirect(Googleurl);


            //string clientid = "831162440368-gjpqkqbdj5mik8adij6s313qt51kqjor.apps.googleusercontent.com";
            ////your client secret  
            //string clientsecret = "8xN5P5Gqtmk1_hwzEHQm22uT";
            //////your redirection url  
            //string redirection_url = "http://localhost:44352/Employee/EmployeeLogin.aspx";
            //string url = "https://accounts.google.com/o/oauth2/v2/auth?scope=profile&include_granted_scopes=true&redirect_uri=" + redirection_url + "&response_type=code&client_id=" + clientid + "";
            //Response.Redirect(url);
            
        }

        protected void FBLogin_Click(object sender, EventArgs e)
        {


            var fb = new FacebookClient();

            var loginUrl = fb.GetLoginUrl(new
            {

                client_id = "682470882264872",
               redirect_uri = "https://localhost:44352/Employee/oauth-redirect.aspx",
                 //redirect_uri = "http://apps.brainlines.net/LMT2M_test/Employee/oauth-redirect.aspx",

                //response_type = "code",b96294d92d87f37d53fe64a993ff6989

                scope = "email" /// Add other permissions as needed

            });
            Response.Redirect(loginUrl.AbsoluteUri);
            {
            }


            //string clientId = "2447416122199968";
            //string redirectUrl = "https://localhost:44352/Employee/oauth-redirect.aspx";
            //Response.Redirect(string.Format("https://graph.facebook.com/oauth/authorize?client_id={0}&redirect_uri={1}", clientId, redirectUrl));
            
        }
    }
}