﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Employee/Employee.Master" CodeBehind="AssessmentMessageold.aspx.cs" Inherits="HealToZeal.Employee.AssessmentMessage" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="EmployeeColumn" runat="server">

     <script type="text/javascript">
        function CloseColorBox(Url) {
            parent.$.colorbox.close();
            top.location.href = Url;
            return false;
        }
        </script>
   <div class="container">
       <h4></h4>
       <div class="row">
            <div id="divalert" class="alert alert-success" runat="server">
                       <strong>Well done!</strong> You successfully completed this assessment.
                          <br />
                          Do you want to attempt  
                          <asp:Button ID="btnNext" CssClass="btn btn-link btn-lg" runat="server" Text="next assessment/ view the result" OnClick="btnNext_Click" /> ?
                          <br />
                          Or want to give your 
                              <asp:Button ID="btnFeedback" CssClass="btn btn-link btn-lg" runat="server" Text="Feedback" OnClick="btnFeedback_Click" /> ?                  
                    
               </div>
        </div>
    </div>  

    </asp:Content>
