﻿using BE;
using DAL;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Timers;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

namespace HealToZeal.Employee
{
    public partial class UserResultReport : System.Web.UI.Page
    {
        log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        public static string employeeId = string.Empty;
        public static DataTable DtClusterPercentage = new DataTable();
        protected void Page_Load(object sender, EventArgs e)
        {
            Session["Heading"] = "report";

            if (!Page.IsPostBack)
            {
                if (Session["EmployeeId"] != null)
                {
                    employeeId = Convert.ToString(Session["EmployeeId"]);
                    string companyId = Convert.ToString(Session["CompanyId"]);
                    BindReportData();
                }
            }
            else
            {
                cpRepeater.DataSource = DtClusterPercentage;
                cpRepeater.DataBind();
            }

            if (Session["EmployeeId"] != null && Session["CompanyId"] != null && Session["FlagTest"].ToString() == "free")
            {
                RedirectMethod();
                HtmlMeta meta = new HtmlMeta();
                meta.HttpEquiv = "Refresh";
                meta.Content = "60;url=EmployeeLogin.aspx";
                this.Page.Controls.Add(meta);

            }

        }

        private void RedirectMethod()
        {
            EmployeeDAL objEmployeeDAL = new EmployeeDAL();
            if (Session["EmployeeId"] != null && Session["CompanyId"] != null && Session["FlagTest"].ToString() == "free")
            {
                try
                {
                    int result = objEmployeeDAL.EmployeesDelete(Session["EmployeeId"].ToString(), Session["CompanyId"].ToString());
                    // HttpContext.Current.Response.Redirect("~/Employee/EmployeeLogin.aspx");
                    Session.Abandon();
                }
                catch (Exception ex)
                {
                    log.Error(ex.Message.ToString() + " " + ex.StackTrace.ToString());
                }
            }
        }

        public void BindReportData()
        {
            try
            {
                EmployeeDAL objEmployeeDAL = new EmployeeDAL();

                DataTable DtEmployeeDetails = objEmployeeDAL.EmployeeAssessmentSubmittedGetByEmployeeId(employeeId);
                for (int i = 0; i < DtEmployeeDetails.Rows.Count; i++)
                {
                    if (i == 0)
                    {
                        DataTable Dt = objEmployeeDAL.EmployeeResultGetForEmployee(Session["EmployeeId"].ToString(), Session["AssessmentInstanceId"].ToString(), Session["AssessmentInstanceCompanyId"].ToString());
                        DataTable filterDT = RemoveNullColumnFromDataTable(Dt);

                    }


                }


                EmployeeAssessmentsHistoryDAL objEmployeeDAL1 = new EmployeeAssessmentsHistoryDAL();
                DtClusterPercentage = objEmployeeDAL1.EmployeeHistory(employeeId);
                DataTable filterTable = RemoveNullColumnFromDataTable(DtClusterPercentage);
                cpRepeater.DataSource = filterTable;
                cpRepeater.DataBind();
            }


            catch (Exception ex)
            {
                //    //ex.InnerException.ToString();
                log.Error(ex.Message.ToString() + " " + ex.StackTrace.ToString());
            }


        }
        public static DataTable RemoveNullColumnFromDataTable(DataTable dt)
        {
            for (int i = dt.Rows.Count - 1; i >= 0; i--)
            {
                if (dt.Rows[i]["ClusterName"] == null || dt.Rows[i]["ClusterName"].ToString() == "" || dt.Rows[i]["ClusterName"] == DBNull.Value)
                    dt.Rows[i].Delete();
            }
            dt.AcceptChanges();
            return dt;
        }

        protected void btnPremiumService_Click(object sender, EventArgs e)
        {
            EmployeeDAL employeeDAL = new EmployeeDAL();
            if (employeeId != null)
            {
                EmployeeDAL objEmployeeDAL1 = new EmployeeDAL();
                EmployeeBE objEmployeeBE = new EmployeeBE();
                objEmployeeBE.propEmployeeId_PK = Convert.ToString(employeeId);
                DataTable DtEmployeeDetails = objEmployeeDAL1.GetEmployeebyEmpid(objEmployeeBE);
                if (DtEmployeeDetails != null && DtEmployeeDetails.Rows.Count > 0)
                {
                    objEmployeeBE = new EmployeeBE();
                    objEmployeeBE.propEmployeeId_PK = Convert.ToString(employeeId);
                    objEmployeeBE.propFirstName = Convert.ToString(DtEmployeeDetails.Rows[0]["FirstName"]);
                    objEmployeeBE.propLastName = Convert.ToString(DtEmployeeDetails.Rows[0]["LastName"]);
                    objEmployeeBE.propEmailId = Convert.ToString(DtEmployeeDetails.Rows[0]["EmailId"]);
                    objEmployeeBE.propAge = Convert.ToDateTime(DtEmployeeDetails.Rows[0]["Age"]).ToString("MM/dd/yyyy");
                    string gender = Convert.ToString(DtEmployeeDetails.Rows[0]["Gender"]);
                    if (gender == "F")
                    {
                        objEmployeeBE.propGender = "Female";
                    }
                    else
                    {
                        objEmployeeBE.propGender = "Male";
                    }
                    objEmployeeBE.propQualification = Convert.ToString(DtEmployeeDetails.Rows[0]["Qualification"]);
                    objEmployeeBE.propExperienceInYears = Convert.ToString(DtEmployeeDetails.Rows[0]["ExperienceInYears"]);
                    objEmployeeBE.propOccupation = Convert.ToString(DtEmployeeDetails.Rows[0]["Occupation"]);
                    objEmployeeBE.propPosition = Convert.ToString(DtEmployeeDetails.Rows[0]["Position"]);
                    objEmployeeBE.propMobileNo = Convert.ToString(DtEmployeeDetails.Rows[0]["MobileNo"]);

                    //SendActivationEmailEmployee(objEmployeeBE);
                    SendActivationEmailAdmin(objEmployeeBE);
                }
            }
            HtmlMeta meta = new HtmlMeta();
            meta.HttpEquiv = "Refresh";
            meta.Content = "30;url=EmployeeLogin.aspx";
            this.Page.Controls.Add(meta);
            RedirectMethod();
            lblMessage1.Text = "Your request has been sent to Let Me Talk To Me admin. You will receive the premium service details shortly.";
            ClientScript.RegisterStartupScript(this.GetType(), "Pop", "openMessageModal();", true);
        }
        private void SendActivationEmailEmployee(EmployeeBE objEmployeeBE)
        {
            try
            {
                string DefaultImagePath = HttpContext.Current.Server.MapPath(@"~\UIFolder\dist\img\logotemp.png");
                //"D:\Brainlines\LetMeTalk2\lmt2mv2\HealToZeal\HealToZeal\UIFolder\dist\img\logotemp.png"

                byte[] imageArray = System.IO.File.ReadAllBytes(DefaultImagePath);
                string base64ImageRepresentation = Convert.ToBase64String(imageArray);

                MailMessage mail = new MailMessage();
                mail.To.Add(Convert.ToString(objEmployeeBE.propEmailId));
                //mail.To.Add("priyanka@brainlines.in,",Anuradha.Kulkarni@brainlines.in"");
                mail.CC.Add("contact@letmetalk2.me");
                mail.Bcc.Add("priyamvada.bavadekar@letmetalk2.me");
                // mail.From = "contact@letmetalk2.me";
                // mail.From = new MailAddress("contact@letmetalk2.me", "Let me talk to me", System.Text.Encoding.UTF8);
                mail.Subject = "click here for a detailed premium service";
                string getDt = DateTime.Now.Date.AddDays(7).ToString("MM/dd/yyyy");
                //string body = "<html><body><font color=\"black\">Hello " + firstName + ",";
                //body += "<br/><br/>";
                //mail.Body = body + " <br/><br/>click here for a detailed premium service<br/></br>Thank You.<br/><br/></font></body></html>";

                StringBuilder body = new StringBuilder();

                body.AppendLine("<table width=\"" + "100%" + "\" border=\"" + "0" + "\" cellpadding=\"" + "0" + "\" cellspacing=\"" + "0" + "\" style=\"" + "border:2px solid #b3c8c8 !important" + "\">");
                body.AppendLine("<tr>");
                body.AppendLine("<td><table width=\"" + "100% " + "\" border=\"" + "0" + "\" cellspacing=\"" + "0" + "\" cellpadding=\"" + "0" + "\">");
                body.AppendLine("<tr>");
                body.AppendLine("<td valign=\"" + "top" + "\"><table width=\"" + "90%" + "\" border=\"" + "0" + "\" align=\"" + "center" + "\" cellpadding=\"" + "0" + "\" cellspacing=\"" + "0" + "\">");
                body.AppendLine("<tr> <td style=\"" + "font-family:Verdana, Arial, Helvetica, sans-serif; font-size:12px; line-height:1.5; text-align:justify;" + "\">&nbsp;</td>");
                body.AppendLine("</tr><tr> <td  style=\"" + "font-family:Verdana, Arial, Helvetica, sans-serif;  font-size:13px; line-height:1.8; text-align:justify;" + "\"><div style=\"" + "font-size:18px; color:#5b6766; font-weight:800;" + "\">hi " + objEmployeeBE.propFirstName + ", </div>");
                body.AppendLine("<p>there are various psychometric tests available on internet but they are very generic and do not focus on the challenges of a particular segment/person. Being generic, it is possible that they are not answered seriously.<br /><br />");
                body.AppendLine("LMT2M tests are customized for a particular segment which focusses on the specific experiences and challenges the person faces based on what environment he is in. This helps the people to actually see themselves in a mirror and answer the tests as if they are actually telling their own story. This helps in accuracy of the results as well as awareness of the state of mind for them selves.<br/><br/>");
                body.AppendLine("so LMT2M tests are very specific in nature and hence it achieves the purpose of awareness and assessments with precision and accuracy. ");
                body.AppendLine("</p> <p style=\"" + "color:#829a9a; font-weight:800; font-size:14px; margin-bottom:0; padding-bottom:0;" + "\">please visit below link to pay and proceed for premium services");
                // body.AppendLine("</p> <a href='' style=\"" + "background-color: #5b756c;padding: 8px 10px;text-align: center;text-decoration: none;display: inline-block;" + "\"");
                body.AppendLine("</p><a href = \"" + ConfigurationManager.AppSettings.Get("Loginurl") + "\" style=\"" + "background-color: #5b756c; padding: 8px 10px;");
                body.AppendLine("text-align: center; text-decoration: none; display: inline-block; font-size: 15px; color: #fff; border-radius: 25px;\"" + ">letmetalk2.me</a></td></tr> <tr>");
                //body.AppendLine("font-size: 15px; color: #fff; border-radius: 25px; margin-top:10px;" + "\">letmetalk2.me</a></td></tr> <tr>");
                body.AppendLine("<td  style=\"" + "font-family:Verdana, Arial, Helvetica, sans-serif; font-size:12px; line-height:1.5; text-align:justify;" + "\">&nbsp;</td>");
                body.AppendLine("</tr></table></td> </tr><tr>");
                body.AppendLine("<td height=\"" + "140" + "\" bgcolor=\"" + "#b3c8c8" + "\" style=\"" + "font-family:Verdana, Arial, Helvetica, sans-serif; font-size:20px; color:#fff;" + "\"><table width=\"" + "90%" + "\" border=\"" + "0" + "\" align=\"" + "center" + "\" cellpadding=\"" + "0" + "\" cellspacing=\"" + "0" + "\">");
                body.AppendLine("<tr><td width=\"" + "23%" + "\"><img src='" + ConfigurationManager.AppSettings.Get("ImagePath") + @"\UIFolder\dist\img\logotemp.png' width ='120' height='122' /></td>");
                body.AppendLine("<td width=\"" + "2%" + "\">&nbsp;</td>");
                body.AppendLine("<td width=\"" + "75% " + "\" align=\"" + "left" + "\"><table width=\"" + "100% " + "\" border=\"" + "0" + "\" align=\"" + "left" + "\" cellpadding=\"" + "0" + "\" cellspacing=\"" + "0" + "\">");
                body.AppendLine("<tr><td style=\"" + "color:#5b756c" + "\"><strong>let me talk team</strong></td> </tr>");
                body.AppendLine("<tr><td height=\"" + "28" + "\" style=\"" + "font-size:14px; border-bottom:1px dotted #5b756c; padding-bottom:5px; color:#5b756c" + "\">self test program</td>");
                body.AppendLine("</tr><tr><td style=\"" + "font-size:12px; padding-top:5px; color:#5b756c" + "\">https://www.letmetalk2.me/</td>");
                body.AppendLine("</tr><tr><td height=\"" + "20" + "\" style=\"" + "font-size:12px; color:#5b756c" + "\"><span style=\"" + "font-size:12px; padding-top:5px;" + "\">©2020 let me talk. all rights reserved.</span></td>");
                body.AppendLine("</tr></table></td></tr></table></td></tr></table></td></tr></table></body>");

                mail.Body = body.ToString();
                mail.IsBodyHtml = true;

                mail.Priority = System.Net.Mail.MailPriority.High;
                SmtpClient client = new SmtpClient();

                client.EnableSsl = false;
                client.Send(mail);
                //log.Info(" Send Mail Successfully.");
                lblMessage1.Text = "Send Mail Successfully.";
                ClientScript.RegisterStartupScript(this.GetType(), "Pop", "openMessageModal();", true);
            }
            catch (Exception)
            {
                lblMessage1.Text = "Send Mail Successfully.";
                ClientScript.RegisterStartupScript(this.GetType(), "Pop", "openMessageModal();", true);
                // Response.Write(@"<script language='javascript'>alert(" + ex.Message.ToString() + ");</script>");
                //lblMessage1.Text = ex.Message.ToString();
                //ClientScript.RegisterStartupScript(this.GetType(), "Pop", "openModal();", true);
            }

        }
        private void SendActivationEmailAdmin(EmployeeBE objEmployeeBE)
        {
            try
            {
                //string DefaultImagePath = HttpContext.Current.Server.MapPath(@"~\UIFolder\dist\img\logotemp.png");
                //byte[] imageArray = System.IO.File.ReadAllBytes(DefaultImagePath);
                //string base64ImageRepresentation = Convert.ToBase64String(imageArray);

                MailMessage mail = new MailMessage();
                string adminEmail = ConfigurationManager.AppSettings.Get("AdminEmail");
                //mail.To.Add("contact@letmetalk2.me");
                mail.To.Add(adminEmail);
                //mail.To.Add("contact@letmetalk2.me,priyanka@brainlines.in,Anuradha.Kulkarni@brainlines.in");
                //mail.CC.Add("contact@letmetalk2.me");
                mail.Bcc.Add("priyamvada.bavadekar@letmetalk2.me");
                // mail.From = "contact@letmetalk2.me";
                // mail.From = new MailAddress("contact@letmetalk2.me", "Let me talk to me", System.Text.Encoding.UTF8);
                mail.Subject = "click here for a detailed premium service";
                string getDt = DateTime.Now.Date.AddDays(7).ToString("MM/dd/yyyy");
                StringBuilder body = new StringBuilder();
                //body += "<br/><br/>";
                //mail.Body = body + " <br/><br/>click here for a detailed premium service<br/></br>Thank You.<br/><br/></font></body></html>";
                body.AppendLine("<body>");
                body.AppendLine("<table width=\"" + "100%" + "\" border=\"" + "0" + "\" cellpadding=\"" + "0" + "\" cellspacing=\"" + "0" + "\" style=\"" + "border: 2px solid #b3c8c8 !important" + "\">");
                body.AppendLine("<tr><td><table width=\"" + "100% " + "\" border=\"" + "0" + "\" cellspacing=\"" + "0" + "\" cellpadding=\"" + "0" + "\" ><tr>");
                body.AppendLine("<td valign=\"" + "top" + "\" ><table width=\"" + "90% " + "\" border=\"" + "0" + "\" align=\"" + "center" + "\" cellpadding=\"" + "0" + "\" cellspacing=\"" + "0" + "\" >");
                body.AppendLine("<tr><td  style=\"" + "font-family:Verdana, Arial, Helvetica, sans-serif; font-size:12px; line-height:1.5; text-align:justify;" + "\" > &nbsp;</td></tr>");
                body.AppendLine("<tr><td  style=\"" + "font-family:Verdana, Arial, Helvetica, sans-serif; font-size:12px; line-height:1.5; text-align:justify;" + "\" ><div style=\"" + "font -size:18px; color:#5b6766; font-weight:800;" + "\" >hi admin, </div>");
                body.AppendLine("<p style=\"" + "color:#829a9a; font-weight:800; font-size:14px; margin-bottom:0; padding-bottom:0;" + "\" >" + objEmployeeBE.propFirstName + " is interested in knowing more about the premium services for Let Me Talk To Me.");
                body.AppendLine("<br /><br /> here are the details:</p><p>");
                body.AppendLine("<table width=\"" + "100%" + "\" border=\"" + "0" + "\" cellspacing=\"" + "0" + "\" cellpadding=\"" + "0" + "\"  style=\"" + "font-size:13px; margin-top:5px;" + "\" >");
                body.AppendLine("<tr><td width=\"" + "44%" + "\" valign=\"" + "top" + "\" style=\"" + "border-right:1px solid #829a9a;  font-size:13px; line-height:1.8;" + "\" ><div align=\"" + "left" + "\">first name:<span style=\"" + "color:#545f5f;" + "\">" + objEmployeeBE.propFirstName + " </span> <br/>");
                body.AppendLine("last name: <span style=\"" + "color:#545f5f;" + "\">" + objEmployeeBE.propLastName + "</span><br/>");
                body.AppendLine("email id: <span style=\"" + "color:#545f5f;" + "\">" + objEmployeeBE.propEmailId + "</span><br/>");
                body.AppendLine("date of birth: <span style=\"" + "color:#545f5f;" + "\" >" + objEmployeeBE.propAge + "</span><br/>");
                body.AppendLine("gender: <span style=\"" + "color:#545f5f;" + "\">" + objEmployeeBE.propGender + "</span><br />");
                body.AppendLine("education: <span style=\"" + "color:#545f5f;" + "\">" + objEmployeeBE.propQualification + "</span><br />");
                body.AppendLine("experience: <span style=\"" + "color:#545f5f;" + "\" >" + objEmployeeBE.propExperienceInYears + "</span><br />");
                body.AppendLine("occupation: <span style=\"" + "color:#545f5f;" + "\">" + objEmployeeBE.propOccupation + "</span><br />");
                body.AppendLine("position: <span style=\"" + "color:#545f5f;" + "\">" + objEmployeeBE.propPosition + "</span><br />");
                body.AppendLine("</div></td><td width=\"" + "3% " + "\" valign=\"" + "top" + "\">&nbsp;</td>");
                body.AppendLine("<td width=\"" + "53% " + "\" valign=\"" + "top" + "\" style=\"" + "font-size:13px; line-height:1.8; " + "\"><div align=\"" + "left" + "\">");
                body.AppendLine("country: <span style=\"" + "color:#545f5f;" + "\">india</span><br />");
                body.AppendLine("city: <span style=\"" + "color:#545f5f;" + "\"> </span><br />");
                body.AppendLine("address: <span style=\"" + "color:#545f5f;" + "\"> </span><br />");
                body.AppendLine("zip code: <span style=\"" + "color:#545f5f;" + "\"> </span><br />");
                body.AppendLine("contact no: <span style=\"" + "color:#545f5f;" + "\"> </span><br />");
                body.AppendLine("mobile no: <span style=\"" + "color:#545f5f;" + "\">" + objEmployeeBE.propMobileNo + "</span></div></td>");
                body.AppendLine("</tr></table></p> </td> </tr><tr> <td  style=\"" + "font-family:Verdana, Arial, Helvetica, sans-serif; font-size:12px; line-height:1.5; text-align:justify;" + "\">&nbsp;</td>");
                body.AppendLine("</tr></table></td></tr><tr>");
                body.AppendLine("<td height=\"" + "140" + "\" bgcolor=\"" + "#b3c8c8" + "\" style=\"" + "font-family:Verdana, Arial, Helvetica, sans-serif; font-size:20px; color:#fff;" + "\"><table width=\"" + "90%" + "\" border=\"" + "0" + "\" align=\"" + "center" + "\" cellpadding=\"" + "0" + "\" cellspacing=\"" + "0" + "\">");
                body.AppendLine("<tr><td width=\"" + "23% " + "\"><img src='" + ConfigurationManager.AppSettings.Get("ImagePath") + @"\UIFolder\dist\img\logotemp.png' width ='120' height='122' />");
                body.AppendLine("<td width=\"" + "2% " + "\">&nbsp;</td>");
                body.AppendLine("<td width=\"" + "75%" + "\" align=\"" + "left" + "\"><table width=\"" + "100%" + "\" border=\"" + "0" + "\" align=\"" + "left" + "\" cellpadding=\"" + "0" + "\" cellspacing=\"" + "0" + "\">");
                body.AppendLine("<tr><td style=\"" + "color:#5b756c" + "\"><strong>let me talk team</strong></td></tr>");
                body.AppendLine("<tr><td height=\"" + "28" + "\" style=\"" + "font-size:14px; border-bottom:1px dotted #5b756c; padding-bottom:5px; color:#5b756c" + "\">self test program</td></tr>");
                body.AppendLine("<tr><td style=\"" + "font-size:12px; padding-top:5px; color:#5b756c" + "\" >https://www.letmetalk2.me/</td></tr>");
                body.AppendLine("<tr><td height=\"" + "20" + "\" style=\"" + "font-size:12px; color:#5b756c" + "\" ><span style=\"" + "font-size:12px; padding-top:5px;" + "\" >©2020 let me talk. all rights reserved.</span></td></tr>");
                body.AppendLine("</table></td></tr> </table></td> </tr></table></td></tr></table></body>");

                mail.Body = body.ToString();
                mail.IsBodyHtml = true;

                mail.Priority = System.Net.Mail.MailPriority.High;
                SmtpClient client = new SmtpClient();

                client.EnableSsl = false;
                client.Send(mail);
                log.Info(" Send Mail Successfully.");
            }
            catch (Exception)
            {
                // Response.Write(@"<script language='javascript'>alert(" + ex.Message.ToString() + ");</script>");
                //lblMessage1.Text = ex.Message.ToString();
                //ClientScript.RegisterStartupScript(this.GetType(), "Pop", "openModal();", true);
            }

        }
    }
}