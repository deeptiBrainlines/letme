﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master/LetMeTalk.Master" AutoEventWireup="true" CodeBehind="PaymentGateway.aspx.cs" Inherits="HealToZeal.Employee.PaymentGateway1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
     <div style="clear:both;"></div>
   
        <section class="content">
            <div >
                <div class="col-md-11 col-centered">
                    <div class="col-md-12 boardp bdr3">
                        <div class="col-md-12 pad0" >
              <%-- style="border-color:#CCCCCC;border-width:1px;border-style:solid;"--%>
                <div class="col-md-12" >
                    <h4>Email verified Successfully</h4>

                    <p><b>Please proceed to pay the below Amount to get the Assessment passcode</b></p>

                    <div class="col-md-6">
                            <label><b>First Name:</b></label>
                        <asp:Label ID="lblFirstname" runat="server" Text="First Name"></asp:Label>
                       
                    </div>
                    <div class="col-md-6">
                         <label><b>Last Name:</b></label>
                        <asp:Label ID="lblLastname" runat="server" Text="Last Name"></asp:Label>
                        
                    </div>

                    <div class="col-md-6">
                         <label><b>Name Of Coupon:</b></label>
                        <asp:Label ID="lblname" runat="server" Text="Name Of Coupon"></asp:Label>
                        
                    </div>
                    <div class="col-md-6">
                     <label><b>Amount:</b></label>
                        <asp:Label ID="lblprice" runat="server" Text="Amount to be payable is"></asp:Label>
                    </div>

                    <div class="col-md-6">
                      <label><b>Percentage:</b></label>
                        <asp:Label ID="lblpercentage" runat="server" Text="Percentage"></asp:Label>
                    </div>
                    <div class="col-md-6">
                        <label><b>Email ID:</b></label>
                        <asp:Label ID="lblEmailid" runat="server" Text="Email ID"></asp:Label>
                    </div>
                    <div class="col-md-6">
                        <asp:Button ID="paymentgateway" runat="server" Text="Proceed to payment" CssClass="loginbtn mb20" OnClick="paymentgateway_Click" />

                    </div>
                </div>

                <div class="col-md-3">
                </div>
            </div>

        </div>
    </div>
   
    </div>
        </section>
</asp:Content>
