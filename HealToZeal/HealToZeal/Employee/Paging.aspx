﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Paging.aspx.cs" Inherits="HealToZeal.Employee.Paging" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
     <link rel="stylesheet" href="../css/main.css" media="screen" />

    <!--page title-->
    <link href="../Styles/FormControl.css" rel="stylesheet" />
</head>
<body>
    <form id="form1" runat="server">
        <div class="container">
            <div class="col-lg-12 col-md-12 col-sm-12">
                <div class="row">
                    <asp:DataList ID="DlistQuestions" runat="server" OnItemDataBound="DlistQuestions_ItemDataBound" Width="100%">
                        <ItemTemplate>
                            <div class="hs_comment">
                                <div class="row">
                                    <div class="col-lg-11 col-md-11 col-sm-10">
                                        <div class="hs_comment_date">
                                            <ul>
                                                <li></li>
                                                <li>
                                                    <asp:Label ID="lblQuestionId" runat="server" Text='<%#Eval("QuestionId_FK") %>' Visible="false"></asp:Label>
                                                </li>
                                            </ul>
                                            <p>
                                                <h4>
                                                    <asp:Label ID="lblSrNo" runat="server" Text='<%#Eval("SrNo")+". " %>'></asp:Label>
                                                    <a class="hs_in_relpy">
                                                        <asp:Label ID="lblQuestion" runat="server" Text='<%#Eval("Question") %>'></asp:Label>
                                                    </a>
                                                </h4>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="hs_sub_comment_div">
                                <div class="hs_sub_comment">
                                    <div class="row">
                                        <div class="col-lg-10 col-md-10 col-sm-10 col-lg-offset-2 col-md-offset-2 col-sm-offset-2">

                                            <div class="hs_comment">
                                                <div class="row">
                                                    <div class="col-lg-11 col-md-10 col-sm-10">
                                                        <%--  <i class="fa fa-paperclip"></i>--%>
                                                        <p>
                                                            <asp:RadioButtonList ID="rblistAnswers" runat="server" RepeatDirection="Vertical" CssClass="radio-inline">
                                                            </asp:RadioButtonList>
                                                        </p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                            <div class="hs_margin_40"></div>
                        </ItemTemplate>
                    </asp:DataList>
                </div>          
                <div class="row">
                    <asp:Repeater ID="rptPager" runat="server">
                        <ItemTemplate>
                            <asp:LinkButton ID="lnkPage" runat="server" Text='<%#Eval("Text") %>' CommandArgument='<%# Eval("Value") %>'
                                CssClass='<%# Convert.ToBoolean(Eval("Enabled")) ? "page_enabled" : "page_disabled" %>'
                                OnClick="Page_Changed" OnClientClick='<%# !Convert.ToBoolean(Eval("Enabled")) ? "return false;" : "" %>'></asp:LinkButton>
                        </ItemTemplate>
                    </asp:Repeater>
                </div>
            </div>           
        </div> 
         <!--main js file start-->
        <script type="text/javascript" src="../js/jquery-1.11.0.min.js"></script>
        <script type="text/javascript" src="../js/bootstrap.js"></script>
        <script type="text/javascript" src="../js/owl.carousel.js"></script>
        <script type="text/javascript" src="../js/jquery.bxslider.js"></script>
        <script type="text/javascript" src="../js/jquery.mixitup.min.js"></script>
        <script type="text/javascript" src="../js/smoothscroll.js"></script>
        <script type="text/javascript" src="../js/single-0.1.0.js"></script>
        <script type="text/javascript" src="../js/custom.js"></script>
        <!--main js file end-->
    </form>
</body>
</html>
