﻿using BE;
using DAL;
using Google.Apis.Gmail.v1;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Mail;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace HealToZeal.Employee
{
    public partial class EmployeeLogin2 : System.Web.UI.Page
    {
        log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        string CompanyId = "";
        EmployeeDAL objEmployeeDAL = new EmployeeDAL();
        string EmployeeId = "";
        string googleplus_client_id = string.Empty;
        string googleplus_client_secret = string.Empty;
        string googleplus_redirect_url = string.Empty;
        //string googleplus_client_id = "961545322562-359k8koggghm7n5jtgnb4hmvugtomfo3.apps.googleusercontent.com";
        //string googleplus_client_secret = "g1B83i1jfVBSn-jxMnqVjkS7";
        //string googleplus_redirect_url = "https://localhost:44352/Employee/EmployeeLogin.aspx";
        // string googleplus_redirect_url = "http://apps.brainlines.net/LMT2M_test/Employee/EmployeeLogin.aspx";
        string Parameters;
        Userclass userAllInfo = new Userclass();

        protected void Page_Load(object sender, EventArgs e)
        {
            googleplus_redirect_url = ConfigurationManager.AppSettings.Get("Loginurl");
            googleplus_client_id = ConfigurationManager.AppSettings.Get("googleplus_client_id");
            googleplus_client_secret = ConfigurationManager.AppSettings.Get("googleplus_client_secret");
            string de = Encryption.Decrypt("TZNgBLKIkBc6EEofyJUhRw==");
            if ((Session.Contents.Count > 0) && (Session["loginWith"] != null) && (Session["loginWith"].ToString() == "google" && Constant.UserFlagLoginOrSingup.ToString() == "signup"))
            {
                try
                {
                    var url = Request.Url.Query;
                    if (url != "")
                    {
                        string queryString = url.ToString();
                        char[] delimiterChars = { '=' };
                        string[] words = queryString.Split(delimiterChars);
                        string code = words[1];

                        if (code != null)
                        {
                            //get the access token 
                            HttpWebRequest webRequest = (HttpWebRequest)WebRequest.Create("https://accounts.google.com/o/oauth2/token");
                            webRequest.Method = "POST";
                            Parameters = "code=" + code + "&client_id=" + googleplus_client_id + "&client_secret=" + googleplus_client_secret + "&redirect_uri=" + googleplus_redirect_url + "&grant_type=authorization_code";
                            byte[] byteArray = Encoding.UTF8.GetBytes(Parameters);
                            webRequest.ContentType = "application/x-www-form-urlencoded";
                            webRequest.ContentLength = byteArray.Length;
                            Stream postStream = webRequest.GetRequestStream();
                            // Add the post data to the web request
                            postStream.Write(byteArray, 0, byteArray.Length);
                            postStream.Close();

                            WebResponse response = webRequest.GetResponse();
                            postStream = response.GetResponseStream();
                            StreamReader reader = new StreamReader(postStream);
                            string responseFromServer = reader.ReadToEnd();

                            GooglePlusAccessToken serStatus = JsonConvert.DeserializeObject<GooglePlusAccessToken>(responseFromServer);

                            if (serStatus != null)
                            {
                                string accessToken = string.Empty;
                                accessToken = serStatus.access_token;

                                if (!string.IsNullOrEmpty(accessToken))
                                {
                                    // This is where you want to add the code if login is successful.
                                    getgoogleplususerdataSer(accessToken);
                                }
                            }

                        }
                    }
                }
                catch (Exception ex)
                {
                    log.Error(ex.Message.ToString() + " " + ex.StackTrace.ToString());
                    // Response.Redirect("index.aspx");
                }
            }
            else
            {
                if ((Session.Contents.Count > 0) && (Session["SignWith"] != null) && (Session["SignWith"].ToString() == "google") && Constant.UserFlagLoginOrSingup.ToString() == "login")
                {
                    googleplus_redirect_url = ConfigurationManager.AppSettings.Get("Loginurl");
                    string googleplus_client_id = ConfigurationManager.AppSettings.Get("googleplus_client_id");
                    string googleplus_client_secret = ConfigurationManager.AppSettings.Get("googleplus_client_secret");
                    //string de = Encryption.Decrypt("TZNgBLKIkBc6EEofyJUhRw==");
                    try
                    {

                        var url = Request.Url.Query;
                        if (url != "")
                        {
                            string queryString = url.ToString();
                            char[] delimiterChars = { '=' };
                            string[] words = queryString.Split(delimiterChars);
                            string code = words[1];

                            if (code != null)
                            {
                                //get the access token 
                                HttpWebRequest webRequest = (HttpWebRequest)WebRequest.Create("https://accounts.google.com/o/oauth2/token");
                                webRequest.Method = "POST";
                                string Parameters = "code=" + code + "&client_id=" + googleplus_client_id + "&client_secret=" + googleplus_client_secret + "&redirect_uri=" + googleplus_redirect_url + "&grant_type=authorization_code";
                                byte[] byteArray = Encoding.UTF8.GetBytes(Parameters);
                                webRequest.ContentType = "application/x-www-form-urlencoded";
                                webRequest.ContentLength = byteArray.Length;
                                Stream postStream = webRequest.GetRequestStream();
                                // Add the post data to the web request
                                postStream.Write(byteArray, 0, byteArray.Length);
                                postStream.Close();

                                WebResponse response = webRequest.GetResponse();
                                postStream = response.GetResponseStream();
                                StreamReader reader = new StreamReader(postStream);
                                string responseFromServer = reader.ReadToEnd();

                                GooglePlusAccessToken serStatus = JsonConvert.DeserializeObject<GooglePlusAccessToken>(responseFromServer);

                                if (serStatus != null)
                                {
                                    string accessToken = string.Empty;
                                    accessToken = serStatus.access_token;

                                    if (!string.IsNullOrEmpty(accessToken))
                                    {
                                        // This is where you want to add the code if login is successful.
                                        getgoogleplususerdataSerSignIn(accessToken);
                                    }
                                }

                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        log.Error(ex.Message.ToString() + " " + ex.StackTrace.ToString());
                        // Response.Redirect("index.aspx");
                    }
                }
            }

            //if((Session.Contents.Count > 0)&& Session["FlagTest"]!=null && Session["UserName"] != null&& Session["Password"]!=null)
            //{
            //    if(Session["FlagTest"].ToString()== "free")
            //    {

            //        UserName.Value = Session["UserName"].ToString();
            //        Password.Attributes.Add("readonly", "readonly");
            //        Password.Value = Session["Password"].ToString();
            //    }
            //}
        }


        public class Userclass
        {
            public string id
            {
                get;
                set;
            }
            public string name
            {
                get;
                set;
            }
            public string given_name
            {
                get;
                set;
            }
            public string family_name
            {
                get;
                set;
            }
            public string link
            {
                get;
                set;
            }
            public string picture
            {
                get;
                set;
            }
            public string gender
            {
                get;
                set;
            }
            public string locale
            {
                get;
                set;
            }
            public string first_name
            {
                get;
                set;
            }
            public string last_name
            {
                get;
                set;
            }
            public String Emails { get; set; }

        }

        public class Email
        {
            public string Value { get; set; }
            public string Type { get; set; }
        }

        public void GetToken(string code)
        {
            //string poststring = "grant_type=authorization_code&code=" + code + "&client_id=" + clientid + "&client_secret=" + clientsecret + "&redirect_uri=" + redirection_url + "";
            //var request = (HttpWebRequest)WebRequest.Create(url);
            //request.ContentType = "application/x-www-form-urlencoded";
            //request.Method = "POST";
            //UTF8Encoding utfenc = new UTF8Encoding();
            //byte[] bytes = utfenc.GetBytes(poststring);
            //Stream outputstream = null;
            //try
            //{
            //    request.ContentLength = bytes.Length;
            //    outputstream = request.GetRequestStream();
            //    outputstream.Write(bytes, 0, bytes.Length);
            //}
            //catch (Exception ex) { }
            //try
            //{
            //    var response = (HttpWebResponse)request.GetResponse();
            //    var streamReader = new StreamReader(response.GetResponseStream());
            //    string responseFromServer = streamReader.ReadToEnd();
            //    JavaScriptSerializer js = new JavaScriptSerializer();
            //    Tokenclass obj = js.Deserialize<Tokenclass>(responseFromServer);
            //    GetuserProfile(obj.access_token);
            //}
            //catch (Exception ex)
            //{

            //}
        }

        protected void Login(object sender, EventArgs e)
        {
            // GoogleConnect.Authorize("profile", "email");
        }

        public void GetuserProfile(string accesstoken)
        {
            // string url = "https://www.googleapis.com/oauth2/v2/userinfo?alt=json&access_token=" + accesstoken + "";
            ////string url1 = "https://www.googleapis.com/oauth2/v2/userinfo?alt=json&access_token=" + accesstoken + "";


            //  imgprofile.ImageUrl = userAllInfo.picture;
            //  lblid.Text = userAllInfo.Emails;
            //  lblgender.Text = userAllInfo.gender;
            //  lbllocale.Text = userAllInfo.locale;
            //  lblname.Text = userAllInfo.name;
            ////  lblid.Text = userinfo.Emails.Find(email => email.Type == "account").Value;
            //  hylprofile.NavigateUrl = userAllInfo.link;

            String str = userAllInfo.name;

            char[] spearator = { ',', ' ' };

            // using the method 
            String[] strlist = str.Split(spearator,
               StringSplitOptions.RemoveEmptyEntries);

            foreach (String s in strlist)
            {
                // string Firstname = s;
                //string Lastname = s;
            }

            try
            {

                //  Guid EmployeeId_PK;
                //  Guid.TryParse("EmployeeId_PK", out EmployeeId_PK);
                //insert
                string password = Encryption.CreatePassword();
                string userName = Encryption.CreateUsername();
                string pass = Encryption.Encrypt(password);
                string verificationcode = Guid.NewGuid().ToString();
                EmployeeBE objEmployeeBE = new EmployeeBE();

                CompanyId = "987109F8-B639-4FE0-85F3-0FCFEFF63B5E";
                objEmployeeBE.propUserName = userName;
                objEmployeeBE.propPassword = pass;
                objEmployeeBE.propCompanyId_FK = CompanyId;
                objEmployeeBE.propFirstName = userAllInfo.given_name;
                objEmployeeBE.propLastName = userAllInfo.family_name;
                objEmployeeBE.propEmailId = userAllInfo.Emails;
                objEmployeeBE.PropBatchID = "372BA8D3-F52A-4EDA-9CA2-B5968010A96C";
                //objEmployeeBE.verificationcode = verificationcode;
                // Session["verificationcode"] = verificationcode;
                //  objEmployeeBE.propOccupation = ddloccupation.SelectedItem.Text;
                //objEmployeeBE.propExperienceInYears = txtexp.Text;

                int Status = 0;

                Status = objEmployeeDAL.EmployeesRegistrationGmailInsert(objEmployeeBE);

                System.Net.Mail.MailMessage mail = new System.Net.Mail.MailMessage();


                mail.To.Add(userAllInfo.Emails);
                mail.CC.Add("contact@letmetalk2.me");
                mail.Bcc.Add("priyamvada.bavadekar@letmetalk2.me");
                // mail.From = "contact@letmetalk2.me";
                // mail.From = new MailAddress("contact@letmetalk2.me", "Let me talk to me", System.Text.Encoding.UTF8);
                mail.Subject = "Welcome to the Health and Personality Insights and Actions Tool.";
                string getDt = DateTime.Now.Date.AddDays(7).ToString("MM/dd/yyyy");

                //mail.Body = "<html><body><font color=\"black\">Dear " + FirstName + " " + LastName + ",<br/><br/>Welcome to <b> Let Me Talk2 Me: A tool to quickly and discreetly assess your mindfulness.</b> You will be able to gain some useful insights into your own personality and learn about remedial actions in case they are needed. <b>'Let Me Talk2 Me'</b> will help you in self-discovery. It will lead on a path of self reflections while avoiding pitfalls of modern complex life and careers. <br/><br/>It is recommended that you use this tool periodically to monitor yourself and be aware of your health and personality parameters. <br/><br/> Please use the link below to login <br/><br/><a href='http://letmetalk2.me.hazel.arvixe.com/Employee/EmployeeLogin.aspx?first=1'> http://letmetalk2.me.hazel.arvixe.com/Employee/EmployeeLogin.aspx </a><br/>Your login details are as follows:<br/>UserId:<b>" + UserName + "</b><br/>Password:<b>" + Password + "</b><br/><br/>If you would like to take a break during assessment, please make sure you log out from the system or close the browser window for security purposes. Please do not leave the browser idle. You can always log back in whenever you like.<br/><br/>Please ensure that you complete this assessment on or before " + getDt + ". To get a maximum benefit, please repeat this exercise every 3 months and study the differentials.<br/> You will be notified for the same when the next assessment is due.<br/><br/>If you wish to join the ecosystem of mindfulness, please pre-register with us.<a href='http://letmetalk2.me/pre-register'>http://letmetalk2.me/pre-register</a></br>Thank You.<br/><br/></font></body></html>";

                mail.Body = "<html><body><font color=\"black\">Dear " + userAllInfo.given_name + " " + userAllInfo.family_name + ",<br/><br/>Welcome to <b> Let Me Talk To Me: A tool to quickly and discreetly assess your mindfulness.</b> You will be able to gain some useful insights into your own personality and learn about remedial actions in case they are needed. <b>'Let Me Talk2 Me'</b> will help you in self-discovery. It will lead on a path of self reflections while avoiding pitfalls of modern complex life and careers. <br/><br/>It is recommended that you use this tool periodically to monitor yourself and be aware of your health and personality parameters. <br/><br/> Please use the link below to login <br/><br/><a href='http://localhost:44352/Employee/EmployeeLogin.aspx?first=1'> http://localhost:44352/Employee/EmployeeLogin.aspx </a><br/>Your login details are as follows:<br/>UserId:<b>" + UserName + "</b><br/>Password:<b>" + Password + "</b><br/><br/>If you would like to take a break during assessment, please make sure you log out from the system or close the browser window for security purposes. Please do not leave the browser idle. You can always log back in whenever you like.<br/><br/>Please ensure that you complete this assessment on or before " + getDt + ". To get a maximum benefit, please repeat this exercise every 3 months and study the differentials.<br/> You will be notified for the same when the next assessment is due.<br/><br/>If you wish to join the ecosystem of mindfulness, please pre-register with us.<a href='http://letmetalk2.me/pre-register'>http://letmetalk2.me/pre-register</a></br>Thank You.<br/><br/></font></body></html>";

                mail.IsBodyHtml = true;

                mail.Priority = System.Net.Mail.MailPriority.High;
                SmtpClient client = new SmtpClient();
                // client.Credentials = new System.Net.NetworkCredential("contact@letmetalk2.me", "contact123");
                //  client.Port = 587;
                // client.Host = "dallas137.arvixeshared.com";
                client.EnableSsl = false;
                client.Send(mail);



                Response.Redirect("EmployeeLogin.aspx");
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('Registration successfull')", true);

                //lblMessage.Text = "Employee added successfully...";


            }
            catch (Exception ex)
            {
                ex.InnerException.ToString();
            }
        }
        public void freetest_Click()
        {
            EmployeeDAL objEmployeeDAL = new EmployeeDAL();
            EmployeeBE objEmployeeBE = new EmployeeBE();
            if (Session["EmployeeID"] != null)
            {
                try
                {
                    objEmployeeBE.propEmployeeId_PK = Convert.ToString(Session["EmployeeID"].ToString());
                    objEmployeeBE.propTestFlag = Convert.ToString("free");

                    //int UpdateTestFlag = objEmployeeDAL.UpdateTestFlag(objEmployeeBE);
                    //if (UpdateTestFlag >= 0)
                    //{
                    DataTable dt = objEmployeeDAL.GetEmployeebyFreeTestEmpid(objEmployeeBE);
                    //if (dt != null && dt.Rows.Count > 0)
                    //{
                    //    string AssessmentInstanceId_FreeTest = ConfigurationManager.AppSettings.Get("AssessmentInstanceId_FreeTest");
                    //    for (int j = 0; j < dt.Rows.Count; j++)
                    //    {
                    //        if (Convert.ToString(dt.Rows[j]["AssessmentInstanceId_FK"]).ToUpper() !=AssessmentInstanceId_FreeTest.ToString())
                    //        {
                    //            dt.Rows.Remove(dt.Rows[j]);
                    //        }
                    //    }
                    //}
                    if (dt != null && dt.Rows.Count > 0)
                    {
                        string FirstName = dt.Rows[0]["FirstName"].ToString();
                        string LastName = dt.Rows[0]["LastName"].ToString();
                        string Email = dt.Rows[0]["EmailId"].ToString();
                        string Password1 = dt.Rows[0]["Password"].ToString();
                        String Password = Encryption.Decrypt(Password1);
                        string UserName = dt.Rows[0]["UserName"].ToString();
                        // Session["CompanyId"] = dt.Rows[0]["CompanyID_FK"].ToString();
                        Session["UserName"] = dt.Rows[0]["UserName"].ToString();
                        Session["Password"] = Password;
                        Session["FlagTest"] = "free";
                        Session["EmployeeId"] = dt.Rows[0]["EmployeeId_PK"].ToString();
                        Session["EmployeeCompanyId"] = dt.Rows[0]["EmployeeCompanyId"].ToString();
                        Session["BatchID"] = dt.Rows[0]["BatchID_Fk"].ToString();
                        Session["AssessmentInstanceId"] = dt.Rows[0]["AssessmentInstanceId_FK"].ToString();
                        Session["AssessmentInstanceCompanyId"] = dt.Rows[0]["InstanceCompanyRelationId"].ToString();
                        Session["CompanyId"] = dt.Rows[0]["FreeTestCompanyId"].ToString();
                        Session["AssessmentId"] = dt.Rows[0]["AssessmentId_FK"].ToString();
                        Response.Redirect("EmployeeMessage.aspx", false);
                    }
                    //}
                    //else
                    //{
                    //    Response.Redirect("FirstLoginEmailVarifiedFreeTest.aspx", false);
                    //}
                }
                catch (Exception ex)
                {

                    log.Error(ex.ToString() + " " + ex.StackTrace.ToString());
                }

            }
            else
            {
                // Response.Redirect("EmployeeLogin.aspx");
            }
        }
        protected void btnLogin_Click(object sender, EventArgs e)
        {
            try
            {
                EmployeeDAL objEmployeeDAL = new EmployeeDAL();

                DataTable DtEmployeeDetails = objEmployeeDAL.EmployeesLogin(UserName.Value, Encryption.Encrypt(Password.Value));
                string pass = Encryption.Decrypt(Password.Value);
                string username = Encryption.Decrypt(UserName.Value);
                if (DtEmployeeDetails != null && DtEmployeeDetails.Rows.Count > 0)
                {
                    Session["UserFullName"]= DtEmployeeDetails.Rows[0]["UserName"].ToString();
                    Session["UserName"] = UserName.Value;
                    //Session["DtCompanyDetails"] = DtCompanyDetails;
                    Session["EmployeeId"] = DtEmployeeDetails.Rows[0]["EmployeeId_PK"].ToString();
                    Session["EmployeeCompanyId"] = DtEmployeeDetails.Rows[0]["EmployeeCompanyId"].ToString();
                    Session["BatchID"] = DtEmployeeDetails.Rows[0]["BatchID_Fk"].ToString();
                    //  Session["CompanyId"] = DtEmployeeDetails.Rows[0]["CompanyId_FK"].ToString();
                    //if ( Convert.ToBoolean( DtEmployeeDetails.Rows[0]["PaymentFlag"]))
                    //{
                    //    Session["FlagTest"] = "Paid";
                    //}

                    //if (Flag == "free")
                    //{
                    //    Session["FlagTest"] = Flag;
                    //    Response.Redirect("EmployeeAssessments.aspx", false);
                    //}
                    //else
                    //{
                 

                    if (Request.QueryString["first"] != null)
                        {
                            // Response.Redirect("EmployeeHome.aspx?first=1");
                            if (DtEmployeeDetails.Rows[0]["Age"].ToString() != "" && DtEmployeeDetails.Rows[0]["Occupation"].ToString() != "" && DtEmployeeDetails.Rows[0]["Gender"].ToString() != "" && DtEmployeeDetails.Rows[0]["ExperienceInYears"].ToString() != "" && DtEmployeeDetails.Rows[0]["Position"].ToString() != "" && DtEmployeeDetails.Rows[0]["Qualification"].ToString() != "")
                            {
                                EmployeeAssessmentsHistoryDAL objAssessmentHistory = new EmployeeAssessmentsHistoryDAL();
                                DataTable DtAssessmentHistory = objAssessmentHistory.EmployeeAssessmentsHistoryAssessmentSubmited(Session["EmployeeId"].ToString(), "");

                                if (DtAssessmentHistory != null && DtAssessmentHistory.Rows.Count > 0)
                                {
                                    Response.Redirect("EmployeeAssessments.aspx", false);
                                }
                                else
                                {
                                    //   Response.Redirect("Assessment.aspx");
                                    Response.Redirect("EmployeeAssessments.aspx", false);
                                    // Response.Redirect("EmployeeMessage.aspx?first=1");
                                    //Response.Redirect("Assessment.aspx");
                                }
                            }
                            else
                            {
                                if (DtEmployeeDetails.Rows[0]["TestFlag"] != null && Convert.ToString(DtEmployeeDetails.Rows[0]["TestFlag"]) == "free")
                                {

                                    Response.Redirect("EmployeeMessage.aspx?first=1", false);
                                freetest_Click();
                                }
                                else
                                {
                                    Response.Redirect("EmployeeMessagePaid.aspx?first=1", false);
                                }
                            }
                            // Response.Redirect("EmployeeMessage.aspx?first=1");
                        }
                        else
                        {
                            if (DtEmployeeDetails.Rows[0]["Age"].ToString() != "" && DtEmployeeDetails.Rows[0]["Occupation"].ToString() != "" && DtEmployeeDetails.Rows[0]["Gender"].ToString() != "" && DtEmployeeDetails.Rows[0]["ExperienceInYears"].ToString() != "" && DtEmployeeDetails.Rows[0]["Position"].ToString() != "" && DtEmployeeDetails.Rows[0]["Qualification"].ToString() != "")
                            {
                                EmployeeAssessmentsHistoryDAL objAssessmentHistory = new EmployeeAssessmentsHistoryDAL();
                                DataTable DtAssessmentHistory = objAssessmentHistory.EmployeeAssessmentsHistoryAssessmentSubmited(Session["EmployeeId"].ToString(), "");
                                if (DtAssessmentHistory != null && DtAssessmentHistory.Rows.Count > 0)
                                {
                                    Response.Redirect("EmployeeAssessments.aspx", false);
                                }
                                else
                                {
                                    if (Request.QueryString["first"] != null && Session["FlagTest"] != null && Session["FlagTest"].ToString() == "free")
                                    {
                                        Response.Redirect("EmployeeAssessments.aspx", false);
                                        
                                }
                                    else
                                    {
                                        //  Response.Redirect("Assessment.aspx");
                                        Response.Redirect("EmployeeAssessments.aspx", false);
                                    }
                                }
                            }
                            else
                            {
                                if (Request.QueryString["first"] != null && Session["FlagTest"] != null && Session["FlagTest"].ToString() == "free")
                                {
                                    Response.Redirect("EmployeeAssessments.aspx");
                                }
                                else if (DtEmployeeDetails.Rows[0]["TestFlag"] != null && Convert.ToString(DtEmployeeDetails.Rows[0]["TestFlag"]) == "free")
                                {

                                    Response.Redirect("EmployeeMessage.aspx?first=1", false);
                                freetest_Click();
                            }
                                else
                                {
                                    Response.Redirect("EmployeeMessagePaid.aspx?first=1", false);
                                }
                            }

                        }
                    }
               // }
                else
                {
                    message.InnerText = "Invalid username/password...";
                }
            }
            catch (Exception ex)
            {
                message.InnerText = ex.Message;
                ex.InnerException.ToString();
                log.Error(ex.Message.ToString() + " " + ex.StackTrace.ToString());
            }
           
        }

        protected void ImgBtnLogin_Click(object sender, ImageClickEventArgs e)
        {
            try
            {
                EmployeeDAL objEmployeeDAL = new EmployeeDAL();

                DataTable DtEmployeeDetails = objEmployeeDAL.EmployeesLogin(UserName.Value, Encryption.Encrypt(Password.Value));

                if (DtEmployeeDetails != null && DtEmployeeDetails.Rows.Count > 0)
                {
                    Session["UserName"] = UserName.Value;
                    //Session["DtCompanyDetails"] = DtCompanyDetails;
                    Session["EmployeeId"] = DtEmployeeDetails.Rows[0]["EmployeeId_PK"].ToString();
                    Session["EmployeeCompanyId"] = DtEmployeeDetails.Rows[0]["EmployeeCompanyId"].ToString();

                    if (Request.QueryString["first"] != null)
                    {
                        EmployeeAssessmentsHistoryDAL objAssessmentHistory = new EmployeeAssessmentsHistoryDAL();
                        DataTable DtAssessmentHistory = objAssessmentHistory.EmployeeAssessmentsHistoryAssessmentSubmited(Session["EmployeeId"].ToString(), "");

                        if (DtAssessmentHistory != null && DtAssessmentHistory.Rows.Count > 0)
                        {
                            Response.Redirect("UserResult.aspx");
                        }
                        else
                        {
                            Response.Redirect("Assessment.aspx");
                        }

                        Response.Redirect("EmployeeMessage.aspx?first=1");

                    }
                    else
                    {
                        //   Response.Redirect("EmployeeHome.aspx?first=1");
                        EmployeeAssessmentsHistoryDAL objAssessmentHistory = new EmployeeAssessmentsHistoryDAL();
                        DataTable DtAssessmentHistory = objAssessmentHistory.EmployeeAssessmentsHistoryAssessmentSubmited(Session["EmployeeId"].ToString(), "");
                        if (DtAssessmentHistory != null && DtAssessmentHistory.Rows.Count > 0)
                        {
                            Response.Redirect("UserResult.aspx");
                        }
                        else
                        {
                            Response.Redirect("Assessment.aspx");
                        }
                        // Response.Redirect("EmployeeMessage.aspx", false);
                    }
                }
                else
                {
                    message.InnerText = "Invalid username/password...";
                }
            }
            catch (Exception ex)
            {
                message.InnerText = ex.Message;
            }

        }

        static string[] Scopes = { GmailService.Scope.GmailReadonly };
        static string ApplicationName = "TestGmail";

        //public void GetData()
        //{
        //    UserCredential credential;
        //    var path = @"C:\Users\Shree\Downloads\credentials.json";
        //    using (var stream =
        //        new FileStream(path, FileMode.Open, FileAccess.Read))
        //    {
        //        // The file token.json stores the user's access and refresh tokens, and is created
        //        // automatically when the authorization flow completes for the first time.
        //        // string credPath = @"C:\Users\Shree\Downloads\credentials1.json";
        //        string credPath = System.Environment.GetFolderPath(
        //             System.Environment.SpecialFolder.Personal);
        //        credential = GoogleWebAuthorizationBroker.AuthorizeAsync(
        //            GoogleClientSecrets.Load(stream).Secrets,
        //            Scopes,
        //            "user",
        //            CancellationToken.None,
        //            new FileDataStore(credPath, true)).Result;
        //        Console.WriteLine("Credential file saved to: " + credPath);
        //    }

        //    // Create Gmail API service.
        //    var service = new GmailService(new BaseClientService.Initializer()
        //    {
        //        HttpClientInitializer = credential,
        //        ApplicationName = ApplicationName,
        //    });

        //    // Define parameters of request.
        //    UsersResource.LabelsResource.ListRequest request = service.Users.Labels.List("me");

        //    var strList = request.Execute().ToString();
        //    // IList<Label> labels = request
        //    //Console.WriteLine("Labels:");
        //    //if (labels != null && labels.Count > 0)
        //    //{
        //    //    foreach (var labelItem in labels)
        //    //    {
        //    //        Console.WriteLine("{0}", labelItem.Name);
        //    //    }
        //    //}
        //    //else
        //    //{
        //    //    Console.WriteLine("No labels found.");
        //    //}
        //    //Console.Read();
        //}


        private async void getgoogleplususerdataSer(string access_token)
        {
            try
            {
                HttpClient client = new HttpClient();
                var urlProfile = "https://www.googleapis.com/oauth2/v1/userinfo?alt=json&access_token=" + access_token;

                client.CancelPendingRequests();
                HttpResponseMessage output = await client.GetAsync(urlProfile);

                if (output.IsSuccessStatusCode)
                {
                    string outputData = await output.Content.ReadAsStringAsync();
                    GoogleUserOutputData serStatus = JsonConvert.DeserializeObject<GoogleUserOutputData>(outputData);

                    if (serStatus != null)
                    {
                        // You will get the user information here.
                        userAllInfo.id = serStatus.id;
                        userAllInfo.first_name = serStatus.given_name;
                        userAllInfo.last_name = serStatus.family_name;
                        userAllInfo.Emails = serStatus.email;
                        userAllInfo.locale = serStatus.locale;
                        userAllInfo.picture = serStatus.picture;
                        userAllInfo.name = serStatus.name;

                        String str = userAllInfo.name;

                        char[] spearator = { ',', ' ' };

                        // using the method 
                        String[] strlist = str.Split(spearator,
                           StringSplitOptions.RemoveEmptyEntries);

                        foreach (String s in strlist)
                        {
                            // string Firstname = s;
                            //string Lastname = s;
                        }

                        try
                        {

                            //  Guid EmployeeId_PK;
                            //  Guid.TryParse("EmployeeId_PK", out EmployeeId_PK);
                            //insert
                            string Password = Encryption.CreatePassword();
                            string UserName = userAllInfo.Emails;
                            string pass = Encryption.Encrypt(Password);
                            string verificationcode = Guid.NewGuid().ToString();
                            EmployeeBE objEmployeeBE = new EmployeeBE();

                            // CompanyId = "987109F8-B639-4FE0-85F3-0FCFEFF63B5E";
                            CompanyId = Session["CompanyId"].ToString();
                            string BatchID = Session["BatchID"].ToString();
                            objEmployeeBE.propUserName = UserName;
                            objEmployeeBE.propPassword = pass;
                            objEmployeeBE.propCompanyId_FK = Session["CompanyId"].ToString();
                            objEmployeeBE.propFirstName = userAllInfo.first_name;
                            objEmployeeBE.propLastName = userAllInfo.last_name;
                            objEmployeeBE.propEmailId = userAllInfo.Emails;
                            //objEmployeeBE.PropBatchID = "372BA8D3-F52A-4EDA-9CA2-B5968010A96C";
                            objEmployeeBE.PropBatchID = Session["BatchID"].ToString();
                            //objEmployeeBE.verificationcode = verificationcode;
                            // Session["verificationcode"] = verificationcode;
                            //  objEmployeeBE.propOccupation = ddloccupation.SelectedItem.Text;
                            //objEmployeeBE.propExperienceInYears = txtexp.Text;
                            Session["Email"] = userAllInfo.Emails;
                            Session["UserFullName"] = userAllInfo.first_name + " " + userAllInfo.last_name;

                            int Status = 0;

                            Status = objEmployeeDAL.EmployeesRegistrationGmailInsert(objEmployeeBE);

                            if (Status > 0)
                            {
                                // Response.Write(@"<script language='javascript'>alert('The following errors have occurred: \n" + "You have Registerd successfully, Please check Email for Assessment Password " + " .');</script>");
                                //System.Net.Mail.MailMessage mail = new System.Net.Mail.MailMessage();
                                //mail.To.Add(userAllInfo.Emails);
                                //mail.CC.Add("contact@letmetalk2.me");
                                //mail.Bcc.Add("priyamvada.bavadekar@letmetalk2.me");
                                //// mail.From = "contact@letmetalk2.me";
                                //// mail.From = new MailAddress("contact@letmetalk2.me", "Let me talk to me", System.Text.Encoding.UTF8);
                                //mail.Subject = "Welcome to the Health and Personality Insights and Actions Tool.";
                                //string getDt = DateTime.Now.Date.AddDays(7).ToString("MM/dd/yyyy");

                                ////mail.Body = "<html><body><font color=\"black\">Dear " + FirstName + " " + LastName + ",<br/><br/>Welcome to <b> Let Me Talk2 Me: A tool to quickly and discreetly assess your mindfulness.</b> You will be able to gain some useful insights into your own personality and learn about remedial actions in case they are needed. <b>'Let Me Talk2 Me'</b> will help you in self-discovery. It will lead on a path of self reflections while avoiding pitfalls of modern complex life and careers. <br/><br/>It is recommended that you use this tool periodically to monitor yourself and be aware of your health and personality parameters. <br/><br/> Please use the link below to login <br/><br/><a href='http://letmetalk2.me.hazel.arvixe.com/Employee/EmployeeLogin.aspx?first=1'> http://letmetalk2.me.hazel.arvixe.com/Employee/EmployeeLogin.aspx </a><br/>Your login details are as follows:<br/>UserId:<b>" + UserName + "</b><br/>Password:<b>" + Password + "</b><br/><br/>If you would like to take a break during assessment, please make sure you log out from the system or close the browser window for security purposes. Please do not leave the browser idle. You can always log back in whenever you like.<br/><br/>Please ensure that you complete this assessment on or before " + getDt + ". To get a maximum benefit, please repeat this exercise every 3 months and study the differentials.<br/> You will be notified for the same when the next assessment is due.<br/><br/>If you wish to join the ecosystem of mindfulness, please pre-register with us.<a href='http://letmetalk2.me/pre-register'>http://letmetalk2.me/pre-register</a></br>Thank You.<br/><br/></font></body></html>";

                                //mail.Body = "<html><body><font color=\"black\">Dear " + userAllInfo.given_name + " " + userAllInfo.family_name + ",<br/><br/>Welcome to <b> Let Me Talk2 Me: A tool to quickly and discreetly assess your mindfulness.</b> You will be able to gain some useful insights into your own personality and learn about remedial actions in case they are needed. <b>'Let Me Talk2 Me'</b> will help you in self-discovery. It will lead on a path of self reflections while avoiding pitfalls of modern complex life and careers. <br/><br/>It is recommended that you use this tool periodically to monitor yourself and be aware of your health and personality parameters. <br/><br/> Please use the link below to login <br/><br/><a href='http://apps.brainlines.net/LMT2M_test/Employee/EmployeeLogin.aspx?first=1'> http://apps.brainlines.net/LMT2M_test/Employee/EmployeeLogin.aspx </a><br/>Your login details are as follows:<br/>UserId:<b>" + UserName + "</b><br/>Password:<b>" + Password + "</b><br/><br/>If you would like to take a break during assessment, please make sure you log out from the system or close the browser window for security purposes. Please do not leave the browser idle. You can always log back in whenever you like.<br/><br/>Please ensure that you complete this assessment on or before " + getDt + ". To get a maximum benefit, please repeat this exercise every 3 months and study the differentials.<br/> You will be notified for the same when the next assessment is due.<br/><br/>If you wish to join the ecosystem of mindfulness, please pre-register with us.<a href='http://letmetalk2.me/pre-register'>http://letmetalk2.me/pre-register</a></br>Thank You.<br/><br/></font></body></html>";

                                //mail.IsBodyHtml = true;

                                //mail.Priority = System.Net.Mail.MailPriority.High;
                                //SmtpClient client1 = new SmtpClient();
                                //// client.Credentials = new System.Net.NetworkCredential("contact@letmetalk2.me", "contact123");
                                ////  client.Port = 587;
                                //// client.Host = "dallas137.arvixeshared.com";
                                //client1.EnableSsl = true;
                                //client1.Send(mail);



                                //Response.Redirect("EmployeeLogin.aspx");
                                //string message = "Please Pay the money to get passcode for Assessment";
                                //string script = "window.onload = function(){ alert('";
                                //script += message;
                                //script += "')};";
                                //ClientScript.RegisterStartupScript(this.GetType(), "SuccessMessage", script, true);
                                DataTable dt = objEmployeeDAL.GetEmployeeByEmail(objEmployeeBE);
                                Session["CompanyId"] = dt.Rows[0]["CompanyID_FK"].ToString();
                                Session["EmployeeID"] = dt.Rows[0]["EmployeeId_PK"].ToString();
                                Session["UserName"] = Convert.ToString(dt.Rows[0]["UserName"].ToString());
                                Session["EmployeeCompanyId"] = Convert.ToString(dt.Rows[0]["CompanyID_FK"].ToString());
                                Session["BatchID"] = Convert.ToString(dt.Rows[0]["BatchID_FK"].ToString());
                                Response.Redirect("FirstLoginEmailVerifiedFreeTest.aspx");
                            }
                            else
                            {
                                //  Response.Write(@"<script language='javascript'>alert('Alert: \n" + "EmailID Already Exists,Plese provide another Email ID" + " .');</script>");
                                //  Response.Redirect("EmployeeLogin.aspx");
                                lblMessage1.Text = "EmailID Already Exists,Please provide another Email ID";
                                ClientScript.RegisterStartupScript(this.GetType(), "Pop", "openModalLogin();", true);

                            }



                            // Response.Redirect("PaymentGateway.aspx");
                            //imgprofile.ImageUrl = userAllInfo.picture;
                            //lblid.Text = userAllInfo.Emails;
                            //lblgender.Text = userAllInfo.gender;
                            //lbllocale.Text = userAllInfo.locale;
                            //lblname.Text = userAllInfo.name;
                            ////  lblid.Text = userinfo.Emails.Find(email => email.Type == "account").Value;
                            //hylprofile.NavigateUrl = userAllInfo.link;


                        }
                        catch (Exception ex)
                        {
                            log.Error(ex.Message.ToString() + " " + ex.StackTrace.ToString());
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                log.Error(ex.Message.ToString() + " " + ex.StackTrace.ToString());
            }
        }

        protected void ImgBtnGmail_Click(object sender, ImageClickEventArgs e)
        {
            try
            {
                googleplus_redirect_url = ConfigurationManager.AppSettings.Get("Loginurl");
                googleplus_client_id = ConfigurationManager.AppSettings.Get("googleplus_client_id");
                googleplus_client_secret = ConfigurationManager.AppSettings.Get("googleplus_client_secret");
                var Googleurl = "https://accounts.google.com/o/oauth2/auth?response_type=code&redirect_uri=" + googleplus_redirect_url + "&scope=https://www.googleapis.com/auth/userinfo.email%20https://www.googleapis.com/auth/userinfo.profile&client_id=" + googleplus_client_id;
                Session["SignWith"] = "google";
                Constant.UserFlagLoginOrSingup = "login";
                Response.Redirect(Googleurl);
            }
            catch (Exception ex)
            {
                log.Error(ex.Message.ToString() + " " + ex.StackTrace.ToString());
            }
        }

        private async void getgoogleplususerdataSerSignIn(string access_token)
        {
            try
            {
                EmployeeDAL objEmployeeDAL = new EmployeeDAL();
                Userclass userAllInfo = new Userclass();
                HttpClient client = new HttpClient();
                //string CompanyId = "";
                var urlProfile = "https://www.googleapis.com/oauth2/v1/userinfo?alt=json&access_token=" + access_token;

                client.CancelPendingRequests();
                HttpResponseMessage output = await client.GetAsync(urlProfile);

                if (output.IsSuccessStatusCode)
                {
                    string outputData = await output.Content.ReadAsStringAsync();
                    GoogleUserOutputData serStatus = JsonConvert.DeserializeObject<GoogleUserOutputData>(outputData);

                    if (serStatus != null)
                    {
                        // You will get the user information here.
                        userAllInfo.id = serStatus.id;
                        userAllInfo.first_name = serStatus.given_name;
                        userAllInfo.last_name = serStatus.family_name;
                        userAllInfo.Emails = serStatus.email;
                        userAllInfo.locale = serStatus.locale;
                        userAllInfo.picture = serStatus.picture;
                        userAllInfo.name = serStatus.name;


                        try
                        {

                            string Password = Encryption.CreatePassword();
                            string UserName = userAllInfo.Emails;
                            string pass = Encryption.Encrypt(Password);
                            string verificationcode = Guid.NewGuid().ToString();
                            EmployeeBE objEmployeeBE = new EmployeeBE();

                            // CompanyId = "987109F8-B639-4FE0-85F3-0FCFEFF63B5E";
                            //CompanyId = Session["CompanyId"].ToString();
                            //string BatchID = Session["BatchID"].ToString();
                            objEmployeeBE.propUserName = UserName;
                            // objEmployeeBE.propPassword = pass;
                            //objEmployeeBE.propCompanyId_FK = Session["CompanyId"].ToString();
                            objEmployeeBE.propFirstName = userAllInfo.first_name;
                            objEmployeeBE.propLastName = userAllInfo.last_name;
                            objEmployeeBE.propEmailId = userAllInfo.Emails;
                            //objEmployeeBE.PropBatchID = "372BA8D3-F52A-4EDA-9CA2-B5968010A96C";
                            //  objEmployeeBE.PropBatchID = Session["BatchID"].ToString();
                            //objEmployeeBE.verificationcode = verificationcode;
                            // Session["verificationcode"] = verificationcode;
                            //  objEmployeeBE.propOccupation = ddloccupation.SelectedItem.Text;
                            //objEmployeeBE.propExperienceInYears = txtexp.Text;
                            Session["Email"] = userAllInfo.Emails;


                            DataTable dt = objEmployeeDAL.GetEmployeeByEmail(objEmployeeBE);

                            if (dt != null && dt.Rows.Count > 0 && dt.Rows[0]["UserName"].ToString() == objEmployeeBE.propEmailId.ToString())
                            {
                                Session["UserFullName"]= objEmployeeBE.propFirstName+" "+objEmployeeBE.propLastName;
                                Session["CompanyId"] = Convert.ToString(dt.Rows[0]["CompanyID_FK"].ToString());
                                Session["EmployeeID"] = Convert.ToString(dt.Rows[0]["EmployeeId_PK"].ToString());
                                Session["BatchID"] = Convert.ToString(dt.Rows[0]["BatchID_FK"].ToString());
                                Session["AssessmentInstanceId"] = "1E562A5C-B068-414E-AFE6-B9F58BB5BE77";
                                Session["AssessmentInstanceCompanyId"] = "3FB35DBD-00B3-4704-BA2C-BC7DC3F244AF";
                                Session["UserName"] = Convert.ToString(dt.Rows[0]["UserName"].ToString());
                                Session["EmployeeCompanyId"] = Convert.ToString(dt.Rows[0]["EmployeeCompanyId"].ToString());
                                Session["FlagTest"] = "free";
                                if (dt != null && dt.Rows.Count > 0 && dt.Rows[0]["Age"].ToString() == "")
                                {
                                    EmployeeAssessmentsHistoryDAL objAssessmentHistory = new EmployeeAssessmentsHistoryDAL();
                                    DataTable DtAssessmentHistory = objAssessmentHistory.EmployeeAssessmentsHistoryAssessmentSubmited(Session["EmployeeId"].ToString(), "");

                                    if (DtAssessmentHistory != null && DtAssessmentHistory.Rows.Count > 0)
                                    {
                                        Response.Redirect("UserResultReport.aspx");
                                    }
                                    else
                                    {
                                        if (Convert.ToString(dt.Rows[0]["TestFlag"]) == "")
                                        {
                                            Response.Redirect("FirstLoginEmailVerifiedFreeTest.aspx");
                                        }
                                        else
                                        {
                                            Response.Redirect("EmployeeMessage.aspx?first=1");
                                        }

                                    }
                                }
                                else
                                {
                                    Response.Redirect("EmployeeAssessments.aspx");
                                }
                            }
                            else
                            {
                                lblMessage1.Text = "Please Sign Up";
                                ClientScript.RegisterStartupScript(this.GetType(), "Pop", "openModalLogin();", true);
                                //Response.Redirect("EmpRegistrationForm.aspx?InstanceCompanyRelationId=3FB35DBD-00B3-4704-BA2C-BC7DC3F244AF&CouponId=1");
                            }


                        }
                        catch (Exception e)
                        {
                            log.Error(e.Message.ToString() + " " + e.StackTrace.ToString());
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                log.Error(ex.Message.ToString() + " " + ex.StackTrace.ToString());
            }
        }
    }
}