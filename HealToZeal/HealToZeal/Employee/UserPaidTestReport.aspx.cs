﻿using DAL;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace HealToZeal.Employee
{
    public partial class UserPaidTestReport : System.Web.UI.Page
    {
        public static List<Cluster> list = new List<Cluster>();

        protected void Page_Load(object sender, EventArgs e)
        {
            Session["Heading"] = "report";
            if (!IsPostBack)
            {
                if (Session["EmployeeId"] != null && Session["AssessmentInstanceId"] != null && Session["AssessmentInstanceCompanyId"] != null)
                {
                    BindReportData();
                }
                else
                {
                    Session.Abandon();
                    Response.Redirect("EmployeeLogin.aspx", false);
                }
            }
        }
        public void BindReportData()
        {
            try
            {
                DataTable Dt = new DataTable();
                EmployeeDAL objEmployeeDAL = new EmployeeDAL();
                // Session["EmployeeId"] = "0B54B7E9-EB94-4922-AEFA-45944CAA480B";
                DataTable DtEmployeeDetails = objEmployeeDAL.EmployeeAssessmentSubmittedGetByEmployeeId(Session["EmployeeId"].ToString());
                for (int i = 0; i < DtEmployeeDetails.Rows.Count; i++)
                {
                    if (i == 0)
                    {

                        //Session["AssessmentInstanceId"] = "db1824dc-c639-4c70-a459-1f281ea89a50";
                        // Session["AssessmentInstanceCompanyId"] = "141c5cfb-4666-4576-b5c9-fada9522dd2a";
                        Dt = objEmployeeDAL.EmployeeResultGetForEmployee(Session["EmployeeId"].ToString(), Session["AssessmentInstanceId"].ToString(), Session["AssessmentInstanceCompanyId"].ToString());
                        foreach (DataRow dr in Dt.Rows)
                        {
                            if (dr["DisplayResultText"] != null && dr["DisplayResultText"].ToString() != "")
                            {
                                Cluster cluster = new Cluster();
                                cluster.ClusterName = Convert.ToString(dr["ClusterName"]);
                                cluster.ClusterDesc = Convert.ToString(dr["DisplayResultText"]);
                                cluster.Color = Convert.ToString(dr["Color"]);
                                cluster.Id= "#"+Convert.ToString(dr["ClusterName"]).Replace(" ", "");
                                if (!list.AsEnumerable().Where(row => row.ClusterName == cluster.ClusterName).Any())
                                {
                                    list.Add(cluster);
                                }
                            }
                        }

                        StringBuilder str = new StringBuilder();
                        for (int j = 0; j < list.Count; j++)
                        {
                            if (j%2 != 0)
                            {
                                str.Append("<div class='boardp mb20 whitebg boxshadow' id='"+ list[j].ClusterName.Replace(" ", "") + "'>");
                            }
                            else
                            {
                                str.Append("<div class='col-md-12' id='"+ list[j].ClusterName.Replace(" ", "") + "'>");
                            }
                            str.Append("<div class='mb20' style='margin-top:20px;'>");
                            str.Append(" <div class='col-md-6 col-sm-6'>");
                            str.Append("<div style='margin-top:0 !important;'>");
                            str.Append("<div>");
                            str.Append("<h3 class='w600 mt0 mb0'>" + list[j].ClusterName + "</h3>");
                            str.Append("<div class='col-md-11 pad0 mt0'>");
                            str.Append("<h3 style='line-height:35px; font-weight:100;'>");
                            str.Append("<ul>");
                            str.Append("<li style='margin-bottom:20px;'>" + list[j].ClusterDesc + " </li>");
                            str.Append("</ul>");
                            str.Append("</h3>");
                            str.Append("</div>");
                            str.Append("</div>");
                            str.Append("<div class='clearfix'></div>");
                            str.Append("</div>");
                            str.Append("</div>");
                            str.Append("<div class='col-md-6 col-sm-6'>");
                            str.Append("<div class='col-md-12'> <img src='../UIFolder/dist/img/" + list[j].ClusterName.ToString().TrimStart() + ".png' class='img-responsive mt30'> </div>");
                            str.Append("</div>");
                            str.Append("<div class='clearfix'></div>");
                            str.Append("</div>");
                            str.Append("</div>");
                            str.Append("<div class='clearfix'></div>");
                        }
                        divContainer.InnerHtml = str.ToString();
                        //lblDisplayText.Text = Convert.ToString(Dt.Rows[0]["DisplayResultText"]);
                        //lblHeading.Text= Convert.ToString(Dt.Rows[0]["ClusterName"]);
                        //Image1.ImageUrl = @"../UIFolder/dist/img/" + Convert.ToString(Dt.Rows[0]["ClusterName"]).TrimStart() + ".png";
                        //lblColor.BackColor= System.Drawing.Color.FromName((string)Convert.ToString(Dt.Rows[0]["Color"]));
                    }

                }

                DataTable DtClusterPercentage = new DataTable();
                EmployeeAssessmentsHistoryDAL objEmployeeDAL1 = new EmployeeAssessmentsHistoryDAL();
                DtClusterPercentage = objEmployeeDAL1.EmployeeHistory(Session["EmployeeId"].ToString());
                ddlCluster.DataSource = list;
                ddlCluster.DataTextField = "ClusterName";
                ddlCluster.DataValueField = "Id";
                ddlCluster.DataBind();
            }

            catch (Exception)
            {
                //    //ex.InnerException.ToString();
                //log.Error(ex.Message.ToString() + " " + ex.StackTrace.ToString());
            }

        }

        protected void ddlCluster_SelectedIndexChanged(object sender, EventArgs e)
        {
            //string clusterName = ddlCluster.SelectedItem.Text;
            //var clusterDetails = list.AsEnumerable().Where(row => row.ClusterName == clusterName).FirstOrDefault();
            //lblDisplayText.Text = clusterDetails.ClusterDesc.ToString();
            //lblHeading.Text = clusterDetails.ClusterName.ToString();
            //Image1.ImageUrl= @"../UIFolder/dist/img/"+ clusterName.TrimStart() + ".png";
            //lblColor.BackColor = System.Drawing.Color.FromName((string)Convert.ToString(clusterDetails.Color));
        }
    }
    public class Cluster
    {
        public string ClusterName { get; set; }
        public string ClusterDesc { get; set; }
        public string Color { get; set; }
        public string Id { get;  set; }
    }
}