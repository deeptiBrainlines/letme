﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master/LetMeTalk.Master" AutoEventWireup="true" CodeBehind="EmployeeMessage.aspx.cs" Inherits="HealToZeal.Employee.EmployeeMessage1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
     <div style="clear:both;"></div>
   
        <section class="content">
            <div >
                <div class="col-md-11 col-centered">
                    <div class="col-md-12 boardp bdr3">
                        <div class="col-md-12 pad0" >
                   <%-- <div class="text-left heading" style="padding-top:0; margin-top:0;">instructions</div>--%>
                        </div>
                        <%--<div class="scrollbar" id="style-3">--%>
                        <p>
                            This is a self report instrument designed to emotional states of mind at any given point of time.
The DASS questionnaire is available in the public domain. This was developed by Lovibond.
Several studies are published on its reliability and validity worldwide, all showing the DASS-21 is a well-established instrument to measure symptoms of emotional health of general population.<br>
                            <br>
                            Unlike, the standard assessment above, Let Me Talk To Me assessments are more specific to the target auidence which relates to their day to day challenges and expereinces. This makes them different from the assessments which are available in public domain.<br>
                            <br>
                            Continue to Free Test
                            <br>
                            <span class="w600">OR</span><br>
                            Contact us for further information on Let Me Talk To Me.
                        </p>
                        <div class="form-group">
                            <div class="form-check">
                                <asp:Button ID="btnNext" runat="server" class="loginbtn pull-right" Text="continue" OnClick="btnNext_Click" />
                                <%--<a href="home.html" class="loginbtn pull-right">accept & continue</a>--%>
                            </div>
                        </div>
                        <div class="force-overflow"></div>
                        <%--</div>--%>

                        <div class="clearfix"></div>

                    </div>
                </div>
            </div>
        </section>
    <div class="clearfix"></div>
</asp:Content>
