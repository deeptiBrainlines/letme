﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BE;
using System.Data.SqlClient;
using System.Data;
namespace DAL
{
  public class EmployeeAssessmentsHistoryDAL
    {
      DBClass objDBClass = new DBClass();
      public int EmployeeAssessmentsHistoryInsert(EmployeeAssessmentsHistoryBE objEmployeeAssessmentsHistoryBE)
      {
          int Status=0;
          try
          {
              SqlParameter[] sqlparam = 
            {
                    new SqlParameter ("@EmployeeId_FK",objEmployeeAssessmentsHistoryBE.propEmployeeId_FK),
                    new SqlParameter ("@AssessmentSubmitted",objEmployeeAssessmentsHistoryBE.propAssessmentSubmitted),
                    new SqlParameter("@CreatedBy",objEmployeeAssessmentsHistoryBE.propCreatedBy),
                  //  new SqlParameter("@batchid",objEmployeeAssessmentsHistoryBE.batchid),
                       new SqlParameter("@AssessmentInstanceId_FK",objEmployeeAssessmentsHistoryBE.propAssessmentInstanceId_FK),
                 //new SqlParameter ("@AssessmentId",objEmployeeAssessmentsHistoryBE.propAssessmentId_FK),

                
                
              // new SqlParameter("@Score",objEmployeeAssessmentsHistoryBE.propScore),
                 
              //  new SqlParameter("@CreatedDate",objEmployeeAssessmentsHistoryBE.propCreatedDate),
                  
                 new SqlParameter("@InstanceCompanyRelationId_FK",objEmployeeAssessmentsHistoryBE.propInstanceCompanyRelationId_FK),
                     

                      //new SqlParameter ("@EmployeeCompanyID", objEmployeeAssessmentsHistoryBE.propEmployeeCompanyID),
                      
            };
               // try
               // {
                    Status = objDBClass.ExecuteProc(EmployeeAssessmentsHistoryBE.SpEmployeeAssessmentsHistoryInserttest, sqlparam);
               // }
                //catch(Exception ex)
                //{
                //    ex.InnerException.ToString();
                //}
          }
            catch (Exception ex)
            {
                ex.InnerException.ToString();
            }
            return Status;
      }

      public DataTable EmployeeAssessmentsHistoryAssessmentSubmited(string EmployeeId_FK, string AssessmentId_FK)
      {
          DataTable DtAssessment = new DataTable();
          try
          {
              SqlParameter[] sqlparam =
                {
                    new SqlParameter("@EmployeeId_FK",EmployeeId_FK),
                  //  new SqlParameter("@AssessmentId_FK",AssessmentId_FK),
                };

              DtAssessment = objDBClass.ExecuteProc(EmployeeAssessmentsHistoryBE.SpEmployeeAssessmentsHistoryAssessmentSubmited, sqlparam, "EmployeeAssessmentsHistory");
          }
          catch
          {

          }
          return DtAssessment;
      }

      public DataTable AssessmentHistoryAllGet(string EmployeeId_FK, string AssessmentId_FK)
      {
          DataTable DtAssessment = new DataTable();
          try
          {
              SqlParameter[] sqlparam =
                {
                    new SqlParameter("@EmployeeId_FK",EmployeeId_FK),
                    new SqlParameter("@AssessmentId_FK",AssessmentId_FK),
                };

              DtAssessment = objDBClass.ExecuteProc(EmployeeAssessmentsHistoryBE.SpEmployeeAssessmentsHistoryAssessmentSubmited, sqlparam, "EmployeeAssessmentsHistory");
          }
          catch
          {

          }
          return DtAssessment;
      }

      public DataTable EmployeeAssessmentHistoryGetEmployees(string CompanyId)
      {
          DataTable DtAssessment = new DataTable();
          try
          {
              SqlParameter[] sqlparam =
                {
                    new SqlParameter("@CompanyId",CompanyId),
                };

              DtAssessment = objDBClass.ExecuteProc(EmployeeAssessmentsHistoryBE.SpEmployeeAssessmentHistoryGetEmployees, sqlparam, "EmployeeAssessmentsHistory");
          }
          catch
          {

          }
          return DtAssessment;
      }

      public DataTable EmployeeAssessmentResult()
      {
          DataTable DtResult = new DataTable();
          try
          {
              DtResult = objDBClass.ExecuteProc("SpEmployeeAssessmentResult");
          }
          catch
          {

          }
          return DtResult;
      }


      public DataTable EmployeeHistory(string EmployeeID)
      {
          DataTable DtAssessment = new DataTable();
          try
          {
              SqlParameter[] sqlparam =
                {
                    new SqlParameter("@EmployeeId",EmployeeID),
                };

              DtAssessment = objDBClass.ExecuteProc("Sp_EmployeeHistory", sqlparam, "EmployeeAssessmentsHistory");
          }
          catch
          {

          }
          return DtAssessment;
      }
      public DataTable AnxietyAssessmentResult(string ClusterName)
      {
          DataTable DtAssessment = new DataTable();
          try
          {
              SqlParameter[] sqlparam =
                {
                    new SqlParameter("@ClusterName",ClusterName),
                };

              DtAssessment = objDBClass.ExecuteProc("SpAnxietyAssessmentResult", sqlparam, "EmployeeAssessmentsHistory");
          }
          catch
          {

          }
          return DtAssessment;
      }

      public DataTable EmployeeAssementsGet(string EmployeeId,string BatchID)
      {
          DataTable DtAssessment = new DataTable();
          try
          {
              SqlParameter[] sqlparam =
                {
                    new SqlParameter("@EmployeeId",EmployeeId),
                    new SqlParameter("@BatchId",BatchID),
                };

              DtAssessment = objDBClass.ExecuteProc(EmployeeAssessmentsHistoryBE.SpEmployeeAssementsGet, sqlparam, "EmployeeAssessmentsHistory");
          }
          catch
          {

          }
          return DtAssessment;
      }
    }
}
