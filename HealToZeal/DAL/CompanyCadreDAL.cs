﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using BE;
using System.Data.SqlClient;


namespace DAL
{
   public class CompanyCadreDAL
    {
       DBClass objDBClass = new DBClass();

       public int CompanyCadreInsert(CompanyCadreBE objCompanyCadreBE)

       {
           int Status = 0;
           try
           {
               SqlParameter[] sqlparam = 
            {                
                 new SqlParameter ("@CompanyId_FK", objCompanyCadreBE.propCompanyId_FK),
                
                 new SqlParameter ("@CadreName", objCompanyCadreBE.propCadreName),                 
                 new SqlParameter("@CreatedBy",objCompanyCadreBE.propCreatedBy),
                 new SqlParameter("@CreatedDate",objCompanyCadreBE.propCreatedDate),
                
            };
               Status = objDBClass.ExecuteProc(CompanyCadreBE.SpCadreInsert, sqlparam);
           }
           catch
           {

           }
           return Status;
       }

       public DataTable CompanyCadreGetByCompanyId(string CompanyId)
       {
           DataTable DtCadre = new DataTable();
           try
           {
               SqlParameter[] sqlparam =
                {
                    new SqlParameter("@CompanyId_FK",CompanyId ),
                };

               DtCadre = objDBClass.ExecuteProc(CompanyCadreBE.SpCompanyCadreGetByCompanyId, sqlparam, "CompanyEmployee");
           }
           catch
           {

           }
           return DtCadre;
       }


       public DataTable GetAllCadrevalue(string CompanyId)
       {
           DataTable DtCadre = new DataTable();
           try
           {
               SqlParameter[] sqlparam =
                {
                    new SqlParameter("@CompanyID",CompanyId ),
                };

               DtCadre = objDBClass.ExecuteProc(CompanyCadreBE.SpGetAllCadrevalue, sqlparam, "CompanyEmployee");
           }
           catch
           {

           }
           return DtCadre;
       }
       public DataTable CompanyCadreGetByCadreId(string CadreId)
       {
           DataTable DtCadre = new DataTable();
           try
           {
               SqlParameter[] sqlparam =
                {
                    new SqlParameter("@CadreId",CadreId ),
                };

               DtCadre = objDBClass.ExecuteProc(CompanyCadreBE.SpCadreGetByCadreId, sqlparam, "CompanyCadre");
           }
           catch
           {

           }
           return DtCadre;
       }


       public DataTable DeleteCadre(string CadreId)
       {
           DataTable DtCadre = new DataTable();
           try
           {
               SqlParameter[] sqlparam =
                {
                    new SqlParameter("@CadreID",CadreId ),
                };

               DtCadre = objDBClass.ExecuteProc(CompanyCadreBE.spDeleteCadre, sqlparam, "CompanyCadre");
           }
           catch
           {

           }
           return DtCadre;
       }
       public int CadreUpdate(CompanyCadreBE objCompanyCadreBE)
       {
           int Status = 0;
           try
           {
               SqlParameter[] sqlparam = 
            {                
                 new SqlParameter ("@CompanyId_FK", objCompanyCadreBE.propCompanyId_FK),
                 new SqlParameter ("@CadreId_PK", objCompanyCadreBE.propCadreId_PK),
                 new SqlParameter("@CadreName",objCompanyCadreBE.propCadreName ),                
                 new SqlParameter("@CreatedBy",objCompanyCadreBE.propCreatedBy),
                 new SqlParameter("@CreatedDate",objCompanyCadreBE.propCreatedDate),

               //  new SqlParameter("@IsActive",objAnswerMasterBE.propIsActive),
            };
               Status = objDBClass.ExecuteProc(CompanyCadreBE.SpCadreUpdate, sqlparam);
           }
           catch
           {
                
           }
           return Status;
       }

       public int CadreDelete(string CadreId, string CompanyId)
       {
           int status = 0;
           try
           {
               SqlParameter[] sqlparam =
                {
                    new SqlParameter("@CadreId",CadreId ),
                      new SqlParameter("@CompanyId",CompanyId ),
                };

               status = objDBClass.ExecuteProc(CompanyCadreBE.SpCadreDelete, sqlparam);
           }
           catch
           {

           }
           return status;
       }

       public DataTable CadreGetByName(string Name, string CompanyId)
       {
           DataTable DtCadre = new DataTable();
           try
           {
               SqlParameter[] sqlparam =
                {
                    new SqlParameter("@Name",Name),
                    new SqlParameter("@CompanyId",CompanyId),
                };

               DtCadre = objDBClass.ExecuteProc(CompanyCadreBE.SpCadreGetByName, sqlparam, "Cadre");
           }
           catch
           {

           }
           return DtCadre;
       }
    }
}
