﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
namespace DAL
{
   public  class DBClass
    {/// <summary>
        /// Connection object for DbClass
        /// </summary>
        protected SqlConnection DbCon;

        /// <summary>
        /// Connection string for DbClass
        /// </summary>
        private string DbConString;

        /// <summary>
        /// Returns the database connection string
        /// </summary>
        protected string GetConString
        {
            get
            {
                return ConfigurationSettings.AppSettings["ConnectionString"].ToString();
            }
        }

        /// <summary>
        /// Initialized connection object with default connection string
        /// </summary>
        public DBClass()
        {
            DbCon = new SqlConnection(GetConString);
        }

        ~DBClass()
        {
            DbCon = null;
        }

        /// <summary>
        /// Initialized connection object with specified connection string
        /// </summary>
        public DBClass(string newConString)
        {
            DbConString = newConString;
            DbCon = new SqlConnection(DbConString);
        }

        /// <summary>
        /// Initializes build of command object
        /// </summary>
        /// <param name="storedProcName">Stored procedure name</param>
        /// <param name="parameters">SQL Parameter list</param>
        /// <returns>Returns integer value which can be outputted from stored procedure</returns>
        public SqlCommand BuildIntCommand(string storedProcName, IDataParameter[] parameters)
        {
            SqlCommand command = BuildQueryCommand(storedProcName, parameters);
            command.CommandTimeout = 1200;
            command.Parameters.Add(new SqlParameter("ReturnValue",
               SqlDbType.Int,
               4, /* Size */
               ParameterDirection.ReturnValue,
               false, /* is nullable */
               0, /* byte precision */
               0, /* byte scale */
               string.Empty,
               DataRowVersion.Default,
               null));

            return command;

        }

        /// <summary>
        /// Builds the command object with the specified parameters
        /// </summary>
        /// <param name="storedProcName">Stored procedure name</param>
        /// <param name="parameters">SQL Parameter list</param>
        /// <returns>Returns the built command object</returns>
        public SqlCommand BuildQueryCommand(string storedProcName, IDataParameter[] parameters)
        {
            SqlCommand command = new SqlCommand(storedProcName, DbCon);
            command.CommandTimeout = 1200;
            command.CommandType = CommandType.StoredProcedure;

            foreach (SqlParameter parameter in parameters)
            {
                command.Parameters.Add(parameter);
            }

            return command;
        }

        /// <summary>
        /// Sets the commands object's command type property to stored procedure
        /// </summary>
        /// <param name="storedProcName">Stored procedure name</param>
        /// <returns>Returns the built command object</returns>
        public SqlCommand BuildQueryCommand(string storedProcName)
        {
            SqlCommand command = new SqlCommand(storedProcName, DbCon);
            command.CommandTimeout = 1200;
            command.CommandType = CommandType.StoredProcedure;

            return command;
        }

        /// <summary>
        /// Executes supplied sql and returns integer scalar value
        /// </summary>
        /// <param name="storedProcName">Stored procedure name</param>
        /// <returns>int</returns>
        public int ExecuteStat(string sqlStatement)
        {
            int result;

            DbCon.Open();
            SqlCommand command = new SqlCommand();
            command.Connection = DbCon;
            command.CommandType = CommandType.Text;
            command.CommandTimeout = 1200;
            command.CommandText = sqlStatement;

            try
            {
                result = (int)command.ExecuteScalar();
            }
            catch
            {
                result = 0;
            }

            DbCon.Close();

            return result;
        }

        public string ExecuteStatStaString(string sqlStatement)
        {
            string result;

            DbCon.Open();
            SqlCommand command = new SqlCommand();
            command.Connection = DbCon;
            command.CommandType = CommandType.Text;
            command.CommandText = sqlStatement;
            command.CommandTimeout = 1200;
            try
            {
                result = Convert.ToString(command.ExecuteScalar());
            }
            catch
            {
                result = "";
            }

            DbCon.Close();

            return result;
        }

        public int executeMyQuery(string sqlStatement)
        {
            int result;

            DbCon.Open();
            SqlCommand command = new SqlCommand();
            command.Connection = DbCon;
            command.CommandType = CommandType.Text;
            command.CommandText = sqlStatement;
            command.CommandTimeout = 1200;
            try
            {
                result = (int)command.ExecuteScalar();
            }
            catch
            {
                result = 5;
            }

            DbCon.Close();

            return result;
        }

        public DataTable ExecuteQuery(string sqlStatement, string tableName)
        {
            DataTable dt = new DataTable(tableName);

            try
            {
                DbCon.Open();
                SqlCommand command = new SqlCommand();
                command.Connection = DbCon;
                command.CommandType = CommandType.Text;
                command.CommandText = sqlStatement;
                command.CommandTimeout = 1200;
                SqlDataAdapter da = new SqlDataAdapter(command);
                da.Fill(dt);
            }
            catch 
            {
            }    
       
            DbCon.Close();
            //Connection.Close();
            return dt;
        }

        /// <summary>
        /// Executes supplied sql
        /// </summary>
        /// <param name="storedProcName">Stored procedure name</param>
        /// <returns>DataTable</returns>
        public DataTable ExecuteStat(string sqlStatement, string tableName)
        {
            DataTable dt = new DataTable(tableName);
            try
            {
                DbCon.Open();
                SqlCommand command = new SqlCommand();
                command.Connection = DbCon;
                command.CommandType = CommandType.Text;
                command.CommandText = sqlStatement;
                command.CommandTimeout = 1200;
                SqlDataAdapter da = new SqlDataAdapter(command);
                da.Fill(dt);

            }
            catch
            {
            }
            DbCon.Close();
            //Connection.Close();
            return dt;
        }

        /// <summary>
        /// Executes the procedure and returns integer scalar value
        /// </summary>
        /// <param name="storedProcName">Stored procedure name</param>
        /// <param name="parameters">Parameter list</param>
        /// <returns>Scalar data</returns>
        public int ExecuteProc(string storedProcName, IDataParameter[] parameters)
        {
            int result = 0;
            DbCon.Open();
            SqlCommand command = BuildIntCommand(storedProcName, parameters);

            try
            {
                command.CommandTimeout = 1200;
                result = command.ExecuteNonQuery();

                if (result  > 0)
                {
                    return 1;
                    //result = Convert.ToInt32(command.Parameters["ReturnValue"].Value);
                }
                else
                {
                    return 0;
                }
            }
            catch(Exception e)
            {
                // throw;
                result = 0;
                e.InnerException.ToString();
            }
            finally
            {
                DbCon.Close();
            }
            DbCon.Close();
            return result;
        }

        public int ExecuteProcWithReturnValue(string storedProcName, IDataParameter[] parameters)
        {
            int result = 0;
            DbCon.Open();
            SqlCommand command = BuildIntCommand(storedProcName, parameters);
            command.CommandTimeout = 1200;
            try
            {
                command.ExecuteNonQuery();

                if (command.Parameters["ReturnValue"].Value != "" || command.Parameters["ReturnValue"].Value != null)
                {
                    result = Convert.ToInt32(command.Parameters["ReturnValue"].Value);
                }
                else
                {
                    return 0;
                }
            }
            catch
            {
                // throw;
                result = 0;
            }
            finally
            {
                DbCon.Close();
            }
            DbCon.Close();

            return result;
        }

        public string  ExecuteProcWithReturnStringValue(string storedProcName, IDataParameter[] parameters)
        {
            string  result = "";
            DbCon.Open();
            SqlCommand command = BuildIntCommand(storedProcName, parameters);
            command.CommandTimeout = 1200;

            try
            {
                command.ExecuteNonQuery();

                if (command.Parameters["@GUIDOutput"].Value != "" || command.Parameters["@GUIDOutput"].Value != null)
                {
                    result = command.Parameters["@GUIDOutput"].Value.ToString();
                }
                else
                {
                    return "";
                }
            }
            catch
            {
                // throw;                  
                result = "";
            }
            finally
            {
                DbCon.Close();
            }
            DbCon.Close();

            return result;
        }


        public string ExecuteProcScalar(string storedProcName, IDataParameter[] parameters)
        {

            string result="";
            try
            {
                DbCon.Open();
                SqlCommand command = BuildIntCommand(storedProcName, parameters);
                command.CommandTimeout = 1200;

                result = (command.ExecuteScalar()).ToString();
            }
            catch
            {

            }           
            DbCon.Close();

            return result;

        }

        /// <summary>
        /// Executes the procedure and returns integer scalar value
        /// </summary>
        /// <param name="storedProcName">Stored procedure name</param>
        /// <param name="parameters">Parameter list</param>
        /// <returns>Scalar data</returns>
        public DataTable ExecuteProc(string storedProcName, IDataParameter[] parameters, string tableName)
        {
            DataTable dt = new DataTable(tableName);
            try
            {

                DbCon.Open();
                SqlCommand command = BuildIntCommand(storedProcName, parameters);
                command.CommandTimeout = 1200;

                //Error Comes here
                SqlDataAdapter da = new SqlDataAdapter(command);
                da.Fill(dt);

                DbCon.Close();
                //Connection.Close();
            }
            catch (Exception ex)
            {
                throw;
              //  DbCon.Close();
            }
           
            return dt;
        }

        public DataTable  ExecuteProcedure(string storedProcName, IDataParameter[] parameters)
        {
            DataTable dt = new DataTable();
            DataSet ds = new DataSet();
            if (DbCon.State == ConnectionState.Open)
            {
                DbCon.Close();
            }           
            DbCon.Open();
            SqlCommand command = BuildIntCommand(storedProcName, parameters);
            command.CommandTimeout = 1200;

            //Error Comes here
            SqlDataAdapter da = new SqlDataAdapter(command);
            da.Fill(dt);

            DbCon.Close();
            //Connection.Close();
            return dt;
        }

        /// <summary>
        /// Executes the stored procedure and returns sqldatareader object
        /// </summary>
        /// <param name="storedProcName">Stored procedure name</param>
        /// <returns>sqlldatareader</returns>
        public SqlDataReader ExecuteProcDataReader(string storedProcName, IDataParameter[] parameters)
        {
            SqlDataReader returnReader;
              DbCon.Open();
              SqlCommand command = BuildQueryCommand(storedProcName, parameters);
            command.CommandTimeout = 1200;

            command.CommandType = CommandType.StoredProcedure;  
              returnReader = command.ExecuteReader();
              DbCon.Close();
              return returnReader;
             
            //Connection.Close();
           
        }

        /// <summary>
        /// Executes the procedure and returns integer scalar value. No. of rows affected by the query is known by passing rowsAffected int variable
        /// </summary>
        /// <param name="storedProcName">Stored procedure name</param>
        /// <param name="parameters">Parameter list</param>
        /// <param name="rowsAffected">Integer variable to hold return values [rowsAffected]</param>
        /// <returns>int</returns>
        public int ExecuteProc(string storedProcName, IDataParameter[] parameters, out int rowsAffected)
        {
            int result = 0;

            DbCon.Open();

            SqlCommand command = BuildIntCommand(storedProcName, parameters);
            command.CommandTimeout = 1200;

            rowsAffected = command.ExecuteNonQuery();
            result = (int)command.Parameters["ReturnValue"].Value;
            DbCon.Close();
            return result;
        }

        public void ExecuteStatement(string SQLString)
        {
            DbCon.Open();
            try
            {
                SqlCommand command = new SqlCommand();
                command.Connection = DbCon;
                command.CommandType = CommandType.Text;
                command.CommandText = SQLString;
                command.CommandTimeout = 1200;

                command.ExecuteNonQuery();
            }
            catch
            {
            }        
            DbCon.Close();
        }


        /// <summary>
        /// Executes the stored procedure and fills datatable
        /// </summary>
        /// <param name="storedProcName">Stored procedure name</param>
        /// <returns>DataTable</returns>
        public DataTable ExecuteProc(string storedProcName)
        {
            DataTable dt = new DataTable();

            DbCon.Open();
            try
            {
                SqlCommand command = BuildQueryCommand(storedProcName);
                command.CommandType = CommandType.StoredProcedure;
                command.CommandTimeout = 1200;

                SqlDataAdapter da = new SqlDataAdapter(command);
                da.Fill(dt);
            }
            catch 
            {
            }          
            DbCon.Close();
            return dt;
        }

        /// <summary>
        /// Executes the stored procedure, fills datatable and sets its name to supplied tablename and stores it in new dataset
        /// </summary>
        /// <param name="storedProcName">Stored procedure name</param>
        /// <returns>Dataset</returns>
        public DataSet ExecuteProcDataSet(string storedProcName, IDataParameter[] parameters, string tableName)
        {
            DataSet dataSet = new DataSet();
            DbCon.Open();
            SqlDataAdapter sqlDA = new SqlDataAdapter();
            try
            {
                sqlDA.SelectCommand = BuildQueryCommand(storedProcName, parameters);
                sqlDA.Fill(dataSet, tableName);
            }
            catch 
            {
                
            }        
            DbCon.Close();

            return dataSet;
        }

        public DataSet ExecuteProcDataSet(string storedProcName, IDataParameter[] parameters)
        {
            DataSet dataSet = new DataSet();
            DbCon.Open();
            SqlDataAdapter sqlDA = new SqlDataAdapter();
            try
            {
                sqlDA.SelectCommand = BuildQueryCommand(storedProcName, parameters);
                sqlDA.Fill(dataSet);
            }
            catch 
            {
            }         
            DbCon.Close();

            return dataSet;
        }

        /// <summary>
        /// Executes the stored procedure, fills datatable and sets its name to supplied tablename and stores it in given dataset
        /// </summary>
        /// <param name="storedProcName">Stored procedure name</param>
        /// <returns></returns>
        public void ExecuteProcDataSet(string storedProcName, IDataParameter[] parameters, DataSet dataSet, string tableName)
        {
            DbCon.Open();
            SqlDataAdapter sqlDA = new SqlDataAdapter();
            try
            {
                sqlDA.SelectCommand = BuildIntCommand(storedProcName, parameters);
                sqlDA.Fill(dataSet, tableName);
            }
            catch
            {
            }       
            DbCon.Close();
        }

        public int ExecuteProcScalarForInt(string storedProcName, IDataParameter[] parameters)
        {

            int result;

            DbCon.Open();
            SqlCommand command = BuildIntCommand(storedProcName, parameters);
            command.CommandTimeout = 1200;

            try
            {
                result = Convert.ToInt32(command.ExecuteScalar());
            }
            catch (Exception)
            {

                result = 0;
            }

            DbCon.Close();
            return result;


        }
    }
}
