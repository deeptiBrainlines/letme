﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BE;
using System.Data;
using System.Data.SqlClient;
namespace DAL
{
   public class ClusterMasterDAL
    {

       DBClass objDBClass = new DBClass();
       ClusterMasterBE objClusterMasterBE = new ClusterMasterBE();

       public DataTable ClusterMasterGet()
       {
           DataTable DtCluster = new DataTable();
           try
           {
               DtCluster = objDBClass.ExecuteProc(ClusterMasterBE.SpClusterMasterGet);
           }
           catch
           {

           }
           return DtCluster;
       }
       public int ClusterTypeMasterInsert(ClusterMasterBE objClusterMasterBE)
       {
           int Status = 0;
           try
           {
               SqlParameter[] sqlparam = 
            {              
                 new SqlParameter ("@ClusterTypeName", objClusterMasterBE.propClusterTypeName),
                 new SqlParameter ("@ClusterIds", objClusterMasterBE.propClusterIds ),
                 new SqlParameter ("@CreatedBy", objClusterMasterBE.propCreatedBy),
                 new SqlParameter ("@CreatedDate", objClusterMasterBE.propCreatedDate),
            };
               Status = objDBClass.ExecuteProc(ClusterMasterBE.SpClusterTypeMasterInsert, sqlparam);
           }
           catch
           {

           }
           return Status;
       }

       public DataTable ClusterTypeMasterGet()
       {
           DataTable DtCluster = new DataTable();
           try
           {
               DtCluster = objDBClass.ExecuteProc(ClusterMasterBE.SpClusterTypeMasterGet);
           }
           catch
           {

           }
           return DtCluster;
       }

       public DataTable ClusterTypeMasterGetClustersById(string ClusterTypeId)
       {
           DataTable DtCluster = new DataTable();
           try
           {
               SqlParameter[] sqlparam = 
                {  
                    new SqlParameter("@ClusterTypeId_FK",ClusterTypeId ),
                };
               DtCluster = objDBClass.ExecuteProc(ClusterMasterBE.SpClusterTypeMasterGetClustersById, sqlparam, "Cluster");
           }
           catch
           {

           }
           return DtCluster;
       }

       public int ClusterTypeMasterUpdate(ClusterMasterBE objClusterMasterBE)
       {
           int Status = 0;
           try
           {
               SqlParameter[] sqlparam = 
            {              
                 new SqlParameter ("@ClusterTypeId_PK", objClusterMasterBE.propClusterTypeId_PK),
                 new SqlParameter ("@ClusterIds", objClusterMasterBE.propClusterIds ),
                 new SqlParameter ("@CreatedBy", objClusterMasterBE.propCreatedBy),
                 new SqlParameter ("@CreatedDate", objClusterMasterBE.propCreatedDate),
            };
               Status = objDBClass.ExecuteProc(ClusterMasterBE.SpClusterTypeMasterUpdate, sqlparam);
           }
           catch
           {

           }
           return Status;
       }

       public DataTable ClusterTypeMasterCheckDuplicate(string ClusterTypeName)
       {
           DataTable DtCluster = new DataTable();
           try
           {
               SqlParameter[] sqlparam = 
                {  
                    new SqlParameter("@ClusterTypeName",ClusterTypeName ),
                };
               DtCluster = objDBClass.ExecuteProc(ClusterMasterBE.SpClusterTypeMasterCheckDuplicate, sqlparam, "Cluster");
           }
           catch
           {

           }
           return DtCluster;
       }
    }
}
