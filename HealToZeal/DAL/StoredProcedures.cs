﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DAL
{
  public class StoredProcedures
    {
        //Company Details
        #region Company Details
      //SPs
         public const string SpAnswerMasterGet = "SpAnswerMasterGet";
         public const string SpAnswerMasterInsert = "SpAnswerMasterInsert";
         public const string SpAnswerMasterUpdate= "SpAnswerMasterUpdate";

      //Fields
        #endregion Company Details
      public const string SpCompanyDetailsGet = "SpCompanyDetailsGet";
      public const string SpCompanyDetailsInsert="SpCompanyDetailsInsert";
      public const string SpCompanyDetailsUpdate="SpCompanyDetailsUpdate";


      public const string SpQuestionMasterGet="SpQuestionMasterGet";
      public const string SpQuestionMasterInsert="SpQuestionMasterInsert";
      public const string SpQuestionMasterUpdate="SpQuestionMasterUpdate";
     

    }
}
