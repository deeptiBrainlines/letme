﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BE;
using System.Data.SqlClient;
using System.Data;

namespace DAL
{
    public class EmployeeAssessmentDetailsDAL
    {
        DBClass objDBClass = new DBClass();
        public int EmployeeAssessmentDetailsInsert(EmployeeAssessmentDetailsBE objEmployeeAssessmentDetailsBE)
        {
            int Status = 0;
            try
            {
                SqlParameter[] sqlparam =
             {
                     new SqlParameter ("@EmployeeId_FK", objEmployeeAssessmentDetailsBE.propEmployeeId_FK),
                  //   new SqlParameter ("@AssessmentId_FK", objEmployeeAssessmentDetailsBE.propAssessmentId_FK),               
                     new SqlParameter("@QuestionId_FK",objEmployeeAssessmentDetailsBE.propQuestionId_FK),
                     new SqlParameter("@AnswerId_FK",objEmployeeAssessmentDetailsBE.propAnswerId_FK),
                     new SqlParameter("@CreatedBy",objEmployeeAssessmentDetailsBE.propCreatedBy),
                     new SqlParameter("@CreatedDate",objEmployeeAssessmentDetailsBE.propCreatedDate),
                       new SqlParameter("@InstanceCompanyRelationId_FK",objEmployeeAssessmentDetailsBE.propInstanceCompanyRelationId_FK),
                };


            //    SqlParameter[] sqlparam =
            //     {
            //     new SqlParameter ("@EmployeeId_FK", Guid.Parse(objEmployeeAssessmentDetailsBE.propEmployeeId_FK)),
            //     new SqlParameter("@QuestionId_FK",Guid.Parse(objEmployeeAssessmentDetailsBE.propQuestionId_FK)),
            //     new SqlParameter("@AnswerId_FK",Guid.Parse(objEmployeeAssessmentDetailsBE.propAnswerId_FK)),
            //     new SqlParameter("@CreatedBy",Guid.Parse(objEmployeeAssessmentDetailsBE.propCreatedBy)),
            //     new SqlParameter("@CreatedDate",objEmployeeAssessmentDetailsBE.propCreatedDate),
            //       new SqlParameter("@InstanceCompanyRelationId_FK",Guid.Parse(objEmployeeAssessmentDetailsBE.propInstanceCompanyRelationId_FK)),
            //};
                Status = objDBClass.ExecuteProc(EmployeeAssessmentDetailsBE.SpEmployeeAssessmentDetailsInsert, sqlparam);
            }
            catch (Exception e)
            {
                e.InnerException.ToString();
            }
            return Status;
        }

        public int PiechartInsert(EmployeeAssessmentDetailsBE objEmployeeAssessmentDetailsBE)
        {
            int Status = 0;
            try
            {
                SqlParameter[] sqlparam =
             {
                // new SqlParameter ("@pie_id", objEmployeeAssessmentDetailsBE.pie_id),
                    
                 new SqlParameter("@percentagered",objEmployeeAssessmentDetailsBE.percentagered),
                 new SqlParameter("@percentageAmber1",objEmployeeAssessmentDetailsBE.percentageAmber1),
                 new SqlParameter("@percentageAmber2",objEmployeeAssessmentDetailsBE.percentageAmber2),
                 new SqlParameter("@percentagegreen",objEmployeeAssessmentDetailsBE.percentagegreen),
                 new SqlParameter("@BatchId",objEmployeeAssessmentDetailsBE.batBatchId),

            };
                Status = objDBClass.ExecuteProc("SP_InsertPiechart", sqlparam);
            }
            catch (Exception e)
            {
                e.InnerException.ToString();
            }
            return Status;
        }

        public int countforpieInsert(EmployeeAssessmentDetailsBE objEmployeeAssessmentDetailsBE)
        {
            int Status = 0;
            try
            {
                SqlParameter[] sqlparam =
             {
                // new SqlParameter ("@pie_id", objEmployeeAssessmentDetailsBE.pie_id),
                  new SqlParameter("@compid",objEmployeeAssessmentDetailsBE.percentagecompid),
                 new SqlParameter("@Color",objEmployeeAssessmentDetailsBE.Color),
                new SqlParameter("@batchid",objEmployeeAssessmentDetailsBE.batBatchId),
              //  new SqlParameter("@Employee_Id",objEmployeeAssessmentDetailsBE.empid)

            };
                Status = objDBClass.ExecuteProc("SP_Insertcolors", sqlparam);
            }
            catch (Exception e)
            {
                e.InnerException.ToString();
            }
            return Status;
        }

        public DataTable AssessmentInstanceName(string AssessmentInstanceId)
        {
            DataTable DtEmployeeAssessmentDetails = new DataTable();
            try
            {
                SqlParameter[] sqlparam =
                 {
                  new SqlParameter("@AssessmentInstanceId_FK",AssessmentInstanceId),
                };

                DtEmployeeAssessmentDetails = objDBClass.ExecuteProc("SpInstanceAssementsName", sqlparam, "AssessmentInstanceMaster");
            }
            catch(Exception ex)
            {

            }
            return DtEmployeeAssessmentDetails;
        }

        public DataTable ifexistsrecords(string batchid)
        {

            DataTable existscheck = new DataTable();
            try
            {
                SqlParameter[] sqlparam =
             {

                 new SqlParameter("@batchid",batchid),

            };
                existscheck = objDBClass.ExecuteProc("Sp_checkBatchExists", sqlparam, "EmployeeAssessmentDetails");

            }
            catch (Exception e)
            {
                e.InnerException.ToString();
            }
            return existscheck;
        }

        public DataTable EmployeeAssessmentDetailsGet(string AssessmentInstanceId, int Position, string EmployeeId, string AssessmentInstanceCompanyId)
        {
            DataTable DtEmployeeAssessmentDetails = new DataTable();
            try
            {
                SqlParameter[] sqlparam =
                 {
                   // new SqlParameter("@AssessmentId_FK",AssessmentId),
                    new SqlParameter("@Position",Position),
                    new SqlParameter("@EmployeeId_FK",EmployeeId),
                    new SqlParameter("@AssessmentInstanceId_FK",AssessmentInstanceId),
                   new SqlParameter("@InstanceCompanyRelationId_FK",AssessmentInstanceCompanyId),
                };

                DtEmployeeAssessmentDetails = objDBClass.ExecuteProc(EmployeeAssessmentDetailsBE.SpEmployeeAssessmentDetailsGet, sqlparam, "EmployeeAssessmentDetails");
            }
            catch
            {

            }
            return DtEmployeeAssessmentDetails;
        }
        public DataTable SpEmployeeAssessmentDetailsGetAll(string AssessmentInstanceId, int Position, string EmployeeId, string AssessmentInstanceCompanyId)
        {
            DataTable DtEmployeeAssessmentDetails = new DataTable();
            try
            {
                SqlParameter[] sqlparam =
                 {
                   // new SqlParameter("@AssessmentId_FK",AssessmentId),
                    new SqlParameter("@Position",Position),
                    new SqlParameter("@EmployeeId_FK",EmployeeId),
                    new SqlParameter("@AssessmentInstanceId_FK",AssessmentInstanceId),
                   new SqlParameter("@InstanceCompanyRelationId_FK",AssessmentInstanceCompanyId),
                };

                DtEmployeeAssessmentDetails = objDBClass.ExecuteProc(EmployeeAssessmentDetailsBE.SpEmployeeAssessmentDetailsGetAll, sqlparam, "EmployeeAssessmentDetails");
            }
            catch
            {

            }
            return DtEmployeeAssessmentDetails;
        }

        public DataTable EmployeeAssessmentDetailsGetCount(string EmployeeId, string AssessmentInstanceId, string InstanceCompanyRelationId)
        {
            DataTable DtEmployeeAssessmentDetails = new DataTable();
            try
            {
                SqlParameter[] sqlparam =
                 {
                  new SqlParameter("@EmployeeId_FK",EmployeeId),
                  new SqlParameter("@AssessmentInstanceId_FK",AssessmentInstanceId),
                  //@InstanceCompanyRelationId_PK
                   new SqlParameter("@InstanceCompanyRelationId_PK",InstanceCompanyRelationId),
                };

                DtEmployeeAssessmentDetails = objDBClass.ExecuteProc(EmployeeAssessmentDetailsBE.SpEmployeeAssessmentDetailsGetCount, sqlparam, "EmployeeAssessmentDetails");
            }
            catch
            {

            }
            return DtEmployeeAssessmentDetails;
        }

        public int EmployeeAssessmentDetailsImport(EmployeeAssessmentDetailsBE objEmployeeAssessmentDetailsBE)
        {
            int Status = 0;
            try
            {
                SqlParameter[] sqlparam =
             {
                 new SqlParameter ("@EmployeeCompanyID", objEmployeeAssessmentDetailsBE.propEmployeeCompanyID),
                // new SqlParameter ("@AssessmentId_FK", objEmployeeAssessmentDetailsBE.propAssessmentId_FK),
                 new SqlParameter("@QuestionId_FK",objEmployeeAssessmentDetailsBE.propQuestionId_FK),
                 new SqlParameter("@AnswerId_FK",objEmployeeAssessmentDetailsBE.propAnswerId_FK),
                 new SqlParameter("@batchid",objEmployeeAssessmentDetailsBE.batchid),
                // new SqlParameter("@CreatedBy",objEmployeeAssessmentDetailsBE.propCreatedBy),
                // new SqlParameter("@CreatedDate",objEmployeeAssessmentDetailsBE.propCreatedDate),
               //  new SqlParameter("@InstanceCompanyRelationId_FK",objEmployeeAssessmentDetailsBE.propInstanceCompanyRelationId_FK),
            };
                Status = objDBClass.ExecuteProc(EmployeeAssessmentDetailsBE.ImportEmployeeDeatils, sqlparam);

                //StoredProcedures = ImportEmployeeDeatils (Added by meenakshi)
            }
            catch (Exception e)
            {
                throw e;
            }
            return Status;
        }


        public int EmployeeAssessmentDetailsImportClusterWise(EmployeeAssessmentDetailsBE objEmployeeAssessmentDetailsBE)
        {
            int Status = 0;
            Guid propEmployeeId_FK;
            Guid.TryParse("propEmployeeId_FK", out propEmployeeId_FK);

            Guid propAssessmentId_FK;
            Guid.TryParse("propAssessmentId_FK", out propAssessmentId_FK);

            Guid propClusterID;
            Guid.TryParse("propClusterID", out propClusterID);

            Guid propInstanceCompanyRelationId_FK;
            Guid.TryParse("propInstanceCompanyRelationId_FK", out propInstanceCompanyRelationId_FK);

            Guid propAssessmentInstanceId_FK;
            Guid.TryParse("propAssessmentInstanceId_FK", out propAssessmentInstanceId_FK);

            try
            {
                SqlParameter[] sqlparam =
             {

                new SqlParameter("@EmployeeID_FK",objEmployeeAssessmentDetailsBE.propEmployeeId_FK),
                //new SqlParameter ("@AssessmentId_FK", objEmployeeAssessmentDetailsBE.propAssessmentId_FK),
                new SqlParameter("@ClusterID",objEmployeeAssessmentDetailsBE.propClusterID),
                 new SqlParameter("@ClusterScore",objEmployeeAssessmentDetailsBE.propClusterScore),

               new SqlParameter("@CreatedBy",objEmployeeAssessmentDetailsBE.propCreatedBy),
               //  new SqlParameter("@InstanceCompanyRelationId_FK",objEmployeeAssessmentDetailsBE.propInstanceCompanyRelationId_FK),
               //  new SqlParameter("@AssessmentInstanceId_FK",objEmployeeAssessmentDetailsBE.propAssessmentInstanceId_FK),
                 new SqlParameter("@ClusterScorePercentage",objEmployeeAssessmentDetailsBE.propPercentage),
                 new SqlParameter("@batchid",objEmployeeAssessmentDetailsBE.batchid)

            };
                Status = objDBClass.ExecuteProc(EmployeeAssessmentDetailsBE.ImportEmployeeDeatilsClusterwise, sqlparam);
            }
            catch (Exception e)
            {
                throw e;
            }
            return Status;
        }
        public DataTable EmployeeFreeAssessmentDetailsGetCount(string EmployeeId)
        {
            DataTable DtEmployeeAssessmentDetails = new DataTable();
            try
            {
                SqlParameter[] sqlparam =
                 {
                  new SqlParameter("@EmployeeId_FK",EmployeeId),
                };

                DtEmployeeAssessmentDetails = objDBClass.ExecuteProc("SpEmployeeFreeAssessmentDetailsGetCount", sqlparam, "EmployeeAssessmentDetails");
            }
            catch
            {

            }
            return DtEmployeeAssessmentDetails;
        }

    }
}
