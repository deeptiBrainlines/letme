﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BE;
using System.Data;
using System.Data.SqlClient;
namespace DAL
{
   public class UsersFeedbackDAL
    {
       DBClass objDBClass = new DBClass();

       public int UsersFeedbackInsert(UsersFeedbackBE objUsersFeedbackBE)
       {
           int Status = 0;
           try
           {
               SqlParameter[] sqlparam = 
            {                
                new SqlParameter ("@EmployeeId_FK", objUsersFeedbackBE.propEmployeeId_FK),
                 new SqlParameter ("@FeedbackQuestionId_FK",objUsersFeedbackBE.propFeedbackQuestionId_FK),
                 new SqlParameter("@FeedbackAnswerId_FK",objUsersFeedbackBE.propFeedbackAnswerId_FK),
                 new SqlParameter("@CreatedBy",objUsersFeedbackBE.propCreatedBy),
                 new SqlParameter("@CreatedDate",objUsersFeedbackBE.propCreatedDate),
                 new SqlParameter("@Feedback",objUsersFeedbackBE.propFeedback ),
               //  new SqlParameter("@IsActive",objAnswerMasterBE.propIsActive),
            };
               Status = objDBClass.ExecuteProc(UsersFeedbackBE.SpUsersFeedbackInsert, sqlparam);
           }
           catch
           {

           }
           return Status;
       }

       public DataTable FeedbackQuestionMasterGet()
       {
           DataTable DtFeedbackQuestions = new DataTable();
           try
           {
               DtFeedbackQuestions = objDBClass.ExecuteProc(UsersFeedbackBE.SpFeedbackQuestionMasterGet);
           }
           catch
           {

           }
           return DtFeedbackQuestions;
       }

       public DataTable FeedbackAnswerMasterGetById(string QuestionId)
       {
           DataTable DtAnswer = new DataTable();
           try
           {
               SqlParameter[] sqlparam =
                {
                    new SqlParameter("@FeedbackQuestionId_FK",QuestionId),
                };

               DtAnswer = objDBClass.ExecuteProc(UsersFeedbackBE.SpFeedbackAnswerMasterGetById, sqlparam, "FeedbackAnswerMaster");
           }
           catch
           {

           }
           return DtAnswer;
       }

       public DataTable GetUserFeedback()
       {
           DataTable DtAnswer = new DataTable();
           try
           {
               //SqlParameter[] sqlparam =
               // {
               //     new SqlParameter("@FeedbackQuestionId_FK",QuestionId),
               // };
               DtAnswer = objDBClass.ExecuteProc(UsersFeedbackBE.SpGetUserFeedback);
           }
           catch
           {

           }
           return DtAnswer;
       }
    }
}
