﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BE;
using System.Data.SqlClient;
using System.Data;

namespace DAL
{
    //Code By Poonam on 14 july 2016
   public  class CompanyDepartmentDAL
    {
        DBClass objDBClass = new DBClass();

        public int CompanyDepartInsert(CompanyDepartmentBE objCompanyDeptBE)
        {
            int Status = 0;
            try
            {
                SqlParameter[] sqlparam = 
            {                
                 new SqlParameter ("@CompanyId_FK", objCompanyDeptBE.propCompanyId_FK),
                 new SqlParameter ("@DepartmentName", objCompanyDeptBE.propDepartmentName),                 
                 new SqlParameter("@CreatedBy",objCompanyDeptBE.propCreatedBy),
                 new SqlParameter("@CreatedDate",objCompanyDeptBE.propCreatedDate),
                
            };
                Status = objDBClass.ExecuteProc(CompanyDepartmentBE.SpCompanyDeaprtmentInsert, sqlparam);
            }
            catch
            {

            }
            return Status;
        }

        public DataTable CompanyDeaprtmentGetByCompanyId(string CompanyId)
        {
            DataTable Dtdept = new DataTable();
            try
            {
                SqlParameter[] sqlparam =
                {
                    new SqlParameter("@CompanyId_FK",CompanyId ),
                    //new SqlParameter("@DeptID_PK",DeptID )
                };

                Dtdept = objDBClass.ExecuteProc(CompanyDepartmentBE.SpCompanyDepartmentGetByCompanyId, sqlparam, "CompanyEmployee");
            }
            catch
            {

            }
            return Dtdept;
        }

        public DataTable GetAllDepartment(string CompanyId)
        {
            DataTable Dtdept = new DataTable();
            try
            {
                SqlParameter[] sqlparam =
                {
                    new SqlParameter("@CompanyID",CompanyId ),
                    //new SqlParameter("@DeptID_PK",DeptID )
                };

                Dtdept = objDBClass.ExecuteProc(CompanyDepartmentBE.SpGetAllDepartment, sqlparam, "CompanyEmployee");
            }
            catch
            {

            }
            return Dtdept;
        }

        public DataTable GetcompnyName(string CompanyId)
        {
            DataTable Dtdept = new DataTable();
            try
            {
                SqlParameter[] sqlparam =
                {
                    new SqlParameter("@CompanyId_FK",CompanyId ),
                    //new SqlParameter("@DeptID_PK",DeptID )
                };

                Dtdept = objDBClass.ExecuteProc(CompanyDepartmentBE.GetcompnyName, sqlparam, "CompanyEmployee");
            }
            catch
            {

            }
            return Dtdept;
        }

        public DataTable CompanyDepartmentGetByCompanyIdandDeptID(string CompanyId, string DeptID)
        {
            DataTable Dtdept = new DataTable();
            try
            {
                SqlParameter[] sqlparam =
                {
                    new SqlParameter("@CompanyId_FK",CompanyId ),
                    new SqlParameter("@DeptID_PK",DeptID )
                };

                Dtdept = objDBClass.ExecuteProc(CompanyDepartmentBE.SpCompanyDepartmentGetByCompanyIdandDeptID, sqlparam, "CompanyEmployee");
            }
            catch
            {

            }
            return Dtdept;
        }


        public DataTable TocheckDepartmentExistsOrNot(string DepartmentName, string CompanyId)
        {
            DataTable DtCadre = new DataTable();
            try
            {
                SqlParameter[] sqlparam =
                {
                     new SqlParameter ("@DepartmentName",DepartmentName), 
                    new SqlParameter("@CompanyId_FK",CompanyId ),

                };

                DtCadre = objDBClass.ExecuteProc(CompanyDepartmentBE.spTocheckDepartmentExistsOrNot, sqlparam, "CompanyDepartment");
            }
            catch
            {

            }
            return DtCadre;
        }

        public int DepartmentUpdate(CompanyDepartmentBE objCompanyDeptBE)
        {
            int Status = 0;
            try
            {
                SqlParameter[] sqlparam = 
            {                
                 new SqlParameter ("@CompanyId_FK", objCompanyDeptBE.propCompanyId_FK),
                 new SqlParameter ("@DeptId_PK", objCompanyDeptBE.propDeptId_PK),
                 new SqlParameter("@DepartmentName",objCompanyDeptBE.propDepartmentName ),                
                 new SqlParameter("@CreatedBy",objCompanyDeptBE.propCreatedBy),
                 new SqlParameter("@CreatedDate",objCompanyDeptBE.propCreatedDate),

               
            };
                Status = objDBClass.ExecuteProc(CompanyDepartmentBE.SpDepartmentUpdate, sqlparam);
            }
            catch
            {
                throw;
            }
            return Status;
        }

        
        public DataTable DeleteDepartMent(string DeptId_PK)
        {
            DataTable Dtdep= new DataTable();
            try
            {
                SqlParameter[] sqlparam =
                {
                    
                   new SqlParameter ("@DeptID", DeptId_PK)

                };

                Dtdep = objDBClass.ExecuteProc(CompanyDepartmentBE.spDeleteDepartMent, sqlparam, "CompanyDepartment");
            }
            catch
            {

            }
            return Dtdep;
        }


        public DataTable AllDeatilsOfDepartment(string DeptId_PK)
        {
            DataTable Dtdept = new DataTable();
            try
            {
                SqlParameter[] sqlparam =
                {
                     new SqlParameter ("@DeptId_PK",DeptId_PK),
                    
                };

                Dtdept = objDBClass.ExecuteProc(CompanyDepartmentBE.spAllDeatilsOfDepartment, sqlparam, "CompanyDepartment");
            }
            catch
            {

            }
            return Dtdept;
        }

       
    }
}
