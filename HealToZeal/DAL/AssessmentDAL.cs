﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BE;
using DAL;
using System.Data;
using System.Data.SqlClient;
namespace DAL
{
   public class AssessmentDAL
   {
       DBClass objDBClass = new DBClass();

       public DataTable AssessmentGet()
       {
           DataTable DtAssessment = new DataTable();
           try
           {
               DtAssessment = objDBClass.ExecuteProc(AssessmentBE.SpAssessmentGet);
           }
           catch
           {

           }
           return DtAssessment;
       }
       public DataTable AssessmentGetByAssessmentId(string AssessmentId)
       {
           DataTable DtAssessment = new DataTable();
           try
           {
               SqlParameter[] sqlparam = 
                {  
                    new SqlParameter("@AssessmentId_PK",AssessmentId ),
                };
               DtAssessment = objDBClass.ExecuteProc(AssessmentBE.SpAssessmentGetByAssessmentId, sqlparam, "Assessment");
           }
           catch
           {

           }
           return DtAssessment;
       }

       public int AssessmentInsert(AssessmentBE objAssessmentBE)
       {
           int Status = 0;
           try
           {
               SqlParameter[] sqlparam = 
            {              
                 new SqlParameter ("@AssessmentName", objAssessmentBE.propAssessmentName ),
                 //new SqlParameter ("@PersonaId_FK", objAssessmentBE.propPersonaId_FK ),
                 new SqlParameter ("@NoOfQuestions",objAssessmentBE.propNoOfQuestions  ),
                 new SqlParameter ("@IsComplete", objAssessmentBE.propIsComplete),
                 new SqlParameter ("@CreatedBy", objAssessmentBE.propCreatedBy),
                 new SqlParameter ("@CreatedDate", objAssessmentBE.propCreatedDate),
            };
               Status = objDBClass.ExecuteProc(AssessmentBE.SpAssessmentInsert, sqlparam);
           }
           catch
           {

           }
           return Status;
       }

       public int AssessmentUpdate(AssessmentBE objAssessmentBE)
       {
           int Status = 0;
           try
           {
               SqlParameter[] sqlparam = 
            {              
                 new SqlParameter ("@AssessmentId_PK",objAssessmentBE.propAssessmentId_PK),
                 new SqlParameter ("@AssessmentName",objAssessmentBE.propAssessmentName),
                 //new SqlParameter ("@PersonaId_FK",objAssessmentBE.propPersonaId_FK),
                 new SqlParameter ("@NoOfQuestions", objAssessmentBE.propNoOfQuestions),
                 new SqlParameter ("@IsComplete",objAssessmentBE.propIsComplete),
                new SqlParameter ("@CreatedBy", objAssessmentBE.propCreatedBy),
                 new SqlParameter ("@CreatedDate", objAssessmentBE.propCreatedDate),
            };
               Status = objDBClass.ExecuteProc(AssessmentBE.SpAssessmentUpdate, sqlparam);
           }
           catch
           {

           }
           return Status;
       }

        // AssessmentInstanceMaster
        //14 July 15


        public int SpAssessmentInstanceCompanyRelationInsert(AssessmentBE objAssessmentBE)
        {
            int Status = 0;
            try
            {
                SqlParameter[] sqlparam =
             {
                 new SqlParameter ("@AssessmentInstanceId_FK", objAssessmentBE.propAssessmentInstanceId_FK),
                 new SqlParameter ("@CompanyId", objAssessmentBE.propCompanyIds ),
                  new SqlParameter ("@ValidFrom", objAssessmentBE.propValidFrom),
                   new SqlParameter ("@ValidTo", objAssessmentBE.propValidTo),
                 new SqlParameter ("@CreatedBy", objAssessmentBE.propCreatedBy),
                 new SqlParameter ("@CreatedDate", objAssessmentBE.propCreatedDate),
            };
                Status = objDBClass.ExecuteProc(AssessmentBE.SpAssessmentInstanceCompanyRelationInsert, sqlparam);
            }
            catch
            {

            }
            return Status;
        }

        public DataTable AssessmentInstanceMasterGet()
       {
           DataTable DtAssessmentInstance = new DataTable();
           try
           {
               DtAssessmentInstance = objDBClass.ExecuteProc(AssessmentBE.SpAssessmentInstanceMasterGet);
           }
           catch
           {

           }
           return DtAssessmentInstance;
       }

       public int AssessmentInstanceAssessmentInsert(AssessmentBE objAssessmentBE)
       {
           int Status = 0;
           try
           {
               SqlParameter[] sqlparam = 
            {              
                 new SqlParameter ("@AssessmentInstanceName", objAssessmentBE.propAssessmentInstanceName),
                 new SqlParameter ("@AssessmentIds", objAssessmentBE.propAssessmentIds ),
                 new SqlParameter ("@CreatedBy", objAssessmentBE.propCreatedBy),
                 new SqlParameter ("@CreatedDate", objAssessmentBE.propCreatedDate),
            };
               Status = objDBClass.ExecuteProc(AssessmentBE.SpAssessmentInstanceAssessmentInsert, sqlparam);
           }
           catch
           {

           }
           return Status;
       }

       //public int SpAssessmentInstanceCompanyRelationInsert(AssessmentBE objAssessmentBE)
       //{
       //    int Status = 0;
       //    try
       //    {
       //        SqlParameter[] sqlparam = 
       //     {              
       //          new SqlParameter ("@AssessmentInstanceId_FK", objAssessmentBE.propAssessmentInstanceId_FK),
       //          new SqlParameter ("@CompanyId", objAssessmentBE.propCompanyIds ),
       //           new SqlParameter ("@ValidFrom", objAssessmentBE.propValidFrom),
       //            new SqlParameter ("@ValidTo", objAssessmentBE.propValidTo),
       //          new SqlParameter ("@CreatedBy", objAssessmentBE.propCreatedBy),
       //          new SqlParameter ("@CreatedDate", objAssessmentBE.propCreatedDate),
       //     };
       //        Status = objDBClass.ExecuteProc(AssessmentBE.SpAssessmentInstanceCompanyRelationInsert, sqlparam);
       //    }
       //    catch
       //    {

       //    }
       //    return Status;
       //}

       //Code by Poonam on 29th july 2016
       public int CompanyRelationAssessmentInstanceInsert(AssessmentBE objAssessmentBE)
       {
           int Status = 0;
           try
           {
               SqlParameter[] sqlparam = 
            {              
                 new SqlParameter ("@AssessmentInstanceId_FK", objAssessmentBE.propAssessmentInstanceId_FK),
                 new SqlParameter ("@CompanyId", objAssessmentBE.propCompanyId_FK ),
                  new SqlParameter ("@ValidFrom", objAssessmentBE.propValidFrom),
                   new SqlParameter ("@ValidTo", objAssessmentBE.propValidTo),
                   new SqlParameter ("@BatchID_FK", objAssessmentBE.propBatchID_FK),
                 new SqlParameter ("@CreatedBy", objAssessmentBE.propCreatedBy),
                 new SqlParameter ("@CreatedDate", objAssessmentBE.propCreatedDate),
            };
               Status = objDBClass.ExecuteProc(AssessmentBE.SpCompanyRelationAssessmentInstanceInsert, sqlparam);
           }
           catch
           {

           }
           return Status;
       }

       public DataTable TocheckInstanceExistsOrNot(string AssesmentInstanceID, string CompanyId)
       {
           DataTable DtCadre = new DataTable();
           try
           {
               SqlParameter[] sqlparam =
                {
                     new SqlParameter ("@AssesmentInstanceID",AssesmentInstanceID), 
                    new SqlParameter("@CompanyId_FK",CompanyId ),

                };

               DtCadre = objDBClass.ExecuteProc(AssessmentBE.spTocheckInstanceExistsOrNot, sqlparam, "CompanyDepartment");
           }
           catch
           {

           }
           return DtCadre;
       }



       public DataTable GetBatchCompanyWise( string CompanyId)
       {
           DataTable DtCadre = new DataTable();
           try
           {
               SqlParameter[] sqlparam =
                {
                   
                    new SqlParameter("@CompanyID_FK",CompanyId ),

                };

               DtCadre = objDBClass.ExecuteProc(AssessmentBE.SpGetBatchCompanyWise, sqlparam, "CompanyDepartment");
           }
           catch
           {

           }
           return DtCadre;
       }

       public DataTable CompanyRelationAssessmentInstanceUpdate(string CompanyId,string AssessmentID,DateTime Validfrom,DateTime ValidTo)
       {
           DataTable DtCadre = new DataTable();
           try
           {
               SqlParameter[] sqlparam = 
            {   
                new SqlParameter ("@CompanyId", CompanyId ),
                 new SqlParameter ("@AssessmentInstanceId", AssessmentID),
                
                  new SqlParameter ("@ValidFrom", Validfrom),
                   new SqlParameter ("@ValidTo", ValidTo),
                // new SqlParameter ("@CreatedBy", objAssessmentBE.propCreatedBy),
               //  new SqlParameter ("@CreatedDate", objAssessmentBE.propCreatedDate),
            };
               DtCadre = objDBClass.ExecuteProc(AssessmentBE.SpCompanyRelationAssessmentInstanceUpdate, sqlparam, "CompanyDepartment");
           }
           catch
           {

           }
           return DtCadre;
       }
       public DataTable AssessmentInstanceAssessmentRelationGetByInstanceId(string AssessmentInstanceId)
       {
           DataTable DtAssessment = new DataTable();
           try
           {
               SqlParameter[] sqlparam = 
                {  
                    new SqlParameter("@AssessmentInstanceId_FK",AssessmentInstanceId ),
                };
               DtAssessment = objDBClass.ExecuteProc(AssessmentBE.SpAssessmentInstanceAssessmentRelationGetByInstanceId, sqlparam, "Assessment");
           }
           catch
           {

           }
           return DtAssessment;
       }

       public DataTable AssessmentInstanceCompanyRelationGet(string AssessmentInstanceId)
       {
           DataTable DtAssessment = new DataTable();
           try
           {
               SqlParameter[] sqlparam = 
                {  
                  //  new SqlParameter("@AssessmentInstanceId_FK",AssessmentInstanceId ),
                     new SqlParameter("@ComapnyID",AssessmentInstanceId ),
                };
               DtAssessment = objDBClass.ExecuteProc(AssessmentBE.SpAssessmentInstanceCompanyRelationGet, sqlparam, "Assessment");
           }
           catch
           {

           }
           return DtAssessment;
       }

       public DataTable AssessmentInstanceMasterCheckDuplicate(string AssessmentInstanceName)
       {
           DataTable DtAssessment = new DataTable();
           try
           {
               SqlParameter[] sqlparam = 
                {  
                    new SqlParameter("@AssessmentInstanceName",AssessmentInstanceName ),
                };
               DtAssessment = objDBClass.ExecuteProc(AssessmentBE.SpAssessmentInstanceMasterCheckDuplicate, sqlparam, "Assessment");
           }
           catch
           {

           }
           return DtAssessment;
       }

       public DataTable AssessmentInstanceGetByInstanceId(string AssessmentInstanceId)
       {
           DataTable DtAssessment = new DataTable();
           try
           {
               SqlParameter[] sqlparam = 
                {  
                    new SqlParameter("@AssessmentInstanceId_FK",AssessmentInstanceId ),
                };
               DtAssessment = objDBClass.ExecuteProc(AssessmentBE.SpAssessmentInstanceGetByInstanceId, sqlparam, "Assessment");
           }
           catch
           {

           }
           return DtAssessment;
       }

       public int AssessmentInstanceAssessmentUpdate(AssessmentBE objAssessmentBE)
       {
           int Status = 0;
           try
           {
               SqlParameter[] sqlparam = 
            {              
                 new SqlParameter ("@AssessmentInstanceId_FK", objAssessmentBE.propAssessmentInstanceId_FK),
                 new SqlParameter ("@AssessmentIds", objAssessmentBE.propAssessmentIds ),
                 new SqlParameter ("@CreatedBy", objAssessmentBE.propCreatedBy),
                 new SqlParameter ("@CreatedDate", objAssessmentBE.propCreatedDate),
            };
               Status = objDBClass.ExecuteProc(AssessmentBE.SpAssessmentInstanceAssessmentUpdate, sqlparam);
           }
           catch
           {

           }
           return Status;
       }

       public int AssessmentInstanceCompanyRelationUpdate(AssessmentBE objAssessmentBE)
       {
           int Status = 0;
           try
           {
               SqlParameter[] sqlparam = 
            {              
                 new SqlParameter ("@AssessmentInstanceId_FK", objAssessmentBE.propAssessmentInstanceId_FK),
                 new SqlParameter ("@CompanyId", objAssessmentBE.propCompanyIds ),
                 //new SqlParameter ("@ValidFrom", objAssessmentBE.propValidFrom),
                 //new SqlParameter ("@ValidTo", objAssessmentBE.propValidTo),
                 new SqlParameter ("@CreatedBy", objAssessmentBE.propCreatedBy),
                 new SqlParameter ("@CreatedDate", objAssessmentBE.propCreatedDate),
            };
               Status = objDBClass.ExecuteProc(AssessmentBE.SpAssessmentInstanceCompanyRelationUpdate, sqlparam);
           }
           catch
           {

           }
           return Status;
       }

       public int AssessmentInstanceCompanyByIdUpdate(AssessmentBE objAssessmentBE)
       {
           int Status = 0;
           try
           {
               SqlParameter[] sqlparam = 
            {              
                 new SqlParameter ("@AssessmentInstanceId_FK", objAssessmentBE.propAssessmentInstanceId_FK),
                 new SqlParameter ("@CompanyId_FK", objAssessmentBE.propCompanyId_FK ),
                 new SqlParameter ("@ValidFrom", objAssessmentBE.propValidFrom),
                 new SqlParameter ("@ValidTo", objAssessmentBE.propValidTo),
                 new SqlParameter ("@CreatedBy", objAssessmentBE.propCreatedBy),
                 new SqlParameter ("@CreatedDate", objAssessmentBE.propCreatedDate),
            };
               Status = objDBClass.ExecuteProc(AssessmentBE.SpAssessmentInstanceCompanyByIdUpdate, sqlparam);
           }
           catch
           {

           }
           return Status;
       }


      

       public DataTable GetAssessmentsByEmployeeId(string EmployeeId)
       {
           DataTable DtAssessment = new DataTable();
           try
           {
               SqlParameter[] sqlparam = 
                {  
                    new SqlParameter("@EmployeeId_PK",EmployeeId ),
                };
               DtAssessment = objDBClass.ExecuteProc(AssessmentBE.SpGetAssessmentsByEmployeeId, sqlparam, "Assessment");
           }
           catch
           {

           }
           return DtAssessment;
       }

       public DataTable EmployeeAssementsGetForCompany(string EmployeeId)
       {
           DataTable DtAssessment = new DataTable();
           try
           {
               SqlParameter[] sqlparam = 
                {  
                    new SqlParameter("@EmployeeId_FK",EmployeeId ),
                };
               DtAssessment = objDBClass.ExecuteProc(AssessmentBE.SpEmployeeAssementsGetForCompany, sqlparam, "Assessment");
           }
           catch
           {

           }
           return DtAssessment;
       }

       ///
       public DataTable AssessmentInstanceMasterGetByCompanyId(string CompanyId)
       {
           DataTable AssessmentIntance = new DataTable();
           try
           {
               SqlParameter[] sqlparam = 
                {  
                    new SqlParameter("@CompanyId",CompanyId),
                };
               AssessmentIntance = objDBClass.ExecuteProc(AssessmentBE.SpAssessmentInstanceMasterGetByCompanyId, sqlparam, "AssessmentIntance");
           }
           catch(Exception ex)
           {
                throw ex;
           }
           return AssessmentIntance;
       }

    }
}
