﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BE;
using System.Data.SqlClient;
using System.Data;

namespace DAL
{
    public class AnswerMasterDAL
    {
        DBClass objDBClass = new DBClass();

        public  DataTable AnswerMasterGet()
        {
            DataTable DtAnswer = new DataTable();
            try
            {
                DtAnswer = objDBClass.ExecuteProc(AnswerMasterBE.SpAnswerMasterGet);
            }
            catch 
            {

            }
            return DtAnswer;
        }
        public  DataTable AnswerMasterGetByQuestionId(string QuestionId)
        {
            DataTable DtAnswer = new DataTable();
            try
            {
                SqlParameter[] sqlparam=
                {
                    new SqlParameter("@QuestionId_FK",QuestionId),
                };

                DtAnswer = objDBClass.ExecuteProc(AnswerMasterBE.SpAnswerMasterGetByQuestionId,sqlparam,"AnswerMaster");
            }
            catch
            {

            }
            return DtAnswer;
        }
        public DataTable AnswerMasterGetByAnswerId(string AnswerId)
        {
            DataTable DtAnswer = new DataTable();
            try
            {
                SqlParameter[] sqlparam =
                {
                    new SqlParameter("@AnswerId_PK",AnswerId),
                };

                DtAnswer = objDBClass.ExecuteProc(AnswerMasterBE.SpAnswerMasterGetByAnswerId, sqlparam, "AnswerMaster");
            }
            catch
            {

            }
            return DtAnswer;
        }
        public  int AnswerMasterInsert(AnswerMasterBE objAnswerMasterBE)
        {
            int Status=0;
            try
            {
                SqlParameter[] sqlparam = 
            {                
                new SqlParameter ("@QuestionId_FK", objAnswerMasterBE.propQuestionId_FK),
                 new SqlParameter ("@Answer", objAnswerMasterBE.propAnswer),
                 new SqlParameter("@AnswerDescription",objAnswerMasterBE.propAnswerDescription),
                 new SqlParameter("@ScoreForAnswer",objAnswerMasterBE.propScoreForAnswer),
                 new SqlParameter("@CreatedBy",objAnswerMasterBE.propCreatedBy),
                 new SqlParameter("@CreatedDate",objAnswerMasterBE.propCreatedDate),
               //  new SqlParameter("@IsActive",objAnswerMasterBE.propIsActive),
            };
               Status  = objDBClass.ExecuteProc(AnswerMasterBE.SpAnswerMasterInsert, sqlparam);
            }
            catch
            {

            }
            return Status;
        }
        public  int AnswerMasterUpdate(AnswerMasterBE objAnswerMasterBE)
        {
            int Status = 0;
            try
            {
                SqlParameter[] sqlparam = 
            {              
                 new SqlParameter ("@AnswerId_PK", objAnswerMasterBE.propAnswerId_PK ),
                 new SqlParameter ("@QuestionId_FK", objAnswerMasterBE.propQuestionId_FK ),
                 new SqlParameter ("@Answer", objAnswerMasterBE.propAnswer),
                 new SqlParameter("@AnswerDescription",objAnswerMasterBE.propAnswerDescription),
                 new SqlParameter("@ScoreForAnswer",objAnswerMasterBE.propScoreForAnswer),
                 new SqlParameter("@UpdatedBy",objAnswerMasterBE.propUpdatedBy ),
                 new SqlParameter("@UpdatedDate",objAnswerMasterBE.propUpdatedDate),
                 //new SqlParameter("@IsActive",objAnswerMasterBE.propIsActive),
            };
                Status = objDBClass.ExecuteProc(AnswerMasterBE.SpAnswerMasterUpdate, sqlparam);
            }
            catch
            {

            }
            return Status;
        }
        public DataTable AnswerMasterGetWithDescriptionByQuestionId(string QuestionId)
        {
            DataTable DtAnswer = new DataTable();
            try
            {
                SqlParameter[] sqlparam =
                {
                    new SqlParameter("@QuestionId_FK",QuestionId),
                };

                DtAnswer = objDBClass.ExecuteProc(AnswerMasterBE.SpAnswerMasterGetWithDescriptionByQuestionId, sqlparam, "AnswerMaster");
            }
            catch
            {

            }
            return DtAnswer;
        }
        public DataTable GetQuestionID(string QuestionId,string batchid)
        {
            DataTable DtAnswer = new DataTable();
            QuestionId = QuestionId.Replace("\n", String.Empty);
            try
            {
                SqlParameter[] sqlparam =
                {
                    new SqlParameter("@Question",QuestionId),
                     new SqlParameter("@batchid",batchid),
                };

                DtAnswer = objDBClass.ExecuteProc(AnswerMasterBE.spGetQuestionID, sqlparam, "AnswerMaster");
            }
            catch (Exception ex)
            {
                ex.InnerException.ToString();
            }
            return DtAnswer;
        }


        public DataTable GetClusterID(string ClusterName)
        {
            DataTable DtAnswer = new DataTable();
            try
            {
                SqlParameter[] sqlparam =
                {
                    new SqlParameter("@ClusterName",ClusterName),
                };

                DtAnswer = objDBClass.ExecuteProc(AnswerMasterBE.spgetclusterID, sqlparam, "AnswerMaster");
            }
            catch
            {

            }
            return DtAnswer;
        }

        public DataTable GetAnswerID(string QuestionId, int scoreforanswer)
        {
            DataTable DtAnswer = new DataTable();
            try
            {
                SqlParameter[] sqlparam =
                {
                    new SqlParameter("@QuestionID_FK",QuestionId.Trim()),
                    new SqlParameter("@ScoreforAnswer",scoreforanswer),
                };

                DtAnswer = objDBClass.ExecuteProc(AnswerMasterBE.SpgetAnswerID_FK, sqlparam, "AnswerMaster");
            }
            catch (Exception ex)
            {
                throw (ex);
            }
            return DtAnswer;
        }
        public DataTable GetAnswerID1(string QuestionId, string answer,string batchid)
        {
            DataTable DtAnswer = new DataTable();
            try
            {
                SqlParameter[] sqlparam =
                {
                    new SqlParameter("@QuestionID_FK",QuestionId.Trim()),
                  //  new SqlParameter("@ScoreforAnswer",scoreforanswer),
                      new SqlParameter("@Answer",answer),
                    // new SqlParameter("@Category",Category),
                    new SqlParameter("@batchid",batchid)
                };

                DtAnswer = objDBClass.ExecuteProc(AnswerMasterBE.SpgetAnswerID_FK1, sqlparam, "AnswerMaster");
            }
            catch (Exception ex)
            {
                throw (ex);
            }
            return DtAnswer;
        }
        public DataTable GetAssessmentID(string Assessment)
        {
            DataTable DtAnswer = new DataTable();
            try
            {
                SqlParameter[] sqlparam =
                {
                    new SqlParameter("@AssessmentName",Assessment),

                };

                DtAnswer = objDBClass.ExecuteProc(AnswerMasterBE.SpGetAssessmentID, sqlparam, "AnswerMaster");
            }
            catch
            {

            }
            return DtAnswer;
        }
    }
}
