﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using BE;
using System.Data.SqlClient;
using System.Web;

namespace DAL
{
    public class EmployeeDAL
    {
        DBClass objDBClass = new DBClass();

        public DataTable EmployeesGetByCompanyId(string CompanyId, int Flag)
        {
            DataTable DtCompanyEmployees = new DataTable();
            try
            {
                SqlParameter[] sqlparam =
                 {
                    new SqlParameter("@CompanyId_FK",CompanyId ),
                    new SqlParameter("@Flag",Flag ),

                };

                DtCompanyEmployees = objDBClass.ExecuteProc(EmployeeBE.SpEmployeesGetByCompanyId, sqlparam, "CompanyEmployee");
            }
            catch
            {

            }
            return DtCompanyEmployees;
        }

        public DataTable EmployeesGetByCompanyIdddlsearch(string CompanyId, int Flag)
        {
            DataTable DtCompanyEmployees = new DataTable();
            try
            {
                SqlParameter[] sqlparam =
                 {
                    new SqlParameter("@CompanyId_FK",CompanyId ),
                    new SqlParameter("@Flag",Flag ),

                };

                DtCompanyEmployees = objDBClass.ExecuteProc("SpEmployeesGetByCompanyId_pending_complete_test", sqlparam, "CompanyEmployee");
            }
            catch
            {

            }
            return DtCompanyEmployees;
        }




        public DataTable EmployeesGetByCompanyId_Email(string CompanyId)
        {
            DataTable DtCompanyEmployees = new DataTable();
            try
            {
                SqlParameter[] sqlparam =
                 {
                    new SqlParameter("@CompanyId_FK",CompanyId ),
                 //   new SqlParameter("@Flag",Flag ),

                };

                DtCompanyEmployees = objDBClass.ExecuteProc("SpEmployeesGetByCompanyId_Email", sqlparam, "CompanyEmployee");
            }
            catch
            {

            }
            return DtCompanyEmployees;
        }


        public DataTable EmployeesGetByCompanyIdsearch(string CompanyId, int Flag)
        {
            DataTable DtCompanyEmployees = new DataTable();
            try
            {
                SqlParameter[] sqlparam =
                 {
                    new SqlParameter("@CompanyId_FK",CompanyId ),
                    new SqlParameter("@Flag",Flag ),

                };

                DtCompanyEmployees = objDBClass.ExecuteProc("sp_SearchEmployee", sqlparam, "CompanyEmployee");
            }
            catch
            {

            }
            return DtCompanyEmployees;
        }

        public DataTable EmployeesGetByNamesearch(string CompanyId, string Flag)
        {
            DataTable DtCompanyEmployees = new DataTable();
            try
            {
                SqlParameter[] sqlparam =
                 {
                    new SqlParameter("@CompanyId_FK",CompanyId ),
                    new SqlParameter("@Flag",Flag ),

                };

                DtCompanyEmployees = objDBClass.ExecuteProc("sp_SearchEmployeeByName", sqlparam, "CompanyEmployee");
            }
            catch
            {

            }
            return DtCompanyEmployees;
        }


        public DataTable EmployeesGetByCompanyIdbatchwise(string batchid)
        {
            DataTable DtCompanyEmployees = new DataTable();
            try
            {
                SqlParameter[] sqlparam =
                 {
                    //new SqlParameter("@CompanyId_FK",CompanyId ),
                    //new SqlParameter("@Flag",Flag ),
                    new SqlParameter("@BatchID_Fk",batchid)
                };

                DtCompanyEmployees = objDBClass.ExecuteProc(EmployeeBE.SP_Getbyemployeebatchwise, sqlparam, "CompanyEmployee");
            }
            catch
            {

            }
            return DtCompanyEmployees;
        }
        public DataTable ResultGetBybatchwise(string color, string batchid)
        {
            DataTable DtCompanyEmployees = new DataTable();
            try
            {
                SqlParameter[] sqlparam =
                 {
                    //new SqlParameter("@CompanyId_FK",CompanyId ),
                    new SqlParameter("@color",color ),
                    new SqlParameter("@Batchid",batchid)
                };

                DtCompanyEmployees = objDBClass.ExecuteProc("spGetCountofresult", sqlparam, "CompanyEmployee");
            }
            catch (Exception e)
            {
                e.InnerException.ToString();
            }
            return DtCompanyEmployees;
        }

        public DataTable NotReady_toShare_Details(string color, string batchid)
        {
            DataTable DtCompanyEmployees = new DataTable();
            try
            {
                SqlParameter[] sqlparam =
                 {
                    //new SqlParameter("@CompanyId_FK",CompanyId ),
                    new SqlParameter("@color",color ),
                    new SqlParameter("@Batchid",batchid)
                };

                DtCompanyEmployees = objDBClass.ExecuteProc("Sp_NotReady_toShare_Details", sqlparam, "CompanyEmployee");
            }
            catch (Exception e)
            {
                e.InnerException.ToString();
            }
            return DtCompanyEmployees;
        }


        public DataTable GetEmployeeBatchWise(string CompanyId, string @BatchID_FK)
        {
            DataTable DtCompanyEmployees = new DataTable();
            try
            {
                SqlParameter[] sqlparam =
                 {
                    new SqlParameter("@CompanyId_FK",CompanyId ),
                    new SqlParameter("@BatchID_FK",BatchID_FK ),
                };

                DtCompanyEmployees = objDBClass.ExecuteProc(EmployeeBE.SpGetEmployeeBatchWise, sqlparam, "CompanyEmployee");
            }
            catch
            {

            }
            return DtCompanyEmployees;
        }


        public DataTable SpDatewiseScoreByEmployeeCompanyId(string EmployeeId)
        {
            DataTable DtCompanyEmployees = new DataTable();
            try
            {
                SqlParameter[] sqlparam =
                 {
                    new SqlParameter("@EmployeeId",EmployeeId ),

                };

                DtCompanyEmployees = objDBClass.ExecuteProc(EmployeeBE.SpDatewiseScoreByEmployeeCompanyId, sqlparam, "CompanyEmployee");
            }
            catch
            {

            }
            return DtCompanyEmployees;
        }

        public int EmployeesInsert(EmployeeBE objEmployeeBE)
        {
            int Status = 0;

            Guid propCompanyId_FK;
            Guid.TryParse(objEmployeeBE.propCompanyId_FK, out propCompanyId_FK);

            Guid propCadreId_FK;
            Guid.TryParse(objEmployeeBE.propCadreId_FK, out propCadreId_FK);

            Guid propDeptId_PK;
            Guid.TryParse(objEmployeeBE.propDeptId_PK, out propDeptId_PK);

            Guid propCreatedBy;
            Guid.TryParse(objEmployeeBE.propCreatedBy, out propCreatedBy);

            Guid PropBatchID;
            Guid.TryParse(objEmployeeBE.PropBatchID, out PropBatchID);



            try
            {
                SqlParameter[] sqlparam =
             {
                 new SqlParameter ("@CompanyId_FK", propCompanyId_FK),
                 new SqlParameter ("@EmployeeCompanyId", objEmployeeBE.propEmployeeCompanyId),
                 new SqlParameter ("@UserName", objEmployeeBE.propUserName),
                 new SqlParameter("@Password",objEmployeeBE.propPassword),
                  new SqlParameter("@CadreId_FK",propCadreId_FK),
                  new SqlParameter("@DeptId_PK",propDeptId_PK),

                 new SqlParameter("@FirstName",objEmployeeBE.propFirstName ),
                 new SqlParameter("@LastName",objEmployeeBE.propLastName ),
                 new SqlParameter("@EmailId",objEmployeeBE.propEmailId ),

                  new SqlParameter("@BatchID_FK",PropBatchID ),
                 new SqlParameter("@CreatedBy",propCreatedBy),
             //   new SqlParameter("@CreatedDate",objEmployeeBE.propCreatedDate),

               //  new SqlParameter("@IsActive",objAnswerMasterBE.propIsActive),
            };
                Status = objDBClass.ExecuteProc(EmployeeBE.SpEmployeeInsert, sqlparam);


            }
            catch (Exception e)
            {
                e.InnerException.ToString();
            }
            return Status;
        }

        public DataTable EmployeesSignInGmailDetails(EmployeeBE objEmployeeBE)
        {
            DataTable DtEmployeeLogin = new DataTable();
            try
            {
                SqlParameter[] sqlparam =
                 {
                    new SqlParameter("@UserName",objEmployeeBE.propEmailId ),
                };
                DtEmployeeLogin = objDBClass.ExecuteProc(EmployeeBE.SpEmployeesLoginGmail, sqlparam, "EmployeeLogin");
            }
            catch
            {
                throw;
            }
            return DtEmployeeLogin;
        }

        public int EmployeesImport(EmployeeBE objEmployeeBE)
        {
            int Status = 0;
            try
            {
                SqlParameter[] sqlparam =
             {
                 new SqlParameter ("@CompanyId_FK", objEmployeeBE.propCompanyId_FK),
                 new SqlParameter ("@EmployeeCompanyId", objEmployeeBE.propEmployeeCompanyId),
                 new SqlParameter ("@UserName", objEmployeeBE.propUserName),
                 new SqlParameter("@Password",objEmployeeBE.propPassword),
                 new SqlParameter("@EmailId",objEmployeeBE.propEmailId ),
                 //new SqlParameter("@CadreId_FK",objEmployeeBE.propCadreId_FK),
                  //new SqlParameter("@DeptId_PK",objEmployeeBE.propDeptId_PK),
                 new SqlParameter("@FirstName",objEmployeeBE.propFirstName ),
                 new SqlParameter("@LastName",objEmployeeBE.propLastName ),
                  new SqlParameter("@BatchID_FK",objEmployeeBE.PropBatchID ), //added by meenu
                 new SqlParameter("@CreatedBy",objEmployeeBE.propCreatedBy),
                 new SqlParameter("@CreatedDate",objEmployeeBE.propCreatedDate),

               //  new SqlParameter("@IsActive",objAnswerMasterBE.propIsActive),
            };
                Status = objDBClass.ExecuteProc(EmployeeBE.SpEmployeeImport, sqlparam);


            }
            catch (Exception e)
            {
                e.InnerException.ToString();
            }
            return Status;
        }

        public int EmployeesInsertManager(EmployeeBE objEmployeeBE)
        {
            int Status = 0;
            try
            {
                SqlParameter[] sqlparam =
             {
                 new SqlParameter("@FirstName",objEmployeeBE.propFirstName ),
                 new SqlParameter("@LastName",objEmployeeBE.propLastName ),
                 new SqlParameter ("@EmployeeParentID", objEmployeeBE.propMangerID),




            };
                Status = objDBClass.ExecuteProc(EmployeeBE.SpInsertManger, sqlparam);


            }
            catch
            {

            }
            return Status;
        }


        public int BatchInsert(EmployeeBE objEmployeeBE)
        {
            int Status = 0;
            try
            {
                SqlParameter[] sqlparam =
             {
                 new SqlParameter("@Batchname",objEmployeeBE.PropBatchName ),
                 new SqlParameter("@CompanyId_FK",objEmployeeBE.propCompanyId_FK ),
                 new SqlParameter("@CreatedBy",objEmployeeBE.propCreatedBy),
                 new SqlParameter("@CreatedDate",objEmployeeBE.propCreatedDate),




            };
                Status = objDBClass.ExecuteProc(EmployeeBE.SpBatchInsert, sqlparam);


            }
            catch
            {

            }
            return Status;
        }
        public DataTable CompanyCadreGetByCompanyId(string CompanyId)
        {
            DataTable DtCompanyEmployees = new DataTable();
            try
            {
                SqlParameter[] sqlparam =
                 {
                    new SqlParameter("@CompanyId_FK",CompanyId ),
                };

                DtCompanyEmployees = objDBClass.ExecuteProc(EmployeeBE.SpCompanyCadreGetByCompanyId, sqlparam, "CompanyEmployee");
            }
            catch
            {

            }
            return DtCompanyEmployees;
        }

        public DataTable ToCheckBatch(string CompanyId, string BatchName)
        {
            DataTable DtCompanyEmployees = new DataTable();
            try
            {
                SqlParameter[] sqlparam =
                 {
                    new SqlParameter("@ComapnyID",CompanyId ),
                     new SqlParameter("@BatchName",BatchName ),
                };

                DtCompanyEmployees = objDBClass.ExecuteProc(EmployeeBE.Sp_ToCheckBatch, sqlparam, "CompanyEmployee");
            }
            catch
            {

            }
            return DtCompanyEmployees;
        }

        public DataTable GetBatchName(string ComapnyID)
        {
            DataTable DtCompanyEmployees = new DataTable();
            try
            {
                SqlParameter[] sqlparam =
                 {
                    new SqlParameter("@CompanyID",ComapnyID ),
                };

                DtCompanyEmployees = objDBClass.ExecuteProc(EmployeeBE.SpGetBatchName, sqlparam, "CompanyEmployee");
            }
            catch
            {

            }
            return DtCompanyEmployees;
        }
        public DataTable AssesmentWiseBatch()
        {
            DataTable DtCompanyEmployees = new DataTable();
            try
            {
                SqlParameter[] sqlparam =
                 {
                  //  new SqlParameter("@CompanyID",ComapnyID ),
                  //  new SqlParameter("@AssesmentInastnceID",AssessmentInstanceID ),
                };

                DtCompanyEmployees = objDBClass.ExecuteProc(EmployeeBE.spAssesmentWiseBatch, sqlparam, "CompanyEmployee");
            }
            catch
            {

            }
            return DtCompanyEmployees;
        }
        //--commented by meenakshi 17.12.2018
        //public DataTable AssesmentWiseBatch(string ComapnyID, string AssessmentInstanceID)
        //{
        //    DataTable DtCompanyEmployees = new DataTable();
        //    try
        //    {
        //        SqlParameter[] sqlparam =
        //         {
        //            new SqlParameter("@CompanyID",ComapnyID ),
        //            new SqlParameter("@AssesmentInastnceID",AssessmentInstanceID ),
        //        };

        //        DtCompanyEmployees = objDBClass.ExecuteProc(EmployeeBE.spAssesmentWiseBatch, sqlparam, "CompanyEmployee");
        //    }
        //    catch
        //    {

        //    }
        //    return DtCompanyEmployees;
        //}

        public DataTable EmployeeDetails(string CompanyId)
        {
            DataTable DtCompanyEmployees = new DataTable();
            try
            {
                SqlParameter[] sqlparam =
                 {
                    new SqlParameter("@CompnayID",CompanyId ),
                };

                DtCompanyEmployees = objDBClass.ExecuteProc(EmployeeBE.SpEmployeeDetails, sqlparam, "CompanyEmployee");
            }
            catch
            {

            }
            return DtCompanyEmployees;
        }

        public DataTable CompanyDepartmentGetByCompanyId(string CompanyId)
        {
            DataTable DtCompanyEmployees = new DataTable();
            try
            {
                SqlParameter[] sqlparam =
                 {
                    new SqlParameter("@CompanyId_FK",CompanyId ),
                };

                DtCompanyEmployees = objDBClass.ExecuteProc(EmployeeBE.SpCompanyDepartmentGetByCompanyId, sqlparam, "CompanyEmployee");
            }
            catch
            {

            }
            return DtCompanyEmployees;
        }
        public DataTable EmployeesLogin(string UserName, string Password)
        {

            DataTable DtEmployeeLogin = new DataTable();
            try
            {
                SqlParameter[] sqlparam =
                 {
                    new SqlParameter("@UserName",UserName ),
                    new SqlParameter("@Password",Password ),
                };
                DtEmployeeLogin = objDBClass.ExecuteProc(EmployeeBE.SpEmployeesLogin, sqlparam, "EmployeeLogin");
            }
            catch
            {
                throw;
            }
            return DtEmployeeLogin;
        }

        public DataTable EmployeesGetByEmployeeId(string EmployeeId)
        {
            DataTable DtEmployees = new DataTable();
            try
            {
                SqlParameter[] sqlparam =
                 {
                    new SqlParameter("@EmployeeId",EmployeeId ),
                };

                DtEmployees = objDBClass.ExecuteProc(EmployeeBE.SpEmployeesGetByEmployeeId, sqlparam, "Employee");
            }
            catch
            {

            }
            return DtEmployees;
        }

        public int EmployeesUpdate(EmployeeBE objEmployeeBE)
        {
            int Status = 0;
            try
            {
                SqlParameter[] sqlparam =
             {
                 new SqlParameter ("@EmployeeId_PK", objEmployeeBE.propEmployeeId_PK),
                 new SqlParameter ("@UserName", objEmployeeBE.propUserName),
                 new SqlParameter("@EmailId",objEmployeeBE.propEmailId ),
                 new SqlParameter("@FirstName",objEmployeeBE.propFirstName ),
                 new SqlParameter("@LastName",objEmployeeBE.propLastName ),
                 new SqlParameter("@EmployeeAddress",objEmployeeBE.propEmployeeAddress ),
                 new SqlParameter("@Country",objEmployeeBE.propCountry),
                 new SqlParameter("@City",objEmployeeBE.propCity),
                 new SqlParameter("@ZipCode",objEmployeeBE.propZipCode),
                 new SqlParameter("@ContactNo",objEmployeeBE.propContactNo),
                 new SqlParameter("@MobileNo",objEmployeeBE.propMobileNo),
                 new SqlParameter("@ExperienceInYears",objEmployeeBE.propExperienceInYears),
                 new SqlParameter("@Age",DateTime.Parse( objEmployeeBE.propAge)),
                 new SqlParameter("@Position",objEmployeeBE.propPosition),
                 new SqlParameter("@Qualification",objEmployeeBE.propQualification),
                 new SqlParameter("@Occupation",objEmployeeBE.propOccupation),
                 new SqlParameter("@Gender",objEmployeeBE.propGender),
                  //new SqlParameter("@CadreId_Pk",objEmployeeBE.propCadreId_FK),
                  //new SqlParameter("@DeptID",objEmployeeBE.propDeptId_PK),
                 new SqlParameter("@UpdatedBy",objEmployeeBE.propUdpatedBy),
                 new SqlParameter("@UpdatedDate",objEmployeeBE.propUpdatedDate),

               //  new SqlParameter("@IsActive",objAnswerMasterBE.propIsActive),
            };
                Status = objDBClass.ExecuteProc(EmployeeBE.SpEmployeesUpdate, sqlparam);
            }
            catch (Exception )
            {

            }
            return Status;
        }

        public int EmployeesFirstUpdate(EmployeeBE objEmployeeBE)
        {
            int Status = 0;
            string chk1 = HttpContext.Current.Session["chk"].ToString();
            try
            {
                DateTime dateAge = DateTime.ParseExact(objEmployeeBE.propAge.ToString(), "MM/dd/yyyy", null);

                SqlParameter[] sqlparam =
             {
                 new SqlParameter ("@EmployeeId_PK", objEmployeeBE.propEmployeeId_PK),
                 new SqlParameter("@ExperienceInYears",objEmployeeBE.propExperienceInYears),
                 new SqlParameter("@Age",dateAge),
                 new SqlParameter("@Position",objEmployeeBE.propPosition),
                 new SqlParameter("@Qualification",objEmployeeBE.propQualification),
                 new SqlParameter("@Occupation",objEmployeeBE.propOccupation),
                 new SqlParameter("@Gender",objEmployeeBE.propGender),
                 new SqlParameter("@UpdatedBy",objEmployeeBE.propUdpatedBy),
                 new SqlParameter("@UpdatedDate",DateTime.Parse(objEmployeeBE.propUpdatedDate.ToShortDateString())),
                 new SqlParameter("@ReadyToshareDetails",chk1),
               //  new SqlParameter("@IsActive",objAnswerMasterBE.propIsActive),
            };
                Status = objDBClass.ExecuteProc(EmployeeBE.SpEmployeesFirstUpdate, sqlparam);

            }
            catch (Exception ex)
            {
                ex.InnerException.ToString();
            }
            return Status;
        }

        public int EmployeesUpdateByCompanyId(EmployeeBE objEmployeeBE)
        {
            int Status = 0;
            Guid propCompanyId_FK;
            Guid.TryParse("propCompanyId_FK", out propCompanyId_FK);

            Guid propCadreId_FK;
            Guid.TryParse("propCadreId_FK", out propCadreId_FK);

            Guid propDeptId_PK;
            Guid.TryParse("propDeptId_PK", out propDeptId_PK);

            Guid propCreatedBy;
            Guid.TryParse("propCreatedBy", out propCreatedBy);

            Guid PropBatchID;
            Guid.TryParse("PropBatchID", out PropBatchID);

            try
            {
                SqlParameter[] sqlparam =
             {
                 new SqlParameter ("@EmployeeId_PK", objEmployeeBE.propEmployeeId_PK),
                 new SqlParameter ("@EmployeeCompanyId", objEmployeeBE.propEmployeeCompanyId),
                 new SqlParameter ("@CadreId_FK", objEmployeeBE.propCadreId_FK),
                 new SqlParameter ("@FirstName", objEmployeeBE.propFirstName ),
                 new SqlParameter ("@LastName", objEmployeeBE.propLastName ),
                 new SqlParameter ("@EmailId", objEmployeeBE.propEmailId),
                 new SqlParameter("@UpdatedBy",objEmployeeBE.propUdpatedBy),
                 new SqlParameter("@UpdatedDate",objEmployeeBE.propUpdatedDate),

               //  new SqlParameter("@IsActive",objAnswerMasterBE.propIsActive),
            };
                Status = objDBClass.ExecuteProc(EmployeeBE.SpEmployeesUpdateByCompanyId, sqlparam);

            }
            catch
            {

            }
            return Status;
        }

        public int EmployeesRegistrationInsert(EmployeeBE objEmployeeBE)
        {
            int Status = 0;

            try
            {
                SqlParameter[] sqlparam =
             {
            //     new SqlParameter ("@EmployeeId_PK", objEmployeeBE.propEmployeeId_PK),
              //   new SqlParameter ("@EmployeeCompanyId", objEmployeeBE.propEmployeeCompanyId),
                 new SqlParameter ("@CompanyId_FK", objEmployeeBE.propCompanyId_FK),
                 new SqlParameter ("@FirstName", objEmployeeBE.propFirstName ),
                 new SqlParameter ("@LastName", objEmployeeBE.propLastName ),
                 new SqlParameter ("@EmailId", objEmployeeBE.propEmailId),
                 new SqlParameter("@BatchID_FK",objEmployeeBE.PropBatchID),
                 new SqlParameter ("@UserName", objEmployeeBE.propUserName),
                 new SqlParameter("@Password",objEmployeeBE.propPassword),
                 //new SqlParameter("@EmployeeCompanyId",objEmployeeBE.propEmployeeCompanyId),
                 //  new SqlParameter("@Occupation", objEmployeeBE.propOccupation),
                 //  new SqlParameter("@ExperienceInYears", objEmployeeBE.propExperienceInYears),
                     new SqlParameter ("@Termsandconditions",  objEmployeeBE.PropTandD),
                     new SqlParameter("@Varificationcode",objEmployeeBE.Verificationcode),
                     new SqlParameter("@MobileNo",objEmployeeBE.propMobileNo)

            };
                Status = objDBClass.ExecuteProc("SpEmployeeRegistration", sqlparam);

            }
            catch (Exception e)
            {
                e.InnerException.ToString();
            }
            return Status;
        }

        public int EmployeesRegistrationInsert1(EmployeeBE objEmployeeBE)
        {
            int Status = 0;

            try
            {
                SqlParameter[] sqlparam =
             {
            //     new SqlParameter ("@EmployeeId_PK", objEmployeeBE.propEmployeeId_PK),
              //   new SqlParameter ("@EmployeeCompanyId", objEmployeeBE.propEmployeeCompanyId),
                 new SqlParameter ("@CompanyId_FK", objEmployeeBE.propCompanyId_FK),
                 new SqlParameter ("@FirstName", objEmployeeBE.propFirstName ),
                 new SqlParameter ("@LastName", objEmployeeBE.propLastName ),
                 new SqlParameter ("@EmailId", objEmployeeBE.propEmailId),
                 new SqlParameter("@BatchID_FK",objEmployeeBE.PropBatchID),
                 new SqlParameter ("@UserName", objEmployeeBE.propUserName),
                 new SqlParameter("@Password",objEmployeeBE.propPassword),
                  //  new SqlParameter("@Occupation", objEmployeeBE.propOccupation),
                 //   new SqlParameter("@ExperienceInYears", objEmployeeBE.propExperienceInYears),
                     //new SqlParameter ("@Termsandconditions",  objEmployeeBE.PropTandD),
                     //   new SqlParameter("@Varificationcode",objEmployeeBE.Varificationcode)

            };
                Status = objDBClass.ExecuteProc("SpEmployeeRegistrationfb", sqlparam);

            }
            catch (Exception e)
            {
                e.InnerException.ToString();
            }
            return Status;
        }

        public int EmployeesUpdateByCompanyIdImport(EmployeeBE objEmployeeBE)
        {
            int Status = 0;
            try
            {
                SqlParameter[] sqlparam =
             {
                 new SqlParameter ("@EmployeeId_PK", objEmployeeBE.propEmployeeId_PK),
                 new SqlParameter ("@EmployeeCompanyId", objEmployeeBE.propEmployeeCompanyId),
                 new SqlParameter ("@CadreId_FK", objEmployeeBE.propCadreId_FK),
                 new SqlParameter ("@DeptId_PK", objEmployeeBE.propDeptId_PK),
                 new SqlParameter ("@Batch_PK", objEmployeeBE.PropBatchID),
                 new SqlParameter ("@FirstName", objEmployeeBE.propFirstName ),
                 new SqlParameter ("@LastName", objEmployeeBE.propLastName ),
                 new SqlParameter ("@EmailId", objEmployeeBE.propEmailId),
                 new SqlParameter("@UpdatedBy",objEmployeeBE.propUdpatedBy),
                 new SqlParameter("@UpdatedDate",objEmployeeBE.propUpdatedDate),

               //  new SqlParameter("@IsActive",objAnswerMasterBE.propIsActive),
            };
                Status = objDBClass.ExecuteProc(EmployeeBE.SpEmployeesUpdateByCompanyId, sqlparam);

            }
            catch
            {

            }
            return Status;
        }

        public DataTable EmployeeAssessmentClusterwiseReport(int EmployeeId)
        {
            DataTable DtEmployees = new DataTable();
            try
            {
                SqlParameter[] sqlparam =
                 {
                    new SqlParameter("@EmployeeCompanyId",EmployeeId ),
                };

                DtEmployees = objDBClass.ExecuteProc(EmployeeBE.SpEmployeeAssessmentClusterwiseReport, sqlparam, "Employee");
            }
            catch
            {

            }
            return DtEmployees;
        }

        public DataTable AssessmentFinalResult(int EmployeeId)
        {
            DataTable DtEmployees = new DataTable();
            try
            {
                SqlParameter[] sqlparam =
                 {
                    new SqlParameter("@EmployeeId",EmployeeId ),
                };

                DtEmployees = objDBClass.ExecuteProc(EmployeeBE.SpAssessmentFinalResult, sqlparam, "Employee");
            }
            catch
            {

            }
            return DtEmployees;
        }

        public DataTable ResultTextMasterGet(int AverageScore)
        {
            DataTable DtEmployees = new DataTable();
            try
            {
                SqlParameter[] sqlparam =
                 {
                    new SqlParameter("@AverageScore",AverageScore ),
                };

                DtEmployees = objDBClass.ExecuteProc(EmployeeBE.SpResultTextMasterGet, sqlparam, "Employee");
            }
            catch
            {

            }
            return DtEmployees;
        }

        public DataTable GetStdVsChetanaclusterScore(string EmployeeId, string CompanyId)
        {
            DataTable DtEmployees = new DataTable();
            try
            {
                SqlParameter[] sqlparam =
                 {
                    new SqlParameter("@EmployeeID",EmployeeId),
                    new SqlParameter("@CompanyID",CompanyId ),
                };

                DtEmployees = objDBClass.ExecuteProc(EmployeeBE.SpGetStdVsChetanaclusterScore, sqlparam, "Employee");
            }
            catch
            {

            }
            return DtEmployees;
        }

        public DataTable GetStdVsChetanaScoreByCompanyID(string CompanyId)
        {
            DataTable DtEmployees = new DataTable();
            try
            {
                SqlParameter[] sqlparam =
                 {
                    new SqlParameter("@CompanyID",CompanyId ),
                };

                DtEmployees = objDBClass.ExecuteProc(EmployeeBE.SpStdVsChetanaScoreByCompanyID, sqlparam, "Employee");
            }
            catch
            {

            }
            return DtEmployees;
        }

        public DataTable SuggestionMasterGetRandom()
        {
            DataTable DtEmployee = new DataTable();
            try
            {
                DtEmployee = objDBClass.ExecuteProc(EmployeeBE.SpSuggestionMasterGetRandom);
            }
            catch
            {

            }
            return DtEmployee;
        }

        public int EmployeesDelete(string EmployeeId, string CompanyId)
        {
            int status = 0;
            try
            {
                SqlParameter[] sqlparam =
                 {
                    new SqlParameter("@EmployeeId",EmployeeId ),
                      new SqlParameter("@CompanyId",CompanyId ),
                };

                status = objDBClass.ExecuteProc(EmployeeBE.SpEmployeesDelete, sqlparam);
            }
            catch
            {

            }
            return status;
        }

        public DataTable EmployeeAssessmentSubmittedGetByEmployeeId(string EmployeeId)
        {
            DataTable DtEmployees = new DataTable();
            try
            {
                SqlParameter[] sqlparam =
                 {
                    new SqlParameter("@EmployeeId",EmployeeId ),
                };

                DtEmployees = objDBClass.ExecuteProc(EmployeeBE.SpEmployeeAssessmentSubmittedGetByEmployeeId, sqlparam, "Employee");
            }
            catch
            {

            }
            return DtEmployees;
        }

        public DataTable EmployeeGetByEmployeeCompanyId(string CompanyId, string EmployeeId)
        {
            DataTable DtCompanyEmployees = new DataTable();
            try
            {
                SqlParameter[] sqlparam =
                 {
                    new SqlParameter("@CompanyId",CompanyId ),
                    new SqlParameter("@EmployeeId",EmployeeId ),
                };

                DtCompanyEmployees = objDBClass.ExecuteProc(EmployeeBE.SpEmployeeGetByEmployeeCompanyId, sqlparam, "CompanyEmployee");
            }
            catch
            {

            }
            return DtCompanyEmployees;
        }

        public DataTable EmployeeResultGet(string EmployeeId, string AssessmentId)
        {
            DataTable DtCompanyEmployees = new DataTable();
            try
            {
                SqlParameter[] sqlparam =
                 {
                    new SqlParameter("@EmployeeId",EmployeeId ),
                    new SqlParameter("@AssessmentId",AssessmentId ),
                };

                DtCompanyEmployees = objDBClass.ExecuteProc(EmployeeBE.SpEmployeeResultGet, sqlparam, "CompanyEmployee");
            }
            catch
            {

            }
            return DtCompanyEmployees;
        }

        public DataTable EmployeesGetByCadreId(string CompanyId, string CadreId)
        {
            DataTable DtCompanyEmployees = new DataTable();
            try
            {
                SqlParameter[] sqlparam =
                 {
                    new SqlParameter("@CompanyId",CompanyId ),
                    new SqlParameter("@CadreId",CadreId ),
                };

                DtCompanyEmployees = objDBClass.ExecuteProc(EmployeeBE.SpEmployeesGetByCadreId, sqlparam, "CompanyEmployee");
            }
            catch
            {

            }
            return DtCompanyEmployees;
        }

        public DataTable EmployeeGetByName(string CompanyId, string Name)
        {
            DataTable DtCompanyEmployees = new DataTable();
            try
            {
                SqlParameter[] sqlparam =
                 {
                    new SqlParameter("@CompanyId",CompanyId ),
                    new SqlParameter("@Name",Name ),
                };

                DtCompanyEmployees = objDBClass.ExecuteProc(EmployeeBE.SpEmployeeGetByName, sqlparam, "CompanyEmployee");
            }
            catch
            {

            }
            return DtCompanyEmployees;
        }

        public DataTable SpEmployeeAllResultGet(string EmployeeId)
        {
            DataTable DtCompanyEmployees = new DataTable();
            try
            {
                SqlParameter[] sqlparam =
                 {
                    new SqlParameter("@EmployeeId",EmployeeId ),
                };

                DtCompanyEmployees = objDBClass.ExecuteProc(EmployeeBE.SpEmployeeAllResultGet, sqlparam, "CompanyEmployee");
            }
            catch
            {

            }
            return DtCompanyEmployees;
        }

        public DataTable EmployeeResultGetForEmployee(string EmployeeId, string AssessmentInstanceId, string AssessmentInstanceCompanyId)
        {
            DataTable DtCompanyEmployees = new DataTable();
            try
            {
                SqlParameter[] sqlparam =
                 {
                    new SqlParameter("@EmployeeId",EmployeeId ),
                    new SqlParameter("@AssessmentInstanceId",AssessmentInstanceId ),
                       new SqlParameter("@AssessmentInstanceCompanyId",AssessmentInstanceCompanyId ),
                };

                DtCompanyEmployees = objDBClass.ExecuteProc(EmployeeBE.SpEmployeeResultGetForEmployee, sqlparam, "CompanyEmployee");
            }
            catch
            {

            }
            return DtCompanyEmployees;
        }

        //30 Jan 16
        public DataTable ParentEmployeeRelationshipGetParent(Guid EmployeeId)
        {
            DataTable DtCompanyEmployees = new DataTable();
            try
            {
                SqlParameter[] sqlparam =
                 {
                    new SqlParameter("@EmployeeId",EmployeeId ),
                };

                DtCompanyEmployees = objDBClass.ExecuteProc(EmployeeBE.SpParentEmployeeRelationshipGetParent, sqlparam, "CompanyEmployee");
            }
            catch
            {

            }
            return DtCompanyEmployees;
        }

        public DataTable ParentEmployeeRelationshipGetByParentId(Guid EmployeeId, int Flag)
        {
            DataTable DtCompanyEmployees = new DataTable();
            try
            {
                SqlParameter[] sqlparam =
                 {
                    new SqlParameter("@EmployeeId",EmployeeId ),
                     new SqlParameter("@Flag",Flag ),
                };

                DtCompanyEmployees = objDBClass.ExecuteProc(EmployeeBE.SpParentEmployeeRelationshipGetByParentId, sqlparam, "CompanyEmployee");
            }
            catch
            {

            }
            return DtCompanyEmployees;
        }

        public DataTable EmployeeGetByNameByEmployeeParentId(Guid EmployeeParentId, string Name)
        {
            DataTable DtCompanyEmployees = new DataTable();
            try
            {
                SqlParameter[] sqlparam =
                 {
                    new SqlParameter("@EmployeeParentId",EmployeeParentId ),
                    new SqlParameter("@Name",Name ),
                };

                DtCompanyEmployees = objDBClass.ExecuteProc(EmployeeBE.SpEmployeeGetByNameByEmployeeParentId, sqlparam, "CompanyEmployee");
            }
            catch
            {

            }
            return DtCompanyEmployees;
        }

        public DataTable EmployeesGetEmployeesNotSubmited(Guid CompanyId)
        {
            DataTable DtCompanyEmployees = new DataTable();
            try
            {
                SqlParameter[] sqlparam =
                 {
                    new SqlParameter("@CompanyId",CompanyId ),
                };

                DtCompanyEmployees = objDBClass.ExecuteProc(EmployeeBE.SpEmployeesGetEmployeesNotSubmited, sqlparam, "CompanyEmployee");
            }
            catch
            {

            }
            return DtCompanyEmployees;
        }

        public DataTable CompanyDeaprtmentGetByCompanyId(string CompanyId)
        {
            throw new NotImplementedException();
        }


        public DataTable Percentageget(string batchid)
        {
            DataTable dtgetpercentage = new DataTable();
            try
            {
                SqlParameter[] sqlparam =
                 {

                    new SqlParameter("@BatchID_Fk",batchid)
                };

                dtgetpercentage = objDBClass.ExecuteProc("Percentageget", sqlparam, "CompanyEmployee");
            }
            catch
            {

            }
            return dtgetpercentage;
        }


        public DataTable get_color(string CompanyId, string batchid)
        {
            DataTable dtgetcolors = new DataTable();
            try
            {
                SqlParameter[] sqlparam =
                 {
                    new SqlParameter("@companyid",CompanyId),
                    new SqlParameter("@batchid",batchid)

                };

                dtgetcolors = objDBClass.ExecuteProc("sp_getclusterpivot", sqlparam, "CompanyEmployee");
            }
            catch
            {

            }
            return dtgetcolors;
        }

        public int UpdateVarificationFlag(EmployeeBE objEmployeeBE)
        {
            int status = 0;
            DataTable Dtupdate = new DataTable();
            try
            {
                SqlParameter[] sqlparam =
                 {
                    new SqlParameter("@Varificationcode",objEmployeeBE.Verificationcode),

                };
                status = objDBClass.ExecuteProc("usp_update_VarificationCode", sqlparam);
            }
            catch (Exception )
            {
                throw;
            }
            return status;
        }


        public DataTable GetEmployeeDetails(EmployeeBE objEmployeeBE)
        {
            //int status = 0;
            DataTable Dtupdate = new DataTable();
            try
            {
                SqlParameter[] sqlparam =
                 {
                    new SqlParameter("@VarificationFlag",objEmployeeBE.VerificationFlag),
                      new SqlParameter("@PaymentFlag",objEmployeeBE.PaymentFlag),
                };
                Dtupdate = objDBClass.ExecuteProc("usp_Get_Approved_Employess", sqlparam, "");
            }
            catch
            {
                throw;
            }
            return Dtupdate;
        }

        public DataTable GetEmployee(EmployeeBE objEmployeeBE)
        {
            //int status = 0;
            DataTable Dtupdate = new DataTable();
            try
            {
                SqlParameter[] sqlparam =
                 {
                    new SqlParameter("@Varificationcode",objEmployeeBE.Verificationcode),

                };
                Dtupdate = objDBClass.ExecuteProc("usp_Get_Employee", sqlparam, "");
            }
            catch (Exception )
            {
                throw;
            }
            return Dtupdate;
        }


        public int EmployeesRegistrationGmailInsert(EmployeeBE objEmployeeBE)
        {
            int Status = 0;

            try
            {
                SqlParameter[] sqlparam =
             {
            //     new SqlParameter ("@EmployeeId_PK", objEmployeeBE.propEmployeeId_PK),
              //   new SqlParameter ("@EmployeeCompanyId", objEmployeeBE.propEmployeeCompanyId),
                 new SqlParameter ("@CompanyId_FK", objEmployeeBE.propCompanyId_FK),
                 new SqlParameter ("@FirstName", objEmployeeBE.propFirstName ),
                 new SqlParameter ("@LastName", objEmployeeBE.propLastName ),
                 new SqlParameter ("@EmailId", objEmployeeBE.propEmailId),
                 new SqlParameter("@BatchID_FK",objEmployeeBE.PropBatchID),
                 new SqlParameter ("@UserName", objEmployeeBE.propUserName),
                 new SqlParameter("@Password",objEmployeeBE.propPassword),
                  //  new SqlParameter("@Occupation", objEmployeeBE.propOccupation),
                 //   new SqlParameter("@ExperienceInYears", objEmployeeBE.propExperienceInYears),
                     //new SqlParameter ("@Termsandconditions",  objEmployeeBE.PropTandD),
                       // new SqlParameter("@Varificationcode",objEmployeeBE.Varificationcode)

            };
                Status = objDBClass.ExecuteProc("SpEmployeeRegistrationfb", sqlparam);

            }
            catch (Exception e)
            {
                e.InnerException.ToString();
            }
            return Status;
        }

        //public DataTable GetCompanyIDBatchID(string InstanceCompanyRelationId)
        //{
        //    DataTable Status = new DataTable();

        //    try
        //    {
        //        SqlParameter[] sqlparam =
        //     {

        //         new SqlParameter ("@InstanceCompanyRelationId_PK", InstanceCompanyRelationId),

        //    };
        //        Status = objDBClass.ExecuteProc("usp_Get_CompanyIDBatchID", sqlparam);

        //    }
        //    catch (Exception e)
        //    {
        //        e.InnerException.ToString();
        //    }
        //    return Status;
        //}
        public DataTable GetCompanyIDBatchID(string InstanceCompanyRelationId)
        {
            //int status = 0;
            DataTable DtCompanyIDBatchID = new DataTable();
            try
            {
                SqlParameter[] sqlparam =
                 {
                    new SqlParameter("@InstanceCompanyRelationId_PK",InstanceCompanyRelationId),

                };
                DtCompanyIDBatchID = objDBClass.ExecuteProc("usp_Get_CompanyIDBatchID", sqlparam, "");
            }
            catch
            {
                throw;
            }
            return DtCompanyIDBatchID;
        }

        public DataTable GetEmployeePrice(EmployeeBE objEmployeeBE)
        {
            //int status = 0;
            DataTable Dtupdate = new DataTable();
            try
            {
                SqlParameter[] sqlparam =
                 {
                    new SqlParameter("@CompanyId_FK",objEmployeeBE.propCompanyId_FK),
                    new SqlParameter("@EmployeeId_PK",objEmployeeBE.propEmployeeId_PK),

                };
                Dtupdate = objDBClass.ExecuteProc("GetCouponPrice", sqlparam, "");
            }
            catch
            {
                throw;
            }
            return Dtupdate;
        }

        public DataTable GetEmployeeByEmail(EmployeeBE objEmployeeBE)
        {
            //int status = 0;
            DataTable Dtupdate = new DataTable();
            try
            {
                SqlParameter[] sqlparam =
                 {
                    new SqlParameter("@EmailID",objEmployeeBE.propUserName),


                };
                Dtupdate = objDBClass.ExecuteProc("usp_Get_EmployeeByEmail", sqlparam, "");
            }
            catch
            {
                throw;
            }
            return Dtupdate;
        }

        public DataTable GetEmployeebyEmpid(EmployeeBE objEmployeeBE)
        {
            //int status = 0;
            DataTable Dtupdate = new DataTable();
            try
            {
                SqlParameter[] sqlparam =
                 {
                    new SqlParameter("@EmpId",objEmployeeBE.propEmployeeId_PK),

                };
                Dtupdate = objDBClass.ExecuteProc("usp_Get_Employee_byEmpid", sqlparam, "");
            }
            catch
            {
                throw;
            }
            return Dtupdate;
        }
        public DataTable GetEmployeebyFreeTestEmpid(EmployeeBE objEmployeeBE)
        {
            //int status = 0;
            DataTable EmpDeTails = new DataTable();
            try
            {
                SqlParameter[] sqlparam =
                 {
                    new SqlParameter("@EmpId",objEmployeeBE.propEmployeeId_PK),

                };
                EmpDeTails = objDBClass.ExecuteProc("usp_Get_EmployeeFreetest_byEmpid", sqlparam, "");
            }
            catch
            {
                throw;
            }
            return EmpDeTails;
        }
        public int EmployeesSuggestFrndInsert(List<SuggestFriend> objSuggestFriendList)
        {
            int Status = 0;

            try
            {
                foreach (SuggestFriend objSuggestFriend in objSuggestFriendList)
                {


                    SqlParameter[] sqlparam =
                    {
                         new SqlParameter ("@strFirstName", objSuggestFriend.FirstName),
                         new SqlParameter ("@strLastName", objSuggestFriend.LastName ),
                         new SqlParameter ("@strEmail", objSuggestFriend.Email ),
                         new SqlParameter ("@strCreatedBy", objSuggestFriend.CreatedBy),
                    };

                    Status = objDBClass.ExecuteProc("SpSuggestFriendDetailsInsert", sqlparam);
                }
            }
            catch (Exception e)
            {
                e.InnerException.ToString();
            }
            return Status;
        }
        public int UpdateTestFlag(EmployeeBE objEmployeeBE)
        {
            int status = 0;
            DataTable Dtupdate = new DataTable();
            try
            {
                SqlParameter[] sqlparam =
                 {
                    new SqlParameter("@EmployeeId_PK",objEmployeeBE.propEmployeeId_PK),
                     new SqlParameter("@TestFlag",objEmployeeBE.propTestFlag),
                };
                status = objDBClass.ExecuteProc("usp_update_testFlag", sqlparam);
            }
            catch (Exception )
            {
                throw;
            }
            return status;
        }
        public int UpdatePaidTestFlag(EmployeeBE objEmployeeBE)
        
{
            int status = 0;
            DataTable Dtupdate = new DataTable();
            try
            {
                SqlParameter[] sqlparam =
                {
                     new SqlParameter("@EmployeeId_PK",objEmployeeBE.propEmployeeId_PK),
                     new SqlParameter("@PaidFlag", objEmployeeBE.PaymentFlag),
                };
                status = objDBClass.ExecuteProc("usp_updatePaidTestFlag", sqlparam);
            }
            catch (Exception)
            {
                throw;
            }
            return status;
        }

        public int EmployeesAcceptTermUpdate(EmployeeBE objEmployeeBE)
        {
            int Status = 0;
            string chk1 = HttpContext.Current.Session["chk"].ToString();
            try
            {

                SqlParameter[] sqlparam =
             {
                 new SqlParameter ("@EmployeeId_PK", objEmployeeBE.propEmployeeId_PK),
                 new SqlParameter("@UpdatedBy",objEmployeeBE.propUdpatedBy),
                 new SqlParameter("@UpdatedDate",DateTime.Parse(objEmployeeBE.propUpdatedDate.ToShortDateString())),
                 new SqlParameter("@ReadyToshareDetails",chk1),
               //  new SqlParameter("@IsActive",objAnswerMasterBE.propIsActive),
            };
                Status = objDBClass.ExecuteProc(EmployeeBE.SpEmployeesFirstUpdate, sqlparam);

            }
            catch (Exception ex)
            {
                ex.InnerException.ToString();
            }
            return Status;
        }
    }
}
