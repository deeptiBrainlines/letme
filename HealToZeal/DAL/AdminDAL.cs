﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BE;
using System.Data;
using System.Data.SqlClient;
namespace DAL
{
   public  class AdminDAL
    {
       DBClass objDBDBClass = new DBClass();
       public DataTable SpAdminLogin(string UserName, string Password)
       {

           DataTable DtAdmin = new DataTable();
           try
           {
               SqlParameter[] sqlparam = 
                {  
                    new SqlParameter("@Name",UserName ),
                    new SqlParameter("@Password",Password ),
                };
               DtAdmin= objDBDBClass.ExecuteProc(AdminBE.SpAdminLogin , sqlparam, "Admin");
           }
           catch
           {

           }
           return DtAdmin;
       }
    }
}
