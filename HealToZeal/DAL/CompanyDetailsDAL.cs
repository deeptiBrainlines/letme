﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using BE;
using System.Data.SqlClient;
namespace DAL
{
    
   public class CompanyDetailsDAL
    {
       DBClass objDBDBClass = new DBClass();
   
       public  int CompanyDetailsInsert(CompanyDetailsBE objCompanyDetailsBE)
       {
           int Status=0;
           try
           {
               SqlParameter[] sqlparam = 
            {              
                 new SqlParameter ("@CompanyName", objCompanyDetailsBE.propCompanyName),
                 new SqlParameter ("@DomainId_FK", objCompanyDetailsBE.propDomainId_FK ),
                 new SqlParameter ("@NoOfEmployees", objCompanyDetailsBE.propNoOfEmployees),
                 new SqlParameter ("@UserName", objCompanyDetailsBE.propUserName),
                 new SqlParameter ("@Password", objCompanyDetailsBE.propPassword),
                 new SqlParameter ("@CompanyAddress", objCompanyDetailsBE.propCompanyAddress),
                 new SqlParameter ("@City", objCompanyDetailsBE.propCity ),
                 new SqlParameter ("@Country", objCompanyDetailsBE.propCountry ),
                 new SqlParameter ("@ZipCode", objCompanyDetailsBE.propZipCode),
                 new SqlParameter ("@CompanyContactNo", objCompanyDetailsBE.propCompanyContactNo ),
                 new SqlParameter ("@ContactPersonName", objCompanyDetailsBE.propContactPersonName  ),
                 new SqlParameter ("@EmailId", objCompanyDetailsBE.propEmailId),
                 new SqlParameter ("@ContactPersonContactNo", objCompanyDetailsBE.propContactPersonContactNo),
                 new SqlParameter ("@MobileNo", objCompanyDetailsBE.propMobileNo),
                 new SqlParameter ("@CreatedBy", objCompanyDetailsBE.propCreatedBy),
                 new SqlParameter ("@CreatedDate", objCompanyDetailsBE.propCreatedDate ),
                   //new SqlParameter ("@IsActive", objCompanyDetailsBE.propIsActive)
            };
               Status = objDBDBClass.ExecuteProc(CompanyDetailsBE.SpCompanyDetailsInsert , sqlparam);
               
           }
           catch 
           {
               
           }
           return Status;
       }

       public int CompanyDetailsUpdate(CompanyDetailsBE objCompanyDetailsBE)
       {
           int Status = 0;
           try
           {
               SqlParameter[] sqlparam = 
            {      
                 new SqlParameter ("@CompanyId_PK",objCompanyDetailsBE.propCompanyId_PK ),
                 new SqlParameter ("@CompanyName", objCompanyDetailsBE.propCompanyName),
                 new SqlParameter ("@DomainId_FK", objCompanyDetailsBE.propDomainId_FK ),
                 new SqlParameter ("@NoOfEmployees", objCompanyDetailsBE.propNoOfEmployees),
                 new SqlParameter ("@UserName", objCompanyDetailsBE.propUserName),
                 //new SqlParameter ("@Password", objCompanyDetailsBE.propPassword),
                 new SqlParameter ("@CompanyAddress", objCompanyDetailsBE.propCompanyAddress),
                 new SqlParameter ("@City", objCompanyDetailsBE.propCity ),
                 new SqlParameter ("@Country", objCompanyDetailsBE.propCountry ),
                 new SqlParameter ("@ZipCode", objCompanyDetailsBE.propZipCode),
                 new SqlParameter ("@CompanyContactNo", objCompanyDetailsBE.propCompanyContactNo ),
                 new SqlParameter ("@ContactPersonName", objCompanyDetailsBE.propContactPersonName  ),
                 new SqlParameter ("@EmailId", objCompanyDetailsBE.propEmailId),
                 new SqlParameter ("@ContactPersonContactNo", objCompanyDetailsBE.propContactPersonContactNo),
                 new SqlParameter ("@MobileNo", objCompanyDetailsBE.propMobileNo),
                 new SqlParameter ("@UpdatedBy", objCompanyDetailsBE.propUpdatedBy ),
                 new SqlParameter ("@UpdatedDate", objCompanyDetailsBE.propUpdatedDate),
                 //new SqlParameter ("@IsActive", objCompanyDetailsBE.propIsActive)
            };
               Status = objDBDBClass.ExecuteProc(CompanyDetailsBE.SpCompanyDetailsUpdate, sqlparam);

           }
           catch
           {

           }
           return Status;
       }

       public  DataTable CompanyDetailsGet()
       {
           DataTable DtCompanyDetail=new DataTable();
           try
           {              
            DtCompanyDetail= objDBDBClass.ExecuteProc(CompanyDetailsBE.SpCompanyDetailsGet);
           }
           catch
           {
               
           }
           return DtCompanyDetail;
       }

       //code by Poonam
       

       public DataTable GetAllDepartments()
       {
           DataTable DtCompanyDetail = new DataTable();
           try
           {
               DtCompanyDetail = objDBDBClass.ExecuteProc(CompanyDetailsBE.SpGetAllDepartments);
           }
           catch
           {

           }
           return DtCompanyDetail;
       }

       public DataTable GetALlCadre()
       {
           DataTable DtCompanyDetail = new DataTable();
           try
           {
               DtCompanyDetail = objDBDBClass.ExecuteProc(CompanyDetailsBE.SpGetALlCadre);
           }
           catch
           {

           }
           return DtCompanyDetail;
       }

       public DataTable GetAllCompanyDetails()
       {
           DataTable DtCompanyDetail = new DataTable();
           try
           {
               DtCompanyDetail = objDBDBClass.ExecuteProc(CompanyDetailsBE.SpGetAllCompanyDetails);
           }
           catch
           {

           }
           return DtCompanyDetail;
       }

        public DataTable CompanyReportStatisticsGet2(string CompanyId, string batchid)
        {
            DataTable DtCompanyDetail = new DataTable();
            try
            {
                //if (DeptID == "Department" || DeptID == "All")
                //{
                //    DeptID = Guid.NewGuid().ToString();
                //}
                //if (Batch_FK == "Select Batch")
                //{
                //    Batch_FK = Guid.NewGuid().ToString();
                //}
                SqlParameter[] sqlparam =
                 {
                    new SqlParameter("@CompanyId",CompanyId ),
                    //  new SqlParameter("@AssessmentInstanceId",AssessmentInstanceId ),
                      //  new SqlParameter("@DeptID_FK",DeptID),
                        new SqlParameter("@BatchID_FK",batchid),
                };

                DtCompanyDetail = objDBDBClass.ExecuteProc(CompanyDetailsBE.SPCompanyReportStatisticsnew, sqlparam, "CompanyProfile");
            }
            catch (Exception ex)
            {
                ex.InnerException.ToString();
            }
            return DtCompanyDetail;
        }
        public DataTable ClusterPercentageGetByCompanyId2(string CompanyId, string BatchID)
        {
            DataTable DtCompanyDetail = new DataTable();
            ///Guid aGuid = new Guid(AssessmentInstanceId);
            //Guid aGuid;
            // Guid.TryParse(AssessmentInstanceId, out aGuid);

            Guid acompnayid;
            Guid.TryParse(CompanyId, out acompnayid);

            //   Guid DeptID1;
            //   Guid.TryParse(DeptID, out DeptID1);

            Guid BatchID1;
            Guid.TryParse(BatchID, out BatchID1);
            //  AssessmentInstanceId = Guid.NewGuid().ToString();


            try
            {

                SqlParameter[] sqlparam =
                 {
                    new SqlParameter("@CompanyId", acompnayid ),
              //     new SqlParameter("@AssessmentInstanceId",aGuid),
                
             //   new SqlParameter("@DeptID_FK",DeptID1 ),
                    new SqlParameter("@Batch_FK",BatchID1 ),
                };
               // DtCompanyDetail = objDBDBClass.ExecuteProc(CompanyDetailsBE.Sp_clusterpercentage3_1_19, sqlparam, "CompanyProfile");
               DtCompanyDetail = objDBDBClass.ExecuteProc("Spclusterpercentagebatch1219", sqlparam, "CompanyProfile");
            }
            catch (Exception ex)
            {
                ex.InnerException.ToString();
            }
            return DtCompanyDetail;
        }

        public DataTable CompanyReportStatisticsGet(string CompanyId,string batchid)
       {
           DataTable DtCompanyDetail = new DataTable();
           try
           {
               //if (DeptID == "Department" || DeptID == "All")
               //{
               //    DeptID = Guid.NewGuid().ToString();
               //}
               //if (Batch_FK == "Select Batch")
               //{
               //    Batch_FK = Guid.NewGuid().ToString();
               //}
               SqlParameter[] sqlparam = 
                {  
                    new SqlParameter("@CompanyId",CompanyId ),
                    //  new SqlParameter("@AssessmentInstanceId",AssessmentInstanceId ),
                      //  new SqlParameter("@DeptID_FK",DeptID),
                        new SqlParameter("@BatchID_FK",batchid),
                };

               DtCompanyDetail = objDBDBClass.ExecuteProc(CompanyDetailsBE.SPCompanyReportStatisticsnew, sqlparam, "CompanyProfile");
           }
           catch(Exception ex)
           {
                ex.InnerException.ToString();
           }
           return DtCompanyDetail;
       }

        public DataTable CompanyReportStatisticsGet1(string CompanyId, string instanceid)
        {
            DataTable DtCompanyDetail = new DataTable();
            try
            {
                //if (DeptID == "Department" || DeptID == "All")
                //{
                //    DeptID = Guid.NewGuid().ToString();
                //}
                //if (Batch_FK == "Select Batch")
                //{
                //    Batch_FK = Guid.NewGuid().ToString();
                //}
                SqlParameter[] sqlparam =
                 {
                    new SqlParameter("@CompanyId",CompanyId ),
                 new SqlParameter("@AssessmentInstanceId",instanceid ),
                    //new SqlParameter("@DeptID_FK",DeptID),
                   //new SqlParameter("@BatchID_FK",batchid),
                };

                DtCompanyDetail = objDBDBClass.ExecuteProc("SPCompanyReportStatisticsnew1_2_19", sqlparam, "CompanyProfile");
            }
            catch (Exception ex)
            {
                ex.InnerException.ToString();
            }
            return DtCompanyDetail;
        }

        public DataTable ClusterPercentageGetByCompanyId1(string CompanyId, string instanceid)
        {
            DataTable DtCompanyDetail = new DataTable();
            ///Guid aGuid = new Guid(AssessmentInstanceId);
            //Guid aGuid;
            // Guid.TryParse(AssessmentInstanceId, out aGuid);

            Guid acompnayid;
            Guid.TryParse(CompanyId, out acompnayid);

            //   Guid DeptID1;
            //   Guid.TryParse(DeptID, out DeptID1);

            Guid instanceid1;
            Guid.TryParse(instanceid, out instanceid1);
            //  AssessmentInstanceId = Guid.NewGuid().ToString();


            try
            {

                SqlParameter[] sqlparam =
                 {
                    new SqlParameter("@CompanyId", acompnayid ),
              //     new SqlParameter("@AssessmentInstanceId",aGuid),
                
             //   new SqlParameter("@DeptID_FK",DeptID1 ),
                    new SqlParameter("@AssessmentInstanceId",instanceid1 ),
                };
               // DtCompanyDetail = objDBDBClass.ExecuteProc(CompanyDetailsBE.SpClusterPercentage, sqlparam, "CompanyProfile");
                DtCompanyDetail = objDBDBClass.ExecuteProc("SpClusterPercentage_1_2_19", sqlparam, "CompanyProfile");
            }
            catch (Exception ex)
            {
                ex.InnerException.ToString();
            }
            return DtCompanyDetail;
        }

        public DataTable CompanyDetailsGetByCompanyId(string CompanyId)
       {
           DataTable DtCompanyDetail = new DataTable();
           try
           {
               SqlParameter[] sqlparam = 
                {  
                    new SqlParameter("@CompanyId_PK",CompanyId ),
                };
               DtCompanyDetail = objDBDBClass.ExecuteProc(CompanyDetailsBE.SpCompanyDetailsGetByCompanyId, sqlparam, "CompanyProfile");
           }
           catch(Exception ex)
           {
                ex.InnerException.ToString();
            }
           return DtCompanyDetail;
       }

        public DataTable ClusterPercentageGetByCompanyId(string CompanyId,string BatchID)
        {
            DataTable DtCompanyDetail = new DataTable();
         ///Guid aGuid = new Guid(AssessmentInstanceId);
            //Guid aGuid;
           // Guid.TryParse(AssessmentInstanceId, out aGuid);

            Guid acompnayid;
            Guid.TryParse(CompanyId, out acompnayid);

         //   Guid DeptID1;
         //   Guid.TryParse(DeptID, out DeptID1);

            Guid BatchID1;
            Guid.TryParse(BatchID, out BatchID1);
            //  AssessmentInstanceId = Guid.NewGuid().ToString();


            try
            {

                SqlParameter[] sqlparam =
                 {
                    new SqlParameter("@CompanyId", acompnayid ),
              //     new SqlParameter("@AssessmentInstanceId",aGuid),
                
             //   new SqlParameter("@DeptID_FK",DeptID1 ),
                    new SqlParameter("@Batch_FK",BatchID1 ),
                };
               
                DtCompanyDetail = objDBDBClass.ExecuteProc(CompanyDetailsBE.Sp_clusterpercentage3_1_19, sqlparam, "CompanyProfile");
               // DtCompanyDetail = objDBDBClass.ExecuteProc("SpClusterPercentage11", sqlparam, "CompanyProfile");
            }
            catch (Exception ex)
            {
                ex.InnerException.ToString();
            }
            return DtCompanyDetail;
        }

        //commented on 18.12.18

        //public DataTable ClusterPercentageGetByCompanyId(string CompanyId, string AssessmentInstanceId,string DeptID,string BatchID)
        //{
        //    DataTable DtCompanyDetail = new DataTable();
        //    try
        //    {

        //        SqlParameter[] sqlparam = 
        //         {  
        //             new SqlParameter("@CompanyId",CompanyId ),
        //             new SqlParameter("@AssessmentInstanceId",AssessmentInstanceId ),

        //              new SqlParameter("@DeptID_FK",DeptID ),
        //               new SqlParameter("@Batch_FK",BatchID ),
        //         };
        //        DtCompanyDetail = objDBDBClass.ExecuteProc(CompanyDetailsBE.SpClusterPercentage, sqlparam, "CompanyProfile");
        //    }
        //    catch(Exception ex)
        //    {
        //         ex.InnerException.ToString();
        //     }
        //    return DtCompanyDetail;
        //}


        public DataTable CompanyDetailsLogin(string UserName, string Password)
       {
          
               DataTable DtCompanyDetail = new DataTable();
               try
               {
                   SqlParameter[] sqlparam = 
                {  
                    new SqlParameter("@UserName",UserName ),
                    new SqlParameter("@Password",Password ),
                };
                   DtCompanyDetail = objDBDBClass.ExecuteProc(CompanyDetailsBE.SpCompanyDetailsLogin, sqlparam, "CompanyProfile");
               }
               catch(Exception e)
               {
               throw e;
               }
               return DtCompanyDetail;
       }

       public DataTable EmployeeResultGetForCompany(string EmployeeId, string AssessmentInstanceId, string AssessmentInstanceCompanyId, string AssessmentId)
       {
           DataTable DtCompanyEmployees = new DataTable();
           try
           {
               SqlParameter[] sqlparam =
                {                
                    new SqlParameter("@EmployeeId",EmployeeId ),
                    new SqlParameter("@AssessmentInstanceId",AssessmentInstanceId ),
                       new SqlParameter("@AssessmentInstanceCompanyId",AssessmentInstanceCompanyId ),
                           new SqlParameter("@AssessmentId",AssessmentId ),
                };

               DtCompanyEmployees = objDBDBClass.ExecuteProc(CompanyDetailsBE.SpEmployeeResultGetForCompany, sqlparam, "CompanyEmployee");
           }
           catch
           {

           }
           return DtCompanyEmployees;
       }

       public DataTable CompanyDetailsGetByEmployeeId(string EmployeeId)
       {
           DataTable DtCompanyDetail = new DataTable();
           try
           {
               SqlParameter[] sqlparam = 
                {  
                    new SqlParameter("@EmployeeId",EmployeeId ),
                };
               DtCompanyDetail = objDBDBClass.ExecuteProc(CompanyDetailsBE.SpCompanyDetailsGetByEmployeeId, sqlparam, "CompanyProfile");
           }
           catch
           {

           }
           return DtCompanyDetail;
       }

       public DataTable GetcompnyName(string p)
       {
           throw new NotImplementedException();
       }


        public DataTable Getcount(string AnswerID_FK, string QuestionID_Fk)
        {
            DataTable DtCompanyDetail = new DataTable();
            try
            {
                SqlParameter[] sqlparam =
                 {
                    new SqlParameter("@AnswerID_FK",AnswerID_FK ),
                      new SqlParameter("@QuestionID_Fk",QuestionID_Fk ),

                };

                DtCompanyDetail = objDBDBClass.ExecuteProc(CompanyDetailsBE.SpGetcountofEmployee, sqlparam, "CompanyProfile");
            }
            catch
            {

            }
            return DtCompanyDetail;
        }

        public DataTable GetAnswer(string QuestionID_Fk)
        {
            DataTable DtCompanyDetail = new DataTable();
            try
            {
                SqlParameter[] sqlparam =
                 {
                   // new SqlParameter("@AnswerID_FK",AnswerID_FK ),
                      new SqlParameter("@QuestionID_Fk",QuestionID_Fk ),

                };

                DtCompanyDetail = objDBDBClass.ExecuteProc(CompanyDetailsBE.Sp_GetAnswerData, sqlparam, "CompanyProfile");
            }
            catch
            {

            }
            return DtCompanyDetail;
        }
        public DataTable GetEmployeeResultNew(string EmployeeId_FK, string Category)
        {
            DataTable DtCompanyDetail = new DataTable();
            try
            {
                SqlParameter[] sqlparam =
                 {
                    new SqlParameter("@EmployeeId_FK",EmployeeId_FK ),
                      new SqlParameter("@Category",Category ),

                };

                DtCompanyDetail = objDBDBClass.ExecuteProc(CompanyDetailsBE.sp_GetStudentResult, sqlparam, "CompanyProfile");
            }
            catch(Exception ex)
            {

            }
            return DtCompanyDetail;
        }


        public DataTable Getclsuterscore_byemployee(string EmployeeId_FK,string CompanyId_FK)
        {
            DataTable DtCompanyDetail = new DataTable();
            try
            {
                SqlParameter[] sqlparam =
                 {
                    new SqlParameter("@CompanyId",CompanyId_FK),
                    new SqlParameter("@EmployeeId",EmployeeId_FK ),
                    
                };

                DtCompanyDetail = objDBDBClass.ExecuteProc(CompanyDetailsBE.SpGetPercentageClusterwise, sqlparam, "CompanyProfile");
            }
            catch
            {

            }
            return DtCompanyDetail;
        }


        //public DataTable Getclsuterscore_byemployee(string EmployeeId_FK)
        //{
        //    DataTable DtCompanyDetail = new DataTable();
        //    try
        //    {
        //        SqlParameter[] sqlparam =
        //         {
        //            new SqlParameter("@EmployeeId_FK",EmployeeId_FK ),

        //        };

        //        DtCompanyDetail = objDBDBClass.ExecuteProc(CompanyDetailsBE.Sp_get_Allclusterscore, sqlparam, "CompanyProfile");
        //    }
        //    catch
        //    {

        //    }
        //    return DtCompanyDetail;
        //}



        public DataTable GetEmployeeResultNewClusterwise(string EmployeeId_FK, string ClusterName)
        {
            DataTable DtCompanyDetail = new DataTable();
            try
            {
                SqlParameter[] sqlparam =
                 {
                    new SqlParameter("@EmployeeID_FK",EmployeeId_FK ),
                      new SqlParameter("@ClusterName",ClusterName ),

                };

                DtCompanyDetail = objDBDBClass.ExecuteProc(CompanyDetailsBE.usp_GetClusterScore, sqlparam, "CompanyProfile");
            }
            catch(Exception ex)
            {

            }
            return DtCompanyDetail;
        }


        public DataTable Sp_get_clustername(string ClusterName)
        {
            DataTable DtCompanyDetail = new DataTable();
            try
            {
                SqlParameter[] sqlparam =
                 {
                    //new SqlParameter("@EmployeeID_FK",EmployeeId_FK ),
                      new SqlParameter("@ClusterName",ClusterName ),

                };

                DtCompanyDetail = objDBDBClass.ExecuteProc(CompanyDetailsBE.Sp_get_clustername, sqlparam, "CompanyProfile");
            }
            catch
            {

            }
            return DtCompanyDetail;
        }

    }
}
