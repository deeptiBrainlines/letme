﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using BE;

namespace DAL
{
  public  class AssessmentQuestionRelationDAL
    {
      DBClass objDBClass = new DBClass();
      /// <summary>
      /// 30 dec 2014
      /// </summary>
      /// <param name="AssessmentId"></param>
      /// <returns>datatable</returns>
      public DataTable AssessmentQuestionsRelationGet(string EmployeeId, int position, string AssessmentInstanceId, string AssessmentInstanceCompanyId)
      {
          DataTable DtAssessmentQuestionRelation = new DataTable();
          try
          {
              SqlParameter[] sqlparam = 
                {  
                    new SqlParameter("@EmployeeId_FK",EmployeeId ),
                    new SqlParameter("@Position",position  ),
                    new SqlParameter("@AssessmentInstanceId_FK",AssessmentInstanceId),
                    new SqlParameter("@InstanceCompanyRelationId_FK",AssessmentInstanceCompanyId),
                    
                };
              DtAssessmentQuestionRelation = objDBClass.ExecuteProc(AssessmentQuestionRelationBE.SpAssessmentQuestionsRelationGet, sqlparam, "DtAssessmentQuestionRelation");
          }
          catch
          {

          }
          return DtAssessmentQuestionRelation;
      }

        public DataTable AssessmentFreeQuestionsRelationGet(string EmployeeId, int position, string AssessmentInstanceId, string AssessmentInstanceCompanyId)
        {
            DataTable DtAssessmentQuestionRelation = new DataTable();
            try
            {
                SqlParameter[] sqlparam =
                  {
                    new SqlParameter("@EmployeeId_FK",EmployeeId ),
                    new SqlParameter("@Position",position  ),
                    new SqlParameter("@AssessmentInstanceId_FK",AssessmentInstanceId),
                    new SqlParameter("@InstanceCompanyRelationId_FK",AssessmentInstanceCompanyId),

                };
                DtAssessmentQuestionRelation = objDBClass.ExecuteProc("SpAssessmentFreeQuestionsRelationGet", sqlparam, "DtAssessmentQuestionRelation");
            }
            catch
            {

            }
            return DtAssessmentQuestionRelation;
        }
    }
}
