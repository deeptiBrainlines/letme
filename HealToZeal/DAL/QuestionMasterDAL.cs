﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using BE;
using System.Data.SqlClient;

namespace DAL
{
  public class QuestionMasterDAL
    {
      DBClass objDBClass = new DBClass();

      public DataTable QuestionMasterGet()
      {
          DataTable DtQuestion = new DataTable();
          try
          {             
              DtQuestion = objDBClass.ExecuteProc(QuestionMasterBE.SpQuestionMasterGet);         
          }
          catch 
          {
              
          }
          return DtQuestion;
      }

      public DataTable QuestionMasterGetByQuestionId(string QuestionId)
      {
          DataTable DtQuestion = new DataTable();
          try
          {
              SqlParameter[] sqlparam = 
                {  
                    new SqlParameter("@QuestionId_PK",QuestionId ),
                };
              DtQuestion = objDBClass.ExecuteProc(QuestionMasterBE.SpQuestionMasterGetByQuestionId, sqlparam, "QuestionMaster");
          }
          catch
          {

          }
          return DtQuestion;
      }

      public  int  QuestionMasterInsert(QuestionMasterBE objQuestionMasterBE)
      {
          int  Status = 0;
          try
          {
              SqlParameter[] sqlparam = 
            {              
              
                 new SqlParameter ("@ClusterIds", objQuestionMasterBE.propClusterIds ),
                 new SqlParameter ("@Question", objQuestionMasterBE.propQuestion),
                 new SqlParameter ("@NoOfAnswers", objQuestionMasterBE.propNoOfAnswers ),
                 new SqlParameter ("@IsReverseScore", objQuestionMasterBE.propIsReverseScore   ),
                 new SqlParameter ("@Weightage", objQuestionMasterBE.propWeightage ),
                 new SqlParameter ("@FlagNeutralAnswer", objQuestionMasterBE.propFlagNeutralAnswer),
                 new SqlParameter ("@CreatedBy", objQuestionMasterBE.propCreatedBy),
                 new SqlParameter ("@CreatedDate", objQuestionMasterBE.propCreatedDate),
                 new SqlParameter("@Answer",objQuestionMasterBE.propAnswerTable),
                   new SqlParameter("@AssessmentId_FK",objQuestionMasterBE.propAssessmentId_FK)
            };
              //Status = objDBClass.ExecuteProcWithReturnStringValue(QuestionMasterBE.SpQuestionMasterInsert, sqlparam);
              Status = objDBClass.ExecuteProc(QuestionMasterBE.SpQuestionMasterInsert,sqlparam);
          }
          catch 
          {
              
          }
          return Status;
      }

      public  int QuestionMasterUpdate(QuestionMasterBE objQuestionMasterBE)
      {
          int Status = 0;
          try
          {
              SqlParameter[] sqlparam = 
            {              
                 new SqlParameter ("@QuestionId_PK", objQuestionMasterBE.propQuestionId_PK),
                 new SqlParameter ("@ClusterIds", objQuestionMasterBE.propClusterIds ),
                 new SqlParameter ("@Question", objQuestionMasterBE.propQuestion ),
                 new SqlParameter ("@NoOfAnswers", objQuestionMasterBE.propNoOfAnswers),
                 new SqlParameter ("@IsReverseScore", objQuestionMasterBE.propIsReverseScore),
                 new SqlParameter ("@Weightage", objQuestionMasterBE.propWeightage ),
                 new SqlParameter ("@FlagNeutralAnswer", objQuestionMasterBE.propFlagNeutralAnswer),
                 new SqlParameter ("@UpdatedBy", objQuestionMasterBE.propUpdatedBy ),
                 new SqlParameter ("@UpdatedDate", objQuestionMasterBE.propUpdatedDate),
            };
              Status = objDBClass.ExecuteProc(QuestionMasterBE.SpQuestionMasterUpdate, sqlparam);
          }
          catch
          {

          }
          return Status;
      }

      public DataTable QuestionMasterGetTotalnoAnswersForQuestion(string QuestionId)
      {
          DataTable DtQuestion = new DataTable();
          try
          {
              SqlParameter[] sqlparam = 
                {  
                    new SqlParameter("@QuestionId_PK",QuestionId ),
                };
              DtQuestion = objDBClass.ExecuteProc(QuestionMasterBE.SpQuestionMasterGetTotalnoAnswersForQuestion, sqlparam, "QuestionMaster");
          }
          catch
          {

          }
          return DtQuestion;
      }

      public DataTable QuestionMasterGetByAssessmentId(string AssessmentId)
      {
          DataTable DtQuestion = new DataTable();
          try
          {
              SqlParameter[] sqlparam = 
                {  
                    new SqlParameter("@AssessmentId_FK",AssessmentId ),
                };
              DtQuestion = objDBClass.ExecuteProc(QuestionMasterBE.SpQuestionMasterGetByAssessmentId, sqlparam, "QuestionMaster");
          }
          catch
          {

          }
          return DtQuestion;
      }
    }
}
