﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BE;
using DAL;
using System.Data;
using System.Data.SqlClient;

namespace DAL
{
  public  class RemedySuggestionsDAL
    {
      DBClass objDBClass = new DBClass();

      public DataTable RemedySuggestionsGet()
      {
          DataTable DtRemedySuggestion = new DataTable();
          try
          {
              DtRemedySuggestion = objDBClass.ExecuteProc(RemedySuggestionsBE.SpRemedySuggestionsGet);
          }
          catch
          {

          }
          return DtRemedySuggestion;
      }
      public int RemedySuggestionsInsert(RemedySuggestionsBE objRemedySuggestionsBE)
      {
          int Status = 0;
          try
          {
              SqlParameter[] sqlparam = 
            {              
                 new SqlParameter ("@Suggestion", objRemedySuggestionsBE.propSuggestion ),
                 new SqlParameter ("@AssessmentId_FK",objRemedySuggestionsBE.propAssessmentId_FK  ),
                 new SqlParameter ("@ClusterId_FK", objRemedySuggestionsBE.propClusterId_FK),
                 new SqlParameter ("@Color", objRemedySuggestionsBE.propColor ),
                 new SqlParameter ("@MinScore",objRemedySuggestionsBE.propMinScore ),
                 new SqlParameter ("@MaxScore", objRemedySuggestionsBE.propMaxScore),
                 new SqlParameter ("@CreatedBy", objRemedySuggestionsBE.propCreatedBy),
                 new SqlParameter ("@CreatedDate", objRemedySuggestionsBE.propCreatedDate),
            };
              Status = objDBClass.ExecuteProc(RemedySuggestionsBE.SpRemedySuggestionsInsert, sqlparam);
          }
          catch
          {

          }
          return Status;
      }

      public int RemedySuggestionsUpdate(RemedySuggestionsBE objRemedySuggestionsBE)
      {
          int Status = 0;
          try
          {
              SqlParameter[] sqlparam = 
            {     
                 new SqlParameter ("@RemedySuggestionId_PK",objRemedySuggestionsBE.RemedySuggestionId_PK  ),      
                 new SqlParameter ("@Suggestion", objRemedySuggestionsBE.propSuggestion ),
                 new SqlParameter ("@AssessmentId_FK",objRemedySuggestionsBE.propAssessmentId_FK  ),
                 new SqlParameter ("@ClusterId_FK", objRemedySuggestionsBE.propClusterId_FK),
                 new SqlParameter ("@Color", objRemedySuggestionsBE.propColor ),
                 new SqlParameter ("@MinScore",objRemedySuggestionsBE.propMinScore ),
                 new SqlParameter ("@MaxScore", objRemedySuggestionsBE.propMaxScore),
                 new SqlParameter ("@CreatedBy", objRemedySuggestionsBE.propCreatedBy),
                 new SqlParameter ("@CreatedDate", objRemedySuggestionsBE.propCreatedDate),
            };
              Status = objDBClass.ExecuteProc(RemedySuggestionsBE.SpRemedySuggestionsUpdate, sqlparam);
          }
          catch
          {

          }
          return Status;
      }

      public DataTable RemedySuggestionsGetById(string RemedySuggestionId)
      {
          DataTable DtRemedySuggestion = new DataTable();
          try
          {
              SqlParameter[] sqlparam =
                {
                    new SqlParameter("@RemedySuggestionId_PK",RemedySuggestionId),
                };

              DtRemedySuggestion = objDBClass.ExecuteProc(RemedySuggestionsBE.SpRemedySuggestionsGetById, sqlparam, "RemedySuggestion");
          }
          catch
          {

          }
          return DtRemedySuggestion;
      }
      public DataTable RemedySuggestionsGetByAssessmentAndClusterId(string AssessmentId, string ClusterId)
      {
          DataTable DtRemedySuggestion = new DataTable();
          try
          {
              SqlParameter[] sqlparam =
                {
                    new SqlParameter("@AssessmentId_FK",AssessmentId),
                    new SqlParameter("@ClusterId_FK",ClusterId),
                };

              DtRemedySuggestion = objDBClass.ExecuteProc(RemedySuggestionsBE.SpRemedySuggestionsGetByAssessmentAndClusterId, sqlparam, "RemedySuggestion");
          }
          catch
          {

          }
          return DtRemedySuggestion;
      }
      
    }
}
