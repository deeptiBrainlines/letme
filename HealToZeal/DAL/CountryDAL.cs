﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Data;

namespace DAL
{
    public  class CountryDAL
    {
        DBClass objDBClass = new DBClass();

        public const string SpCityMasterGetByCountryId = "SpCityMasterGetByCountryId";
        public const string SpCountryMasterGet = "SpCountryMasterGet";
        public const string SpCompanyDomainGet = "SpCompanyDomainGet";

        public DataTable CountryMasterGet()
        {
            DataTable DtCountry = new DataTable();
            try
            {
                DtCountry = objDBClass.ExecuteProc(CountryDAL.SpCountryMasterGet );
            }
            catch
            {

            }
            return DtCountry;
        }
        public DataTable CityMasterGetByCountryId(int CountryId)
        {
            DataTable DtCity = new DataTable();
            try
            {
                SqlParameter[] sqlparam =
                {
                new SqlParameter ("@CountryID_FK",CountryId )
                };
                DtCity = objDBClass.ExecuteProc(CountryDAL.SpCityMasterGetByCountryId,sqlparam,"City");
            }
            catch
            {

            }
            return DtCity;
        }
        public DataTable CompanyDomainGet()
        {
            DataTable DtCountry = new DataTable();
            try
            {
                DtCountry = objDBClass.ExecuteProc(CountryDAL.SpCompanyDomainGet);
            }
            catch
            {

            }
            return DtCountry;
        }
    }
}
